.class public final enum Lcom/google/d/a/a/ry;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ry;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ry;

.field public static final enum b:Lcom/google/d/a/a/ry;

.field public static final enum c:Lcom/google/d/a/a/ry;

.field public static final enum d:Lcom/google/d/a/a/ry;

.field public static final enum e:Lcom/google/d/a/a/ry;

.field public static final enum f:Lcom/google/d/a/a/ry;

.field public static final enum g:Lcom/google/d/a/a/ry;

.field private static final synthetic i:[Lcom/google/d/a/a/ry;


# instance fields
.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 448
    new-instance v0, Lcom/google/d/a/a/ry;

    const-string v1, "SERVICE_FOOD"

    invoke-direct {v0, v1, v7, v3}, Lcom/google/d/a/a/ry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ry;->a:Lcom/google/d/a/a/ry;

    .line 452
    new-instance v0, Lcom/google/d/a/a/ry;

    const-string v1, "SERVICE_RESTAURANT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v3, v2}, Lcom/google/d/a/a/ry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ry;->b:Lcom/google/d/a/a/ry;

    .line 456
    new-instance v0, Lcom/google/d/a/a/ry;

    const-string v1, "SERVICE_SNACK_BAR"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/ry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ry;->c:Lcom/google/d/a/a/ry;

    .line 460
    new-instance v0, Lcom/google/d/a/a/ry;

    const-string v1, "SERVICE_SNACK_TROLLEY"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/ry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ry;->d:Lcom/google/d/a/a/ry;

    .line 464
    new-instance v0, Lcom/google/d/a/a/ry;

    const-string v1, "SERVICE_PLAYGROUND"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/ry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ry;->e:Lcom/google/d/a/a/ry;

    .line 468
    new-instance v0, Lcom/google/d/a/a/ry;

    const-string v1, "SERVICE_BUSINESS_COMPARTMENT"

    invoke-direct {v0, v1, v6, v5}, Lcom/google/d/a/a/ry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ry;->f:Lcom/google/d/a/a/ry;

    .line 472
    new-instance v0, Lcom/google/d/a/a/ry;

    const-string v1, "SERVICE_SLEEPER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v6}, Lcom/google/d/a/a/ry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ry;->g:Lcom/google/d/a/a/ry;

    .line 443
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/d/a/a/ry;

    sget-object v1, Lcom/google/d/a/a/ry;->a:Lcom/google/d/a/a/ry;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/ry;->b:Lcom/google/d/a/a/ry;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/ry;->c:Lcom/google/d/a/a/ry;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/ry;->d:Lcom/google/d/a/a/ry;

    aput-object v1, v0, v5

    const/4 v1, 0x4

    sget-object v2, Lcom/google/d/a/a/ry;->e:Lcom/google/d/a/a/ry;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/ry;->f:Lcom/google/d/a/a/ry;

    aput-object v1, v0, v6

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/ry;->g:Lcom/google/d/a/a/ry;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/ry;->i:[Lcom/google/d/a/a/ry;

    .line 527
    new-instance v0, Lcom/google/d/a/a/rz;

    invoke-direct {v0}, Lcom/google/d/a/a/rz;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 536
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 537
    iput p3, p0, Lcom/google/d/a/a/ry;->h:I

    .line 538
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ry;
    .locals 1

    .prologue
    .line 510
    sparse-switch p0, :sswitch_data_0

    .line 518
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 511
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/ry;->a:Lcom/google/d/a/a/ry;

    goto :goto_0

    .line 512
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/ry;->b:Lcom/google/d/a/a/ry;

    goto :goto_0

    .line 513
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/ry;->c:Lcom/google/d/a/a/ry;

    goto :goto_0

    .line 514
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/ry;->d:Lcom/google/d/a/a/ry;

    goto :goto_0

    .line 515
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/ry;->e:Lcom/google/d/a/a/ry;

    goto :goto_0

    .line 516
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/ry;->f:Lcom/google/d/a/a/ry;

    goto :goto_0

    .line 517
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/ry;->g:Lcom/google/d/a/a/ry;

    goto :goto_0

    .line 510
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_4
        0x3 -> :sswitch_5
        0x5 -> :sswitch_6
        0x11 -> :sswitch_1
        0x12 -> :sswitch_2
        0x13 -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ry;
    .locals 1

    .prologue
    .line 443
    const-class v0, Lcom/google/d/a/a/ry;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ry;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ry;
    .locals 1

    .prologue
    .line 443
    sget-object v0, Lcom/google/d/a/a/ry;->i:[Lcom/google/d/a/a/ry;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ry;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 506
    iget v0, p0, Lcom/google/d/a/a/ry;->h:I

    return v0
.end method
