.class public final enum Lcom/google/d/a/a/al;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/al;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/al;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum b:Lcom/google/d/a/a/al;

.field public static final enum c:Lcom/google/d/a/a/al;

.field public static final enum d:Lcom/google/d/a/a/al;

.field public static final enum e:Lcom/google/d/a/a/al;

.field public static final enum f:Lcom/google/d/a/a/al;

.field public static final enum g:Lcom/google/d/a/a/al;

.field public static final enum h:Lcom/google/d/a/a/al;

.field public static final enum i:Lcom/google/d/a/a/al;

.field public static final enum j:Lcom/google/d/a/a/al;

.field public static final enum k:Lcom/google/d/a/a/al;

.field private static final synthetic m:[Lcom/google/d/a/a/al;


# instance fields
.field final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 442
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->a:Lcom/google/d/a/a/al;

    .line 447
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->b:Lcom/google/d/a/a/al;

    .line 451
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "INTEGER"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->c:Lcom/google/d/a/a/al;

    .line 455
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "DOUBLE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->d:Lcom/google/d/a/a/al;

    .line 459
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "BOOLEAN"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->e:Lcom/google/d/a/a/al;

    .line 463
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "PROTO_VALUE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->f:Lcom/google/d/a/a/al;

    .line 467
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "INT64"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->g:Lcom/google/d/a/a/al;

    .line 471
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "FLOAT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->h:Lcom/google/d/a/a/al;

    .line 475
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "DISPLAY_ONLY"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->i:Lcom/google/d/a/a/al;

    .line 479
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "UINT32"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->j:Lcom/google/d/a/a/al;

    .line 483
    new-instance v0, Lcom/google/d/a/a/al;

    const-string v1, "ENUM_ID"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/al;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/al;->k:Lcom/google/d/a/a/al;

    .line 437
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/d/a/a/al;

    sget-object v1, Lcom/google/d/a/a/al;->a:Lcom/google/d/a/a/al;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/al;->b:Lcom/google/d/a/a/al;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/al;->c:Lcom/google/d/a/a/al;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/al;->d:Lcom/google/d/a/a/al;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/al;->e:Lcom/google/d/a/a/al;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/al;->f:Lcom/google/d/a/a/al;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/al;->g:Lcom/google/d/a/a/al;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/al;->h:Lcom/google/d/a/a/al;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/al;->i:Lcom/google/d/a/a/al;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/al;->j:Lcom/google/d/a/a/al;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/al;->k:Lcom/google/d/a/a/al;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/al;->m:[Lcom/google/d/a/a/al;

    .line 558
    new-instance v0, Lcom/google/d/a/a/am;

    invoke-direct {v0}, Lcom/google/d/a/a/am;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 567
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 568
    iput p3, p0, Lcom/google/d/a/a/al;->l:I

    .line 569
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/al;
    .locals 1

    .prologue
    .line 537
    packed-switch p0, :pswitch_data_0

    .line 549
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 538
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/al;->a:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 539
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/al;->b:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 540
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/al;->c:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 541
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/al;->d:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 542
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/al;->e:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 543
    :pswitch_5
    sget-object v0, Lcom/google/d/a/a/al;->f:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 544
    :pswitch_6
    sget-object v0, Lcom/google/d/a/a/al;->g:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 545
    :pswitch_7
    sget-object v0, Lcom/google/d/a/a/al;->h:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 546
    :pswitch_8
    sget-object v0, Lcom/google/d/a/a/al;->i:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 547
    :pswitch_9
    sget-object v0, Lcom/google/d/a/a/al;->j:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 548
    :pswitch_a
    sget-object v0, Lcom/google/d/a/a/al;->k:Lcom/google/d/a/a/al;

    goto :goto_0

    .line 537
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/al;
    .locals 1

    .prologue
    .line 437
    const-class v0, Lcom/google/d/a/a/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/al;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/al;
    .locals 1

    .prologue
    .line 437
    sget-object v0, Lcom/google/d/a/a/al;->m:[Lcom/google/d/a/a/al;

    invoke-virtual {v0}, [Lcom/google/d/a/a/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/al;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 533
    iget v0, p0, Lcom/google/d/a/a/al;->l:I

    return v0
.end method
