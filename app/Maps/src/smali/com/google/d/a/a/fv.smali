.class public final enum Lcom/google/d/a/a/fv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/fv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/fv;

.field public static final enum b:Lcom/google/d/a/a/fv;

.field public static final enum c:Lcom/google/d/a/a/fv;

.field public static final enum d:Lcom/google/d/a/a/fv;

.field public static final enum e:Lcom/google/d/a/a/fv;

.field public static final enum f:Lcom/google/d/a/a/fv;

.field public static final enum g:Lcom/google/d/a/a/fv;

.field public static final enum h:Lcom/google/d/a/a/fv;

.field public static final enum i:Lcom/google/d/a/a/fv;

.field public static final enum j:Lcom/google/d/a/a/fv;

.field public static final enum k:Lcom/google/d/a/a/fv;

.field public static final enum l:Lcom/google/d/a/a/fv;

.field public static final enum m:Lcom/google/d/a/a/fv;

.field public static final enum n:Lcom/google/d/a/a/fv;

.field public static final enum o:Lcom/google/d/a/a/fv;

.field public static final enum p:Lcom/google/d/a/a/fv;

.field private static final synthetic r:[Lcom/google/d/a/a/fv;


# instance fields
.field final q:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 274
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->a:Lcom/google/d/a/a/fv;

    .line 278
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_NORMAL"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->b:Lcom/google/d/a/a/fv;

    .line 282
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_PASSING"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->c:Lcom/google/d/a/a/fv;

    .line 286
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_LEFT_TURN"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->d:Lcom/google/d/a/a/fv;

    .line 290
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_LEFT_TURN_OFF"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->e:Lcom/google/d/a/a/fv;

    .line 294
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_LEFT_TURN_ON_OFF"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->f:Lcom/google/d/a/a/fv;

    .line 298
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_RIGHT_TURN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v8}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->g:Lcom/google/d/a/a/fv;

    .line 302
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_RIGHT_TURN_OFF"

    const/4 v2, 0x7

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->h:Lcom/google/d/a/a/fv;

    .line 306
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_RIGHT_TURN_ON_OFF"

    const/16 v2, 0x8

    const/16 v3, 0x52

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->i:Lcom/google/d/a/a/fv;

    .line 310
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_BICYCLE"

    const/16 v2, 0x9

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->j:Lcom/google/d/a/a/fv;

    .line 314
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_PARKING"

    const/16 v2, 0xa

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->k:Lcom/google/d/a/a/fv;

    .line 318
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_PARKING_IMPLIED"

    const/16 v2, 0xb

    const/16 v3, 0x71

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->l:Lcom/google/d/a/a/fv;

    .line 322
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_PARKING_MARKED"

    const/16 v2, 0xc

    const/16 v3, 0x72

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->m:Lcom/google/d/a/a/fv;

    .line 326
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_EXIT_ENTRANCE"

    const/16 v2, 0xd

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->n:Lcom/google/d/a/a/fv;

    .line 330
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_EXIT_LANE"

    const/16 v2, 0xe

    const/16 v3, 0x81

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->o:Lcom/google/d/a/a/fv;

    .line 334
    new-instance v0, Lcom/google/d/a/a/fv;

    const-string v1, "TYPE_ENTRANCE_LANE"

    const/16 v2, 0xf

    const/16 v3, 0x82

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/fv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/fv;->p:Lcom/google/d/a/a/fv;

    .line 269
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/d/a/a/fv;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/d/a/a/fv;->a:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/fv;->b:Lcom/google/d/a/a/fv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/fv;->c:Lcom/google/d/a/a/fv;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/fv;->d:Lcom/google/d/a/a/fv;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/fv;->e:Lcom/google/d/a/a/fv;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/fv;->f:Lcom/google/d/a/a/fv;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/fv;->g:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/fv;->h:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/fv;->i:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/fv;->j:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/fv;->k:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/fv;->l:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/fv;->m:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/d/a/a/fv;->n:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/d/a/a/fv;->o:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/d/a/a/fv;->p:Lcom/google/d/a/a/fv;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/fv;->r:[Lcom/google/d/a/a/fv;

    .line 434
    new-instance v0, Lcom/google/d/a/a/fw;

    invoke-direct {v0}, Lcom/google/d/a/a/fw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 443
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 444
    iput p3, p0, Lcom/google/d/a/a/fv;->q:I

    .line 445
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/fv;
    .locals 1

    .prologue
    .line 408
    sparse-switch p0, :sswitch_data_0

    .line 425
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 409
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/fv;->a:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 410
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/fv;->b:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 411
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/fv;->c:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 412
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/fv;->d:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 413
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/fv;->e:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 414
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/fv;->f:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 415
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/fv;->g:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 416
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/fv;->h:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 417
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/fv;->i:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 418
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/fv;->j:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 419
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/fv;->k:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 420
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/fv;->l:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 421
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/fv;->m:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 422
    :sswitch_d
    sget-object v0, Lcom/google/d/a/a/fv;->n:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 423
    :sswitch_e
    sget-object v0, Lcom/google/d/a/a/fv;->o:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 424
    :sswitch_f
    sget-object v0, Lcom/google/d/a/a/fv;->p:Lcom/google/d/a/a/fv;

    goto :goto_0

    .line 408
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_6
        0x6 -> :sswitch_9
        0x7 -> :sswitch_a
        0x8 -> :sswitch_d
        0x41 -> :sswitch_4
        0x42 -> :sswitch_5
        0x51 -> :sswitch_7
        0x52 -> :sswitch_8
        0x71 -> :sswitch_b
        0x72 -> :sswitch_c
        0x81 -> :sswitch_e
        0x82 -> :sswitch_f
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/fv;
    .locals 1

    .prologue
    .line 269
    const-class v0, Lcom/google/d/a/a/fv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fv;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/fv;
    .locals 1

    .prologue
    .line 269
    sget-object v0, Lcom/google/d/a/a/fv;->r:[Lcom/google/d/a/a/fv;

    invoke-virtual {v0}, [Lcom/google/d/a/a/fv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/fv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/google/d/a/a/fv;->q:I

    return v0
.end method
