.class public final Lcom/google/d/a/a/ee;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/eh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ec;",
        "Lcom/google/d/a/a/ee;",
        ">;",
        "Lcom/google/d/a/a/eh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 365
    sget-object v0, Lcom/google/d/a/a/ec;->d:Lcom/google/d/a/a/ec;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 428
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/d/a/a/ee;->b:I

    .line 465
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    .line 366
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ec;)Lcom/google/d/a/a/ee;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 398
    invoke-static {}, Lcom/google/d/a/a/ec;->d()Lcom/google/d/a/a/ec;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 413
    :goto_0
    return-object p0

    .line 399
    :cond_0
    iget v1, p1, Lcom/google/d/a/a/ec;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 400
    iget v0, p1, Lcom/google/d/a/a/ec;->b:I

    invoke-static {v0}, Lcom/google/d/a/a/ef;->a(I)Lcom/google/d/a/a/ef;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/d/a/a/ef;->a:Lcom/google/d/a/a/ef;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 399
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 400
    :cond_3
    iget v1, p0, Lcom/google/d/a/a/ee;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/d/a/a/ee;->a:I

    iget v0, v0, Lcom/google/d/a/a/ef;->b:I

    iput v0, p0, Lcom/google/d/a/a/ee;->b:I

    .line 402
    :cond_4
    iget-object v0, p1, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 403
    iget-object v0, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 404
    iget-object v0, p1, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    .line 405
    iget v0, p0, Lcom/google/d/a/a/ee;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/d/a/a/ee;->a:I

    .line 412
    :cond_5
    :goto_2
    iget-object v0, p1, Lcom/google/d/a/a/ec;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 407
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/ee;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/ee;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/ee;->a:I

    .line 408
    :cond_7
    iget-object v0, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 357
    new-instance v2, Lcom/google/d/a/a/ec;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ec;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ee;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/ee;->b:I

    iput v1, v2, Lcom/google/d/a/a/ec;->b:I

    iget v1, p0, Lcom/google/d/a/a/ee;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/ee;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/d/a/a/ee;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/d/a/a/ec;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 357
    check-cast p1, Lcom/google/d/a/a/ec;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ee;->a(Lcom/google/d/a/a/ec;)Lcom/google/d/a/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 417
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/google/d/a/a/ee;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/fz;->d()Lcom/google/d/a/a/fz;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fz;

    invoke-virtual {v0}, Lcom/google/d/a/a/fz;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 423
    :goto_1
    return v2

    .line 417
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 423
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
