.class public final enum Lcom/google/d/a/a/qs;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/qs;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/qs;

.field public static final enum b:Lcom/google/d/a/a/qs;

.field public static final enum c:Lcom/google/d/a/a/qs;

.field public static final enum d:Lcom/google/d/a/a/qs;

.field public static final enum e:Lcom/google/d/a/a/qs;

.field public static final enum f:Lcom/google/d/a/a/qs;

.field private static final synthetic h:[Lcom/google/d/a/a/qs;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 193
    new-instance v0, Lcom/google/d/a/a/qs;

    const-string v1, "PROPERTY_ANY"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/qs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/qs;->a:Lcom/google/d/a/a/qs;

    .line 197
    new-instance v0, Lcom/google/d/a/a/qs;

    const-string v1, "PROPERTY_EXPRESS_CLASS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/qs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/qs;->b:Lcom/google/d/a/a/qs;

    .line 201
    new-instance v0, Lcom/google/d/a/a/qs;

    const-string v1, "PROPERTY_JOURNEY_DETAILS"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/d/a/a/qs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/qs;->c:Lcom/google/d/a/a/qs;

    .line 205
    new-instance v0, Lcom/google/d/a/a/qs;

    const-string v1, "PROPERTY_DESTINATION"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/qs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/qs;->d:Lcom/google/d/a/a/qs;

    .line 209
    new-instance v0, Lcom/google/d/a/a/qs;

    const-string v1, "PROPERTY_DETAILED_DESTINATION"

    const/16 v2, 0x211

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/qs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/qs;->e:Lcom/google/d/a/a/qs;

    .line 213
    new-instance v0, Lcom/google/d/a/a/qs;

    const-string v1, "PROPERTY_DIRECTION_GROUP_AMENDMENT"

    const/4 v2, 0x5

    const/16 v3, 0x212

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/qs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/qs;->f:Lcom/google/d/a/a/qs;

    .line 188
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/d/a/a/qs;

    sget-object v1, Lcom/google/d/a/a/qs;->a:Lcom/google/d/a/a/qs;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/qs;->b:Lcom/google/d/a/a/qs;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/qs;->c:Lcom/google/d/a/a/qs;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/qs;->d:Lcom/google/d/a/a/qs;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/qs;->e:Lcom/google/d/a/a/qs;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/qs;->f:Lcom/google/d/a/a/qs;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/qs;->h:[Lcom/google/d/a/a/qs;

    .line 263
    new-instance v0, Lcom/google/d/a/a/qt;

    invoke-direct {v0}, Lcom/google/d/a/a/qt;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 272
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 273
    iput p3, p0, Lcom/google/d/a/a/qs;->g:I

    .line 274
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/qs;
    .locals 1

    .prologue
    .line 247
    sparse-switch p0, :sswitch_data_0

    .line 254
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 248
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/qs;->a:Lcom/google/d/a/a/qs;

    goto :goto_0

    .line 249
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/qs;->b:Lcom/google/d/a/a/qs;

    goto :goto_0

    .line 250
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/qs;->c:Lcom/google/d/a/a/qs;

    goto :goto_0

    .line 251
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/qs;->d:Lcom/google/d/a/a/qs;

    goto :goto_0

    .line 252
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/qs;->e:Lcom/google/d/a/a/qs;

    goto :goto_0

    .line 253
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/qs;->f:Lcom/google/d/a/a/qs;

    goto :goto_0

    .line 247
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x21 -> :sswitch_3
        0x211 -> :sswitch_4
        0x212 -> :sswitch_5
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/qs;
    .locals 1

    .prologue
    .line 188
    const-class v0, Lcom/google/d/a/a/qs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/qs;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/qs;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/google/d/a/a/qs;->h:[Lcom/google/d/a/a/qs;

    invoke-virtual {v0}, [Lcom/google/d/a/a/qs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/qs;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/google/d/a/a/qs;->g:I

    return v0
.end method
