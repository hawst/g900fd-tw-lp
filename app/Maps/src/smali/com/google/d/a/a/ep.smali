.class public final Lcom/google/d/a/a/ep;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/eq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/en;",
        "Lcom/google/d/a/a/ep;",
        ">;",
        "Lcom/google/d/a/a/eq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 887
    sget-object v0, Lcom/google/d/a/a/en;->c:Lcom/google/d/a/a/en;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 936
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ep;->b:Lcom/google/n/ao;

    .line 888
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/en;)Lcom/google/d/a/a/ep;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 915
    invoke-static {}, Lcom/google/d/a/a/en;->d()Lcom/google/d/a/a/en;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 921
    :goto_0
    return-object p0

    .line 916
    :cond_0
    iget v1, p1, Lcom/google/d/a/a/en;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 917
    iget-object v0, p0, Lcom/google/d/a/a/ep;->b:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/en;->b:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 918
    iget v0, p0, Lcom/google/d/a/a/ep;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/ep;->a:I

    .line 920
    :cond_1
    iget-object v0, p1, Lcom/google/d/a/a/en;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 916
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 879
    new-instance v2, Lcom/google/d/a/a/en;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/en;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ep;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v3, v2, Lcom/google/d/a/a/en;->b:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/ep;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/ep;->b:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/en;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 879
    check-cast p1, Lcom/google/d/a/a/en;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ep;->a(Lcom/google/d/a/a/en;)Lcom/google/d/a/a/ep;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 925
    iget v0, p0, Lcom/google/d/a/a/ep;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 926
    iget-object v0, p0, Lcom/google/d/a/a/ep;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ej;->d()Lcom/google/d/a/a/ej;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ej;

    invoke-virtual {v0}, Lcom/google/d/a/a/ej;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 931
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 925
    goto :goto_0

    :cond_1
    move v0, v2

    .line 931
    goto :goto_1
.end method
