.class public final Lcom/google/d/a/a/rn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/rq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/rn;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/d/a/a/rn;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:F

.field e:Z

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:I

.field k:Z

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 213
    new-instance v0, Lcom/google/d/a/a/ro;

    invoke-direct {v0}, Lcom/google/d/a/a/ro;-><init>()V

    sput-object v0, Lcom/google/d/a/a/rn;->PARSER:Lcom/google/n/ax;

    .line 524
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/rn;->o:Lcom/google/n/aw;

    .line 1269
    new-instance v0, Lcom/google/d/a/a/rn;

    invoke-direct {v0}, Lcom/google/d/a/a/rn;-><init>()V

    sput-object v0, Lcom/google/d/a/a/rn;->l:Lcom/google/d/a/a/rn;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 230
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    .line 246
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    .line 292
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    .line 308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    .line 324
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    .line 340
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    .line 385
    iput-byte v4, p0, Lcom/google/d/a/a/rn;->m:B

    .line 471
    iput v4, p0, Lcom/google/d/a/a/rn;->n:I

    .line 117
    iget-object v0, p0, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 118
    iget-object v0, p0, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 119
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/rn;->d:F

    .line 120
    iput-boolean v3, p0, Lcom/google/d/a/a/rn;->e:Z

    .line 121
    iget-object v0, p0, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 122
    iget-object v0, p0, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 123
    iget-object v0, p0, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 124
    iget-object v0, p0, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 125
    iput v3, p0, Lcom/google/d/a/a/rn;->j:I

    .line 126
    iput-boolean v3, p0, Lcom/google/d/a/a/rn;->k:Z

    .line 127
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Lcom/google/d/a/a/rn;-><init>()V

    .line 134
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 139
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 140
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 141
    sparse-switch v0, :sswitch_data_0

    .line 146
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 148
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 144
    goto :goto_0

    .line 153
    :sswitch_1
    iget-object v0, p0, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 154
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/rn;->au:Lcom/google/n/bn;

    throw v0

    .line 158
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 159
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 206
    :catch_1
    move-exception v0

    .line 207
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 208
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 163
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I

    .line 164
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/rn;->d:F

    goto :goto_0

    .line 168
    :sswitch_4
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I

    .line 169
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/rn;->e:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 173
    :sswitch_5
    iget-object v0, p0, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 174
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I

    goto/16 :goto_0

    .line 178
    :sswitch_6
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I

    .line 179
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/rn;->j:I

    goto/16 :goto_0

    .line 183
    :sswitch_7
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I

    .line 184
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/d/a/a/rn;->k:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 188
    :sswitch_8
    iget-object v0, p0, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 189
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I

    goto/16 :goto_0

    .line 193
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 194
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I

    goto/16 :goto_0

    .line 198
    :sswitch_a
    iget-object v0, p0, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 199
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/rn;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 210
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rn;->au:Lcom/google/n/bn;

    .line 211
    return-void

    .line 141
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 114
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 230
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    .line 246
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    .line 292
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    .line 308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    .line 324
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    .line 340
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    .line 385
    iput-byte v1, p0, Lcom/google/d/a/a/rn;->m:B

    .line 471
    iput v1, p0, Lcom/google/d/a/a/rn;->n:I

    .line 115
    return-void
.end method

.method public static d()Lcom/google/d/a/a/rn;
    .locals 1

    .prologue
    .line 1272
    sget-object v0, Lcom/google/d/a/a/rn;->l:Lcom/google/d/a/a/rn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/rp;
    .locals 1

    .prologue
    .line 586
    new-instance v0, Lcom/google/d/a/a/rp;

    invoke-direct {v0}, Lcom/google/d/a/a/rp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/rn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    sget-object v0, Lcom/google/d/a/a/rn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 437
    invoke-virtual {p0}, Lcom/google/d/a/a/rn;->c()I

    .line 438
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 439
    iget-object v0, p0, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 441
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 442
    iget-object v0, p0, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 444
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 445
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/d/a/a/rn;->d:F

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v3}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 447
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 448
    iget-boolean v0, p0, Lcom/google/d/a/a/rn;->e:Z

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_a

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 450
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 451
    iget-object v0, p0, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 453
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_5

    .line 454
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/d/a/a/rn;->j:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_b

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 456
    :cond_5
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_6

    .line 457
    const/4 v0, 0x7

    iget-boolean v3, p0, Lcom/google/d/a/a/rn;->k:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_c

    :goto_2
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 459
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 460
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 462
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 463
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 465
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 466
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 468
    :cond_9
    iget-object v0, p0, Lcom/google/d/a/a/rn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 469
    return-void

    :cond_a
    move v0, v2

    .line 448
    goto/16 :goto_0

    .line 454
    :cond_b
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    :cond_c
    move v1, v2

    .line 457
    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 387
    iget-byte v0, p0, Lcom/google/d/a/a/rn;->m:B

    .line 388
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 432
    :goto_0
    return v0

    .line 389
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 391
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 392
    iput-byte v2, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v2

    .line 393
    goto :goto_0

    :cond_2
    move v0, v2

    .line 391
    goto :goto_1

    .line 395
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    .line 396
    iput-byte v2, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v2

    .line 397
    goto :goto_0

    :cond_4
    move v0, v2

    .line 395
    goto :goto_2

    .line 399
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 400
    iput-byte v2, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v2

    .line 401
    goto :goto_0

    .line 403
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 404
    iput-byte v2, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v2

    .line 405
    goto :goto_0

    .line 407
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_3
    if-eqz v0, :cond_9

    .line 408
    iget-object v0, p0, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 409
    iput-byte v2, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v2

    .line 410
    goto :goto_0

    :cond_8
    move v0, v2

    .line 407
    goto :goto_3

    .line 413
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_4
    if-eqz v0, :cond_b

    .line 414
    iget-object v0, p0, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 415
    iput-byte v2, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v2

    .line 416
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 413
    goto :goto_4

    .line 419
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_5
    if-eqz v0, :cond_d

    .line 420
    iget-object v0, p0, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 421
    iput-byte v2, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v2

    .line 422
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 419
    goto :goto_5

    .line 425
    :cond_d
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_6
    if-eqz v0, :cond_f

    .line 426
    iget-object v0, p0, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_f

    .line 427
    iput-byte v2, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v2

    .line 428
    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 425
    goto :goto_6

    .line 431
    :cond_f
    iput-byte v1, p0, Lcom/google/d/a/a/rn;->m:B

    move v0, v1

    .line 432
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 473
    iget v0, p0, Lcom/google/d/a/a/rn;->n:I

    .line 474
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 519
    :goto_0
    return v0

    .line 477
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 478
    iget-object v0, p0, Lcom/google/d/a/a/rn;->b:Lcom/google/n/ao;

    .line 479
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 481
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 482
    iget-object v2, p0, Lcom/google/d/a/a/rn;->c:Lcom/google/n/ao;

    .line 483
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 485
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 486
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/d/a/a/rn;->d:F

    .line 487
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 489
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 490
    iget-boolean v2, p0, Lcom/google/d/a/a/rn;->e:Z

    .line 491
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 493
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 494
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/d/a/a/rn;->f:Lcom/google/n/ao;

    .line 495
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 497
    :cond_4
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_5

    .line 498
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/d/a/a/rn;->j:I

    .line 499
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 501
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_6

    .line 502
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/d/a/a/rn;->k:Z

    .line 503
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 505
    :cond_6
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_7

    .line 506
    iget-object v2, p0, Lcom/google/d/a/a/rn;->g:Lcom/google/n/ao;

    .line 507
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 509
    :cond_7
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_8

    .line 510
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/d/a/a/rn;->h:Lcom/google/n/ao;

    .line 511
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 513
    :cond_8
    iget v2, p0, Lcom/google/d/a/a/rn;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_9

    .line 514
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/d/a/a/rn;->i:Lcom/google/n/ao;

    .line 515
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 517
    :cond_9
    iget-object v1, p0, Lcom/google/d/a/a/rn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 518
    iput v0, p0, Lcom/google/d/a/a/rn;->n:I

    goto/16 :goto_0

    .line 499
    :cond_a
    const/16 v2, 0xa

    goto :goto_2

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/google/d/a/a/rn;->newBuilder()Lcom/google/d/a/a/rp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/rp;->a(Lcom/google/d/a/a/rn;)Lcom/google/d/a/a/rp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/google/d/a/a/rn;->newBuilder()Lcom/google/d/a/a/rp;

    move-result-object v0

    return-object v0
.end method
