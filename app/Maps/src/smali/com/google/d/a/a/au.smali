.class public final Lcom/google/d/a/a/au;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/az;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/au;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/a/a/au;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/google/d/a/a/av;

    invoke-direct {v0}, Lcom/google/d/a/a/av;-><init>()V

    sput-object v0, Lcom/google/d/a/a/au;->PARSER:Lcom/google/n/ax;

    .line 462
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/au;->j:Lcom/google/n/aw;

    .line 975
    new-instance v0, Lcom/google/d/a/a/au;

    invoke-direct {v0}, Lcom/google/d/a/a/au;-><init>()V

    sput-object v0, Lcom/google/d/a/a/au;->g:Lcom/google/d/a/a/au;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    .line 324
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    .line 382
    iput-byte v3, p0, Lcom/google/d/a/a/au;->h:B

    .line 429
    iput v3, p0, Lcom/google/d/a/a/au;->i:I

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/au;->b:I

    .line 78
    iput v2, p0, Lcom/google/d/a/a/au;->c:I

    .line 79
    iget-object v0, p0, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 80
    iget-object v0, p0, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 81
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    .line 82
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x10

    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Lcom/google/d/a/a/au;-><init>()V

    .line 91
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 94
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 95
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 96
    sparse-switch v4, :sswitch_data_0

    .line 101
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 103
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 99
    goto :goto_0

    .line 108
    :sswitch_1
    iget v4, p0, Lcom/google/d/a/a/au;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/au;->a:I

    .line 109
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/au;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 145
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 146
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_1

    .line 152
    iget-object v1, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    .line 154
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/au;->au:Lcom/google/n/bn;

    throw v0

    .line 113
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 114
    invoke-static {v4}, Lcom/google/d/a/a/aw;->a(I)Lcom/google/d/a/a/aw;

    move-result-object v5

    .line 115
    if-nez v5, :cond_2

    .line 116
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 147
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 148
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 149
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 118
    :cond_2
    :try_start_4
    iget v5, p0, Lcom/google/d/a/a/au;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/d/a/a/au;->a:I

    .line 119
    iput v4, p0, Lcom/google/d/a/a/au;->c:I

    goto :goto_0

    .line 151
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 124
    :sswitch_3
    iget-object v4, p0, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 125
    iget v4, p0, Lcom/google/d/a/a/au;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/au;->a:I

    goto :goto_0

    .line 129
    :sswitch_4
    iget-object v4, p0, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 130
    iget v4, p0, Lcom/google/d/a/a/au;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/a/a/au;->a:I

    goto/16 :goto_0

    .line 134
    :sswitch_5
    and-int/lit8 v4, v0, 0x10

    if-eq v4, v7, :cond_3

    .line 135
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    .line 137
    or-int/lit8 v0, v0, 0x10

    .line 139
    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 139
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 151
    :cond_4
    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_5

    .line 152
    iget-object v0, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    .line 154
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/au;->au:Lcom/google/n/bn;

    .line 155
    return-void

    .line 96
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 74
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    .line 324
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    .line 382
    iput-byte v1, p0, Lcom/google/d/a/a/au;->h:B

    .line 429
    iput v1, p0, Lcom/google/d/a/a/au;->i:I

    .line 75
    return-void
.end method

.method public static d()Lcom/google/d/a/a/au;
    .locals 1

    .prologue
    .line 978
    sget-object v0, Lcom/google/d/a/a/au;->g:Lcom/google/d/a/a/au;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ay;
    .locals 1

    .prologue
    .line 524
    new-instance v0, Lcom/google/d/a/a/ay;

    invoke-direct {v0}, Lcom/google/d/a/a/ay;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/au;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    sget-object v0, Lcom/google/d/a/a/au;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x2

    .line 410
    invoke-virtual {p0}, Lcom/google/d/a/a/au;->c()I

    .line 411
    iget v1, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 412
    iget v1, p0, Lcom/google/d/a/a/au;->b:I

    invoke-static {v2, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 414
    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 415
    iget v1, p0, Lcom/google/d/a/a/au;->c:I

    invoke-static {v4, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 417
    :cond_1
    :goto_1
    iget v1, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 418
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 420
    :cond_2
    iget v1, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 421
    iget-object v1, p0, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_3
    move v1, v0

    .line 423
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 424
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 423
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 412
    :cond_4
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 415
    :cond_5
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 426
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/au;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 427
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 384
    iget-byte v0, p0, Lcom/google/d/a/a/au;->h:B

    .line 385
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 405
    :goto_0
    return v0

    .line 386
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 388
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 389
    iput-byte v2, p0, Lcom/google/d/a/a/au;->h:B

    move v0, v2

    .line 390
    goto :goto_0

    :cond_2
    move v0, v2

    .line 388
    goto :goto_1

    .line 392
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 393
    iget-object v0, p0, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 394
    iput-byte v2, p0, Lcom/google/d/a/a/au;->h:B

    move v0, v2

    .line 395
    goto :goto_0

    :cond_4
    move v0, v2

    .line 392
    goto :goto_2

    .line 398
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 399
    iget-object v0, p0, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 400
    iput-byte v2, p0, Lcom/google/d/a/a/au;->h:B

    move v0, v2

    .line 401
    goto :goto_0

    :cond_6
    move v0, v2

    .line 398
    goto :goto_3

    .line 404
    :cond_7
    iput-byte v1, p0, Lcom/google/d/a/a/au;->h:B

    move v0, v1

    .line 405
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v1, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 431
    iget v0, p0, Lcom/google/d/a/a/au;->i:I

    .line 432
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 457
    :goto_0
    return v0

    .line 435
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 436
    iget v0, p0, Lcom/google/d/a/a/au;->b:I

    .line 437
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 439
    :goto_2
    iget v3, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 440
    iget v3, p0, Lcom/google/d/a/a/au;->c:I

    .line 441
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 443
    :cond_2
    iget v1, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_3

    .line 444
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    .line 445
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 447
    :cond_3
    iget v1, p0, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    .line 448
    iget-object v1, p0, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    .line 449
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    :cond_4
    move v1, v2

    move v3, v0

    .line 451
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 452
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    .line 453
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 451
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    move v0, v1

    .line 437
    goto :goto_1

    .line 455
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/au;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 456
    iput v0, p0, Lcom/google/d/a/a/au;->i:I

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/a/a/au;->newBuilder()Lcom/google/d/a/a/ay;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ay;->a(Lcom/google/d/a/a/au;)Lcom/google/d/a/a/ay;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/a/a/au;->newBuilder()Lcom/google/d/a/a/ay;

    move-result-object v0

    return-object v0
.end method
