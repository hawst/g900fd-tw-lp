.class public final Lcom/google/d/a/a/x;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/aa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/x;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/d/a/a/x;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field d:Z

.field e:Z

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Lcom/google/n/ao;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 226
    new-instance v0, Lcom/google/d/a/a/y;

    invoke-direct {v0}, Lcom/google/d/a/a/y;-><init>()V

    sput-object v0, Lcom/google/d/a/a/x;->PARSER:Lcom/google/n/ax;

    .line 507
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/x;->l:Lcom/google/n/aw;

    .line 1121
    new-instance v0, Lcom/google/d/a/a/x;

    invoke-direct {v0}, Lcom/google/d/a/a/x;-><init>()V

    sput-object v0, Lcom/google/d/a/a/x;->i:Lcom/google/d/a/a/x;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 401
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    .line 416
    iput-byte v2, p0, Lcom/google/d/a/a/x;->j:B

    .line 459
    iput v2, p0, Lcom/google/d/a/a/x;->k:I

    .line 108
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    .line 109
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/d/a/a/x;->d:Z

    .line 111
    iput-boolean v3, p0, Lcom/google/d/a/a/x;->e:Z

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/x;->f:Ljava/lang/Object;

    .line 113
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/x;->g:Ljava/lang/Object;

    .line 114
    iget-object v0, p0, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 115
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/d/a/a/x;-><init>()V

    .line 122
    const/4 v1, 0x0

    .line 124
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 126
    const/4 v0, 0x0

    move v2, v0

    .line 127
    :cond_0
    :goto_0
    if-nez v2, :cond_f

    .line 128
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 129
    sparse-switch v0, :sswitch_data_0

    .line 134
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 131
    :sswitch_0
    const/4 v0, 0x1

    move v2, v0

    .line 132
    goto :goto_0

    .line 141
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    const/4 v4, 0x1

    if-eq v0, v4, :cond_1

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    .line 143
    or-int/lit8 v1, v1, 0x1

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x1

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    .line 218
    iget-object v2, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    .line 220
    :cond_2
    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 221
    iget-object v1, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    .line 223
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/x;->au:Lcom/google/n/bn;

    throw v0

    .line 149
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 150
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 151
    and-int/lit8 v0, v1, 0x1

    const/4 v5, 0x1

    if-eq v0, v5, :cond_4

    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_5

    const/4 v0, -0x1

    :goto_1
    if-lez v0, :cond_4

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    .line 153
    or-int/lit8 v1, v1, 0x1

    .line 155
    :cond_4
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_6

    const/4 v0, -0x1

    :goto_3
    if-lez v0, :cond_7

    .line 156
    iget-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 213
    :catch_1
    move-exception v0

    .line 214
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 215
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 151
    :cond_5
    :try_start_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_1

    .line 155
    :cond_6
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_3

    .line 158
    :cond_7
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 162
    :sswitch_3
    and-int/lit8 v0, v1, 0x2

    const/4 v4, 0x2

    if-eq v0, v4, :cond_8

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    .line 164
    or-int/lit8 v1, v1, 0x2

    .line 166
    :cond_8
    iget-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 170
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 171
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 172
    and-int/lit8 v0, v1, 0x2

    const/4 v5, 0x2

    if-eq v0, v5, :cond_9

    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_a

    const/4 v0, -0x1

    :goto_4
    if-lez v0, :cond_9

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    .line 174
    or-int/lit8 v1, v1, 0x2

    .line 176
    :cond_9
    :goto_5
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_b

    const/4 v0, -0x1

    :goto_6
    if-lez v0, :cond_c

    .line 177
    iget-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 172
    :cond_a
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_4

    .line 176
    :cond_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_6

    .line 179
    :cond_c
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 183
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/x;->a:I

    .line 184
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lcom/google/d/a/a/x;->e:Z

    goto/16 :goto_0

    :cond_d
    const/4 v0, 0x0

    goto :goto_7

    .line 188
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 189
    iget v4, p0, Lcom/google/d/a/a/x;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/x;->a:I

    .line 190
    iput-object v0, p0, Lcom/google/d/a/a/x;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 194
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 195
    iget v4, p0, Lcom/google/d/a/a/x;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/a/a/x;->a:I

    .line 196
    iput-object v0, p0, Lcom/google/d/a/a/x;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 200
    :sswitch_8
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/x;->a:I

    .line 201
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, Lcom/google/d/a/a/x;->d:Z

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto :goto_8

    .line 205
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 206
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/x;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 217
    :cond_f
    and-int/lit8 v0, v1, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_10

    .line 218
    iget-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    .line 220
    :cond_10
    and-int/lit8 v0, v1, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_11

    .line 221
    iget-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    .line 223
    :cond_11
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->au:Lcom/google/n/bn;

    .line 224
    return-void

    .line 129
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_4
        0x15 -> :sswitch_3
        0x18 -> :sswitch_5
        0x22 -> :sswitch_6
        0x2a -> :sswitch_7
        0x30 -> :sswitch_8
        0x7a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 105
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 401
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    .line 416
    iput-byte v1, p0, Lcom/google/d/a/a/x;->j:B

    .line 459
    iput v1, p0, Lcom/google/d/a/a/x;->k:I

    .line 106
    return-void
.end method

.method public static d()Lcom/google/d/a/a/x;
    .locals 1

    .prologue
    .line 1124
    sget-object v0, Lcom/google/d/a/a/x;->i:Lcom/google/d/a/a/x;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/z;
    .locals 1

    .prologue
    .line 569
    new-instance v0, Lcom/google/d/a/a/z;

    invoke-direct {v0}, Lcom/google/d/a/a/z;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    sget-object v0, Lcom/google/d/a/a/x;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 434
    invoke-virtual {p0}, Lcom/google/d/a/a/x;->c()I

    move v1, v2

    .line 435
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 436
    iget-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 435
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 436
    :cond_0
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_1
    move v1, v2

    .line 438
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 439
    iget-object v0, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v6, v8}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 438
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 441
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 442
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/d/a/a/x;->e:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_8

    move v0, v3

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 444
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 445
    iget-object v0, p0, Lcom/google/d/a/a/x;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 447
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 448
    iget-object v0, p0, Lcom/google/d/a/a/x;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 450
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 451
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/d/a/a/x;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_b

    :goto_6
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 453
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 454
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 456
    :cond_7
    iget-object v0, p0, Lcom/google/d/a/a/x;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 457
    return-void

    :cond_8
    move v0, v2

    .line 442
    goto/16 :goto_3

    .line 445
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 448
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_b
    move v3, v2

    .line 451
    goto :goto_6
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 418
    iget-byte v0, p0, Lcom/google/d/a/a/x;->j:B

    .line 419
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 429
    :goto_0
    return v0

    .line 420
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 422
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 423
    iget-object v0, p0, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 424
    iput-byte v2, p0, Lcom/google/d/a/a/x;->j:B

    move v0, v2

    .line 425
    goto :goto_0

    :cond_2
    move v0, v2

    .line 422
    goto :goto_1

    .line 428
    :cond_3
    iput-byte v1, p0, Lcom/google/d/a/a/x;->j:B

    move v0, v1

    .line 429
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 461
    iget v0, p0, Lcom/google/d/a/a/x;->k:I

    .line 462
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 502
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 467
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 468
    iget-object v0, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    .line 469
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    .line 467
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 469
    :cond_1
    const/16 v0, 0xa

    goto :goto_2

    .line 471
    :cond_2
    add-int/lit8 v0, v3, 0x0

    .line 472
    iget-object v1, p0, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 475
    iget-object v1, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    .line 477
    add-int/2addr v0, v1

    .line 478
    iget-object v1, p0, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 480
    iget v1, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_9

    .line 481
    const/4 v1, 0x3

    iget-boolean v3, p0, Lcom/google/d/a/a/x;->e:Z

    .line 482
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v0

    .line 484
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 486
    iget-object v0, p0, Lcom/google/d/a/a/x;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 488
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    .line 489
    const/4 v3, 0x5

    .line 490
    iget-object v0, p0, Lcom/google/d/a/a/x;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/x;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 492
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_5

    .line 493
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/d/a/a/x;->d:Z

    .line 494
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 496
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_6

    .line 497
    const/16 v0, 0xf

    iget-object v3, p0, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    .line 498
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 500
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/x;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 501
    iput v0, p0, Lcom/google/d/a/a/x;->k:I

    goto/16 :goto_0

    .line 486
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 490
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_9
    move v1, v0

    goto/16 :goto_3
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/d/a/a/x;->newBuilder()Lcom/google/d/a/a/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/z;->a(Lcom/google/d/a/a/x;)Lcom/google/d/a/a/z;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/d/a/a/x;->newBuilder()Lcom/google/d/a/a/z;

    move-result-object v0

    return-object v0
.end method
