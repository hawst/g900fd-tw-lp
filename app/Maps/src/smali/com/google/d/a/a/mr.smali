.class public final enum Lcom/google/d/a/a/mr;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mr;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/mr;

.field public static final enum b:Lcom/google/d/a/a/mr;

.field public static final enum c:Lcom/google/d/a/a/mr;

.field public static final enum d:Lcom/google/d/a/a/mr;

.field public static final enum e:Lcom/google/d/a/a/mr;

.field private static final synthetic g:[Lcom/google/d/a/a/mr;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2147
    new-instance v0, Lcom/google/d/a/a/mr;

    const-string v1, "PEDESTRIAN_FACILITY_UNKNOWN"

    invoke-direct {v0, v1, v6, v3}, Lcom/google/d/a/a/mr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mr;->a:Lcom/google/d/a/a/mr;

    .line 2151
    new-instance v0, Lcom/google/d/a/a/mr;

    const-string v1, "PEDESTRIAN_FACILITY_NONE"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/d/a/a/mr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mr;->b:Lcom/google/d/a/a/mr;

    .line 2155
    new-instance v0, Lcom/google/d/a/a/mr;

    const-string v1, "PEDESTRIAN_FACILITY_PRESENT"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/d/a/a/mr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mr;->c:Lcom/google/d/a/a/mr;

    .line 2159
    new-instance v0, Lcom/google/d/a/a/mr;

    const-string v1, "PEDESTRIAN_FACILITY_SIDEWALK"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/mr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mr;->d:Lcom/google/d/a/a/mr;

    .line 2163
    new-instance v0, Lcom/google/d/a/a/mr;

    const-string v1, "PEDESTRIAN_FACILITY_WIDE_SHOULDER"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/mr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mr;->e:Lcom/google/d/a/a/mr;

    .line 2142
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/d/a/a/mr;

    sget-object v1, Lcom/google/d/a/a/mr;->a:Lcom/google/d/a/a/mr;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/mr;->b:Lcom/google/d/a/a/mr;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/mr;->c:Lcom/google/d/a/a/mr;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mr;->d:Lcom/google/d/a/a/mr;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/mr;->e:Lcom/google/d/a/a/mr;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/d/a/a/mr;->g:[Lcom/google/d/a/a/mr;

    .line 2208
    new-instance v0, Lcom/google/d/a/a/ms;

    invoke-direct {v0}, Lcom/google/d/a/a/ms;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2218
    iput p3, p0, Lcom/google/d/a/a/mr;->f:I

    .line 2219
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mr;
    .locals 1

    .prologue
    .line 2193
    sparse-switch p0, :sswitch_data_0

    .line 2199
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2194
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/mr;->a:Lcom/google/d/a/a/mr;

    goto :goto_0

    .line 2195
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/mr;->b:Lcom/google/d/a/a/mr;

    goto :goto_0

    .line 2196
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/mr;->c:Lcom/google/d/a/a/mr;

    goto :goto_0

    .line 2197
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/mr;->d:Lcom/google/d/a/a/mr;

    goto :goto_0

    .line 2198
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/mr;->e:Lcom/google/d/a/a/mr;

    goto :goto_0

    .line 2193
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x31 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mr;
    .locals 1

    .prologue
    .line 2142
    const-class v0, Lcom/google/d/a/a/mr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mr;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mr;
    .locals 1

    .prologue
    .line 2142
    sget-object v0, Lcom/google/d/a/a/mr;->g:[Lcom/google/d/a/a/mr;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2189
    iget v0, p0, Lcom/google/d/a/a/mr;->f:I

    return v0
.end method
