.class public final Lcom/google/d/a/a/bd;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/bg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/bb;",
        "Lcom/google/d/a/a/bd;",
        ">;",
        "Lcom/google/d/a/a/bg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:F

.field private f:F

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 612
    sget-object v0, Lcom/google/d/a/a/bb;->i:Lcom/google/d/a/a/bb;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 732
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/bd;->b:I

    .line 800
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bd;->d:Lcom/google/n/ao;

    .line 924
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    .line 1060
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bd;->h:Lcom/google/n/ao;

    .line 613
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/bb;)Lcom/google/d/a/a/bd;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 679
    invoke-static {}, Lcom/google/d/a/a/bb;->d()Lcom/google/d/a/a/bb;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 711
    :goto_0
    return-object p0

    .line 680
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 681
    iget v2, p1, Lcom/google/d/a/a/bb;->b:I

    invoke-static {v2}, Lcom/google/d/a/a/be;->a(I)Lcom/google/d/a/a/be;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/d/a/a/be;->a:Lcom/google/d/a/a/be;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 680
    goto :goto_1

    .line 681
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/bd;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/bd;->a:I

    iget v2, v2, Lcom/google/d/a/a/be;->h:I

    iput v2, p0, Lcom/google/d/a/a/bd;->b:I

    .line 683
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 684
    iget v2, p1, Lcom/google/d/a/a/bb;->c:I

    iget v3, p0, Lcom/google/d/a/a/bd;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/bd;->a:I

    iput v2, p0, Lcom/google/d/a/a/bd;->c:I

    .line 686
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 687
    iget-object v2, p0, Lcom/google/d/a/a/bd;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/bb;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 688
    iget v2, p0, Lcom/google/d/a/a/bd;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/bd;->a:I

    .line 690
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 691
    iget v2, p1, Lcom/google/d/a/a/bb;->e:F

    iget v3, p0, Lcom/google/d/a/a/bd;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/bd;->a:I

    iput v2, p0, Lcom/google/d/a/a/bd;->e:F

    .line 693
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 694
    iget v2, p1, Lcom/google/d/a/a/bb;->f:F

    iget v3, p0, Lcom/google/d/a/a/bd;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/bd;->a:I

    iput v2, p0, Lcom/google/d/a/a/bd;->f:F

    .line 696
    :cond_8
    iget-object v2, p1, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 697
    iget-object v2, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 698
    iget-object v2, p1, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    .line 699
    iget v2, p0, Lcom/google/d/a/a/bd;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/d/a/a/bd;->a:I

    .line 706
    :cond_9
    :goto_6
    iget v2, p1, Lcom/google/d/a/a/bb;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_11

    :goto_7
    if-eqz v0, :cond_a

    .line 707
    iget-object v0, p0, Lcom/google/d/a/a/bd;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 708
    iget v0, p0, Lcom/google/d/a/a/bd;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/bd;->a:I

    .line 710
    :cond_a
    iget-object v0, p1, Lcom/google/d/a/a/bb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 683
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 686
    goto :goto_3

    :cond_d
    move v2, v1

    .line 690
    goto :goto_4

    :cond_e
    move v2, v1

    .line 693
    goto :goto_5

    .line 701
    :cond_f
    iget v2, p0, Lcom/google/d/a/a/bd;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_10

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/bd;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/bd;->a:I

    .line 702
    :cond_10
    iget-object v2, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_11
    move v0, v1

    .line 706
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 604
    new-instance v2, Lcom/google/d/a/a/bb;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/bb;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/bd;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/bd;->b:I

    iput v4, v2, Lcom/google/d/a/a/bb;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/d/a/a/bd;->c:I

    iput v4, v2, Lcom/google/d/a/a/bb;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/d/a/a/bb;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/bd;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/bd;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/d/a/a/bd;->e:F

    iput v4, v2, Lcom/google/d/a/a/bb;->e:F

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/d/a/a/bd;->f:F

    iput v4, v2, Lcom/google/d/a/a/bb;->f:F

    iget v4, p0, Lcom/google/d/a/a/bd;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/bd;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/d/a/a/bd;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/bb;->g:Ljava/util/List;

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v3, v2, Lcom/google/d/a/a/bb;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/bd;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/bd;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/bb;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 604
    check-cast p1, Lcom/google/d/a/a/bb;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/bd;->a(Lcom/google/d/a/a/bb;)Lcom/google/d/a/a/bd;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 715
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 716
    iget-object v0, p0, Lcom/google/d/a/a/bd;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 727
    :cond_0
    :goto_1
    return v2

    .line 715
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 721
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/bd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 722
    iget-object v0, p0, Lcom/google/d/a/a/bd;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v3

    .line 727
    goto :goto_1

    :cond_4
    move v0, v2

    .line 721
    goto :goto_2
.end method
