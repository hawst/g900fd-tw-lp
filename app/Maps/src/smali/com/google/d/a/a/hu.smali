.class public final Lcom/google/d/a/a/hu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/hx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hu;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/d/a/a/hu;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field c:Lcom/google/n/ao;

.field d:D

.field e:F

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/google/d/a/a/hv;

    invoke-direct {v0}, Lcom/google/d/a/a/hv;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hu;->PARSER:Lcom/google/n/ax;

    .line 263
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/hu;->i:Lcom/google/n/aw;

    .line 578
    new-instance v0, Lcom/google/d/a/a/hu;

    invoke-direct {v0}, Lcom/google/d/a/a/hu;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hu;->f:Lcom/google/d/a/a/hu;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 62
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 155
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hu;->c:Lcom/google/n/ao;

    .line 200
    iput-byte v1, p0, Lcom/google/d/a/a/hu;->g:B

    .line 234
    iput v1, p0, Lcom/google/d/a/a/hu;->h:I

    .line 63
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/hu;->b:J

    .line 64
    iget-object v0, p0, Lcom/google/d/a/a/hu;->c:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 65
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/hu;->d:D

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/hu;->e:F

    .line 67
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Lcom/google/d/a/a/hu;-><init>()V

    .line 74
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 79
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 80
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 81
    sparse-switch v3, :sswitch_data_0

    .line 86
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 88
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 84
    goto :goto_0

    .line 93
    :sswitch_1
    iget v3, p0, Lcom/google/d/a/a/hu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/hu;->a:I

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/hu;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/hu;->au:Lcom/google/n/bn;

    throw v0

    .line 98
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/d/a/a/hu;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 99
    iget v3, p0, Lcom/google/d/a/a/hu;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/hu;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 116
    :catch_1
    move-exception v0

    .line 117
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 118
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/d/a/a/hu;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/hu;->a:I

    .line 104
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/hu;->d:D

    goto :goto_0

    .line 108
    :sswitch_4
    iget v3, p0, Lcom/google/d/a/a/hu;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/hu;->a:I

    .line 109
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/hu;->e:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hu;->au:Lcom/google/n/bn;

    .line 121
    return-void

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 60
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 155
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hu;->c:Lcom/google/n/ao;

    .line 200
    iput-byte v1, p0, Lcom/google/d/a/a/hu;->g:B

    .line 234
    iput v1, p0, Lcom/google/d/a/a/hu;->h:I

    .line 61
    return-void
.end method

.method public static d()Lcom/google/d/a/a/hu;
    .locals 1

    .prologue
    .line 581
    sget-object v0, Lcom/google/d/a/a/hu;->f:Lcom/google/d/a/a/hu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/hw;
    .locals 1

    .prologue
    .line 325
    new-instance v0, Lcom/google/d/a/a/hw;

    invoke-direct {v0}, Lcom/google/d/a/a/hw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    sget-object v0, Lcom/google/d/a/a/hu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 218
    invoke-virtual {p0}, Lcom/google/d/a/a/hu;->c()I

    .line 219
    iget v0, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 220
    iget-wide v0, p0, Lcom/google/d/a/a/hu;->b:J

    const/4 v2, 0x0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 222
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/d/a/a/hu;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 225
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 226
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/d/a/a/hu;->d:D

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 228
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 229
    iget v0, p0, Lcom/google/d/a/a/hu;->e:F

    const/4 v1, 0x5

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 231
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/hu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 232
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 202
    iget-byte v0, p0, Lcom/google/d/a/a/hu;->g:B

    .line 203
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 213
    :goto_0
    return v0

    .line 204
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 206
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 207
    iget-object v0, p0, Lcom/google/d/a/a/hu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 208
    iput-byte v2, p0, Lcom/google/d/a/a/hu;->g:B

    move v0, v2

    .line 209
    goto :goto_0

    :cond_2
    move v0, v2

    .line 206
    goto :goto_1

    .line 212
    :cond_3
    iput-byte v1, p0, Lcom/google/d/a/a/hu;->g:B

    move v0, v1

    .line 213
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 236
    iget v0, p0, Lcom/google/d/a/a/hu;->h:I

    .line 237
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 258
    :goto_0
    return v0

    .line 240
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 241
    iget-wide v2, p0, Lcom/google/d/a/a/hu;->b:J

    .line 242
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 244
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 245
    iget-object v2, p0, Lcom/google/d/a/a/hu;->c:Lcom/google/n/ao;

    .line 246
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 248
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 249
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/d/a/a/hu;->d:D

    .line 250
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 252
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/hu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 253
    iget v2, p0, Lcom/google/d/a/a/hu;->e:F

    .line 254
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 256
    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/hu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    iput v0, p0, Lcom/google/d/a/a/hu;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/d/a/a/hu;->newBuilder()Lcom/google/d/a/a/hw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/hw;->a(Lcom/google/d/a/a/hu;)Lcom/google/d/a/a/hw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/d/a/a/hu;->newBuilder()Lcom/google/d/a/a/hw;

    move-result-object v0

    return-object v0
.end method
