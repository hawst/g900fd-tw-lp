.class public final Lcom/google/d/a/a/iu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/iz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/is;",
        "Lcom/google/d/a/a/iu;",
        ">;",
        "Lcom/google/d/a/a/iz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z

.field private e:I

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1087
    sget-object v0, Lcom/google/d/a/a/is;->m:Lcom/google/d/a/a/is;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1232
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    .line 1368
    iput v1, p0, Lcom/google/d/a/a/iu;->c:I

    .line 1436
    iput v1, p0, Lcom/google/d/a/a/iu;->e:I

    .line 1088
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/is;)Lcom/google/d/a/a/iu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1174
    invoke-static {}, Lcom/google/d/a/a/is;->d()Lcom/google/d/a/a/is;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1216
    :goto_0
    return-object p0

    .line 1175
    :cond_0
    iget-object v2, p1, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1176
    iget-object v2, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1177
    iget-object v2, p1, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    .line 1178
    iget v2, p0, Lcom/google/d/a/a/iu;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/d/a/a/iu;->a:I

    .line 1185
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 1186
    iget v2, p1, Lcom/google/d/a/a/is;->c:I

    invoke-static {v2}, Lcom/google/d/a/a/iv;->a(I)Lcom/google/d/a/a/iv;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/d/a/a/iv;->a:Lcom/google/d/a/a/iv;

    :cond_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1180
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/iu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/iu;->a:I

    .line 1181
    :cond_4
    iget-object v2, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 1185
    goto :goto_2

    .line 1186
    :cond_6
    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iget v2, v2, Lcom/google/d/a/a/iv;->J:I

    iput v2, p0, Lcom/google/d/a/a/iu;->c:I

    .line 1188
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 1189
    iget-boolean v2, p1, Lcom/google/d/a/a/is;->d:Z

    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/iu;->d:Z

    .line 1191
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_d

    .line 1192
    iget v2, p1, Lcom/google/d/a/a/is;->e:I

    invoke-static {v2}, Lcom/google/d/a/a/ix;->a(I)Lcom/google/d/a/a/ix;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/d/a/a/ix;->a:Lcom/google/d/a/a/ix;

    :cond_9
    if-nez v2, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 1188
    goto :goto_3

    :cond_b
    move v2, v1

    .line 1191
    goto :goto_4

    .line 1192
    :cond_c
    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iget v2, v2, Lcom/google/d/a/a/ix;->e:I

    iput v2, p0, Lcom/google/d/a/a/iu;->e:I

    .line 1194
    :cond_d
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_5
    if-eqz v2, :cond_e

    .line 1195
    iget v2, p1, Lcom/google/d/a/a/is;->f:F

    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iput v2, p0, Lcom/google/d/a/a/iu;->f:F

    .line 1197
    :cond_e
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_6
    if-eqz v2, :cond_f

    .line 1198
    iget v2, p1, Lcom/google/d/a/a/is;->g:F

    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iput v2, p0, Lcom/google/d/a/a/iu;->g:F

    .line 1200
    :cond_f
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_7
    if-eqz v2, :cond_10

    .line 1201
    iget v2, p1, Lcom/google/d/a/a/is;->h:F

    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iput v2, p0, Lcom/google/d/a/a/iu;->h:F

    .line 1203
    :cond_10
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_8
    if-eqz v2, :cond_11

    .line 1204
    iget v2, p1, Lcom/google/d/a/a/is;->i:F

    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iput v2, p0, Lcom/google/d/a/a/iu;->i:F

    .line 1206
    :cond_11
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_9
    if-eqz v2, :cond_12

    .line 1207
    iget v2, p1, Lcom/google/d/a/a/is;->j:F

    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iput v2, p0, Lcom/google/d/a/a/iu;->j:F

    .line 1209
    :cond_12
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_a
    if-eqz v2, :cond_13

    .line 1210
    iget v2, p1, Lcom/google/d/a/a/is;->k:F

    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/d/a/a/iu;->a:I

    iput v2, p0, Lcom/google/d/a/a/iu;->k:F

    .line 1212
    :cond_13
    iget v2, p1, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1b

    :goto_b
    if-eqz v0, :cond_14

    .line 1213
    iget v0, p1, Lcom/google/d/a/a/is;->l:F

    iget v1, p0, Lcom/google/d/a/a/iu;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/d/a/a/iu;->a:I

    iput v0, p0, Lcom/google/d/a/a/iu;->l:F

    .line 1215
    :cond_14
    iget-object v0, p1, Lcom/google/d/a/a/is;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_15
    move v2, v1

    .line 1194
    goto/16 :goto_5

    :cond_16
    move v2, v1

    .line 1197
    goto/16 :goto_6

    :cond_17
    move v2, v1

    .line 1200
    goto :goto_7

    :cond_18
    move v2, v1

    .line 1203
    goto :goto_8

    :cond_19
    move v2, v1

    .line 1206
    goto :goto_9

    :cond_1a
    move v2, v1

    .line 1209
    goto :goto_a

    :cond_1b
    move v0, v1

    .line 1212
    goto :goto_b
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1079
    new-instance v2, Lcom/google/d/a/a/is;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/is;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/iu;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/d/a/a/iu;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/iu;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/iu;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_a

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/iu;->c:I

    iput v1, v2, Lcom/google/d/a/a/is;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-boolean v1, p0, Lcom/google/d/a/a/iu;->d:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/is;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/iu;->e:I

    iput v1, v2, Lcom/google/d/a/a/is;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget v1, p0, Lcom/google/d/a/a/iu;->f:F

    iput v1, v2, Lcom/google/d/a/a/is;->f:F

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget v1, p0, Lcom/google/d/a/a/iu;->g:F

    iput v1, v2, Lcom/google/d/a/a/is;->g:F

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget v1, p0, Lcom/google/d/a/a/iu;->h:F

    iput v1, v2, Lcom/google/d/a/a/is;->h:F

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget v1, p0, Lcom/google/d/a/a/iu;->i:F

    iput v1, v2, Lcom/google/d/a/a/is;->i:F

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget v1, p0, Lcom/google/d/a/a/iu;->j:F

    iput v1, v2, Lcom/google/d/a/a/is;->j:F

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget v1, p0, Lcom/google/d/a/a/iu;->k:F

    iput v1, v2, Lcom/google/d/a/a/is;->k:F

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget v1, p0, Lcom/google/d/a/a/iu;->l:F

    iput v1, v2, Lcom/google/d/a/a/is;->l:F

    iput v0, v2, Lcom/google/d/a/a/is;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1079
    check-cast p1, Lcom/google/d/a/a/is;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/iu;->a(Lcom/google/d/a/a/is;)Lcom/google/d/a/a/iu;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1220
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1221
    iget-object v0, p0, Lcom/google/d/a/a/iu;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/jb;->d()Lcom/google/d/a/a/jb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/jb;

    invoke-virtual {v0}, Lcom/google/d/a/a/jb;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1226
    :goto_1
    return v2

    .line 1220
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1226
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
