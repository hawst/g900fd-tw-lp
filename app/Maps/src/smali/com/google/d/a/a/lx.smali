.class public final enum Lcom/google/d/a/a/lx;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/lx;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/lx;

.field public static final enum b:Lcom/google/d/a/a/lx;

.field public static final enum c:Lcom/google/d/a/a/lx;

.field public static final enum d:Lcom/google/d/a/a/lx;

.field private static final synthetic f:[Lcom/google/d/a/a/lx;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 7225
    new-instance v0, Lcom/google/d/a/a/lx;

    const-string v1, "DISPLAY_PREFERRED"

    invoke-direct {v0, v1, v6, v3}, Lcom/google/d/a/a/lx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/lx;->a:Lcom/google/d/a/a/lx;

    .line 7229
    new-instance v0, Lcom/google/d/a/a/lx;

    const-string v1, "DISPLAY_BEST"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v3, v2}, Lcom/google/d/a/a/lx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/lx;->b:Lcom/google/d/a/a/lx;

    .line 7233
    new-instance v0, Lcom/google/d/a/a/lx;

    const-string v1, "DISPLAY_OK"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/lx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/lx;->c:Lcom/google/d/a/a/lx;

    .line 7237
    new-instance v0, Lcom/google/d/a/a/lx;

    const-string v1, "DISPLAY_HIDE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/lx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/lx;->d:Lcom/google/d/a/a/lx;

    .line 7220
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/d/a/a/lx;

    sget-object v1, Lcom/google/d/a/a/lx;->a:Lcom/google/d/a/a/lx;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/lx;->b:Lcom/google/d/a/a/lx;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/lx;->c:Lcom/google/d/a/a/lx;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/lx;->d:Lcom/google/d/a/a/lx;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/d/a/a/lx;->f:[Lcom/google/d/a/a/lx;

    .line 7277
    new-instance v0, Lcom/google/d/a/a/ly;

    invoke-direct {v0}, Lcom/google/d/a/a/ly;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 7286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 7287
    iput p3, p0, Lcom/google/d/a/a/lx;->e:I

    .line 7288
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/lx;
    .locals 1

    .prologue
    .line 7263
    sparse-switch p0, :sswitch_data_0

    .line 7268
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 7264
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/lx;->a:Lcom/google/d/a/a/lx;

    goto :goto_0

    .line 7265
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/lx;->b:Lcom/google/d/a/a/lx;

    goto :goto_0

    .line 7266
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/lx;->c:Lcom/google/d/a/a/lx;

    goto :goto_0

    .line 7267
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/lx;->d:Lcom/google/d/a/a/lx;

    goto :goto_0

    .line 7263
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x11 -> :sswitch_1
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/lx;
    .locals 1

    .prologue
    .line 7220
    const-class v0, Lcom/google/d/a/a/lx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/lx;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/lx;
    .locals 1

    .prologue
    .line 7220
    sget-object v0, Lcom/google/d/a/a/lx;->f:[Lcom/google/d/a/a/lx;

    invoke-virtual {v0}, [Lcom/google/d/a/a/lx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/lx;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 7259
    iget v0, p0, Lcom/google/d/a/a/lx;->e:I

    return v0
.end method
