.class public final Lcom/google/d/a/a/bk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/bl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/bi;",
        "Lcom/google/d/a/a/bk;",
        ">;",
        "Lcom/google/d/a/a/bl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;

.field private k:Ljava/lang/Object;

.field private l:Ljava/lang/Object;

.field private m:Ljava/lang/Object;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1111
    sget-object v0, Lcom/google/d/a/a/bi;->p:Lcom/google/d/a/a/bi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1323
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->b:Ljava/lang/Object;

    .line 1399
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->c:Ljava/lang/Object;

    .line 1507
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bk;->e:Lcom/google/n/ao;

    .line 1566
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->f:Ljava/lang/Object;

    .line 1643
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    .line 1779
    const/16 v0, 0x1111

    iput v0, p0, Lcom/google/d/a/a/bk;->h:I

    .line 1815
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->i:Ljava/lang/Object;

    .line 1891
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->j:Ljava/lang/Object;

    .line 1967
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->k:Ljava/lang/Object;

    .line 2043
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->l:Ljava/lang/Object;

    .line 2119
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->m:Ljava/lang/Object;

    .line 2196
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    .line 2332
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bk;->o:Ljava/lang/Object;

    .line 1112
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/bi;)Lcom/google/d/a/a/bk;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1219
    invoke-static {}, Lcom/google/d/a/a/bi;->d()Lcom/google/d/a/a/bi;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1296
    :goto_0
    return-object p0

    .line 1220
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1221
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1222
    iget-object v2, p1, Lcom/google/d/a/a/bi;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->b:Ljava/lang/Object;

    .line 1225
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1226
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1227
    iget-object v2, p1, Lcom/google/d/a/a/bi;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->c:Ljava/lang/Object;

    .line 1230
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1231
    iget v2, p1, Lcom/google/d/a/a/bi;->d:I

    iget v3, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/bk;->a:I

    iput v2, p0, Lcom/google/d/a/a/bk;->d:I

    .line 1233
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1234
    iget-object v2, p0, Lcom/google/d/a/a/bk;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1235
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1237
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1238
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1239
    iget-object v2, p1, Lcom/google/d/a/a/bi;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->f:Ljava/lang/Object;

    .line 1242
    :cond_5
    iget-object v2, p1, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1243
    iget-object v2, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1244
    iget-object v2, p1, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    .line 1245
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1252
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_11

    .line 1253
    iget v2, p1, Lcom/google/d/a/a/bi;->h:I

    invoke-static {v2}, Lcom/google/d/a/a/bq;->a(I)Lcom/google/d/a/a/bq;

    move-result-object v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/d/a/a/bq;->b:Lcom/google/d/a/a/bq;

    :cond_7
    if-nez v2, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v1

    .line 1220
    goto/16 :goto_1

    :cond_9
    move v2, v1

    .line 1225
    goto/16 :goto_2

    :cond_a
    move v2, v1

    .line 1230
    goto :goto_3

    :cond_b
    move v2, v1

    .line 1233
    goto :goto_4

    :cond_c
    move v2, v1

    .line 1237
    goto :goto_5

    .line 1247
    :cond_d
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1248
    :cond_e
    iget-object v2, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_f
    move v2, v1

    .line 1252
    goto :goto_7

    .line 1253
    :cond_10
    iget v3, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/bk;->a:I

    iget v2, v2, Lcom/google/d/a/a/bq;->hy:I

    iput v2, p0, Lcom/google/d/a/a/bk;->h:I

    .line 1255
    :cond_11
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_8
    if-eqz v2, :cond_12

    .line 1256
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1257
    iget-object v2, p1, Lcom/google/d/a/a/bi;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->i:Ljava/lang/Object;

    .line 1260
    :cond_12
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_9
    if-eqz v2, :cond_13

    .line 1261
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1262
    iget-object v2, p1, Lcom/google/d/a/a/bi;->j:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->j:Ljava/lang/Object;

    .line 1265
    :cond_13
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_a
    if-eqz v2, :cond_14

    .line 1266
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1267
    iget-object v2, p1, Lcom/google/d/a/a/bi;->k:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->k:Ljava/lang/Object;

    .line 1270
    :cond_14
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_b
    if-eqz v2, :cond_15

    .line 1271
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1272
    iget-object v2, p1, Lcom/google/d/a/a/bi;->l:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->l:Ljava/lang/Object;

    .line 1275
    :cond_15
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_c
    if-eqz v2, :cond_16

    .line 1276
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1277
    iget-object v2, p1, Lcom/google/d/a/a/bi;->m:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->m:Ljava/lang/Object;

    .line 1280
    :cond_16
    iget-object v2, p1, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_17

    .line 1281
    iget-object v2, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1282
    iget-object v2, p1, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    .line 1283
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit16 v2, v2, -0x1001

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1290
    :cond_17
    :goto_d
    iget v2, p1, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_20

    :goto_e
    if-eqz v0, :cond_18

    .line 1291
    iget v0, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1292
    iget-object v0, p1, Lcom/google/d/a/a/bi;->o:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/d/a/a/bk;->o:Ljava/lang/Object;

    .line 1295
    :cond_18
    iget-object v0, p1, Lcom/google/d/a/a/bi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_19
    move v2, v1

    .line 1255
    goto/16 :goto_8

    :cond_1a
    move v2, v1

    .line 1260
    goto/16 :goto_9

    :cond_1b
    move v2, v1

    .line 1265
    goto :goto_a

    :cond_1c
    move v2, v1

    .line 1270
    goto :goto_b

    :cond_1d
    move v2, v1

    .line 1275
    goto :goto_c

    .line 1285
    :cond_1e
    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-eq v2, v3, :cond_1f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/bk;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/d/a/a/bk;->a:I

    .line 1286
    :cond_1f
    iget-object v2, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_d

    :cond_20
    move v0, v1

    .line 1290
    goto :goto_e
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1103
    new-instance v2, Lcom/google/d/a/a/bi;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/bi;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_d

    :goto_0
    iget-object v4, p0, Lcom/google/d/a/a/bk;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/bi;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/bk;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/bi;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/bk;->d:I

    iput v4, v2, Lcom/google/d/a/a/bi;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/bk;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/bk;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/bk;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->f:Ljava/lang/Object;

    iget v1, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    iget-object v1, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/d/a/a/bk;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget v1, p0, Lcom/google/d/a/a/bk;->h:I

    iput v1, v2, Lcom/google/d/a/a/bi;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v1, p0, Lcom/google/d/a/a/bk;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->i:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v1, p0, Lcom/google/d/a/a/bk;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->j:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v1, p0, Lcom/google/d/a/a/bk;->k:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->k:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v1, p0, Lcom/google/d/a/a/bk;->l:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->l:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget-object v1, p0, Lcom/google/d/a/a/bk;->m:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->m:Ljava/lang/Object;

    iget v1, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    iget-object v1, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit16 v1, v1, -0x1001

    iput v1, p0, Lcom/google/d/a/a/bk;->a:I

    :cond_b
    iget-object v1, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    and-int/lit16 v1, v3, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_c

    or-int/lit16 v0, v0, 0x800

    :cond_c
    iget-object v1, p0, Lcom/google/d/a/a/bk;->o:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/bi;->o:Ljava/lang/Object;

    iput v0, v2, Lcom/google/d/a/a/bi;->a:I

    return-object v2

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1103
    check-cast p1, Lcom/google/d/a/a/bi;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/bk;->a(Lcom/google/d/a/a/bi;)Lcom/google/d/a/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1300
    iget v0, p0, Lcom/google/d/a/a/bk;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1301
    iget-object v0, p0, Lcom/google/d/a/a/bk;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1318
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1300
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1306
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1307
    iget-object v0, p0, Lcom/google/d/a/a/bk;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/jn;->d()Lcom/google/d/a/a/jn;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/jn;

    invoke-virtual {v0}, Lcom/google/d/a/a/jn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1306
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 1312
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1313
    iget-object v0, p0, Lcom/google/d/a/a/bk;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1312
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v2, v3

    .line 1318
    goto :goto_1
.end method
