.class public final enum Lcom/google/d/a/a/kc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/kc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/d/a/a/kc;

.field public static final enum B:Lcom/google/d/a/a/kc;

.field public static final enum C:Lcom/google/d/a/a/kc;

.field public static final enum D:Lcom/google/d/a/a/kc;

.field public static final enum E:Lcom/google/d/a/a/kc;

.field private static final synthetic G:[Lcom/google/d/a/a/kc;

.field public static final enum a:Lcom/google/d/a/a/kc;

.field public static final enum b:Lcom/google/d/a/a/kc;

.field public static final enum c:Lcom/google/d/a/a/kc;

.field public static final enum d:Lcom/google/d/a/a/kc;

.field public static final enum e:Lcom/google/d/a/a/kc;

.field public static final enum f:Lcom/google/d/a/a/kc;

.field public static final enum g:Lcom/google/d/a/a/kc;

.field public static final enum h:Lcom/google/d/a/a/kc;

.field public static final enum i:Lcom/google/d/a/a/kc;

.field public static final enum j:Lcom/google/d/a/a/kc;

.field public static final enum k:Lcom/google/d/a/a/kc;

.field public static final enum l:Lcom/google/d/a/a/kc;

.field public static final enum m:Lcom/google/d/a/a/kc;

.field public static final enum n:Lcom/google/d/a/a/kc;

.field public static final enum o:Lcom/google/d/a/a/kc;

.field public static final enum p:Lcom/google/d/a/a/kc;

.field public static final enum q:Lcom/google/d/a/a/kc;

.field public static final enum r:Lcom/google/d/a/a/kc;

.field public static final enum s:Lcom/google/d/a/a/kc;

.field public static final enum t:Lcom/google/d/a/a/kc;

.field public static final enum u:Lcom/google/d/a/a/kc;

.field public static final enum v:Lcom/google/d/a/a/kc;

.field public static final enum w:Lcom/google/d/a/a/kc;

.field public static final enum x:Lcom/google/d/a/a/kc;

.field public static final enum y:Lcom/google/d/a/a/kc;

.field public static final enum z:Lcom/google/d/a/a/kc;


# instance fields
.field final F:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 282
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_OVERLAPS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->a:Lcom/google/d/a/a/kc;

    .line 286
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_CONTAINED_BY"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->b:Lcom/google/d/a/a/kc;

    .line 290
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_EQUAL_TO"

    const/16 v2, 0x111

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->c:Lcom/google/d/a/a/kc;

    .line 294
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_POLITICAL"

    invoke-direct {v0, v1, v6, v5}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->d:Lcom/google/d/a/a/kc;

    .line 298
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_CAPITAL_OF"

    invoke-direct {v0, v1, v7, v6}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->e:Lcom/google/d/a/a/kc;

    .line 302
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_DISAMBIGUATED_BY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v7}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->f:Lcom/google/d/a/a/kc;

    .line 306
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_NEIGHBOR_OF"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->g:Lcom/google/d/a/a/kc;

    .line 310
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_OPPOSITE_TO"

    const/4 v2, 0x7

    const/16 v3, 0x411

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->h:Lcom/google/d/a/a/kc;

    .line 314
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_NEXT_TO"

    const/16 v2, 0x8

    const/16 v3, 0x412

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->i:Lcom/google/d/a/a/kc;

    .line 318
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_RIGHT_OF"

    const/16 v2, 0x9

    const/16 v3, 0x4121

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->j:Lcom/google/d/a/a/kc;

    .line 322
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_LEFT_OF"

    const/16 v2, 0xa

    const/16 v3, 0x4122

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->k:Lcom/google/d/a/a/kc;

    .line 326
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_BEHIND"

    const/16 v2, 0xb

    const/16 v3, 0x413

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->l:Lcom/google/d/a/a/kc;

    .line 330
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_IN_FRONT_OF"

    const/16 v2, 0xc

    const/16 v3, 0x414

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->m:Lcom/google/d/a/a/kc;

    .line 334
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_SAME_BUILDING"

    const/16 v2, 0xd

    const/16 v3, 0x42

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->n:Lcom/google/d/a/a/kc;

    .line 338
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_ABOVE"

    const/16 v2, 0xe

    const/16 v3, 0x421

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->o:Lcom/google/d/a/a/kc;

    .line 342
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_BELOW"

    const/16 v2, 0xf

    const/16 v3, 0x422

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->p:Lcom/google/d/a/a/kc;

    .line 346
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_NEAR"

    const/16 v2, 0x10

    const/16 v3, 0x43

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->q:Lcom/google/d/a/a/kc;

    .line 350
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_ORGANIZATIONALLY_PART_OF"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v8}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->r:Lcom/google/d/a/a/kc;

    .line 354
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_DEPARTMENT_OF"

    const/16 v2, 0x12

    const/16 v3, 0x61

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->s:Lcom/google/d/a/a/kc;

    .line 358
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_BRANCH_OF_DEPRECATED"

    const/16 v2, 0x13

    const/16 v3, 0x62

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->t:Lcom/google/d/a/a/kc;

    .line 362
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_WORKS_AT"

    const/16 v2, 0x14

    const/16 v3, 0x63

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->u:Lcom/google/d/a/a/kc;

    .line 366
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_INDEPENDENT_ESTABLISHMENT_IN"

    const/16 v2, 0x15

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->v:Lcom/google/d/a/a/kc;

    .line 370
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_ON_LEVEL"

    const/16 v2, 0x16

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->w:Lcom/google/d/a/a/kc;

    .line 374
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_OCCUPIES"

    const/16 v2, 0x17

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->x:Lcom/google/d/a/a/kc;

    .line 378
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_BUSINESS_LIFE_CYCLE"

    const/16 v2, 0x18

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->y:Lcom/google/d/a/a/kc;

    .line 382
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_BUSINESS_MOVED"

    const/16 v2, 0x19

    const/16 v3, 0x91

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->z:Lcom/google/d/a/a/kc;

    .line 386
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_BUSINESS_REBRANDED"

    const/16 v2, 0x1a

    const/16 v3, 0x92

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->A:Lcom/google/d/a/a/kc;

    .line 390
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_MEMBER_OF_CHAIN"

    const/16 v2, 0x1b

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->B:Lcom/google/d/a/a/kc;

    .line 394
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_SUBSIDIARY_OF"

    const/16 v2, 0x1c

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->C:Lcom/google/d/a/a/kc;

    .line 398
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_PRIMARILY_OCCUPIED_BY"

    const/16 v2, 0x1d

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->D:Lcom/google/d/a/a/kc;

    .line 402
    new-instance v0, Lcom/google/d/a/a/kc;

    const-string v1, "RELATION_CLIENT_DEFINED"

    const/16 v2, 0x1e

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/kc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/kc;->E:Lcom/google/d/a/a/kc;

    .line 277
    const/16 v0, 0x1f

    new-array v0, v0, [Lcom/google/d/a/a/kc;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/d/a/a/kc;->a:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/kc;->b:Lcom/google/d/a/a/kc;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/kc;->c:Lcom/google/d/a/a/kc;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/kc;->d:Lcom/google/d/a/a/kc;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/kc;->e:Lcom/google/d/a/a/kc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/kc;->f:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/kc;->g:Lcom/google/d/a/a/kc;

    aput-object v1, v0, v8

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/kc;->h:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/kc;->i:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/kc;->j:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/kc;->k:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/kc;->l:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/kc;->m:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/d/a/a/kc;->n:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/d/a/a/kc;->o:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/d/a/a/kc;->p:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/d/a/a/kc;->q:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/d/a/a/kc;->r:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/d/a/a/kc;->s:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/d/a/a/kc;->t:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/d/a/a/kc;->u:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/d/a/a/kc;->v:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/d/a/a/kc;->w:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/d/a/a/kc;->x:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/d/a/a/kc;->y:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/d/a/a/kc;->z:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/d/a/a/kc;->A:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/d/a/a/kc;->B:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/d/a/a/kc;->C:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/d/a/a/kc;->D:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/d/a/a/kc;->E:Lcom/google/d/a/a/kc;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/kc;->G:[Lcom/google/d/a/a/kc;

    .line 577
    new-instance v0, Lcom/google/d/a/a/kd;

    invoke-direct {v0}, Lcom/google/d/a/a/kd;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 586
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 587
    iput p3, p0, Lcom/google/d/a/a/kc;->F:I

    .line 588
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/kc;
    .locals 1

    .prologue
    .line 536
    sparse-switch p0, :sswitch_data_0

    .line 568
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 537
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/kc;->a:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 538
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/kc;->b:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 539
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/kc;->c:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 540
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/kc;->d:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 541
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/kc;->e:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 542
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/kc;->f:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 543
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/kc;->g:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 544
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/kc;->h:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 545
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/kc;->i:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 546
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/kc;->j:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 547
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/kc;->k:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 548
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/kc;->l:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 549
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/kc;->m:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 550
    :sswitch_d
    sget-object v0, Lcom/google/d/a/a/kc;->n:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 551
    :sswitch_e
    sget-object v0, Lcom/google/d/a/a/kc;->o:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 552
    :sswitch_f
    sget-object v0, Lcom/google/d/a/a/kc;->p:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 553
    :sswitch_10
    sget-object v0, Lcom/google/d/a/a/kc;->q:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 554
    :sswitch_11
    sget-object v0, Lcom/google/d/a/a/kc;->r:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 555
    :sswitch_12
    sget-object v0, Lcom/google/d/a/a/kc;->s:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 556
    :sswitch_13
    sget-object v0, Lcom/google/d/a/a/kc;->t:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 557
    :sswitch_14
    sget-object v0, Lcom/google/d/a/a/kc;->u:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 558
    :sswitch_15
    sget-object v0, Lcom/google/d/a/a/kc;->v:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 559
    :sswitch_16
    sget-object v0, Lcom/google/d/a/a/kc;->w:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 560
    :sswitch_17
    sget-object v0, Lcom/google/d/a/a/kc;->x:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 561
    :sswitch_18
    sget-object v0, Lcom/google/d/a/a/kc;->y:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 562
    :sswitch_19
    sget-object v0, Lcom/google/d/a/a/kc;->z:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 563
    :sswitch_1a
    sget-object v0, Lcom/google/d/a/a/kc;->A:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 564
    :sswitch_1b
    sget-object v0, Lcom/google/d/a/a/kc;->B:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 565
    :sswitch_1c
    sget-object v0, Lcom/google/d/a/a/kc;->C:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 566
    :sswitch_1d
    sget-object v0, Lcom/google/d/a/a/kc;->D:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 567
    :sswitch_1e
    sget-object v0, Lcom/google/d/a/a/kc;->E:Lcom/google/d/a/a/kc;

    goto :goto_0

    .line 536
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x6 -> :sswitch_11
        0x7 -> :sswitch_16
        0x8 -> :sswitch_17
        0x9 -> :sswitch_18
        0xa -> :sswitch_1b
        0xb -> :sswitch_1c
        0xc -> :sswitch_1d
        0xf -> :sswitch_1e
        0x11 -> :sswitch_1
        0x41 -> :sswitch_6
        0x42 -> :sswitch_d
        0x43 -> :sswitch_10
        0x61 -> :sswitch_12
        0x62 -> :sswitch_13
        0x63 -> :sswitch_14
        0x64 -> :sswitch_15
        0x91 -> :sswitch_19
        0x92 -> :sswitch_1a
        0x111 -> :sswitch_2
        0x411 -> :sswitch_7
        0x412 -> :sswitch_8
        0x413 -> :sswitch_b
        0x414 -> :sswitch_c
        0x421 -> :sswitch_e
        0x422 -> :sswitch_f
        0x4121 -> :sswitch_9
        0x4122 -> :sswitch_a
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/kc;
    .locals 1

    .prologue
    .line 277
    const-class v0, Lcom/google/d/a/a/kc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/kc;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/kc;
    .locals 1

    .prologue
    .line 277
    sget-object v0, Lcom/google/d/a/a/kc;->G:[Lcom/google/d/a/a/kc;

    invoke-virtual {v0}, [Lcom/google/d/a/a/kc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/kc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 532
    iget v0, p0, Lcom/google/d/a/a/kc;->F:I

    return v0
.end method
