.class public final Lcom/google/d/a/a/qd;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/qg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/qb;",
        "Lcom/google/d/a/a/qd;",
        ">;",
        "Lcom/google/d/a/a/qg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 503
    sget-object v0, Lcom/google/d/a/a/qb;->g:Lcom/google/d/a/a/qb;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 615
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/qd;->b:Lcom/google/n/ao;

    .line 674
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/qd;->c:Lcom/google/n/ao;

    .line 734
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    .line 870
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/qd;->e:I

    .line 504
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/qb;)Lcom/google/d/a/a/qd;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 558
    invoke-static {}, Lcom/google/d/a/a/qb;->d()Lcom/google/d/a/a/qb;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 584
    :goto_0
    return-object p0

    .line 559
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 560
    iget-object v2, p0, Lcom/google/d/a/a/qd;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 561
    iget v2, p0, Lcom/google/d/a/a/qd;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/qd;->a:I

    .line 563
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 564
    iget-object v2, p0, Lcom/google/d/a/a/qd;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 565
    iget v2, p0, Lcom/google/d/a/a/qd;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/qd;->a:I

    .line 567
    :cond_2
    iget-object v2, p1, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 568
    iget-object v2, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 569
    iget-object v2, p1, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    .line 570
    iget v2, p0, Lcom/google/d/a/a/qd;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/a/a/qd;->a:I

    .line 577
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 578
    iget v2, p1, Lcom/google/d/a/a/qb;->e:I

    invoke-static {v2}, Lcom/google/d/a/a/qe;->a(I)Lcom/google/d/a/a/qe;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/d/a/a/qe;->a:Lcom/google/d/a/a/qe;

    :cond_4
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 559
    goto :goto_1

    :cond_6
    move v2, v1

    .line 563
    goto :goto_2

    .line 572
    :cond_7
    iget v2, p0, Lcom/google/d/a/a/qd;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/qd;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/qd;->a:I

    .line 573
    :cond_8
    iget-object v2, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_9
    move v2, v1

    .line 577
    goto :goto_4

    .line 578
    :cond_a
    iget v3, p0, Lcom/google/d/a/a/qd;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/qd;->a:I

    iget v2, v2, Lcom/google/d/a/a/qe;->c:I

    iput v2, p0, Lcom/google/d/a/a/qd;->e:I

    .line 580
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/qb;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    :goto_5
    if-eqz v0, :cond_c

    .line 581
    iget-boolean v0, p1, Lcom/google/d/a/a/qb;->f:Z

    iget v1, p0, Lcom/google/d/a/a/qd;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/d/a/a/qd;->a:I

    iput-boolean v0, p0, Lcom/google/d/a/a/qd;->f:Z

    .line 583
    :cond_c
    iget-object v0, p1, Lcom/google/d/a/a/qb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 580
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 495
    new-instance v2, Lcom/google/d/a/a/qb;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/qb;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/qd;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/qb;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/qd;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/qd;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/d/a/a/qb;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/qd;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/qd;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/d/a/a/qd;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qd;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/d/a/a/qd;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/qb;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/qd;->e:I

    iput v1, v2, Lcom/google/d/a/a/qb;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-boolean v1, p0, Lcom/google/d/a/a/qd;->f:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/qb;->f:Z

    iput v0, v2, Lcom/google/d/a/a/qb;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 495
    check-cast p1, Lcom/google/d/a/a/qb;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/qd;->a(Lcom/google/d/a/a/qb;)Lcom/google/d/a/a/qd;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 588
    iget v0, p0, Lcom/google/d/a/a/qd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 610
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 588
    goto :goto_0

    .line 592
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/qd;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    move v0, v3

    :goto_2
    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/google/d/a/a/qd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/google/d/a/a/qd;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 604
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 605
    iget-object v0, p0, Lcom/google/d/a/a/qd;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v2

    .line 592
    goto :goto_2

    :cond_4
    move v2, v3

    .line 610
    goto :goto_1
.end method
