.class public final Lcom/google/d/a/a/hm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/hn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/hk;",
        "Lcom/google/d/a/a/hm;",
        ">;",
        "Lcom/google/d/a/a/hn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:D

.field private c:D

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 350
    sget-object v0, Lcom/google/d/a/a/hk;->f:Lcom/google/d/a/a/hk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 499
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hm;->d:Lcom/google/n/ao;

    .line 558
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hm;->e:Lcom/google/n/ao;

    .line 351
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/hk;)Lcom/google/d/a/a/hm;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 398
    invoke-static {}, Lcom/google/d/a/a/hk;->d()Lcom/google/d/a/a/hk;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 414
    :goto_0
    return-object p0

    .line 399
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/hk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 400
    iget-wide v2, p1, Lcom/google/d/a/a/hk;->b:D

    iget v4, p0, Lcom/google/d/a/a/hm;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/hm;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/hm;->b:D

    .line 402
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/hk;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 403
    iget-wide v2, p1, Lcom/google/d/a/a/hk;->c:D

    iget v4, p0, Lcom/google/d/a/a/hm;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/hm;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/hm;->c:D

    .line 405
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/hk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 406
    iget-object v2, p0, Lcom/google/d/a/a/hm;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/hk;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 407
    iget v2, p0, Lcom/google/d/a/a/hm;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/hm;->a:I

    .line 409
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/hk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 410
    iget-object v0, p0, Lcom/google/d/a/a/hm;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/hk;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 411
    iget v0, p0, Lcom/google/d/a/a/hm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/hm;->a:I

    .line 413
    :cond_4
    iget-object v0, p1, Lcom/google/d/a/a/hk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 399
    goto :goto_1

    :cond_6
    move v2, v1

    .line 402
    goto :goto_2

    :cond_7
    move v2, v1

    .line 405
    goto :goto_3

    :cond_8
    move v0, v1

    .line 409
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 342
    new-instance v2, Lcom/google/d/a/a/hk;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/hk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/hm;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-wide v4, p0, Lcom/google/d/a/a/hm;->b:D

    iput-wide v4, v2, Lcom/google/d/a/a/hk;->b:D

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/d/a/a/hm;->c:D

    iput-wide v4, v2, Lcom/google/d/a/a/hk;->c:D

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/d/a/a/hk;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/hm;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/hm;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v3, v2, Lcom/google/d/a/a/hk;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/hm;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/hm;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/hk;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 342
    check-cast p1, Lcom/google/d/a/a/hk;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/hm;->a(Lcom/google/d/a/a/hk;)Lcom/google/d/a/a/hm;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 418
    iget v0, p0, Lcom/google/d/a/a/hm;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/google/d/a/a/hm;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/oe;->d()Lcom/google/d/a/a/oe;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/oe;

    invoke-virtual {v0}, Lcom/google/d/a/a/oe;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 430
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 418
    goto :goto_0

    .line 424
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/hm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 425
    iget-object v0, p0, Lcom/google/d/a/a/hm;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/oe;->d()Lcom/google/d/a/a/oe;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/oe;

    invoke-virtual {v0}, Lcom/google/d/a/a/oe;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 427
    goto :goto_1

    :cond_2
    move v0, v1

    .line 424
    goto :goto_2

    :cond_3
    move v0, v2

    .line 430
    goto :goto_1
.end method
