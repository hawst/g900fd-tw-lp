.class public final Lcom/google/d/a/a/qk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/qn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/qi;",
        "Lcom/google/d/a/a/qk;",
        ">;",
        "Lcom/google/d/a/a/qn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 694
    sget-object v0, Lcom/google/d/a/a/qi;->g:Lcom/google/d/a/a/qi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 799
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    .line 935
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/qk;->c:I

    .line 1036
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    .line 695
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/qi;)Lcom/google/d/a/a/qk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 746
    invoke-static {}, Lcom/google/d/a/a/qi;->d()Lcom/google/d/a/a/qi;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 777
    :goto_0
    return-object p0

    .line 747
    :cond_0
    iget-object v2, p1, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 748
    iget-object v2, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 749
    iget-object v2, p1, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    .line 750
    iget v2, p0, Lcom/google/d/a/a/qk;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/d/a/a/qk;->a:I

    .line 757
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 758
    iget v2, p1, Lcom/google/d/a/a/qi;->c:I

    invoke-static {v2}, Lcom/google/d/a/a/ql;->a(I)Lcom/google/d/a/a/ql;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/d/a/a/ql;->a:Lcom/google/d/a/a/ql;

    :cond_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 752
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/qk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/qk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/qk;->a:I

    .line 753
    :cond_4
    iget-object v2, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 757
    goto :goto_2

    .line 758
    :cond_6
    iget v3, p0, Lcom/google/d/a/a/qk;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/qk;->a:I

    iget v2, v2, Lcom/google/d/a/a/ql;->u:I

    iput v2, p0, Lcom/google/d/a/a/qk;->c:I

    .line 760
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 761
    iget v2, p1, Lcom/google/d/a/a/qi;->d:I

    iget v3, p0, Lcom/google/d/a/a/qk;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/qk;->a:I

    iput v2, p0, Lcom/google/d/a/a/qk;->d:I

    .line 763
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    :goto_4
    if-eqz v0, :cond_9

    .line 764
    iget v0, p1, Lcom/google/d/a/a/qi;->e:I

    iget v1, p0, Lcom/google/d/a/a/qk;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/d/a/a/qk;->a:I

    iput v0, p0, Lcom/google/d/a/a/qk;->e:I

    .line 766
    :cond_9
    iget-object v0, p1, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 767
    iget-object v0, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 768
    iget-object v0, p1, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    .line 769
    iget v0, p0, Lcom/google/d/a/a/qk;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/d/a/a/qk;->a:I

    .line 776
    :cond_a
    :goto_5
    iget-object v0, p1, Lcom/google/d/a/a/qi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 760
    goto :goto_3

    :cond_c
    move v0, v1

    .line 763
    goto :goto_4

    .line 771
    :cond_d
    iget v0, p0, Lcom/google/d/a/a/qk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_e

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/qk;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/qk;->a:I

    .line 772
    :cond_e
    iget-object v0, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 686
    new-instance v2, Lcom/google/d/a/a/qi;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/qi;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/qk;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/d/a/a/qk;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/qk;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/qk;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/qk;->c:I

    iput v1, v2, Lcom/google/d/a/a/qi;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lcom/google/d/a/a/qk;->d:I

    iput v1, v2, Lcom/google/d/a/a/qi;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/qk;->e:I

    iput v1, v2, Lcom/google/d/a/a/qi;->e:I

    iget v1, p0, Lcom/google/d/a/a/qk;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qk;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/d/a/a/qk;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    iput v0, v2, Lcom/google/d/a/a/qi;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 686
    check-cast p1, Lcom/google/d/a/a/qi;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/qk;->a(Lcom/google/d/a/a/qi;)Lcom/google/d/a/a/qk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 781
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 782
    iget-object v0, p0, Lcom/google/d/a/a/qk;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 793
    :cond_0
    :goto_1
    return v2

    .line 781
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 787
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 788
    iget-object v0, p0, Lcom/google/d/a/a/qk;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 793
    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method
