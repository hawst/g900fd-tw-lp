.class public final Lcom/google/d/a/a/ay;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/az;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/au;",
        "Lcom/google/d/a/a/ay;",
        ">;",
        "Lcom/google/d/a/a/az;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 542
    sget-object v0, Lcom/google/d/a/a/au;->g:Lcom/google/d/a/a/au;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 680
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/ay;->c:I

    .line 716
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ay;->d:Lcom/google/n/ao;

    .line 775
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ay;->e:Lcom/google/n/ao;

    .line 835
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    .line 543
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/au;)Lcom/google/d/a/a/ay;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 597
    invoke-static {}, Lcom/google/d/a/a/au;->d()Lcom/google/d/a/a/au;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 623
    :goto_0
    return-object p0

    .line 598
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 599
    iget v2, p1, Lcom/google/d/a/a/au;->b:I

    iget v3, p0, Lcom/google/d/a/a/ay;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/ay;->a:I

    iput v2, p0, Lcom/google/d/a/a/ay;->b:I

    .line 601
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 602
    iget v2, p1, Lcom/google/d/a/a/au;->c:I

    invoke-static {v2}, Lcom/google/d/a/a/aw;->a(I)Lcom/google/d/a/a/aw;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/d/a/a/aw;->a:Lcom/google/d/a/a/aw;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 598
    goto :goto_1

    :cond_4
    move v2, v1

    .line 601
    goto :goto_2

    .line 602
    :cond_5
    iget v3, p0, Lcom/google/d/a/a/ay;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/ay;->a:I

    iget v2, v2, Lcom/google/d/a/a/aw;->h:I

    iput v2, p0, Lcom/google/d/a/a/ay;->c:I

    .line 604
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 605
    iget-object v2, p0, Lcom/google/d/a/a/ay;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 606
    iget v2, p0, Lcom/google/d/a/a/ay;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/ay;->a:I

    .line 608
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/au;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_4
    if-eqz v0, :cond_8

    .line 609
    iget-object v0, p0, Lcom/google/d/a/a/ay;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 610
    iget v0, p0, Lcom/google/d/a/a/ay;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/ay;->a:I

    .line 612
    :cond_8
    iget-object v0, p1, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 613
    iget-object v0, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 614
    iget-object v0, p1, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    .line 615
    iget v0, p0, Lcom/google/d/a/a/ay;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/d/a/a/ay;->a:I

    .line 622
    :cond_9
    :goto_5
    iget-object v0, p1, Lcom/google/d/a/a/au;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 604
    goto :goto_3

    :cond_b
    move v0, v1

    .line 608
    goto :goto_4

    .line 617
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/ay;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/ay;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/ay;->a:I

    .line 618
    :cond_d
    iget-object v0, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 534
    new-instance v2, Lcom/google/d/a/a/au;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/au;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ay;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/ay;->b:I

    iput v4, v2, Lcom/google/d/a/a/au;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/d/a/a/ay;->c:I

    iput v4, v2, Lcom/google/d/a/a/au;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/d/a/a/au;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/ay;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/ay;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v3, v2, Lcom/google/d/a/a/au;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/ay;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/ay;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/d/a/a/ay;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/ay;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/d/a/a/ay;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/ay;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/au;->f:Ljava/util/List;

    iput v0, v2, Lcom/google/d/a/a/au;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 534
    check-cast p1, Lcom/google/d/a/a/au;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ay;->a(Lcom/google/d/a/a/au;)Lcom/google/d/a/a/ay;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 627
    iget v0, p0, Lcom/google/d/a/a/ay;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 643
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 627
    goto :goto_0

    .line 631
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ay;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 632
    iget-object v0, p0, Lcom/google/d/a/a/ay;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 634
    goto :goto_1

    :cond_2
    move v0, v1

    .line 631
    goto :goto_2

    .line 637
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/ay;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 638
    iget-object v0, p0, Lcom/google/d/a/a/ay;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 640
    goto :goto_1

    :cond_4
    move v0, v1

    .line 637
    goto :goto_3

    :cond_5
    move v0, v2

    .line 643
    goto :goto_1
.end method
