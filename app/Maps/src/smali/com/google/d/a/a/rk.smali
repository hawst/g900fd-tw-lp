.class public final Lcom/google/d/a/a/rk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ri;",
        "Lcom/google/d/a/a/rk;",
        ">;",
        "Lcom/google/d/a/a/rl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/ao;

.field private e:I

.field private f:I

.field private g:Z

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 770
    sget-object v0, Lcom/google/d/a/a/ri;->o:Lcom/google/d/a/a/ri;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 956
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    .line 1093
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    .line 1229
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rk;->d:Lcom/google/n/ao;

    .line 1416
    iput-boolean v1, p0, Lcom/google/d/a/a/rk;->i:Z

    .line 1448
    iput-boolean v1, p0, Lcom/google/d/a/a/rk;->j:Z

    .line 771
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ri;)Lcom/google/d/a/a/rk;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 872
    invoke-static {}, Lcom/google/d/a/a/ri;->d()Lcom/google/d/a/a/ri;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 928
    :goto_0
    return-object p0

    .line 873
    :cond_0
    iget-object v2, p1, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 874
    iget-object v2, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 875
    iget-object v2, p1, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    .line 876
    iget v2, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/d/a/a/rk;->a:I

    .line 883
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 884
    iget-object v2, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 885
    iget-object v2, p1, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    .line 886
    iget v2, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/d/a/a/rk;->a:I

    .line 893
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_12

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 894
    iget-object v2, p0, Lcom/google/d/a/a/rk;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 895
    iget v2, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/rk;->a:I

    .line 897
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_13

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 898
    iget v2, p1, Lcom/google/d/a/a/ri;->e:I

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput v2, p0, Lcom/google/d/a/a/rk;->e:I

    .line 900
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 901
    iget v2, p1, Lcom/google/d/a/a/ri;->f:I

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput v2, p0, Lcom/google/d/a/a/rk;->f:I

    .line 903
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 904
    iget-boolean v2, p1, Lcom/google/d/a/a/ri;->g:Z

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/rk;->g:Z

    .line 906
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 907
    iget v2, p1, Lcom/google/d/a/a/ri;->h:I

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput v2, p0, Lcom/google/d/a/a/rk;->h:I

    .line 909
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 910
    iget-boolean v2, p1, Lcom/google/d/a/a/ri;->i:Z

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/rk;->i:Z

    .line 912
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 913
    iget-boolean v2, p1, Lcom/google/d/a/a/ri;->j:Z

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/rk;->j:Z

    .line 915
    :cond_9
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 916
    iget-boolean v2, p1, Lcom/google/d/a/a/ri;->k:Z

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/rk;->k:Z

    .line 918
    :cond_a
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 919
    iget-boolean v2, p1, Lcom/google/d/a/a/ri;->l:Z

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/rk;->l:Z

    .line 921
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 922
    iget-boolean v2, p1, Lcom/google/d/a/a/ri;->m:Z

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/d/a/a/rk;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/rk;->m:Z

    .line 924
    :cond_c
    iget v2, p1, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1c

    :goto_d
    if-eqz v0, :cond_d

    .line 925
    iget v0, p1, Lcom/google/d/a/a/ri;->n:I

    iget v1, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/d/a/a/rk;->a:I

    iput v0, p0, Lcom/google/d/a/a/rk;->n:I

    .line 927
    :cond_d
    iget-object v0, p1, Lcom/google/d/a/a/ri;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 878
    :cond_e
    iget v2, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/rk;->a:I

    .line 879
    :cond_f
    iget-object v2, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 888
    :cond_10
    iget v2, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_11

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/rk;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/rk;->a:I

    .line 889
    :cond_11
    iget-object v2, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_12
    move v2, v1

    .line 893
    goto/16 :goto_3

    :cond_13
    move v2, v1

    .line 897
    goto/16 :goto_4

    :cond_14
    move v2, v1

    .line 900
    goto/16 :goto_5

    :cond_15
    move v2, v1

    .line 903
    goto/16 :goto_6

    :cond_16
    move v2, v1

    .line 906
    goto/16 :goto_7

    :cond_17
    move v2, v1

    .line 909
    goto/16 :goto_8

    :cond_18
    move v2, v1

    .line 912
    goto/16 :goto_9

    :cond_19
    move v2, v1

    .line 915
    goto/16 :goto_a

    :cond_1a
    move v2, v1

    .line 918
    goto/16 :goto_b

    :cond_1b
    move v2, v1

    .line 921
    goto/16 :goto_c

    :cond_1c
    move v0, v1

    .line 924
    goto :goto_d
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 762
    new-instance v2, Lcom/google/d/a/a/ri;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ri;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/rk;->a:I

    iget v4, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/rk;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/d/a/a/rk;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_c

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/rk;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/rk;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/rk;->e:I

    iput v1, v2, Lcom/google/d/a/a/ri;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget v1, p0, Lcom/google/d/a/a/rk;->f:I

    iput v1, v2, Lcom/google/d/a/a/ri;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-boolean v1, p0, Lcom/google/d/a/a/rk;->g:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/ri;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget v1, p0, Lcom/google/d/a/a/rk;->h:I

    iput v1, v2, Lcom/google/d/a/a/ri;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-boolean v1, p0, Lcom/google/d/a/a/rk;->i:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/ri;->i:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget-boolean v1, p0, Lcom/google/d/a/a/rk;->j:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/ri;->j:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-boolean v1, p0, Lcom/google/d/a/a/rk;->k:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/ri;->k:Z

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x100

    :cond_9
    iget-boolean v1, p0, Lcom/google/d/a/a/rk;->l:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/ri;->l:Z

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x200

    :cond_a
    iget-boolean v1, p0, Lcom/google/d/a/a/rk;->m:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/ri;->m:Z

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x400

    :cond_b
    iget v1, p0, Lcom/google/d/a/a/rk;->n:I

    iput v1, v2, Lcom/google/d/a/a/ri;->n:I

    iput v0, v2, Lcom/google/d/a/a/ri;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 762
    check-cast p1, Lcom/google/d/a/a/ri;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/rk;->a(Lcom/google/d/a/a/ri;)Lcom/google/d/a/a/rk;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 932
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 933
    iget-object v0, p0, Lcom/google/d/a/a/rk;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 950
    :cond_0
    :goto_1
    return v2

    .line 932
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 938
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 939
    iget-object v0, p0, Lcom/google/d/a/a/rk;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 938
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 944
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/rk;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 945
    iget-object v0, p0, Lcom/google/d/a/a/rk;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 950
    goto :goto_1

    :cond_5
    move v0, v2

    .line 944
    goto :goto_3
.end method
