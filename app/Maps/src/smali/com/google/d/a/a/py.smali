.class public final Lcom/google/d/a/a/py;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/pz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/pw;",
        "Lcom/google/d/a/a/py;",
        ">;",
        "Lcom/google/d/a/a/pz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 439
    sget-object v0, Lcom/google/d/a/a/pw;->d:Lcom/google/d/a/a/pw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 538
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    .line 675
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    .line 812
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    .line 440
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/pw;)Lcom/google/d/a/a/py;
    .locals 2

    .prologue
    .line 478
    invoke-static {}, Lcom/google/d/a/a/pw;->d()Lcom/google/d/a/a/pw;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 510
    :goto_0
    return-object p0

    .line 479
    :cond_0
    iget-object v0, p1, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 481
    iget-object v0, p1, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    .line 482
    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/d/a/a/py;->a:I

    .line 489
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 490
    iget-object v0, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 491
    iget-object v0, p1, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    .line 492
    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/d/a/a/py;->a:I

    .line 499
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 500
    iget-object v0, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 501
    iget-object v0, p1, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    .line 502
    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/d/a/a/py;->a:I

    .line 509
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/d/a/a/pw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 484
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/py;->a:I

    .line 485
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 494
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/py;->a:I

    .line 495
    :cond_7
    iget-object v0, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 504
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/py;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/py;->a:I

    .line 505
    :cond_9
    iget-object v0, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 431
    new-instance v0, Lcom/google/d/a/a/pw;

    invoke-direct {v0, p0}, Lcom/google/d/a/a/pw;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/d/a/a/py;->a:I

    iget v1, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/d/a/a/py;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/d/a/a/py;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    iput-object v1, v0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/py;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/d/a/a/py;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    iput-object v1, v0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 431
    check-cast p1, Lcom/google/d/a/a/pw;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/py;->a(Lcom/google/d/a/a/pw;)Lcom/google/d/a/a/py;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 514
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 515
    iget-object v0, p0, Lcom/google/d/a/a/py;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 532
    :cond_0
    :goto_1
    return v2

    .line 514
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 520
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 521
    iget-object v0, p0, Lcom/google/d/a/a/py;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/nn;->d()Lcom/google/d/a/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nn;

    invoke-virtual {v0}, Lcom/google/d/a/a/nn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 526
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 527
    iget-object v0, p0, Lcom/google/d/a/a/py;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 532
    :cond_4
    const/4 v2, 0x1

    goto :goto_1
.end method
