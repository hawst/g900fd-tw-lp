.class public final Lcom/google/d/a/a/rs;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/sa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/rs;",
            ">;"
        }
    .end annotation
.end field

.field static final r:Lcom/google/d/a/a/rs;

.field private static final serialVersionUID:J

.field private static volatile u:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/f;

.field d:Lcom/google/n/f;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Ljava/lang/Object;

.field h:I

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field l:I

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field n:I

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field q:I

.field private s:B

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 425
    new-instance v0, Lcom/google/d/a/a/rt;

    invoke-direct {v0}, Lcom/google/d/a/a/rt;-><init>()V

    sput-object v0, Lcom/google/d/a/a/rs;->PARSER:Lcom/google/n/ax;

    .line 749
    new-instance v0, Lcom/google/d/a/a/ru;

    invoke-direct {v0}, Lcom/google/d/a/a/ru;-><init>()V

    .line 1210
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/rs;->u:Lcom/google/n/aw;

    .line 2713
    new-instance v0, Lcom/google/d/a/a/rs;

    invoke-direct {v0}, Lcom/google/d/a/a/rs;-><init>()V

    sput-object v0, Lcom/google/d/a/a/rs;->r:Lcom/google/d/a/a/rs;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 203
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 612
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->b:Lcom/google/n/ao;

    .line 658
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->e:Lcom/google/n/ao;

    .line 674
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->f:Lcom/google/n/ao;

    .line 1017
    iput-byte v4, p0, Lcom/google/d/a/a/rs;->s:B

    .line 1123
    iput v4, p0, Lcom/google/d/a/a/rs;->t:I

    .line 204
    iget-object v0, p0, Lcom/google/d/a/a/rs;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 205
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/d/a/a/rs;->c:Lcom/google/n/f;

    .line 206
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/d/a/a/rs;->d:Lcom/google/n/f;

    .line 207
    iget-object v0, p0, Lcom/google/d/a/a/rs;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 208
    iget-object v0, p0, Lcom/google/d/a/a/rs;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 209
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/rs;->g:Ljava/lang/Object;

    .line 210
    iput v3, p0, Lcom/google/d/a/a/rs;->h:I

    .line 211
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    .line 212
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    .line 213
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    .line 214
    iput v3, p0, Lcom/google/d/a/a/rs;->l:I

    .line 215
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    .line 216
    iput v3, p0, Lcom/google/d/a/a/rs;->n:I

    .line 217
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    .line 218
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    .line 219
    iput v2, p0, Lcom/google/d/a/a/rs;->q:I

    .line 220
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/16 v9, 0x100

    const/4 v2, -0x1

    const/16 v8, 0x800

    const/16 v7, 0x80

    const/4 v0, 0x0

    .line 226
    invoke-direct {p0}, Lcom/google/d/a/a/rs;-><init>()V

    .line 229
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v0

    move v1, v0

    .line 232
    :cond_0
    :goto_0
    if-nez v3, :cond_17

    .line 233
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 234
    sparse-switch v0, :sswitch_data_0

    .line 239
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 236
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 237
    goto :goto_0

    .line 246
    :sswitch_1
    iget-object v0, p0, Lcom/google/d/a/a/rs;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 247
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/rs;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 398
    :catch_0
    move-exception v0

    .line 399
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404
    :catchall_0
    move-exception v0

    and-int/lit16 v2, v1, 0x80

    if-ne v2, v7, :cond_1

    .line 405
    iget-object v2, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    .line 407
    :cond_1
    and-int/lit16 v2, v1, 0x100

    if-ne v2, v9, :cond_2

    .line 408
    iget-object v2, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    .line 410
    :cond_2
    and-int/lit16 v2, v1, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_3

    .line 411
    iget-object v2, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    .line 413
    :cond_3
    and-int/lit16 v2, v1, 0x800

    if-ne v2, v8, :cond_4

    .line 414
    iget-object v2, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    .line 416
    :cond_4
    and-int/lit16 v2, v1, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_5

    .line 417
    iget-object v2, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    .line 419
    :cond_5
    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_6

    .line 420
    iget-object v1, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    .line 422
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/rs;->au:Lcom/google/n/bn;

    throw v0

    .line 251
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/rs;->a:I

    .line 252
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->c:Lcom/google/n/f;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 400
    :catch_1
    move-exception v0

    .line 401
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 402
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 256
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/d/a/a/rs;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 257
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/rs;->a:I

    goto/16 :goto_0

    .line 261
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 262
    invoke-static {v0}, Lcom/google/d/a/a/ry;->a(I)Lcom/google/d/a/a/ry;

    move-result-object v5

    .line 263
    if-nez v5, :cond_7

    .line 264
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 266
    :cond_7
    and-int/lit16 v5, v1, 0x80

    if-eq v5, v7, :cond_8

    .line 267
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    .line 268
    or-int/lit16 v1, v1, 0x80

    .line 270
    :cond_8
    iget-object v5, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 275
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 276
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 277
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_9

    move v0, v2

    :goto_2
    if-lez v0, :cond_c

    .line 278
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 279
    invoke-static {v0}, Lcom/google/d/a/a/ry;->a(I)Lcom/google/d/a/a/ry;

    move-result-object v6

    .line 280
    if-nez v6, :cond_a

    .line 281
    const/4 v6, 0x5

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 277
    :cond_9
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_2

    .line 283
    :cond_a
    and-int/lit16 v6, v1, 0x80

    if-eq v6, v7, :cond_b

    .line 284
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    .line 285
    or-int/lit16 v1, v1, 0x80

    .line 287
    :cond_b
    iget-object v6, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 290
    :cond_c
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 294
    :sswitch_6
    and-int/lit16 v0, v1, 0x100

    if-eq v0, v9, :cond_d

    .line 295
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    .line 297
    or-int/lit16 v1, v1, 0x100

    .line 299
    :cond_d
    iget-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 300
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 299
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 304
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 305
    iget v5, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/d/a/a/rs;->a:I

    .line 306
    iput-object v0, p0, Lcom/google/d/a/a/rs;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 310
    :sswitch_8
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/rs;->a:I

    .line 311
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/rs;->h:I

    goto/16 :goto_0

    .line 315
    :sswitch_9
    and-int/lit16 v0, v1, 0x200

    const/16 v5, 0x200

    if-eq v0, v5, :cond_e

    .line 316
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    .line 318
    or-int/lit16 v1, v1, 0x200

    .line 320
    :cond_e
    iget-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 321
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 320
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 325
    :sswitch_a
    and-int/lit16 v0, v1, 0x800

    if-eq v0, v8, :cond_f

    .line 326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    .line 327
    or-int/lit16 v1, v1, 0x800

    .line 329
    :cond_f
    iget-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 333
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 334
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 335
    and-int/lit16 v0, v1, 0x800

    if-eq v0, v8, :cond_10

    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_11

    move v0, v2

    :goto_3
    if-lez v0, :cond_10

    .line 336
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    .line 337
    or-int/lit16 v1, v1, 0x800

    .line 339
    :cond_10
    :goto_4
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_12

    move v0, v2

    :goto_5
    if-lez v0, :cond_13

    .line 340
    iget-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 335
    :cond_11
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_3

    .line 339
    :cond_12
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_5

    .line 342
    :cond_13
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 346
    :sswitch_c
    iget-object v0, p0, Lcom/google/d/a/a/rs;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 347
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/rs;->a:I

    goto/16 :goto_0

    .line 351
    :sswitch_d
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/rs;->a:I

    .line 352
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/rs;->n:I

    goto/16 :goto_0

    .line 356
    :sswitch_e
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/rs;->a:I

    .line 357
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/rs;->l:I

    goto/16 :goto_0

    .line 361
    :sswitch_f
    and-int/lit16 v0, v1, 0x2000

    const/16 v5, 0x2000

    if-eq v0, v5, :cond_14

    .line 362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    .line 364
    or-int/lit16 v1, v1, 0x2000

    .line 366
    :cond_14
    iget-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 367
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 366
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 371
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 372
    invoke-static {v0}, Lcom/google/d/a/a/rv;->a(I)Lcom/google/d/a/a/rv;

    move-result-object v5

    .line 373
    if-nez v5, :cond_15

    .line 374
    const/16 v5, 0x11

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 376
    :cond_15
    iget v5, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/d/a/a/rs;->a:I

    .line 377
    iput v0, p0, Lcom/google/d/a/a/rs;->q:I

    goto/16 :goto_0

    .line 382
    :sswitch_11
    and-int/lit16 v0, v1, 0x4000

    const/16 v5, 0x4000

    if-eq v0, v5, :cond_16

    .line 383
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    .line 385
    or-int/lit16 v1, v1, 0x4000

    .line 387
    :cond_16
    iget-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 388
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 387
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 392
    :sswitch_12
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/rs;->a:I

    .line 393
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->d:Lcom/google/n/f;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 404
    :cond_17
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v7, :cond_18

    .line 405
    iget-object v0, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    .line 407
    :cond_18
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v9, :cond_19

    .line 408
    iget-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    .line 410
    :cond_19
    and-int/lit16 v0, v1, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_1a

    .line 411
    iget-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    .line 413
    :cond_1a
    and-int/lit16 v0, v1, 0x800

    if-ne v0, v8, :cond_1b

    .line 414
    iget-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    .line 416
    :cond_1b
    and-int/lit16 v0, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_1c

    .line 417
    iget-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    .line 419
    :cond_1c
    and-int/lit16 v0, v1, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_1d

    .line 420
    iget-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    .line 422
    :cond_1d
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->au:Lcom/google/n/bn;

    .line 423
    return-void

    .line 234
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x60 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x82 -> :sswitch_f
        0x88 -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 201
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 612
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->b:Lcom/google/n/ao;

    .line 658
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->e:Lcom/google/n/ao;

    .line 674
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rs;->f:Lcom/google/n/ao;

    .line 1017
    iput-byte v1, p0, Lcom/google/d/a/a/rs;->s:B

    .line 1123
    iput v1, p0, Lcom/google/d/a/a/rs;->t:I

    .line 202
    return-void
.end method

.method public static d()Lcom/google/d/a/a/rs;
    .locals 1

    .prologue
    .line 2716
    sget-object v0, Lcom/google/d/a/a/rs;->r:Lcom/google/d/a/a/rs;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/rx;
    .locals 1

    .prologue
    .line 1272
    new-instance v0, Lcom/google/d/a/a/rx;

    invoke-direct {v0}, Lcom/google/d/a/a/rx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/rs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 437
    sget-object v0, Lcom/google/d/a/a/rs;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 1071
    invoke-virtual {p0}, Lcom/google/d/a/a/rs;->c()I

    .line 1072
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1073
    iget-object v0, p0, Lcom/google/d/a/a/rs;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1075
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 1076
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/d/a/a/rs;->c:Lcom/google/n/f;

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1078
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 1079
    iget-object v0, p0, Lcom/google/d/a/a/rs;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_2
    move v1, v2

    .line 1081
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1082
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1081
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1082
    :cond_3
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_4
    move v1, v2

    .line 1084
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1085
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1084
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1087
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 1088
    iget-object v0, p0, Lcom/google/d/a/a/rs;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->g:Ljava/lang/Object;

    :goto_3
    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1090
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 1091
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/d/a/a/rs;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_7
    :goto_4
    move v1, v2

    .line 1093
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 1094
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1093
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1088
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 1091
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    :cond_a
    move v1, v2

    .line 1096
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 1097
    const/16 v3, 0xc

    iget-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1096
    :goto_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1097
    :cond_b
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_7

    .line 1099
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_d

    .line 1100
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/d/a/a/rs;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1102
    :cond_d
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_e

    .line 1103
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/d/a/a/rs;->n:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_10

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1105
    :cond_e
    :goto_8
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_f

    .line 1106
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/d/a/a/rs;->l:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_11

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_f
    :goto_9
    move v1, v2

    .line 1108
    :goto_a
    iget-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 1109
    const/16 v3, 0x10

    iget-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1108
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 1103
    :cond_10
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_8

    .line 1106
    :cond_11
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_9

    .line 1111
    :cond_12
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_13

    .line 1112
    const/16 v0, 0x11

    iget v1, p0, Lcom/google/d/a/a/rs;->q:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_14

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1114
    :cond_13
    :goto_b
    iget-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_15

    .line 1115
    const/16 v1, 0x12

    iget-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1114
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 1112
    :cond_14
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_b

    .line 1117
    :cond_15
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_16

    .line 1118
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/d/a/a/rs;->d:Lcom/google/n/f;

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1120
    :cond_16
    iget-object v0, p0, Lcom/google/d/a/a/rs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1121
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1019
    iget-byte v0, p0, Lcom/google/d/a/a/rs;->s:B

    .line 1020
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1066
    :cond_0
    :goto_0
    return v2

    .line 1021
    :cond_1
    if-eqz v0, :cond_0

    .line 1023
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    .line 1024
    iput-byte v2, p0, Lcom/google/d/a/a/rs;->s:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1023
    goto :goto_1

    .line 1027
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-nez v0, :cond_5

    .line 1028
    iput-byte v2, p0, Lcom/google/d/a/a/rs;->s:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1027
    goto :goto_2

    .line 1031
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/rs;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1032
    iput-byte v2, p0, Lcom/google/d/a/a/rs;->s:B

    goto :goto_0

    .line 1035
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    move v0, v3

    :goto_3
    if-eqz v0, :cond_8

    .line 1036
    iget-object v0, p0, Lcom/google/d/a/a/rs;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1037
    iput-byte v2, p0, Lcom/google/d/a/a/rs;->s:B

    goto :goto_0

    :cond_7
    move v0, v2

    .line 1035
    goto :goto_3

    .line 1041
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_4
    if-eqz v0, :cond_a

    .line 1042
    iget-object v0, p0, Lcom/google/d/a/a/rs;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1043
    iput-byte v2, p0, Lcom/google/d/a/a/rs;->s:B

    goto :goto_0

    :cond_9
    move v0, v2

    .line 1041
    goto :goto_4

    :cond_a
    move v1, v2

    .line 1047
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 1048
    iget-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/qp;->d()Lcom/google/d/a/a/qp;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/qp;

    invoke-virtual {v0}, Lcom/google/d/a/a/qp;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1049
    iput-byte v2, p0, Lcom/google/d/a/a/rs;->s:B

    goto/16 :goto_0

    .line 1047
    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_c
    move v1, v2

    .line 1053
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 1054
    iget-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1055
    iput-byte v2, p0, Lcom/google/d/a/a/rs;->s:B

    goto/16 :goto_0

    .line 1053
    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_e
    move v1, v2

    .line 1059
    :goto_7
    iget-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 1060
    iget-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ri;->d()Lcom/google/d/a/a/ri;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ri;

    invoke-virtual {v0}, Lcom/google/d/a/a/ri;->b()Z

    move-result v0

    if-nez v0, :cond_f

    .line 1061
    iput-byte v2, p0, Lcom/google/d/a/a/rs;->s:B

    goto/16 :goto_0

    .line 1059
    :cond_f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1065
    :cond_10
    iput-byte v3, p0, Lcom/google/d/a/a/rs;->s:B

    move v2, v3

    .line 1066
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/16 v5, 0xa

    const/4 v1, 0x0

    .line 1125
    iget v0, p0, Lcom/google/d/a/a/rs;->t:I

    .line 1126
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1205
    :goto_0
    return v0

    .line 1129
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_17

    .line 1130
    iget-object v0, p0, Lcom/google/d/a/a/rs;->b:Lcom/google/n/ao;

    .line 1131
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1133
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1134
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/d/a/a/rs;->c:Lcom/google/n/f;

    .line 1135
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1137
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_16

    .line 1138
    iget-object v2, p0, Lcom/google/d/a/a/rs;->f:Lcom/google/n/ao;

    .line 1139
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    :goto_2
    move v3, v1

    move v4, v1

    .line 1143
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1144
    iget-object v0, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    .line 1145
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v4, v0

    .line 1143
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_2
    move v0, v5

    .line 1145
    goto :goto_4

    .line 1147
    :cond_3
    add-int v0, v2, v4

    .line 1148
    iget-object v2, p0, Lcom/google/d/a/a/rs;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v1

    move v3, v0

    .line 1150
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1151
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/rs;->j:Ljava/util/List;

    .line 1152
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1150
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1154
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 1156
    iget-object v0, p0, Lcom/google/d/a/a/rs;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rs;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1158
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 1159
    const/16 v0, 0x9

    iget v2, p0, Lcom/google/d/a/a/rs;->h:I

    .line 1160
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_8

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    :cond_6
    move v2, v1

    .line 1162
    :goto_8
    iget-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 1163
    iget-object v0, p0, Lcom/google/d/a/a/rs;->k:Ljava/util/List;

    .line 1164
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1162
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 1156
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_8
    move v0, v5

    .line 1160
    goto :goto_7

    :cond_9
    move v2, v1

    move v4, v1

    .line 1168
    :goto_9
    iget-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 1169
    iget-object v0, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    .line 1170
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v4, v0

    .line 1168
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    :cond_a
    move v0, v5

    .line 1170
    goto :goto_a

    .line 1172
    :cond_b
    add-int v0, v3, v4

    .line 1173
    iget-object v2, p0, Lcom/google/d/a/a/rs;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1175
    iget v2, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v7, :cond_c

    .line 1176
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/d/a/a/rs;->e:Lcom/google/n/ao;

    .line 1177
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1179
    :cond_c
    iget v2, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_d

    .line 1180
    const/16 v2, 0xe

    iget v3, p0, Lcom/google/d/a/a/rs;->n:I

    .line 1181
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_f

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_b
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1183
    :cond_d
    iget v2, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_e

    .line 1184
    const/16 v2, 0xf

    iget v3, p0, Lcom/google/d/a/a/rs;->l:I

    .line 1185
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_10

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_c
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    :cond_e
    move v2, v1

    move v3, v0

    .line 1187
    :goto_d
    iget-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_11

    .line 1188
    const/16 v4, 0x10

    iget-object v0, p0, Lcom/google/d/a/a/rs;->o:Ljava/util/List;

    .line 1189
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1187
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    :cond_f
    move v2, v5

    .line 1181
    goto :goto_b

    :cond_10
    move v2, v5

    .line 1185
    goto :goto_c

    .line 1191
    :cond_11
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_13

    .line 1192
    const/16 v0, 0x11

    iget v2, p0, Lcom/google/d/a/a/rs;->q:I

    .line 1193
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v2, :cond_12

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    :cond_12
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    :cond_13
    move v2, v1

    .line 1195
    :goto_e
    iget-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_14

    .line 1196
    const/16 v4, 0x12

    iget-object v0, p0, Lcom/google/d/a/a/rs;->p:Ljava/util/List;

    .line 1197
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1195
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_e

    .line 1199
    :cond_14
    iget v0, p0, Lcom/google/d/a/a/rs;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_15

    .line 1200
    const/16 v0, 0x13

    iget-object v2, p0, Lcom/google/d/a/a/rs;->d:Lcom/google/n/f;

    .line 1201
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1203
    :cond_15
    iget-object v0, p0, Lcom/google/d/a/a/rs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1204
    iput v0, p0, Lcom/google/d/a/a/rs;->t:I

    goto/16 :goto_0

    :cond_16
    move v2, v0

    goto/16 :goto_2

    :cond_17
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 195
    invoke-static {}, Lcom/google/d/a/a/rs;->newBuilder()Lcom/google/d/a/a/rx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/rx;->a(Lcom/google/d/a/a/rs;)Lcom/google/d/a/a/rx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 195
    invoke-static {}, Lcom/google/d/a/a/rs;->newBuilder()Lcom/google/d/a/a/rx;

    move-result-object v0

    return-object v0
.end method
