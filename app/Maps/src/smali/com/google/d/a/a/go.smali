.class public final Lcom/google/d/a/a/go;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/gr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/go;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/a/a/go;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:F

.field d:Z

.field e:F

.field f:F

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/d/a/a/gp;

    invoke-direct {v0}, Lcom/google/d/a/a/gp;-><init>()V

    sput-object v0, Lcom/google/d/a/a/go;->PARSER:Lcom/google/n/ax;

    .line 330
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/go;->j:Lcom/google/n/aw;

    .line 700
    new-instance v0, Lcom/google/d/a/a/go;

    invoke-direct {v0}, Lcom/google/d/a/a/go;-><init>()V

    sput-object v0, Lcom/google/d/a/a/go;->g:Lcom/google/d/a/a/go;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 262
    iput-byte v0, p0, Lcom/google/d/a/a/go;->h:B

    .line 297
    iput v0, p0, Lcom/google/d/a/a/go;->i:I

    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/go;->b:Ljava/lang/Object;

    .line 78
    iput v1, p0, Lcom/google/d/a/a/go;->c:F

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/d/a/a/go;->d:Z

    .line 80
    iput v1, p0, Lcom/google/d/a/a/go;->e:F

    .line 81
    iput v1, p0, Lcom/google/d/a/a/go;->f:F

    .line 82
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 88
    invoke-direct {p0}, Lcom/google/d/a/a/go;-><init>()V

    .line 89
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 94
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 95
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 96
    sparse-switch v0, :sswitch_data_0

    .line 101
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 103
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 99
    goto :goto_0

    .line 108
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 109
    iget v5, p0, Lcom/google/d/a/a/go;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/go;->a:I

    .line 110
    iput-object v0, p0, Lcom/google/d/a/a/go;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    .line 136
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/go;->au:Lcom/google/n/bn;

    throw v0

    .line 114
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/go;->a:I

    .line 115
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/go;->c:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 137
    :catch_1
    move-exception v0

    .line 138
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 139
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 119
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/go;->a:I

    .line 120
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/go;->d:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 124
    :sswitch_4
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/go;->a:I

    .line 125
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/go;->e:F

    goto :goto_0

    .line 129
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/go;->a:I

    .line 130
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/go;->f:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 141
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/go;->au:Lcom/google/n/bn;

    .line 142
    return-void

    .line 96
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 74
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 262
    iput-byte v0, p0, Lcom/google/d/a/a/go;->h:B

    .line 297
    iput v0, p0, Lcom/google/d/a/a/go;->i:I

    .line 75
    return-void
.end method

.method public static d()Lcom/google/d/a/a/go;
    .locals 1

    .prologue
    .line 703
    sget-object v0, Lcom/google/d/a/a/go;->g:Lcom/google/d/a/a/go;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/gq;
    .locals 1

    .prologue
    .line 392
    new-instance v0, Lcom/google/d/a/a/gq;

    invoke-direct {v0}, Lcom/google/d/a/a/gq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/go;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    sget-object v0, Lcom/google/d/a/a/go;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v4, 0x5

    .line 278
    invoke-virtual {p0}, Lcom/google/d/a/a/go;->c()I

    .line 279
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 280
    iget-object v0, p0, Lcom/google/d/a/a/go;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/go;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 282
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 283
    iget v0, p0, Lcom/google/d/a/a/go;->c:F

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 285
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 286
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/d/a/a/go;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 288
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 289
    iget v0, p0, Lcom/google/d/a/a/go;->e:F

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 291
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 292
    iget v0, p0, Lcom/google/d/a/a/go;->f:F

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 294
    :cond_4
    iget-object v0, p0, Lcom/google/d/a/a/go;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 295
    return-void

    .line 280
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    :cond_6
    move v0, v2

    .line 286
    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 264
    iget-byte v2, p0, Lcom/google/d/a/a/go;->h:B

    .line 265
    if-ne v2, v0, :cond_0

    .line 273
    :goto_0
    return v0

    .line 266
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 268
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 269
    iput-byte v1, p0, Lcom/google/d/a/a/go;->h:B

    move v0, v1

    .line 270
    goto :goto_0

    :cond_2
    move v2, v1

    .line 268
    goto :goto_1

    .line 272
    :cond_3
    iput-byte v0, p0, Lcom/google/d/a/a/go;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 299
    iget v0, p0, Lcom/google/d/a/a/go;->i:I

    .line 300
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 325
    :goto_0
    return v0

    .line 303
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 305
    iget-object v0, p0, Lcom/google/d/a/a/go;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/go;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 307
    :goto_2
    iget v2, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 308
    iget v2, p0, Lcom/google/d/a/a/go;->c:F

    .line 309
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 311
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 312
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/d/a/a/go;->d:Z

    .line 313
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 315
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 316
    iget v2, p0, Lcom/google/d/a/a/go;->e:F

    .line 317
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 319
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 320
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/d/a/a/go;->f:F

    .line 321
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 323
    :cond_4
    iget-object v1, p0, Lcom/google/d/a/a/go;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 324
    iput v0, p0, Lcom/google/d/a/a/go;->i:I

    goto/16 :goto_0

    .line 305
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/a/a/go;->newBuilder()Lcom/google/d/a/a/gq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/gq;->a(Lcom/google/d/a/a/go;)Lcom/google/d/a/a/gq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/a/a/go;->newBuilder()Lcom/google/d/a/a/gq;

    move-result-object v0

    return-object v0
.end method
