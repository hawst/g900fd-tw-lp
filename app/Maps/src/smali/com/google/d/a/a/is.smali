.class public final Lcom/google/d/a/a/is;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/iz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/is;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/d/a/a/is;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Z

.field e:I

.field f:F

.field g:F

.field h:F

.field i:F

.field j:F

.field k:F

.field l:F

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/google/d/a/a/it;

    invoke-direct {v0}, Lcom/google/d/a/a/it;-><init>()V

    sput-object v0, Lcom/google/d/a/a/is;->PARSER:Lcom/google/n/ax;

    .line 1007
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/is;->p:Lcom/google/n/aw;

    .line 1700
    new-instance v0, Lcom/google/d/a/a/is;

    invoke-direct {v0}, Lcom/google/d/a/a/is;-><init>()V

    sput-object v0, Lcom/google/d/a/a/is;->m:Lcom/google/d/a/a/is;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 130
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 895
    iput-byte v0, p0, Lcom/google/d/a/a/is;->n:B

    .line 950
    iput v0, p0, Lcom/google/d/a/a/is;->o:I

    .line 131
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    .line 132
    iput v2, p0, Lcom/google/d/a/a/is;->c:I

    .line 133
    iput-boolean v2, p0, Lcom/google/d/a/a/is;->d:Z

    .line 134
    iput v2, p0, Lcom/google/d/a/a/is;->e:I

    .line 135
    iput v1, p0, Lcom/google/d/a/a/is;->f:F

    .line 136
    iput v1, p0, Lcom/google/d/a/a/is;->g:F

    .line 137
    iput v1, p0, Lcom/google/d/a/a/is;->h:F

    .line 138
    iput v1, p0, Lcom/google/d/a/a/is;->i:F

    .line 139
    iput v1, p0, Lcom/google/d/a/a/is;->j:F

    .line 140
    iput v1, p0, Lcom/google/d/a/a/is;->k:F

    .line 141
    iput v1, p0, Lcom/google/d/a/a/is;->l:F

    .line 142
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 148
    invoke-direct {p0}, Lcom/google/d/a/a/is;-><init>()V

    .line 151
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 154
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 155
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 156
    sparse-switch v0, :sswitch_data_0

    .line 161
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 163
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 159
    goto :goto_0

    .line 168
    :sswitch_1
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/is;->a:I

    .line 169
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/is;->f:F
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 248
    iget-object v1, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    .line 250
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/is;->au:Lcom/google/n/bn;

    throw v0

    .line 173
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/is;->a:I

    .line 174
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/is;->h:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 243
    :catch_1
    move-exception v0

    .line 244
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 245
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 178
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/is;->a:I

    .line 179
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/is;->g:F

    goto :goto_0

    .line 183
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 184
    invoke-static {v0}, Lcom/google/d/a/a/ix;->a(I)Lcom/google/d/a/a/ix;

    move-result-object v6

    .line 185
    if-nez v6, :cond_2

    .line 186
    const/16 v6, 0x8

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 188
    :cond_2
    iget v6, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/d/a/a/is;->a:I

    .line 189
    iput v0, p0, Lcom/google/d/a/a/is;->e:I

    goto/16 :goto_0

    .line 194
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/is;->a:I

    .line 195
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/is;->i:F

    goto/16 :goto_0

    .line 199
    :sswitch_6
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/is;->a:I

    .line 200
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/is;->j:F

    goto/16 :goto_0

    .line 204
    :sswitch_7
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/is;->a:I

    .line 205
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/is;->k:F

    goto/16 :goto_0

    .line 209
    :sswitch_8
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/d/a/a/is;->a:I

    .line 210
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/is;->l:F

    goto/16 :goto_0

    .line 214
    :sswitch_9
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v2, :cond_3

    .line 215
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    .line 217
    or-int/lit8 v1, v1, 0x1

    .line 219
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 220
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 219
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 224
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 225
    invoke-static {v0}, Lcom/google/d/a/a/iv;->a(I)Lcom/google/d/a/a/iv;

    move-result-object v6

    .line 226
    if-nez v6, :cond_4

    .line 227
    const/16 v6, 0x10

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 229
    :cond_4
    iget v6, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/d/a/a/is;->a:I

    .line 230
    iput v0, p0, Lcom/google/d/a/a/is;->c:I

    goto/16 :goto_0

    .line 235
    :sswitch_b
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/is;->a:I

    .line 236
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/is;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    .line 247
    :cond_6
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_7

    .line 248
    iget-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    .line 250
    :cond_7
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/is;->au:Lcom/google/n/bn;

    .line 251
    return-void

    .line 156
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x2d -> :sswitch_2
        0x3d -> :sswitch_3
        0x40 -> :sswitch_4
        0x4d -> :sswitch_5
        0x55 -> :sswitch_6
        0x5d -> :sswitch_7
        0x65 -> :sswitch_8
        0x6a -> :sswitch_9
        0x80 -> :sswitch_a
        0x88 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 128
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 895
    iput-byte v0, p0, Lcom/google/d/a/a/is;->n:B

    .line 950
    iput v0, p0, Lcom/google/d/a/a/is;->o:I

    .line 129
    return-void
.end method

.method public static d()Lcom/google/d/a/a/is;
    .locals 1

    .prologue
    .line 1703
    sget-object v0, Lcom/google/d/a/a/is;->m:Lcom/google/d/a/a/is;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/iu;
    .locals 1

    .prologue
    .line 1069
    new-instance v0, Lcom/google/d/a/a/iu;

    invoke-direct {v0}, Lcom/google/d/a/a/iu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/is;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    sget-object v0, Lcom/google/d/a/a/is;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x5

    .line 913
    invoke-virtual {p0}, Lcom/google/d/a/a/is;->c()I

    .line 914
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_0

    .line 915
    iget v0, p0, Lcom/google/d/a/a/is;->f:F

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 917
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 918
    iget v0, p0, Lcom/google/d/a/a/is;->h:F

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 920
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 921
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/d/a/a/is;->g:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 923
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 924
    iget v0, p0, Lcom/google/d/a/a/is;->e:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 926
    :cond_3
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    .line 927
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/d/a/a/is;->i:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 929
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 930
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/d/a/a/is;->j:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 932
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_6

    .line 933
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/d/a/a/is;->k:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 935
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_7

    .line 936
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/d/a/a/is;->l:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    :cond_7
    move v1, v2

    .line 938
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 939
    const/16 v4, 0xd

    iget-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 938
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 924
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 941
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 942
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/d/a/a/is;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_d

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 944
    :cond_a
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_c

    .line 945
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/d/a/a/is;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_b

    move v2, v3

    :cond_b
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 947
    :cond_c
    iget-object v0, p0, Lcom/google/d/a/a/is;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 948
    return-void

    .line 942
    :cond_d
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 897
    iget-byte v0, p0, Lcom/google/d/a/a/is;->n:B

    .line 898
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 908
    :cond_0
    :goto_0
    return v2

    .line 899
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 901
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 902
    iget-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/jb;->d()Lcom/google/d/a/a/jb;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/jb;

    invoke-virtual {v0}, Lcom/google/d/a/a/jb;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 903
    iput-byte v2, p0, Lcom/google/d/a/a/is;->n:B

    goto :goto_0

    .line 901
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 907
    :cond_3
    iput-byte v3, p0, Lcom/google/d/a/a/is;->n:B

    move v2, v3

    .line 908
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/16 v5, 0x8

    const/4 v6, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 952
    iget v0, p0, Lcom/google/d/a/a/is;->o:I

    .line 953
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1002
    :goto_0
    return v0

    .line 956
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_d

    .line 957
    iget v0, p0, Lcom/google/d/a/a/is;->f:F

    .line 958
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 960
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_1

    .line 961
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/d/a/a/is;->h:F

    .line 962
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 964
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v7, :cond_2

    .line 965
    const/4 v2, 0x7

    iget v4, p0, Lcom/google/d/a/a/is;->g:F

    .line 966
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 968
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_3

    .line 969
    iget v2, p0, Lcom/google/d/a/a/is;->e:I

    .line 970
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_8

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 972
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_4

    .line 973
    const/16 v2, 0x9

    iget v4, p0, Lcom/google/d/a/a/is;->i:F

    .line 974
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 976
    :cond_4
    iget v2, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_5

    .line 977
    iget v2, p0, Lcom/google/d/a/a/is;->j:F

    .line 978
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 980
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v4, 0x100

    if-ne v2, v4, :cond_6

    .line 981
    const/16 v2, 0xb

    iget v4, p0, Lcom/google/d/a/a/is;->k:F

    .line 982
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 984
    :cond_6
    iget v2, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v4, 0x200

    if-ne v2, v4, :cond_7

    .line 985
    const/16 v2, 0xc

    iget v4, p0, Lcom/google/d/a/a/is;->l:F

    .line 986
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    :cond_7
    move v2, v1

    move v4, v0

    .line 988
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 989
    const/16 v5, 0xd

    iget-object v0, p0, Lcom/google/d/a/a/is;->b:Ljava/util/List;

    .line 990
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 988
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_8
    move v2, v3

    .line 970
    goto/16 :goto_2

    .line 992
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_b

    .line 993
    iget v0, p0, Lcom/google/d/a/a/is;->c:I

    .line 994
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_a
    add-int v0, v2, v3

    add-int/2addr v4, v0

    .line 996
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/is;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_c

    .line 997
    const/16 v0, 0x11

    iget-boolean v2, p0, Lcom/google/d/a/a/is;->d:Z

    .line 998
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 1000
    :cond_c
    iget-object v0, p0, Lcom/google/d/a/a/is;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1001
    iput v0, p0, Lcom/google/d/a/a/is;->o:I

    goto/16 :goto_0

    :cond_d
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 122
    invoke-static {}, Lcom/google/d/a/a/is;->newBuilder()Lcom/google/d/a/a/iu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/iu;->a(Lcom/google/d/a/a/is;)Lcom/google/d/a/a/iu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 122
    invoke-static {}, Lcom/google/d/a/a/is;->newBuilder()Lcom/google/d/a/a/iu;

    move-result-object v0

    return-object v0
.end method
