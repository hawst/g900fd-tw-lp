.class public final enum Lcom/google/d/a/a/f;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/f;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/f;

.field public static final enum b:Lcom/google/d/a/a/f;

.field private static final synthetic d:[Lcom/google/d/a/a/f;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 320
    new-instance v0, Lcom/google/d/a/a/f;

    const-string v1, "TYPE_PRIMARY"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/d/a/a/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/f;->a:Lcom/google/d/a/a/f;

    .line 324
    new-instance v0, Lcom/google/d/a/a/f;

    const-string v1, "TYPE_SECONDARY"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/f;->b:Lcom/google/d/a/a/f;

    .line 315
    new-array v0, v4, [Lcom/google/d/a/a/f;

    sget-object v1, Lcom/google/d/a/a/f;->a:Lcom/google/d/a/a/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/f;->b:Lcom/google/d/a/a/f;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/d/a/a/f;->d:[Lcom/google/d/a/a/f;

    .line 354
    new-instance v0, Lcom/google/d/a/a/g;

    invoke-direct {v0}, Lcom/google/d/a/a/g;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 363
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 364
    iput p3, p0, Lcom/google/d/a/a/f;->c:I

    .line 365
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/f;
    .locals 1

    .prologue
    .line 342
    packed-switch p0, :pswitch_data_0

    .line 345
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 343
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/f;->a:Lcom/google/d/a/a/f;

    goto :goto_0

    .line 344
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/f;->b:Lcom/google/d/a/a/f;

    goto :goto_0

    .line 342
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/f;
    .locals 1

    .prologue
    .line 315
    const-class v0, Lcom/google/d/a/a/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/f;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/f;
    .locals 1

    .prologue
    .line 315
    sget-object v0, Lcom/google/d/a/a/f;->d:[Lcom/google/d/a/a/f;

    invoke-virtual {v0}, [Lcom/google/d/a/a/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/f;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/google/d/a/a/f;->c:I

    return v0
.end method
