.class public final Lcom/google/d/a/a/ds;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/dv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ds;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/d/a/a/ds;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:J

.field public c:J

.field d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/d/a/a/dt;

    invoke-direct {v0}, Lcom/google/d/a/a/dt;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ds;->PARSER:Lcom/google/n/ax;

    .line 234
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/ds;->h:Lcom/google/n/aw;

    .line 516
    new-instance v0, Lcom/google/d/a/a/ds;

    invoke-direct {v0}, Lcom/google/d/a/a/ds;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ds;->e:Lcom/google/d/a/a/ds;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 155
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    .line 170
    iput-byte v2, p0, Lcom/google/d/a/a/ds;->f:B

    .line 209
    iput v2, p0, Lcom/google/d/a/a/ds;->g:I

    .line 54
    iput-wide v4, p0, Lcom/google/d/a/a/ds;->b:J

    .line 55
    iput-wide v4, p0, Lcom/google/d/a/a/ds;->c:J

    .line 56
    iget-object v0, p0, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Lcom/google/d/a/a/ds;-><init>()V

    .line 64
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 69
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 71
    sparse-switch v3, :sswitch_data_0

    .line 76
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 78
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    iget v3, p0, Lcom/google/d/a/a/ds;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/ds;->a:I

    .line 84
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/ds;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ds;->au:Lcom/google/n/bn;

    throw v0

    .line 88
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/d/a/a/ds;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/ds;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/ds;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 93
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 94
    iget v3, p0, Lcom/google/d/a/a/ds;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/ds;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ds;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 51
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 155
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    .line 170
    iput-byte v1, p0, Lcom/google/d/a/a/ds;->f:B

    .line 209
    iput v1, p0, Lcom/google/d/a/a/ds;->g:I

    .line 52
    return-void
.end method

.method public static a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;
    .locals 1

    .prologue
    .line 299
    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/d/a/a/ds;
    .locals 1

    .prologue
    .line 519
    sget-object v0, Lcom/google/d/a/a/ds;->e:Lcom/google/d/a/a/ds;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/du;
    .locals 1

    .prologue
    .line 296
    new-instance v0, Lcom/google/d/a/a/du;

    invoke-direct {v0}, Lcom/google/d/a/a/du;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ds;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/d/a/a/ds;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 196
    invoke-virtual {p0}, Lcom/google/d/a/a/ds;->c()I

    .line 197
    iget v0, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 198
    iget-wide v0, p0, Lcom/google/d/a/a/ds;->b:J

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 200
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 201
    iget-wide v0, p0, Lcom/google/d/a/a/ds;->c:J

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 203
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 204
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/ds;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 207
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    iget-byte v0, p0, Lcom/google/d/a/a/ds;->f:B

    .line 173
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 191
    :goto_0
    return v0

    .line 174
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 176
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 177
    iput-byte v2, p0, Lcom/google/d/a/a/ds;->f:B

    move v0, v2

    .line 178
    goto :goto_0

    :cond_2
    move v0, v2

    .line 176
    goto :goto_1

    .line 180
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    .line 181
    iput-byte v2, p0, Lcom/google/d/a/a/ds;->f:B

    move v0, v2

    .line 182
    goto :goto_0

    :cond_4
    move v0, v2

    .line 180
    goto :goto_2

    .line 184
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 185
    iget-object v0, p0, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 186
    iput-byte v2, p0, Lcom/google/d/a/a/ds;->f:B

    move v0, v2

    .line 187
    goto :goto_0

    :cond_6
    move v0, v2

    .line 184
    goto :goto_3

    .line 190
    :cond_7
    iput-byte v1, p0, Lcom/google/d/a/a/ds;->f:B

    move v0, v1

    .line 191
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 211
    iget v0, p0, Lcom/google/d/a/a/ds;->g:I

    .line 212
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 229
    :goto_0
    return v0

    .line 215
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    .line 216
    iget-wide v2, p0, Lcom/google/d/a/a/ds;->b:J

    .line 217
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 219
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 220
    iget-wide v2, p0, Lcom/google/d/a/a/ds;->c:J

    .line 221
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 223
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/ds;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 224
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/d/a/a/ds;->d:Lcom/google/n/ao;

    .line 225
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 227
    :cond_2
    iget-object v1, p0, Lcom/google/d/a/a/ds;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    iput v0, p0, Lcom/google/d/a/a/ds;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v0

    return-object v0
.end method
