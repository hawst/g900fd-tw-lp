.class public final Lcom/google/d/a/a/oe;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/oh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/oe;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/a/a/oe;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:F

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/google/d/a/a/of;

    invoke-direct {v0}, Lcom/google/d/a/a/of;-><init>()V

    sput-object v0, Lcom/google/d/a/a/oe;->PARSER:Lcom/google/n/ax;

    .line 195
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/oe;->g:Lcom/google/n/aw;

    .line 434
    new-instance v0, Lcom/google/d/a/a/oe;

    invoke-direct {v0}, Lcom/google/d/a/a/oe;-><init>()V

    sput-object v0, Lcom/google/d/a/a/oe;->d:Lcom/google/d/a/a/oe;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 110
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/oe;->b:Lcom/google/n/ao;

    .line 140
    iput-byte v2, p0, Lcom/google/d/a/a/oe;->e:B

    .line 174
    iput v2, p0, Lcom/google/d/a/a/oe;->f:I

    .line 45
    iget-object v0, p0, Lcom/google/d/a/a/oe;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/oe;->c:F

    .line 47
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/d/a/a/oe;-><init>()V

    .line 54
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 59
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 60
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 61
    sparse-switch v3, :sswitch_data_0

    .line 66
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 68
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 64
    goto :goto_0

    .line 73
    :sswitch_1
    iget-object v3, p0, Lcom/google/d/a/a/oe;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 74
    iget v3, p0, Lcom/google/d/a/a/oe;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/oe;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/oe;->au:Lcom/google/n/bn;

    throw v0

    .line 78
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/d/a/a/oe;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/oe;->a:I

    .line 79
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/oe;->c:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 86
    :catch_1
    move-exception v0

    .line 87
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 88
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 90
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/oe;->au:Lcom/google/n/bn;

    .line 91
    return-void

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 42
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 110
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/oe;->b:Lcom/google/n/ao;

    .line 140
    iput-byte v1, p0, Lcom/google/d/a/a/oe;->e:B

    .line 174
    iput v1, p0, Lcom/google/d/a/a/oe;->f:I

    .line 43
    return-void
.end method

.method public static d()Lcom/google/d/a/a/oe;
    .locals 1

    .prologue
    .line 437
    sget-object v0, Lcom/google/d/a/a/oe;->d:Lcom/google/d/a/a/oe;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/og;
    .locals 1

    .prologue
    .line 257
    new-instance v0, Lcom/google/d/a/a/og;

    invoke-direct {v0}, Lcom/google/d/a/a/og;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/oe;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lcom/google/d/a/a/oe;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 164
    invoke-virtual {p0}, Lcom/google/d/a/a/oe;->c()I

    .line 165
    iget v0, p0, Lcom/google/d/a/a/oe;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/d/a/a/oe;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 168
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/oe;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 169
    iget v0, p0, Lcom/google/d/a/a/oe;->c:F

    const/4 v1, 0x5

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/oe;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 172
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 142
    iget-byte v0, p0, Lcom/google/d/a/a/oe;->e:B

    .line 143
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 159
    :goto_0
    return v0

    .line 144
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 146
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/oe;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 147
    iput-byte v2, p0, Lcom/google/d/a/a/oe;->e:B

    move v0, v2

    .line 148
    goto :goto_0

    :cond_2
    move v0, v2

    .line 146
    goto :goto_1

    .line 150
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/oe;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    .line 151
    iput-byte v2, p0, Lcom/google/d/a/a/oe;->e:B

    move v0, v2

    .line 152
    goto :goto_0

    :cond_4
    move v0, v2

    .line 150
    goto :goto_2

    .line 154
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/oe;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 155
    iput-byte v2, p0, Lcom/google/d/a/a/oe;->e:B

    move v0, v2

    .line 156
    goto :goto_0

    .line 158
    :cond_6
    iput-byte v1, p0, Lcom/google/d/a/a/oe;->e:B

    move v0, v1

    .line 159
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 176
    iget v0, p0, Lcom/google/d/a/a/oe;->f:I

    .line 177
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 190
    :goto_0
    return v0

    .line 180
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/oe;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 181
    iget-object v0, p0, Lcom/google/d/a/a/oe;->b:Lcom/google/n/ao;

    .line 182
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 184
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/oe;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 185
    iget v2, p0, Lcom/google/d/a/a/oe;->c:F

    .line 186
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 188
    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/oe;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    iput v0, p0, Lcom/google/d/a/a/oe;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/d/a/a/oe;->newBuilder()Lcom/google/d/a/a/og;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/og;->a(Lcom/google/d/a/a/oe;)Lcom/google/d/a/a/og;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/d/a/a/oe;->newBuilder()Lcom/google/d/a/a/og;

    move-result-object v0

    return-object v0
.end method
