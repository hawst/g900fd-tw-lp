.class public final Lcom/google/d/a/a/ec;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/eh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ec;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/a/a/ec;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/google/d/a/a/ed;

    invoke-direct {v0}, Lcom/google/d/a/a/ed;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ec;->PARSER:Lcom/google/n/ax;

    .line 285
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/ec;->g:Lcom/google/n/aw;

    .line 605
    new-instance v0, Lcom/google/d/a/a/ec;

    invoke-direct {v0}, Lcom/google/d/a/a/ec;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ec;->d:Lcom/google/d/a/a/ec;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 49
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 236
    iput-byte v0, p0, Lcom/google/d/a/a/ec;->e:B

    .line 264
    iput v0, p0, Lcom/google/d/a/a/ec;->f:I

    .line 50
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/d/a/a/ec;->b:I

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    .line 52
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v2, 0x1

    .line 58
    invoke-direct {p0}, Lcom/google/d/a/a/ec;-><init>()V

    .line 61
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 64
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 65
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 66
    sparse-switch v4, :sswitch_data_0

    .line 71
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 73
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 69
    goto :goto_0

    .line 78
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 79
    invoke-static {v4}, Lcom/google/d/a/a/ef;->a(I)Lcom/google/d/a/a/ef;

    move-result-object v5

    .line 80
    if-nez v5, :cond_2

    .line 81
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 100
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 101
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 107
    iget-object v1, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    .line 109
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ec;->au:Lcom/google/n/bn;

    throw v0

    .line 83
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/d/a/a/ec;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/ec;->a:I

    .line 84
    iput v4, p0, Lcom/google/d/a/a/ec;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 102
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 103
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 104
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 89
    :sswitch_2
    and-int/lit8 v4, v0, 0x2

    if-eq v4, v7, :cond_3

    .line 90
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    .line 92
    or-int/lit8 v0, v0, 0x2

    .line 94
    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 95
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 94
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 106
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    :cond_4
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_5

    .line 107
    iget-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    .line 109
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ec;->au:Lcom/google/n/bn;

    .line 110
    return-void

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 47
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 236
    iput-byte v0, p0, Lcom/google/d/a/a/ec;->e:B

    .line 264
    iput v0, p0, Lcom/google/d/a/a/ec;->f:I

    .line 48
    return-void
.end method

.method public static d()Lcom/google/d/a/a/ec;
    .locals 1

    .prologue
    .line 608
    sget-object v0, Lcom/google/d/a/a/ec;->d:Lcom/google/d/a/a/ec;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ee;
    .locals 1

    .prologue
    .line 347
    new-instance v0, Lcom/google/d/a/a/ee;

    invoke-direct {v0}, Lcom/google/d/a/a/ee;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/google/d/a/a/ec;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 254
    invoke-virtual {p0}, Lcom/google/d/a/a/ec;->c()I

    .line 255
    iget v1, p0, Lcom/google/d/a/a/ec;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 256
    iget v1, p0, Lcom/google/d/a/a/ec;->b:I

    invoke-static {v2, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_0
    :goto_0
    move v1, v0

    .line 258
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 259
    iget-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 258
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 256
    :cond_1
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/ec;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 262
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    iget-byte v0, p0, Lcom/google/d/a/a/ec;->e:B

    .line 239
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 249
    :cond_0
    :goto_0
    return v2

    .line 240
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 242
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 243
    iget-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/fz;->d()Lcom/google/d/a/a/fz;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fz;

    invoke-virtual {v0}, Lcom/google/d/a/a/fz;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 244
    iput-byte v2, p0, Lcom/google/d/a/a/ec;->e:B

    goto :goto_0

    .line 242
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 248
    :cond_3
    iput-byte v3, p0, Lcom/google/d/a/a/ec;->e:B

    move v2, v3

    .line 249
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 266
    iget v0, p0, Lcom/google/d/a/a/ec;->f:I

    .line 267
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 280
    :goto_0
    return v0

    .line 270
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ec;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 271
    iget v0, p0, Lcom/google/d/a/a/ec;->b:I

    .line 272
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 274
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 275
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/d/a/a/ec;->c:Ljava/util/List;

    .line 276
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 274
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 272
    :cond_1
    const/16 v0, 0xa

    goto :goto_1

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/ec;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 279
    iput v0, p0, Lcom/google/d/a/a/ec;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/d/a/a/ec;->newBuilder()Lcom/google/d/a/a/ee;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ee;->a(Lcom/google/d/a/a/ec;)Lcom/google/d/a/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/d/a/a/ec;->newBuilder()Lcom/google/d/a/a/ee;

    move-result-object v0

    return-object v0
.end method
