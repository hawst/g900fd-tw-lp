.class public final enum Lcom/google/d/a/a/jq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/jq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/jq;

.field public static final enum b:Lcom/google/d/a/a/jq;

.field public static final enum c:Lcom/google/d/a/a/jq;

.field private static final synthetic e:[Lcom/google/d/a/a/jq;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 170
    new-instance v0, Lcom/google/d/a/a/jq;

    const-string v1, "CONFLATION_PICK_FIRST_VALUE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/d/a/a/jq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/jq;->a:Lcom/google/d/a/a/jq;

    .line 174
    new-instance v0, Lcom/google/d/a/a/jq;

    const-string v1, "CONFLATION_UNION_CSV"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/a/a/jq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/jq;->b:Lcom/google/d/a/a/jq;

    .line 178
    new-instance v0, Lcom/google/d/a/a/jq;

    const-string v1, "CONFLATION_SUM"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/jq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/jq;->c:Lcom/google/d/a/a/jq;

    .line 165
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/d/a/a/jq;

    sget-object v1, Lcom/google/d/a/a/jq;->a:Lcom/google/d/a/a/jq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/jq;->b:Lcom/google/d/a/a/jq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/jq;->c:Lcom/google/d/a/a/jq;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/d/a/a/jq;->e:[Lcom/google/d/a/a/jq;

    .line 213
    new-instance v0, Lcom/google/d/a/a/jr;

    invoke-direct {v0}, Lcom/google/d/a/a/jr;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 222
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 223
    iput p3, p0, Lcom/google/d/a/a/jq;->d:I

    .line 224
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/jq;
    .locals 1

    .prologue
    .line 200
    packed-switch p0, :pswitch_data_0

    .line 204
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 201
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/jq;->a:Lcom/google/d/a/a/jq;

    goto :goto_0

    .line 202
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/jq;->b:Lcom/google/d/a/a/jq;

    goto :goto_0

    .line 203
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/jq;->c:Lcom/google/d/a/a/jq;

    goto :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/jq;
    .locals 1

    .prologue
    .line 165
    const-class v0, Lcom/google/d/a/a/jq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/jq;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/jq;
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/google/d/a/a/jq;->e:[Lcom/google/d/a/a/jq;

    invoke-virtual {v0}, [Lcom/google/d/a/a/jq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/jq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/google/d/a/a/jq;->d:I

    return v0
.end method
