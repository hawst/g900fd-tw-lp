.class public final Lcom/google/d/a/a/ot;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/or;",
        "Lcom/google/d/a/a/ot;",
        ">;",
        "Lcom/google/d/a/a/ow;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2742
    sget-object v0, Lcom/google/d/a/a/or;->d:Lcom/google/d/a/a/or;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2800
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    .line 2936
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/ot;->c:I

    .line 2743
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/or;)Lcom/google/d/a/a/ot;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2775
    invoke-static {}, Lcom/google/d/a/a/or;->g()Lcom/google/d/a/a/or;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 2790
    :goto_0
    return-object p0

    .line 2776
    :cond_0
    iget-object v1, p1, Lcom/google/d/a/a/or;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2777
    iget-object v1, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2778
    iget-object v1, p1, Lcom/google/d/a/a/or;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    .line 2779
    iget v1, p0, Lcom/google/d/a/a/ot;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/d/a/a/ot;->a:I

    .line 2786
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/d/a/a/or;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_2
    if-eqz v0, :cond_7

    .line 2787
    iget v0, p1, Lcom/google/d/a/a/or;->c:I

    invoke-static {v0}, Lcom/google/d/a/a/ou;->a(I)Lcom/google/d/a/a/ou;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/d/a/a/ou;->a:Lcom/google/d/a/a/ou;

    :cond_2
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2781
    :cond_3
    iget v1, p0, Lcom/google/d/a/a/ot;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/ot;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/d/a/a/ot;->a:I

    .line 2782
    :cond_4
    iget-object v1, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/d/a/a/or;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2786
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 2787
    :cond_6
    iget v1, p0, Lcom/google/d/a/a/ot;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/d/a/a/ot;->a:I

    iget v0, v0, Lcom/google/d/a/a/ou;->c:I

    iput v0, p0, Lcom/google/d/a/a/ot;->c:I

    .line 2789
    :cond_7
    iget-object v0, p1, Lcom/google/d/a/a/or;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2734
    new-instance v2, Lcom/google/d/a/a/or;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/or;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ot;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/d/a/a/ot;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/ot;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/ot;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/ot;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/or;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/ot;->c:I

    iput v1, v2, Lcom/google/d/a/a/or;->c:I

    iput v0, v2, Lcom/google/d/a/a/or;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2734
    check-cast p1, Lcom/google/d/a/a/or;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ot;->a(Lcom/google/d/a/a/or;)Lcom/google/d/a/a/ot;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2794
    const/4 v0, 0x1

    return v0
.end method
