.class public final Lcom/google/d/a/a/ib;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ic;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/hz;",
        "Lcom/google/d/a/a/ib;",
        ">;",
        "Lcom/google/d/a/a/ic;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/n/f;

.field private d:F

.field private e:F

.field private f:J

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 502
    sget-object v0, Lcom/google/d/a/a/hz;->i:Lcom/google/d/a/a/hz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 623
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    .line 759
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/d/a/a/ib;->c:Lcom/google/n/f;

    .line 890
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ib;->g:Lcom/google/n/ao;

    .line 949
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ib;->h:Lcom/google/n/ao;

    .line 503
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/hz;)Lcom/google/d/a/a/ib;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 569
    invoke-static {}, Lcom/google/d/a/a/hz;->d()Lcom/google/d/a/a/hz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 601
    :goto_0
    return-object p0

    .line 570
    :cond_0
    iget-object v2, p1, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 571
    iget-object v2, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 572
    iget-object v2, p1, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    .line 573
    iget v2, p0, Lcom/google/d/a/a/ib;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/d/a/a/ib;->a:I

    .line 580
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 581
    iget-object v2, p1, Lcom/google/d/a/a/hz;->c:Lcom/google/n/f;

    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 575
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/ib;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_3

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/ib;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/ib;->a:I

    .line 576
    :cond_3
    iget-object v2, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_4
    move v2, v1

    .line 580
    goto :goto_2

    .line 581
    :cond_5
    iget v3, p0, Lcom/google/d/a/a/ib;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/ib;->a:I

    iput-object v2, p0, Lcom/google/d/a/a/ib;->c:Lcom/google/n/f;

    .line 583
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 584
    iget v2, p1, Lcom/google/d/a/a/hz;->d:F

    iget v3, p0, Lcom/google/d/a/a/ib;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/ib;->a:I

    iput v2, p0, Lcom/google/d/a/a/ib;->d:F

    .line 586
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 587
    iget v2, p1, Lcom/google/d/a/a/hz;->e:F

    iget v3, p0, Lcom/google/d/a/a/ib;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/ib;->a:I

    iput v2, p0, Lcom/google/d/a/a/ib;->e:F

    .line 589
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_9

    .line 590
    iget-wide v2, p1, Lcom/google/d/a/a/hz;->f:J

    iget v4, p0, Lcom/google/d/a/a/ib;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/d/a/a/ib;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/ib;->f:J

    .line 592
    :cond_9
    iget v2, p1, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_a

    .line 593
    iget-object v2, p0, Lcom/google/d/a/a/ib;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/hz;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 594
    iget v2, p0, Lcom/google/d/a/a/ib;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/ib;->a:I

    .line 596
    :cond_a
    iget v2, p1, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    :goto_7
    if-eqz v0, :cond_b

    .line 597
    iget-object v0, p0, Lcom/google/d/a/a/ib;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 598
    iget v0, p0, Lcom/google/d/a/a/ib;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/ib;->a:I

    .line 600
    :cond_b
    iget-object v0, p1, Lcom/google/d/a/a/hz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 583
    goto :goto_3

    :cond_d
    move v2, v1

    .line 586
    goto :goto_4

    :cond_e
    move v2, v1

    .line 589
    goto :goto_5

    :cond_f
    move v2, v1

    .line 592
    goto :goto_6

    :cond_10
    move v0, v1

    .line 596
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 494
    new-instance v2, Lcom/google/d/a/a/hz;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/hz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ib;->a:I

    iget v4, p0, Lcom/google/d/a/a/ib;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/ib;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/ib;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    :goto_0
    iget-object v4, p0, Lcom/google/d/a/a/ib;->c:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/d/a/a/hz;->c:Lcom/google/n/f;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/ib;->d:F

    iput v4, v2, Lcom/google/d/a/a/hz;->d:F

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/d/a/a/ib;->e:F

    iput v4, v2, Lcom/google/d/a/a/hz;->e:F

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-wide v4, p0, Lcom/google/d/a/a/ib;->f:J

    iput-wide v4, v2, Lcom/google/d/a/a/hz;->f:J

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/d/a/a/hz;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/ib;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/ib;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v3, v2, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/ib;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/ib;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/hz;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 494
    check-cast p1, Lcom/google/d/a/a/hz;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ib;->a(Lcom/google/d/a/a/hz;)Lcom/google/d/a/a/ib;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 605
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 606
    iget-object v0, p0, Lcom/google/d/a/a/ib;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ie;->d()Lcom/google/d/a/a/ie;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ie;

    invoke-virtual {v0}, Lcom/google/d/a/a/ie;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 617
    :cond_0
    :goto_1
    return v2

    .line 605
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 611
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/ib;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 612
    iget-object v0, p0, Lcom/google/d/a/a/ib;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v3

    .line 617
    goto :goto_1

    :cond_4
    move v0, v2

    .line 611
    goto :goto_2
.end method
