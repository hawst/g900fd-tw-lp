.class public final Lcom/google/d/a/a/gg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/gh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ge;",
        "Lcom/google/d/a/a/gg;",
        ">;",
        "Lcom/google/d/a/a/gh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:F

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 309
    sget-object v0, Lcom/google/d/a/a/ge;->d:Lcom/google/d/a/a/ge;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 405
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    .line 310
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ge;)Lcom/google/d/a/a/gg;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 342
    invoke-static {}, Lcom/google/d/a/a/ge;->d()Lcom/google/d/a/a/ge;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 357
    :goto_0
    return-object p0

    .line 343
    :cond_0
    iget v1, p1, Lcom/google/d/a/a/ge;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 344
    iget v0, p1, Lcom/google/d/a/a/ge;->b:F

    iget v1, p0, Lcom/google/d/a/a/gg;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/d/a/a/gg;->a:I

    iput v0, p0, Lcom/google/d/a/a/gg;->b:F

    .line 346
    :cond_1
    iget-object v0, p1, Lcom/google/d/a/a/ge;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 347
    iget-object v0, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 348
    iget-object v0, p1, Lcom/google/d/a/a/ge;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    .line 349
    iget v0, p0, Lcom/google/d/a/a/gg;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/d/a/a/gg;->a:I

    .line 356
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/d/a/a/ge;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 343
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 351
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/gg;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/gg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/gg;->a:I

    .line 352
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/ge;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 301
    new-instance v2, Lcom/google/d/a/a/ge;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ge;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/gg;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/gg;->b:F

    iput v1, v2, Lcom/google/d/a/a/ge;->b:F

    iget v1, p0, Lcom/google/d/a/a/gg;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/gg;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/d/a/a/gg;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/ge;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/d/a/a/ge;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 301
    check-cast p1, Lcom/google/d/a/a/ge;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/gg;->a(Lcom/google/d/a/a/ge;)Lcom/google/d/a/a/gg;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 361
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/google/d/a/a/gg;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    :goto_1
    return v2

    .line 361
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 367
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
