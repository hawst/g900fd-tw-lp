.class public final Lcom/google/d/a/a/lu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/lz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/lu;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/d/a/a/lu;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7202
    new-instance v0, Lcom/google/d/a/a/lv;

    invoke-direct {v0}, Lcom/google/d/a/a/lv;-><init>()V

    sput-object v0, Lcom/google/d/a/a/lu;->PARSER:Lcom/google/n/ax;

    .line 7400
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/lu;->h:Lcom/google/n/aw;

    .line 7684
    new-instance v0, Lcom/google/d/a/a/lu;

    invoke-direct {v0}, Lcom/google/d/a/a/lu;-><init>()V

    sput-object v0, Lcom/google/d/a/a/lu;->e:Lcom/google/d/a/a/lu;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 7135
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 7295
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    .line 7342
    iput-byte v2, p0, Lcom/google/d/a/a/lu;->f:B

    .line 7375
    iput v2, p0, Lcom/google/d/a/a/lu;->g:I

    .line 7136
    iget-object v0, p0, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 7137
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/lu;->c:I

    .line 7138
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/lu;->d:I

    .line 7139
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 7145
    invoke-direct {p0}, Lcom/google/d/a/a/lu;-><init>()V

    .line 7146
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 7151
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 7152
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 7153
    sparse-switch v3, :sswitch_data_0

    .line 7158
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 7160
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 7156
    goto :goto_0

    .line 7165
    :sswitch_1
    iget-object v3, p0, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 7166
    iget v3, p0, Lcom/google/d/a/a/lu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/lu;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 7193
    :catch_0
    move-exception v0

    .line 7194
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7199
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/lu;->au:Lcom/google/n/bn;

    throw v0

    .line 7170
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 7171
    invoke-static {v3}, Lcom/google/d/a/a/lx;->a(I)Lcom/google/d/a/a/lx;

    move-result-object v4

    .line 7172
    if-nez v4, :cond_1

    .line 7173
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 7195
    :catch_1
    move-exception v0

    .line 7196
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 7197
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7175
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/d/a/a/lu;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/lu;->a:I

    .line 7176
    iput v3, p0, Lcom/google/d/a/a/lu;->c:I

    goto :goto_0

    .line 7181
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 7182
    invoke-static {v3}, Lcom/google/d/a/a/ln;->a(I)Lcom/google/d/a/a/ln;

    move-result-object v4

    .line 7183
    if-nez v4, :cond_2

    .line 7184
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 7186
    :cond_2
    iget v4, p0, Lcom/google/d/a/a/lu;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/lu;->a:I

    .line 7187
    iput v3, p0, Lcom/google/d/a/a/lu;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 7199
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/lu;->au:Lcom/google/n/bn;

    .line 7200
    return-void

    .line 7153
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 7133
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 7295
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    .line 7342
    iput-byte v1, p0, Lcom/google/d/a/a/lu;->f:B

    .line 7375
    iput v1, p0, Lcom/google/d/a/a/lu;->g:I

    .line 7134
    return-void
.end method

.method public static d()Lcom/google/d/a/a/lu;
    .locals 1

    .prologue
    .line 7687
    sget-object v0, Lcom/google/d/a/a/lu;->e:Lcom/google/d/a/a/lu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/lw;
    .locals 1

    .prologue
    .line 7462
    new-instance v0, Lcom/google/d/a/a/lw;

    invoke-direct {v0}, Lcom/google/d/a/a/lw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/lu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7214
    sget-object v0, Lcom/google/d/a/a/lu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 7362
    invoke-virtual {p0}, Lcom/google/d/a/a/lu;->c()I

    .line 7363
    iget v0, p0, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 7364
    iget-object v0, p0, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 7366
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 7367
    iget v0, p0, Lcom/google/d/a/a/lu;->c:I

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 7369
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 7370
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/lu;->d:I

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 7372
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/lu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 7373
    return-void

    .line 7367
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 7370
    :cond_4
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 7344
    iget-byte v0, p0, Lcom/google/d/a/a/lu;->f:B

    .line 7345
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 7357
    :goto_0
    return v0

    .line 7346
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 7348
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 7349
    iput-byte v2, p0, Lcom/google/d/a/a/lu;->f:B

    move v0, v2

    .line 7350
    goto :goto_0

    :cond_2
    move v0, v2

    .line 7348
    goto :goto_1

    .line 7352
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 7353
    iput-byte v2, p0, Lcom/google/d/a/a/lu;->f:B

    move v0, v2

    .line 7354
    goto :goto_0

    .line 7356
    :cond_4
    iput-byte v1, p0, Lcom/google/d/a/a/lu;->f:B

    move v0, v1

    .line 7357
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v3, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 7377
    iget v0, p0, Lcom/google/d/a/a/lu;->g:I

    .line 7378
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 7395
    :goto_0
    return v0

    .line 7381
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 7382
    iget-object v0, p0, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    .line 7383
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 7385
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 7386
    iget v2, p0, Lcom/google/d/a/a/lu;->c:I

    .line 7387
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 7389
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_3

    .line 7390
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/d/a/a/lu;->d:I

    .line 7391
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 7393
    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/lu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 7394
    iput v0, p0, Lcom/google/d/a/a/lu;->g:I

    goto :goto_0

    :cond_4
    move v2, v3

    .line 7387
    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7127
    invoke-static {}, Lcom/google/d/a/a/lu;->newBuilder()Lcom/google/d/a/a/lw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/lw;->a(Lcom/google/d/a/a/lu;)Lcom/google/d/a/a/lw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7127
    invoke-static {}, Lcom/google/d/a/a/lu;->newBuilder()Lcom/google/d/a/a/lw;

    move-result-object v0

    return-object v0
.end method
