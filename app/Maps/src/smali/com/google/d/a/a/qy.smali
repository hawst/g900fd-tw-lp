.class public final Lcom/google/d/a/a/qy;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/rb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/qw;",
        "Lcom/google/d/a/a/qy;",
        ">;",
        "Lcom/google/d/a/a/rb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 687
    sget-object v0, Lcom/google/d/a/a/qw;->h:Lcom/google/d/a/a/qw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 830
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/qy;->b:Ljava/lang/Object;

    .line 907
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    .line 1044
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    .line 1181
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    .line 1317
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/qy;->f:I

    .line 1354
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    .line 688
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/qw;)Lcom/google/d/a/a/qy;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 747
    invoke-static {}, Lcom/google/d/a/a/qw;->d()Lcom/google/d/a/a/qw;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 797
    :goto_0
    return-object p0

    .line 748
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/qw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 749
    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/qy;->a:I

    .line 750
    iget-object v2, p1, Lcom/google/d/a/a/qw;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/qy;->b:Ljava/lang/Object;

    .line 753
    :cond_1
    iget-object v2, p1, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 754
    iget-object v2, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 755
    iget-object v2, p1, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    .line 756
    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/d/a/a/qy;->a:I

    .line 763
    :cond_2
    :goto_2
    iget-object v2, p1, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 764
    iget-object v2, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 765
    iget-object v2, p1, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    .line 766
    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/a/a/qy;->a:I

    .line 773
    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 774
    iget-object v2, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 775
    iget-object v2, p1, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    .line 776
    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/d/a/a/qy;->a:I

    .line 783
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/d/a/a/qw;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_d

    :goto_5
    if-eqz v0, :cond_f

    .line 784
    iget v0, p1, Lcom/google/d/a/a/qw;->f:I

    invoke-static {v0}, Lcom/google/d/a/a/qz;->a(I)Lcom/google/d/a/a/qz;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/d/a/a/qz;->a:Lcom/google/d/a/a/qz;

    :cond_5
    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 748
    goto :goto_1

    .line 758
    :cond_7
    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/qy;->a:I

    .line 759
    :cond_8
    iget-object v2, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 768
    :cond_9
    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-eq v2, v3, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/qy;->a:I

    .line 769
    :cond_a
    iget-object v2, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 778
    :cond_b
    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-eq v2, v3, :cond_c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/qy;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/qy;->a:I

    .line 779
    :cond_c
    iget-object v2, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_d
    move v0, v1

    .line 783
    goto :goto_5

    .line 784
    :cond_e
    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/d/a/a/qy;->a:I

    iget v0, v0, Lcom/google/d/a/a/qz;->b:I

    iput v0, p0, Lcom/google/d/a/a/qy;->f:I

    .line 786
    :cond_f
    iget-object v0, p1, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 787
    iget-object v0, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 788
    iget-object v0, p1, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    .line 789
    iget v0, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/d/a/a/qy;->a:I

    .line 796
    :cond_10
    :goto_6
    iget-object v0, p1, Lcom/google/d/a/a/qw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 791
    :cond_11
    iget v0, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_12

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/qy;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/qy;->a:I

    .line 792
    :cond_12
    iget-object v0, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 679
    new-instance v2, Lcom/google/d/a/a/qw;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/qw;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/qy;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/d/a/a/qy;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/qw;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/d/a/a/qy;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/d/a/a/qy;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/d/a/a/qy;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x2

    :cond_3
    iget v1, p0, Lcom/google/d/a/a/qy;->f:I

    iput v1, v2, Lcom/google/d/a/a/qw;->f:I

    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qy;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/d/a/a/qy;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    iput v0, v2, Lcom/google/d/a/a/qw;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 679
    check-cast p1, Lcom/google/d/a/a/qw;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/qy;->a(Lcom/google/d/a/a/qw;)Lcom/google/d/a/a/qy;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 801
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 802
    iget-object v0, p0, Lcom/google/d/a/a/qy;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 825
    :cond_0
    :goto_1
    return v2

    .line 801
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 807
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 808
    iget-object v0, p0, Lcom/google/d/a/a/qy;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sm;->d()Lcom/google/d/a/a/sm;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sm;

    invoke-virtual {v0}, Lcom/google/d/a/a/sm;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 807
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 813
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 814
    iget-object v0, p0, Lcom/google/d/a/a/qy;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v1, v2

    .line 819
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 820
    iget-object v0, p0, Lcom/google/d/a/a/qy;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 819
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 825
    :cond_5
    const/4 v2, 0x1

    goto :goto_1
.end method
