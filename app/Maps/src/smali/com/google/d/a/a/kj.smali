.class public final Lcom/google/d/a/a/kj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/kq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/kg;",
        "Lcom/google/d/a/a/kj;",
        ">;",
        "Lcom/google/d/a/a/kq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1034
    sget-object v0, Lcom/google/d/a/a/kg;->j:Lcom/google/d/a/a/kg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1184
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    .line 1320
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/d/a/a/kj;->c:I

    .line 1357
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    .line 1430
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/kj;->e:I

    .line 1466
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kj;->f:Lcom/google/n/ao;

    .line 1525
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kj;->g:Lcom/google/n/ao;

    .line 1584
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kj;->h:Lcom/google/n/ao;

    .line 1643
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kj;->i:Lcom/google/n/ao;

    .line 1035
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/kg;)Lcom/google/d/a/a/kj;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1112
    invoke-static {}, Lcom/google/d/a/a/kg;->d()Lcom/google/d/a/a/kg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1156
    :goto_0
    return-object p0

    .line 1113
    :cond_0
    iget-object v2, p1, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1114
    iget-object v2, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1115
    iget-object v2, p1, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    .line 1116
    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/d/a/a/kj;->a:I

    .line 1123
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 1124
    iget v2, p1, Lcom/google/d/a/a/kg;->c:I

    invoke-static {v2}, Lcom/google/d/a/a/km;->a(I)Lcom/google/d/a/a/km;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/d/a/a/km;->a:Lcom/google/d/a/a/km;

    :cond_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1118
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/kj;->a:I

    .line 1119
    :cond_4
    iget-object v2, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 1123
    goto :goto_2

    .line 1124
    :cond_6
    iget v3, p0, Lcom/google/d/a/a/kj;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/kj;->a:I

    iget v2, v2, Lcom/google/d/a/a/km;->q:I

    iput v2, p0, Lcom/google/d/a/a/kj;->c:I

    .line 1126
    :cond_7
    iget-object v2, p1, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1127
    iget-object v2, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1128
    iget-object v2, p1, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    .line 1129
    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/a/a/kj;->a:I

    .line 1136
    :cond_8
    :goto_3
    iget v2, p1, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_e

    .line 1137
    iget v2, p1, Lcom/google/d/a/a/kg;->e:I

    invoke-static {v2}, Lcom/google/d/a/a/kk;->a(I)Lcom/google/d/a/a/kk;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/d/a/a/kk;->a:Lcom/google/d/a/a/kk;

    :cond_9
    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1131
    :cond_a
    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/kj;->a:I

    .line 1132
    :cond_b
    iget-object v2, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_c
    move v2, v1

    .line 1136
    goto :goto_4

    .line 1137
    :cond_d
    iget v3, p0, Lcom/google/d/a/a/kj;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/kj;->a:I

    iget v2, v2, Lcom/google/d/a/a/kk;->e:I

    iput v2, p0, Lcom/google/d/a/a/kj;->e:I

    .line 1139
    :cond_e
    iget v2, p1, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_f

    .line 1140
    iget-object v2, p0, Lcom/google/d/a/a/kj;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1141
    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/kj;->a:I

    .line 1143
    :cond_f
    iget v2, p1, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_10

    .line 1144
    iget-object v2, p0, Lcom/google/d/a/a/kj;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/kg;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1145
    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/kj;->a:I

    .line 1147
    :cond_10
    iget v2, p1, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_11

    .line 1148
    iget-object v2, p0, Lcom/google/d/a/a/kj;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1149
    iget v2, p0, Lcom/google/d/a/a/kj;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/kj;->a:I

    .line 1151
    :cond_11
    iget v2, p1, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_16

    :goto_8
    if-eqz v0, :cond_12

    .line 1152
    iget-object v0, p0, Lcom/google/d/a/a/kj;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/kg;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1153
    iget v0, p0, Lcom/google/d/a/a/kj;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/kj;->a:I

    .line 1155
    :cond_12
    iget-object v0, p1, Lcom/google/d/a/a/kg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v2, v1

    .line 1139
    goto :goto_5

    :cond_14
    move v2, v1

    .line 1143
    goto :goto_6

    :cond_15
    move v2, v1

    .line 1147
    goto :goto_7

    :cond_16
    move v0, v1

    .line 1151
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1026
    new-instance v2, Lcom/google/d/a/a/kg;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/kg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/kj;->a:I

    iget v4, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/kj;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/kj;->c:I

    iput v4, v2, Lcom/google/d/a/a/kg;->c:I

    iget v4, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/d/a/a/kj;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/kj;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget v4, p0, Lcom/google/d/a/a/kj;->e:I

    iput v4, v2, Lcom/google/d/a/a/kg;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v4, v2, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/kj;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/kj;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v4, v2, Lcom/google/d/a/a/kg;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/kj;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/kj;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v4, v2, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/kj;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/kj;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v3, v2, Lcom/google/d/a/a/kg;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/kj;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/kj;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/kg;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1026
    check-cast p1, Lcom/google/d/a/a/kg;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/kj;->a(Lcom/google/d/a/a/kg;)Lcom/google/d/a/a/kj;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1160
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1161
    iget-object v0, p0, Lcom/google/d/a/a/kj;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1178
    :cond_0
    :goto_1
    return v2

    .line 1160
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1166
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1167
    iget-object v0, p0, Lcom/google/d/a/a/kj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1172
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/kj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1173
    iget-object v0, p0, Lcom/google/d/a/a/kj;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 1178
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1166
    goto :goto_2

    :cond_6
    move v0, v2

    .line 1172
    goto :goto_3
.end method
