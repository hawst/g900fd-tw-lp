.class public final enum Lcom/google/d/a/a/dh;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/dh;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/dh;

.field public static final enum b:Lcom/google/d/a/a/dh;

.field public static final enum c:Lcom/google/d/a/a/dh;

.field public static final enum d:Lcom/google/d/a/a/dh;

.field public static final enum e:Lcom/google/d/a/a/dh;

.field public static final enum f:Lcom/google/d/a/a/dh;

.field public static final enum g:Lcom/google/d/a/a/dh;

.field public static final enum h:Lcom/google/d/a/a/dh;

.field public static final enum i:Lcom/google/d/a/a/dh;

.field public static final enum j:Lcom/google/d/a/a/dh;

.field private static final synthetic l:[Lcom/google/d/a/a/dh;


# instance fields
.field final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 246
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->a:Lcom/google/d/a/a/dh;

    .line 250
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "BOGUS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->b:Lcom/google/d/a/a/dh;

    .line 254
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "PRIVATE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->c:Lcom/google/d/a/a/dh;

    .line 258
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "SPAM"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->d:Lcom/google/d/a/a/dh;

    .line 262
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "UNSUPPORTED"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->e:Lcom/google/d/a/a/dh;

    .line 266
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "PENDING"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->f:Lcom/google/d/a/a/dh;

    .line 270
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "MERGED"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->g:Lcom/google/d/a/a/dh;

    .line 274
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "DEPRECATED_AWAITING_OFFLINE_REVIEW"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->h:Lcom/google/d/a/a/dh;

    .line 278
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "OLD_SCHEMA"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->i:Lcom/google/d/a/a/dh;

    .line 282
    new-instance v0, Lcom/google/d/a/a/dh;

    const-string v1, "REPLACED"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/dh;->j:Lcom/google/d/a/a/dh;

    .line 241
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/d/a/a/dh;

    sget-object v1, Lcom/google/d/a/a/dh;->a:Lcom/google/d/a/a/dh;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/dh;->b:Lcom/google/d/a/a/dh;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/dh;->c:Lcom/google/d/a/a/dh;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/dh;->d:Lcom/google/d/a/a/dh;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/dh;->e:Lcom/google/d/a/a/dh;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/dh;->f:Lcom/google/d/a/a/dh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/dh;->g:Lcom/google/d/a/a/dh;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/dh;->h:Lcom/google/d/a/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/dh;->i:Lcom/google/d/a/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/dh;->j:Lcom/google/d/a/a/dh;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/dh;->l:[Lcom/google/d/a/a/dh;

    .line 352
    new-instance v0, Lcom/google/d/a/a/di;

    invoke-direct {v0}, Lcom/google/d/a/a/di;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 361
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 362
    iput p3, p0, Lcom/google/d/a/a/dh;->k:I

    .line 363
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/dh;
    .locals 1

    .prologue
    .line 332
    packed-switch p0, :pswitch_data_0

    .line 343
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 333
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/dh;->a:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 334
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/dh;->b:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 335
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/dh;->c:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 336
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/dh;->d:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 337
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/dh;->e:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 338
    :pswitch_5
    sget-object v0, Lcom/google/d/a/a/dh;->f:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 339
    :pswitch_6
    sget-object v0, Lcom/google/d/a/a/dh;->g:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 340
    :pswitch_7
    sget-object v0, Lcom/google/d/a/a/dh;->h:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 341
    :pswitch_8
    sget-object v0, Lcom/google/d/a/a/dh;->i:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 342
    :pswitch_9
    sget-object v0, Lcom/google/d/a/a/dh;->j:Lcom/google/d/a/a/dh;

    goto :goto_0

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/dh;
    .locals 1

    .prologue
    .line 241
    const-class v0, Lcom/google/d/a/a/dh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/dh;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/dh;
    .locals 1

    .prologue
    .line 241
    sget-object v0, Lcom/google/d/a/a/dh;->l:[Lcom/google/d/a/a/dh;

    invoke-virtual {v0}, [Lcom/google/d/a/a/dh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/dh;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lcom/google/d/a/a/dh;->k:I

    return v0
.end method
