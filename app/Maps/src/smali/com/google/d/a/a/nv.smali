.class public final enum Lcom/google/d/a/a/nv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/nv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/nv;

.field public static final enum b:Lcom/google/d/a/a/nv;

.field public static final enum c:Lcom/google/d/a/a/nv;

.field private static final synthetic e:[Lcom/google/d/a/a/nv;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 396
    new-instance v0, Lcom/google/d/a/a/nv;

    const-string v1, "UNVERIFIED"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/d/a/a/nv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nv;->a:Lcom/google/d/a/a/nv;

    .line 400
    new-instance v0, Lcom/google/d/a/a/nv;

    const-string v1, "DOES_NOT_EXIST"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/a/a/nv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nv;->b:Lcom/google/d/a/a/nv;

    .line 404
    new-instance v0, Lcom/google/d/a/a/nv;

    const-string v1, "EXISTS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/nv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nv;->c:Lcom/google/d/a/a/nv;

    .line 391
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/d/a/a/nv;

    sget-object v1, Lcom/google/d/a/a/nv;->a:Lcom/google/d/a/a/nv;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/nv;->b:Lcom/google/d/a/a/nv;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/nv;->c:Lcom/google/d/a/a/nv;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/d/a/a/nv;->e:[Lcom/google/d/a/a/nv;

    .line 439
    new-instance v0, Lcom/google/d/a/a/nw;

    invoke-direct {v0}, Lcom/google/d/a/a/nw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 448
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 449
    iput p3, p0, Lcom/google/d/a/a/nv;->d:I

    .line 450
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/nv;
    .locals 1

    .prologue
    .line 426
    packed-switch p0, :pswitch_data_0

    .line 430
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 427
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/nv;->a:Lcom/google/d/a/a/nv;

    goto :goto_0

    .line 428
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/nv;->b:Lcom/google/d/a/a/nv;

    goto :goto_0

    .line 429
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/nv;->c:Lcom/google/d/a/a/nv;

    goto :goto_0

    .line 426
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/nv;
    .locals 1

    .prologue
    .line 391
    const-class v0, Lcom/google/d/a/a/nv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nv;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/nv;
    .locals 1

    .prologue
    .line 391
    sget-object v0, Lcom/google/d/a/a/nv;->e:[Lcom/google/d/a/a/nv;

    invoke-virtual {v0}, [Lcom/google/d/a/a/nv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/nv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 422
    iget v0, p0, Lcom/google/d/a/a/nv;->d:I

    return v0
.end method
