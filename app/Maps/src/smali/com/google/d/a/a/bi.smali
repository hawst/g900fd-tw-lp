.class public final Lcom/google/d/a/a/bi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/bl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/bi;",
            ">;"
        }
    .end annotation
.end field

.field static final p:Lcom/google/d/a/a/bi;

.field private static volatile s:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:I

.field e:Lcom/google/n/ao;

.field f:Ljava/lang/Object;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:I

.field i:Ljava/lang/Object;

.field j:Ljava/lang/Object;

.field k:Ljava/lang/Object;

.field l:Ljava/lang/Object;

.field m:Ljava/lang/Object;

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field o:Ljava/lang/Object;

.field private q:B

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 359
    new-instance v0, Lcom/google/d/a/a/bj;

    invoke-direct {v0}, Lcom/google/d/a/a/bj;-><init>()V

    sput-object v0, Lcom/google/d/a/a/bi;->PARSER:Lcom/google/n/ax;

    .line 1031
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/bi;->s:Lcom/google/n/aw;

    .line 2412
    new-instance v0, Lcom/google/d/a/a/bi;

    invoke-direct {v0}, Lcom/google/d/a/a/bi;-><init>()V

    sput-object v0, Lcom/google/d/a/a/bi;->p:Lcom/google/d/a/a/bi;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 207
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 475
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    .line 886
    iput-byte v2, p0, Lcom/google/d/a/a/bi;->q:B

    .line 962
    iput v2, p0, Lcom/google/d/a/a/bi;->r:I

    .line 208
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->b:Ljava/lang/Object;

    .line 209
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->c:Ljava/lang/Object;

    .line 210
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/bi;->d:I

    .line 211
    iget-object v0, p0, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 212
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->f:Ljava/lang/Object;

    .line 213
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    .line 214
    const/16 v0, 0x1111

    iput v0, p0, Lcom/google/d/a/a/bi;->h:I

    .line 215
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->i:Ljava/lang/Object;

    .line 216
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->j:Ljava/lang/Object;

    .line 217
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->k:Ljava/lang/Object;

    .line 218
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->l:Ljava/lang/Object;

    .line 219
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->m:Ljava/lang/Object;

    .line 220
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    .line 221
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/bi;->o:Ljava/lang/Object;

    .line 222
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v8, 0x1000

    const/16 v7, 0x20

    const/4 v0, 0x0

    .line 228
    invoke-direct {p0}, Lcom/google/d/a/a/bi;-><init>()V

    .line 231
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 234
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 235
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 236
    sparse-switch v4, :sswitch_data_0

    .line 241
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 243
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 239
    goto :goto_0

    .line 248
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 249
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 250
    iput-object v4, p0, Lcom/google/d/a/a/bi;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 344
    :catch_0
    move-exception v0

    .line 345
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 350
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x20

    if-ne v2, v7, :cond_1

    .line 351
    iget-object v2, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    .line 353
    :cond_1
    and-int/lit16 v1, v1, 0x1000

    if-ne v1, v8, :cond_2

    .line 354
    iget-object v1, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    .line 356
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/bi;->au:Lcom/google/n/bn;

    throw v0

    .line 254
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 255
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 256
    iput-object v4, p0, Lcom/google/d/a/a/bi;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 346
    :catch_1
    move-exception v0

    .line 347
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 348
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 260
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/bi;->a:I

    .line 261
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/bi;->d:I

    goto :goto_0

    .line 265
    :sswitch_4
    iget-object v4, p0, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 266
    iget v4, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/a/a/bi;->a:I

    goto :goto_0

    .line 270
    :sswitch_5
    and-int/lit8 v4, v1, 0x20

    if-eq v4, v7, :cond_3

    .line 271
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    .line 273
    or-int/lit8 v1, v1, 0x20

    .line 275
    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 276
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 275
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 280
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 281
    invoke-static {v4}, Lcom/google/d/a/a/bq;->a(I)Lcom/google/d/a/a/bq;

    move-result-object v5

    .line 282
    if-nez v5, :cond_4

    .line 283
    const/4 v5, 0x7

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 285
    :cond_4
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 286
    iput v4, p0, Lcom/google/d/a/a/bi;->h:I

    goto/16 :goto_0

    .line 291
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 292
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 293
    iput-object v4, p0, Lcom/google/d/a/a/bi;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 297
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 298
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 299
    iput-object v4, p0, Lcom/google/d/a/a/bi;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 303
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 304
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 305
    iput-object v4, p0, Lcom/google/d/a/a/bi;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 309
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 310
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 311
    iput-object v4, p0, Lcom/google/d/a/a/bi;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 315
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 316
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit16 v5, v5, 0x400

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 317
    iput-object v4, p0, Lcom/google/d/a/a/bi;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 321
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 322
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 323
    iput-object v4, p0, Lcom/google/d/a/a/bi;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 327
    :sswitch_d
    and-int/lit16 v4, v1, 0x1000

    if-eq v4, v8, :cond_5

    .line 328
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    .line 330
    or-int/lit16 v1, v1, 0x1000

    .line 332
    :cond_5
    iget-object v4, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 333
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 332
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 337
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 338
    iget v5, p0, Lcom/google/d/a/a/bi;->a:I

    or-int/lit16 v5, v5, 0x800

    iput v5, p0, Lcom/google/d/a/a/bi;->a:I

    .line 339
    iput-object v4, p0, Lcom/google/d/a/a/bi;->o:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 350
    :cond_6
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v7, :cond_7

    .line 351
    iget-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    .line 353
    :cond_7
    and-int/lit16 v0, v1, 0x1000

    if-ne v0, v8, :cond_8

    .line 354
    iget-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    .line 356
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->au:Lcom/google/n/bn;

    .line 357
    return-void

    .line 236
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 205
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 475
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    .line 886
    iput-byte v1, p0, Lcom/google/d/a/a/bi;->q:B

    .line 962
    iput v1, p0, Lcom/google/d/a/a/bi;->r:I

    .line 206
    return-void
.end method

.method public static d()Lcom/google/d/a/a/bi;
    .locals 1

    .prologue
    .line 2415
    sget-object v0, Lcom/google/d/a/a/bi;->p:Lcom/google/d/a/a/bi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/bk;
    .locals 1

    .prologue
    .line 1093
    new-instance v0, Lcom/google/d/a/a/bk;

    invoke-direct {v0}, Lcom/google/d/a/a/bk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/bi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371
    sget-object v0, Lcom/google/d/a/a/bi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 916
    invoke-virtual {p0}, Lcom/google/d/a/a/bi;->c()I

    .line 917
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 918
    iget-object v0, p0, Lcom/google/d/a/a/bi;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 920
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 921
    iget-object v0, p0, Lcom/google/d/a/a/bi;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 923
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 924
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/bi;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_6

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 926
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 927
    iget-object v0, p0, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_3
    move v1, v2

    .line 929
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 930
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 929
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 918
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 921
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 924
    :cond_6
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 932
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 933
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/d/a/a/bi;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_f

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 935
    :cond_8
    :goto_4
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 936
    iget-object v0, p0, Lcom/google/d/a/a/bi;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->i:Ljava/lang/Object;

    :goto_5
    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 938
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 939
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/d/a/a/bi;->j:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->j:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 941
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 942
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/d/a/a/bi;->k:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->k:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 944
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_c

    .line 945
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/d/a/a/bi;->l:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->l:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 947
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_d

    .line 948
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/d/a/a/bi;->m:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->m:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 950
    :cond_d
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_e

    .line 951
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/d/a/a/bi;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->f:Ljava/lang/Object;

    :goto_a
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 953
    :cond_e
    :goto_b
    iget-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_16

    .line 954
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 953
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 933
    :cond_f
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 936
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 939
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 942
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 945
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 948
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    .line 951
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto :goto_a

    .line 956
    :cond_16
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_17

    .line 957
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/d/a/a/bi;->o:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->o:Ljava/lang/Object;

    :goto_c
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 959
    :cond_17
    iget-object v0, p0, Lcom/google/d/a/a/bi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 960
    return-void

    .line 957
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto :goto_c
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 888
    iget-byte v0, p0, Lcom/google/d/a/a/bi;->q:B

    .line 889
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 911
    :cond_0
    :goto_0
    return v2

    .line 890
    :cond_1
    if-eqz v0, :cond_0

    .line 892
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 893
    iget-object v0, p0, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 894
    iput-byte v2, p0, Lcom/google/d/a/a/bi;->q:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 892
    goto :goto_1

    :cond_3
    move v1, v2

    .line 898
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 899
    iget-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/jn;->d()Lcom/google/d/a/a/jn;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/jn;

    invoke-virtual {v0}, Lcom/google/d/a/a/jn;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 900
    iput-byte v2, p0, Lcom/google/d/a/a/bi;->q:B

    goto :goto_0

    .line 898
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 904
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 905
    iget-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 906
    iput-byte v2, p0, Lcom/google/d/a/a/bi;->q:B

    goto :goto_0

    .line 904
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 910
    :cond_7
    iput-byte v3, p0, Lcom/google/d/a/a/bi;->q:B

    move v2, v3

    .line 911
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 964
    iget v0, p0, Lcom/google/d/a/a/bi;->r:I

    .line 965
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1026
    :goto_0
    return v0

    .line 968
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_19

    .line 970
    iget-object v0, p0, Lcom/google/d/a/a/bi;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 972
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 974
    iget-object v0, p0, Lcom/google/d/a/a/bi;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 976
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 977
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/d/a/a/bi;->d:I

    .line 978
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 980
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_3

    .line 981
    iget-object v0, p0, Lcom/google/d/a/a/bi;->e:Lcom/google/n/ao;

    .line 982
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    :cond_3
    move v4, v1

    move v1, v2

    .line 984
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 985
    const/4 v5, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/bi;->g:Ljava/util/List;

    .line 986
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 984
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 970
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 974
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_6
    move v0, v3

    .line 978
    goto :goto_4

    .line 988
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 989
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/d/a/a/bi;->h:I

    .line 990
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_f

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 992
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 993
    const/16 v1, 0x8

    .line 994
    iget-object v0, p0, Lcom/google/d/a/a/bi;->i:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->i:Ljava/lang/Object;

    :goto_7
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 996
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 997
    const/16 v1, 0x9

    .line 998
    iget-object v0, p0, Lcom/google/d/a/a/bi;->j:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->j:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1000
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 1002
    iget-object v0, p0, Lcom/google/d/a/a/bi;->k:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->k:Ljava/lang/Object;

    :goto_9
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1004
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_c

    .line 1005
    const/16 v1, 0xb

    .line 1006
    iget-object v0, p0, Lcom/google/d/a/a/bi;->l:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->l:Ljava/lang/Object;

    :goto_a
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1008
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_d

    .line 1009
    const/16 v1, 0xc

    .line 1010
    iget-object v0, p0, Lcom/google/d/a/a/bi;->m:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->m:Ljava/lang/Object;

    :goto_b
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1012
    :cond_d
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_e

    .line 1013
    const/16 v1, 0xd

    .line 1014
    iget-object v0, p0, Lcom/google/d/a/a/bi;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->f:Ljava/lang/Object;

    :goto_c
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    :cond_e
    move v1, v2

    .line 1016
    :goto_d
    iget-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_16

    .line 1017
    const/16 v3, 0xe

    iget-object v0, p0, Lcom/google/d/a/a/bi;->n:Ljava/util/List;

    .line 1018
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 1016
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    :cond_f
    move v0, v3

    .line 990
    goto/16 :goto_6

    .line 994
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 998
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 1002
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 1006
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    .line 1010
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_b

    .line 1014
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto :goto_c

    .line 1020
    :cond_16
    iget v0, p0, Lcom/google/d/a/a/bi;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_17

    .line 1021
    const/16 v1, 0xf

    .line 1022
    iget-object v0, p0, Lcom/google/d/a/a/bi;->o:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bi;->o:Ljava/lang/Object;

    :goto_e
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1024
    :cond_17
    iget-object v0, p0, Lcom/google/d/a/a/bi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1025
    iput v0, p0, Lcom/google/d/a/a/bi;->r:I

    goto/16 :goto_0

    .line 1022
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto :goto_e

    :cond_19
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 199
    invoke-static {}, Lcom/google/d/a/a/bi;->newBuilder()Lcom/google/d/a/a/bk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/bk;->a(Lcom/google/d/a/a/bi;)Lcom/google/d/a/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 199
    invoke-static {}, Lcom/google/d/a/a/bi;->newBuilder()Lcom/google/d/a/a/bk;

    move-result-object v0

    return-object v0
.end method
