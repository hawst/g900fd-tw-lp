.class public final enum Lcom/google/d/a/a/aw;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/aw;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/aw;

.field public static final enum b:Lcom/google/d/a/a/aw;

.field public static final enum c:Lcom/google/d/a/a/aw;

.field public static final enum d:Lcom/google/d/a/a/aw;

.field public static final enum e:Lcom/google/d/a/a/aw;

.field public static final enum f:Lcom/google/d/a/a/aw;

.field public static final enum g:Lcom/google/d/a/a/aw;

.field private static final synthetic i:[Lcom/google/d/a/a/aw;


# instance fields
.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 180
    new-instance v0, Lcom/google/d/a/a/aw;

    const-string v1, "STATUS_NORMAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/aw;->a:Lcom/google/d/a/a/aw;

    .line 184
    new-instance v0, Lcom/google/d/a/a/aw;

    const-string v1, "STATUS_DISPUTED"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/d/a/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/aw;->b:Lcom/google/d/a/a/aw;

    .line 188
    new-instance v0, Lcom/google/d/a/a/aw;

    const-string v1, "STATUS_UNSURVEYED"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/d/a/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/aw;->c:Lcom/google/d/a/a/aw;

    .line 192
    new-instance v0, Lcom/google/d/a/a/aw;

    const-string v1, "STATUS_INTERNATIONAL_WATER"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/d/a/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/aw;->d:Lcom/google/d/a/a/aw;

    .line 196
    new-instance v0, Lcom/google/d/a/a/aw;

    const-string v1, "STATUS_NEVER_DISPLAY"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/d/a/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/aw;->e:Lcom/google/d/a/a/aw;

    .line 200
    new-instance v0, Lcom/google/d/a/a/aw;

    const-string v1, "STATUS_TREATY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/aw;->f:Lcom/google/d/a/a/aw;

    .line 204
    new-instance v0, Lcom/google/d/a/a/aw;

    const-string v1, "STATUS_PROVISIONAL"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/aw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/aw;->g:Lcom/google/d/a/a/aw;

    .line 175
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/d/a/a/aw;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/d/a/a/aw;->a:Lcom/google/d/a/a/aw;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/aw;->b:Lcom/google/d/a/a/aw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/aw;->c:Lcom/google/d/a/a/aw;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/aw;->d:Lcom/google/d/a/a/aw;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/aw;->e:Lcom/google/d/a/a/aw;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/aw;->f:Lcom/google/d/a/a/aw;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/aw;->g:Lcom/google/d/a/a/aw;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/aw;->i:[Lcom/google/d/a/a/aw;

    .line 259
    new-instance v0, Lcom/google/d/a/a/ax;

    invoke-direct {v0}, Lcom/google/d/a/a/ax;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 268
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 269
    iput p3, p0, Lcom/google/d/a/a/aw;->h:I

    .line 270
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/aw;
    .locals 1

    .prologue
    .line 242
    packed-switch p0, :pswitch_data_0

    .line 250
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 243
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/aw;->a:Lcom/google/d/a/a/aw;

    goto :goto_0

    .line 244
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/aw;->b:Lcom/google/d/a/a/aw;

    goto :goto_0

    .line 245
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/aw;->c:Lcom/google/d/a/a/aw;

    goto :goto_0

    .line 246
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/aw;->d:Lcom/google/d/a/a/aw;

    goto :goto_0

    .line 247
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/aw;->e:Lcom/google/d/a/a/aw;

    goto :goto_0

    .line 248
    :pswitch_5
    sget-object v0, Lcom/google/d/a/a/aw;->f:Lcom/google/d/a/a/aw;

    goto :goto_0

    .line 249
    :pswitch_6
    sget-object v0, Lcom/google/d/a/a/aw;->g:Lcom/google/d/a/a/aw;

    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/aw;
    .locals 1

    .prologue
    .line 175
    const-class v0, Lcom/google/d/a/a/aw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/aw;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/aw;
    .locals 1

    .prologue
    .line 175
    sget-object v0, Lcom/google/d/a/a/aw;->i:[Lcom/google/d/a/a/aw;

    invoke-virtual {v0}, [Lcom/google/d/a/a/aw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/aw;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/google/d/a/a/aw;->h:I

    return v0
.end method
