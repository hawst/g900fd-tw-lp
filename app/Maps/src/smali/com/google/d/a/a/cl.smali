.class public final Lcom/google/d/a/a/cl;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/co;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/cl;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/d/a/a/cl;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Z

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/d/a/a/cm;

    invoke-direct {v0}, Lcom/google/d/a/a/cm;-><init>()V

    sput-object v0, Lcom/google/d/a/a/cl;->PARSER:Lcom/google/n/ax;

    .line 226
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/cl;->h:Lcom/google/n/aw;

    .line 500
    new-instance v0, Lcom/google/d/a/a/cl;

    invoke-direct {v0}, Lcom/google/d/a/a/cl;-><init>()V

    sput-object v0, Lcom/google/d/a/a/cl;->e:Lcom/google/d/a/a/cl;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 125
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cl;->b:Lcom/google/n/ao;

    .line 170
    iput-byte v2, p0, Lcom/google/d/a/a/cl;->f:B

    .line 201
    iput v2, p0, Lcom/google/d/a/a/cl;->g:I

    .line 54
    iget-object v0, p0, Lcom/google/d/a/a/cl;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 55
    iput-boolean v3, p0, Lcom/google/d/a/a/cl;->c:Z

    .line 56
    iput-boolean v3, p0, Lcom/google/d/a/a/cl;->d:Z

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0}, Lcom/google/d/a/a/cl;-><init>()V

    .line 64
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 69
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 71
    sparse-switch v0, :sswitch_data_0

    .line 76
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 78
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    iget-object v0, p0, Lcom/google/d/a/a/cl;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 84
    iget v0, p0, Lcom/google/d/a/a/cl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/cl;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/cl;->au:Lcom/google/n/bn;

    throw v0

    .line 88
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/d/a/a/cl;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/cl;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/cl;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 89
    goto :goto_1

    .line 93
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/d/a/a/cl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/cl;->a:I

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/d/a/a/cl;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 105
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cl;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 51
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 125
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cl;->b:Lcom/google/n/ao;

    .line 170
    iput-byte v1, p0, Lcom/google/d/a/a/cl;->f:B

    .line 201
    iput v1, p0, Lcom/google/d/a/a/cl;->g:I

    .line 52
    return-void
.end method

.method public static d()Lcom/google/d/a/a/cl;
    .locals 1

    .prologue
    .line 503
    sget-object v0, Lcom/google/d/a/a/cl;->e:Lcom/google/d/a/a/cl;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/cn;
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lcom/google/d/a/a/cn;

    invoke-direct {v0}, Lcom/google/d/a/a/cn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/cl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/d/a/a/cl;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 188
    invoke-virtual {p0}, Lcom/google/d/a/a/cl;->c()I

    .line 189
    iget v0, p0, Lcom/google/d/a/a/cl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/d/a/a/cl;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 192
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/cl;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 193
    iget-boolean v0, p0, Lcom/google/d/a/a/cl;->c:Z

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 195
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/cl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 196
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/d/a/a/cl;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_4

    :goto_1
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/cl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 199
    return-void

    :cond_3
    move v0, v2

    .line 193
    goto :goto_0

    :cond_4
    move v1, v2

    .line 196
    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 172
    iget-byte v0, p0, Lcom/google/d/a/a/cl;->f:B

    .line 173
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 183
    :goto_0
    return v0

    .line 174
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 176
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/cl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 177
    iget-object v0, p0, Lcom/google/d/a/a/cl;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 178
    iput-byte v2, p0, Lcom/google/d/a/a/cl;->f:B

    move v0, v2

    .line 179
    goto :goto_0

    :cond_2
    move v0, v2

    .line 176
    goto :goto_1

    .line 182
    :cond_3
    iput-byte v1, p0, Lcom/google/d/a/a/cl;->f:B

    move v0, v1

    .line 183
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 203
    iget v0, p0, Lcom/google/d/a/a/cl;->g:I

    .line 204
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 221
    :goto_0
    return v0

    .line 207
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/cl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 208
    iget-object v0, p0, Lcom/google/d/a/a/cl;->b:Lcom/google/n/ao;

    .line 209
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 211
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/cl;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 212
    iget-boolean v2, p0, Lcom/google/d/a/a/cl;->c:Z

    .line 213
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 215
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/cl;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 216
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/d/a/a/cl;->d:Z

    .line 217
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 219
    :cond_2
    iget-object v1, p0, Lcom/google/d/a/a/cl;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    iput v0, p0, Lcom/google/d/a/a/cl;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/d/a/a/cl;->newBuilder()Lcom/google/d/a/a/cn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/cn;->a(Lcom/google/d/a/a/cl;)Lcom/google/d/a/a/cn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/d/a/a/cl;->newBuilder()Lcom/google/d/a/a/cn;

    move-result-object v0

    return-object v0
.end method
