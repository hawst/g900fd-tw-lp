.class public final Lcom/google/d/a/a/qr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/qu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/qp;",
        "Lcom/google/d/a/a/qr;",
        ">;",
        "Lcom/google/d/a/a/qu;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 568
    sget-object v0, Lcom/google/d/a/a/qp;->g:Lcom/google/d/a/a/qp;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 672
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/qr;->b:I

    .line 709
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    .line 846
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    .line 1014
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/qr;->f:I

    .line 569
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/qp;)Lcom/google/d/a/a/qr;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 620
    invoke-static {}, Lcom/google/d/a/a/qp;->d()Lcom/google/d/a/a/qp;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 651
    :goto_0
    return-object p0

    .line 621
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 622
    iget v2, p1, Lcom/google/d/a/a/qp;->b:I

    invoke-static {v2}, Lcom/google/d/a/a/qs;->a(I)Lcom/google/d/a/a/qs;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/d/a/a/qs;->a:Lcom/google/d/a/a/qs;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 621
    goto :goto_1

    .line 622
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/qr;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/qr;->a:I

    iget v2, v2, Lcom/google/d/a/a/qs;->g:I

    iput v2, p0, Lcom/google/d/a/a/qr;->b:I

    .line 624
    :cond_4
    iget-object v2, p1, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 625
    iget-object v2, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 626
    iget-object v2, p1, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    .line 627
    iget v2, p0, Lcom/google/d/a/a/qr;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/d/a/a/qr;->a:I

    .line 634
    :cond_5
    :goto_2
    iget-object v2, p1, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 635
    iget-object v2, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 636
    iget-object v2, p1, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    .line 637
    iget v2, p0, Lcom/google/d/a/a/qr;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/a/a/qr;->a:I

    .line 644
    :cond_6
    :goto_3
    iget v2, p1, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 645
    iget v2, p1, Lcom/google/d/a/a/qp;->e:I

    iget v3, p0, Lcom/google/d/a/a/qr;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/qr;->a:I

    iput v2, p0, Lcom/google/d/a/a/qr;->e:I

    .line 647
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_e

    :goto_5
    if-eqz v0, :cond_8

    .line 648
    iget v0, p1, Lcom/google/d/a/a/qp;->f:I

    iget v1, p0, Lcom/google/d/a/a/qr;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/d/a/a/qr;->a:I

    iput v0, p0, Lcom/google/d/a/a/qr;->f:I

    .line 650
    :cond_8
    iget-object v0, p1, Lcom/google/d/a/a/qp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 629
    :cond_9
    iget v2, p0, Lcom/google/d/a/a/qr;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/qr;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/qr;->a:I

    .line 630
    :cond_a
    iget-object v2, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 639
    :cond_b
    iget v2, p0, Lcom/google/d/a/a/qr;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v5, :cond_c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/qr;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/qr;->a:I

    .line 640
    :cond_c
    iget-object v2, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_d
    move v2, v1

    .line 644
    goto :goto_4

    :cond_e
    move v0, v1

    .line 647
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 560
    new-instance v2, Lcom/google/d/a/a/qp;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/qp;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/qr;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/qr;->b:I

    iput v1, v2, Lcom/google/d/a/a/qp;->b:I

    iget v1, p0, Lcom/google/d/a/a/qr;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qr;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/d/a/a/qr;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qr;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/qr;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/d/a/a/qr;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/qr;->e:I

    iput v1, v2, Lcom/google/d/a/a/qp;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget v1, p0, Lcom/google/d/a/a/qr;->f:I

    iput v1, v2, Lcom/google/d/a/a/qp;->f:I

    iput v0, v2, Lcom/google/d/a/a/qp;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 560
    check-cast p1, Lcom/google/d/a/a/qp;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/qr;->a(Lcom/google/d/a/a/qp;)Lcom/google/d/a/a/qr;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 655
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 656
    iget-object v0, p0, Lcom/google/d/a/a/qr;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 667
    :cond_0
    :goto_1
    return v2

    .line 655
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 661
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 662
    iget-object v0, p0, Lcom/google/d/a/a/qr;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 667
    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method
