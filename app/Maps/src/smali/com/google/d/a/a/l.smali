.class public final Lcom/google/d/a/a/l;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/o;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/l;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/d/a/a/l;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Z

.field f:Z

.field g:Z

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235
    new-instance v0, Lcom/google/d/a/a/m;

    invoke-direct {v0}, Lcom/google/d/a/a/m;-><init>()V

    sput-object v0, Lcom/google/d/a/a/l;->PARSER:Lcom/google/n/ax;

    .line 587
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/l;->m:Lcom/google/n/aw;

    .line 1530
    new-instance v0, Lcom/google/d/a/a/l;

    invoke-direct {v0}, Lcom/google/d/a/a/l;-><init>()V

    sput-object v0, Lcom/google/d/a/a/l;->j:Lcom/google/d/a/a/l;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 469
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    .line 484
    iput-byte v3, p0, Lcom/google/d/a/a/l;->k:B

    .line 542
    iput v3, p0, Lcom/google/d/a/a/l;->l:I

    .line 119
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    .line 120
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    .line 121
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    .line 122
    iput-boolean v1, p0, Lcom/google/d/a/a/l;->e:Z

    .line 123
    iput-boolean v1, p0, Lcom/google/d/a/a/l;->f:Z

    .line 124
    iput-boolean v1, p0, Lcom/google/d/a/a/l;->g:Z

    .line 125
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    .line 126
    iget-object v0, p0, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 127
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/d/a/a/l;-><init>()V

    .line 134
    const/4 v1, 0x0

    .line 136
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 138
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 139
    :cond_0
    :goto_0
    if-nez v2, :cond_9

    .line 140
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 141
    sparse-switch v1, :sswitch_data_0

    .line 146
    invoke-virtual {v3, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 143
    :sswitch_0
    const/4 v1, 0x1

    move v2, v1

    .line 144
    goto :goto_0

    .line 153
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    const/4 v4, 0x1

    if-eq v1, v4, :cond_10

    .line 154
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 156
    or-int/lit8 v1, v0, 0x1

    .line 158
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 159
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 158
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 160
    goto :goto_0

    .line 163
    :sswitch_2
    :try_start_2
    iget v1, p0, Lcom/google/d/a/a/l;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/d/a/a/l;->a:I

    .line 164
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Lcom/google/d/a/a/l;->e:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 214
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 215
    :goto_3
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 220
    :catchall_0
    move-exception v0

    :goto_4
    and-int/lit8 v2, v1, 0x1

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 221
    iget-object v2, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    .line 223
    :cond_1
    and-int/lit8 v2, v1, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_2

    .line 224
    iget-object v2, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    .line 226
    :cond_2
    and-int/lit8 v2, v1, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_3

    .line 227
    iget-object v2, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    .line 229
    :cond_3
    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 230
    iget-object v1, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    .line 232
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/l;->au:Lcom/google/n/bn;

    throw v0

    .line 164
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 168
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/d/a/a/l;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/d/a/a/l;->a:I

    .line 169
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_5
    iput-boolean v1, p0, Lcom/google/d/a/a/l;->f:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 216
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 217
    :goto_6
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 218
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 169
    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    .line 173
    :sswitch_4
    and-int/lit8 v1, v0, 0x40

    const/16 v4, 0x40

    if-eq v1, v4, :cond_f

    .line 174
    :try_start_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 176
    or-int/lit8 v1, v0, 0x40

    .line 178
    :goto_7
    :try_start_7
    iget-object v0, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 179
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 178
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 180
    goto/16 :goto_0

    .line 183
    :sswitch_5
    and-int/lit8 v1, v0, 0x2

    const/4 v4, 0x2

    if-eq v1, v4, :cond_e

    .line 184
    :try_start_8
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 186
    or-int/lit8 v1, v0, 0x2

    .line 188
    :goto_8
    :try_start_9
    iget-object v0, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 189
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 188
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 190
    goto/16 :goto_0

    .line 193
    :sswitch_6
    :try_start_a
    iget v1, p0, Lcom/google/d/a/a/l;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/d/a/a/l;->a:I

    .line 194
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_9
    iput-boolean v1, p0, Lcom/google/d/a/a/l;->g:Z

    goto/16 :goto_0

    .line 220
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_4

    .line 194
    :cond_7
    const/4 v1, 0x0

    goto :goto_9

    .line 198
    :sswitch_7
    iget-object v1, p0, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    .line 199
    iget v1, p0, Lcom/google/d/a/a/l;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/d/a/a/l;->a:I

    goto/16 :goto_0

    .line 203
    :sswitch_8
    and-int/lit8 v1, v0, 0x4

    const/4 v4, 0x4

    if-eq v1, v4, :cond_8

    .line 204
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    .line 206
    or-int/lit8 v0, v0, 0x4

    .line 208
    :cond_8
    iget-object v1, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 209
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 208
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_0

    .line 220
    :cond_9
    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    .line 221
    iget-object v1, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    .line 223
    :cond_a
    and-int/lit8 v1, v0, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_b

    .line 224
    iget-object v1, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    .line 226
    :cond_b
    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_c

    .line 227
    iget-object v1, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    .line 229
    :cond_c
    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_d

    .line 230
    iget-object v0, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    .line 232
    :cond_d
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/l;->au:Lcom/google/n/bn;

    .line 233
    return-void

    .line 216
    :catch_2
    move-exception v0

    goto/16 :goto_6

    .line 214
    :catch_3
    move-exception v0

    goto/16 :goto_3

    :cond_e
    move v1, v0

    goto/16 :goto_8

    :cond_f
    move v1, v0

    goto/16 :goto_7

    :cond_10
    move v1, v0

    goto/16 :goto_1

    .line 141
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 116
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 469
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    .line 484
    iput-byte v1, p0, Lcom/google/d/a/a/l;->k:B

    .line 542
    iput v1, p0, Lcom/google/d/a/a/l;->l:I

    .line 117
    return-void
.end method

.method public static d()Lcom/google/d/a/a/l;
    .locals 1

    .prologue
    .line 1533
    sget-object v0, Lcom/google/d/a/a/l;->j:Lcom/google/d/a/a/l;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/n;
    .locals 1

    .prologue
    .line 649
    new-instance v0, Lcom/google/d/a/a/n;

    invoke-direct {v0}, Lcom/google/d/a/a/n;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    sget-object v0, Lcom/google/d/a/a/l;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 514
    invoke-virtual {p0}, Lcom/google/d/a/a/l;->c()I

    move v1, v2

    .line 515
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 515
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 518
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 519
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/d/a/a/l;->e:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_3

    move v0, v3

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 521
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 522
    iget-boolean v0, p0, Lcom/google/d/a/a/l;->f:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_4

    move v0, v3

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_2
    move v1, v2

    .line 524
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 525
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 524
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v2

    .line 519
    goto :goto_1

    :cond_4
    move v0, v2

    .line 522
    goto :goto_2

    :cond_5
    move v1, v2

    .line 527
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 528
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 527
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 530
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_7

    .line 531
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/d/a/a/l;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_9

    :goto_5
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 533
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_8

    .line 534
    iget-object v0, p0, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 536
    :cond_8
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 537
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 536
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_9
    move v3, v2

    .line 531
    goto :goto_5

    .line 539
    :cond_a
    iget-object v0, p0, Lcom/google/d/a/a/l;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 540
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 486
    iget-byte v0, p0, Lcom/google/d/a/a/l;->k:B

    .line 487
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 509
    :cond_0
    :goto_0
    return v2

    .line 488
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 490
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 491
    iget-object v0, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/q;->d()Lcom/google/d/a/a/q;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/q;

    invoke-virtual {v0}, Lcom/google/d/a/a/q;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 492
    iput-byte v2, p0, Lcom/google/d/a/a/l;->k:B

    goto :goto_0

    .line 490
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 496
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 497
    iget-object v0, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/q;->d()Lcom/google/d/a/a/q;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/q;

    invoke-virtual {v0}, Lcom/google/d/a/a/q;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 498
    iput-byte v2, p0, Lcom/google/d/a/a/l;->k:B

    goto :goto_0

    .line 496
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 502
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 503
    iget-object v0, p0, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 504
    iput-byte v2, p0, Lcom/google/d/a/a/l;->k:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 502
    goto :goto_3

    .line 508
    :cond_7
    iput-byte v3, p0, Lcom/google/d/a/a/l;->k:B

    move v2, v3

    .line 509
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 544
    iget v0, p0, Lcom/google/d/a/a/l;->l:I

    .line 545
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 582
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 548
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 549
    iget-object v0, p0, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    .line 550
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 548
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 552
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 553
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/d/a/a/l;->e:Z

    .line 554
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 556
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_3

    .line 557
    iget-boolean v0, p0, Lcom/google/d/a/a/l;->f:Z

    .line 558
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_3
    move v1, v2

    .line 560
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 561
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    .line 562
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 560
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    move v1, v2

    .line 564
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 565
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    .line 566
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 564
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 568
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_6

    .line 569
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/d/a/a/l;->g:Z

    .line 570
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 572
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_7

    .line 573
    iget-object v0, p0, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    .line 574
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_7
    move v1, v2

    .line 576
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 577
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    .line 578
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 576
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 580
    :cond_8
    iget-object v0, p0, Lcom/google/d/a/a/l;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 581
    iput v0, p0, Lcom/google/d/a/a/l;->l:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/google/d/a/a/l;->newBuilder()Lcom/google/d/a/a/n;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/n;->a(Lcom/google/d/a/a/l;)Lcom/google/d/a/a/n;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/google/d/a/a/l;->newBuilder()Lcom/google/d/a/a/n;

    move-result-object v0

    return-object v0
.end method
