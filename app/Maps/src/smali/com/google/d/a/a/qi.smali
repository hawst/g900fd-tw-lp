.class public final Lcom/google/d/a/a/qi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/qn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/qi;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/a/a/qi;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field e:I

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/google/d/a/a/qj;

    invoke-direct {v0}, Lcom/google/d/a/a/qj;-><init>()V

    sput-object v0, Lcom/google/d/a/a/qi;->PARSER:Lcom/google/n/ax;

    .line 614
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/qi;->j:Lcom/google/n/aw;

    .line 1176
    new-instance v0, Lcom/google/d/a/a/qi;

    invoke-direct {v0}, Lcom/google/d/a/a/qi;-><init>()V

    sput-object v0, Lcom/google/d/a/a/qi;->g:Lcom/google/d/a/a/qi;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 81
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 538
    iput-byte v0, p0, Lcom/google/d/a/a/qi;->h:B

    .line 581
    iput v0, p0, Lcom/google/d/a/a/qi;->i:I

    .line 82
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    .line 83
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/qi;->c:I

    .line 84
    iput v1, p0, Lcom/google/d/a/a/qi;->d:I

    .line 85
    iput v1, p0, Lcom/google/d/a/a/qi;->e:I

    .line 86
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    .line 87
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/16 v7, 0x10

    const/4 v3, 0x1

    .line 93
    invoke-direct {p0}, Lcom/google/d/a/a/qi;-><init>()V

    .line 96
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 99
    :cond_0
    :goto_0
    if-nez v2, :cond_5

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 101
    sparse-switch v1, :sswitch_data_0

    .line 106
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 108
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 104
    goto :goto_0

    .line 113
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_8

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 116
    or-int/lit8 v1, v0, 0x1

    .line 118
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 119
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 118
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 120
    goto :goto_0

    .line 123
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 124
    invoke-static {v1}, Lcom/google/d/a/a/ql;->a(I)Lcom/google/d/a/a/ql;

    move-result-object v5

    .line 125
    if-nez v5, :cond_3

    .line 126
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 155
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 156
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 161
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_1

    .line 162
    iget-object v2, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    .line 164
    :cond_1
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_2

    .line 165
    iget-object v1, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    .line 167
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qi;->au:Lcom/google/n/bn;

    throw v0

    .line 128
    :cond_3
    :try_start_4
    iget v5, p0, Lcom/google/d/a/a/qi;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/qi;->a:I

    .line 129
    iput v1, p0, Lcom/google/d/a/a/qi;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 157
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 158
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 159
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 134
    :sswitch_3
    :try_start_6
    iget v1, p0, Lcom/google/d/a/a/qi;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/d/a/a/qi;->a:I

    .line 135
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v1

    iput v1, p0, Lcom/google/d/a/a/qi;->d:I

    goto/16 :goto_0

    .line 161
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_3

    .line 139
    :sswitch_4
    iget v1, p0, Lcom/google/d/a/a/qi;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/d/a/a/qi;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v1

    iput v1, p0, Lcom/google/d/a/a/qi;->e:I

    goto/16 :goto_0

    .line 144
    :sswitch_5
    and-int/lit8 v1, v0, 0x10

    if-eq v1, v7, :cond_4

    .line 145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    .line 147
    or-int/lit8 v0, v0, 0x10

    .line 149
    :cond_4
    iget-object v1, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 150
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 149
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 161
    :cond_5
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_6

    .line 162
    iget-object v1, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    .line 164
    :cond_6
    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_7

    .line 165
    iget-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    .line 167
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qi;->au:Lcom/google/n/bn;

    .line 168
    return-void

    .line 157
    :catch_2
    move-exception v0

    goto :goto_4

    .line 155
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_8
    move v1, v0

    goto/16 :goto_1

    .line 101
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x25 -> :sswitch_3
        0x2d -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 79
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 538
    iput-byte v0, p0, Lcom/google/d/a/a/qi;->h:B

    .line 581
    iput v0, p0, Lcom/google/d/a/a/qi;->i:I

    .line 80
    return-void
.end method

.method public static d()Lcom/google/d/a/a/qi;
    .locals 1

    .prologue
    .line 1179
    sget-object v0, Lcom/google/d/a/a/qi;->g:Lcom/google/d/a/a/qi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/qk;
    .locals 1

    .prologue
    .line 676
    new-instance v0, Lcom/google/d/a/a/qk;

    invoke-direct {v0}, Lcom/google/d/a/a/qk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/qi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    sget-object v0, Lcom/google/d/a/a/qi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 562
    invoke-virtual {p0}, Lcom/google/d/a/a/qi;->c()I

    move v1, v2

    .line 563
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 563
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 566
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_1

    .line 567
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/qi;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_4

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 569
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 570
    iget v0, p0, Lcom/google/d/a/a/qi;->d:I

    invoke-static {v7, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 572
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_3

    .line 573
    iget v0, p0, Lcom/google/d/a/a/qi;->e:I

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 575
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 576
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 575
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 567
    :cond_4
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 578
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/qi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 579
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 540
    iget-byte v0, p0, Lcom/google/d/a/a/qi;->h:B

    .line 541
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 557
    :cond_0
    :goto_0
    return v2

    .line 542
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 544
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 545
    iget-object v0, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 546
    iput-byte v2, p0, Lcom/google/d/a/a/qi;->h:B

    goto :goto_0

    .line 544
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 550
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 551
    iget-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 552
    iput-byte v2, p0, Lcom/google/d/a/a/qi;->h:B

    goto :goto_0

    .line 550
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 556
    :cond_5
    iput-byte v3, p0, Lcom/google/d/a/a/qi;->h:B

    move v2, v3

    .line 557
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 583
    iget v0, p0, Lcom/google/d/a/a/qi;->i:I

    .line 584
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 609
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 587
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/google/d/a/a/qi;->b:Ljava/util/List;

    .line 589
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 587
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 591
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 592
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/qi;->c:I

    .line 593
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v1, :cond_5

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 595
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 596
    iget v0, p0, Lcom/google/d/a/a/qi;->d:I

    .line 597
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 599
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/qi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_4

    .line 600
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/d/a/a/qi;->e:I

    .line 601
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    :cond_4
    move v1, v2

    .line 603
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 604
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/qi;->f:Ljava/util/List;

    .line 605
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 603
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 593
    :cond_5
    const/16 v0, 0xa

    goto :goto_2

    .line 607
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/qi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 608
    iput v0, p0, Lcom/google/d/a/a/qi;->i:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/d/a/a/qi;->newBuilder()Lcom/google/d/a/a/qk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/qk;->a(Lcom/google/d/a/a/qi;)Lcom/google/d/a/a/qk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/d/a/a/qi;->newBuilder()Lcom/google/d/a/a/qk;

    move-result-object v0

    return-object v0
.end method
