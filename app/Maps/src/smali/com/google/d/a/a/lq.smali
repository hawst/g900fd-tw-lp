.class public final Lcom/google/d/a/a/lq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/lt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/lq;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/d/a/a/lq;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:F

.field e:F

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7808
    new-instance v0, Lcom/google/d/a/a/lr;

    invoke-direct {v0}, Lcom/google/d/a/a/lr;-><init>()V

    sput-object v0, Lcom/google/d/a/a/lq;->PARSER:Lcom/google/n/ax;

    .line 7948
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/lq;->i:Lcom/google/n/aw;

    .line 8263
    new-instance v0, Lcom/google/d/a/a/lq;

    invoke-direct {v0}, Lcom/google/d/a/a/lq;-><init>()V

    sput-object v0, Lcom/google/d/a/a/lq;->f:Lcom/google/d/a/a/lq;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 7747
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 7825
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lq;->b:Lcom/google/n/ao;

    .line 7885
    iput-byte v2, p0, Lcom/google/d/a/a/lq;->g:B

    .line 7919
    iput v2, p0, Lcom/google/d/a/a/lq;->h:I

    .line 7748
    iget-object v0, p0, Lcom/google/d/a/a/lq;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 7749
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/lq;->c:I

    .line 7750
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/lq;->d:F

    .line 7751
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/d/a/a/lq;->e:F

    .line 7752
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 7758
    invoke-direct {p0}, Lcom/google/d/a/a/lq;-><init>()V

    .line 7759
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 7764
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 7765
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 7766
    sparse-switch v3, :sswitch_data_0

    .line 7771
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 7773
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 7769
    goto :goto_0

    .line 7778
    :sswitch_1
    iget-object v3, p0, Lcom/google/d/a/a/lq;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 7779
    iget v3, p0, Lcom/google/d/a/a/lq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/lq;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 7799
    :catch_0
    move-exception v0

    .line 7800
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7805
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/lq;->au:Lcom/google/n/bn;

    throw v0

    .line 7783
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/d/a/a/lq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/lq;->a:I

    .line 7784
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/lq;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 7801
    :catch_1
    move-exception v0

    .line 7802
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 7803
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7788
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/d/a/a/lq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/lq;->a:I

    .line 7789
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/lq;->d:F

    goto :goto_0

    .line 7793
    :sswitch_4
    iget v3, p0, Lcom/google/d/a/a/lq;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/lq;->a:I

    .line 7794
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/lq;->e:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 7805
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/lq;->au:Lcom/google/n/bn;

    .line 7806
    return-void

    .line 7766
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 7745
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 7825
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lq;->b:Lcom/google/n/ao;

    .line 7885
    iput-byte v1, p0, Lcom/google/d/a/a/lq;->g:B

    .line 7919
    iput v1, p0, Lcom/google/d/a/a/lq;->h:I

    .line 7746
    return-void
.end method

.method public static d()Lcom/google/d/a/a/lq;
    .locals 1

    .prologue
    .line 8266
    sget-object v0, Lcom/google/d/a/a/lq;->f:Lcom/google/d/a/a/lq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ls;
    .locals 1

    .prologue
    .line 8010
    new-instance v0, Lcom/google/d/a/a/ls;

    invoke-direct {v0}, Lcom/google/d/a/a/ls;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/lq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7820
    sget-object v0, Lcom/google/d/a/a/lq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 7903
    invoke-virtual {p0}, Lcom/google/d/a/a/lq;->c()I

    .line 7904
    iget v0, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 7905
    iget-object v0, p0, Lcom/google/d/a/a/lq;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 7907
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 7908
    iget v0, p0, Lcom/google/d/a/a/lq;->c:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 7910
    :cond_1
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 7911
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/lq;->d:F

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 7913
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 7914
    iget v0, p0, Lcom/google/d/a/a/lq;->e:F

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 7916
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/lq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 7917
    return-void

    .line 7908
    :cond_4
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 7887
    iget-byte v0, p0, Lcom/google/d/a/a/lq;->g:B

    .line 7888
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 7898
    :goto_0
    return v0

    .line 7889
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 7891
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 7892
    iget-object v0, p0, Lcom/google/d/a/a/lq;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 7893
    iput-byte v2, p0, Lcom/google/d/a/a/lq;->g:B

    move v0, v2

    .line 7894
    goto :goto_0

    :cond_2
    move v0, v2

    .line 7891
    goto :goto_1

    .line 7897
    :cond_3
    iput-byte v1, p0, Lcom/google/d/a/a/lq;->g:B

    move v0, v1

    .line 7898
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 7921
    iget v0, p0, Lcom/google/d/a/a/lq;->h:I

    .line 7922
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 7943
    :goto_0
    return v0

    .line 7925
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 7926
    iget-object v0, p0, Lcom/google/d/a/a/lq;->b:Lcom/google/n/ao;

    .line 7927
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 7929
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 7930
    iget v2, p0, Lcom/google/d/a/a/lq;->c:I

    .line 7931
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 7933
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 7934
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/d/a/a/lq;->d:F

    .line 7935
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 7937
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/lq;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 7938
    iget v2, p0, Lcom/google/d/a/a/lq;->e:F

    .line 7939
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 7941
    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/lq;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 7942
    iput v0, p0, Lcom/google/d/a/a/lq;->h:I

    goto :goto_0

    .line 7931
    :cond_4
    const/16 v2, 0xa

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7739
    invoke-static {}, Lcom/google/d/a/a/lq;->newBuilder()Lcom/google/d/a/a/ls;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ls;->a(Lcom/google/d/a/a/lq;)Lcom/google/d/a/a/ls;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7739
    invoke-static {}, Lcom/google/d/a/a/lq;->newBuilder()Lcom/google/d/a/a/ls;

    move-result-object v0

    return-object v0
.end method
