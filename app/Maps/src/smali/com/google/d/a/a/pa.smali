.class public final enum Lcom/google/d/a/a/pa;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/pa;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/pa;

.field public static final enum b:Lcom/google/d/a/a/pa;

.field public static final enum c:Lcom/google/d/a/a/pa;

.field public static final enum d:Lcom/google/d/a/a/pa;

.field public static final enum e:Lcom/google/d/a/a/pa;

.field public static final enum f:Lcom/google/d/a/a/pa;

.field public static final enum g:Lcom/google/d/a/a/pa;

.field public static final enum h:Lcom/google/d/a/a/pa;

.field private static final synthetic j:[Lcom/google/d/a/a/pa;


# instance fields
.field public final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 239
    new-instance v0, Lcom/google/d/a/a/pa;

    const-string v1, "SUNDAY"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/pa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pa;->a:Lcom/google/d/a/a/pa;

    .line 243
    new-instance v0, Lcom/google/d/a/a/pa;

    const-string v1, "MONDAY"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/pa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pa;->b:Lcom/google/d/a/a/pa;

    .line 247
    new-instance v0, Lcom/google/d/a/a/pa;

    const-string v1, "TUESDAY"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/d/a/a/pa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pa;->c:Lcom/google/d/a/a/pa;

    .line 251
    new-instance v0, Lcom/google/d/a/a/pa;

    const-string v1, "WEDNESDAY"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/d/a/a/pa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pa;->d:Lcom/google/d/a/a/pa;

    .line 255
    new-instance v0, Lcom/google/d/a/a/pa;

    const-string v1, "THURSDAY"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/d/a/a/pa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pa;->e:Lcom/google/d/a/a/pa;

    .line 259
    new-instance v0, Lcom/google/d/a/a/pa;

    const-string v1, "FRIDAY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/pa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pa;->f:Lcom/google/d/a/a/pa;

    .line 263
    new-instance v0, Lcom/google/d/a/a/pa;

    const-string v1, "SATURDAY"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/pa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pa;->g:Lcom/google/d/a/a/pa;

    .line 267
    new-instance v0, Lcom/google/d/a/a/pa;

    const-string v1, "NEXT_SUNDAY"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/pa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pa;->h:Lcom/google/d/a/a/pa;

    .line 234
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/d/a/a/pa;

    sget-object v1, Lcom/google/d/a/a/pa;->a:Lcom/google/d/a/a/pa;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/pa;->b:Lcom/google/d/a/a/pa;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/pa;->c:Lcom/google/d/a/a/pa;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/pa;->d:Lcom/google/d/a/a/pa;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/pa;->e:Lcom/google/d/a/a/pa;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/pa;->f:Lcom/google/d/a/a/pa;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/pa;->g:Lcom/google/d/a/a/pa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/pa;->h:Lcom/google/d/a/a/pa;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/pa;->j:[Lcom/google/d/a/a/pa;

    .line 327
    new-instance v0, Lcom/google/d/a/a/pb;

    invoke-direct {v0}, Lcom/google/d/a/a/pb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 336
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 337
    iput p3, p0, Lcom/google/d/a/a/pa;->i:I

    .line 338
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/pa;
    .locals 1

    .prologue
    .line 309
    packed-switch p0, :pswitch_data_0

    .line 318
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 310
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/pa;->a:Lcom/google/d/a/a/pa;

    goto :goto_0

    .line 311
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/pa;->b:Lcom/google/d/a/a/pa;

    goto :goto_0

    .line 312
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/pa;->c:Lcom/google/d/a/a/pa;

    goto :goto_0

    .line 313
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/pa;->d:Lcom/google/d/a/a/pa;

    goto :goto_0

    .line 314
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/pa;->e:Lcom/google/d/a/a/pa;

    goto :goto_0

    .line 315
    :pswitch_5
    sget-object v0, Lcom/google/d/a/a/pa;->f:Lcom/google/d/a/a/pa;

    goto :goto_0

    .line 316
    :pswitch_6
    sget-object v0, Lcom/google/d/a/a/pa;->g:Lcom/google/d/a/a/pa;

    goto :goto_0

    .line 317
    :pswitch_7
    sget-object v0, Lcom/google/d/a/a/pa;->h:Lcom/google/d/a/a/pa;

    goto :goto_0

    .line 309
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/pa;
    .locals 1

    .prologue
    .line 234
    const-class v0, Lcom/google/d/a/a/pa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pa;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/pa;
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/google/d/a/a/pa;->j:[Lcom/google/d/a/a/pa;

    invoke-virtual {v0}, [Lcom/google/d/a/a/pa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/pa;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/google/d/a/a/pa;->i:I

    return v0
.end method
