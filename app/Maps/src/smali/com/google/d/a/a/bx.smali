.class public final enum Lcom/google/d/a/a/bx;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/bx;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/bx;

.field public static final enum b:Lcom/google/d/a/a/bx;

.field public static final enum c:Lcom/google/d/a/a/bx;

.field public static final enum d:Lcom/google/d/a/a/bx;

.field public static final enum e:Lcom/google/d/a/a/bx;

.field public static final enum f:Lcom/google/d/a/a/bx;

.field public static final enum g:Lcom/google/d/a/a/bx;

.field public static final enum h:Lcom/google/d/a/a/bx;

.field private static final synthetic j:[Lcom/google/d/a/a/bx;


# instance fields
.field final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 122
    new-instance v0, Lcom/google/d/a/a/bx;

    const-string v1, "PRECISION_CENTURY"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/bx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/bx;->a:Lcom/google/d/a/a/bx;

    .line 126
    new-instance v0, Lcom/google/d/a/a/bx;

    const-string v1, "PRECISION_DECADE"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/bx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/bx;->b:Lcom/google/d/a/a/bx;

    .line 130
    new-instance v0, Lcom/google/d/a/a/bx;

    const-string v1, "PRECISION_YEAR"

    const/16 v2, 0x12c

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/bx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/bx;->c:Lcom/google/d/a/a/bx;

    .line 134
    new-instance v0, Lcom/google/d/a/a/bx;

    const-string v1, "PRECISION_MONTH"

    const/16 v2, 0x190

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/bx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/bx;->d:Lcom/google/d/a/a/bx;

    .line 138
    new-instance v0, Lcom/google/d/a/a/bx;

    const-string v1, "PRECISION_DAY"

    const/16 v2, 0x1f4

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/bx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/bx;->e:Lcom/google/d/a/a/bx;

    .line 142
    new-instance v0, Lcom/google/d/a/a/bx;

    const-string v1, "PRECISION_HOUR"

    const/4 v2, 0x5

    const/16 v3, 0x258

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/bx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/bx;->f:Lcom/google/d/a/a/bx;

    .line 146
    new-instance v0, Lcom/google/d/a/a/bx;

    const-string v1, "PRECISION_MINUTE"

    const/4 v2, 0x6

    const/16 v3, 0x2bc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/bx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/bx;->g:Lcom/google/d/a/a/bx;

    .line 150
    new-instance v0, Lcom/google/d/a/a/bx;

    const-string v1, "PRECISION_SECOND"

    const/4 v2, 0x7

    const/16 v3, 0x320

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/bx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/bx;->h:Lcom/google/d/a/a/bx;

    .line 117
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/d/a/a/bx;

    sget-object v1, Lcom/google/d/a/a/bx;->a:Lcom/google/d/a/a/bx;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/bx;->b:Lcom/google/d/a/a/bx;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/bx;->c:Lcom/google/d/a/a/bx;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/bx;->d:Lcom/google/d/a/a/bx;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/bx;->e:Lcom/google/d/a/a/bx;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/bx;->f:Lcom/google/d/a/a/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/bx;->g:Lcom/google/d/a/a/bx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/bx;->h:Lcom/google/d/a/a/bx;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/bx;->j:[Lcom/google/d/a/a/bx;

    .line 210
    new-instance v0, Lcom/google/d/a/a/by;

    invoke-direct {v0}, Lcom/google/d/a/a/by;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 220
    iput p3, p0, Lcom/google/d/a/a/bx;->i:I

    .line 221
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/bx;
    .locals 1

    .prologue
    .line 192
    sparse-switch p0, :sswitch_data_0

    .line 201
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 193
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/bx;->a:Lcom/google/d/a/a/bx;

    goto :goto_0

    .line 194
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/bx;->b:Lcom/google/d/a/a/bx;

    goto :goto_0

    .line 195
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/bx;->c:Lcom/google/d/a/a/bx;

    goto :goto_0

    .line 196
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/bx;->d:Lcom/google/d/a/a/bx;

    goto :goto_0

    .line 197
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/bx;->e:Lcom/google/d/a/a/bx;

    goto :goto_0

    .line 198
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/bx;->f:Lcom/google/d/a/a/bx;

    goto :goto_0

    .line 199
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/bx;->g:Lcom/google/d/a/a/bx;

    goto :goto_0

    .line 200
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/bx;->h:Lcom/google/d/a/a/bx;

    goto :goto_0

    .line 192
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/bx;
    .locals 1

    .prologue
    .line 117
    const-class v0, Lcom/google/d/a/a/bx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bx;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/bx;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/google/d/a/a/bx;->j:[Lcom/google/d/a/a/bx;

    invoke-virtual {v0}, [Lcom/google/d/a/a/bx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/bx;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/google/d/a/a/bx;->i:I

    return v0
.end method
