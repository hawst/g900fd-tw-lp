.class public final Lcom/google/d/a/a/a/j;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/a/m;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/a/j;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/d/a/a/a/j;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:I

.field f:J

.field g:D

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1379
    new-instance v0, Lcom/google/d/a/a/a/k;

    invoke-direct {v0}, Lcom/google/d/a/a/a/k;-><init>()V

    sput-object v0, Lcom/google/d/a/a/a/j;->PARSER:Lcom/google/n/ax;

    .line 1617
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/a/j;->k:Lcom/google/n/aw;

    .line 2106
    new-instance v0, Lcom/google/d/a/a/a/j;

    invoke-direct {v0}, Lcom/google/d/a/a/a/j;-><init>()V

    sput-object v0, Lcom/google/d/a/a/a/j;->h:Lcom/google/d/a/a/a/j;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1304
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1396
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->b:Lcom/google/n/ao;

    .line 1540
    iput-byte v2, p0, Lcom/google/d/a/a/a/j;->i:B

    .line 1580
    iput v2, p0, Lcom/google/d/a/a/a/j;->j:I

    .line 1305
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1306
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->c:Ljava/lang/Object;

    .line 1307
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->d:Ljava/lang/Object;

    .line 1308
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/a/j;->e:I

    .line 1309
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/a/j;->f:J

    .line 1310
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/a/j;->g:D

    .line 1311
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1317
    invoke-direct {p0}, Lcom/google/d/a/a/a/j;-><init>()V

    .line 1318
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1323
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1324
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1325
    sparse-switch v3, :sswitch_data_0

    .line 1330
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1332
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1328
    goto :goto_0

    .line 1337
    :sswitch_1
    iget-object v3, p0, Lcom/google/d/a/a/a/j;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1338
    iget v3, p0, Lcom/google/d/a/a/a/j;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/a/j;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1370
    :catch_0
    move-exception v0

    .line 1371
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1376
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/a/j;->au:Lcom/google/n/bn;

    throw v0

    .line 1342
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1343
    iget v4, p0, Lcom/google/d/a/a/a/j;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/a/j;->a:I

    .line 1344
    iput-object v3, p0, Lcom/google/d/a/a/a/j;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1372
    :catch_1
    move-exception v0

    .line 1373
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1374
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1348
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1349
    iget v4, p0, Lcom/google/d/a/a/a/j;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/a/j;->a:I

    .line 1350
    iput-object v3, p0, Lcom/google/d/a/a/a/j;->d:Ljava/lang/Object;

    goto :goto_0

    .line 1354
    :sswitch_4
    iget v3, p0, Lcom/google/d/a/a/a/j;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/a/j;->a:I

    .line 1355
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/a/j;->e:I

    goto :goto_0

    .line 1359
    :sswitch_5
    iget v3, p0, Lcom/google/d/a/a/a/j;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/a/j;->a:I

    .line 1360
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/a/j;->g:D

    goto :goto_0

    .line 1364
    :sswitch_6
    iget v3, p0, Lcom/google/d/a/a/a/j;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/a/j;->a:I

    .line 1365
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/a/j;->f:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1376
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->au:Lcom/google/n/bn;

    .line 1377
    return-void

    .line 1325
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x29 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1302
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1396
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->b:Lcom/google/n/ao;

    .line 1540
    iput-byte v1, p0, Lcom/google/d/a/a/a/j;->i:B

    .line 1580
    iput v1, p0, Lcom/google/d/a/a/a/j;->j:I

    .line 1303
    return-void
.end method

.method public static d()Lcom/google/d/a/a/a/j;
    .locals 1

    .prologue
    .line 2109
    sget-object v0, Lcom/google/d/a/a/a/j;->h:Lcom/google/d/a/a/a/j;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/a/l;
    .locals 1

    .prologue
    .line 1679
    new-instance v0, Lcom/google/d/a/a/a/l;

    invoke-direct {v0}, Lcom/google/d/a/a/a/l;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/a/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1391
    sget-object v0, Lcom/google/d/a/a/a/j;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 1558
    invoke-virtual {p0}, Lcom/google/d/a/a/a/j;->c()I

    .line 1559
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 1560
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1562
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1563
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1565
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 1566
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/a/j;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1568
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1569
    iget v0, p0, Lcom/google/d/a/a/a/j;->e:I

    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1571
    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 1572
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/d/a/a/a/j;->g:D

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 1574
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 1575
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/d/a/a/a/j;->f:J

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    .line 1577
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1578
    return-void

    .line 1563
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 1566
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1569
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1542
    iget-byte v0, p0, Lcom/google/d/a/a/a/j;->i:B

    .line 1543
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1553
    :goto_0
    return v0

    .line 1544
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1546
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1547
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1548
    iput-byte v2, p0, Lcom/google/d/a/a/a/j;->i:B

    move v0, v2

    .line 1549
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1546
    goto :goto_1

    .line 1552
    :cond_3
    iput-byte v1, p0, Lcom/google/d/a/a/a/j;->i:B

    move v0, v1

    .line 1553
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1582
    iget v0, p0, Lcom/google/d/a/a/a/j;->j:I

    .line 1583
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1612
    :goto_0
    return v0

    .line 1586
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 1587
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->b:Lcom/google/n/ao;

    .line 1588
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1590
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1592
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1594
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1595
    const/4 v3, 0x3

    .line 1596
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/j;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1598
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 1599
    iget v0, p0, Lcom/google/d/a/a/a/j;->e:I

    .line 1600
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1602
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 1603
    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/google/d/a/a/a/j;->g:D

    .line 1604
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v1, v0

    .line 1606
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/a/j;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_5

    .line 1607
    const/4 v0, 0x6

    iget-wide v4, p0, Lcom/google/d/a/a/a/j;->f:J

    .line 1608
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1610
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/a/j;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1611
    iput v0, p0, Lcom/google/d/a/a/a/j;->j:I

    goto/16 :goto_0

    .line 1592
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 1596
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 1600
    :cond_8
    const/16 v0, 0xa

    goto :goto_4

    :cond_9
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1296
    invoke-static {}, Lcom/google/d/a/a/a/j;->newBuilder()Lcom/google/d/a/a/a/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/a/l;->a(Lcom/google/d/a/a/a/j;)Lcom/google/d/a/a/a/l;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1296
    invoke-static {}, Lcom/google/d/a/a/a/j;->newBuilder()Lcom/google/d/a/a/a/l;

    move-result-object v0

    return-object v0
.end method
