.class public final Lcom/google/d/a/a/qw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/rb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/qw;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/d/a/a/qw;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:I

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/d/a/a/qx;

    invoke-direct {v0}, Lcom/google/d/a/a/qx;-><init>()V

    sput-object v0, Lcom/google/d/a/a/qw;->PARSER:Lcom/google/n/ax;

    .line 607
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/qw;->k:Lcom/google/n/aw;

    .line 1494
    new-instance v0, Lcom/google/d/a/a/qw;

    invoke-direct {v0}, Lcom/google/d/a/a/qw;-><init>()V

    sput-object v0, Lcom/google/d/a/a/qw;->h:Lcom/google/d/a/a/qw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 105
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 512
    iput-byte v0, p0, Lcom/google/d/a/a/qw;->i:B

    .line 570
    iput v0, p0, Lcom/google/d/a/a/qw;->j:I

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/qw;->b:Ljava/lang/Object;

    .line 107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    .line 108
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    .line 109
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    .line 110
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/qw;->f:I

    .line 111
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    .line 112
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/16 v9, 0x20

    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v6, 0x4

    .line 118
    invoke-direct {p0}, Lcom/google/d/a/a/qw;-><init>()V

    .line 121
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v2, v0

    .line 124
    :cond_0
    :goto_0
    if-nez v2, :cond_7

    .line 125
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 126
    sparse-switch v1, :sswitch_data_0

    .line 131
    invoke-virtual {v3, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 133
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 128
    :sswitch_0
    const/4 v1, 0x1

    move v2, v1

    .line 129
    goto :goto_0

    .line 138
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 139
    iget v4, p0, Lcom/google/d/a/a/qw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/qw;->a:I

    .line 140
    iput-object v1, p0, Lcom/google/d/a/a/qw;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 196
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 197
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 203
    iget-object v2, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    .line 205
    :cond_1
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v8, :cond_2

    .line 206
    iget-object v2, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    .line 208
    :cond_2
    and-int/lit8 v2, v1, 0x20

    if-ne v2, v9, :cond_3

    .line 209
    iget-object v2, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    .line 211
    :cond_3
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_4

    .line 212
    iget-object v1, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    .line 214
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qw;->au:Lcom/google/n/bn;

    throw v0

    .line 144
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_e

    .line 145
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147
    or-int/lit8 v1, v0, 0x2

    .line 149
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 150
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 149
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 151
    goto :goto_0

    .line 154
    :sswitch_3
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v8, :cond_d

    .line 155
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 157
    or-int/lit8 v1, v0, 0x8

    .line 159
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 160
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 159
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 161
    goto/16 :goto_0

    .line 164
    :sswitch_4
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 165
    invoke-static {v1}, Lcom/google/d/a/a/qz;->a(I)Lcom/google/d/a/a/qz;

    move-result-object v4

    .line 166
    if-nez v4, :cond_5

    .line 167
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 198
    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 199
    :goto_5
    :try_start_7
    new-instance v2, Lcom/google/n/ak;

    .line 200
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 169
    :cond_5
    :try_start_8
    iget v4, p0, Lcom/google/d/a/a/qw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/qw;->a:I

    .line 170
    iput v1, p0, Lcom/google/d/a/a/qw;->f:I

    goto/16 :goto_0

    .line 202
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_2

    .line 175
    :sswitch_5
    and-int/lit8 v1, v0, 0x20

    if-eq v1, v9, :cond_c

    .line 176
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 178
    or-int/lit8 v1, v0, 0x20

    .line 180
    :goto_6
    :try_start_9
    iget-object v0, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 181
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 180
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 182
    goto/16 :goto_0

    .line 185
    :sswitch_6
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v6, :cond_6

    .line 186
    :try_start_a
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    .line 188
    or-int/lit8 v0, v0, 0x4

    .line 190
    :cond_6
    iget-object v1, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 191
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 190
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_0

    .line 202
    :cond_7
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v7, :cond_8

    .line 203
    iget-object v1, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    .line 205
    :cond_8
    and-int/lit8 v1, v0, 0x8

    if-ne v1, v8, :cond_9

    .line 206
    iget-object v1, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    .line 208
    :cond_9
    and-int/lit8 v1, v0, 0x20

    if-ne v1, v9, :cond_a

    .line 209
    iget-object v1, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    .line 211
    :cond_a
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_b

    .line 212
    iget-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    .line 214
    :cond_b
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qw;->au:Lcom/google/n/bn;

    .line 215
    return-void

    .line 198
    :catch_2
    move-exception v0

    goto/16 :goto_5

    .line 196
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_c
    move v1, v0

    goto :goto_6

    :cond_d
    move v1, v0

    goto/16 :goto_4

    :cond_e
    move v1, v0

    goto/16 :goto_3

    .line 126
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 103
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 512
    iput-byte v0, p0, Lcom/google/d/a/a/qw;->i:B

    .line 570
    iput v0, p0, Lcom/google/d/a/a/qw;->j:I

    .line 104
    return-void
.end method

.method public static d()Lcom/google/d/a/a/qw;
    .locals 1

    .prologue
    .line 1497
    sget-object v0, Lcom/google/d/a/a/qw;->h:Lcom/google/d/a/a/qw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/qy;
    .locals 1

    .prologue
    .line 669
    new-instance v0, Lcom/google/d/a/a/qy;

    invoke-direct {v0}, Lcom/google/d/a/a/qy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/qw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    sget-object v0, Lcom/google/d/a/a/qw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 548
    invoke-virtual {p0}, Lcom/google/d/a/a/qw;->c()I

    .line 549
    iget v0, p0, Lcom/google/d/a/a/qw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 550
    iget-object v0, p0, Lcom/google/d/a/a/qw;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qw;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 552
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 553
    iget-object v0, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 552
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 550
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    :cond_2
    move v1, v2

    .line 555
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 556
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 555
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 558
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/qw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 559
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/d/a/a/qw;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_4
    :goto_3
    move v1, v2

    .line 561
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 562
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 561
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 559
    :cond_5
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 564
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 565
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 564
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 567
    :cond_7
    iget-object v0, p0, Lcom/google/d/a/a/qw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 568
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 514
    iget-byte v0, p0, Lcom/google/d/a/a/qw;->i:B

    .line 515
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 543
    :cond_0
    :goto_0
    return v2

    .line 516
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 518
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 519
    iget-object v0, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 520
    iput-byte v2, p0, Lcom/google/d/a/a/qw;->i:B

    goto :goto_0

    .line 518
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 524
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 525
    iget-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sm;->d()Lcom/google/d/a/a/sm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sm;

    invoke-virtual {v0}, Lcom/google/d/a/a/sm;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 526
    iput-byte v2, p0, Lcom/google/d/a/a/qw;->i:B

    goto :goto_0

    .line 524
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 530
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 531
    iget-object v0, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 532
    iput-byte v2, p0, Lcom/google/d/a/a/qw;->i:B

    goto :goto_0

    .line 530
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_7
    move v1, v2

    .line 536
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 537
    iget-object v0, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 538
    iput-byte v2, p0, Lcom/google/d/a/a/qw;->i:B

    goto/16 :goto_0

    .line 536
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 542
    :cond_9
    iput-byte v3, p0, Lcom/google/d/a/a/qw;->i:B

    move v2, v3

    .line 543
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 572
    iget v0, p0, Lcom/google/d/a/a/qw;->j:I

    .line 573
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 602
    :goto_0
    return v0

    .line 576
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/qw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 578
    iget-object v0, p0, Lcom/google/d/a/a/qw;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qw;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 580
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 581
    iget-object v0, p0, Lcom/google/d/a/a/qw;->c:Ljava/util/List;

    .line 582
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 580
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 578
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v2, v1

    .line 584
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 585
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/qw;->e:Ljava/util/List;

    .line 586
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 584
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 588
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/qw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_4

    .line 589
    const/4 v0, 0x4

    iget v2, p0, Lcom/google/d/a/a/qw;->f:I

    .line 590
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_5

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    :cond_4
    move v2, v1

    .line 592
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 593
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/qw;->g:Ljava/util/List;

    .line 594
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 592
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 590
    :cond_5
    const/16 v0, 0xa

    goto :goto_5

    :cond_6
    move v2, v1

    .line 596
    :goto_7
    iget-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 597
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/qw;->d:Ljava/util/List;

    .line 598
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 596
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 600
    :cond_7
    iget-object v0, p0, Lcom/google/d/a/a/qw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 601
    iput v0, p0, Lcom/google/d/a/a/qw;->j:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lcom/google/d/a/a/qw;->newBuilder()Lcom/google/d/a/a/qy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/qy;->a(Lcom/google/d/a/a/qw;)Lcom/google/d/a/a/qy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lcom/google/d/a/a/qw;->newBuilder()Lcom/google/d/a/a/qy;

    move-result-object v0

    return-object v0
.end method
