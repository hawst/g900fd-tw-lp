.class public final Lcom/google/d/a/a/ej;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/em;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ej;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/d/a/a/ej;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/google/d/a/a/ek;

    invoke-direct {v0}, Lcom/google/d/a/a/ek;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ej;->PARSER:Lcom/google/n/ax;

    .line 267
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/ej;->h:Lcom/google/n/aw;

    .line 654
    new-instance v0, Lcom/google/d/a/a/ej;

    invoke-direct {v0}, Lcom/google/d/a/a/ej;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ej;->e:Lcom/google/d/a/a/ej;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 196
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ej;->d:Lcom/google/n/ao;

    .line 211
    iput-byte v2, p0, Lcom/google/d/a/a/ej;->f:B

    .line 242
    iput v2, p0, Lcom/google/d/a/a/ej;->g:I

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/ej;->b:I

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    .line 61
    iget-object v0, p0, Lcom/google/d/a/a/ej;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 62
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Lcom/google/d/a/a/ej;-><init>()V

    .line 71
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 74
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 75
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 76
    sparse-switch v4, :sswitch_data_0

    .line 81
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 83
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 79
    goto :goto_0

    .line 88
    :sswitch_1
    iget v4, p0, Lcom/google/d/a/a/ej;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/ej;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/ej;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 110
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 116
    iget-object v1, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    .line 118
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ej;->au:Lcom/google/n/bn;

    throw v0

    .line 93
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_2

    .line 94
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    .line 96
    or-int/lit8 v1, v1, 0x2

    .line 98
    :cond_2
    iget-object v4, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 98
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 111
    :catch_1
    move-exception v0

    .line 112
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 113
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/d/a/a/ej;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 104
    iget v4, p0, Lcom/google/d/a/a/ej;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/ej;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 115
    :cond_3
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_4

    .line 116
    iget-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    .line 118
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ej;->au:Lcom/google/n/bn;

    .line 119
    return-void

    .line 76
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 56
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 196
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ej;->d:Lcom/google/n/ao;

    .line 211
    iput-byte v1, p0, Lcom/google/d/a/a/ej;->f:B

    .line 242
    iput v1, p0, Lcom/google/d/a/a/ej;->g:I

    .line 57
    return-void
.end method

.method public static d()Lcom/google/d/a/a/ej;
    .locals 1

    .prologue
    .line 657
    sget-object v0, Lcom/google/d/a/a/ej;->e:Lcom/google/d/a/a/ej;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/el;
    .locals 1

    .prologue
    .line 329
    new-instance v0, Lcom/google/d/a/a/el;

    invoke-direct {v0}, Lcom/google/d/a/a/el;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    sget-object v0, Lcom/google/d/a/a/ej;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x2

    .line 229
    invoke-virtual {p0}, Lcom/google/d/a/a/ej;->c()I

    .line 230
    iget v1, p0, Lcom/google/d/a/a/ej;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 231
    iget v1, p0, Lcom/google/d/a/a/ej;->b:I

    invoke-static {v2, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_0
    :goto_0
    move v1, v0

    .line 233
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 234
    iget-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 233
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 231
    :cond_1
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 236
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/ej;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 237
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/d/a/a/ej;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 239
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/ej;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 240
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 213
    iget-byte v0, p0, Lcom/google/d/a/a/ej;->f:B

    .line 214
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 224
    :cond_0
    :goto_0
    return v2

    .line 215
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 217
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 218
    iget-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 219
    iput-byte v2, p0, Lcom/google/d/a/a/ej;->f:B

    goto :goto_0

    .line 217
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 223
    :cond_3
    iput-byte v3, p0, Lcom/google/d/a/a/ej;->f:B

    move v2, v3

    .line 224
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 244
    iget v0, p0, Lcom/google/d/a/a/ej;->g:I

    .line 245
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 262
    :goto_0
    return v0

    .line 248
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ej;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 249
    iget v0, p0, Lcom/google/d/a/a/ej;->b:I

    .line 250
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 252
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 253
    iget-object v0, p0, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    .line 254
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 252
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 250
    :cond_1
    const/16 v0, 0xa

    goto :goto_1

    .line 256
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/ej;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_3

    .line 257
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/d/a/a/ej;->d:Lcom/google/n/ao;

    .line 258
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 260
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/ej;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 261
    iput v0, p0, Lcom/google/d/a/a/ej;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/d/a/a/ej;->newBuilder()Lcom/google/d/a/a/el;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/el;->a(Lcom/google/d/a/a/ej;)Lcom/google/d/a/a/el;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/d/a/a/ej;->newBuilder()Lcom/google/d/a/a/el;

    move-result-object v0

    return-object v0
.end method
