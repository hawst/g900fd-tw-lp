.class public final Lcom/google/d/a/a/bw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/bz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/bu;",
        "Lcom/google/d/a/a/bw;",
        ">;",
        "Lcom/google/d/a/a/bz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:D

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 385
    sget-object v0, Lcom/google/d/a/a/bu;->d:Lcom/google/d/a/a/bu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 470
    const/16 v0, 0x320

    iput v0, p0, Lcom/google/d/a/a/bw;->c:I

    .line 386
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/bu;)Lcom/google/d/a/a/bw;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 417
    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 425
    :goto_0
    return-object p0

    .line 418
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 419
    iget-wide v2, p1, Lcom/google/d/a/a/bu;->b:D

    iget v4, p0, Lcom/google/d/a/a/bw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/bw;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/bw;->b:D

    .line 421
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/bu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_6

    .line 422
    iget v0, p1, Lcom/google/d/a/a/bu;->c:I

    invoke-static {v0}, Lcom/google/d/a/a/bx;->a(I)Lcom/google/d/a/a/bx;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/d/a/a/bx;->h:Lcom/google/d/a/a/bx;

    :cond_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 418
    goto :goto_1

    :cond_4
    move v0, v1

    .line 421
    goto :goto_2

    .line 422
    :cond_5
    iget v1, p0, Lcom/google/d/a/a/bw;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/d/a/a/bw;->a:I

    iget v0, v0, Lcom/google/d/a/a/bx;->i:I

    iput v0, p0, Lcom/google/d/a/a/bw;->c:I

    .line 424
    :cond_6
    iget-object v0, p1, Lcom/google/d/a/a/bu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 377
    new-instance v2, Lcom/google/d/a/a/bu;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/bu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/bw;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-wide v4, p0, Lcom/google/d/a/a/bw;->b:D

    iput-wide v4, v2, Lcom/google/d/a/a/bu;->b:D

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/d/a/a/bw;->c:I

    iput v1, v2, Lcom/google/d/a/a/bu;->c:I

    iput v0, v2, Lcom/google/d/a/a/bu;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 377
    check-cast p1, Lcom/google/d/a/a/bu;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/bw;->a(Lcom/google/d/a/a/bu;)Lcom/google/d/a/a/bw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 429
    iget v2, p0, Lcom/google/d/a/a/bw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 433
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 429
    goto :goto_0

    :cond_1
    move v0, v1

    .line 433
    goto :goto_1
.end method
