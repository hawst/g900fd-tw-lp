.class public final enum Lcom/google/d/a/a/mx;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mx;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/mx;

.field public static final enum b:Lcom/google/d/a/a/mx;

.field public static final enum c:Lcom/google/d/a/a/mx;

.field public static final enum d:Lcom/google/d/a/a/mx;

.field public static final enum e:Lcom/google/d/a/a/mx;

.field public static final enum f:Lcom/google/d/a/a/mx;

.field public static final enum g:Lcom/google/d/a/a/mx;

.field public static final enum h:Lcom/google/d/a/a/mx;

.field public static final enum i:Lcom/google/d/a/a/mx;

.field public static final enum j:Lcom/google/d/a/a/mx;

.field public static final enum k:Lcom/google/d/a/a/mx;

.field private static final synthetic m:[Lcom/google/d/a/a/mx;


# instance fields
.field final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1242
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_PAVED"

    invoke-direct {v0, v1, v6, v4}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->a:Lcom/google/d/a/a/mx;

    .line 1246
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_ASPHALT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->b:Lcom/google/d/a/a/mx;

    .line 1250
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_CONCRETE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->c:Lcom/google/d/a/a/mx;

    .line 1254
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_CHIPSEAL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->d:Lcom/google/d/a/a/mx;

    .line 1258
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_BRICK"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->e:Lcom/google/d/a/a/mx;

    .line 1262
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_SETT"

    const/4 v2, 0x5

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->f:Lcom/google/d/a/a/mx;

    .line 1266
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_COBBLESTONE"

    const/4 v2, 0x6

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->g:Lcom/google/d/a/a/mx;

    .line 1270
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_UNPAVED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v5}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->h:Lcom/google/d/a/a/mx;

    .line 1274
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_GRAVEL"

    const/16 v2, 0x8

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->i:Lcom/google/d/a/a/mx;

    .line 1278
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_DIRT"

    const/16 v2, 0x9

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->j:Lcom/google/d/a/a/mx;

    .line 1282
    new-instance v0, Lcom/google/d/a/a/mx;

    const-string v1, "SURFACE_SAND"

    const/16 v2, 0xa

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mx;->k:Lcom/google/d/a/a/mx;

    .line 1237
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/d/a/a/mx;

    sget-object v1, Lcom/google/d/a/a/mx;->a:Lcom/google/d/a/a/mx;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/mx;->b:Lcom/google/d/a/a/mx;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mx;->c:Lcom/google/d/a/a/mx;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/mx;->d:Lcom/google/d/a/a/mx;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/mx;->e:Lcom/google/d/a/a/mx;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/mx;->f:Lcom/google/d/a/a/mx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/mx;->g:Lcom/google/d/a/a/mx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/mx;->h:Lcom/google/d/a/a/mx;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/mx;->i:Lcom/google/d/a/a/mx;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/mx;->j:Lcom/google/d/a/a/mx;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/mx;->k:Lcom/google/d/a/a/mx;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/mx;->m:[Lcom/google/d/a/a/mx;

    .line 1357
    new-instance v0, Lcom/google/d/a/a/my;

    invoke-direct {v0}, Lcom/google/d/a/a/my;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1366
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1367
    iput p3, p0, Lcom/google/d/a/a/mx;->l:I

    .line 1368
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mx;
    .locals 1

    .prologue
    .line 1336
    sparse-switch p0, :sswitch_data_0

    .line 1348
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1337
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/mx;->a:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1338
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/mx;->b:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1339
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/mx;->c:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1340
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/mx;->d:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1341
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/mx;->e:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1342
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/mx;->f:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1343
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/mx;->g:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1344
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/mx;->h:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1345
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/mx;->i:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1346
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/mx;->j:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1347
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/mx;->k:Lcom/google/d/a/a/mx;

    goto :goto_0

    .line 1336
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_7
        0x11 -> :sswitch_1
        0x12 -> :sswitch_2
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_5
        0x16 -> :sswitch_6
        0x21 -> :sswitch_8
        0x22 -> :sswitch_9
        0x23 -> :sswitch_a
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mx;
    .locals 1

    .prologue
    .line 1237
    const-class v0, Lcom/google/d/a/a/mx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mx;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mx;
    .locals 1

    .prologue
    .line 1237
    sget-object v0, Lcom/google/d/a/a/mx;->m:[Lcom/google/d/a/a/mx;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mx;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1332
    iget v0, p0, Lcom/google/d/a/a/mx;->l:I

    return v0
.end method
