.class public final Lcom/google/d/a/a/pw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/pz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/pw;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/a/a/pw;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/google/d/a/a/px;

    invoke-direct {v0}, Lcom/google/d/a/a/px;-><init>()V

    sput-object v0, Lcom/google/d/a/a/pw;->PARSER:Lcom/google/n/ax;

    .line 359
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/pw;->g:Lcom/google/n/aw;

    .line 952
    new-instance v0, Lcom/google/d/a/a/pw;

    invoke-direct {v0}, Lcom/google/d/a/a/pw;-><init>()V

    sput-object v0, Lcom/google/d/a/a/pw;->d:Lcom/google/d/a/a/pw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 68
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 291
    iput-byte v0, p0, Lcom/google/d/a/a/pw;->e:B

    .line 334
    iput v0, p0, Lcom/google/d/a/a/pw;->f:I

    .line 69
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    .line 70
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    .line 71
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    .line 72
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v3, 0x1

    .line 78
    invoke-direct {p0}, Lcom/google/d/a/a/pw;-><init>()V

    .line 81
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 84
    :cond_0
    :goto_0
    if-nez v2, :cond_5

    .line 85
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 86
    sparse-switch v1, :sswitch_data_0

    .line 91
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 93
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 89
    goto :goto_0

    .line 98
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_a

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 101
    or-int/lit8 v1, v0, 0x1

    .line 103
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 104
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 103
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 105
    goto :goto_0

    .line 108
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_9

    .line 109
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 111
    or-int/lit8 v1, v0, 0x2

    .line 113
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 114
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 113
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 115
    goto :goto_0

    .line 118
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_1

    .line 119
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    .line 121
    or-int/lit8 v0, v0, 0x4

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 124
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 123
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 129
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 130
    :goto_3
    :try_start_5
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 135
    :catchall_0
    move-exception v0

    :goto_4
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_2

    .line 136
    iget-object v2, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    .line 138
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_3

    .line 139
    iget-object v2, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    .line 141
    :cond_3
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_4

    .line 142
    iget-object v1, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    .line 144
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/pw;->au:Lcom/google/n/bn;

    throw v0

    .line 135
    :cond_5
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_6

    .line 136
    iget-object v1, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    .line 138
    :cond_6
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v7, :cond_7

    .line 139
    iget-object v1, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    .line 141
    :cond_7
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_8

    .line 142
    iget-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    .line 144
    :cond_8
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/pw;->au:Lcom/google/n/bn;

    .line 145
    return-void

    .line 131
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 132
    :goto_5
    :try_start_6
    new-instance v2, Lcom/google/n/ak;

    .line 133
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 135
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_4

    .line 131
    :catch_2
    move-exception v0

    goto :goto_5

    .line 129
    :catch_3
    move-exception v0

    goto :goto_3

    :cond_9
    move v1, v0

    goto/16 :goto_2

    :cond_a
    move v1, v0

    goto/16 :goto_1

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 66
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 291
    iput-byte v0, p0, Lcom/google/d/a/a/pw;->e:B

    .line 334
    iput v0, p0, Lcom/google/d/a/a/pw;->f:I

    .line 67
    return-void
.end method

.method public static d()Lcom/google/d/a/a/pw;
    .locals 1

    .prologue
    .line 955
    sget-object v0, Lcom/google/d/a/a/pw;->d:Lcom/google/d/a/a/pw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/py;
    .locals 1

    .prologue
    .line 421
    new-instance v0, Lcom/google/d/a/a/py;

    invoke-direct {v0}, Lcom/google/d/a/a/py;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/pw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lcom/google/d/a/a/pw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 321
    invoke-virtual {p0}, Lcom/google/d/a/a/pw;->c()I

    move v1, v2

    .line 322
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 323
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 322
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 325
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 325
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 328
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 329
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 328
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 331
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/pw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 332
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 293
    iget-byte v0, p0, Lcom/google/d/a/a/pw;->e:B

    .line 294
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 316
    :cond_0
    :goto_0
    return v2

    .line 295
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 297
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 298
    iget-object v0, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 299
    iput-byte v2, p0, Lcom/google/d/a/a/pw;->e:B

    goto :goto_0

    .line 297
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 303
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 304
    iget-object v0, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/nn;->d()Lcom/google/d/a/a/nn;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nn;

    invoke-virtual {v0}, Lcom/google/d/a/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 305
    iput-byte v2, p0, Lcom/google/d/a/a/pw;->e:B

    goto :goto_0

    .line 303
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 309
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 310
    iget-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 311
    iput-byte v2, p0, Lcom/google/d/a/a/pw;->e:B

    goto :goto_0

    .line 309
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 315
    :cond_7
    iput-byte v3, p0, Lcom/google/d/a/a/pw;->e:B

    move v2, v3

    .line 316
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 336
    iget v0, p0, Lcom/google/d/a/a/pw;->f:I

    .line 337
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 354
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 340
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 341
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/d/a/a/pw;->a:Ljava/util/List;

    .line 342
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 340
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 344
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 345
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/d/a/a/pw;->b:Ljava/util/List;

    .line 346
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 344
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v1, v2

    .line 348
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 349
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/pw;->c:Ljava/util/List;

    .line 350
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 348
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 352
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/pw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 353
    iput v0, p0, Lcom/google/d/a/a/pw;->f:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/google/d/a/a/pw;->newBuilder()Lcom/google/d/a/a/py;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/py;->a(Lcom/google/d/a/a/pw;)Lcom/google/d/a/a/py;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/google/d/a/a/pw;->newBuilder()Lcom/google/d/a/a/py;

    move-result-object v0

    return-object v0
.end method
