.class public final enum Lcom/google/d/a/a/gx;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/gx;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/d/a/a/gx;

.field public static final enum B:Lcom/google/d/a/a/gx;

.field public static final enum C:Lcom/google/d/a/a/gx;

.field public static final enum D:Lcom/google/d/a/a/gx;

.field public static final enum E:Lcom/google/d/a/a/gx;

.field public static final enum F:Lcom/google/d/a/a/gx;

.field public static final enum G:Lcom/google/d/a/a/gx;

.field public static final enum H:Lcom/google/d/a/a/gx;

.field public static final enum I:Lcom/google/d/a/a/gx;

.field public static final enum J:Lcom/google/d/a/a/gx;

.field public static final enum K:Lcom/google/d/a/a/gx;

.field private static final synthetic M:[Lcom/google/d/a/a/gx;

.field public static final enum a:Lcom/google/d/a/a/gx;

.field public static final enum b:Lcom/google/d/a/a/gx;

.field public static final enum c:Lcom/google/d/a/a/gx;

.field public static final enum d:Lcom/google/d/a/a/gx;

.field public static final enum e:Lcom/google/d/a/a/gx;

.field public static final enum f:Lcom/google/d/a/a/gx;

.field public static final enum g:Lcom/google/d/a/a/gx;

.field public static final enum h:Lcom/google/d/a/a/gx;

.field public static final enum i:Lcom/google/d/a/a/gx;

.field public static final enum j:Lcom/google/d/a/a/gx;

.field public static final enum k:Lcom/google/d/a/a/gx;

.field public static final enum l:Lcom/google/d/a/a/gx;

.field public static final enum m:Lcom/google/d/a/a/gx;

.field public static final enum n:Lcom/google/d/a/a/gx;

.field public static final enum o:Lcom/google/d/a/a/gx;

.field public static final enum p:Lcom/google/d/a/a/gx;

.field public static final enum q:Lcom/google/d/a/a/gx;

.field public static final enum r:Lcom/google/d/a/a/gx;

.field public static final enum s:Lcom/google/d/a/a/gx;

.field public static final enum t:Lcom/google/d/a/a/gx;

.field public static final enum u:Lcom/google/d/a/a/gx;

.field public static final enum v:Lcom/google/d/a/a/gx;

.field public static final enum w:Lcom/google/d/a/a/gx;

.field public static final enum x:Lcom/google/d/a/a/gx;

.field public static final enum y:Lcom/google/d/a/a/gx;

.field public static final enum z:Lcom/google/d/a/a/gx;


# instance fields
.field private final L:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 286
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_ANY"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->a:Lcom/google/d/a/a/gx;

    .line 290
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_IN_LOCAL_LANGUAGE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->b:Lcom/google/d/a/a/gx;

    .line 294
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_PREFERRED"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->c:Lcom/google/d/a/a/gx;

    .line 298
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_OFFICIAL"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->d:Lcom/google/d/a/a/gx;

    .line 302
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_OBSCURE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->e:Lcom/google/d/a/a/gx;

    .line 306
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_ON_SIGNS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->f:Lcom/google/d/a/a/gx;

    .line 310
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_EXIT_NAME_NUMBER"

    const/4 v2, 0x6

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->g:Lcom/google/d/a/a/gx;

    .line 314
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_EXIT_NAME"

    const/4 v2, 0x7

    const/16 v3, 0x511

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->h:Lcom/google/d/a/a/gx;

    .line 318
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_INTERCHANGE_NAME"

    const/16 v2, 0x8

    const/16 v3, 0x5111

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->i:Lcom/google/d/a/a/gx;

    .line 322
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_EXIT_NUMBER"

    const/16 v2, 0x9

    const/16 v3, 0x512

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->j:Lcom/google/d/a/a/gx;

    .line 326
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_INTERCHANGE_NUMBER"

    const/16 v2, 0xa

    const/16 v3, 0x5121

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->k:Lcom/google/d/a/a/gx;

    .line 330
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_TRANSIT_HEADSIGN"

    const/16 v2, 0xb

    const/16 v3, 0x52

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->l:Lcom/google/d/a/a/gx;

    .line 334
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_CONNECTS_DIRECTLY"

    const/16 v2, 0xc

    const/16 v3, 0x53

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->m:Lcom/google/d/a/a/gx;

    .line 338
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_CONNECTS_INDIRECTLY"

    const/16 v2, 0xd

    const/16 v3, 0x54

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->n:Lcom/google/d/a/a/gx;

    .line 342
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_INTERSECTION_NAME"

    const/16 v2, 0xe

    const/16 v3, 0x55

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->o:Lcom/google/d/a/a/gx;

    .line 346
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_VANITY"

    const/16 v2, 0xf

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->p:Lcom/google/d/a/a/gx;

    .line 350
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_ROUTE_NUMBER"

    const/16 v2, 0x10

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->q:Lcom/google/d/a/a/gx;

    .line 354
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_COUNTRY_CODE_2"

    const/16 v2, 0x11

    const/16 v3, 0x81

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->r:Lcom/google/d/a/a/gx;

    .line 358
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_ABBREVIATED"

    const/16 v2, 0x12

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->s:Lcom/google/d/a/a/gx;

    .line 362
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_ID"

    const/16 v2, 0x13

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->t:Lcom/google/d/a/a/gx;

    .line 366
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_DESIGNATED_MARKET_AREA_ID"

    const/16 v2, 0x14

    const/16 v3, 0xa1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->u:Lcom/google/d/a/a/gx;

    .line 370
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_IATA_ID"

    const/16 v2, 0x15

    const/16 v3, 0xa3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->v:Lcom/google/d/a/a/gx;

    .line 374
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_ICAO_ID"

    const/16 v2, 0x16

    const/16 v3, 0xa4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->w:Lcom/google/d/a/a/gx;

    .line 378
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_ISO_3166_2"

    const/16 v2, 0x17

    const/16 v3, 0xa5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->x:Lcom/google/d/a/a/gx;

    .line 382
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_COUNTRY_SPECIFIC_ID"

    const/16 v2, 0x18

    const/16 v3, 0xa6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->y:Lcom/google/d/a/a/gx;

    .line 386
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_LANGUAGE_CODE"

    const/16 v2, 0x19

    const/16 v3, 0xa7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->z:Lcom/google/d/a/a/gx;

    .line 390
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_TIMEZONE_ID"

    const/16 v2, 0x1a

    const/16 v3, 0xa8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->A:Lcom/google/d/a/a/gx;

    .line 394
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_PHONE_NUMBER_PREFIX"

    const/16 v2, 0x1b

    const/16 v3, 0xa9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->B:Lcom/google/d/a/a/gx;

    .line 398
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_PHONE_NUMBER_AREA_CODE"

    const/16 v2, 0x1c

    const/16 v3, 0xa91

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->C:Lcom/google/d/a/a/gx;

    .line 402
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_TRANSLITERATED"

    const/16 v2, 0x1d

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->D:Lcom/google/d/a/a/gx;

    .line 406
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_NOT_ON_SIGNS"

    const/16 v2, 0x1e

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->E:Lcom/google/d/a/a/gx;

    .line 410
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_NOT_IN_LOCAL_LANGUAGE"

    const/16 v2, 0x1f

    const/16 v3, 0xd1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->F:Lcom/google/d/a/a/gx;

    .line 414
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_ROUNDABOUT_ROUTE"

    const/16 v2, 0x20

    const/16 v3, 0xd3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->G:Lcom/google/d/a/a/gx;

    .line 418
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_NEVER_DISPLAY"

    const/16 v2, 0x21

    const/16 v3, 0xd4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->H:Lcom/google/d/a/a/gx;

    .line 422
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_BICYCLE_ROUTE"

    const/16 v2, 0x22

    const/16 v3, 0xd5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->I:Lcom/google/d/a/a/gx;

    .line 426
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_MACHINE_GENERATED"

    const/16 v2, 0x23

    const/16 v3, 0xd7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->J:Lcom/google/d/a/a/gx;

    .line 430
    new-instance v0, Lcom/google/d/a/a/gx;

    const-string v1, "FLAG_SUSPICIOUS"

    const/16 v2, 0x24

    const/16 v3, 0xd8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/gx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/gx;->K:Lcom/google/d/a/a/gx;

    .line 281
    const/16 v0, 0x25

    new-array v0, v0, [Lcom/google/d/a/a/gx;

    sget-object v1, Lcom/google/d/a/a/gx;->a:Lcom/google/d/a/a/gx;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/gx;->b:Lcom/google/d/a/a/gx;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/gx;->c:Lcom/google/d/a/a/gx;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/gx;->d:Lcom/google/d/a/a/gx;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/gx;->e:Lcom/google/d/a/a/gx;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/gx;->f:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/gx;->g:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/gx;->h:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/gx;->i:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/gx;->j:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/gx;->k:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/gx;->l:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/gx;->m:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/d/a/a/gx;->n:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/d/a/a/gx;->o:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/d/a/a/gx;->p:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/d/a/a/gx;->q:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/d/a/a/gx;->r:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/d/a/a/gx;->s:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/d/a/a/gx;->t:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/d/a/a/gx;->u:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/d/a/a/gx;->v:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/d/a/a/gx;->w:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/d/a/a/gx;->x:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/d/a/a/gx;->y:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/d/a/a/gx;->z:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/d/a/a/gx;->A:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/d/a/a/gx;->B:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/d/a/a/gx;->C:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/d/a/a/gx;->D:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/d/a/a/gx;->E:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/d/a/a/gx;->F:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/d/a/a/gx;->G:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/d/a/a/gx;->H:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/d/a/a/gx;->I:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/d/a/a/gx;->J:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/d/a/a/gx;->K:Lcom/google/d/a/a/gx;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/gx;->M:[Lcom/google/d/a/a/gx;

    .line 635
    new-instance v0, Lcom/google/d/a/a/gy;

    invoke-direct {v0}, Lcom/google/d/a/a/gy;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 644
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 645
    iput p3, p0, Lcom/google/d/a/a/gx;->L:I

    .line 646
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/gx;
    .locals 1

    .prologue
    .line 588
    sparse-switch p0, :sswitch_data_0

    .line 626
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 589
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/gx;->a:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 590
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/gx;->b:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 591
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/gx;->c:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 592
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/gx;->d:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 593
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/gx;->e:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 594
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/gx;->f:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 595
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/gx;->g:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 596
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/gx;->h:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 597
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/gx;->i:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 598
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/gx;->j:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 599
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/gx;->k:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 600
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/gx;->l:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 601
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/gx;->m:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 602
    :sswitch_d
    sget-object v0, Lcom/google/d/a/a/gx;->n:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 603
    :sswitch_e
    sget-object v0, Lcom/google/d/a/a/gx;->o:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 604
    :sswitch_f
    sget-object v0, Lcom/google/d/a/a/gx;->p:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 605
    :sswitch_10
    sget-object v0, Lcom/google/d/a/a/gx;->q:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 606
    :sswitch_11
    sget-object v0, Lcom/google/d/a/a/gx;->r:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 607
    :sswitch_12
    sget-object v0, Lcom/google/d/a/a/gx;->s:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 608
    :sswitch_13
    sget-object v0, Lcom/google/d/a/a/gx;->t:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 609
    :sswitch_14
    sget-object v0, Lcom/google/d/a/a/gx;->u:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 610
    :sswitch_15
    sget-object v0, Lcom/google/d/a/a/gx;->v:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 611
    :sswitch_16
    sget-object v0, Lcom/google/d/a/a/gx;->w:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 612
    :sswitch_17
    sget-object v0, Lcom/google/d/a/a/gx;->x:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 613
    :sswitch_18
    sget-object v0, Lcom/google/d/a/a/gx;->y:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 614
    :sswitch_19
    sget-object v0, Lcom/google/d/a/a/gx;->z:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 615
    :sswitch_1a
    sget-object v0, Lcom/google/d/a/a/gx;->A:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 616
    :sswitch_1b
    sget-object v0, Lcom/google/d/a/a/gx;->B:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 617
    :sswitch_1c
    sget-object v0, Lcom/google/d/a/a/gx;->C:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 618
    :sswitch_1d
    sget-object v0, Lcom/google/d/a/a/gx;->D:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 619
    :sswitch_1e
    sget-object v0, Lcom/google/d/a/a/gx;->E:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 620
    :sswitch_1f
    sget-object v0, Lcom/google/d/a/a/gx;->F:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 621
    :sswitch_20
    sget-object v0, Lcom/google/d/a/a/gx;->G:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 622
    :sswitch_21
    sget-object v0, Lcom/google/d/a/a/gx;->H:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 623
    :sswitch_22
    sget-object v0, Lcom/google/d/a/a/gx;->I:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 624
    :sswitch_23
    sget-object v0, Lcom/google/d/a/a/gx;->J:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 625
    :sswitch_24
    sget-object v0, Lcom/google/d/a/a/gx;->K:Lcom/google/d/a/a/gx;

    goto :goto_0

    .line 588
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_f
        0x7 -> :sswitch_10
        0x9 -> :sswitch_12
        0xa -> :sswitch_13
        0xb -> :sswitch_1d
        0xc -> :sswitch_1e
        0x51 -> :sswitch_6
        0x52 -> :sswitch_b
        0x53 -> :sswitch_c
        0x54 -> :sswitch_d
        0x55 -> :sswitch_e
        0x81 -> :sswitch_11
        0xa1 -> :sswitch_14
        0xa3 -> :sswitch_15
        0xa4 -> :sswitch_16
        0xa5 -> :sswitch_17
        0xa6 -> :sswitch_18
        0xa7 -> :sswitch_19
        0xa8 -> :sswitch_1a
        0xa9 -> :sswitch_1b
        0xd1 -> :sswitch_1f
        0xd3 -> :sswitch_20
        0xd4 -> :sswitch_21
        0xd5 -> :sswitch_22
        0xd7 -> :sswitch_23
        0xd8 -> :sswitch_24
        0x511 -> :sswitch_7
        0x512 -> :sswitch_9
        0xa91 -> :sswitch_1c
        0x5111 -> :sswitch_8
        0x5121 -> :sswitch_a
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/gx;
    .locals 1

    .prologue
    .line 281
    const-class v0, Lcom/google/d/a/a/gx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gx;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/gx;
    .locals 1

    .prologue
    .line 281
    sget-object v0, Lcom/google/d/a/a/gx;->M:[Lcom/google/d/a/a/gx;

    invoke-virtual {v0}, [Lcom/google/d/a/a/gx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/gx;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 584
    iget v0, p0, Lcom/google/d/a/a/gx;->L:I

    return v0
.end method
