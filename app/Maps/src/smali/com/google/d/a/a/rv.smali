.class public final enum Lcom/google/d/a/a/rv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/rv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/rv;

.field public static final enum b:Lcom/google/d/a/a/rv;

.field public static final enum c:Lcom/google/d/a/a/rv;

.field private static final synthetic e:[Lcom/google/d/a/a/rv;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 551
    new-instance v0, Lcom/google/d/a/a/rv;

    const-string v1, "ACCESSIBILITY_UNKNOWN"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/rv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/rv;->a:Lcom/google/d/a/a/rv;

    .line 555
    new-instance v0, Lcom/google/d/a/a/rv;

    const-string v1, "ACCESSIBILITY_ACCESSIBLE"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/rv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/rv;->b:Lcom/google/d/a/a/rv;

    .line 559
    new-instance v0, Lcom/google/d/a/a/rv;

    const-string v1, "ACCESSIBILITY_INACCESSIBLE"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/d/a/a/rv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/rv;->c:Lcom/google/d/a/a/rv;

    .line 546
    new-array v0, v5, [Lcom/google/d/a/a/rv;

    sget-object v1, Lcom/google/d/a/a/rv;->a:Lcom/google/d/a/a/rv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/rv;->b:Lcom/google/d/a/a/rv;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/rv;->c:Lcom/google/d/a/a/rv;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/d/a/a/rv;->e:[Lcom/google/d/a/a/rv;

    .line 594
    new-instance v0, Lcom/google/d/a/a/rw;

    invoke-direct {v0}, Lcom/google/d/a/a/rw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 603
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 604
    iput p3, p0, Lcom/google/d/a/a/rv;->d:I

    .line 605
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/rv;
    .locals 1

    .prologue
    .line 581
    packed-switch p0, :pswitch_data_0

    .line 585
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 582
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/rv;->a:Lcom/google/d/a/a/rv;

    goto :goto_0

    .line 583
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/rv;->b:Lcom/google/d/a/a/rv;

    goto :goto_0

    .line 584
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/rv;->c:Lcom/google/d/a/a/rv;

    goto :goto_0

    .line 581
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/rv;
    .locals 1

    .prologue
    .line 546
    const-class v0, Lcom/google/d/a/a/rv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/rv;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/rv;
    .locals 1

    .prologue
    .line 546
    sget-object v0, Lcom/google/d/a/a/rv;->e:[Lcom/google/d/a/a/rv;

    invoke-virtual {v0}, [Lcom/google/d/a/a/rv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/rv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 577
    iget v0, p0, Lcom/google/d/a/a/rv;->d:I

    return v0
.end method
