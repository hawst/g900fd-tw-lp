.class public final Lcom/google/d/a/a/cg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/cj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/cg;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/d/a/a/cg;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Z

.field e:Z

.field f:I

.field g:Lcom/google/n/ao;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/google/d/a/a/ch;

    invoke-direct {v0}, Lcom/google/d/a/a/ch;-><init>()V

    sput-object v0, Lcom/google/d/a/a/cg;->PARSER:Lcom/google/n/ax;

    .line 345
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/cg;->k:Lcom/google/n/aw;

    .line 750
    new-instance v0, Lcom/google/d/a/a/cg;

    invoke-direct {v0}, Lcom/google/d/a/a/cg;-><init>()V

    sput-object v0, Lcom/google/d/a/a/cg;->h:Lcom/google/d/a/a/cg;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 245
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cg;->g:Lcom/google/n/ao;

    .line 260
    iput-byte v3, p0, Lcom/google/d/a/a/cg;->i:B

    .line 308
    iput v3, p0, Lcom/google/d/a/a/cg;->j:I

    .line 81
    iput v1, p0, Lcom/google/d/a/a/cg;->b:I

    .line 82
    iput v1, p0, Lcom/google/d/a/a/cg;->c:I

    .line 83
    iput-boolean v1, p0, Lcom/google/d/a/a/cg;->d:Z

    .line 84
    iput-boolean v1, p0, Lcom/google/d/a/a/cg;->e:Z

    .line 85
    iput v1, p0, Lcom/google/d/a/a/cg;->f:I

    .line 86
    iget-object v0, p0, Lcom/google/d/a/a/cg;->g:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 87
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Lcom/google/d/a/a/cg;-><init>()V

    .line 94
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 99
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 101
    sparse-switch v0, :sswitch_data_0

    .line 106
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 108
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 104
    goto :goto_0

    .line 113
    :sswitch_1
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/cg;->a:I

    .line 114
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/cg;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/cg;->au:Lcom/google/n/bn;

    throw v0

    .line 118
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/cg;->a:I

    .line 119
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/cg;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 146
    :catch_1
    move-exception v0

    .line 147
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 148
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 123
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/cg;->a:I

    .line 124
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/cg;->d:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 128
    :sswitch_4
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/cg;->a:I

    .line 129
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/d/a/a/cg;->e:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 133
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/cg;->a:I

    .line 134
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/cg;->f:I

    goto :goto_0

    .line 138
    :sswitch_6
    iget-object v0, p0, Lcom/google/d/a/a/cg;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 139
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/cg;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 150
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cg;->au:Lcom/google/n/bn;

    .line 151
    return-void

    .line 101
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 78
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 245
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cg;->g:Lcom/google/n/ao;

    .line 260
    iput-byte v1, p0, Lcom/google/d/a/a/cg;->i:B

    .line 308
    iput v1, p0, Lcom/google/d/a/a/cg;->j:I

    .line 79
    return-void
.end method

.method public static d()Lcom/google/d/a/a/cg;
    .locals 1

    .prologue
    .line 753
    sget-object v0, Lcom/google/d/a/a/cg;->h:Lcom/google/d/a/a/cg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ci;
    .locals 1

    .prologue
    .line 407
    new-instance v0, Lcom/google/d/a/a/ci;

    invoke-direct {v0}, Lcom/google/d/a/a/ci;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/cg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    sget-object v0, Lcom/google/d/a/a/cg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 286
    invoke-virtual {p0}, Lcom/google/d/a/a/cg;->c()I

    .line 287
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 288
    iget v0, p0, Lcom/google/d/a/a/cg;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 290
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 291
    iget v0, p0, Lcom/google/d/a/a/cg;->c:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 293
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 294
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/d/a/a/cg;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_8

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 296
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 297
    iget-boolean v0, p0, Lcom/google/d/a/a/cg;->e:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_9

    :goto_3
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 299
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 300
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/d/a/a/cg;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 302
    :cond_4
    :goto_4
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 303
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/d/a/a/cg;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 305
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/cg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 306
    return-void

    .line 288
    :cond_6
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 291
    :cond_7
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    :cond_8
    move v0, v2

    .line 294
    goto :goto_2

    :cond_9
    move v1, v2

    .line 297
    goto :goto_3

    .line 300
    :cond_a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 262
    iget-byte v0, p0, Lcom/google/d/a/a/cg;->i:B

    .line 263
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 281
    :goto_0
    return v0

    .line 264
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 266
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 267
    iput-byte v2, p0, Lcom/google/d/a/a/cg;->i:B

    move v0, v2

    .line 268
    goto :goto_0

    :cond_2
    move v0, v2

    .line 266
    goto :goto_1

    .line 270
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    .line 271
    iput-byte v2, p0, Lcom/google/d/a/a/cg;->i:B

    move v0, v2

    .line 272
    goto :goto_0

    :cond_4
    move v0, v2

    .line 270
    goto :goto_2

    .line 274
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 275
    iget-object v0, p0, Lcom/google/d/a/a/cg;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 276
    iput-byte v2, p0, Lcom/google/d/a/a/cg;->i:B

    move v0, v2

    .line 277
    goto :goto_0

    :cond_6
    move v0, v2

    .line 274
    goto :goto_3

    .line 280
    :cond_7
    iput-byte v1, p0, Lcom/google/d/a/a/cg;->i:B

    move v0, v1

    .line 281
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 310
    iget v0, p0, Lcom/google/d/a/a/cg;->j:I

    .line 311
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 340
    :goto_0
    return v0

    .line 314
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 315
    iget v0, p0, Lcom/google/d/a/a/cg;->b:I

    .line 316
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 318
    :goto_2
    iget v3, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 319
    iget v3, p0, Lcom/google/d/a/a/cg;->c:I

    .line 320
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 322
    :cond_1
    iget v3, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 323
    const/4 v3, 0x3

    iget-boolean v4, p0, Lcom/google/d/a/a/cg;->d:Z

    .line 324
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 326
    :cond_2
    iget v3, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 327
    iget-boolean v3, p0, Lcom/google/d/a/a/cg;->e:Z

    .line 328
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 330
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_5

    .line 331
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/d/a/a/cg;->f:I

    .line 332
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_4
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 334
    :cond_5
    iget v1, p0, Lcom/google/d/a/a/cg;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_6

    .line 335
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/d/a/a/cg;->g:Lcom/google/n/ao;

    .line 336
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 338
    :cond_6
    iget-object v1, p0, Lcom/google/d/a/a/cg;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 339
    iput v0, p0, Lcom/google/d/a/a/cg;->j:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 316
    goto/16 :goto_1

    :cond_8
    move v3, v1

    .line 320
    goto :goto_3

    :cond_9
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/google/d/a/a/cg;->newBuilder()Lcom/google/d/a/a/ci;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ci;->a(Lcom/google/d/a/a/cg;)Lcom/google/d/a/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/google/d/a/a/cg;->newBuilder()Lcom/google/d/a/a/ci;

    move-result-object v0

    return-object v0
.end method
