.class public final Lcom/google/d/a/a/kg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/kq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/kg;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/d/a/a/kg;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field e:I

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 243
    new-instance v0, Lcom/google/d/a/a/kh;

    invoke-direct {v0}, Lcom/google/d/a/a/kh;-><init>()V

    sput-object v0, Lcom/google/d/a/a/kg;->PARSER:Lcom/google/n/ax;

    .line 738
    new-instance v0, Lcom/google/d/a/a/ki;

    invoke-direct {v0}, Lcom/google/d/a/a/ki;-><init>()V

    .line 954
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/kg;->m:Lcom/google/n/aw;

    .line 1706
    new-instance v0, Lcom/google/d/a/a/kg;

    invoke-direct {v0}, Lcom/google/d/a/a/kg;-><init>()V

    sput-object v0, Lcom/google/d/a/a/kg;->j:Lcom/google/d/a/a/kg;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 783
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    .line 799
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->g:Lcom/google/n/ao;

    .line 815
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    .line 831
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->i:Lcom/google/n/ao;

    .line 846
    iput-byte v3, p0, Lcom/google/d/a/a/kg;->k:B

    .line 904
    iput v3, p0, Lcom/google/d/a/a/kg;->l:I

    .line 108
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    .line 109
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/d/a/a/kg;->c:I

    .line 110
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    .line 111
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/kg;->e:I

    .line 112
    iget-object v0, p0, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 113
    iget-object v0, p0, Lcom/google/d/a/a/kg;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 114
    iget-object v0, p0, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 115
    iget-object v0, p0, Lcom/google/d/a/a/kg;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 116
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 122
    invoke-direct {p0}, Lcom/google/d/a/a/kg;-><init>()V

    .line 125
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    move v1, v0

    .line 128
    :cond_0
    :goto_0
    if-nez v2, :cond_c

    .line 129
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 130
    sparse-switch v0, :sswitch_data_0

    .line 135
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 137
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 133
    goto :goto_0

    .line 142
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v3, :cond_1

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    .line 145
    or-int/lit8 v1, v1, 0x1

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 148
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 147
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_2

    .line 235
    iget-object v2, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    .line 237
    :cond_2
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_3

    .line 238
    iget-object v1, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    .line 240
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/kg;->au:Lcom/google/n/bn;

    throw v0

    .line 152
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 153
    invoke-static {v0}, Lcom/google/d/a/a/km;->a(I)Lcom/google/d/a/a/km;

    move-result-object v5

    .line 154
    if-nez v5, :cond_4

    .line 155
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 230
    :catch_1
    move-exception v0

    .line 231
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 232
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 157
    :cond_4
    :try_start_4
    iget v5, p0, Lcom/google/d/a/a/kg;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/kg;->a:I

    .line 158
    iput v0, p0, Lcom/google/d/a/a/kg;->c:I

    goto :goto_0

    .line 163
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 164
    invoke-static {v0}, Lcom/google/d/a/a/ko;->a(I)Lcom/google/d/a/a/ko;

    move-result-object v5

    .line 165
    if-nez v5, :cond_5

    .line 166
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 168
    :cond_5
    and-int/lit8 v5, v1, 0x4

    if-eq v5, v7, :cond_6

    .line 169
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    .line 170
    or-int/lit8 v1, v1, 0x4

    .line 172
    :cond_6
    iget-object v5, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 177
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 178
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 179
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_7

    const/4 v0, -0x1

    :goto_2
    if-lez v0, :cond_a

    .line 180
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 181
    invoke-static {v0}, Lcom/google/d/a/a/ko;->a(I)Lcom/google/d/a/a/ko;

    move-result-object v6

    .line 182
    if-nez v6, :cond_8

    .line 183
    const/4 v6, 0x3

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 179
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_2

    .line 185
    :cond_8
    and-int/lit8 v6, v1, 0x4

    if-eq v6, v7, :cond_9

    .line 186
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    .line 187
    or-int/lit8 v1, v1, 0x4

    .line 189
    :cond_9
    iget-object v6, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 192
    :cond_a
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 196
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 197
    invoke-static {v0}, Lcom/google/d/a/a/kk;->a(I)Lcom/google/d/a/a/kk;

    move-result-object v5

    .line 198
    if-nez v5, :cond_b

    .line 199
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 201
    :cond_b
    iget v5, p0, Lcom/google/d/a/a/kg;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/d/a/a/kg;->a:I

    .line 202
    iput v0, p0, Lcom/google/d/a/a/kg;->e:I

    goto/16 :goto_0

    .line 207
    :sswitch_6
    iget-object v0, p0, Lcom/google/d/a/a/kg;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 208
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/kg;->a:I

    goto/16 :goto_0

    .line 212
    :sswitch_7
    iget-object v0, p0, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 213
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/kg;->a:I

    goto/16 :goto_0

    .line 217
    :sswitch_8
    iget-object v0, p0, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 218
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/kg;->a:I

    goto/16 :goto_0

    .line 222
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/kg;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 223
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/kg;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 234
    :cond_c
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v3, :cond_d

    .line 235
    iget-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    .line 237
    :cond_d
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v7, :cond_e

    .line 238
    iget-object v0, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    .line 240
    :cond_e
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kg;->au:Lcom/google/n/bn;

    .line 241
    return-void

    .line 130
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0xfa2 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 105
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 783
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    .line 799
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->g:Lcom/google/n/ao;

    .line 815
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    .line 831
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kg;->i:Lcom/google/n/ao;

    .line 846
    iput-byte v1, p0, Lcom/google/d/a/a/kg;->k:B

    .line 904
    iput v1, p0, Lcom/google/d/a/a/kg;->l:I

    .line 106
    return-void
.end method

.method public static d()Lcom/google/d/a/a/kg;
    .locals 1

    .prologue
    .line 1709
    sget-object v0, Lcom/google/d/a/a/kg;->j:Lcom/google/d/a/a/kg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/kj;
    .locals 1

    .prologue
    .line 1016
    new-instance v0, Lcom/google/d/a/a/kj;

    invoke-direct {v0}, Lcom/google/d/a/a/kj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/kg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255
    sget-object v0, Lcom/google/d/a/a/kg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 876
    invoke-virtual {p0}, Lcom/google/d/a/a/kg;->c()I

    move v1, v2

    .line 877
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 878
    iget-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 877
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 880
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1

    .line 881
    iget v0, p0, Lcom/google/d/a/a/kg;->c:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_1
    :goto_1
    move v1, v2

    .line 883
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 884
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 883
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 881
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 884
    :cond_3
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 886
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_5

    .line 887
    iget v0, p0, Lcom/google/d/a/a/kg;->e:I

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_a

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 889
    :cond_5
    :goto_4
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 890
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/d/a/a/kg;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 892
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 893
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 895
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_8

    .line 896
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 898
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 899
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/kg;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 901
    :cond_9
    iget-object v0, p0, Lcom/google/d/a/a/kg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 902
    return-void

    .line 887
    :cond_a
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 848
    iget-byte v0, p0, Lcom/google/d/a/a/kg;->k:B

    .line 849
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 871
    :cond_0
    :goto_0
    return v2

    .line 850
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 852
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 853
    iget-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 854
    iput-byte v2, p0, Lcom/google/d/a/a/kg;->k:B

    goto :goto_0

    .line 852
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 858
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 859
    iget-object v0, p0, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 860
    iput-byte v2, p0, Lcom/google/d/a/a/kg;->k:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 858
    goto :goto_2

    .line 864
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 865
    iget-object v0, p0, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 866
    iput-byte v2, p0, Lcom/google/d/a/a/kg;->k:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 864
    goto :goto_3

    .line 870
    :cond_7
    iput-byte v3, p0, Lcom/google/d/a/a/kg;->k:B

    move v2, v3

    .line 871
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 906
    iget v0, p0, Lcom/google/d/a/a/kg;->l:I

    .line 907
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 949
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 910
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 911
    iget-object v0, p0, Lcom/google/d/a/a/kg;->b:Ljava/util/List;

    .line 912
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 910
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 914
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_2

    .line 915
    iget v0, p0, Lcom/google/d/a/a/kg;->c:I

    .line 916
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_2
    move v1, v2

    move v5, v2

    .line 920
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 921
    iget-object v0, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    .line 922
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v5, v0

    .line 920
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v4

    .line 916
    goto :goto_2

    :cond_4
    move v0, v4

    .line 922
    goto :goto_4

    .line 924
    :cond_5
    add-int v0, v3, v5

    .line 925
    iget-object v1, p0, Lcom/google/d/a/a/kg;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 927
    iget v1, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_7

    .line 928
    iget v1, p0, Lcom/google/d/a/a/kg;->e:I

    .line 929
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v1, :cond_6

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_6
    add-int v1, v3, v4

    add-int/2addr v0, v1

    .line 931
    :cond_7
    iget v1, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_8

    .line 932
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/d/a/a/kg;->g:Lcom/google/n/ao;

    .line 933
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 935
    :cond_8
    iget v1, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_9

    .line 936
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/d/a/a/kg;->h:Lcom/google/n/ao;

    .line 937
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 939
    :cond_9
    iget v1, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_a

    .line 940
    const/4 v1, 0x7

    iget-object v3, p0, Lcom/google/d/a/a/kg;->f:Lcom/google/n/ao;

    .line 941
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 943
    :cond_a
    iget v1, p0, Lcom/google/d/a/a/kg;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_b

    .line 944
    const/16 v1, 0x1f4

    iget-object v3, p0, Lcom/google/d/a/a/kg;->i:Lcom/google/n/ao;

    .line 945
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 947
    :cond_b
    iget-object v1, p0, Lcom/google/d/a/a/kg;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 948
    iput v0, p0, Lcom/google/d/a/a/kg;->l:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/d/a/a/kg;->newBuilder()Lcom/google/d/a/a/kj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/kj;->a(Lcom/google/d/a/a/kg;)Lcom/google/d/a/a/kj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/d/a/a/kg;->newBuilder()Lcom/google/d/a/a/kj;

    move-result-object v0

    return-object v0
.end method
