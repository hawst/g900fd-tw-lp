.class public final Lcom/google/d/a/a/a/h;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/a/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/a/f;",
        "Lcom/google/d/a/a/a/h;",
        ">;",
        "Lcom/google/d/a/a/a/i;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:I

.field private g:I

.field private h:I

.field private i:F

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2675
    sget-object v0, Lcom/google/d/a/a/a/f;->k:Lcom/google/d/a/a/a/f;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3025
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/d/a/a/a/h;->i:F

    .line 3058
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    .line 2676
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/a/f;)Lcom/google/d/a/a/a/h;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2750
    invoke-static {}, Lcom/google/d/a/a/a/f;->d()Lcom/google/d/a/a/a/f;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2786
    :goto_0
    return-object p0

    .line 2751
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2752
    iget v2, p1, Lcom/google/d/a/a/a/f;->b:F

    iget v3, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/a/h;->a:I

    iput v2, p0, Lcom/google/d/a/a/a/h;->b:F

    .line 2754
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2755
    iget v2, p1, Lcom/google/d/a/a/a/f;->c:F

    iget v3, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/a/h;->a:I

    iput v2, p0, Lcom/google/d/a/a/a/h;->c:F

    .line 2757
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 2758
    iget v2, p1, Lcom/google/d/a/a/a/f;->d:F

    iget v3, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/a/h;->a:I

    iput v2, p0, Lcom/google/d/a/a/a/h;->d:F

    .line 2760
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 2761
    iget v2, p1, Lcom/google/d/a/a/a/f;->e:F

    iget v3, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/a/h;->a:I

    iput v2, p0, Lcom/google/d/a/a/a/h;->e:F

    .line 2763
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 2764
    iget v2, p1, Lcom/google/d/a/a/a/f;->f:I

    iget v3, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/a/h;->a:I

    iput v2, p0, Lcom/google/d/a/a/a/h;->f:I

    .line 2766
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 2767
    iget v2, p1, Lcom/google/d/a/a/a/f;->g:I

    iget v3, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/a/h;->a:I

    iput v2, p0, Lcom/google/d/a/a/a/h;->g:I

    .line 2769
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 2770
    iget v2, p1, Lcom/google/d/a/a/a/f;->h:I

    iget v3, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/a/h;->a:I

    iput v2, p0, Lcom/google/d/a/a/a/h;->h:I

    .line 2772
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    :goto_8
    if-eqz v0, :cond_8

    .line 2773
    iget v0, p1, Lcom/google/d/a/a/a/f;->i:F

    iget v1, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/d/a/a/a/h;->a:I

    iput v0, p0, Lcom/google/d/a/a/a/h;->i:F

    .line 2775
    :cond_8
    iget-object v0, p1, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2776
    iget-object v0, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2777
    iget-object v0, p1, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    .line 2778
    iget v0, p0, Lcom/google/d/a/a/a/h;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/d/a/a/a/h;->a:I

    .line 2785
    :cond_9
    :goto_9
    iget-object v0, p1, Lcom/google/d/a/a/a/f;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 2751
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 2754
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 2757
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 2760
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 2763
    goto :goto_5

    :cond_f
    move v2, v1

    .line 2766
    goto :goto_6

    :cond_10
    move v2, v1

    .line 2769
    goto :goto_7

    :cond_11
    move v0, v1

    .line 2772
    goto :goto_8

    .line 2780
    :cond_12
    iget v0, p0, Lcom/google/d/a/a/a/h;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_13

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/a/h;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/a/h;->a:I

    .line 2781
    :cond_13
    iget-object v0, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2667
    new-instance v2, Lcom/google/d/a/a/a/f;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/a/f;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/a/h;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/a/h;->b:F

    iput v1, v2, Lcom/google/d/a/a/a/f;->b:F

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/d/a/a/a/h;->c:F

    iput v1, v2, Lcom/google/d/a/a/a/f;->c:F

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/d/a/a/a/h;->d:F

    iput v1, v2, Lcom/google/d/a/a/a/f;->d:F

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/a/h;->e:F

    iput v1, v2, Lcom/google/d/a/a/a/f;->e:F

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/d/a/a/a/h;->f:I

    iput v1, v2, Lcom/google/d/a/a/a/f;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/d/a/a/a/h;->g:I

    iput v1, v2, Lcom/google/d/a/a/a/f;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/d/a/a/a/h;->h:I

    iput v1, v2, Lcom/google/d/a/a/a/f;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/d/a/a/a/h;->i:F

    iput v1, v2, Lcom/google/d/a/a/a/f;->i:F

    iget v1, p0, Lcom/google/d/a/a/a/h;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/a/h;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/d/a/a/a/h;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    iput v0, v2, Lcom/google/d/a/a/a/f;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2667
    check-cast p1, Lcom/google/d/a/a/a/f;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/a/h;->a(Lcom/google/d/a/a/a/f;)Lcom/google/d/a/a/a/h;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2790
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2791
    iget-object v0, p0, Lcom/google/d/a/a/a/h;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/a/j;->d()Lcom/google/d/a/a/a/j;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/a/j;

    invoke-virtual {v0}, Lcom/google/d/a/a/a/j;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2796
    :goto_1
    return v2

    .line 2790
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2796
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
