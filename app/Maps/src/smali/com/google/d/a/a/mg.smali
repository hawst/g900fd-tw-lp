.class public final enum Lcom/google/d/a/a/mg;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mg;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/mg;

.field public static final enum b:Lcom/google/d/a/a/mg;

.field public static final enum c:Lcom/google/d/a/a/mg;

.field private static final synthetic e:[Lcom/google/d/a/a/mg;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2080
    new-instance v0, Lcom/google/d/a/a/mg;

    const-string v1, "BICYCLE_SAFETY_RECOMMENDED"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/mg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mg;->a:Lcom/google/d/a/a/mg;

    .line 2084
    new-instance v0, Lcom/google/d/a/a/mg;

    const-string v1, "BICYCLE_SAFETY_NEUTRAL"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mg;->b:Lcom/google/d/a/a/mg;

    .line 2088
    new-instance v0, Lcom/google/d/a/a/mg;

    const-string v1, "BICYCLE_SAFETY_CAUTION"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/d/a/a/mg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mg;->c:Lcom/google/d/a/a/mg;

    .line 2075
    new-array v0, v5, [Lcom/google/d/a/a/mg;

    sget-object v1, Lcom/google/d/a/a/mg;->a:Lcom/google/d/a/a/mg;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mg;->b:Lcom/google/d/a/a/mg;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/mg;->c:Lcom/google/d/a/a/mg;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/d/a/a/mg;->e:[Lcom/google/d/a/a/mg;

    .line 2123
    new-instance v0, Lcom/google/d/a/a/mh;

    invoke-direct {v0}, Lcom/google/d/a/a/mh;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2133
    iput p3, p0, Lcom/google/d/a/a/mg;->d:I

    .line 2134
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mg;
    .locals 1

    .prologue
    .line 2110
    packed-switch p0, :pswitch_data_0

    .line 2114
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2111
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/mg;->a:Lcom/google/d/a/a/mg;

    goto :goto_0

    .line 2112
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/mg;->b:Lcom/google/d/a/a/mg;

    goto :goto_0

    .line 2113
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/mg;->c:Lcom/google/d/a/a/mg;

    goto :goto_0

    .line 2110
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mg;
    .locals 1

    .prologue
    .line 2075
    const-class v0, Lcom/google/d/a/a/mg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mg;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mg;
    .locals 1

    .prologue
    .line 2075
    sget-object v0, Lcom/google/d/a/a/mg;->e:[Lcom/google/d/a/a/mg;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mg;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2106
    iget v0, p0, Lcom/google/d/a/a/mg;->d:I

    return v0
.end method
