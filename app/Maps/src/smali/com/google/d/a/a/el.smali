.class public final Lcom/google/d/a/a/el;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/em;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ej;",
        "Lcom/google/d/a/a/el;",
        ">;",
        "Lcom/google/d/a/a/em;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lcom/google/d/a/a/ej;->e:Lcom/google/d/a/a/ej;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 455
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    .line 591
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/el;->d:Lcom/google/n/ao;

    .line 348
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ej;)Lcom/google/d/a/a/el;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 388
    invoke-static {}, Lcom/google/d/a/a/ej;->d()Lcom/google/d/a/a/ej;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 407
    :goto_0
    return-object p0

    .line 389
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/ej;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 390
    iget v2, p1, Lcom/google/d/a/a/ej;->b:I

    iget v3, p0, Lcom/google/d/a/a/el;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/el;->a:I

    iput v2, p0, Lcom/google/d/a/a/el;->b:I

    .line 392
    :cond_1
    iget-object v2, p1, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 393
    iget-object v2, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 394
    iget-object v2, p1, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    .line 395
    iget v2, p0, Lcom/google/d/a/a/el;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/d/a/a/el;->a:I

    .line 402
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/d/a/a/ej;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 403
    iget-object v0, p0, Lcom/google/d/a/a/el;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/ej;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 404
    iget v0, p0, Lcom/google/d/a/a/el;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/el;->a:I

    .line 406
    :cond_3
    iget-object v0, p1, Lcom/google/d/a/a/ej;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 389
    goto :goto_1

    .line 397
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/el;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/el;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/el;->a:I

    .line 398
    :cond_6
    iget-object v2, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_7
    move v0, v1

    .line 402
    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 339
    new-instance v2, Lcom/google/d/a/a/ej;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ej;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/el;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/el;->b:I

    iput v4, v2, Lcom/google/d/a/a/ej;->b:I

    iget v4, p0, Lcom/google/d/a/a/el;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/el;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/d/a/a/el;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ej;->c:Ljava/util/List;

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v3, v2, Lcom/google/d/a/a/ej;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/el;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/el;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/ej;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 339
    check-cast p1, Lcom/google/d/a/a/ej;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/el;->a(Lcom/google/d/a/a/ej;)Lcom/google/d/a/a/el;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 411
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/google/d/a/a/el;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    :goto_1
    return v2

    .line 411
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 417
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
