.class public final Lcom/google/d/a/a/jb;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/jg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/jb;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/a/a/jb;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:F

.field d:F

.field e:Ljava/lang/Object;

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/google/d/a/a/jc;

    invoke-direct {v0}, Lcom/google/d/a/a/jc;-><init>()V

    sput-object v0, Lcom/google/d/a/a/jb;->PARSER:Lcom/google/n/ax;

    .line 967
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/jb;->j:Lcom/google/n/aw;

    .line 1371
    new-instance v0, Lcom/google/d/a/a/jb;

    invoke-direct {v0}, Lcom/google/d/a/a/jb;-><init>()V

    sput-object v0, Lcom/google/d/a/a/jb;->g:Lcom/google/d/a/a/jb;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 884
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jb;->f:Lcom/google/n/ao;

    .line 899
    iput-byte v3, p0, Lcom/google/d/a/a/jb;->h:B

    .line 934
    iput v3, p0, Lcom/google/d/a/a/jb;->i:I

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/jb;->b:I

    .line 78
    iput v2, p0, Lcom/google/d/a/a/jb;->c:F

    .line 79
    iput v2, p0, Lcom/google/d/a/a/jb;->d:F

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jb;->e:Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/google/d/a/a/jb;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 82
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 88
    invoke-direct {p0}, Lcom/google/d/a/a/jb;-><init>()V

    .line 89
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 94
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 95
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 96
    sparse-switch v3, :sswitch_data_0

    .line 101
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 103
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 99
    goto :goto_0

    .line 108
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 109
    invoke-static {v3}, Lcom/google/d/a/a/je;->a(I)Lcom/google/d/a/a/je;

    move-result-object v4

    .line 110
    if-nez v4, :cond_1

    .line 111
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/jb;->au:Lcom/google/n/bn;

    throw v0

    .line 113
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/d/a/a/jb;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/jb;->a:I

    .line 114
    iput v3, p0, Lcom/google/d/a/a/jb;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 143
    :catch_1
    move-exception v0

    .line 144
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 145
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 119
    :sswitch_2
    :try_start_4
    iget v3, p0, Lcom/google/d/a/a/jb;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/jb;->a:I

    .line 120
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/jb;->c:F

    goto :goto_0

    .line 124
    :sswitch_3
    iget v3, p0, Lcom/google/d/a/a/jb;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/jb;->a:I

    .line 125
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/jb;->d:F

    goto :goto_0

    .line 129
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 130
    iget v4, p0, Lcom/google/d/a/a/jb;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/a/a/jb;->a:I

    .line 131
    iput-object v3, p0, Lcom/google/d/a/a/jb;->e:Ljava/lang/Object;

    goto :goto_0

    .line 135
    :sswitch_5
    iget-object v3, p0, Lcom/google/d/a/a/jb;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 136
    iget v3, p0, Lcom/google/d/a/a/jb;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/jb;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 147
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jb;->au:Lcom/google/n/bn;

    .line 148
    return-void

    .line 96
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x25 -> :sswitch_3
        0x2a -> :sswitch_4
        0xfa2 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 74
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 884
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jb;->f:Lcom/google/n/ao;

    .line 899
    iput-byte v1, p0, Lcom/google/d/a/a/jb;->h:B

    .line 934
    iput v1, p0, Lcom/google/d/a/a/jb;->i:I

    .line 75
    return-void
.end method

.method public static d()Lcom/google/d/a/a/jb;
    .locals 1

    .prologue
    .line 1374
    sget-object v0, Lcom/google/d/a/a/jb;->g:Lcom/google/d/a/a/jb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/jd;
    .locals 1

    .prologue
    .line 1029
    new-instance v0, Lcom/google/d/a/a/jd;

    invoke-direct {v0}, Lcom/google/d/a/a/jd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/jb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    sget-object v0, Lcom/google/d/a/a/jb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x5

    const/4 v2, 0x2

    .line 915
    invoke-virtual {p0}, Lcom/google/d/a/a/jb;->c()I

    .line 916
    iget v0, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_0

    .line 917
    iget v0, p0, Lcom/google/d/a/a/jb;->b:I

    const/4 v1, 0x0

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 919
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 920
    iget v0, p0, Lcom/google/d/a/a/jb;->c:F

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 922
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 923
    iget v0, p0, Lcom/google/d/a/a/jb;->d:F

    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 925
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 926
    iget-object v0, p0, Lcom/google/d/a/a/jb;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jb;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 928
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 929
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/jb;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 931
    :cond_4
    iget-object v0, p0, Lcom/google/d/a/a/jb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 932
    return-void

    .line 917
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 926
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 901
    iget-byte v2, p0, Lcom/google/d/a/a/jb;->h:B

    .line 902
    if-ne v2, v0, :cond_0

    .line 910
    :goto_0
    return v0

    .line 903
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 905
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 906
    iput-byte v1, p0, Lcom/google/d/a/a/jb;->h:B

    move v0, v1

    .line 907
    goto :goto_0

    :cond_2
    move v2, v1

    .line 905
    goto :goto_1

    .line 909
    :cond_3
    iput-byte v0, p0, Lcom/google/d/a/a/jb;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 936
    iget v0, p0, Lcom/google/d/a/a/jb;->i:I

    .line 937
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 962
    :goto_0
    return v0

    .line 940
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 941
    iget v0, p0, Lcom/google/d/a/a/jb;->b:I

    .line 942
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 944
    :goto_2
    iget v2, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 945
    iget v2, p0, Lcom/google/d/a/a/jb;->c:F

    .line 946
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 948
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_6

    .line 949
    iget v2, p0, Lcom/google/d/a/a/jb;->d:F

    .line 950
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    move v2, v0

    .line 952
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 953
    const/4 v3, 0x5

    .line 954
    iget-object v0, p0, Lcom/google/d/a/a/jb;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jb;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 956
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 957
    const/16 v0, 0x1f4

    iget-object v3, p0, Lcom/google/d/a/a/jb;->f:Lcom/google/n/ao;

    .line 958
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 960
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/jb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 961
    iput v0, p0, Lcom/google/d/a/a/jb;->i:I

    goto/16 :goto_0

    .line 942
    :cond_4
    const/16 v0, 0xa

    goto/16 :goto_1

    .line 954
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v2, v0

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/a/a/jb;->newBuilder()Lcom/google/d/a/a/jd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/jd;->a(Lcom/google/d/a/a/jb;)Lcom/google/d/a/a/jd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/d/a/a/jb;->newBuilder()Lcom/google/d/a/a/jd;

    move-result-object v0

    return-object v0
.end method
