.class public final enum Lcom/google/d/a/a/ou;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ou;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ou;

.field public static final enum b:Lcom/google/d/a/a/ou;

.field private static final synthetic d:[Lcom/google/d/a/a/ou;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2509
    new-instance v0, Lcom/google/d/a/a/ou;

    const-string v1, "COMPONENT_TYPE_POSITIVE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/d/a/a/ou;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ou;->a:Lcom/google/d/a/a/ou;

    .line 2513
    new-instance v0, Lcom/google/d/a/a/ou;

    const-string v1, "COMPONENT_TYPE_MISSING_DATA"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/a/a/ou;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ou;->b:Lcom/google/d/a/a/ou;

    .line 2504
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/d/a/a/ou;

    sget-object v1, Lcom/google/d/a/a/ou;->a:Lcom/google/d/a/a/ou;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/ou;->b:Lcom/google/d/a/a/ou;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/d/a/a/ou;->d:[Lcom/google/d/a/a/ou;

    .line 2543
    new-instance v0, Lcom/google/d/a/a/ov;

    invoke-direct {v0}, Lcom/google/d/a/a/ov;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2552
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2553
    iput p3, p0, Lcom/google/d/a/a/ou;->c:I

    .line 2554
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ou;
    .locals 1

    .prologue
    .line 2531
    packed-switch p0, :pswitch_data_0

    .line 2534
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2532
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/ou;->a:Lcom/google/d/a/a/ou;

    goto :goto_0

    .line 2533
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/ou;->b:Lcom/google/d/a/a/ou;

    goto :goto_0

    .line 2531
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ou;
    .locals 1

    .prologue
    .line 2504
    const-class v0, Lcom/google/d/a/a/ou;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ou;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ou;
    .locals 1

    .prologue
    .line 2504
    sget-object v0, Lcom/google/d/a/a/ou;->d:[Lcom/google/d/a/a/ou;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ou;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ou;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2527
    iget v0, p0, Lcom/google/d/a/a/ou;->c:I

    return v0
.end method
