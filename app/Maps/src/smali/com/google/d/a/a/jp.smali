.class public final Lcom/google/d/a/a/jp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/js;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/jn;",
        "Lcom/google/d/a/a/jp;",
        ">;",
        "Lcom/google/d/a/a/js;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 521
    sget-object v0, Lcom/google/d/a/a/jn;->f:Lcom/google/d/a/a/jn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 606
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jp;->b:Ljava/lang/Object;

    .line 682
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jp;->c:Ljava/lang/Object;

    .line 758
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jp;->d:Ljava/lang/Object;

    .line 834
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/jp;->e:I

    .line 522
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/jn;)Lcom/google/d/a/a/jp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 565
    invoke-static {}, Lcom/google/d/a/a/jn;->d()Lcom/google/d/a/a/jn;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 585
    :goto_0
    return-object p0

    .line 566
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 567
    iget v2, p0, Lcom/google/d/a/a/jp;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/jp;->a:I

    .line 568
    iget-object v2, p1, Lcom/google/d/a/a/jn;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/jp;->b:Ljava/lang/Object;

    .line 571
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 572
    iget v2, p0, Lcom/google/d/a/a/jp;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/jp;->a:I

    .line 573
    iget-object v2, p1, Lcom/google/d/a/a/jn;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/jp;->c:Ljava/lang/Object;

    .line 576
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 577
    iget v2, p0, Lcom/google/d/a/a/jp;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/jp;->a:I

    .line 578
    iget-object v2, p1, Lcom/google/d/a/a/jn;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/jp;->d:Ljava/lang/Object;

    .line 581
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_a

    .line 582
    iget v0, p1, Lcom/google/d/a/a/jn;->e:I

    invoke-static {v0}, Lcom/google/d/a/a/jq;->a(I)Lcom/google/d/a/a/jq;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/d/a/a/jq;->a:Lcom/google/d/a/a/jq;

    :cond_4
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 566
    goto :goto_1

    :cond_6
    move v2, v1

    .line 571
    goto :goto_2

    :cond_7
    move v2, v1

    .line 576
    goto :goto_3

    :cond_8
    move v0, v1

    .line 581
    goto :goto_4

    .line 582
    :cond_9
    iget v1, p0, Lcom/google/d/a/a/jp;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/d/a/a/jp;->a:I

    iget v0, v0, Lcom/google/d/a/a/jq;->d:I

    iput v0, p0, Lcom/google/d/a/a/jp;->e:I

    .line 584
    :cond_a
    iget-object v0, p1, Lcom/google/d/a/a/jn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 513
    new-instance v2, Lcom/google/d/a/a/jn;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/jn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/jp;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/d/a/a/jp;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/jn;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/d/a/a/jp;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/jn;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/jp;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/jn;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/jp;->e:I

    iput v1, v2, Lcom/google/d/a/a/jn;->e:I

    iput v0, v2, Lcom/google/d/a/a/jn;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 513
    check-cast p1, Lcom/google/d/a/a/jn;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/jp;->a(Lcom/google/d/a/a/jn;)Lcom/google/d/a/a/jp;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 589
    iget v2, p0, Lcom/google/d/a/a/jp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 601
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 589
    goto :goto_0

    .line 593
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/jp;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    .line 597
    iget v2, p0, Lcom/google/d/a/a/jp;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    move v0, v1

    .line 601
    goto :goto_1

    :cond_3
    move v2, v0

    .line 593
    goto :goto_2

    :cond_4
    move v2, v0

    .line 597
    goto :goto_3
.end method
