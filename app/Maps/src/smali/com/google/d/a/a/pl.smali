.class public final Lcom/google/d/a/a/pl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/pq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/pj;",
        "Lcom/google/d/a/a/pl;",
        ">;",
        "Lcom/google/d/a/a/pq;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field private e:Z

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2065
    sget-object v0, Lcom/google/d/a/a/pj;->g:Lcom/google/d/a/a/pj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2147
    iput v1, p0, Lcom/google/d/a/a/pl;->b:I

    .line 2215
    iput v1, p0, Lcom/google/d/a/a/pl;->f:I

    .line 2251
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/pl;->c:Lcom/google/n/ao;

    .line 2310
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/pl;->d:Lcom/google/n/ao;

    .line 2066
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/pj;)Lcom/google/d/a/a/pl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2119
    invoke-static {}, Lcom/google/d/a/a/pj;->d()Lcom/google/d/a/a/pj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2138
    :goto_0
    return-object p0

    .line 2120
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 2121
    iget v2, p1, Lcom/google/d/a/a/pj;->b:I

    invoke-static {v2}, Lcom/google/d/a/a/pm;->a(I)Lcom/google/d/a/a/pm;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/d/a/a/pm;->a:Lcom/google/d/a/a/pm;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 2120
    goto :goto_1

    .line 2121
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/pl;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/pl;->a:I

    iget v2, v2, Lcom/google/d/a/a/pm;->c:I

    iput v2, p0, Lcom/google/d/a/a/pl;->b:I

    .line 2123
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 2124
    iget-boolean v2, p1, Lcom/google/d/a/a/pj;->c:Z

    iget v3, p0, Lcom/google/d/a/a/pl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/pl;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/pl;->e:Z

    .line 2126
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 2127
    iget v2, p1, Lcom/google/d/a/a/pj;->d:I

    invoke-static {v2}, Lcom/google/d/a/a/po;->a(I)Lcom/google/d/a/a/po;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/d/a/a/po;->a:Lcom/google/d/a/a/po;

    :cond_6
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v2, v1

    .line 2123
    goto :goto_2

    :cond_8
    move v2, v1

    .line 2126
    goto :goto_3

    .line 2127
    :cond_9
    iget v3, p0, Lcom/google/d/a/a/pl;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/pl;->a:I

    iget v2, v2, Lcom/google/d/a/a/po;->z:I

    iput v2, p0, Lcom/google/d/a/a/pl;->f:I

    .line 2129
    :cond_a
    iget v2, p1, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 2130
    iget-object v2, p0, Lcom/google/d/a/a/pl;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2131
    iget v2, p0, Lcom/google/d/a/a/pl;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/pl;->a:I

    .line 2133
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    :goto_5
    if-eqz v0, :cond_c

    .line 2134
    iget-object v0, p0, Lcom/google/d/a/a/pl;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2135
    iget v0, p0, Lcom/google/d/a/a/pl;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/pl;->a:I

    .line 2137
    :cond_c
    iget-object v0, p1, Lcom/google/d/a/a/pj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 2129
    goto :goto_4

    :cond_e
    move v0, v1

    .line 2133
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2057
    new-instance v2, Lcom/google/d/a/a/pj;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/pj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/pl;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/pl;->b:I

    iput v4, v2, Lcom/google/d/a/a/pj;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/d/a/a/pl;->e:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/pj;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/pl;->f:I

    iput v4, v2, Lcom/google/d/a/a/pj;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/pl;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/pl;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v3, v2, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/pl;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/pl;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/pj;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2057
    check-cast p1, Lcom/google/d/a/a/pj;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/pl;->a(Lcom/google/d/a/a/pj;)Lcom/google/d/a/a/pl;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2142
    const/4 v0, 0x1

    return v0
.end method
