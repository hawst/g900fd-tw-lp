.class public final Lcom/google/d/a/a/dc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/dj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/dc;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/d/a/a/dc;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Z

.field c:I

.field d:J

.field e:Z

.field f:I

.field g:Z

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 223
    new-instance v0, Lcom/google/d/a/a/dd;

    invoke-direct {v0}, Lcom/google/d/a/a/dd;-><init>()V

    sput-object v0, Lcom/google/d/a/a/dc;->PARSER:Lcom/google/n/ax;

    .line 713
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/dc;->n:Lcom/google/n/aw;

    .line 1396
    new-instance v0, Lcom/google/d/a/a/dc;

    invoke-direct {v0}, Lcom/google/d/a/a/dc;-><init>()V

    sput-object v0, Lcom/google/d/a/a/dc;->k:Lcom/google/d/a/a/dc;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 112
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 529
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    .line 545
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    .line 603
    iput-byte v1, p0, Lcom/google/d/a/a/dc;->l:B

    .line 664
    iput v1, p0, Lcom/google/d/a/a/dc;->m:I

    .line 113
    iput-boolean v3, p0, Lcom/google/d/a/a/dc;->b:Z

    .line 114
    iput v3, p0, Lcom/google/d/a/a/dc;->c:I

    .line 115
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/dc;->d:J

    .line 116
    iput-boolean v3, p0, Lcom/google/d/a/a/dc;->e:Z

    .line 117
    iput v3, p0, Lcom/google/d/a/a/dc;->f:I

    .line 118
    iput-boolean v3, p0, Lcom/google/d/a/a/dc;->g:Z

    .line 119
    iget-object v0, p0, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 120
    iget-object v0, p0, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 121
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    .line 122
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v8, 0x100

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 128
    invoke-direct {p0}, Lcom/google/d/a/a/dc;-><init>()V

    .line 131
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 134
    :cond_0
    :goto_0
    if-nez v4, :cond_8

    .line 135
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 136
    sparse-switch v0, :sswitch_data_0

    .line 141
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 143
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 139
    goto :goto_0

    .line 148
    :sswitch_1
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/dc;->a:I

    .line 149
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/dc;->e:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x100

    if-ne v1, v8, :cond_1

    .line 218
    iget-object v1, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    .line 220
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/dc;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    move v0, v3

    .line 149
    goto :goto_1

    .line 153
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/dc;->a:I

    .line 154
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/d/a/a/dc;->b:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 213
    :catch_1
    move-exception v0

    .line 214
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 215
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    move v0, v3

    .line 154
    goto :goto_2

    .line 158
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 159
    invoke-static {v0}, Lcom/google/d/a/a/dh;->a(I)Lcom/google/d/a/a/dh;

    move-result-object v6

    .line 160
    if-nez v6, :cond_4

    .line 161
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 163
    :cond_4
    iget v6, p0, Lcom/google/d/a/a/dc;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/d/a/a/dc;->a:I

    .line 164
    iput v0, p0, Lcom/google/d/a/a/dc;->c:I

    goto :goto_0

    .line 169
    :sswitch_4
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/dc;->a:I

    .line 170
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_5

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/d/a/a/dc;->g:Z

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_3

    .line 174
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 175
    invoke-static {v0}, Lcom/google/d/a/a/df;->a(I)Lcom/google/d/a/a/df;

    move-result-object v6

    .line 176
    if-nez v6, :cond_6

    .line 177
    const/4 v6, 0x5

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 179
    :cond_6
    iget v6, p0, Lcom/google/d/a/a/dc;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/d/a/a/dc;->a:I

    .line 180
    iput v0, p0, Lcom/google/d/a/a/dc;->f:I

    goto/16 :goto_0

    .line 185
    :sswitch_6
    iget-object v0, p0, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 186
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/dc;->a:I

    goto/16 :goto_0

    .line 190
    :sswitch_7
    iget-object v0, p0, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 191
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/dc;->a:I

    goto/16 :goto_0

    .line 195
    :sswitch_8
    and-int/lit16 v0, v1, 0x100

    if-eq v0, v8, :cond_7

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    .line 198
    or-int/lit16 v1, v1, 0x100

    .line 200
    :cond_7
    iget-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 201
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 200
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 205
    :sswitch_9
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/dc;->a:I

    .line 206
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/d/a/a/dc;->d:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 217
    :cond_8
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v8, :cond_9

    .line 218
    iget-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    .line 220
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/dc;->au:Lcom/google/n/bn;

    .line 221
    return-void

    .line 136
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 110
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 529
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    .line 545
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    .line 603
    iput-byte v1, p0, Lcom/google/d/a/a/dc;->l:B

    .line 664
    iput v1, p0, Lcom/google/d/a/a/dc;->m:I

    .line 111
    return-void
.end method

.method public static d()Lcom/google/d/a/a/dc;
    .locals 1

    .prologue
    .line 1399
    sget-object v0, Lcom/google/d/a/a/dc;->k:Lcom/google/d/a/a/dc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/de;
    .locals 1

    .prologue
    .line 775
    new-instance v0, Lcom/google/d/a/a/de;

    invoke-direct {v0}, Lcom/google/d/a/a/de;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/dc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    sget-object v0, Lcom/google/d/a/a/dc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 633
    invoke-virtual {p0}, Lcom/google/d/a/a/dc;->c()I

    .line 634
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_0

    .line 635
    iget-boolean v0, p0, Lcom/google/d/a/a/dc;->e:Z

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 637
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 638
    iget-boolean v0, p0, Lcom/google/d/a/a/dc;->b:Z

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 640
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    .line 641
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/d/a/a/dc;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_9

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 643
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_3

    .line 644
    iget-boolean v0, p0, Lcom/google/d/a/a/dc;->g:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_a

    :goto_3
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 646
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 647
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/d/a/a/dc;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 649
    :cond_4
    :goto_4
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 650
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 652
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 653
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_6
    move v1, v2

    .line 655
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 656
    iget-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v8, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 655
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_7
    move v0, v2

    .line 635
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 638
    goto/16 :goto_1

    .line 641
    :cond_9
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    :cond_a
    move v1, v2

    .line 644
    goto/16 :goto_3

    .line 647
    :cond_b
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 658
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_d

    .line 659
    const/16 v0, 0x9

    iget-wide v4, p0, Lcom/google/d/a/a/dc;->d:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 661
    :cond_d
    iget-object v0, p0, Lcom/google/d/a/a/dc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 662
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 605
    iget-byte v0, p0, Lcom/google/d/a/a/dc;->l:B

    .line 606
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 628
    :cond_0
    :goto_0
    return v2

    .line 607
    :cond_1
    if-eqz v0, :cond_0

    .line 609
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 610
    iget-object v0, p0, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 611
    iput-byte v2, p0, Lcom/google/d/a/a/dc;->l:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 609
    goto :goto_1

    .line 615
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 616
    iget-object v0, p0, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 617
    iput-byte v2, p0, Lcom/google/d/a/a/dc;->l:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 615
    goto :goto_2

    :cond_5
    move v1, v2

    .line 621
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 622
    iget-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/nz;->d()Lcom/google/d/a/a/nz;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nz;

    invoke-virtual {v0}, Lcom/google/d/a/a/nz;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 623
    iput-byte v2, p0, Lcom/google/d/a/a/dc;->l:B

    goto :goto_0

    .line 621
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 627
    :cond_7
    iput-byte v3, p0, Lcom/google/d/a/a/dc;->l:B

    move v2, v3

    .line 628
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 666
    iget v0, p0, Lcom/google/d/a/a/dc;->m:I

    .line 667
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 708
    :goto_0
    return v0

    .line 670
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_b

    .line 671
    iget-boolean v0, p0, Lcom/google/d/a/a/dc;->e:Z

    .line 672
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 674
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    .line 675
    iget-boolean v2, p0, Lcom/google/d/a/a/dc;->b:Z

    .line 676
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 678
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    .line 679
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/d/a/a/dc;->c:I

    .line 680
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 682
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_3

    .line 683
    iget-boolean v2, p0, Lcom/google/d/a/a/dc;->g:Z

    .line 684
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 686
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 687
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/d/a/a/dc;->f:I

    .line 688
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 690
    :cond_4
    iget v2, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_5

    .line 691
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    .line 692
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 694
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_6

    .line 695
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    .line 696
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_6
    move v2, v1

    move v3, v0

    .line 698
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 699
    iget-object v0, p0, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    .line 700
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 698
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 680
    :cond_7
    const/16 v2, 0xa

    goto/16 :goto_2

    .line 688
    :cond_8
    const/16 v2, 0xa

    goto :goto_3

    .line 702
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_a

    .line 703
    const/16 v0, 0x9

    iget-wide v4, p0, Lcom/google/d/a/a/dc;->d:J

    .line 704
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 706
    :cond_a
    iget-object v0, p0, Lcom/google/d/a/a/dc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 707
    iput v0, p0, Lcom/google/d/a/a/dc;->m:I

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 104
    invoke-static {}, Lcom/google/d/a/a/dc;->newBuilder()Lcom/google/d/a/a/de;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/de;->a(Lcom/google/d/a/a/dc;)Lcom/google/d/a/a/de;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 104
    invoke-static {}, Lcom/google/d/a/a/dc;->newBuilder()Lcom/google/d/a/a/de;

    move-result-object v0

    return-object v0
.end method
