.class public final Lcom/google/d/a/a/n;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/l;",
        "Lcom/google/d/a/a/n;",
        ">;",
        "Lcom/google/d/a/a/o;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 667
    sget-object v0, Lcom/google/d/a/a/l;->j:Lcom/google/d/a/a/l;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 824
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    .line 961
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    .line 1098
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    .line 1331
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    .line 1467
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/n;->i:Lcom/google/n/ao;

    .line 668
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/l;)Lcom/google/d/a/a/n;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 741
    invoke-static {}, Lcom/google/d/a/a/l;->d()Lcom/google/d/a/a/l;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 796
    :goto_0
    return-object p0

    .line 742
    :cond_0
    iget-object v2, p1, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 743
    iget-object v2, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 744
    iget-object v2, p1, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    .line 745
    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/d/a/a/n;->a:I

    .line 752
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 753
    iget-object v2, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 754
    iget-object v2, p1, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    .line 755
    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/d/a/a/n;->a:I

    .line 762
    :cond_2
    :goto_2
    iget-object v2, p1, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 763
    iget-object v2, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 764
    iget-object v2, p1, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    .line 765
    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/a/a/n;->a:I

    .line 772
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 773
    iget-boolean v2, p1, Lcom/google/d/a/a/l;->e:Z

    iget v3, p0, Lcom/google/d/a/a/n;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/n;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/n;->e:Z

    .line 775
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 776
    iget-boolean v2, p1, Lcom/google/d/a/a/l;->f:Z

    iget v3, p0, Lcom/google/d/a/a/n;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/n;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/n;->f:Z

    .line 778
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 779
    iget-boolean v2, p1, Lcom/google/d/a/a/l;->g:Z

    iget v3, p0, Lcom/google/d/a/a/n;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/n;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/n;->g:Z

    .line 781
    :cond_6
    iget-object v2, p1, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 782
    iget-object v2, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 783
    iget-object v2, p1, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    .line 784
    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/d/a/a/n;->a:I

    .line 791
    :cond_7
    :goto_7
    iget v2, p1, Lcom/google/d/a/a/l;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_14

    :goto_8
    if-eqz v0, :cond_8

    .line 792
    iget-object v0, p0, Lcom/google/d/a/a/n;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 793
    iget v0, p0, Lcom/google/d/a/a/n;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/n;->a:I

    .line 795
    :cond_8
    iget-object v0, p1, Lcom/google/d/a/a/l;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 747
    :cond_9
    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/n;->a:I

    .line 748
    :cond_a
    iget-object v2, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 757
    :cond_b
    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/n;->a:I

    .line 758
    :cond_c
    iget-object v2, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 767
    :cond_d
    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v5, :cond_e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/n;->a:I

    .line 768
    :cond_e
    iget-object v2, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 772
    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 775
    goto/16 :goto_5

    :cond_11
    move v2, v1

    .line 778
    goto/16 :goto_6

    .line 786
    :cond_12
    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-eq v2, v3, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/n;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/n;->a:I

    .line 787
    :cond_13
    iget-object v2, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    :cond_14
    move v0, v1

    .line 791
    goto/16 :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 659
    new-instance v2, Lcom/google/d/a/a/l;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/l;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/n;->a:I

    iget v4, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/n;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/l;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/d/a/a/n;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/n;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/l;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/d/a/a/n;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/d/a/a/n;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/l;->d:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_7

    :goto_0
    iget-boolean v4, p0, Lcom/google/d/a/a/n;->e:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/l;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x2

    :cond_3
    iget-boolean v4, p0, Lcom/google/d/a/a/n;->f:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/l;->f:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x4

    :cond_4
    iget-boolean v4, p0, Lcom/google/d/a/a/n;->g:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/l;->g:Z

    iget v4, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/d/a/a/n;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/l;->h:Ljava/util/List;

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit8 v0, v0, 0x8

    :cond_6
    iget-object v3, v2, Lcom/google/d/a/a/l;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/n;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/n;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/l;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 659
    check-cast p1, Lcom/google/d/a/a/l;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/n;->a(Lcom/google/d/a/a/l;)Lcom/google/d/a/a/n;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 800
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 801
    iget-object v0, p0, Lcom/google/d/a/a/n;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/q;->d()Lcom/google/d/a/a/q;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/q;

    invoke-virtual {v0}, Lcom/google/d/a/a/q;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 818
    :cond_0
    :goto_1
    return v2

    .line 800
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 806
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 807
    iget-object v0, p0, Lcom/google/d/a/a/n;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/q;->d()Lcom/google/d/a/a/q;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/q;

    invoke-virtual {v0}, Lcom/google/d/a/a/q;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 806
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 812
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/n;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 813
    iget-object v0, p0, Lcom/google/d/a/a/n;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 818
    goto :goto_1

    :cond_5
    move v0, v2

    .line 812
    goto :goto_3
.end method
