.class public final Lcom/google/d/a/a/a/f;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/a/i;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/a/f;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/d/a/a/a/f;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:F

.field c:F

.field d:F

.field e:F

.field f:I

.field g:I

.field h:I

.field i:F

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2318
    new-instance v0, Lcom/google/d/a/a/a/g;

    invoke-direct {v0}, Lcom/google/d/a/a/a/g;-><init>()V

    sput-object v0, Lcom/google/d/a/a/a/f;->PARSER:Lcom/google/n/ax;

    .line 2595
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/a/f;->n:Lcom/google/n/aw;

    .line 3198
    new-instance v0, Lcom/google/d/a/a/a/f;

    invoke-direct {v0}, Lcom/google/d/a/a/a/f;-><init>()V

    sput-object v0, Lcom/google/d/a/a/a/f;->k:Lcom/google/d/a/a/a/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2219
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2497
    iput-byte v2, p0, Lcom/google/d/a/a/a/f;->l:B

    .line 2546
    iput v2, p0, Lcom/google/d/a/a/a/f;->m:I

    .line 2220
    iput v0, p0, Lcom/google/d/a/a/a/f;->b:F

    .line 2221
    iput v0, p0, Lcom/google/d/a/a/a/f;->c:F

    .line 2222
    iput v0, p0, Lcom/google/d/a/a/a/f;->d:F

    .line 2223
    iput v0, p0, Lcom/google/d/a/a/a/f;->e:F

    .line 2224
    iput v1, p0, Lcom/google/d/a/a/a/f;->f:I

    .line 2225
    iput v1, p0, Lcom/google/d/a/a/a/f;->g:I

    .line 2226
    iput v1, p0, Lcom/google/d/a/a/a/f;->h:I

    .line 2227
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/d/a/a/a/f;->i:F

    .line 2228
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    .line 2229
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/16 v7, 0x100

    .line 2235
    invoke-direct {p0}, Lcom/google/d/a/a/a/f;-><init>()V

    .line 2238
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 2241
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 2242
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 2243
    sparse-switch v4, :sswitch_data_0

    .line 2248
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 2250
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 2246
    goto :goto_0

    .line 2255
    :sswitch_1
    iget v4, p0, Lcom/google/d/a/a/a/f;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/a/f;->a:I

    .line 2256
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/a/f;->b:F
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2306
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 2307
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2312
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit16 v1, v1, 0x100

    if-ne v1, v7, :cond_1

    .line 2313
    iget-object v1, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    .line 2315
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/a/f;->au:Lcom/google/n/bn;

    throw v0

    .line 2260
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/d/a/a/a/f;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/a/f;->a:I

    .line 2261
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/a/f;->c:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2308
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 2309
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 2310
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2265
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/d/a/a/a/f;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/a/f;->a:I

    .line 2266
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/a/f;->d:F

    goto :goto_0

    .line 2312
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 2270
    :sswitch_4
    iget v4, p0, Lcom/google/d/a/a/a/f;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/a/a/a/f;->a:I

    .line 2271
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/a/f;->e:F

    goto/16 :goto_0

    .line 2275
    :sswitch_5
    iget v4, p0, Lcom/google/d/a/a/a/f;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/d/a/a/a/f;->a:I

    .line 2276
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/a/f;->f:I

    goto/16 :goto_0

    .line 2280
    :sswitch_6
    iget v4, p0, Lcom/google/d/a/a/a/f;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/d/a/a/a/f;->a:I

    .line 2281
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/a/f;->g:I

    goto/16 :goto_0

    .line 2285
    :sswitch_7
    iget v4, p0, Lcom/google/d/a/a/a/f;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/d/a/a/a/f;->a:I

    .line 2286
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/a/f;->h:I

    goto/16 :goto_0

    .line 2290
    :sswitch_8
    iget v4, p0, Lcom/google/d/a/a/a/f;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/d/a/a/a/f;->a:I

    .line 2291
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/a/f;->i:F

    goto/16 :goto_0

    .line 2295
    :sswitch_9
    and-int/lit16 v4, v0, 0x100

    if-eq v4, v7, :cond_2

    .line 2296
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    .line 2298
    or-int/lit16 v0, v0, 0x100

    .line 2300
    :cond_2
    iget-object v4, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 2301
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 2300
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 2312
    :cond_3
    and-int/lit16 v0, v0, 0x100

    if-ne v0, v7, :cond_4

    .line 2313
    iget-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    .line 2315
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/f;->au:Lcom/google/n/bn;

    .line 2316
    return-void

    .line 2243
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x45 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2217
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2497
    iput-byte v0, p0, Lcom/google/d/a/a/a/f;->l:B

    .line 2546
    iput v0, p0, Lcom/google/d/a/a/a/f;->m:I

    .line 2218
    return-void
.end method

.method public static d()Lcom/google/d/a/a/a/f;
    .locals 1

    .prologue
    .line 3201
    sget-object v0, Lcom/google/d/a/a/a/f;->k:Lcom/google/d/a/a/a/f;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/a/h;
    .locals 1

    .prologue
    .line 2657
    new-instance v0, Lcom/google/d/a/a/a/h;

    invoke-direct {v0}, Lcom/google/d/a/a/a/h;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/a/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2330
    sget-object v0, Lcom/google/d/a/a/a/f;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v4, 0x5

    .line 2515
    invoke-virtual {p0}, Lcom/google/d/a/a/a/f;->c()I

    .line 2516
    iget v1, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 2517
    iget v1, p0, Lcom/google/d/a/a/a/f;->b:F

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 2519
    :cond_0
    iget v1, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v5, :cond_1

    .line 2520
    iget v1, p0, Lcom/google/d/a/a/a/f;->c:F

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 2522
    :cond_1
    iget v1, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v3, :cond_2

    .line 2523
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/d/a/a/a/f;->d:F

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 2525
    :cond_2
    iget v1, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 2526
    iget v1, p0, Lcom/google/d/a/a/a/f;->e:F

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 2528
    :cond_3
    iget v1, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 2529
    iget v1, p0, Lcom/google/d/a/a/a/f;->f:I

    invoke-static {v4, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_8

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 2531
    :cond_4
    :goto_0
    iget v1, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 2532
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/d/a/a/a/f;->g:I

    invoke-static {v1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_9

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 2534
    :cond_5
    :goto_1
    iget v1, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    .line 2535
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/d/a/a/a/f;->h:I

    invoke-static {v1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_a

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 2537
    :cond_6
    :goto_2
    iget v1, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_7

    .line 2538
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/d/a/a/a/f;->i:F

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    :cond_7
    move v1, v0

    .line 2540
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 2541
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2540
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2529
    :cond_8
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 2532
    :cond_9
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 2535
    :cond_a
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 2543
    :cond_b
    iget-object v0, p0, Lcom/google/d/a/a/a/f;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2544
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2499
    iget-byte v0, p0, Lcom/google/d/a/a/a/f;->l:B

    .line 2500
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 2510
    :cond_0
    :goto_0
    return v2

    .line 2501
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 2503
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2504
    iget-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/a/j;->d()Lcom/google/d/a/a/a/j;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/a/j;

    invoke-virtual {v0}, Lcom/google/d/a/a/a/j;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2505
    iput-byte v2, p0, Lcom/google/d/a/a/a/f;->l:B

    goto :goto_0

    .line 2503
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2509
    :cond_3
    iput-byte v3, p0, Lcom/google/d/a/a/a/f;->l:B

    move v2, v3

    .line 2510
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 2548
    iget v0, p0, Lcom/google/d/a/a/a/f;->m:I

    .line 2549
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2590
    :goto_0
    return v0

    .line 2552
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 2553
    iget v0, p0, Lcom/google/d/a/a/a/f;->b:F

    .line 2554
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 2556
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 2557
    iget v2, p0, Lcom/google/d/a/a/a/f;->c:F

    .line 2558
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 2560
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 2561
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/d/a/a/a/f;->d:F

    .line 2562
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 2564
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_3

    .line 2565
    iget v2, p0, Lcom/google/d/a/a/a/f;->e:F

    .line 2566
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 2568
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_4

    .line 2569
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/d/a/a/a/f;->f:I

    .line 2570
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 2572
    :cond_4
    iget v2, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_5

    .line 2573
    const/4 v2, 0x6

    iget v4, p0, Lcom/google/d/a/a/a/f;->g:I

    .line 2574
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 2576
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_7

    .line 2577
    const/4 v2, 0x7

    iget v4, p0, Lcom/google/d/a/a/a/f;->h:I

    .line 2578
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_6
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2580
    :cond_7
    iget v2, p0, Lcom/google/d/a/a/a/f;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8

    .line 2581
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/d/a/a/a/f;->i:F

    .line 2582
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    :cond_8
    move v2, v1

    move v3, v0

    .line 2584
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 2585
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/d/a/a/a/f;->j:Ljava/util/List;

    .line 2586
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2584
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_9
    move v2, v3

    .line 2570
    goto :goto_2

    :cond_a
    move v2, v3

    .line 2574
    goto :goto_3

    .line 2588
    :cond_b
    iget-object v0, p0, Lcom/google/d/a/a/a/f;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2589
    iput v0, p0, Lcom/google/d/a/a/a/f;->m:I

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2211
    invoke-static {}, Lcom/google/d/a/a/a/f;->newBuilder()Lcom/google/d/a/a/a/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/a/h;->a(Lcom/google/d/a/a/a/f;)Lcom/google/d/a/a/a/h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2211
    invoke-static {}, Lcom/google/d/a/a/a/f;->newBuilder()Lcom/google/d/a/a/a/h;

    move-result-object v0

    return-object v0
.end method
