.class public final Lcom/google/d/a/a/ac;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ah;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ac;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/d/a/a/ac;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2939
    new-instance v0, Lcom/google/d/a/a/ad;

    invoke-direct {v0}, Lcom/google/d/a/a/ad;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ac;->PARSER:Lcom/google/n/ax;

    .line 3193
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/ac;->h:Lcom/google/n/aw;

    .line 3539
    new-instance v0, Lcom/google/d/a/a/ac;

    invoke-direct {v0}, Lcom/google/d/a/a/ac;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ac;->e:Lcom/google/d/a/a/ac;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2876
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3131
    iput-byte v0, p0, Lcom/google/d/a/a/ac;->f:B

    .line 3168
    iput v0, p0, Lcom/google/d/a/a/ac;->g:I

    .line 2877
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ac;->b:Ljava/lang/Object;

    .line 2878
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ac;->c:Ljava/lang/Object;

    .line 2879
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/ac;->d:I

    .line 2880
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2886
    invoke-direct {p0}, Lcom/google/d/a/a/ac;-><init>()V

    .line 2887
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2891
    const/4 v0, 0x0

    .line 2892
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 2893
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2894
    sparse-switch v3, :sswitch_data_0

    .line 2899
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2901
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2897
    goto :goto_0

    .line 2906
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2907
    iget v4, p0, Lcom/google/d/a/a/ac;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/ac;->a:I

    .line 2908
    iput-object v3, p0, Lcom/google/d/a/a/ac;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2930
    :catch_0
    move-exception v0

    .line 2931
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2936
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ac;->au:Lcom/google/n/bn;

    throw v0

    .line 2912
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2913
    iget v4, p0, Lcom/google/d/a/a/ac;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/ac;->a:I

    .line 2914
    iput-object v3, p0, Lcom/google/d/a/a/ac;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2932
    :catch_1
    move-exception v0

    .line 2933
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2934
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2918
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 2919
    invoke-static {v3}, Lcom/google/d/a/a/af;->a(I)Lcom/google/d/a/a/af;

    move-result-object v4

    .line 2920
    if-nez v4, :cond_1

    .line 2921
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 2923
    :cond_1
    iget v4, p0, Lcom/google/d/a/a/ac;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/ac;->a:I

    .line 2924
    iput v3, p0, Lcom/google/d/a/a/ac;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2936
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ac;->au:Lcom/google/n/bn;

    .line 2937
    return-void

    .line 2894
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2874
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3131
    iput-byte v0, p0, Lcom/google/d/a/a/ac;->f:B

    .line 3168
    iput v0, p0, Lcom/google/d/a/a/ac;->g:I

    .line 2875
    return-void
.end method

.method public static d()Lcom/google/d/a/a/ac;
    .locals 1

    .prologue
    .line 3542
    sget-object v0, Lcom/google/d/a/a/ac;->e:Lcom/google/d/a/a/ac;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ae;
    .locals 1

    .prologue
    .line 3255
    new-instance v0, Lcom/google/d/a/a/ae;

    invoke-direct {v0}, Lcom/google/d/a/a/ae;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2951
    sget-object v0, Lcom/google/d/a/a/ac;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 3155
    invoke-virtual {p0}, Lcom/google/d/a/a/ac;->c()I

    .line 3156
    iget v0, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 3157
    iget-object v0, p0, Lcom/google/d/a/a/ac;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ac;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3159
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3160
    iget-object v0, p0, Lcom/google/d/a/a/ac;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ac;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3162
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 3163
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/ac;->d:I

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 3165
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/ac;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3166
    return-void

    .line 3157
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 3160
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 3163
    :cond_5
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3133
    iget-byte v2, p0, Lcom/google/d/a/a/ac;->f:B

    .line 3134
    if-ne v2, v0, :cond_0

    .line 3150
    :goto_0
    return v0

    .line 3135
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 3137
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 3138
    iput-byte v1, p0, Lcom/google/d/a/a/ac;->f:B

    move v0, v1

    .line 3139
    goto :goto_0

    :cond_2
    move v2, v1

    .line 3137
    goto :goto_1

    .line 3141
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 3142
    iput-byte v1, p0, Lcom/google/d/a/a/ac;->f:B

    move v0, v1

    .line 3143
    goto :goto_0

    :cond_4
    move v2, v1

    .line 3141
    goto :goto_2

    .line 3145
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-nez v2, :cond_7

    .line 3146
    iput-byte v1, p0, Lcom/google/d/a/a/ac;->f:B

    move v0, v1

    .line 3147
    goto :goto_0

    :cond_6
    move v2, v1

    .line 3145
    goto :goto_3

    .line 3149
    :cond_7
    iput-byte v0, p0, Lcom/google/d/a/a/ac;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3170
    iget v0, p0, Lcom/google/d/a/a/ac;->g:I

    .line 3171
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3188
    :goto_0
    return v0

    .line 3174
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 3176
    iget-object v0, p0, Lcom/google/d/a/a/ac;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ac;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 3178
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 3180
    iget-object v0, p0, Lcom/google/d/a/a/ac;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ac;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 3182
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 3183
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/d/a/a/ac;->d:I

    .line 3184
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_5

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 3186
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/ac;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 3187
    iput v0, p0, Lcom/google/d/a/a/ac;->g:I

    goto :goto_0

    .line 3176
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 3180
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 3184
    :cond_5
    const/16 v0, 0xa

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2868
    invoke-static {}, Lcom/google/d/a/a/ac;->newBuilder()Lcom/google/d/a/a/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ae;->a(Lcom/google/d/a/a/ac;)Lcom/google/d/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2868
    invoke-static {}, Lcom/google/d/a/a/ac;->newBuilder()Lcom/google/d/a/a/ae;

    move-result-object v0

    return-object v0
.end method
