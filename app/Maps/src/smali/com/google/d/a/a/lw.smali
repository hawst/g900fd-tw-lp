.class public final Lcom/google/d/a/a/lw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/lz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/lu;",
        "Lcom/google/d/a/a/lw;",
        ">;",
        "Lcom/google/d/a/a/lz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 7480
    sget-object v0, Lcom/google/d/a/a/lu;->e:Lcom/google/d/a/a/lu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 7549
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lw;->b:Lcom/google/n/ao;

    .line 7608
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/lw;->c:I

    .line 7644
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/lw;->d:I

    .line 7481
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/lu;)Lcom/google/d/a/a/lw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 7520
    invoke-static {}, Lcom/google/d/a/a/lu;->d()Lcom/google/d/a/a/lu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 7532
    :goto_0
    return-object p0

    .line 7521
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 7522
    iget-object v2, p0, Lcom/google/d/a/a/lw;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 7523
    iget v2, p0, Lcom/google/d/a/a/lw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/lw;->a:I

    .line 7525
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 7526
    iget v2, p1, Lcom/google/d/a/a/lu;->c:I

    invoke-static {v2}, Lcom/google/d/a/a/lx;->a(I)Lcom/google/d/a/a/lx;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/d/a/a/lx;->c:Lcom/google/d/a/a/lx;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 7521
    goto :goto_1

    :cond_4
    move v2, v1

    .line 7525
    goto :goto_2

    .line 7526
    :cond_5
    iget v3, p0, Lcom/google/d/a/a/lw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/lw;->a:I

    iget v2, v2, Lcom/google/d/a/a/lx;->e:I

    iput v2, p0, Lcom/google/d/a/a/lw;->c:I

    .line 7528
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/lu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_a

    .line 7529
    iget v0, p1, Lcom/google/d/a/a/lu;->d:I

    invoke-static {v0}, Lcom/google/d/a/a/ln;->a(I)Lcom/google/d/a/a/ln;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/d/a/a/ln;->a:Lcom/google/d/a/a/ln;

    :cond_7
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v0, v1

    .line 7528
    goto :goto_3

    .line 7529
    :cond_9
    iget v1, p0, Lcom/google/d/a/a/lw;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/d/a/a/lw;->a:I

    iget v0, v0, Lcom/google/d/a/a/ln;->j:I

    iput v0, p0, Lcom/google/d/a/a/lw;->d:I

    .line 7531
    :cond_a
    iget-object v0, p1, Lcom/google/d/a/a/lu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7472
    new-instance v2, Lcom/google/d/a/a/lu;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/lu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/lw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/lu;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/lw;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/lw;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/d/a/a/lw;->c:I

    iput v1, v2, Lcom/google/d/a/a/lu;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/d/a/a/lw;->d:I

    iput v1, v2, Lcom/google/d/a/a/lu;->d:I

    iput v0, v2, Lcom/google/d/a/a/lu;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 7472
    check-cast p1, Lcom/google/d/a/a/lu;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/lw;->a(Lcom/google/d/a/a/lu;)Lcom/google/d/a/a/lw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 7536
    iget v0, p0, Lcom/google/d/a/a/lw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 7544
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 7536
    goto :goto_0

    .line 7540
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/lw;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 7542
    goto :goto_1

    :cond_2
    move v0, v2

    .line 7544
    goto :goto_1
.end method
