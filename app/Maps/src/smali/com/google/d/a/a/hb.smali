.class public final Lcom/google/d/a/a/hb;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/he;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hb;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/d/a/a/hb;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:I

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1478
    new-instance v0, Lcom/google/d/a/a/hc;

    invoke-direct {v0}, Lcom/google/d/a/a/hc;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hb;->PARSER:Lcom/google/n/ax;

    .line 1652
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/hb;->i:Lcom/google/n/aw;

    .line 2086
    new-instance v0, Lcom/google/d/a/a/hb;

    invoke-direct {v0}, Lcom/google/d/a/a/hb;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hb;->f:Lcom/google/d/a/a/hb;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1409
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1495
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hb;->b:Lcom/google/n/ao;

    .line 1583
    iput-byte v2, p0, Lcom/google/d/a/a/hb;->g:B

    .line 1623
    iput v2, p0, Lcom/google/d/a/a/hb;->h:I

    .line 1410
    iget-object v0, p0, Lcom/google/d/a/a/hb;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1411
    iput v3, p0, Lcom/google/d/a/a/hb;->c:I

    .line 1412
    iput v3, p0, Lcom/google/d/a/a/hb;->d:I

    .line 1413
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    .line 1414
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v0, 0x0

    .line 1420
    invoke-direct {p0}, Lcom/google/d/a/a/hb;-><init>()V

    .line 1423
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 1426
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 1427
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 1428
    sparse-switch v4, :sswitch_data_0

    .line 1433
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 1435
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 1431
    goto :goto_0

    .line 1440
    :sswitch_1
    iget-object v4, p0, Lcom/google/d/a/a/hb;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 1441
    iget v4, p0, Lcom/google/d/a/a/hb;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/hb;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 1466
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 1467
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1472
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_1

    .line 1473
    iget-object v1, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    .line 1475
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/hb;->au:Lcom/google/n/bn;

    throw v0

    .line 1445
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/d/a/a/hb;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/hb;->a:I

    .line 1446
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/hb;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 1468
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 1469
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1470
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1450
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/d/a/a/hb;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/hb;->a:I

    .line 1451
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/hb;->d:I

    goto :goto_0

    .line 1472
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 1455
    :sswitch_4
    and-int/lit8 v4, v0, 0x8

    if-eq v4, v7, :cond_2

    .line 1456
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    .line 1458
    or-int/lit8 v0, v0, 0x8

    .line 1460
    :cond_2
    iget-object v4, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 1461
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 1460
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 1472
    :cond_3
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_4

    .line 1473
    iget-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    .line 1475
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hb;->au:Lcom/google/n/bn;

    .line 1476
    return-void

    .line 1428
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1407
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1495
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hb;->b:Lcom/google/n/ao;

    .line 1583
    iput-byte v1, p0, Lcom/google/d/a/a/hb;->g:B

    .line 1623
    iput v1, p0, Lcom/google/d/a/a/hb;->h:I

    .line 1408
    return-void
.end method

.method public static d()Lcom/google/d/a/a/hb;
    .locals 1

    .prologue
    .line 2089
    sget-object v0, Lcom/google/d/a/a/hb;->f:Lcom/google/d/a/a/hb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/hd;
    .locals 1

    .prologue
    .line 1714
    new-instance v0, Lcom/google/d/a/a/hd;

    invoke-direct {v0}, Lcom/google/d/a/a/hd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1490
    sget-object v0, Lcom/google/d/a/a/hb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x2

    .line 1607
    invoke-virtual {p0}, Lcom/google/d/a/a/hb;->c()I

    .line 1608
    iget v1, p0, Lcom/google/d/a/a/hb;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 1609
    iget-object v1, p0, Lcom/google/d/a/a/hb;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1611
    :cond_0
    iget v1, p0, Lcom/google/d/a/a/hb;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 1612
    iget v1, p0, Lcom/google/d/a/a/hb;->c:I

    invoke-static {v4, v0}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_3

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1614
    :cond_1
    :goto_0
    iget v1, p0, Lcom/google/d/a/a/hb;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 1615
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/d/a/a/hb;->d:I

    invoke-static {v1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_4

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    :cond_2
    :goto_1
    move v1, v0

    .line 1617
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1618
    iget-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1617
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1612
    :cond_3
    int-to-long v2, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 1615
    :cond_4
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 1620
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/hb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1621
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1585
    iget-byte v0, p0, Lcom/google/d/a/a/hb;->g:B

    .line 1586
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1602
    :cond_0
    :goto_0
    return v2

    .line 1587
    :cond_1
    if-eqz v0, :cond_0

    .line 1589
    iget v0, p0, Lcom/google/d/a/a/hb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 1590
    iget-object v0, p0, Lcom/google/d/a/a/hb;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1591
    iput-byte v2, p0, Lcom/google/d/a/a/hb;->g:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1589
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1595
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1596
    iget-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1597
    iput-byte v2, p0, Lcom/google/d/a/a/hb;->g:B

    goto :goto_0

    .line 1595
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1601
    :cond_5
    iput-byte v3, p0, Lcom/google/d/a/a/hb;->g:B

    move v2, v3

    .line 1602
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v3, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1625
    iget v0, p0, Lcom/google/d/a/a/hb;->h:I

    .line 1626
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1647
    :goto_0
    return v0

    .line 1629
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/hb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 1630
    iget-object v0, p0, Lcom/google/d/a/a/hb;->b:Lcom/google/n/ao;

    .line 1631
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1633
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/hb;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 1634
    iget v2, p0, Lcom/google/d/a/a/hb;->c:I

    .line 1635
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1637
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/hb;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_3

    .line 1638
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/d/a/a/hb;->d:I

    .line 1639
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v0

    .line 1641
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1642
    iget-object v0, p0, Lcom/google/d/a/a/hb;->e:Ljava/util/List;

    .line 1643
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1641
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    move v2, v3

    .line 1635
    goto :goto_2

    .line 1645
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/hb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1646
    iput v0, p0, Lcom/google/d/a/a/hb;->h:I

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1401
    invoke-static {}, Lcom/google/d/a/a/hb;->newBuilder()Lcom/google/d/a/a/hd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/hd;->a(Lcom/google/d/a/a/hb;)Lcom/google/d/a/a/hd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1401
    invoke-static {}, Lcom/google/d/a/a/hb;->newBuilder()Lcom/google/d/a/a/hd;

    move-result-object v0

    return-object v0
.end method
