.class public final enum Lcom/google/d/a/a/ql;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ql;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ql;

.field public static final enum b:Lcom/google/d/a/a/ql;

.field public static final enum c:Lcom/google/d/a/a/ql;

.field public static final enum d:Lcom/google/d/a/a/ql;

.field public static final enum e:Lcom/google/d/a/a/ql;

.field public static final enum f:Lcom/google/d/a/a/ql;

.field public static final enum g:Lcom/google/d/a/a/ql;

.field public static final enum h:Lcom/google/d/a/a/ql;

.field public static final enum i:Lcom/google/d/a/a/ql;

.field public static final enum j:Lcom/google/d/a/a/ql;

.field public static final enum k:Lcom/google/d/a/a/ql;

.field public static final enum l:Lcom/google/d/a/a/ql;

.field public static final enum m:Lcom/google/d/a/a/ql;

.field public static final enum n:Lcom/google/d/a/a/ql;

.field public static final enum o:Lcom/google/d/a/a/ql;

.field public static final enum p:Lcom/google/d/a/a/ql;

.field public static final enum q:Lcom/google/d/a/a/ql;

.field public static final enum r:Lcom/google/d/a/a/ql;

.field public static final enum s:Lcom/google/d/a/a/ql;

.field public static final enum t:Lcom/google/d/a/a/ql;

.field private static final synthetic v:[Lcom/google/d/a/a/ql;


# instance fields
.field final u:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 193
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_RAIL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->a:Lcom/google/d/a/a/ql;

    .line 197
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_METRO_RAIL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->b:Lcom/google/d/a/a/ql;

    .line 201
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_SUBWAY"

    const/16 v2, 0x111

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->c:Lcom/google/d/a/a/ql;

    .line 205
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_TRAM"

    const/16 v2, 0x112

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->d:Lcom/google/d/a/a/ql;

    .line 209
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_MONORAIL"

    const/16 v2, 0x113

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->e:Lcom/google/d/a/a/ql;

    .line 213
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_HEAVY_RAIL"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->f:Lcom/google/d/a/a/ql;

    .line 217
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_COMMUTER_TRAIN"

    const/4 v2, 0x6

    const/16 v3, 0x121

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->g:Lcom/google/d/a/a/ql;

    .line 221
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_HIGH_SPEED_TRAIN"

    const/4 v2, 0x7

    const/16 v3, 0x122

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->h:Lcom/google/d/a/a/ql;

    .line 225
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_LONG_DISTANCE_TRAIN"

    const/16 v2, 0x8

    const/16 v3, 0x123

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->i:Lcom/google/d/a/a/ql;

    .line 229
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_BUS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v5}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->j:Lcom/google/d/a/a/ql;

    .line 233
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_INTERCITY_BUS"

    const/16 v2, 0xa

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->k:Lcom/google/d/a/a/ql;

    .line 237
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_TROLLEYBUS"

    const/16 v2, 0xb

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->l:Lcom/google/d/a/a/ql;

    .line 241
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_SHARE_TAXI"

    const/16 v2, 0xc

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->m:Lcom/google/d/a/a/ql;

    .line 245
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_FERRY"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v6}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->n:Lcom/google/d/a/a/ql;

    .line 249
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_CABLE_CAR"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v7}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->o:Lcom/google/d/a/a/ql;

    .line 253
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_GONDOLA_LIFT"

    const/16 v2, 0xf

    const/16 v3, 0x41

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->p:Lcom/google/d/a/a/ql;

    .line 257
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_FUNICULAR"

    const/16 v2, 0x10

    const/16 v3, 0x42

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->q:Lcom/google/d/a/a/ql;

    .line 261
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_SPECIAL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v8}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->r:Lcom/google/d/a/a/ql;

    .line 265
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_HORSE_CARRIAGE"

    const/16 v2, 0x12

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->s:Lcom/google/d/a/a/ql;

    .line 269
    new-instance v0, Lcom/google/d/a/a/ql;

    const-string v1, "VEHICLE_TYPE_AIRPLANE"

    const/16 v2, 0x13

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ql;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ql;->t:Lcom/google/d/a/a/ql;

    .line 188
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/google/d/a/a/ql;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/d/a/a/ql;->a:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/ql;->b:Lcom/google/d/a/a/ql;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/ql;->c:Lcom/google/d/a/a/ql;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/ql;->d:Lcom/google/d/a/a/ql;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/ql;->e:Lcom/google/d/a/a/ql;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/ql;->f:Lcom/google/d/a/a/ql;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/ql;->g:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/ql;->h:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/ql;->i:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/ql;->j:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/ql;->k:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/ql;->l:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/ql;->m:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/d/a/a/ql;->n:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/d/a/a/ql;->o:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/d/a/a/ql;->p:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/d/a/a/ql;->q:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/d/a/a/ql;->r:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/d/a/a/ql;->s:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/d/a/a/ql;->t:Lcom/google/d/a/a/ql;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/ql;->v:[Lcom/google/d/a/a/ql;

    .line 389
    new-instance v0, Lcom/google/d/a/a/qm;

    invoke-direct {v0}, Lcom/google/d/a/a/qm;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 398
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 399
    iput p3, p0, Lcom/google/d/a/a/ql;->u:I

    .line 400
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ql;
    .locals 1

    .prologue
    .line 359
    sparse-switch p0, :sswitch_data_0

    .line 380
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 360
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/ql;->a:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 361
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/ql;->b:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 362
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/ql;->c:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 363
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/ql;->d:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 364
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/ql;->e:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 365
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/ql;->f:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 366
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/ql;->g:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 367
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/ql;->h:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 368
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/ql;->i:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 369
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/ql;->j:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 370
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/ql;->k:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 371
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/ql;->l:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 372
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/ql;->m:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 373
    :sswitch_d
    sget-object v0, Lcom/google/d/a/a/ql;->n:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 374
    :sswitch_e
    sget-object v0, Lcom/google/d/a/a/ql;->o:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 375
    :sswitch_f
    sget-object v0, Lcom/google/d/a/a/ql;->p:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 376
    :sswitch_10
    sget-object v0, Lcom/google/d/a/a/ql;->q:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 377
    :sswitch_11
    sget-object v0, Lcom/google/d/a/a/ql;->r:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 378
    :sswitch_12
    sget-object v0, Lcom/google/d/a/a/ql;->s:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 379
    :sswitch_13
    sget-object v0, Lcom/google/d/a/a/ql;->t:Lcom/google/d/a/a/ql;

    goto :goto_0

    .line 359
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_9
        0x3 -> :sswitch_d
        0x4 -> :sswitch_e
        0x5 -> :sswitch_11
        0x6 -> :sswitch_13
        0x11 -> :sswitch_1
        0x12 -> :sswitch_5
        0x21 -> :sswitch_a
        0x22 -> :sswitch_b
        0x23 -> :sswitch_c
        0x41 -> :sswitch_f
        0x42 -> :sswitch_10
        0x51 -> :sswitch_12
        0x111 -> :sswitch_2
        0x112 -> :sswitch_3
        0x113 -> :sswitch_4
        0x121 -> :sswitch_6
        0x122 -> :sswitch_7
        0x123 -> :sswitch_8
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ql;
    .locals 1

    .prologue
    .line 188
    const-class v0, Lcom/google/d/a/a/ql;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ql;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ql;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/google/d/a/a/ql;->v:[Lcom/google/d/a/a/ql;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ql;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ql;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 355
    iget v0, p0, Lcom/google/d/a/a/ql;->u:I

    return v0
.end method
