.class public final Lcom/google/d/a/a/oi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ol;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/oi;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/a/a/oi;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1057
    new-instance v0, Lcom/google/d/a/a/oj;

    invoke-direct {v0}, Lcom/google/d/a/a/oj;-><init>()V

    sput-object v0, Lcom/google/d/a/a/oi;->PARSER:Lcom/google/n/ax;

    .line 1247
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/oi;->g:Lcom/google/n/aw;

    .line 1763
    new-instance v0, Lcom/google/d/a/a/oi;

    invoke-direct {v0}, Lcom/google/d/a/a/oi;-><init>()V

    sput-object v0, Lcom/google/d/a/a/oi;->d:Lcom/google/d/a/a/oi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 967
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1180
    iput-byte v0, p0, Lcom/google/d/a/a/oi;->e:B

    .line 1217
    iput v0, p0, Lcom/google/d/a/a/oi;->f:I

    .line 968
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    .line 969
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    .line 970
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    .line 971
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v9, 0x2

    const/4 v8, 0x4

    const/4 v4, 0x1

    .line 977
    invoke-direct {p0}, Lcom/google/d/a/a/oi;-><init>()V

    .line 980
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 983
    :cond_0
    :goto_0
    if-nez v3, :cond_8

    .line 984
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 985
    sparse-switch v1, :sswitch_data_0

    .line 990
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 992
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 988
    goto :goto_0

    .line 997
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v4, :cond_e

    .line 998
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1000
    or-int/lit8 v1, v0, 0x1

    .line 1002
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 1003
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 1002
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 1004
    goto :goto_0

    .line 1007
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v9, :cond_d

    .line 1008
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1010
    or-int/lit8 v1, v0, 0x2

    .line 1012
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 1013
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 1012
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 1014
    goto :goto_0

    .line 1017
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_c

    .line 1018
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1019
    or-int/lit8 v1, v0, 0x4

    .line 1021
    :goto_3
    :try_start_5
    iget-object v0, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 1022
    goto :goto_0

    .line 1025
    :sswitch_4
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 1026
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 1027
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_1

    iget v1, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v1, v7, :cond_5

    move v1, v2

    :goto_4
    if-lez v1, :cond_1

    .line 1028
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    .line 1029
    or-int/lit8 v0, v0, 0x4

    .line 1031
    :cond_1
    :goto_5
    iget v1, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v1, v7, :cond_6

    move v1, v2

    :goto_6
    if-lez v1, :cond_7

    .line 1032
    iget-object v1, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_5

    .line 1039
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 1040
    :goto_7
    :try_start_7
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1045
    :catchall_0
    move-exception v0

    :goto_8
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_2

    .line 1046
    iget-object v2, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    .line 1048
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v9, :cond_3

    .line 1049
    iget-object v2, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    .line 1051
    :cond_3
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_4

    .line 1052
    iget-object v1, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    .line 1054
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/oi;->au:Lcom/google/n/bn;

    throw v0

    .line 1027
    :cond_5
    :try_start_8
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_4

    .line 1031
    :cond_6
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_6

    .line 1034
    :cond_7
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 1041
    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 1042
    :goto_9
    :try_start_9
    new-instance v2, Lcom/google/n/ak;

    .line 1043
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1045
    :cond_8
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v4, :cond_9

    .line 1046
    iget-object v1, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    .line 1048
    :cond_9
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v9, :cond_a

    .line 1049
    iget-object v1, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    .line 1051
    :cond_a
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_b

    .line 1052
    iget-object v0, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    .line 1054
    :cond_b
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/oi;->au:Lcom/google/n/bn;

    .line 1055
    return-void

    .line 1045
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_8

    .line 1041
    :catch_2
    move-exception v0

    goto :goto_9

    .line 1039
    :catch_3
    move-exception v0

    goto/16 :goto_7

    :cond_c
    move v1, v0

    goto/16 :goto_3

    :cond_d
    move v1, v0

    goto/16 :goto_2

    :cond_e
    move v1, v0

    goto/16 :goto_1

    .line 985
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x32 -> :sswitch_1
        0x3a -> :sswitch_2
        0x40 -> :sswitch_3
        0x42 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 965
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1180
    iput-byte v0, p0, Lcom/google/d/a/a/oi;->e:B

    .line 1217
    iput v0, p0, Lcom/google/d/a/a/oi;->f:I

    .line 966
    return-void
.end method

.method public static d()Lcom/google/d/a/a/oi;
    .locals 1

    .prologue
    .line 1766
    sget-object v0, Lcom/google/d/a/a/oi;->d:Lcom/google/d/a/a/oi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ok;
    .locals 1

    .prologue
    .line 1309
    new-instance v0, Lcom/google/d/a/a/ok;

    invoke-direct {v0}, Lcom/google/d/a/a/ok;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/oi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1069
    sget-object v0, Lcom/google/d/a/a/oi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 1204
    invoke-virtual {p0}, Lcom/google/d/a/a/oi;->c()I

    move v1, v2

    .line 1205
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1206
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1205
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 1208
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1209
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1208
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 1211
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1212
    const/16 v3, 0x8

    iget-object v0, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1211
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1212
    :cond_2
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 1214
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/oi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1215
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1182
    iget-byte v0, p0, Lcom/google/d/a/a/oi;->e:B

    .line 1183
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1199
    :cond_0
    :goto_0
    return v2

    .line 1184
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1186
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1187
    iget-object v0, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/om;->d()Lcom/google/d/a/a/om;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/om;

    invoke-virtual {v0}, Lcom/google/d/a/a/om;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1188
    iput-byte v2, p0, Lcom/google/d/a/a/oi;->e:B

    goto :goto_0

    .line 1186
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 1192
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1193
    iget-object v0, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/oe;->d()Lcom/google/d/a/a/oe;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/oe;

    invoke-virtual {v0}, Lcom/google/d/a/a/oe;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1194
    iput-byte v2, p0, Lcom/google/d/a/a/oi;->e:B

    goto :goto_0

    .line 1192
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1198
    :cond_5
    iput-byte v3, p0, Lcom/google/d/a/a/oi;->e:B

    move v2, v3

    .line 1199
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1219
    iget v0, p0, Lcom/google/d/a/a/oi;->f:I

    .line 1220
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1242
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1223
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1224
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/oi;->a:Ljava/util/List;

    .line 1225
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1223
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 1227
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1228
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/d/a/a/oi;->b:Ljava/util/List;

    .line 1229
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1227
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v1, v2

    .line 1233
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1234
    iget-object v0, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    .line 1235
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    .line 1233
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_3

    .line 1235
    :cond_3
    const/16 v0, 0xa

    goto :goto_4

    .line 1237
    :cond_4
    add-int v0, v3, v1

    .line 1238
    iget-object v1, p0, Lcom/google/d/a/a/oi;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1240
    iget-object v1, p0, Lcom/google/d/a/a/oi;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1241
    iput v0, p0, Lcom/google/d/a/a/oi;->f:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 959
    invoke-static {}, Lcom/google/d/a/a/oi;->newBuilder()Lcom/google/d/a/a/ok;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ok;->a(Lcom/google/d/a/a/oi;)Lcom/google/d/a/a/ok;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 959
    invoke-static {}, Lcom/google/d/a/a/oi;->newBuilder()Lcom/google/d/a/a/ok;

    move-result-object v0

    return-object v0
.end method
