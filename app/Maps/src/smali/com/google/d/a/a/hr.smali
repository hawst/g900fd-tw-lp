.class public final Lcom/google/d/a/a/hr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/hs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/hp;",
        "Lcom/google/d/a/a/hr;",
        ">;",
        "Lcom/google/d/a/a/hs;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 352
    sget-object v0, Lcom/google/d/a/a/hp;->f:Lcom/google/d/a/a/hp;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 503
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hr;->d:Lcom/google/n/ao;

    .line 562
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hr;->e:Lcom/google/n/ao;

    .line 353
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 400
    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 416
    :goto_0
    return-object p0

    .line 401
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 402
    iget v2, p1, Lcom/google/d/a/a/hp;->b:I

    iget v3, p0, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/hr;->a:I

    iput v2, p0, Lcom/google/d/a/a/hr;->b:I

    .line 404
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 405
    iget v2, p1, Lcom/google/d/a/a/hp;->c:I

    iget v3, p0, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/hr;->a:I

    iput v2, p0, Lcom/google/d/a/a/hr;->c:I

    .line 407
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 408
    iget-object v2, p0, Lcom/google/d/a/a/hr;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/hp;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 409
    iget v2, p0, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/hr;->a:I

    .line 411
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 412
    iget-object v0, p0, Lcom/google/d/a/a/hr;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 413
    iget v0, p0, Lcom/google/d/a/a/hr;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/hr;->a:I

    .line 415
    :cond_4
    iget-object v0, p1, Lcom/google/d/a/a/hp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 401
    goto :goto_1

    :cond_6
    move v2, v1

    .line 404
    goto :goto_2

    :cond_7
    move v2, v1

    .line 407
    goto :goto_3

    :cond_8
    move v0, v1

    .line 411
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/google/d/a/a/hr;->c()Lcom/google/d/a/a/hp;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 344
    check-cast p1, Lcom/google/d/a/a/hp;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 420
    iget v0, p0, Lcom/google/d/a/a/hr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 434
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 420
    goto :goto_0

    .line 424
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/hr;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-nez v0, :cond_3

    move v0, v1

    .line 426
    goto :goto_1

    :cond_2
    move v0, v1

    .line 424
    goto :goto_2

    .line 428
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/hr;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 429
    iget-object v0, p0, Lcom/google/d/a/a/hr;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 431
    goto :goto_1

    :cond_4
    move v0, v1

    .line 428
    goto :goto_3

    :cond_5
    move v0, v2

    .line 434
    goto :goto_1
.end method

.method public final c()Lcom/google/d/a/a/hp;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 372
    new-instance v2, Lcom/google/d/a/a/hp;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/hp;-><init>(Lcom/google/n/v;)V

    .line 373
    iget v3, p0, Lcom/google/d/a/a/hr;->a:I

    .line 375
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 378
    :goto_0
    iget v4, p0, Lcom/google/d/a/a/hr;->b:I

    iput v4, v2, Lcom/google/d/a/a/hp;->b:I

    .line 379
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 380
    or-int/lit8 v0, v0, 0x2

    .line 382
    :cond_0
    iget v4, p0, Lcom/google/d/a/a/hr;->c:I

    iput v4, v2, Lcom/google/d/a/a/hp;->c:I

    .line 383
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 384
    or-int/lit8 v0, v0, 0x4

    .line 386
    :cond_1
    iget-object v4, v2, Lcom/google/d/a/a/hp;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/hr;->d:Lcom/google/n/ao;

    .line 387
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/hr;->d:Lcom/google/n/ao;

    .line 388
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 386
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 389
    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 390
    or-int/lit8 v0, v0, 0x8

    .line 392
    :cond_2
    iget-object v3, v2, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/hr;->e:Lcom/google/n/ao;

    .line 393
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/hr;->e:Lcom/google/n/ao;

    .line 394
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 392
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 395
    iput v0, v2, Lcom/google/d/a/a/hp;->a:I

    .line 396
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
