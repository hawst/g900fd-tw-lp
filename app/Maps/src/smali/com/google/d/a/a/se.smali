.class public final Lcom/google/d/a/a/se;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/sf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/sc;",
        "Lcom/google/d/a/a/se;",
        ">;",
        "Lcom/google/d/a/a/sf;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:J

.field private e:Ljava/lang/Object;

.field private f:I

.field private g:Z

.field private h:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 551
    sget-object v0, Lcom/google/d/a/a/sc;->i:Lcom/google/d/a/a/sc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 658
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/se;->b:Ljava/lang/Object;

    .line 734
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/se;->c:Ljava/lang/Object;

    .line 842
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/se;->e:Ljava/lang/Object;

    .line 982
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/se;->h:Lcom/google/n/ao;

    .line 552
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/sc;)Lcom/google/d/a/a/se;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 615
    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 645
    :goto_0
    return-object p0

    .line 616
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 617
    iget v2, p0, Lcom/google/d/a/a/se;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/se;->a:I

    .line 618
    iget-object v2, p1, Lcom/google/d/a/a/sc;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/se;->b:Ljava/lang/Object;

    .line 621
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 622
    iget v2, p0, Lcom/google/d/a/a/se;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/se;->a:I

    .line 623
    iget-object v2, p1, Lcom/google/d/a/a/sc;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/se;->c:Ljava/lang/Object;

    .line 626
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 627
    iget-wide v2, p1, Lcom/google/d/a/a/sc;->d:J

    iget v4, p0, Lcom/google/d/a/a/se;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/se;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/se;->d:J

    .line 629
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 630
    iget v2, p0, Lcom/google/d/a/a/se;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/se;->a:I

    .line 631
    iget-object v2, p1, Lcom/google/d/a/a/sc;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/se;->e:Ljava/lang/Object;

    .line 634
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 635
    iget v2, p1, Lcom/google/d/a/a/sc;->f:I

    iget v3, p0, Lcom/google/d/a/a/se;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/se;->a:I

    iput v2, p0, Lcom/google/d/a/a/se;->f:I

    .line 637
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 638
    iget-boolean v2, p1, Lcom/google/d/a/a/sc;->g:Z

    iget v3, p0, Lcom/google/d/a/a/se;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/se;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/se;->g:Z

    .line 640
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/sc;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_e

    :goto_7
    if-eqz v0, :cond_7

    .line 641
    iget-object v0, p0, Lcom/google/d/a/a/se;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/sc;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 642
    iget v0, p0, Lcom/google/d/a/a/se;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/se;->a:I

    .line 644
    :cond_7
    iget-object v0, p1, Lcom/google/d/a/a/sc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_8
    move v2, v1

    .line 616
    goto/16 :goto_1

    :cond_9
    move v2, v1

    .line 621
    goto/16 :goto_2

    :cond_a
    move v2, v1

    .line 626
    goto :goto_3

    :cond_b
    move v2, v1

    .line 629
    goto :goto_4

    :cond_c
    move v2, v1

    .line 634
    goto :goto_5

    :cond_d
    move v2, v1

    .line 637
    goto :goto_6

    :cond_e
    move v0, v1

    .line 640
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 543
    new-instance v2, Lcom/google/d/a/a/sc;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/sc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/se;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v4, p0, Lcom/google/d/a/a/se;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/sc;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/se;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/sc;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/d/a/a/se;->d:J

    iput-wide v4, v2, Lcom/google/d/a/a/sc;->d:J

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/d/a/a/se;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/sc;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/d/a/a/se;->f:I

    iput v4, v2, Lcom/google/d/a/a/sc;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v4, p0, Lcom/google/d/a/a/se;->g:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/sc;->g:Z

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v3, v2, Lcom/google/d/a/a/sc;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/se;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/se;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/sc;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 543
    check-cast p1, Lcom/google/d/a/a/sc;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/se;->a(Lcom/google/d/a/a/sc;)Lcom/google/d/a/a/se;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 649
    iget v2, p0, Lcom/google/d/a/a/se;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 653
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 649
    goto :goto_0

    :cond_1
    move v0, v1

    .line 653
    goto :goto_1
.end method
