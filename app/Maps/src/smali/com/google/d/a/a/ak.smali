.class public final Lcom/google/d/a/a/ak;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/an;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ai;",
        "Lcom/google/d/a/a/ak;",
        ">;",
        "Lcom/google/d/a/a/an;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:I

.field private h:J

.field private i:I

.field private j:D

.field private k:F

.field private l:Z

.field private m:Ljava/lang/Object;

.field private n:Lcom/google/n/ao;

.field private o:Ljava/lang/Object;

.field private p:I

.field private q:Ljava/lang/Object;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcom/google/n/ao;

.field private u:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1329
    sget-object v0, Lcom/google/d/a/a/ai;->v:Lcom/google/d/a/a/ai;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1616
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ak;->b:Lcom/google/n/ao;

    .line 1675
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ak;->c:Lcom/google/n/ao;

    .line 1734
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ak;->d:Lcom/google/n/ao;

    .line 1793
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/ak;->e:I

    .line 1829
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ak;->f:Ljava/lang/Object;

    .line 2097
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ak;->m:Ljava/lang/Object;

    .line 2173
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ak;->n:Lcom/google/n/ao;

    .line 2232
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ak;->o:Ljava/lang/Object;

    .line 2340
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ak;->q:Ljava/lang/Object;

    .line 2417
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    .line 2554
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    .line 2690
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ak;->t:Lcom/google/n/ao;

    .line 2749
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ak;->u:Lcom/google/n/ao;

    .line 1330
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ai;)Lcom/google/d/a/a/ak;
    .locals 8

    .prologue
    const v7, 0x8000

    const/high16 v6, 0x20000

    const/high16 v5, 0x10000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1483
    invoke-static {}, Lcom/google/d/a/a/ai;->d()Lcom/google/d/a/a/ai;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1573
    :goto_0
    return-object p0

    .line 1484
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1485
    iget-object v2, p0, Lcom/google/d/a/a/ak;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1486
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1488
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1489
    iget-object v2, p0, Lcom/google/d/a/a/ak;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1490
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1492
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1493
    iget-object v2, p0, Lcom/google/d/a/a/ak;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1494
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1496
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 1497
    iget v2, p1, Lcom/google/d/a/a/ai;->e:I

    invoke-static {v2}, Lcom/google/d/a/a/al;->a(I)Lcom/google/d/a/a/al;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/d/a/a/al;->a:Lcom/google/d/a/a/al;

    :cond_4
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 1484
    goto :goto_1

    :cond_6
    move v2, v1

    .line 1488
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1492
    goto :goto_3

    :cond_8
    move v2, v1

    .line 1496
    goto :goto_4

    .line 1497
    :cond_9
    iget v3, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/ak;->a:I

    iget v2, v2, Lcom/google/d/a/a/al;->l:I

    iput v2, p0, Lcom/google/d/a/a/ak;->e:I

    .line 1499
    :cond_a
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_b

    .line 1500
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1501
    iget-object v2, p1, Lcom/google/d/a/a/ai;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/ak;->f:Ljava/lang/Object;

    .line 1504
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_6
    if-eqz v2, :cond_c

    .line 1505
    iget v2, p1, Lcom/google/d/a/a/ai;->g:I

    iget v3, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/ak;->a:I

    iput v2, p0, Lcom/google/d/a/a/ak;->g:I

    .line 1507
    :cond_c
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_7
    if-eqz v2, :cond_d

    .line 1508
    iget-wide v2, p1, Lcom/google/d/a/a/ai;->h:J

    iget v4, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/d/a/a/ak;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/ak;->h:J

    .line 1510
    :cond_d
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_8
    if-eqz v2, :cond_e

    .line 1511
    iget v2, p1, Lcom/google/d/a/a/ai;->i:I

    iget v3, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/d/a/a/ak;->a:I

    iput v2, p0, Lcom/google/d/a/a/ak;->i:I

    .line 1513
    :cond_e
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_9
    if-eqz v2, :cond_f

    .line 1514
    iget-wide v2, p1, Lcom/google/d/a/a/ai;->j:D

    iget v4, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/d/a/a/ak;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/ak;->j:D

    .line 1516
    :cond_f
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_a
    if-eqz v2, :cond_10

    .line 1517
    iget v2, p1, Lcom/google/d/a/a/ai;->k:F

    iget v3, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/d/a/a/ak;->a:I

    iput v2, p0, Lcom/google/d/a/a/ak;->k:F

    .line 1519
    :cond_10
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_b
    if-eqz v2, :cond_11

    .line 1520
    iget-boolean v2, p1, Lcom/google/d/a/a/ai;->l:Z

    iget v3, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/d/a/a/ak;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/ak;->l:Z

    .line 1522
    :cond_11
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_c
    if-eqz v2, :cond_12

    .line 1523
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1524
    iget-object v2, p1, Lcom/google/d/a/a/ai;->m:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/ak;->m:Ljava/lang/Object;

    .line 1527
    :cond_12
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_23

    move v2, v0

    :goto_d
    if-eqz v2, :cond_13

    .line 1528
    iget-object v2, p0, Lcom/google/d/a/a/ak;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1529
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1531
    :cond_13
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_24

    move v2, v0

    :goto_e
    if-eqz v2, :cond_14

    .line 1532
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1533
    iget-object v2, p1, Lcom/google/d/a/a/ai;->o:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/ak;->o:Ljava/lang/Object;

    .line 1536
    :cond_14
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_25

    move v2, v0

    :goto_f
    if-eqz v2, :cond_15

    .line 1537
    iget v2, p1, Lcom/google/d/a/a/ai;->p:I

    iget v3, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/d/a/a/ak;->a:I

    iput v2, p0, Lcom/google/d/a/a/ak;->p:I

    .line 1539
    :cond_15
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_26

    move v2, v0

    :goto_10
    if-eqz v2, :cond_16

    .line 1540
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/2addr v2, v7

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1541
    iget-object v2, p1, Lcom/google/d/a/a/ai;->q:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/ak;->q:Ljava/lang/Object;

    .line 1544
    :cond_16
    iget-object v2, p1, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_17

    .line 1545
    iget-object v2, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1546
    iget-object v2, p1, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    .line 1547
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    const v3, -0x10001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1554
    :cond_17
    :goto_11
    iget-object v2, p1, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_18

    .line 1555
    iget-object v2, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_29

    .line 1556
    iget-object v2, p1, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    .line 1557
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    const v3, -0x20001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1564
    :cond_18
    :goto_12
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_2b

    move v2, v0

    :goto_13
    if-eqz v2, :cond_19

    .line 1565
    iget-object v2, p0, Lcom/google/d/a/a/ak;->t:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1566
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1568
    :cond_19
    iget v2, p1, Lcom/google/d/a/a/ai;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_2c

    :goto_14
    if-eqz v0, :cond_1a

    .line 1569
    iget-object v0, p0, Lcom/google/d/a/a/ak;->u:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/ai;->u:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1570
    iget v0, p0, Lcom/google/d/a/a/ak;->a:I

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1572
    :cond_1a
    iget-object v0, p1, Lcom/google/d/a/a/ai;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_1b
    move v2, v1

    .line 1499
    goto/16 :goto_5

    :cond_1c
    move v2, v1

    .line 1504
    goto/16 :goto_6

    :cond_1d
    move v2, v1

    .line 1507
    goto/16 :goto_7

    :cond_1e
    move v2, v1

    .line 1510
    goto/16 :goto_8

    :cond_1f
    move v2, v1

    .line 1513
    goto/16 :goto_9

    :cond_20
    move v2, v1

    .line 1516
    goto/16 :goto_a

    :cond_21
    move v2, v1

    .line 1519
    goto/16 :goto_b

    :cond_22
    move v2, v1

    .line 1522
    goto/16 :goto_c

    :cond_23
    move v2, v1

    .line 1527
    goto/16 :goto_d

    :cond_24
    move v2, v1

    .line 1531
    goto/16 :goto_e

    :cond_25
    move v2, v1

    .line 1536
    goto/16 :goto_f

    :cond_26
    move v2, v1

    .line 1539
    goto/16 :goto_10

    .line 1549
    :cond_27
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/2addr v2, v5

    if-eq v2, v5, :cond_28

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/2addr v2, v5

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1550
    :cond_28
    iget-object v2, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_11

    .line 1559
    :cond_29
    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/2addr v2, v6

    if-eq v2, v6, :cond_2a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/ak;->a:I

    or-int/2addr v2, v6

    iput v2, p0, Lcom/google/d/a/a/ak;->a:I

    .line 1560
    :cond_2a
    iget-object v2, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_12

    :cond_2b
    move v2, v1

    .line 1564
    goto/16 :goto_13

    :cond_2c
    move v0, v1

    .line 1568
    goto :goto_14
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1321
    new-instance v2, Lcom/google/d/a/a/ai;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ai;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_13

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/ak;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/ak;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/ak;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/ak;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/ak;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/ak;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/d/a/a/ak;->e:I

    iput v4, v2, Lcom/google/d/a/a/ai;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/ak;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/ai;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/d/a/a/ak;->g:I

    iput v4, v2, Lcom/google/d/a/a/ai;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-wide v4, p0, Lcom/google/d/a/a/ak;->h:J

    iput-wide v4, v2, Lcom/google/d/a/a/ai;->h:J

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v4, p0, Lcom/google/d/a/a/ak;->i:I

    iput v4, v2, Lcom/google/d/a/a/ai;->i:I

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-wide v4, p0, Lcom/google/d/a/a/ak;->j:D

    iput-wide v4, v2, Lcom/google/d/a/a/ai;->j:D

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v4, p0, Lcom/google/d/a/a/ak;->k:F

    iput v4, v2, Lcom/google/d/a/a/ai;->k:F

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-boolean v4, p0, Lcom/google/d/a/a/ak;->l:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/ai;->l:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-object v4, p0, Lcom/google/d/a/a/ak;->m:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/ai;->m:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget-object v4, v2, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/ak;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/ak;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget-object v4, p0, Lcom/google/d/a/a/ak;->o:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/ai;->o:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x4000

    :cond_d
    iget v4, p0, Lcom/google/d/a/a/ak;->p:I

    iput v4, v2, Lcom/google/d/a/a/ai;->p:I

    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    or-int/2addr v0, v7

    :cond_e
    iget-object v4, p0, Lcom/google/d/a/a/ak;->q:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/ai;->q:Ljava/lang/Object;

    iget v4, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/2addr v4, v8

    if-ne v4, v8, :cond_f

    iget-object v4, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/ak;->a:I

    const v5, -0x10001

    and-int/2addr v4, v5

    iput v4, p0, Lcom/google/d/a/a/ak;->a:I

    :cond_f
    iget-object v4, p0, Lcom/google/d/a/a/ak;->r:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/2addr v4, v9

    if-ne v4, v9, :cond_10

    iget-object v4, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/ak;->a:I

    const v5, -0x20001

    and-int/2addr v4, v5

    iput v4, p0, Lcom/google/d/a/a/ak;->a:I

    :cond_10
    iget-object v4, p0, Lcom/google/d/a/a/ak;->s:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    const/high16 v4, 0x40000

    and-int/2addr v4, v3

    const/high16 v5, 0x40000

    if-ne v4, v5, :cond_11

    or-int/2addr v0, v8

    :cond_11
    iget-object v4, v2, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/ak;->t:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/ak;->t:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v4, 0x80000

    and-int/2addr v3, v4

    const/high16 v4, 0x80000

    if-ne v3, v4, :cond_12

    or-int/2addr v0, v9

    :cond_12
    iget-object v3, v2, Lcom/google/d/a/a/ai;->u:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/ak;->u:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/ak;->u:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/ai;->a:I

    return-object v2

    :cond_13
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1321
    check-cast p1, Lcom/google/d/a/a/ai;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ak;->a(Lcom/google/d/a/a/ai;)Lcom/google/d/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/high16 v4, 0x40000

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1577
    iget v0, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 1611
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1577
    goto :goto_0

    .line 1581
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 1582
    iget-object v0, p0, Lcom/google/d/a/a/ak;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ac;->d()Lcom/google/d/a/a/ac;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ac;

    invoke-virtual {v0}, Lcom/google/d/a/a/ac;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1584
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1581
    goto :goto_2

    .line 1587
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 1588
    iget-object v0, p0, Lcom/google/d/a/a/ak;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ac;->d()Lcom/google/d/a/a/ac;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ac;

    invoke-virtual {v0}, Lcom/google/d/a/a/ac;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 1590
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1587
    goto :goto_3

    .line 1593
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    .line 1594
    iget-object v0, p0, Lcom/google/d/a/a/ak;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ac;->d()Lcom/google/d/a/a/ac;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ac;

    invoke-virtual {v0}, Lcom/google/d/a/a/ac;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 1596
    goto :goto_1

    :cond_6
    move v0, v1

    .line 1593
    goto :goto_4

    .line 1599
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_8

    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    .line 1600
    iget-object v0, p0, Lcom/google/d/a/a/ak;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 1602
    goto :goto_1

    :cond_8
    move v0, v1

    .line 1599
    goto :goto_5

    .line 1605
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/ak;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_a

    move v0, v2

    :goto_6
    if-eqz v0, :cond_b

    .line 1606
    iget-object v0, p0, Lcom/google/d/a/a/ak;->t:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 1608
    goto/16 :goto_1

    :cond_a
    move v0, v1

    .line 1605
    goto :goto_6

    :cond_b
    move v0, v2

    .line 1611
    goto/16 :goto_1
.end method
