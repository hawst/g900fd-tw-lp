.class public final Lcom/google/d/a/a/mi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/nb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ma;",
        "Lcom/google/d/a/a/mi;",
        ">;",
        "Lcom/google/d/a/a/nb;"
    }
.end annotation


# instance fields
.field private A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private B:Z

.field private C:I

.field private D:Lcom/google/n/ao;

.field private E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private F:I

.field private G:Lcom/google/n/ao;

.field private H:Lcom/google/n/ao;

.field private J:Lcom/google/n/ao;

.field private K:I

.field private L:Lcom/google/n/ao;

.field private M:I

.field private N:I

.field private O:I

.field private P:Z

.field private Q:I

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private T:F

.field private U:Lcom/google/n/ao;

.field private a:I

.field private b:I

.field private c:Lcom/google/n/ao;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/google/n/ao;

.field private l:Z

.field private m:F

.field private n:Lcom/google/n/ao;

.field private o:Z

.field private p:F

.field private q:Lcom/google/n/ao;

.field private r:I

.field private s:Lcom/google/n/ao;

.field private t:I

.field private u:Lcom/google/n/ao;

.field private v:I

.field private w:Lcom/google/n/ao;

.field private x:I

.field private y:Z

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x11

    const/4 v1, 0x1

    .line 3712
    sget-object v0, Lcom/google/d/a/a/ma;->U:Lcom/google/d/a/a/ma;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4342
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->c:Lcom/google/n/ao;

    .line 4402
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    .line 4539
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    .line 4675
    iput v2, p0, Lcom/google/d/a/a/mi;->f:I

    .line 4711
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->g:Lcom/google/n/ao;

    .line 4770
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->h:Lcom/google/n/ao;

    .line 4830
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    .line 4967
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    .line 5103
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->k:Lcom/google/n/ao;

    .line 5162
    iput-boolean v1, p0, Lcom/google/d/a/a/mi;->l:Z

    .line 5226
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->n:Lcom/google/n/ao;

    .line 5285
    iput-boolean v1, p0, Lcom/google/d/a/a/mi;->o:Z

    .line 5349
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->q:Lcom/google/n/ao;

    .line 5408
    iput v1, p0, Lcom/google/d/a/a/mi;->r:I

    .line 5444
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->s:Lcom/google/n/ao;

    .line 5503
    iput v1, p0, Lcom/google/d/a/a/mi;->t:I

    .line 5539
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->u:Lcom/google/n/ao;

    .line 5598
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/mi;->v:I

    .line 5634
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->w:Lcom/google/n/ao;

    .line 5693
    iput v2, p0, Lcom/google/d/a/a/mi;->x:I

    .line 5762
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    .line 5899
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    .line 6067
    iput v1, p0, Lcom/google/d/a/a/mi;->C:I

    .line 6103
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->D:Lcom/google/n/ao;

    .line 6162
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    .line 6228
    iput v1, p0, Lcom/google/d/a/a/mi;->F:I

    .line 6264
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->G:Lcom/google/n/ao;

    .line 6323
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->H:Lcom/google/n/ao;

    .line 6382
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->J:Lcom/google/n/ao;

    .line 6441
    iput v1, p0, Lcom/google/d/a/a/mi;->K:I

    .line 6477
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->L:Lcom/google/n/ao;

    .line 6536
    iput v1, p0, Lcom/google/d/a/a/mi;->M:I

    .line 6572
    iput v1, p0, Lcom/google/d/a/a/mi;->N:I

    .line 6608
    iput v1, p0, Lcom/google/d/a/a/mi;->O:I

    .line 6676
    iput v1, p0, Lcom/google/d/a/a/mi;->Q:I

    .line 6713
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    .line 6850
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    .line 7018
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/mi;->U:Lcom/google/n/ao;

    .line 3713
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ma;)Lcom/google/d/a/a/mi;
    .locals 8

    .prologue
    const/high16 v7, 0x10000000

    const/high16 v6, 0x1000000

    const/high16 v5, 0x800000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4038
    invoke-static {}, Lcom/google/d/a/a/ma;->d()Lcom/google/d/a/a/ma;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4250
    :goto_0
    return-object p0

    .line 4039
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4040
    iget-object v2, p0, Lcom/google/d/a/a/mi;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4041
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4043
    :cond_1
    iget-object v2, p1, Lcom/google/d/a/a/ma;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4044
    iget-object v2, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4045
    iget-object v2, p1, Lcom/google/d/a/a/ma;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    .line 4046
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4053
    :cond_2
    :goto_2
    iget-object v2, p1, Lcom/google/d/a/a/ma;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 4054
    iget-object v2, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 4055
    iget-object v2, p1, Lcom/google/d/a/a/ma;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    .line 4056
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4063
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_c

    .line 4064
    iget v2, p1, Lcom/google/d/a/a/ma;->f:I

    invoke-static {v2}, Lcom/google/d/a/a/mp;->a(I)Lcom/google/d/a/a/mp;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/d/a/a/mp;->a:Lcom/google/d/a/a/mp;

    :cond_4
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 4039
    goto :goto_1

    .line 4048
    :cond_6
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4049
    :cond_7
    iget-object v2, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 4058
    :cond_8
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-eq v2, v3, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4059
    :cond_9
    iget-object v2, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_a
    move v2, v1

    .line 4063
    goto :goto_4

    .line 4064
    :cond_b
    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iget v2, v2, Lcom/google/d/a/a/mp;->p:I

    iput v2, p0, Lcom/google/d/a/a/mi;->f:I

    .line 4066
    :cond_c
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_5
    if-eqz v2, :cond_d

    .line 4067
    iget-object v2, p0, Lcom/google/d/a/a/mi;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4068
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4070
    :cond_d
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_6
    if-eqz v2, :cond_e

    .line 4071
    iget-object v2, p0, Lcom/google/d/a/a/mi;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4072
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4074
    :cond_e
    iget-object v2, p1, Lcom/google/d/a/a/ma;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 4075
    iget-object v2, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 4076
    iget-object v2, p1, Lcom/google/d/a/a/ma;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    .line 4077
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4084
    :cond_f
    :goto_7
    iget-object v2, p1, Lcom/google/d/a/a/ma;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_10

    .line 4085
    iget-object v2, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 4086
    iget-object v2, p1, Lcom/google/d/a/a/ma;->j:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    .line 4087
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4094
    :cond_10
    :goto_8
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_9
    if-eqz v2, :cond_11

    .line 4095
    iget-object v2, p0, Lcom/google/d/a/a/mi;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4096
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4098
    :cond_11
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_a
    if-eqz v2, :cond_12

    .line 4099
    iget-boolean v2, p1, Lcom/google/d/a/a/ma;->l:Z

    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/mi;->l:Z

    .line 4101
    :cond_12
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_b
    if-eqz v2, :cond_13

    .line 4102
    iget v2, p1, Lcom/google/d/a/a/ma;->m:F

    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iput v2, p0, Lcom/google/d/a/a/mi;->m:F

    .line 4104
    :cond_13
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_c
    if-eqz v2, :cond_14

    .line 4105
    iget-object v2, p0, Lcom/google/d/a/a/mi;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4106
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4108
    :cond_14
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_23

    move v2, v0

    :goto_d
    if-eqz v2, :cond_15

    .line 4109
    iget-boolean v2, p1, Lcom/google/d/a/a/ma;->o:Z

    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/mi;->o:Z

    .line 4111
    :cond_15
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_24

    move v2, v0

    :goto_e
    if-eqz v2, :cond_16

    .line 4112
    iget v2, p1, Lcom/google/d/a/a/ma;->p:F

    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iput v2, p0, Lcom/google/d/a/a/mi;->p:F

    .line 4114
    :cond_16
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_25

    move v2, v0

    :goto_f
    if-eqz v2, :cond_17

    .line 4115
    iget-object v2, p0, Lcom/google/d/a/a/mi;->q:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->q:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4116
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4118
    :cond_17
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_26

    move v2, v0

    :goto_10
    if-eqz v2, :cond_28

    .line 4119
    iget v2, p1, Lcom/google/d/a/a/ma;->r:I

    invoke-static {v2}, Lcom/google/d/a/a/mn;->a(I)Lcom/google/d/a/a/mn;

    move-result-object v2

    if-nez v2, :cond_18

    sget-object v2, Lcom/google/d/a/a/mn;->a:Lcom/google/d/a/a/mn;

    :cond_18
    if-nez v2, :cond_27

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    move v2, v1

    .line 4066
    goto/16 :goto_5

    :cond_1a
    move v2, v1

    .line 4070
    goto/16 :goto_6

    .line 4079
    :cond_1b
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-eq v2, v3, :cond_1c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4080
    :cond_1c
    iget-object v2, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    .line 4089
    :cond_1d
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-eq v2, v3, :cond_1e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4090
    :cond_1e
    iget-object v2, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_1f
    move v2, v1

    .line 4094
    goto/16 :goto_9

    :cond_20
    move v2, v1

    .line 4098
    goto/16 :goto_a

    :cond_21
    move v2, v1

    .line 4101
    goto/16 :goto_b

    :cond_22
    move v2, v1

    .line 4104
    goto/16 :goto_c

    :cond_23
    move v2, v1

    .line 4108
    goto/16 :goto_d

    :cond_24
    move v2, v1

    .line 4111
    goto/16 :goto_e

    :cond_25
    move v2, v1

    .line 4114
    goto/16 :goto_f

    :cond_26
    move v2, v1

    .line 4118
    goto :goto_10

    .line 4119
    :cond_27
    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    const v4, 0x8000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iget v2, v2, Lcom/google/d/a/a/mn;->j:I

    iput v2, p0, Lcom/google/d/a/a/mi;->r:I

    .line 4121
    :cond_28
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_2b

    move v2, v0

    :goto_11
    if-eqz v2, :cond_29

    .line 4122
    iget-object v2, p0, Lcom/google/d/a/a/mi;->s:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->s:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4123
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v3, 0x10000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4125
    :cond_29
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_2c

    move v2, v0

    :goto_12
    if-eqz v2, :cond_2e

    .line 4126
    iget v2, p1, Lcom/google/d/a/a/ma;->t:I

    invoke-static {v2}, Lcom/google/d/a/a/mx;->a(I)Lcom/google/d/a/a/mx;

    move-result-object v2

    if-nez v2, :cond_2a

    sget-object v2, Lcom/google/d/a/a/mx;->a:Lcom/google/d/a/a/mx;

    :cond_2a
    if-nez v2, :cond_2d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2b
    move v2, v1

    .line 4121
    goto :goto_11

    :cond_2c
    move v2, v1

    .line 4125
    goto :goto_12

    .line 4126
    :cond_2d
    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v4, 0x20000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iget v2, v2, Lcom/google/d/a/a/mx;->l:I

    iput v2, p0, Lcom/google/d/a/a/mi;->t:I

    .line 4128
    :cond_2e
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_31

    move v2, v0

    :goto_13
    if-eqz v2, :cond_2f

    .line 4129
    iget-object v2, p0, Lcom/google/d/a/a/mi;->u:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->u:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4130
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4132
    :cond_2f
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const v3, 0x8000

    and-int/2addr v2, v3

    const v3, 0x8000

    if-ne v2, v3, :cond_32

    move v2, v0

    :goto_14
    if-eqz v2, :cond_34

    .line 4133
    iget v2, p1, Lcom/google/d/a/a/ma;->v:I

    invoke-static {v2}, Lcom/google/d/a/a/mv;->a(I)Lcom/google/d/a/a/mv;

    move-result-object v2

    if-nez v2, :cond_30

    sget-object v2, Lcom/google/d/a/a/mv;->a:Lcom/google/d/a/a/mv;

    :cond_30
    if-nez v2, :cond_33

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_31
    move v2, v1

    .line 4128
    goto :goto_13

    :cond_32
    move v2, v1

    .line 4132
    goto :goto_14

    .line 4133
    :cond_33
    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v4, 0x80000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iget v2, v2, Lcom/google/d/a/a/mv;->j:I

    iput v2, p0, Lcom/google/d/a/a/mi;->v:I

    .line 4135
    :cond_34
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000

    if-ne v2, v3, :cond_37

    move v2, v0

    :goto_15
    if-eqz v2, :cond_35

    .line 4136
    iget-object v2, p0, Lcom/google/d/a/a/mi;->w:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->w:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4137
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v3, 0x100000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4139
    :cond_35
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_38

    move v2, v0

    :goto_16
    if-eqz v2, :cond_3a

    .line 4140
    iget v2, p1, Lcom/google/d/a/a/ma;->x:I

    invoke-static {v2}, Lcom/google/d/a/a/mz;->a(I)Lcom/google/d/a/a/mz;

    move-result-object v2

    if-nez v2, :cond_36

    sget-object v2, Lcom/google/d/a/a/mz;->a:Lcom/google/d/a/a/mz;

    :cond_36
    if-nez v2, :cond_39

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_37
    move v2, v1

    .line 4135
    goto :goto_15

    :cond_38
    move v2, v1

    .line 4139
    goto :goto_16

    .line 4140
    :cond_39
    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v4, 0x200000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iget v2, v2, Lcom/google/d/a/a/mz;->z:I

    iput v2, p0, Lcom/google/d/a/a/mi;->x:I

    .line 4142
    :cond_3a
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_40

    move v2, v0

    :goto_17
    if-eqz v2, :cond_3b

    .line 4143
    iget-boolean v2, p1, Lcom/google/d/a/a/ma;->y:Z

    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v4, 0x400000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/mi;->y:Z

    .line 4145
    :cond_3b
    iget-object v2, p1, Lcom/google/d/a/a/ma;->z:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3c

    .line 4146
    iget-object v2, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_41

    .line 4147
    iget-object v2, p1, Lcom/google/d/a/a/ma;->z:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    .line 4148
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const v3, -0x800001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4155
    :cond_3c
    :goto_18
    iget-object v2, p1, Lcom/google/d/a/a/ma;->A:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3d

    .line 4156
    iget-object v2, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_43

    .line 4157
    iget-object v2, p1, Lcom/google/d/a/a/ma;->A:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    .line 4158
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const v3, -0x1000001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4165
    :cond_3d
    :goto_19
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_45

    move v2, v0

    :goto_1a
    if-eqz v2, :cond_3e

    .line 4166
    iget-boolean v2, p1, Lcom/google/d/a/a/ma;->B:Z

    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v4, 0x2000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/mi;->B:Z

    .line 4168
    :cond_3e
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_46

    move v2, v0

    :goto_1b
    if-eqz v2, :cond_48

    .line 4169
    iget v2, p1, Lcom/google/d/a/a/ma;->C:I

    invoke-static {v2}, Lcom/google/d/a/a/mc;->a(I)Lcom/google/d/a/a/mc;

    move-result-object v2

    if-nez v2, :cond_3f

    sget-object v2, Lcom/google/d/a/a/mc;->a:Lcom/google/d/a/a/mc;

    :cond_3f
    if-nez v2, :cond_47

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_40
    move v2, v1

    .line 4142
    goto :goto_17

    .line 4150
    :cond_41
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/2addr v2, v5

    if-eq v2, v5, :cond_42

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/2addr v2, v5

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4151
    :cond_42
    iget-object v2, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->z:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_18

    .line 4160
    :cond_43
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/2addr v2, v6

    if-eq v2, v6, :cond_44

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/2addr v2, v6

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4161
    :cond_44
    iget-object v2, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->A:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_19

    :cond_45
    move v2, v1

    .line 4165
    goto :goto_1a

    :cond_46
    move v2, v1

    .line 4168
    goto :goto_1b

    .line 4169
    :cond_47
    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v4, 0x4000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iget v2, v2, Lcom/google/d/a/a/mc;->e:I

    iput v2, p0, Lcom/google/d/a/a/mi;->C:I

    .line 4171
    :cond_48
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_4c

    move v2, v0

    :goto_1c
    if-eqz v2, :cond_49

    .line 4172
    iget-object v2, p0, Lcom/google/d/a/a/mi;->D:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->D:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4173
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v3, 0x8000000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4175
    :cond_49
    iget-object v2, p1, Lcom/google/d/a/a/ma;->E:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4a

    .line 4176
    iget-object v2, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4d

    .line 4177
    iget-object v2, p1, Lcom/google/d/a/a/ma;->E:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    .line 4178
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const v3, -0x10000001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4185
    :cond_4a
    :goto_1d
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    const/high16 v3, 0x400000

    if-ne v2, v3, :cond_4f

    move v2, v0

    :goto_1e
    if-eqz v2, :cond_51

    .line 4186
    iget v2, p1, Lcom/google/d/a/a/ma;->F:I

    invoke-static {v2}, Lcom/google/d/a/a/ml;->a(I)Lcom/google/d/a/a/ml;

    move-result-object v2

    if-nez v2, :cond_4b

    sget-object v2, Lcom/google/d/a/a/ml;->a:Lcom/google/d/a/a/ml;

    :cond_4b
    if-nez v2, :cond_50

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4c
    move v2, v1

    .line 4171
    goto :goto_1c

    .line 4180
    :cond_4d
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/2addr v2, v7

    if-eq v2, v7, :cond_4e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    or-int/2addr v2, v7

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4181
    :cond_4e
    iget-object v2, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->E:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1d

    :cond_4f
    move v2, v1

    .line 4185
    goto :goto_1e

    .line 4186
    :cond_50
    iget v3, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v4, 0x20000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/d/a/a/mi;->a:I

    iget v2, v2, Lcom/google/d/a/a/ml;->f:I

    iput v2, p0, Lcom/google/d/a/a/mi;->F:I

    .line 4188
    :cond_51
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_56

    move v2, v0

    :goto_1f
    if-eqz v2, :cond_52

    .line 4189
    iget-object v2, p0, Lcom/google/d/a/a/mi;->G:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->G:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4190
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4192
    :cond_52
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_57

    move v2, v0

    :goto_20
    if-eqz v2, :cond_53

    .line 4193
    iget-object v2, p0, Lcom/google/d/a/a/mi;->H:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->H:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4194
    iget v2, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v3, -0x80000000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/d/a/a/mi;->a:I

    .line 4196
    :cond_53
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    const/high16 v3, 0x2000000

    if-ne v2, v3, :cond_58

    move v2, v0

    :goto_21
    if-eqz v2, :cond_54

    .line 4197
    iget-object v2, p0, Lcom/google/d/a/a/mi;->J:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->I:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4198
    iget v2, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/mi;->b:I

    .line 4200
    :cond_54
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v2, v3

    const/high16 v3, 0x4000000

    if-ne v2, v3, :cond_59

    move v2, v0

    :goto_22
    if-eqz v2, :cond_5b

    .line 4201
    iget v2, p1, Lcom/google/d/a/a/ma;->J:I

    invoke-static {v2}, Lcom/google/d/a/a/mj;->a(I)Lcom/google/d/a/a/mj;

    move-result-object v2

    if-nez v2, :cond_55

    sget-object v2, Lcom/google/d/a/a/mj;->a:Lcom/google/d/a/a/mj;

    :cond_55
    if-nez v2, :cond_5a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_56
    move v2, v1

    .line 4188
    goto :goto_1f

    :cond_57
    move v2, v1

    .line 4192
    goto :goto_20

    :cond_58
    move v2, v1

    .line 4196
    goto :goto_21

    :cond_59
    move v2, v1

    .line 4200
    goto :goto_22

    .line 4201
    :cond_5a
    iget v3, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/mi;->b:I

    iget v2, v2, Lcom/google/d/a/a/mj;->c:I

    iput v2, p0, Lcom/google/d/a/a/mi;->K:I

    .line 4203
    :cond_5b
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v2, v3

    const/high16 v3, 0x8000000

    if-ne v2, v3, :cond_5e

    move v2, v0

    :goto_23
    if-eqz v2, :cond_5c

    .line 4204
    iget-object v2, p0, Lcom/google/d/a/a/mi;->L:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->K:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4205
    iget v2, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/mi;->b:I

    .line 4207
    :cond_5c
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_5f

    move v2, v0

    :goto_24
    if-eqz v2, :cond_61

    .line 4208
    iget v2, p1, Lcom/google/d/a/a/ma;->L:I

    invoke-static {v2}, Lcom/google/d/a/a/me;->a(I)Lcom/google/d/a/a/me;

    move-result-object v2

    if-nez v2, :cond_5d

    sget-object v2, Lcom/google/d/a/a/me;->a:Lcom/google/d/a/a/me;

    :cond_5d
    if-nez v2, :cond_60

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5e
    move v2, v1

    .line 4203
    goto :goto_23

    :cond_5f
    move v2, v1

    .line 4207
    goto :goto_24

    .line 4208
    :cond_60
    iget v3, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/mi;->b:I

    iget v2, v2, Lcom/google/d/a/a/me;->g:I

    iput v2, p0, Lcom/google/d/a/a/mi;->M:I

    .line 4210
    :cond_61
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000000

    if-ne v2, v3, :cond_63

    move v2, v0

    :goto_25
    if-eqz v2, :cond_65

    .line 4211
    iget v2, p1, Lcom/google/d/a/a/ma;->M:I

    invoke-static {v2}, Lcom/google/d/a/a/mg;->a(I)Lcom/google/d/a/a/mg;

    move-result-object v2

    if-nez v2, :cond_62

    sget-object v2, Lcom/google/d/a/a/mg;->a:Lcom/google/d/a/a/mg;

    :cond_62
    if-nez v2, :cond_64

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_63
    move v2, v1

    .line 4210
    goto :goto_25

    .line 4211
    :cond_64
    iget v3, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/mi;->b:I

    iget v2, v2, Lcom/google/d/a/a/mg;->d:I

    iput v2, p0, Lcom/google/d/a/a/mi;->N:I

    .line 4213
    :cond_65
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v2, v3, :cond_67

    move v2, v0

    :goto_26
    if-eqz v2, :cond_69

    .line 4214
    iget v2, p1, Lcom/google/d/a/a/ma;->N:I

    invoke-static {v2}, Lcom/google/d/a/a/mr;->a(I)Lcom/google/d/a/a/mr;

    move-result-object v2

    if-nez v2, :cond_66

    sget-object v2, Lcom/google/d/a/a/mr;->a:Lcom/google/d/a/a/mr;

    :cond_66
    if-nez v2, :cond_68

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_67
    move v2, v1

    .line 4213
    goto :goto_26

    .line 4214
    :cond_68
    iget v3, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/mi;->b:I

    iget v2, v2, Lcom/google/d/a/a/mr;->f:I

    iput v2, p0, Lcom/google/d/a/a/mi;->O:I

    .line 4216
    :cond_69
    iget v2, p1, Lcom/google/d/a/a/ma;->a:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_6c

    move v2, v0

    :goto_27
    if-eqz v2, :cond_6a

    .line 4217
    iget-boolean v2, p1, Lcom/google/d/a/a/ma;->O:Z

    iget v3, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/mi;->b:I

    iput-boolean v2, p0, Lcom/google/d/a/a/mi;->P:Z

    .line 4219
    :cond_6a
    iget v2, p1, Lcom/google/d/a/a/ma;->b:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6d

    move v2, v0

    :goto_28
    if-eqz v2, :cond_6f

    .line 4220
    iget v2, p1, Lcom/google/d/a/a/ma;->P:I

    invoke-static {v2}, Lcom/google/d/a/a/mt;->a(I)Lcom/google/d/a/a/mt;

    move-result-object v2

    if-nez v2, :cond_6b

    sget-object v2, Lcom/google/d/a/a/mt;->a:Lcom/google/d/a/a/mt;

    :cond_6b
    if-nez v2, :cond_6e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6c
    move v2, v1

    .line 4216
    goto :goto_27

    :cond_6d
    move v2, v1

    .line 4219
    goto :goto_28

    .line 4220
    :cond_6e
    iget v3, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/d/a/a/mi;->b:I

    iget v2, v2, Lcom/google/d/a/a/mt;->d:I

    iput v2, p0, Lcom/google/d/a/a/mi;->Q:I

    .line 4222
    :cond_6f
    iget-object v2, p1, Lcom/google/d/a/a/ma;->Q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_70

    .line 4223
    iget-object v2, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_74

    .line 4224
    iget-object v2, p1, Lcom/google/d/a/a/ma;->Q:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    .line 4225
    iget v2, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v2, v2, -0x101

    iput v2, p0, Lcom/google/d/a/a/mi;->b:I

    .line 4232
    :cond_70
    :goto_29
    iget-object v2, p1, Lcom/google/d/a/a/ma;->R:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_71

    .line 4233
    iget-object v2, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_76

    .line 4234
    iget-object v2, p1, Lcom/google/d/a/a/ma;->R:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    .line 4235
    iget v2, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/d/a/a/mi;->b:I

    .line 4242
    :cond_71
    :goto_2a
    iget v2, p1, Lcom/google/d/a/a/ma;->b:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_78

    move v2, v0

    :goto_2b
    if-eqz v2, :cond_72

    .line 4243
    iget v2, p1, Lcom/google/d/a/a/ma;->S:F

    iget v3, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/d/a/a/mi;->b:I

    iput v2, p0, Lcom/google/d/a/a/mi;->T:F

    .line 4245
    :cond_72
    iget v2, p1, Lcom/google/d/a/a/ma;->b:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_79

    :goto_2c
    if-eqz v0, :cond_73

    .line 4246
    iget-object v0, p0, Lcom/google/d/a/a/mi;->U:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/ma;->T:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4247
    iget v0, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/d/a/a/mi;->b:I

    .line 4249
    :cond_73
    iget-object v0, p1, Lcom/google/d/a/a/ma;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 4227
    :cond_74
    iget v2, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-eq v2, v3, :cond_75

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/d/a/a/mi;->b:I

    .line 4228
    :cond_75
    iget-object v2, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->Q:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_29

    .line 4237
    :cond_76
    iget v2, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-eq v2, v3, :cond_77

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/mi;->b:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/d/a/a/mi;->b:I

    .line 4238
    :cond_77
    iget-object v2, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/ma;->R:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2a

    :cond_78
    move v2, v1

    .line 4242
    goto :goto_2b

    :cond_79
    move v0, v1

    .line 4245
    goto :goto_2c
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 12

    .prologue
    const/high16 v11, 0x10000

    const v10, 0x8000

    const/high16 v9, -0x80000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3704
    new-instance v3, Lcom/google/d/a/a/ma;

    invoke-direct {v3, p0}, Lcom/google/d/a/a/ma;-><init>(Lcom/google/n/v;)V

    iget v4, p0, Lcom/google/d/a/a/mi;->a:I

    iget v5, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit8 v0, v4, 0x1

    if-ne v0, v1, :cond_2b

    move v0, v1

    :goto_0
    iget-object v6, v3, Lcom/google/d/a/a/ma;->c:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->c:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->c:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v6, v6, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v6, v6, -0x3

    iput v6, p0, Lcom/google/d/a/a/mi;->a:I

    :cond_0
    iget-object v6, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    iput-object v6, v3, Lcom/google/d/a/a/ma;->d:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v6, v6, 0x4

    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    iget-object v6, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v6, v6, -0x5

    iput v6, p0, Lcom/google/d/a/a/mi;->a:I

    :cond_1
    iget-object v6, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    iput-object v6, v3, Lcom/google/d/a/a/ma;->e:Ljava/util/List;

    and-int/lit8 v6, v4, 0x8

    const/16 v7, 0x8

    if-ne v6, v7, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget v6, p0, Lcom/google/d/a/a/mi;->f:I

    iput v6, v3, Lcom/google/d/a/a/ma;->f:I

    and-int/lit8 v6, v4, 0x10

    const/16 v7, 0x10

    if-ne v6, v7, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v6, v3, Lcom/google/d/a/a/ma;->g:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->g:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->g:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int/lit8 v6, v4, 0x20

    const/16 v7, 0x20

    if-ne v6, v7, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v6, v3, Lcom/google/d/a/a/ma;->h:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->h:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->h:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v6, v6, 0x40

    const/16 v7, 0x40

    if-ne v6, v7, :cond_5

    iget-object v6, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v6, v6, -0x41

    iput v6, p0, Lcom/google/d/a/a/mi;->a:I

    :cond_5
    iget-object v6, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    iput-object v6, v3, Lcom/google/d/a/a/ma;->i:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit16 v6, v6, 0x80

    const/16 v7, 0x80

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit16 v6, v6, -0x81

    iput v6, p0, Lcom/google/d/a/a/mi;->a:I

    :cond_6
    iget-object v6, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    iput-object v6, v3, Lcom/google/d/a/a/ma;->j:Ljava/util/List;

    and-int/lit16 v6, v4, 0x100

    const/16 v7, 0x100

    if-ne v6, v7, :cond_7

    or-int/lit8 v0, v0, 0x10

    :cond_7
    iget-object v6, v3, Lcom/google/d/a/a/ma;->k:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->k:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->k:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int/lit16 v6, v4, 0x200

    const/16 v7, 0x200

    if-ne v6, v7, :cond_8

    or-int/lit8 v0, v0, 0x20

    :cond_8
    iget-boolean v6, p0, Lcom/google/d/a/a/mi;->l:Z

    iput-boolean v6, v3, Lcom/google/d/a/a/ma;->l:Z

    and-int/lit16 v6, v4, 0x400

    const/16 v7, 0x400

    if-ne v6, v7, :cond_9

    or-int/lit8 v0, v0, 0x40

    :cond_9
    iget v6, p0, Lcom/google/d/a/a/mi;->m:F

    iput v6, v3, Lcom/google/d/a/a/ma;->m:F

    and-int/lit16 v6, v4, 0x800

    const/16 v7, 0x800

    if-ne v6, v7, :cond_a

    or-int/lit16 v0, v0, 0x80

    :cond_a
    iget-object v6, v3, Lcom/google/d/a/a/ma;->n:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->n:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->n:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int/lit16 v6, v4, 0x1000

    const/16 v7, 0x1000

    if-ne v6, v7, :cond_b

    or-int/lit16 v0, v0, 0x100

    :cond_b
    iget-boolean v6, p0, Lcom/google/d/a/a/mi;->o:Z

    iput-boolean v6, v3, Lcom/google/d/a/a/ma;->o:Z

    and-int/lit16 v6, v4, 0x2000

    const/16 v7, 0x2000

    if-ne v6, v7, :cond_c

    or-int/lit16 v0, v0, 0x200

    :cond_c
    iget v6, p0, Lcom/google/d/a/a/mi;->p:F

    iput v6, v3, Lcom/google/d/a/a/ma;->p:F

    and-int/lit16 v6, v4, 0x4000

    const/16 v7, 0x4000

    if-ne v6, v7, :cond_d

    or-int/lit16 v0, v0, 0x400

    :cond_d
    iget-object v6, v3, Lcom/google/d/a/a/ma;->q:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->q:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->q:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int v6, v4, v10

    if-ne v6, v10, :cond_e

    or-int/lit16 v0, v0, 0x800

    :cond_e
    iget v6, p0, Lcom/google/d/a/a/mi;->r:I

    iput v6, v3, Lcom/google/d/a/a/ma;->r:I

    and-int v6, v4, v11

    if-ne v6, v11, :cond_f

    or-int/lit16 v0, v0, 0x1000

    :cond_f
    iget-object v6, v3, Lcom/google/d/a/a/ma;->s:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->s:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->s:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x20000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000

    if-ne v6, v7, :cond_10

    or-int/lit16 v0, v0, 0x2000

    :cond_10
    iget v6, p0, Lcom/google/d/a/a/mi;->t:I

    iput v6, v3, Lcom/google/d/a/a/ma;->t:I

    const/high16 v6, 0x40000

    and-int/2addr v6, v4

    const/high16 v7, 0x40000

    if-ne v6, v7, :cond_11

    or-int/lit16 v0, v0, 0x4000

    :cond_11
    iget-object v6, v3, Lcom/google/d/a/a/ma;->u:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->u:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->u:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x80000

    and-int/2addr v6, v4

    const/high16 v7, 0x80000

    if-ne v6, v7, :cond_12

    or-int/2addr v0, v10

    :cond_12
    iget v6, p0, Lcom/google/d/a/a/mi;->v:I

    iput v6, v3, Lcom/google/d/a/a/ma;->v:I

    const/high16 v6, 0x100000

    and-int/2addr v6, v4

    const/high16 v7, 0x100000

    if-ne v6, v7, :cond_13

    or-int/2addr v0, v11

    :cond_13
    iget-object v6, v3, Lcom/google/d/a/a/ma;->w:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->w:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->w:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    const/high16 v6, 0x200000

    and-int/2addr v6, v4

    const/high16 v7, 0x200000

    if-ne v6, v7, :cond_14

    const/high16 v6, 0x20000

    or-int/2addr v0, v6

    :cond_14
    iget v6, p0, Lcom/google/d/a/a/mi;->x:I

    iput v6, v3, Lcom/google/d/a/a/ma;->x:I

    const/high16 v6, 0x400000

    and-int/2addr v6, v4

    const/high16 v7, 0x400000

    if-ne v6, v7, :cond_15

    const/high16 v6, 0x40000

    or-int/2addr v0, v6

    :cond_15
    iget-boolean v6, p0, Lcom/google/d/a/a/mi;->y:Z

    iput-boolean v6, v3, Lcom/google/d/a/a/ma;->y:Z

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v7, 0x800000

    and-int/2addr v6, v7

    const/high16 v7, 0x800000

    if-ne v6, v7, :cond_16

    iget-object v6, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    const v7, -0x800001

    and-int/2addr v6, v7

    iput v6, p0, Lcom/google/d/a/a/mi;->a:I

    :cond_16
    iget-object v6, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    iput-object v6, v3, Lcom/google/d/a/a/ma;->z:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v7, 0x1000000

    and-int/2addr v6, v7

    const/high16 v7, 0x1000000

    if-ne v6, v7, :cond_17

    iget-object v6, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    const v7, -0x1000001

    and-int/2addr v6, v7

    iput v6, p0, Lcom/google/d/a/a/mi;->a:I

    :cond_17
    iget-object v6, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    iput-object v6, v3, Lcom/google/d/a/a/ma;->A:Ljava/util/List;

    const/high16 v6, 0x2000000

    and-int/2addr v6, v4

    const/high16 v7, 0x2000000

    if-ne v6, v7, :cond_18

    const/high16 v6, 0x80000

    or-int/2addr v0, v6

    :cond_18
    iget-boolean v6, p0, Lcom/google/d/a/a/mi;->B:Z

    iput-boolean v6, v3, Lcom/google/d/a/a/ma;->B:Z

    const/high16 v6, 0x4000000

    and-int/2addr v6, v4

    const/high16 v7, 0x4000000

    if-ne v6, v7, :cond_19

    const/high16 v6, 0x100000

    or-int/2addr v0, v6

    :cond_19
    iget v6, p0, Lcom/google/d/a/a/mi;->C:I

    iput v6, v3, Lcom/google/d/a/a/ma;->C:I

    const/high16 v6, 0x8000000

    and-int/2addr v6, v4

    const/high16 v7, 0x8000000

    if-ne v6, v7, :cond_1a

    const/high16 v6, 0x200000

    or-int/2addr v0, v6

    :cond_1a
    iget-object v6, v3, Lcom/google/d/a/a/ma;->D:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->D:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->D:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    const/high16 v7, 0x10000000

    and-int/2addr v6, v7

    const/high16 v7, 0x10000000

    if-ne v6, v7, :cond_1b

    iget-object v6, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    iget v6, p0, Lcom/google/d/a/a/mi;->a:I

    const v7, -0x10000001

    and-int/2addr v6, v7

    iput v6, p0, Lcom/google/d/a/a/mi;->a:I

    :cond_1b
    iget-object v6, p0, Lcom/google/d/a/a/mi;->E:Ljava/util/List;

    iput-object v6, v3, Lcom/google/d/a/a/ma;->E:Ljava/util/List;

    const/high16 v6, 0x20000000

    and-int/2addr v6, v4

    const/high16 v7, 0x20000000

    if-ne v6, v7, :cond_1c

    const/high16 v6, 0x400000

    or-int/2addr v0, v6

    :cond_1c
    iget v6, p0, Lcom/google/d/a/a/mi;->F:I

    iput v6, v3, Lcom/google/d/a/a/ma;->F:I

    const/high16 v6, 0x40000000    # 2.0f

    and-int/2addr v6, v4

    const/high16 v7, 0x40000000    # 2.0f

    if-ne v6, v7, :cond_1d

    const/high16 v6, 0x800000

    or-int/2addr v0, v6

    :cond_1d
    iget-object v6, v3, Lcom/google/d/a/a/ma;->G:Lcom/google/n/ao;

    iget-object v7, p0, Lcom/google/d/a/a/mi;->G:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v7

    iget-object v8, p0, Lcom/google/d/a/a/mi;->G:Lcom/google/n/ao;

    iget-object v8, v8, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v7, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v8, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    and-int/2addr v4, v9

    if-ne v4, v9, :cond_1e

    const/high16 v4, 0x1000000

    or-int/2addr v0, v4

    :cond_1e
    iget-object v4, v3, Lcom/google/d/a/a/ma;->H:Lcom/google/n/ao;

    iget-object v6, p0, Lcom/google/d/a/a/mi;->H:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v7, p0, Lcom/google/d/a/a/mi;->H:Lcom/google/n/ao;

    iget-object v7, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v7, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v5, 0x1

    if-ne v4, v1, :cond_1f

    const/high16 v4, 0x2000000

    or-int/2addr v0, v4

    :cond_1f
    iget-object v4, v3, Lcom/google/d/a/a/ma;->I:Lcom/google/n/ao;

    iget-object v6, p0, Lcom/google/d/a/a/mi;->J:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v7, p0, Lcom/google/d/a/a/mi;->J:Lcom/google/n/ao;

    iget-object v7, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v7, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v5, 0x2

    const/4 v6, 0x2

    if-ne v4, v6, :cond_20

    const/high16 v4, 0x4000000

    or-int/2addr v0, v4

    :cond_20
    iget v4, p0, Lcom/google/d/a/a/mi;->K:I

    iput v4, v3, Lcom/google/d/a/a/ma;->J:I

    and-int/lit8 v4, v5, 0x4

    const/4 v6, 0x4

    if-ne v4, v6, :cond_21

    const/high16 v4, 0x8000000

    or-int/2addr v0, v4

    :cond_21
    iget-object v4, v3, Lcom/google/d/a/a/ma;->K:Lcom/google/n/ao;

    iget-object v6, p0, Lcom/google/d/a/a/mi;->L:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v7, p0, Lcom/google/d/a/a/mi;->L:Lcom/google/n/ao;

    iget-object v7, v7, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v7, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v5, 0x8

    const/16 v6, 0x8

    if-ne v4, v6, :cond_22

    const/high16 v4, 0x10000000

    or-int/2addr v0, v4

    :cond_22
    iget v4, p0, Lcom/google/d/a/a/mi;->M:I

    iput v4, v3, Lcom/google/d/a/a/ma;->L:I

    and-int/lit8 v4, v5, 0x10

    const/16 v6, 0x10

    if-ne v4, v6, :cond_23

    const/high16 v4, 0x20000000

    or-int/2addr v0, v4

    :cond_23
    iget v4, p0, Lcom/google/d/a/a/mi;->N:I

    iput v4, v3, Lcom/google/d/a/a/ma;->M:I

    and-int/lit8 v4, v5, 0x20

    const/16 v6, 0x20

    if-ne v4, v6, :cond_24

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v0, v4

    :cond_24
    iget v4, p0, Lcom/google/d/a/a/mi;->O:I

    iput v4, v3, Lcom/google/d/a/a/ma;->N:I

    and-int/lit8 v4, v5, 0x40

    const/16 v6, 0x40

    if-ne v4, v6, :cond_25

    or-int/2addr v0, v9

    :cond_25
    iget-boolean v4, p0, Lcom/google/d/a/a/mi;->P:Z

    iput-boolean v4, v3, Lcom/google/d/a/a/ma;->O:Z

    and-int/lit16 v4, v5, 0x80

    const/16 v6, 0x80

    if-ne v4, v6, :cond_2a

    :goto_1
    iget v4, p0, Lcom/google/d/a/a/mi;->Q:I

    iput v4, v3, Lcom/google/d/a/a/ma;->P:I

    iget v4, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v4, v4, 0x100

    const/16 v6, 0x100

    if-ne v4, v6, :cond_26

    iget-object v4, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v4, v4, -0x101

    iput v4, p0, Lcom/google/d/a/a/mi;->b:I

    :cond_26
    iget-object v4, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    iput-object v4, v3, Lcom/google/d/a/a/ma;->Q:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v4, v4, 0x200

    const/16 v6, 0x200

    if-ne v4, v6, :cond_27

    iget-object v4, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v4, v4, -0x201

    iput v4, p0, Lcom/google/d/a/a/mi;->b:I

    :cond_27
    iget-object v4, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    iput-object v4, v3, Lcom/google/d/a/a/ma;->R:Ljava/util/List;

    and-int/lit16 v4, v5, 0x400

    const/16 v6, 0x400

    if-ne v4, v6, :cond_28

    or-int/lit8 v1, v1, 0x2

    :cond_28
    iget v4, p0, Lcom/google/d/a/a/mi;->T:F

    iput v4, v3, Lcom/google/d/a/a/ma;->S:F

    and-int/lit16 v4, v5, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_29

    or-int/lit8 v1, v1, 0x4

    :cond_29
    iget-object v4, v3, Lcom/google/d/a/a/ma;->T:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/mi;->U:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/mi;->U:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v4, Lcom/google/n/ao;->d:Z

    iput v0, v3, Lcom/google/d/a/a/ma;->a:I

    iput v1, v3, Lcom/google/d/a/a/ma;->b:I

    return-object v3

    :cond_2a
    move v1, v2

    goto :goto_1

    :cond_2b
    move v0, v2

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3704
    check-cast p1, Lcom/google/d/a/a/ma;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/mi;->a(Lcom/google/d/a/a/ma;)Lcom/google/d/a/a/mi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4254
    iget v0, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 4336
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 4254
    goto :goto_0

    .line 4258
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    move v0, v3

    :goto_2
    if-eqz v0, :cond_0

    .line 4262
    iget-object v0, p0, Lcom/google/d/a/a/mi;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 4266
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 4267
    iget-object v0, p0, Lcom/google/d/a/a/mi;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4266
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v2

    .line 4258
    goto :goto_2

    :cond_4
    move v1, v2

    .line 4272
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 4273
    iget-object v0, p0, Lcom/google/d/a/a/mi;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/lu;->d()Lcom/google/d/a/a/lu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/lu;

    invoke-virtual {v0}, Lcom/google/d/a/a/lu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4272
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 4278
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/mi;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 4282
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 4283
    iget-object v0, p0, Lcom/google/d/a/a/mi;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/kg;->d()Lcom/google/d/a/a/kg;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/kg;

    invoke-virtual {v0}, Lcom/google/d/a/a/kg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4282
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_6
    move v1, v2

    .line 4288
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 4289
    iget-object v0, p0, Lcom/google/d/a/a/mi;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/fm;->d()Lcom/google/d/a/a/fm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fm;

    invoke-virtual {v0}, Lcom/google/d/a/a/fm;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4288
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_7
    move v1, v2

    .line 4294
    :goto_7
    iget-object v0, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 4295
    iget-object v0, p0, Lcom/google/d/a/a/mi;->z:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4294
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_8
    move v1, v2

    .line 4300
    :goto_8
    iget-object v0, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 4301
    iget-object v0, p0, Lcom/google/d/a/a/mi;->A:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/dx;->d()Lcom/google/d/a/a/dx;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/dx;

    invoke-virtual {v0}, Lcom/google/d/a/a/dx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4300
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 4306
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/mi;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_c

    move v0, v3

    :goto_9
    if-eqz v0, :cond_a

    .line 4307
    iget-object v0, p0, Lcom/google/d/a/a/mi;->H:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4312
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_d

    move v0, v3

    :goto_a
    if-eqz v0, :cond_b

    .line 4313
    iget-object v0, p0, Lcom/google/d/a/a/mi;->J:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_b
    move v1, v2

    .line 4318
    :goto_b
    iget-object v0, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 4319
    iget-object v0, p0, Lcom/google/d/a/a/mi;->R:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ie;->d()Lcom/google/d/a/a/ie;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ie;

    invoke-virtual {v0}, Lcom/google/d/a/a/ie;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4318
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    :cond_c
    move v0, v2

    .line 4306
    goto :goto_9

    :cond_d
    move v0, v2

    .line 4312
    goto :goto_a

    :cond_e
    move v1, v2

    .line 4324
    :goto_c
    iget-object v0, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 4325
    iget-object v0, p0, Lcom/google/d/a/a/mi;->S:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/lq;->d()Lcom/google/d/a/a/lq;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/lq;

    invoke-virtual {v0}, Lcom/google/d/a/a/lq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4324
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 4330
    :cond_f
    iget v0, p0, Lcom/google/d/a/a/mi;->b:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_11

    move v0, v3

    :goto_d
    if-eqz v0, :cond_10

    .line 4331
    iget-object v0, p0, Lcom/google/d/a/a/mi;->U:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/es;->d()Lcom/google/d/a/a/es;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/es;

    invoke-virtual {v0}, Lcom/google/d/a/a/es;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_10
    move v2, v3

    .line 4336
    goto/16 :goto_1

    :cond_11
    move v0, v2

    .line 4330
    goto :goto_d
.end method
