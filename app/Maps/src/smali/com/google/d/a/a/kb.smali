.class public final Lcom/google/d/a/a/kb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ke;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/jz;",
        "Lcom/google/d/a/a/kb;",
        ">;",
        "Lcom/google/d/a/a/ke;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:F

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/lang/Object;

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1054
    sget-object v0, Lcom/google/d/a/a/jz;->m:Lcom/google/d/a/a/jz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1230
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/kb;->b:I

    .line 1266
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kb;->c:Lcom/google/n/ao;

    .line 1357
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/kb;->e:Ljava/lang/Object;

    .line 1466
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    .line 1634
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/kb;->i:Ljava/lang/Object;

    .line 1710
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kb;->j:Lcom/google/n/ao;

    .line 1769
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kb;->k:Lcom/google/n/ao;

    .line 1828
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/kb;->l:Lcom/google/n/ao;

    .line 1055
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/jz;)Lcom/google/d/a/a/kb;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1149
    invoke-static {}, Lcom/google/d/a/a/jz;->d()Lcom/google/d/a/a/jz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1199
    :goto_0
    return-object p0

    .line 1150
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1151
    iget v2, p1, Lcom/google/d/a/a/jz;->b:I

    invoke-static {v2}, Lcom/google/d/a/a/kc;->a(I)Lcom/google/d/a/a/kc;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/d/a/a/kc;->a:Lcom/google/d/a/a/kc;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1150
    goto :goto_1

    .line 1151
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/kb;->a:I

    iget v2, v2, Lcom/google/d/a/a/kc;->F:I

    iput v2, p0, Lcom/google/d/a/a/kb;->b:I

    .line 1153
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1154
    iget-object v2, p0, Lcom/google/d/a/a/kb;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1155
    iget v2, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/kb;->a:I

    .line 1157
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 1158
    iget v2, p1, Lcom/google/d/a/a/jz;->d:I

    iget v3, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/kb;->a:I

    iput v2, p0, Lcom/google/d/a/a/kb;->d:I

    .line 1160
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 1161
    iget v2, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/kb;->a:I

    .line 1162
    iget-object v2, p1, Lcom/google/d/a/a/jz;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/kb;->e:Ljava/lang/Object;

    .line 1165
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 1166
    iget v2, p1, Lcom/google/d/a/a/jz;->f:F

    iget v3, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/kb;->a:I

    iput v2, p0, Lcom/google/d/a/a/kb;->f:F

    .line 1168
    :cond_8
    iget-object v2, p1, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1169
    iget-object v2, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1170
    iget-object v2, p1, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    .line 1171
    iget v2, p0, Lcom/google/d/a/a/kb;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/d/a/a/kb;->a:I

    .line 1178
    :cond_9
    :goto_6
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 1179
    iget-boolean v2, p1, Lcom/google/d/a/a/jz;->h:Z

    iget v3, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/kb;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/kb;->h:Z

    .line 1181
    :cond_a
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_b

    .line 1182
    iget v2, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/d/a/a/kb;->a:I

    .line 1183
    iget-object v2, p1, Lcom/google/d/a/a/jz;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/kb;->i:Ljava/lang/Object;

    .line 1186
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_c

    .line 1187
    iget-object v2, p0, Lcom/google/d/a/a/kb;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1188
    iget v2, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/d/a/a/kb;->a:I

    .line 1190
    :cond_c
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_a
    if-eqz v2, :cond_d

    .line 1191
    iget-object v2, p0, Lcom/google/d/a/a/kb;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/jz;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1192
    iget v2, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/d/a/a/kb;->a:I

    .line 1194
    :cond_d
    iget v2, p1, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_19

    :goto_b
    if-eqz v0, :cond_e

    .line 1195
    iget-object v0, p0, Lcom/google/d/a/a/kb;->l:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/jz;->l:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1196
    iget v0, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/d/a/a/kb;->a:I

    .line 1198
    :cond_e
    iget-object v0, p1, Lcom/google/d/a/a/jz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v2, v1

    .line 1153
    goto/16 :goto_2

    :cond_10
    move v2, v1

    .line 1157
    goto/16 :goto_3

    :cond_11
    move v2, v1

    .line 1160
    goto/16 :goto_4

    :cond_12
    move v2, v1

    .line 1165
    goto/16 :goto_5

    .line 1173
    :cond_13
    iget v2, p0, Lcom/google/d/a/a/kb;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/kb;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/kb;->a:I

    .line 1174
    :cond_14
    iget-object v2, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 1178
    goto/16 :goto_7

    :cond_16
    move v2, v1

    .line 1181
    goto/16 :goto_8

    :cond_17
    move v2, v1

    .line 1186
    goto :goto_9

    :cond_18
    move v2, v1

    .line 1190
    goto :goto_a

    :cond_19
    move v0, v1

    .line 1194
    goto :goto_b
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1046
    new-instance v2, Lcom/google/d/a/a/jz;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/jz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/kb;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/kb;->b:I

    iput v4, v2, Lcom/google/d/a/a/jz;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/kb;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/kb;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/kb;->d:I

    iput v4, v2, Lcom/google/d/a/a/jz;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/d/a/a/kb;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/jz;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/d/a/a/kb;->f:F

    iput v4, v2, Lcom/google/d/a/a/jz;->f:F

    iget v4, p0, Lcom/google/d/a/a/kb;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/kb;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/d/a/a/kb;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-boolean v4, p0, Lcom/google/d/a/a/kb;->h:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/jz;->h:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v4, p0, Lcom/google/d/a/a/kb;->i:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/jz;->i:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v4, v2, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/kb;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/kb;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v4, v2, Lcom/google/d/a/a/jz;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/kb;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/kb;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v3, v2, Lcom/google/d/a/a/jz;->l:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/kb;->l:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/kb;->l:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/jz;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1046
    check-cast p1, Lcom/google/d/a/a/jz;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/kb;->a(Lcom/google/d/a/a/jz;)Lcom/google/d/a/a/kb;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1203
    iget v0, p0, Lcom/google/d/a/a/kb;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 1225
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1203
    goto :goto_0

    .line 1207
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/kb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1208
    iget-object v0, p0, Lcom/google/d/a/a/kb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 1213
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1214
    iget-object v0, p0, Lcom/google/d/a/a/kb;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1213
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 1207
    goto :goto_2

    .line 1219
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/kb;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    move v0, v3

    :goto_4
    if-eqz v0, :cond_6

    .line 1220
    iget-object v0, p0, Lcom/google/d/a/a/kb;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_6
    move v2, v3

    .line 1225
    goto :goto_1

    :cond_7
    move v0, v2

    .line 1219
    goto :goto_4
.end method
