.class public final Lcom/google/d/a/a/ni;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/nl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ni;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/d/a/a/ni;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/lang/Object;

.field e:I

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:J

.field j:J

.field k:Lcom/google/n/ao;

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field m:Lcom/google/n/ao;

.field n:Lcom/google/n/ao;

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 308
    new-instance v0, Lcom/google/d/a/a/nj;

    invoke-direct {v0}, Lcom/google/d/a/a/nj;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ni;->PARSER:Lcom/google/n/ax;

    .line 845
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/ni;->r:Lcom/google/n/aw;

    .line 2061
    new-instance v0, Lcom/google/d/a/a/ni;

    invoke-direct {v0}, Lcom/google/d/a/a/ni;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ni;->o:Lcom/google/d/a/a/ni;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 173
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 325
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    .line 597
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    .line 656
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    .line 672
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    .line 687
    iput-byte v3, p0, Lcom/google/d/a/a/ni;->p:B

    .line 780
    iput v3, p0, Lcom/google/d/a/a/ni;->q:I

    .line 174
    iget-object v0, p0, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 175
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    .line 176
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ni;->d:Ljava/lang/Object;

    .line 177
    const/16 v0, 0x1111

    iput v0, p0, Lcom/google/d/a/a/ni;->e:I

    .line 178
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ni;->f:Ljava/lang/Object;

    .line 179
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ni;->g:Ljava/lang/Object;

    .line 180
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ni;->h:Ljava/lang/Object;

    .line 181
    iput-wide v4, p0, Lcom/google/d/a/a/ni;->i:J

    .line 182
    iput-wide v4, p0, Lcom/google/d/a/a/ni;->j:J

    .line 183
    iget-object v0, p0, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 184
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    .line 185
    iget-object v0, p0, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 186
    iget-object v0, p0, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 187
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v8, 0x400

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 193
    invoke-direct {p0}, Lcom/google/d/a/a/ni;-><init>()V

    .line 196
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 199
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 200
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 201
    sparse-switch v4, :sswitch_data_0

    .line 206
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 208
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 204
    goto :goto_0

    .line 213
    :sswitch_1
    iget-object v4, p0, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 214
    iget v4, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/ni;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 293
    :catch_0
    move-exception v0

    .line 294
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 299
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 300
    iget-object v2, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    .line 302
    :cond_1
    and-int/lit16 v1, v1, 0x400

    if-ne v1, v8, :cond_2

    .line 303
    iget-object v1, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    .line 305
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ni;->au:Lcom/google/n/bn;

    throw v0

    .line 218
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_3

    .line 219
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    .line 221
    or-int/lit8 v1, v1, 0x2

    .line 223
    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 224
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 223
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 295
    :catch_1
    move-exception v0

    .line 296
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 297
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 228
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 229
    iget v5, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/d/a/a/ni;->a:I

    .line 230
    iput-object v4, p0, Lcom/google/d/a/a/ni;->d:Ljava/lang/Object;

    goto :goto_0

    .line 234
    :sswitch_4
    iget v4, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/ni;->a:I

    .line 235
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/ni;->e:I

    goto/16 :goto_0

    .line 239
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 240
    iget v5, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/d/a/a/ni;->a:I

    .line 241
    iput-object v4, p0, Lcom/google/d/a/a/ni;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 245
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 246
    iget v5, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/d/a/a/ni;->a:I

    .line 247
    iput-object v4, p0, Lcom/google/d/a/a/ni;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 251
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 252
    iget v5, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/d/a/a/ni;->a:I

    .line 253
    iput-object v4, p0, Lcom/google/d/a/a/ni;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 257
    :sswitch_8
    iget v4, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/d/a/a/ni;->a:I

    .line 258
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/ni;->i:J

    goto/16 :goto_0

    .line 262
    :sswitch_9
    iget v4, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/d/a/a/ni;->a:I

    .line 263
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/ni;->j:J

    goto/16 :goto_0

    .line 267
    :sswitch_a
    iget-object v4, p0, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 268
    iget v4, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/d/a/a/ni;->a:I

    goto/16 :goto_0

    .line 272
    :sswitch_b
    iget-object v4, p0, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 273
    iget v4, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/d/a/a/ni;->a:I

    goto/16 :goto_0

    .line 277
    :sswitch_c
    and-int/lit16 v4, v1, 0x400

    if-eq v4, v8, :cond_4

    .line 278
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    .line 280
    or-int/lit16 v1, v1, 0x400

    .line 282
    :cond_4
    iget-object v4, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 283
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 282
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 287
    :sswitch_d
    iget-object v4, p0, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 288
    iget v4, p0, Lcom/google/d/a/a/ni;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/d/a/a/ni;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 299
    :cond_5
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_6

    .line 300
    iget-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    .line 302
    :cond_6
    and-int/lit16 v0, v1, 0x400

    if-ne v0, v8, :cond_7

    .line 303
    iget-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    .line 305
    :cond_7
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->au:Lcom/google/n/bn;

    .line 306
    return-void

    .line 201
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 171
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 325
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    .line 597
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    .line 656
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    .line 672
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    .line 687
    iput-byte v1, p0, Lcom/google/d/a/a/ni;->p:B

    .line 780
    iput v1, p0, Lcom/google/d/a/a/ni;->q:I

    .line 172
    return-void
.end method

.method public static d()Lcom/google/d/a/a/ni;
    .locals 1

    .prologue
    .line 2064
    sget-object v0, Lcom/google/d/a/a/ni;->o:Lcom/google/d/a/a/ni;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/nk;
    .locals 1

    .prologue
    .line 907
    new-instance v0, Lcom/google/d/a/a/nk;

    invoke-direct {v0}, Lcom/google/d/a/a/nk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ni;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    sget-object v0, Lcom/google/d/a/a/ni;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 737
    invoke-virtual {p0}, Lcom/google/d/a/a/ni;->c()I

    .line 738
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 739
    iget-object v0, p0, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 741
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 742
    iget-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 741
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 744
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    .line 745
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/ni;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 747
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 748
    iget v0, p0, Lcom/google/d/a/a/ni;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_c

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 750
    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_4

    .line 751
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/ni;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 753
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 754
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/ni;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 756
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 757
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/d/a/a/ni;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->h:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 759
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 760
    iget-wide v0, p0, Lcom/google/d/a/a/ni;->i:J

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 762
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 763
    const/16 v0, 0x9

    iget-wide v4, p0, Lcom/google/d/a/a/ni;->j:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 765
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 766
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 768
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 769
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 771
    :cond_a
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_10

    .line 772
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 771
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 745
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 748
    :cond_c
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 751
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 754
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 757
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 774
    :cond_10
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_11

    .line 775
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 777
    :cond_11
    iget-object v0, p0, Lcom/google/d/a/a/ni;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 778
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 689
    iget-byte v0, p0, Lcom/google/d/a/a/ni;->p:B

    .line 690
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 732
    :cond_0
    :goto_0
    return v2

    .line 691
    :cond_1
    if-eqz v0, :cond_0

    .line 693
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    .line 694
    iput-byte v2, p0, Lcom/google/d/a/a/ni;->p:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 693
    goto :goto_1

    .line 697
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 698
    iput-byte v2, p0, Lcom/google/d/a/a/ni;->p:B

    goto :goto_0

    :cond_4
    move v1, v2

    .line 701
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 702
    iget-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ji;->d()Lcom/google/d/a/a/ji;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ji;

    invoke-virtual {v0}, Lcom/google/d/a/a/ji;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 703
    iput-byte v2, p0, Lcom/google/d/a/a/ni;->p:B

    goto :goto_0

    .line 701
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 707
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    move v0, v3

    :goto_3
    if-eqz v0, :cond_8

    .line 708
    iget-object v0, p0, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 709
    iput-byte v2, p0, Lcom/google/d/a/a/ni;->p:B

    goto :goto_0

    :cond_7
    move v0, v2

    .line 707
    goto :goto_3

    :cond_8
    move v1, v2

    .line 713
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 714
    iget-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 715
    iput-byte v2, p0, Lcom/google/d/a/a/ni;->p:B

    goto/16 :goto_0

    .line 713
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 719
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    move v0, v3

    :goto_5
    if-eqz v0, :cond_c

    .line 720
    iget-object v0, p0, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_c

    .line 721
    iput-byte v2, p0, Lcom/google/d/a/a/ni;->p:B

    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 719
    goto :goto_5

    .line 725
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_d

    move v0, v3

    :goto_6
    if-eqz v0, :cond_e

    .line 726
    iget-object v0, p0, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/a/o;->d()Lcom/google/d/a/a/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/a/o;

    invoke-virtual {v0}, Lcom/google/d/a/a/a/o;->b()Z

    move-result v0

    if-nez v0, :cond_e

    .line 727
    iput-byte v2, p0, Lcom/google/d/a/a/ni;->p:B

    goto/16 :goto_0

    :cond_d
    move v0, v2

    .line 725
    goto :goto_6

    .line 731
    :cond_e
    iput-byte v3, p0, Lcom/google/d/a/a/ni;->p:B

    move v2, v3

    .line 732
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 782
    iget v0, p0, Lcom/google/d/a/a/ni;->q:I

    .line 783
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 840
    :goto_0
    return v0

    .line 786
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_12

    .line 787
    iget-object v0, p0, Lcom/google/d/a/a/ni;->b:Lcom/google/n/ao;

    .line 788
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 790
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 791
    iget-object v0, p0, Lcom/google/d/a/a/ni;->c:Ljava/util/List;

    .line 792
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 790
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 794
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 795
    const/4 v2, 0x3

    .line 796
    iget-object v0, p0, Lcom/google/d/a/a/ni;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 798
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 799
    iget v0, p0, Lcom/google/d/a/a/ni;->e:I

    .line 800
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_c

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 802
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_4

    .line 803
    const/4 v2, 0x5

    .line 804
    iget-object v0, p0, Lcom/google/d/a/a/ni;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 806
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_5

    .line 807
    const/4 v2, 0x6

    .line 808
    iget-object v0, p0, Lcom/google/d/a/a/ni;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 810
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_6

    .line 811
    const/4 v2, 0x7

    .line 812
    iget-object v0, p0, Lcom/google/d/a/a/ni;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ni;->h:Ljava/lang/Object;

    :goto_7
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 814
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_7

    .line 815
    iget-wide v4, p0, Lcom/google/d/a/a/ni;->i:J

    .line 816
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 818
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_8

    .line 819
    const/16 v0, 0x9

    iget-wide v4, p0, Lcom/google/d/a/a/ni;->j:J

    .line 820
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 822
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_9

    .line 823
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/d/a/a/ni;->k:Lcom/google/n/ao;

    .line 824
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 826
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_a

    .line 827
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/d/a/a/ni;->m:Lcom/google/n/ao;

    .line 828
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_a
    move v2, v1

    .line 830
    :goto_8
    iget-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_10

    .line 831
    const/16 v4, 0xc

    iget-object v0, p0, Lcom/google/d/a/a/ni;->l:Ljava/util/List;

    .line 832
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 830
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 796
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 800
    :cond_c
    const/16 v0, 0xa

    goto/16 :goto_4

    .line 804
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 808
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 812
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 834
    :cond_10
    iget v0, p0, Lcom/google/d/a/a/ni;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_11

    .line 835
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/google/d/a/a/ni;->n:Lcom/google/n/ao;

    .line 836
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 838
    :cond_11
    iget-object v0, p0, Lcom/google/d/a/a/ni;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 839
    iput v0, p0, Lcom/google/d/a/a/ni;->q:I

    goto/16 :goto_0

    :cond_12
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Lcom/google/d/a/a/ni;->newBuilder()Lcom/google/d/a/a/nk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/nk;->a(Lcom/google/d/a/a/ni;)Lcom/google/d/a/a/nk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Lcom/google/d/a/a/ni;->newBuilder()Lcom/google/d/a/a/nk;

    move-result-object v0

    return-object v0
.end method
