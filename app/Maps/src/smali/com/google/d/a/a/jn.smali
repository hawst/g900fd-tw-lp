.class public final Lcom/google/d/a/a/jn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/js;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/jn;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/d/a/a/jn;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/google/d/a/a/jo;

    invoke-direct {v0}, Lcom/google/d/a/a/jo;-><init>()V

    sput-object v0, Lcom/google/d/a/a/jn;->PARSER:Lcom/google/n/ax;

    .line 441
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/jn;->i:Lcom/google/n/aw;

    .line 874
    new-instance v0, Lcom/google/d/a/a/jn;

    invoke-direct {v0}, Lcom/google/d/a/a/jn;-><init>()V

    sput-object v0, Lcom/google/d/a/a/jn;->f:Lcom/google/d/a/a/jn;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 77
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 372
    iput-byte v0, p0, Lcom/google/d/a/a/jn;->g:B

    .line 412
    iput v0, p0, Lcom/google/d/a/a/jn;->h:I

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jn;->b:Ljava/lang/Object;

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jn;->c:Ljava/lang/Object;

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jn;->d:Ljava/lang/Object;

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/jn;->e:I

    .line 82
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 88
    invoke-direct {p0}, Lcom/google/d/a/a/jn;-><init>()V

    .line 89
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 93
    const/4 v0, 0x0

    .line 94
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 95
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 96
    sparse-switch v3, :sswitch_data_0

    .line 101
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 103
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 99
    goto :goto_0

    .line 108
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 109
    iget v4, p0, Lcom/google/d/a/a/jn;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/jn;->a:I

    .line 110
    iput-object v3, p0, Lcom/google/d/a/a/jn;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/jn;->au:Lcom/google/n/bn;

    throw v0

    .line 114
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 115
    iget v4, p0, Lcom/google/d/a/a/jn;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/jn;->a:I

    .line 116
    iput-object v3, p0, Lcom/google/d/a/a/jn;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 140
    :catch_1
    move-exception v0

    .line 141
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 142
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 120
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 121
    iget v4, p0, Lcom/google/d/a/a/jn;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/jn;->a:I

    .line 122
    iput-object v3, p0, Lcom/google/d/a/a/jn;->d:Ljava/lang/Object;

    goto :goto_0

    .line 126
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 127
    invoke-static {v3}, Lcom/google/d/a/a/jq;->a(I)Lcom/google/d/a/a/jq;

    move-result-object v4

    .line 128
    if-nez v4, :cond_1

    .line 129
    const/4 v4, 0x4

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 131
    :cond_1
    iget v4, p0, Lcom/google/d/a/a/jn;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/a/a/jn;->a:I

    .line 132
    iput v3, p0, Lcom/google/d/a/a/jn;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 144
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jn;->au:Lcom/google/n/bn;

    .line 145
    return-void

    .line 96
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 75
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 372
    iput-byte v0, p0, Lcom/google/d/a/a/jn;->g:B

    .line 412
    iput v0, p0, Lcom/google/d/a/a/jn;->h:I

    .line 76
    return-void
.end method

.method public static d()Lcom/google/d/a/a/jn;
    .locals 1

    .prologue
    .line 877
    sget-object v0, Lcom/google/d/a/a/jn;->f:Lcom/google/d/a/a/jn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/jp;
    .locals 1

    .prologue
    .line 503
    new-instance v0, Lcom/google/d/a/a/jp;

    invoke-direct {v0}, Lcom/google/d/a/a/jp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/jn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lcom/google/d/a/a/jn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 396
    invoke-virtual {p0}, Lcom/google/d/a/a/jn;->c()I

    .line 397
    iget v0, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 398
    iget-object v0, p0, Lcom/google/d/a/a/jn;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jn;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 400
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 401
    iget-object v0, p0, Lcom/google/d/a/a/jn;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jn;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 403
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 404
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/jn;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jn;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 406
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 407
    iget v0, p0, Lcom/google/d/a/a/jn;->e:I

    const/4 v1, 0x0

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 409
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/jn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 410
    return-void

    .line 398
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 401
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 404
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 407
    :cond_7
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 374
    iget-byte v2, p0, Lcom/google/d/a/a/jn;->g:B

    .line 375
    if-ne v2, v0, :cond_0

    .line 391
    :goto_0
    return v0

    .line 376
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 378
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 379
    iput-byte v1, p0, Lcom/google/d/a/a/jn;->g:B

    move v0, v1

    .line 380
    goto :goto_0

    :cond_2
    move v2, v1

    .line 378
    goto :goto_1

    .line 382
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 383
    iput-byte v1, p0, Lcom/google/d/a/a/jn;->g:B

    move v0, v1

    .line 384
    goto :goto_0

    :cond_4
    move v2, v1

    .line 382
    goto :goto_2

    .line 386
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-nez v2, :cond_7

    .line 387
    iput-byte v1, p0, Lcom/google/d/a/a/jn;->g:B

    move v0, v1

    .line 388
    goto :goto_0

    :cond_6
    move v2, v1

    .line 386
    goto :goto_3

    .line 390
    :cond_7
    iput-byte v0, p0, Lcom/google/d/a/a/jn;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 414
    iget v0, p0, Lcom/google/d/a/a/jn;->h:I

    .line 415
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 436
    :goto_0
    return v0

    .line 418
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 420
    iget-object v0, p0, Lcom/google/d/a/a/jn;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jn;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 422
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 424
    iget-object v0, p0, Lcom/google/d/a/a/jn;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jn;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 426
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 427
    const/4 v3, 0x3

    .line 428
    iget-object v0, p0, Lcom/google/d/a/a/jn;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jn;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 430
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/jn;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 431
    iget v0, p0, Lcom/google/d/a/a/jn;->e:I

    .line 432
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 434
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/jn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 435
    iput v0, p0, Lcom/google/d/a/a/jn;->h:I

    goto/16 :goto_0

    .line 420
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 424
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 428
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 432
    :cond_7
    const/16 v0, 0xa

    goto :goto_5

    :cond_8
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 69
    invoke-static {}, Lcom/google/d/a/a/jn;->newBuilder()Lcom/google/d/a/a/jp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/jp;->a(Lcom/google/d/a/a/jn;)Lcom/google/d/a/a/jp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 69
    invoke-static {}, Lcom/google/d/a/a/jn;->newBuilder()Lcom/google/d/a/a/jp;

    move-result-object v0

    return-object v0
.end method
