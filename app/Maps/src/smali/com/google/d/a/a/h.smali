.class public final enum Lcom/google/d/a/a/h;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/h;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/h;

.field public static final enum b:Lcom/google/d/a/a/h;

.field public static final enum c:Lcom/google/d/a/a/h;

.field public static final enum d:Lcom/google/d/a/a/h;

.field private static final synthetic f:[Lcom/google/d/a/a/h;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 378
    new-instance v0, Lcom/google/d/a/a/h;

    const-string v1, "TRAVEL_MODE_MOTOR_VEHICLE"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/h;->a:Lcom/google/d/a/a/h;

    .line 382
    new-instance v0, Lcom/google/d/a/a/h;

    const-string v1, "TRAVEL_MODE_BICYCLE"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/h;->b:Lcom/google/d/a/a/h;

    .line 386
    new-instance v0, Lcom/google/d/a/a/h;

    const-string v1, "TRAVEL_MODE_PEDESTRIAN"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/d/a/a/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/h;->c:Lcom/google/d/a/a/h;

    .line 390
    new-instance v0, Lcom/google/d/a/a/h;

    const-string v1, "TRAVEL_MODE_PUBLIC_TRANSIT"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/d/a/a/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/h;->d:Lcom/google/d/a/a/h;

    .line 373
    new-array v0, v6, [Lcom/google/d/a/a/h;

    sget-object v1, Lcom/google/d/a/a/h;->a:Lcom/google/d/a/a/h;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/h;->b:Lcom/google/d/a/a/h;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/h;->c:Lcom/google/d/a/a/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/h;->d:Lcom/google/d/a/a/h;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/d/a/a/h;->f:[Lcom/google/d/a/a/h;

    .line 430
    new-instance v0, Lcom/google/d/a/a/i;

    invoke-direct {v0}, Lcom/google/d/a/a/i;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 439
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 440
    iput p3, p0, Lcom/google/d/a/a/h;->e:I

    .line 441
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/h;
    .locals 1

    .prologue
    .line 416
    packed-switch p0, :pswitch_data_0

    .line 421
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 417
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/h;->a:Lcom/google/d/a/a/h;

    goto :goto_0

    .line 418
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/h;->b:Lcom/google/d/a/a/h;

    goto :goto_0

    .line 419
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/h;->c:Lcom/google/d/a/a/h;

    goto :goto_0

    .line 420
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/h;->d:Lcom/google/d/a/a/h;

    goto :goto_0

    .line 416
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/h;
    .locals 1

    .prologue
    .line 373
    const-class v0, Lcom/google/d/a/a/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/h;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/h;
    .locals 1

    .prologue
    .line 373
    sget-object v0, Lcom/google/d/a/a/h;->f:[Lcom/google/d/a/a/h;

    invoke-virtual {v0}, [Lcom/google/d/a/a/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/h;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 412
    iget v0, p0, Lcom/google/d/a/a/h;->e:I

    return v0
.end method
