.class public final Lcom/google/d/a/a/ri;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/rl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ri;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/d/a/a/ri;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/n/ao;

.field e:I

.field f:I

.field g:Z

.field h:I

.field i:Z

.field j:Z

.field k:Z

.field l:Z

.field m:Z

.field n:I

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 284
    new-instance v0, Lcom/google/d/a/a/rj;

    invoke-direct {v0}, Lcom/google/d/a/a/rj;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ri;->PARSER:Lcom/google/n/ax;

    .line 690
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/ri;->r:Lcom/google/n/aw;

    .line 1612
    new-instance v0, Lcom/google/d/a/a/ri;

    invoke-direct {v0}, Lcom/google/d/a/a/ri;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ri;->o:Lcom/google/d/a/a/ri;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 153
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 387
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    .line 552
    iput-byte v4, p0, Lcom/google/d/a/a/ri;->p:B

    .line 625
    iput v4, p0, Lcom/google/d/a/a/ri;->q:I

    .line 154
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    .line 155
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    .line 156
    iget-object v0, p0, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 157
    iput v1, p0, Lcom/google/d/a/a/ri;->e:I

    .line 158
    iput v1, p0, Lcom/google/d/a/a/ri;->f:I

    .line 159
    iput-boolean v1, p0, Lcom/google/d/a/a/ri;->g:Z

    .line 160
    iput v1, p0, Lcom/google/d/a/a/ri;->h:I

    .line 161
    iput-boolean v2, p0, Lcom/google/d/a/a/ri;->i:Z

    .line 162
    iput-boolean v2, p0, Lcom/google/d/a/a/ri;->j:Z

    .line 163
    iput-boolean v1, p0, Lcom/google/d/a/a/ri;->k:Z

    .line 164
    iput-boolean v1, p0, Lcom/google/d/a/a/ri;->l:Z

    .line 165
    iput-boolean v1, p0, Lcom/google/d/a/a/ri;->m:Z

    .line 166
    iput v1, p0, Lcom/google/d/a/a/ri;->n:I

    .line 167
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 173
    invoke-direct {p0}, Lcom/google/d/a/a/ri;-><init>()V

    .line 176
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 179
    :cond_0
    :goto_0
    if-nez v4, :cond_b

    .line 180
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 181
    sparse-switch v0, :sswitch_data_0

    .line 186
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 188
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 184
    goto :goto_0

    .line 193
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v2, :cond_1

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    .line 196
    or-int/lit8 v1, v1, 0x1

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 199
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 198
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v0

    .line 270
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    :catchall_0
    move-exception v0

    and-int/lit8 v3, v1, 0x1

    if-ne v3, v2, :cond_2

    .line 276
    iget-object v2, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    .line 278
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v10, :cond_3

    .line 279
    iget-object v1, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    .line 281
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ri;->au:Lcom/google/n/bn;

    throw v0

    .line 203
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v10, :cond_4

    .line 204
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    .line 206
    or-int/lit8 v1, v1, 0x2

    .line 208
    :cond_4
    iget-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 209
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 208
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 271
    :catch_1
    move-exception v0

    .line 272
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 273
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 213
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 214
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    goto/16 :goto_0

    .line 218
    :sswitch_4
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 219
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/ri;->e:I

    goto/16 :goto_0

    .line 223
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 224
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/ri;->f:I

    goto/16 :goto_0

    .line 228
    :sswitch_6
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 229
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/ri;->g:Z

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    .line 233
    :sswitch_7
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 234
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/ri;->h:I

    goto/16 :goto_0

    .line 238
    :sswitch_8
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 239
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/d/a/a/ri;->i:Z

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto :goto_2

    .line 243
    :sswitch_9
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 244
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_7

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/d/a/a/ri;->j:Z

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto :goto_3

    .line 248
    :sswitch_a
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 249
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    move v0, v2

    :goto_4
    iput-boolean v0, p0, Lcom/google/d/a/a/ri;->k:Z

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_4

    .line 253
    :sswitch_b
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 254
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_9

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lcom/google/d/a/a/ri;->l:Z

    goto/16 :goto_0

    :cond_9
    move v0, v3

    goto :goto_5

    .line 258
    :sswitch_c
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 259
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_a

    move v0, v2

    :goto_6
    iput-boolean v0, p0, Lcom/google/d/a/a/ri;->m:Z

    goto/16 :goto_0

    :cond_a
    move v0, v3

    goto :goto_6

    .line 263
    :sswitch_d
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/d/a/a/ri;->a:I

    .line 264
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/ri;->n:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 275
    :cond_b
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_c

    .line 276
    iget-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    .line 278
    :cond_c
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v10, :cond_d

    .line 279
    iget-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    .line 281
    :cond_d
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ri;->au:Lcom/google/n/bn;

    .line 282
    return-void

    .line 181
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 151
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 387
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    .line 552
    iput-byte v1, p0, Lcom/google/d/a/a/ri;->p:B

    .line 625
    iput v1, p0, Lcom/google/d/a/a/ri;->q:I

    .line 152
    return-void
.end method

.method public static d()Lcom/google/d/a/a/ri;
    .locals 1

    .prologue
    .line 1615
    sget-object v0, Lcom/google/d/a/a/ri;->o:Lcom/google/d/a/a/ri;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/rk;
    .locals 1

    .prologue
    .line 752
    new-instance v0, Lcom/google/d/a/a/rk;

    invoke-direct {v0}, Lcom/google/d/a/a/rk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    sget-object v0, Lcom/google/d/a/a/ri;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 582
    invoke-virtual {p0}, Lcom/google/d/a/a/ri;->c()I

    move v1, v2

    .line 583
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 583
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 586
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 587
    iget-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 586
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 589
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 590
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 592
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_3

    .line 593
    iget v0, p0, Lcom/google/d/a/a/ri;->e:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_d

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 595
    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_4

    .line 596
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/d/a/a/ri;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_e

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 598
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_5

    .line 599
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_f

    move v0, v3

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 601
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 602
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/d/a/a/ri;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_10

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 604
    :cond_6
    :goto_5
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 605
    iget-boolean v0, p0, Lcom/google/d/a/a/ri;->i:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_11

    move v0, v3

    :goto_6
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 607
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 608
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->j:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_12

    move v0, v3

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 610
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 611
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->k:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_13

    move v0, v3

    :goto_8
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 613
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 614
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->l:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_14

    move v0, v3

    :goto_9
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 616
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    .line 617
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->m:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_15

    :goto_a
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 619
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 620
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/d/a/a/ri;->n:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_16

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 622
    :cond_c
    :goto_b
    iget-object v0, p0, Lcom/google/d/a/a/ri;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 623
    return-void

    .line 593
    :cond_d
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 596
    :cond_e
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    :cond_f
    move v0, v2

    .line 599
    goto/16 :goto_4

    .line 602
    :cond_10
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_5

    :cond_11
    move v0, v2

    .line 605
    goto/16 :goto_6

    :cond_12
    move v0, v2

    .line 608
    goto/16 :goto_7

    :cond_13
    move v0, v2

    .line 611
    goto :goto_8

    :cond_14
    move v0, v2

    .line 614
    goto :goto_9

    :cond_15
    move v3, v2

    .line 617
    goto :goto_a

    .line 620
    :cond_16
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_b
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 554
    iget-byte v0, p0, Lcom/google/d/a/a/ri;->p:B

    .line 555
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 577
    :cond_0
    :goto_0
    return v2

    .line 556
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 558
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 559
    iget-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 560
    iput-byte v2, p0, Lcom/google/d/a/a/ri;->p:B

    goto :goto_0

    .line 558
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 564
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 565
    iget-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 566
    iput-byte v2, p0, Lcom/google/d/a/a/ri;->p:B

    goto :goto_0

    .line 564
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 570
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 571
    iget-object v0, p0, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 572
    iput-byte v2, p0, Lcom/google/d/a/a/ri;->p:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 570
    goto :goto_3

    .line 576
    :cond_7
    iput-byte v3, p0, Lcom/google/d/a/a/ri;->p:B

    move v2, v3

    .line 577
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 627
    iget v0, p0, Lcom/google/d/a/a/ri;->q:I

    .line 628
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 685
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 631
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 632
    iget-object v0, p0, Lcom/google/d/a/a/ri;->b:Ljava/util/List;

    .line 633
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 631
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 635
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 636
    iget-object v0, p0, Lcom/google/d/a/a/ri;->c:Ljava/util/List;

    .line 637
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 635
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 639
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_3

    .line 640
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/d/a/a/ri;->d:Lcom/google/n/ao;

    .line 641
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 643
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_4

    .line 644
    iget v0, p0, Lcom/google/d/a/a/ri;->e:I

    .line 645
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_f

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 647
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_5

    .line 648
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/d/a/a/ri;->f:I

    .line 649
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_10

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 651
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 652
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->g:Z

    .line 653
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 655
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 656
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/d/a/a/ri;->h:I

    .line 657
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_11

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 659
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 660
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->i:Z

    .line 661
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 663
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 664
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->j:Z

    .line 665
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 667
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 668
    iget-boolean v0, p0, Lcom/google/d/a/a/ri;->k:Z

    .line 669
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 671
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 672
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->l:Z

    .line 673
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 675
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_c

    .line 676
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/d/a/a/ri;->m:Z

    .line 677
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 679
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/ri;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 680
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/d/a/a/ri;->n:I

    .line 681
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_d

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_d
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 683
    :cond_e
    iget-object v0, p0, Lcom/google/d/a/a/ri;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 684
    iput v0, p0, Lcom/google/d/a/a/ri;->q:I

    goto/16 :goto_0

    :cond_f
    move v0, v4

    .line 645
    goto/16 :goto_3

    :cond_10
    move v0, v4

    .line 649
    goto/16 :goto_4

    :cond_11
    move v0, v4

    .line 657
    goto/16 :goto_5
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 145
    invoke-static {}, Lcom/google/d/a/a/ri;->newBuilder()Lcom/google/d/a/a/rk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/rk;->a(Lcom/google/d/a/a/ri;)Lcom/google/d/a/a/rk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 145
    invoke-static {}, Lcom/google/d/a/a/ri;->newBuilder()Lcom/google/d/a/a/rk;

    move-result-object v0

    return-object v0
.end method
