.class public final Lcom/google/d/a/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/j;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final n:Lcom/google/d/a/a/b;

.field private static volatile q:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:I

.field e:F

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Z

.field i:Z

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field k:I

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field m:Lcom/google/n/ao;

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 297
    new-instance v0, Lcom/google/d/a/a/c;

    invoke-direct {v0}, Lcom/google/d/a/a/c;-><init>()V

    sput-object v0, Lcom/google/d/a/a/b;->PARSER:Lcom/google/n/ax;

    .line 633
    new-instance v0, Lcom/google/d/a/a/d;

    invoke-direct {v0}, Lcom/google/d/a/a/d;-><init>()V

    .line 825
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/b;->q:Lcom/google/n/aw;

    .line 1779
    new-instance v0, Lcom/google/d/a/a/b;

    invoke-direct {v0}, Lcom/google/d/a/a/b;-><init>()V

    sput-object v0, Lcom/google/d/a/a/b;->n:Lcom/google/d/a/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 143
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 448
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    .line 464
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    .line 510
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    .line 526
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    .line 662
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->m:Lcom/google/n/ao;

    .line 677
    iput-byte v4, p0, Lcom/google/d/a/a/b;->o:B

    .line 759
    iput v4, p0, Lcom/google/d/a/a/b;->p:I

    .line 144
    iget-object v0, p0, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 145
    iget-object v0, p0, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 146
    iput v3, p0, Lcom/google/d/a/a/b;->d:I

    .line 147
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/b;->e:F

    .line 148
    iget-object v0, p0, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 149
    iget-object v0, p0, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 150
    iput-boolean v3, p0, Lcom/google/d/a/a/b;->h:Z

    .line 151
    iput-boolean v3, p0, Lcom/google/d/a/a/b;->i:Z

    .line 152
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    .line 153
    iput v2, p0, Lcom/google/d/a/a/b;->k:I

    .line 154
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    .line 155
    iget-object v0, p0, Lcom/google/d/a/a/b;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 156
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v11, 0x100

    const/16 v10, 0x400

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 162
    invoke-direct {p0}, Lcom/google/d/a/a/b;-><init>()V

    .line 165
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 168
    :cond_0
    :goto_0
    if-nez v4, :cond_d

    .line 169
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 170
    sparse-switch v0, :sswitch_data_0

    .line 175
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 177
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 173
    goto :goto_0

    .line 182
    :sswitch_1
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/b;->a:I

    .line 183
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/b;->e:F
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 282
    :catch_0
    move-exception v0

    .line 283
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    :catchall_0
    move-exception v0

    and-int/lit16 v2, v1, 0x100

    if-ne v2, v11, :cond_1

    .line 289
    iget-object v2, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    .line 291
    :cond_1
    and-int/lit16 v1, v1, 0x400

    if-ne v1, v10, :cond_2

    .line 292
    iget-object v1, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    .line 294
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 187
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 188
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/b;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 284
    :catch_1
    move-exception v0

    .line 285
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 286
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 192
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 193
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/b;->a:I

    goto :goto_0

    .line 197
    :sswitch_4
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/b;->a:I

    .line 198
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/b;->h:Z

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 202
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/b;->a:I

    .line 203
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/d/a/a/b;->i:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_2

    .line 207
    :sswitch_6
    and-int/lit16 v0, v1, 0x100

    if-eq v0, v11, :cond_5

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    .line 210
    or-int/lit16 v1, v1, 0x100

    .line 212
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 213
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 212
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 217
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 218
    invoke-static {v0}, Lcom/google/d/a/a/f;->a(I)Lcom/google/d/a/a/f;

    move-result-object v6

    .line 219
    if-nez v6, :cond_6

    .line 220
    const/16 v6, 0x8

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 222
    :cond_6
    iget v6, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit16 v6, v6, 0x100

    iput v6, p0, Lcom/google/d/a/a/b;->a:I

    .line 223
    iput v0, p0, Lcom/google/d/a/a/b;->k:I

    goto/16 :goto_0

    .line 228
    :sswitch_8
    iget-object v0, p0, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 229
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/b;->a:I

    goto/16 :goto_0

    .line 233
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 234
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/b;->a:I

    goto/16 :goto_0

    .line 238
    :sswitch_a
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/b;->a:I

    .line 239
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/b;->d:I

    goto/16 :goto_0

    .line 243
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 244
    invoke-static {v0}, Lcom/google/d/a/a/h;->a(I)Lcom/google/d/a/a/h;

    move-result-object v6

    .line 245
    if-nez v6, :cond_7

    .line 246
    const/16 v6, 0xc

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 248
    :cond_7
    and-int/lit16 v6, v1, 0x400

    if-eq v6, v10, :cond_8

    .line 249
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    .line 250
    or-int/lit16 v1, v1, 0x400

    .line 252
    :cond_8
    iget-object v6, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 257
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 258
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 259
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_9

    const/4 v0, -0x1

    :goto_4
    if-lez v0, :cond_c

    .line 260
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 261
    invoke-static {v0}, Lcom/google/d/a/a/h;->a(I)Lcom/google/d/a/a/h;

    move-result-object v7

    .line 262
    if-nez v7, :cond_a

    .line 263
    const/16 v7, 0xc

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_3

    .line 259
    :cond_9
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 265
    :cond_a
    and-int/lit16 v7, v1, 0x400

    if-eq v7, v10, :cond_b

    .line 266
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    .line 267
    or-int/lit16 v1, v1, 0x400

    .line 269
    :cond_b
    iget-object v7, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 272
    :cond_c
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 276
    :sswitch_d
    iget-object v0, p0, Lcom/google/d/a/a/b;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 277
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/d/a/a/b;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 288
    :cond_d
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v11, :cond_e

    .line 289
    iget-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    .line 291
    :cond_e
    and-int/lit16 v0, v1, 0x400

    if-ne v0, v10, :cond_f

    .line 292
    iget-object v0, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    .line 294
    :cond_f
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/b;->au:Lcom/google/n/bn;

    .line 295
    return-void

    .line 170
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x15 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x62 -> :sswitch_c
        0xfa2 -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 141
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 448
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    .line 464
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    .line 510
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    .line 526
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    .line 662
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/b;->m:Lcom/google/n/ao;

    .line 677
    iput-byte v1, p0, Lcom/google/d/a/a/b;->o:B

    .line 759
    iput v1, p0, Lcom/google/d/a/a/b;->p:I

    .line 142
    return-void
.end method

.method public static d()Lcom/google/d/a/a/b;
    .locals 1

    .prologue
    .line 1782
    sget-object v0, Lcom/google/d/a/a/b;->n:Lcom/google/d/a/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/e;
    .locals 1

    .prologue
    .line 887
    new-instance v0, Lcom/google/d/a/a/e;

    invoke-direct {v0}, Lcom/google/d/a/a/e;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    sget-object v0, Lcom/google/d/a/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v4, 0x5

    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 719
    invoke-virtual {p0}, Lcom/google/d/a/a/b;->c()I

    .line 720
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    .line 721
    iget v0, p0, Lcom/google/d/a/a/b;->e:F

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 723
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    .line 724
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 726
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    .line 727
    iget-object v0, p0, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 729
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_3

    .line 730
    iget-boolean v0, p0, Lcom/google/d/a/a/b;->h:Z

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 732
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    .line 733
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/d/a/a/b;->i:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    :cond_4
    move v3, v2

    .line 735
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 736
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 735
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    move v0, v2

    .line 730
    goto :goto_0

    :cond_6
    move v0, v2

    .line 733
    goto :goto_1

    .line 738
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    .line 739
    const/16 v0, 0x8

    iget v3, p0, Lcom/google/d/a/a/b;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_c

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 741
    :cond_8
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_9

    .line 742
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 744
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_a

    .line 745
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 747
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_b

    .line 748
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/d/a/a/b;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_d

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_b
    :goto_4
    move v1, v2

    .line 750
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 751
    const/16 v3, 0xc

    iget-object v0, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_e

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 750
    :goto_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 739
    :cond_c
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 748
    :cond_d
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 751
    :cond_e
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_6

    .line 753
    :cond_f
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_10

    .line 754
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/b;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 756
    :cond_10
    iget-object v0, p0, Lcom/google/d/a/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 757
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 679
    iget-byte v0, p0, Lcom/google/d/a/a/b;->o:B

    .line 680
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 714
    :cond_0
    :goto_0
    return v2

    .line 681
    :cond_1
    if-eqz v0, :cond_0

    .line 683
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 684
    iget-object v0, p0, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 685
    iput-byte v2, p0, Lcom/google/d/a/a/b;->o:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 683
    goto :goto_1

    .line 689
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 690
    iget-object v0, p0, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 691
    iput-byte v2, p0, Lcom/google/d/a/a/b;->o:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 689
    goto :goto_2

    .line 695
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 696
    iget-object v0, p0, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 697
    iput-byte v2, p0, Lcom/google/d/a/a/b;->o:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 695
    goto :goto_3

    .line 701
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_9

    .line 702
    iget-object v0, p0, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 703
    iput-byte v2, p0, Lcom/google/d/a/a/b;->o:B

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 701
    goto :goto_4

    :cond_9
    move v1, v2

    .line 707
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 708
    iget-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 709
    iput-byte v2, p0, Lcom/google/d/a/a/b;->o:B

    goto/16 :goto_0

    .line 707
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 713
    :cond_b
    iput-byte v3, p0, Lcom/google/d/a/a/b;->o:B

    move v2, v3

    .line 714
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 761
    iget v0, p0, Lcom/google/d/a/a/b;->p:I

    .line 762
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 820
    :goto_0
    return v0

    .line 765
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_f

    .line 766
    iget v0, p0, Lcom/google/d/a/a/b;->e:F

    .line 767
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 769
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1

    .line 770
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    .line 771
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 773
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2

    .line 774
    iget-object v2, p0, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    .line 775
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 777
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_3

    .line 778
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/d/a/a/b;->h:Z

    .line 779
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 781
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_4

    .line 782
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/d/a/a/b;->i:Z

    .line 783
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_4
    move v2, v1

    move v3, v0

    .line 785
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 786
    const/4 v5, 0x7

    iget-object v0, p0, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    .line 787
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 785
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 789
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_6

    .line 790
    iget v0, p0, Lcom/google/d/a/a/b;->k:I

    .line 791
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 793
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    .line 794
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    .line 795
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 797
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_8

    .line 798
    iget-object v0, p0, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    .line 799
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 801
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_9

    .line 802
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/d/a/a/b;->d:I

    .line 803
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_b

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    :cond_9
    move v2, v1

    move v5, v1

    .line 807
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_d

    .line 808
    iget-object v0, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    .line 809
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_c

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v5, v0

    .line 807
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_a
    move v0, v4

    .line 791
    goto :goto_3

    :cond_b
    move v0, v4

    .line 803
    goto :goto_4

    :cond_c
    move v0, v4

    .line 809
    goto :goto_6

    .line 811
    :cond_d
    add-int v0, v3, v5

    .line 812
    iget-object v2, p0, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 814
    iget v2, p0, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_e

    .line 815
    const/16 v2, 0x1f4

    iget-object v3, p0, Lcom/google/d/a/a/b;->m:Lcom/google/n/ao;

    .line 816
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 818
    :cond_e
    iget-object v1, p0, Lcom/google/d/a/a/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 819
    iput v0, p0, Lcom/google/d/a/a/b;->p:I

    goto/16 :goto_0

    :cond_f
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 135
    invoke-static {}, Lcom/google/d/a/a/b;->newBuilder()Lcom/google/d/a/a/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/e;->a(Lcom/google/d/a/a/b;)Lcom/google/d/a/a/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 135
    invoke-static {}, Lcom/google/d/a/a/b;->newBuilder()Lcom/google/d/a/a/e;

    move-result-object v0

    return-object v0
.end method
