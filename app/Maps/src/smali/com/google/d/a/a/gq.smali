.class public final Lcom/google/d/a/a/gq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/gr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/go;",
        "Lcom/google/d/a/a/gq;",
        ">;",
        "Lcom/google/d/a/a/gr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:F

.field private d:Z

.field private e:F

.field private f:F


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 410
    sget-object v0, Lcom/google/d/a/a/go;->g:Lcom/google/d/a/a/go;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 492
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gq;->b:Ljava/lang/Object;

    .line 411
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/go;)Lcom/google/d/a/a/gq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 460
    invoke-static {}, Lcom/google/d/a/a/go;->d()Lcom/google/d/a/a/go;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 479
    :goto_0
    return-object p0

    .line 461
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 462
    iget v2, p0, Lcom/google/d/a/a/gq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/gq;->a:I

    .line 463
    iget-object v2, p1, Lcom/google/d/a/a/go;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/gq;->b:Ljava/lang/Object;

    .line 466
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 467
    iget v2, p1, Lcom/google/d/a/a/go;->c:F

    iget v3, p0, Lcom/google/d/a/a/gq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/gq;->a:I

    iput v2, p0, Lcom/google/d/a/a/gq;->c:F

    .line 469
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 470
    iget-boolean v2, p1, Lcom/google/d/a/a/go;->d:Z

    iget v3, p0, Lcom/google/d/a/a/gq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/gq;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/gq;->d:Z

    .line 472
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 473
    iget v2, p1, Lcom/google/d/a/a/go;->e:F

    iget v3, p0, Lcom/google/d/a/a/gq;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/gq;->a:I

    iput v2, p0, Lcom/google/d/a/a/gq;->e:F

    .line 475
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/go;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 476
    iget v0, p1, Lcom/google/d/a/a/go;->f:F

    iget v1, p0, Lcom/google/d/a/a/gq;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/d/a/a/gq;->a:I

    iput v0, p0, Lcom/google/d/a/a/gq;->f:F

    .line 478
    :cond_5
    iget-object v0, p1, Lcom/google/d/a/a/go;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 461
    goto :goto_1

    :cond_7
    move v2, v1

    .line 466
    goto :goto_2

    :cond_8
    move v2, v1

    .line 469
    goto :goto_3

    :cond_9
    move v2, v1

    .line 472
    goto :goto_4

    :cond_a
    move v0, v1

    .line 475
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 402
    new-instance v2, Lcom/google/d/a/a/go;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/go;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/gq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/d/a/a/gq;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/go;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/d/a/a/gq;->c:F

    iput v1, v2, Lcom/google/d/a/a/go;->c:F

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/d/a/a/gq;->d:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/go;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/gq;->e:F

    iput v1, v2, Lcom/google/d/a/a/go;->e:F

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/d/a/a/gq;->f:F

    iput v1, v2, Lcom/google/d/a/a/go;->f:F

    iput v0, v2, Lcom/google/d/a/a/go;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 402
    check-cast p1, Lcom/google/d/a/a/go;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/gq;->a(Lcom/google/d/a/a/go;)Lcom/google/d/a/a/gq;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 483
    iget v2, p0, Lcom/google/d/a/a/gq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 487
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 483
    goto :goto_0

    :cond_1
    move v0, v1

    .line 487
    goto :goto_1
.end method
