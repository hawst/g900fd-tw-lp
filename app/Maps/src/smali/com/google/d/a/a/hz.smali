.class public final Lcom/google/d/a/a/hz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ic;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hz;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/d/a/a/hz;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/n/f;

.field d:F

.field e:F

.field f:J

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lcom/google/d/a/a/ia;

    invoke-direct {v0}, Lcom/google/d/a/a/ia;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hz;->PARSER:Lcom/google/n/ax;

    .line 422
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/hz;->l:Lcom/google/n/aw;

    .line 1012
    new-instance v0, Lcom/google/d/a/a/hz;

    invoke-direct {v0}, Lcom/google/d/a/a/hz;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hz;->i:Lcom/google/d/a/a/hz;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 94
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 301
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hz;->g:Lcom/google/n/ao;

    .line 317
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    .line 332
    iput-byte v3, p0, Lcom/google/d/a/a/hz;->j:B

    .line 381
    iput v3, p0, Lcom/google/d/a/a/hz;->k:I

    .line 95
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    .line 96
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/d/a/a/hz;->c:Lcom/google/n/f;

    .line 97
    iput v1, p0, Lcom/google/d/a/a/hz;->d:F

    .line 98
    iput v1, p0, Lcom/google/d/a/a/hz;->e:F

    .line 99
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/hz;->f:J

    .line 100
    iget-object v0, p0, Lcom/google/d/a/a/hz;->g:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 101
    iget-object v0, p0, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 102
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 108
    invoke-direct {p0}, Lcom/google/d/a/a/hz;-><init>()V

    .line 111
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 114
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 115
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 116
    sparse-switch v4, :sswitch_data_0

    .line 121
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 123
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 119
    goto :goto_0

    .line 128
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 129
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    .line 131
    or-int/lit8 v1, v1, 0x1

    .line 133
    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 134
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 133
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 176
    iget-object v1, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    .line 178
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/hz;->au:Lcom/google/n/bn;

    throw v0

    .line 138
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/d/a/a/hz;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/hz;->a:I

    .line 139
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/hz;->d:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 171
    :catch_1
    move-exception v0

    .line 172
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 173
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 143
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/d/a/a/hz;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/hz;->a:I

    .line 144
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/hz;->e:F

    goto :goto_0

    .line 148
    :sswitch_4
    iget v4, p0, Lcom/google/d/a/a/hz;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/d/a/a/hz;->a:I

    .line 149
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/hz;->f:J

    goto/16 :goto_0

    .line 153
    :sswitch_5
    iget v4, p0, Lcom/google/d/a/a/hz;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/hz;->a:I

    .line 154
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/hz;->c:Lcom/google/n/f;

    goto/16 :goto_0

    .line 158
    :sswitch_6
    iget-object v4, p0, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 159
    iget v4, p0, Lcom/google/d/a/a/hz;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/d/a/a/hz;->a:I

    goto/16 :goto_0

    .line 163
    :sswitch_7
    iget-object v4, p0, Lcom/google/d/a/a/hz;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 164
    iget v4, p0, Lcom/google/d/a/a/hz;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/d/a/a/hz;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 175
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 176
    iget-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    .line 178
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hz;->au:Lcom/google/n/bn;

    .line 179
    return-void

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x7a -> :sswitch_6
        0xfa2 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 92
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 301
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hz;->g:Lcom/google/n/ao;

    .line 317
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    .line 332
    iput-byte v1, p0, Lcom/google/d/a/a/hz;->j:B

    .line 381
    iput v1, p0, Lcom/google/d/a/a/hz;->k:I

    .line 93
    return-void
.end method

.method public static d()Lcom/google/d/a/a/hz;
    .locals 1

    .prologue
    .line 1015
    sget-object v0, Lcom/google/d/a/a/hz;->i:Lcom/google/d/a/a/hz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ib;
    .locals 1

    .prologue
    .line 484
    new-instance v0, Lcom/google/d/a/a/ib;

    invoke-direct {v0}, Lcom/google/d/a/a/ib;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    sget-object v0, Lcom/google/d/a/a/hz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x5

    const/4 v4, 0x2

    .line 356
    invoke-virtual {p0}, Lcom/google/d/a/a/hz;->c()I

    move v1, v2

    .line 357
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 357
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 360
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 361
    iget v0, p0, Lcom/google/d/a/a/hz;->d:F

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 363
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 364
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/hz;->e:F

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 366
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 367
    iget-wide v0, p0, Lcom/google/d/a/a/hz;->f:J

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 369
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_4

    .line 370
    iget-object v0, p0, Lcom/google/d/a/a/hz;->c:Lcom/google/n/f;

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 372
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 373
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 375
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 376
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/hz;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 378
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/hz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 379
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 334
    iget-byte v0, p0, Lcom/google/d/a/a/hz;->j:B

    .line 335
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 351
    :cond_0
    :goto_0
    return v2

    .line 336
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 338
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 339
    iget-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ie;->d()Lcom/google/d/a/a/ie;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ie;

    invoke-virtual {v0}, Lcom/google/d/a/a/ie;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 340
    iput-byte v2, p0, Lcom/google/d/a/a/hz;->j:B

    goto :goto_0

    .line 338
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 344
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 345
    iget-object v0, p0, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 346
    iput-byte v2, p0, Lcom/google/d/a/a/hz;->j:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 344
    goto :goto_2

    .line 350
    :cond_5
    iput-byte v3, p0, Lcom/google/d/a/a/hz;->j:B

    move v2, v3

    .line 351
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 383
    iget v0, p0, Lcom/google/d/a/a/hz;->k:I

    .line 384
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 417
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 387
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/google/d/a/a/hz;->b:Ljava/util/List;

    .line 389
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 387
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 391
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    .line 392
    iget v0, p0, Lcom/google/d/a/a/hz;->d:F

    .line 393
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 395
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_3

    .line 396
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/hz;->e:F

    .line 397
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 399
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 400
    iget-wide v0, p0, Lcom/google/d/a/a/hz;->f:J

    .line 401
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0, v1}, Lcom/google/n/l;->b(J)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 403
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_5

    .line 404
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/d/a/a/hz;->c:Lcom/google/n/f;

    .line 405
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 407
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 408
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/d/a/a/hz;->h:Lcom/google/n/ao;

    .line 409
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 411
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/hz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 412
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/hz;->g:Lcom/google/n/ao;

    .line 413
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 415
    :cond_7
    iget-object v0, p0, Lcom/google/d/a/a/hz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 416
    iput v0, p0, Lcom/google/d/a/a/hz;->k:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/google/d/a/a/hz;->newBuilder()Lcom/google/d/a/a/ib;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ib;->a(Lcom/google/d/a/a/hz;)Lcom/google/d/a/a/ib;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/google/d/a/a/hz;->newBuilder()Lcom/google/d/a/a/ib;

    move-result-object v0

    return-object v0
.end method
