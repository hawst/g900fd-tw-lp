.class public final Lcom/google/d/a/a/hf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/hi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hf;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/d/a/a/hf;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:I

.field e:I

.field f:I

.field g:Z

.field h:I

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/ao;

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 226
    new-instance v0, Lcom/google/d/a/a/hg;

    invoke-direct {v0}, Lcom/google/d/a/a/hg;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hf;->PARSER:Lcom/google/n/ax;

    .line 553
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/hf;->o:Lcom/google/n/aw;

    .line 1341
    new-instance v0, Lcom/google/d/a/a/hf;

    invoke-direct {v0}, Lcom/google/d/a/a/hf;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hf;->l:Lcom/google/d/a/a/hf;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 243
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    .line 259
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    .line 393
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    .line 409
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    .line 424
    iput-byte v4, p0, Lcom/google/d/a/a/hf;->m:B

    .line 500
    iput v4, p0, Lcom/google/d/a/a/hf;->n:I

    .line 122
    iget-object v0, p0, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 123
    iget-object v0, p0, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 124
    iput v2, p0, Lcom/google/d/a/a/hf;->d:I

    .line 125
    iput v2, p0, Lcom/google/d/a/a/hf;->e:I

    .line 126
    iput v2, p0, Lcom/google/d/a/a/hf;->f:I

    .line 127
    iput-boolean v2, p0, Lcom/google/d/a/a/hf;->g:Z

    .line 128
    iput v2, p0, Lcom/google/d/a/a/hf;->h:I

    .line 129
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    .line 130
    iget-object v0, p0, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 131
    iget-object v0, p0, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 132
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v10, 0x80

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 138
    invoke-direct {p0}, Lcom/google/d/a/a/hf;-><init>()V

    .line 141
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 144
    :cond_0
    :goto_0
    if-nez v4, :cond_4

    .line 145
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 146
    sparse-switch v0, :sswitch_data_0

    .line 151
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 153
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 149
    goto :goto_0

    .line 158
    :sswitch_1
    iget-object v0, p0, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 159
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    .line 215
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x80

    if-ne v1, v10, :cond_1

    .line 221
    iget-object v1, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    .line 223
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/hf;->au:Lcom/google/n/bn;

    throw v0

    .line 163
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 164
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 216
    :catch_1
    move-exception v0

    .line 217
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 218
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 168
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I

    .line 169
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/hf;->d:I

    goto :goto_0

    .line 173
    :sswitch_4
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I

    .line 174
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/hf;->e:I

    goto :goto_0

    .line 178
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I

    .line 179
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/hf;->f:I

    goto/16 :goto_0

    .line 183
    :sswitch_6
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I

    .line 184
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/hf;->g:Z

    goto/16 :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 188
    :sswitch_7
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I

    .line 189
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/hf;->h:I

    goto/16 :goto_0

    .line 193
    :sswitch_8
    and-int/lit16 v0, v1, 0x80

    if-eq v0, v10, :cond_3

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    .line 196
    or-int/lit16 v1, v1, 0x80

    .line 198
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 199
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 198
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 203
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 204
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I

    goto/16 :goto_0

    .line 208
    :sswitch_a
    iget-object v0, p0, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 209
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/hf;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 220
    :cond_4
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v10, :cond_5

    .line 221
    iget-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    .line 223
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hf;->au:Lcom/google/n/bn;

    .line 224
    return-void

    .line 146
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 119
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 243
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    .line 259
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    .line 393
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    .line 409
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    .line 424
    iput-byte v1, p0, Lcom/google/d/a/a/hf;->m:B

    .line 500
    iput v1, p0, Lcom/google/d/a/a/hf;->n:I

    .line 120
    return-void
.end method

.method public static d()Lcom/google/d/a/a/hf;
    .locals 1

    .prologue
    .line 1344
    sget-object v0, Lcom/google/d/a/a/hf;->l:Lcom/google/d/a/a/hf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/hh;
    .locals 1

    .prologue
    .line 615
    new-instance v0, Lcom/google/d/a/a/hh;

    invoke-direct {v0}, Lcom/google/d/a/a/hh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    sget-object v0, Lcom/google/d/a/a/hf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v0, 0x1

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 466
    invoke-virtual {p0}, Lcom/google/d/a/a/hf;->c()I

    .line 467
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 468
    iget-object v2, p0, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 470
    :cond_0
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 471
    iget-object v2, p0, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 473
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 474
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/d/a/a/hf;->d:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_7

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 476
    :cond_2
    :goto_0
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 477
    iget v2, p0, Lcom/google/d/a/a/hf;->e:I

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_8

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 479
    :cond_3
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 480
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/d/a/a/hf;->f:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_9

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 482
    :cond_4
    :goto_2
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 483
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/d/a/a/hf;->g:Z

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_a

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 485
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 486
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/d/a/a/hf;->h:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_b

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 488
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 489
    iget-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 488
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 474
    :cond_7
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 477
    :cond_8
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 480
    :cond_9
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    :cond_a
    move v0, v1

    .line 483
    goto :goto_3

    .line 486
    :cond_b
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 491
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_d

    .line 492
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 494
    :cond_d
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_e

    .line 495
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 497
    :cond_e
    iget-object v0, p0, Lcom/google/d/a/a/hf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 498
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 426
    iget-byte v0, p0, Lcom/google/d/a/a/hf;->m:B

    .line 427
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 461
    :cond_0
    :goto_0
    return v2

    .line 428
    :cond_1
    if-eqz v0, :cond_0

    .line 430
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 431
    iget-object v0, p0, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 432
    iput-byte v2, p0, Lcom/google/d/a/a/hf;->m:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 430
    goto :goto_1

    .line 436
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 437
    iget-object v0, p0, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 438
    iput-byte v2, p0, Lcom/google/d/a/a/hf;->m:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 436
    goto :goto_2

    :cond_5
    move v1, v2

    .line 442
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 443
    iget-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 444
    iput-byte v2, p0, Lcom/google/d/a/a/hf;->m:B

    goto :goto_0

    .line 442
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 448
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_9

    .line 449
    iget-object v0, p0, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hb;->d()Lcom/google/d/a/a/hb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hb;

    invoke-virtual {v0}, Lcom/google/d/a/a/hb;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 450
    iput-byte v2, p0, Lcom/google/d/a/a/hf;->m:B

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 448
    goto :goto_4

    .line 454
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    move v0, v3

    :goto_5
    if-eqz v0, :cond_b

    .line 455
    iget-object v0, p0, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hb;->d()Lcom/google/d/a/a/hb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hb;

    invoke-virtual {v0}, Lcom/google/d/a/a/hb;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 456
    iput-byte v2, p0, Lcom/google/d/a/a/hf;->m:B

    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 454
    goto :goto_5

    .line 460
    :cond_b
    iput-byte v3, p0, Lcom/google/d/a/a/hf;->m:B

    move v2, v3

    .line 461
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 502
    iget v0, p0, Lcom/google/d/a/a/hf;->n:I

    .line 503
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 548
    :goto_0
    return v0

    .line 506
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_e

    .line 507
    iget-object v0, p0, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    .line 508
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 510
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 511
    iget-object v2, p0, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    .line 512
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 514
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 515
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/d/a/a/hf;->d:I

    .line 516
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 518
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_3

    .line 519
    iget v2, p0, Lcom/google/d/a/a/hf;->e:I

    .line 520
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_8

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 522
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_4

    .line 523
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/d/a/a/hf;->f:I

    .line 524
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 526
    :cond_4
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_5

    .line 527
    const/4 v2, 0x6

    iget-boolean v4, p0, Lcom/google/d/a/a/hf;->g:Z

    .line 528
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 530
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_6

    .line 531
    const/4 v2, 0x7

    iget v4, p0, Lcom/google/d/a/a/hf;->h:I

    .line 532
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_5
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    :cond_6
    move v2, v1

    move v4, v0

    .line 534
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 535
    const/16 v5, 0x8

    iget-object v0, p0, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    .line 536
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 534
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_7
    move v2, v3

    .line 516
    goto/16 :goto_2

    :cond_8
    move v2, v3

    .line 520
    goto :goto_3

    :cond_9
    move v2, v3

    .line 524
    goto :goto_4

    :cond_a
    move v2, v3

    .line 532
    goto :goto_5

    .line 538
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_c

    .line 539
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    .line 540
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 542
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/hf;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_d

    .line 543
    iget-object v0, p0, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    .line 544
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 546
    :cond_d
    iget-object v0, p0, Lcom/google/d/a/a/hf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 547
    iput v0, p0, Lcom/google/d/a/a/hf;->n:I

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/google/d/a/a/hf;->newBuilder()Lcom/google/d/a/a/hh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/hh;->a(Lcom/google/d/a/a/hf;)Lcom/google/d/a/a/hh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/google/d/a/a/hf;->newBuilder()Lcom/google/d/a/a/hh;

    move-result-object v0

    return-object v0
.end method
