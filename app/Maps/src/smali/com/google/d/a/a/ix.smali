.class public final enum Lcom/google/d/a/a/ix;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ix;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ix;

.field public static final enum b:Lcom/google/d/a/a/ix;

.field public static final enum c:Lcom/google/d/a/a/ix;

.field public static final enum d:Lcom/google/d/a/a/ix;

.field private static final synthetic f:[Lcom/google/d/a/a/ix;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 631
    new-instance v0, Lcom/google/d/a/a/ix;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/a/a/ix;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ix;->a:Lcom/google/d/a/a/ix;

    .line 635
    new-instance v0, Lcom/google/d/a/a/ix;

    const-string v1, "RMF"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/ix;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ix;->b:Lcom/google/d/a/a/ix;

    .line 639
    new-instance v0, Lcom/google/d/a/a/ix;

    const-string v1, "EUROPA"

    invoke-direct {v0, v1, v6, v5}, Lcom/google/d/a/a/ix;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ix;->c:Lcom/google/d/a/a/ix;

    .line 643
    new-instance v0, Lcom/google/d/a/a/ix;

    const-string v1, "MULTINET"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/ix;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ix;->d:Lcom/google/d/a/a/ix;

    .line 626
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/d/a/a/ix;

    sget-object v1, Lcom/google/d/a/a/ix;->a:Lcom/google/d/a/a/ix;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/ix;->b:Lcom/google/d/a/a/ix;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/ix;->c:Lcom/google/d/a/a/ix;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/ix;->d:Lcom/google/d/a/a/ix;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/d/a/a/ix;->f:[Lcom/google/d/a/a/ix;

    .line 683
    new-instance v0, Lcom/google/d/a/a/iy;

    invoke-direct {v0}, Lcom/google/d/a/a/iy;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 692
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 693
    iput p3, p0, Lcom/google/d/a/a/ix;->e:I

    .line 694
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ix;
    .locals 1

    .prologue
    .line 669
    packed-switch p0, :pswitch_data_0

    .line 674
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 670
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/ix;->a:Lcom/google/d/a/a/ix;

    goto :goto_0

    .line 671
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/ix;->b:Lcom/google/d/a/a/ix;

    goto :goto_0

    .line 672
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/ix;->c:Lcom/google/d/a/a/ix;

    goto :goto_0

    .line 673
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/ix;->d:Lcom/google/d/a/a/ix;

    goto :goto_0

    .line 669
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ix;
    .locals 1

    .prologue
    .line 626
    const-class v0, Lcom/google/d/a/a/ix;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ix;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ix;
    .locals 1

    .prologue
    .line 626
    sget-object v0, Lcom/google/d/a/a/ix;->f:[Lcom/google/d/a/a/ix;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ix;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ix;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 665
    iget v0, p0, Lcom/google/d/a/a/ix;->e:I

    return v0
.end method
