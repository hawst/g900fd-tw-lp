.class public final enum Lcom/google/d/a/a/po;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/po;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field private static final synthetic A:[Lcom/google/d/a/a/po;

.field public static final enum a:Lcom/google/d/a/a/po;

.field public static final enum b:Lcom/google/d/a/a/po;

.field public static final enum c:Lcom/google/d/a/a/po;

.field public static final enum d:Lcom/google/d/a/a/po;

.field public static final enum e:Lcom/google/d/a/a/po;

.field public static final enum f:Lcom/google/d/a/a/po;

.field public static final enum g:Lcom/google/d/a/a/po;

.field public static final enum h:Lcom/google/d/a/a/po;

.field public static final enum i:Lcom/google/d/a/a/po;

.field public static final enum j:Lcom/google/d/a/a/po;

.field public static final enum k:Lcom/google/d/a/a/po;

.field public static final enum l:Lcom/google/d/a/a/po;

.field public static final enum m:Lcom/google/d/a/a/po;

.field public static final enum n:Lcom/google/d/a/a/po;

.field public static final enum o:Lcom/google/d/a/a/po;

.field public static final enum p:Lcom/google/d/a/a/po;

.field public static final enum q:Lcom/google/d/a/a/po;

.field public static final enum r:Lcom/google/d/a/a/po;

.field public static final enum s:Lcom/google/d/a/a/po;

.field public static final enum t:Lcom/google/d/a/a/po;

.field public static final enum u:Lcom/google/d/a/a/po;

.field public static final enum v:Lcom/google/d/a/a/po;

.field public static final enum w:Lcom/google/d/a/a/po;

.field public static final enum x:Lcom/google/d/a/a/po;

.field public static final enum y:Lcom/google/d/a/a/po;


# instance fields
.field final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1584
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_SEASON"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->a:Lcom/google/d/a/a/po;

    .line 1588
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_SEASON_WINTER"

    const/16 v2, 0x101

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->b:Lcom/google/d/a/a/po;

    .line 1592
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_SEASON_SUMMER"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->c:Lcom/google/d/a/a/po;

    .line 1596
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_DAYS"

    invoke-direct {v0, v1, v6, v5}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->d:Lcom/google/d/a/a/po;

    .line 1600
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_DAYS_SCHOOL"

    const/16 v2, 0x201

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->e:Lcom/google/d/a/a/po;

    .line 1604
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_DAYS_HOLIDAY"

    const/16 v2, 0x202

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->f:Lcom/google/d/a/a/po;

    .line 1608
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_DAYS_PRE_HOLIDAY"

    const/4 v2, 0x6

    const/16 v3, 0x203

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->g:Lcom/google/d/a/a/po;

    .line 1612
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_HOURS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v6}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->h:Lcom/google/d/a/a/po;

    .line 1616
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_HOURS_PEAK"

    const/16 v2, 0x8

    const/16 v3, 0x301

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->i:Lcom/google/d/a/a/po;

    .line 1620
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_HOURS_SCHOOL"

    const/16 v2, 0x9

    const/16 v3, 0x302

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->j:Lcom/google/d/a/a/po;

    .line 1624
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_HOURS_MARKET"

    const/16 v2, 0xa

    const/16 v3, 0x303

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->k:Lcom/google/d/a/a/po;

    .line 1628
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_HOURS_BUSINESS"

    const/16 v2, 0xb

    const/16 v3, 0x304

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->l:Lcom/google/d/a/a/po;

    .line 1632
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_HOURS_DUSK_TO_DAWN"

    const/16 v2, 0xc

    const/16 v3, 0x305

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->m:Lcom/google/d/a/a/po;

    .line 1636
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_HOURS_HIGH_TIDE"

    const/16 v2, 0xd

    const/16 v3, 0x306

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->n:Lcom/google/d/a/a/po;

    .line 1640
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_CONDITIONS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v7}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->o:Lcom/google/d/a/a/po;

    .line 1644
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_CONDITIONS_HIGH_WATER"

    const/16 v2, 0xf

    const/16 v3, 0x401

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->p:Lcom/google/d/a/a/po;

    .line 1648
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_CONDITIONS_ADVERSE"

    const/16 v2, 0x10

    const/16 v3, 0x402

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->q:Lcom/google/d/a/a/po;

    .line 1652
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_CONDITIONS_WINTERY"

    const/16 v2, 0x11

    const/16 v3, 0x403

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->r:Lcom/google/d/a/a/po;

    .line 1656
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_CONDITIONS_WINTERY_AVALANCHE"

    const/16 v2, 0x12

    const v3, 0x40301

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->s:Lcom/google/d/a/a/po;

    .line 1660
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_CONDITIONS_EVENT"

    const/16 v2, 0x13

    const/16 v3, 0x404

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->t:Lcom/google/d/a/a/po;

    .line 1664
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_CONDITIONS_POLLUTION"

    const/16 v2, 0x14

    const/16 v3, 0x405

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->u:Lcom/google/d/a/a/po;

    .line 1668
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_CONDITIONS_LOW_WATER"

    const/16 v2, 0x15

    const/16 v3, 0x406

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->v:Lcom/google/d/a/a/po;

    .line 1672
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_UNDEFINED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2, v8}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->w:Lcom/google/d/a/a/po;

    .line 1676
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_UNDEFINED_REGULAR"

    const/16 v2, 0x17

    const/16 v3, 0x501

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->x:Lcom/google/d/a/a/po;

    .line 1680
    new-instance v0, Lcom/google/d/a/a/po;

    const-string v1, "OCCASION_UNDEFINED_SELDOM"

    const/16 v2, 0x18

    const/16 v3, 0x502

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/po;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/po;->y:Lcom/google/d/a/a/po;

    .line 1579
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/google/d/a/a/po;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/d/a/a/po;->a:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/po;->b:Lcom/google/d/a/a/po;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/po;->c:Lcom/google/d/a/a/po;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/po;->d:Lcom/google/d/a/a/po;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/po;->e:Lcom/google/d/a/a/po;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/po;->f:Lcom/google/d/a/a/po;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/po;->g:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/po;->h:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/po;->i:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/po;->j:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/po;->k:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/po;->l:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/po;->m:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/d/a/a/po;->n:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/d/a/a/po;->o:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/d/a/a/po;->p:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/d/a/a/po;->q:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/d/a/a/po;->r:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/d/a/a/po;->s:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/d/a/a/po;->t:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/d/a/a/po;->u:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/d/a/a/po;->v:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/d/a/a/po;->w:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/d/a/a/po;->x:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/d/a/a/po;->y:Lcom/google/d/a/a/po;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/po;->A:[Lcom/google/d/a/a/po;

    .line 1825
    new-instance v0, Lcom/google/d/a/a/pp;

    invoke-direct {v0}, Lcom/google/d/a/a/pp;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1834
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1835
    iput p3, p0, Lcom/google/d/a/a/po;->z:I

    .line 1836
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/po;
    .locals 1

    .prologue
    .line 1790
    sparse-switch p0, :sswitch_data_0

    .line 1816
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1791
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/po;->a:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1792
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/po;->b:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1793
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/po;->c:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1794
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/po;->d:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1795
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/po;->e:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1796
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/po;->f:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1797
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/po;->g:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1798
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/po;->h:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1799
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/po;->i:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1800
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/po;->j:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1801
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/po;->k:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1802
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/po;->l:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1803
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/po;->m:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1804
    :sswitch_d
    sget-object v0, Lcom/google/d/a/a/po;->n:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1805
    :sswitch_e
    sget-object v0, Lcom/google/d/a/a/po;->o:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1806
    :sswitch_f
    sget-object v0, Lcom/google/d/a/a/po;->p:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1807
    :sswitch_10
    sget-object v0, Lcom/google/d/a/a/po;->q:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1808
    :sswitch_11
    sget-object v0, Lcom/google/d/a/a/po;->r:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1809
    :sswitch_12
    sget-object v0, Lcom/google/d/a/a/po;->s:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1810
    :sswitch_13
    sget-object v0, Lcom/google/d/a/a/po;->t:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1811
    :sswitch_14
    sget-object v0, Lcom/google/d/a/a/po;->u:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1812
    :sswitch_15
    sget-object v0, Lcom/google/d/a/a/po;->v:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1813
    :sswitch_16
    sget-object v0, Lcom/google/d/a/a/po;->w:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1814
    :sswitch_17
    sget-object v0, Lcom/google/d/a/a/po;->x:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1815
    :sswitch_18
    sget-object v0, Lcom/google/d/a/a/po;->y:Lcom/google/d/a/a/po;

    goto :goto_0

    .line 1790
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_7
        0x4 -> :sswitch_e
        0x5 -> :sswitch_16
        0x101 -> :sswitch_1
        0x102 -> :sswitch_2
        0x201 -> :sswitch_4
        0x202 -> :sswitch_5
        0x203 -> :sswitch_6
        0x301 -> :sswitch_8
        0x302 -> :sswitch_9
        0x303 -> :sswitch_a
        0x304 -> :sswitch_b
        0x305 -> :sswitch_c
        0x306 -> :sswitch_d
        0x401 -> :sswitch_f
        0x402 -> :sswitch_10
        0x403 -> :sswitch_11
        0x404 -> :sswitch_13
        0x405 -> :sswitch_14
        0x406 -> :sswitch_15
        0x501 -> :sswitch_17
        0x502 -> :sswitch_18
        0x40301 -> :sswitch_12
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/po;
    .locals 1

    .prologue
    .line 1579
    const-class v0, Lcom/google/d/a/a/po;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/po;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/po;
    .locals 1

    .prologue
    .line 1579
    sget-object v0, Lcom/google/d/a/a/po;->A:[Lcom/google/d/a/a/po;

    invoke-virtual {v0}, [Lcom/google/d/a/a/po;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/po;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1786
    iget v0, p0, Lcom/google/d/a/a/po;->z:I

    return v0
.end method
