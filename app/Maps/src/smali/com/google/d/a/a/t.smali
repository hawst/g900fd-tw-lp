.class public final enum Lcom/google/d/a/a/t;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/t;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/t;

.field public static final enum b:Lcom/google/d/a/a/t;

.field public static final enum c:Lcom/google/d/a/a/t;

.field public static final enum d:Lcom/google/d/a/a/t;

.field public static final enum e:Lcom/google/d/a/a/t;

.field public static final enum f:Lcom/google/d/a/a/t;

.field public static final enum g:Lcom/google/d/a/a/t;

.field public static final enum h:Lcom/google/d/a/a/t;

.field private static final synthetic j:[Lcom/google/d/a/a/t;


# instance fields
.field final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 210
    new-instance v0, Lcom/google/d/a/a/t;

    const-string v1, "TYPE_FEATURE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/t;->a:Lcom/google/d/a/a/t;

    .line 214
    new-instance v0, Lcom/google/d/a/a/t;

    const-string v1, "TYPE_POSTAL_CODE_SUFFIX"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/t;->b:Lcom/google/d/a/a/t;

    .line 218
    new-instance v0, Lcom/google/d/a/a/t;

    const-string v1, "TYPE_POST_BOX"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/t;->c:Lcom/google/d/a/a/t;

    .line 222
    new-instance v0, Lcom/google/d/a/a/t;

    const-string v1, "TYPE_STREET_NUMBER"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/t;->d:Lcom/google/d/a/a/t;

    .line 226
    new-instance v0, Lcom/google/d/a/a/t;

    const-string v1, "TYPE_FLOOR"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/t;->e:Lcom/google/d/a/a/t;

    .line 230
    new-instance v0, Lcom/google/d/a/a/t;

    const-string v1, "TYPE_ROOM"

    const/4 v2, 0x5

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/t;->f:Lcom/google/d/a/a/t;

    .line 234
    new-instance v0, Lcom/google/d/a/a/t;

    const-string v1, "TYPE_HOUSE_ID"

    const/4 v2, 0x6

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/t;->g:Lcom/google/d/a/a/t;

    .line 238
    new-instance v0, Lcom/google/d/a/a/t;

    const-string v1, "TYPE_DISTANCE_MARKER"

    const/4 v2, 0x7

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/t;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/t;->h:Lcom/google/d/a/a/t;

    .line 205
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/d/a/a/t;

    sget-object v1, Lcom/google/d/a/a/t;->a:Lcom/google/d/a/a/t;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/t;->b:Lcom/google/d/a/a/t;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/t;->c:Lcom/google/d/a/a/t;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/t;->d:Lcom/google/d/a/a/t;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/t;->e:Lcom/google/d/a/a/t;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/t;->f:Lcom/google/d/a/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/t;->g:Lcom/google/d/a/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/t;->h:Lcom/google/d/a/a/t;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/t;->j:[Lcom/google/d/a/a/t;

    .line 298
    new-instance v0, Lcom/google/d/a/a/u;

    invoke-direct {v0}, Lcom/google/d/a/a/u;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 307
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 308
    iput p3, p0, Lcom/google/d/a/a/t;->i:I

    .line 309
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/t;
    .locals 1

    .prologue
    .line 280
    packed-switch p0, :pswitch_data_0

    .line 289
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 281
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/t;->a:Lcom/google/d/a/a/t;

    goto :goto_0

    .line 282
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/t;->b:Lcom/google/d/a/a/t;

    goto :goto_0

    .line 283
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/t;->c:Lcom/google/d/a/a/t;

    goto :goto_0

    .line 284
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/t;->d:Lcom/google/d/a/a/t;

    goto :goto_0

    .line 285
    :pswitch_5
    sget-object v0, Lcom/google/d/a/a/t;->e:Lcom/google/d/a/a/t;

    goto :goto_0

    .line 286
    :pswitch_6
    sget-object v0, Lcom/google/d/a/a/t;->f:Lcom/google/d/a/a/t;

    goto :goto_0

    .line 287
    :pswitch_7
    sget-object v0, Lcom/google/d/a/a/t;->g:Lcom/google/d/a/a/t;

    goto :goto_0

    .line 288
    :pswitch_8
    sget-object v0, Lcom/google/d/a/a/t;->h:Lcom/google/d/a/a/t;

    goto :goto_0

    .line 280
    nop

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/t;
    .locals 1

    .prologue
    .line 205
    const-class v0, Lcom/google/d/a/a/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/t;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/t;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/google/d/a/a/t;->j:[Lcom/google/d/a/a/t;

    invoke-virtual {v0}, [Lcom/google/d/a/a/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/t;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/google/d/a/a/t;->i:I

    return v0
.end method
