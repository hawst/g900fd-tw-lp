.class public final Lcom/google/d/a/a/fo;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/fx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/fm;",
        "Lcom/google/d/a/a/fo;",
        ">;",
        "Lcom/google/d/a/a/fx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:I

.field private e:I

.field private f:F

.field private g:F

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:F

.field private k:F


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1358
    sget-object v0, Lcom/google/d/a/a/fm;->l:Lcom/google/d/a/a/fm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1575
    iput v1, p0, Lcom/google/d/a/a/fo;->d:I

    .line 1611
    iput v1, p0, Lcom/google/d/a/a/fo;->e:I

    .line 1712
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    .line 1849
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    .line 1359
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/fm;)Lcom/google/d/a/a/fo;
    .locals 6

    .prologue
    const/16 v5, 0x80

    const/16 v4, 0x40

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1440
    invoke-static {}, Lcom/google/d/a/a/fm;->d()Lcom/google/d/a/a/fm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1486
    :goto_0
    return-object p0

    .line 1441
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1442
    iget v2, p1, Lcom/google/d/a/a/fm;->b:I

    iget v3, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/fo;->a:I

    iput v2, p0, Lcom/google/d/a/a/fo;->b:I

    .line 1444
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1445
    iget-boolean v2, p1, Lcom/google/d/a/a/fm;->c:Z

    iget v3, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/fo;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/fo;->c:Z

    .line 1447
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 1448
    iget v2, p1, Lcom/google/d/a/a/fm;->d:I

    invoke-static {v2}, Lcom/google/d/a/a/fv;->a(I)Lcom/google/d/a/a/fv;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/d/a/a/fv;->a:Lcom/google/d/a/a/fv;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 1441
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1444
    goto :goto_2

    :cond_6
    move v2, v1

    .line 1447
    goto :goto_3

    .line 1448
    :cond_7
    iget v3, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/fo;->a:I

    iget v2, v2, Lcom/google/d/a/a/fv;->q:I

    iput v2, p0, Lcom/google/d/a/a/fo;->d:I

    .line 1450
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_c

    .line 1451
    iget v2, p1, Lcom/google/d/a/a/fm;->e:I

    invoke-static {v2}, Lcom/google/d/a/a/ft;->a(I)Lcom/google/d/a/a/ft;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/d/a/a/ft;->a:Lcom/google/d/a/a/ft;

    :cond_9
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 1450
    goto :goto_4

    .line 1451
    :cond_b
    iget v3, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/fo;->a:I

    iget v2, v2, Lcom/google/d/a/a/ft;->e:I

    iput v2, p0, Lcom/google/d/a/a/fo;->e:I

    .line 1453
    :cond_c
    iget v2, p1, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_d

    .line 1454
    iget v2, p1, Lcom/google/d/a/a/fm;->f:F

    iget v3, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/fo;->a:I

    iput v2, p0, Lcom/google/d/a/a/fo;->f:F

    .line 1456
    :cond_d
    iget v2, p1, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_e

    .line 1457
    iget v2, p1, Lcom/google/d/a/a/fm;->g:F

    iget v3, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/fo;->a:I

    iput v2, p0, Lcom/google/d/a/a/fo;->g:F

    .line 1459
    :cond_e
    iget-object v2, p1, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 1460
    iget-object v2, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1461
    iget-object v2, p1, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    .line 1462
    iget v2, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/d/a/a/fo;->a:I

    .line 1469
    :cond_f
    :goto_7
    iget-object v2, p1, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_10

    .line 1470
    iget-object v2, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1471
    iget-object v2, p1, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    .line 1472
    iget v2, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/d/a/a/fo;->a:I

    .line 1479
    :cond_10
    :goto_8
    iget v2, p1, Lcom/google/d/a/a/fm;->a:I

    and-int/lit8 v2, v2, 0x40

    if-ne v2, v4, :cond_19

    move v2, v0

    :goto_9
    if-eqz v2, :cond_11

    .line 1480
    iget v2, p1, Lcom/google/d/a/a/fm;->j:F

    iget v3, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/d/a/a/fo;->a:I

    iput v2, p0, Lcom/google/d/a/a/fo;->j:F

    .line 1482
    :cond_11
    iget v2, p1, Lcom/google/d/a/a/fm;->a:I

    and-int/lit16 v2, v2, 0x80

    if-ne v2, v5, :cond_1a

    :goto_a
    if-eqz v0, :cond_12

    .line 1483
    iget v0, p1, Lcom/google/d/a/a/fm;->k:F

    iget v1, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/d/a/a/fo;->a:I

    iput v0, p0, Lcom/google/d/a/a/fo;->k:F

    .line 1485
    :cond_12
    iget-object v0, p1, Lcom/google/d/a/a/fm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v2, v1

    .line 1453
    goto/16 :goto_5

    :cond_14
    move v2, v1

    .line 1456
    goto :goto_6

    .line 1464
    :cond_15
    iget v2, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit8 v2, v2, 0x40

    if-eq v2, v4, :cond_16

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/fo;->a:I

    .line 1465
    :cond_16
    iget-object v2, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_7

    .line 1474
    :cond_17
    iget v2, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eq v2, v5, :cond_18

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/fo;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/d/a/a/fo;->a:I

    .line 1475
    :cond_18
    iget-object v2, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_19
    move v2, v1

    .line 1479
    goto :goto_9

    :cond_1a
    move v0, v1

    .line 1482
    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1350
    new-instance v2, Lcom/google/d/a/a/fm;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/fm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/fo;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/fo;->b:I

    iput v1, v2, Lcom/google/d/a/a/fm;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/d/a/a/fo;->c:Z

    iput-boolean v1, v2, Lcom/google/d/a/a/fm;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/d/a/a/fo;->d:I

    iput v1, v2, Lcom/google/d/a/a/fm;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/fo;->e:I

    iput v1, v2, Lcom/google/d/a/a/fm;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/d/a/a/fo;->f:F

    iput v1, v2, Lcom/google/d/a/a/fm;->f:F

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/d/a/a/fo;->g:F

    iput v1, v2, Lcom/google/d/a/a/fm;->g:F

    iget v1, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/google/d/a/a/fo;->a:I

    :cond_5
    iget-object v1, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/fm;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/d/a/a/fo;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/fm;->i:Ljava/util/List;

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget v1, p0, Lcom/google/d/a/a/fo;->j:F

    iput v1, v2, Lcom/google/d/a/a/fm;->j:F

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget v1, p0, Lcom/google/d/a/a/fo;->k:F

    iput v1, v2, Lcom/google/d/a/a/fm;->k:F

    iput v0, v2, Lcom/google/d/a/a/fm;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1350
    check-cast p1, Lcom/google/d/a/a/fm;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/fo;->a(Lcom/google/d/a/a/fm;)Lcom/google/d/a/a/fo;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1490
    iget v0, p0, Lcom/google/d/a/a/fo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 1506
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1490
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1494
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1495
    iget-object v0, p0, Lcom/google/d/a/a/fo;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/kg;->d()Lcom/google/d/a/a/kg;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/kg;

    invoke-virtual {v0}, Lcom/google/d/a/a/kg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1494
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 1500
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1501
    iget-object v0, p0, Lcom/google/d/a/a/fo;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/fp;->d()Lcom/google/d/a/a/fp;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fp;

    invoke-virtual {v0}, Lcom/google/d/a/a/fp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1500
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v2, v3

    .line 1506
    goto :goto_1
.end method
