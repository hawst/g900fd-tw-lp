.class public final Lcom/google/d/a/a/qp;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/qu;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/qp;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/a/a/qp;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:I

.field f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/google/d/a/a/qq;

    invoke-direct {v0}, Lcom/google/d/a/a/qq;-><init>()V

    sput-object v0, Lcom/google/d/a/a/qp;->PARSER:Lcom/google/n/ax;

    .line 488
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/qp;->j:Lcom/google/n/aw;

    .line 1050
    new-instance v0, Lcom/google/d/a/a/qp;

    invoke-direct {v0}, Lcom/google/d/a/a/qp;-><init>()V

    sput-object v0, Lcom/google/d/a/a/qp;->g:Lcom/google/d/a/a/qp;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 81
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 412
    iput-byte v0, p0, Lcom/google/d/a/a/qp;->h:B

    .line 455
    iput v0, p0, Lcom/google/d/a/a/qp;->i:I

    .line 82
    iput v1, p0, Lcom/google/d/a/a/qp;->b:I

    .line 83
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    .line 84
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    .line 85
    iput v1, p0, Lcom/google/d/a/a/qp;->e:I

    .line 86
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/qp;->f:I

    .line 87
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v2, 0x1

    .line 93
    invoke-direct {p0}, Lcom/google/d/a/a/qp;-><init>()V

    .line 96
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 99
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 101
    sparse-switch v4, :sswitch_data_0

    .line 106
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 108
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 104
    goto :goto_0

    .line 113
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 114
    invoke-static {v4}, Lcom/google/d/a/a/qs;->a(I)Lcom/google/d/a/a/qs;

    move-result-object v5

    .line 115
    if-nez v5, :cond_3

    .line 116
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 162
    iget-object v2, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    .line 164
    :cond_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_2

    .line 165
    iget-object v1, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    .line 167
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/qp;->au:Lcom/google/n/bn;

    throw v0

    .line 118
    :cond_3
    :try_start_2
    iget v5, p0, Lcom/google/d/a/a/qp;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/qp;->a:I

    .line 119
    iput v4, p0, Lcom/google/d/a/a/qp;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 157
    :catch_1
    move-exception v0

    .line 158
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 159
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 124
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_4

    .line 125
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    .line 127
    or-int/lit8 v1, v1, 0x2

    .line 129
    :cond_4
    iget-object v4, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 130
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 129
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    :sswitch_3
    and-int/lit8 v4, v1, 0x4

    if-eq v4, v8, :cond_5

    .line 135
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    .line 137
    or-int/lit8 v1, v1, 0x4

    .line 139
    :cond_5
    iget-object v4, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 139
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 144
    :sswitch_4
    iget v4, p0, Lcom/google/d/a/a/qp;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/qp;->a:I

    .line 145
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/qp;->e:I

    goto/16 :goto_0

    .line 149
    :sswitch_5
    iget v4, p0, Lcom/google/d/a/a/qp;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/qp;->a:I

    .line 150
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/qp;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 161
    :cond_6
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_7

    .line 162
    iget-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    .line 164
    :cond_7
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v8, :cond_8

    .line 165
    iget-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    .line 167
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/qp;->au:Lcom/google/n/bn;

    .line 168
    return-void

    .line 101
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 79
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 412
    iput-byte v0, p0, Lcom/google/d/a/a/qp;->h:B

    .line 455
    iput v0, p0, Lcom/google/d/a/a/qp;->i:I

    .line 80
    return-void
.end method

.method public static d()Lcom/google/d/a/a/qp;
    .locals 1

    .prologue
    .line 1053
    sget-object v0, Lcom/google/d/a/a/qp;->g:Lcom/google/d/a/a/qp;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/qr;
    .locals 1

    .prologue
    .line 550
    new-instance v0, Lcom/google/d/a/a/qr;

    invoke-direct {v0}, Lcom/google/d/a/a/qr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/qp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    sget-object v0, Lcom/google/d/a/a/qp;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 436
    invoke-virtual {p0}, Lcom/google/d/a/a/qp;->c()I

    .line 437
    iget v0, p0, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 438
    iget v0, p0, Lcom/google/d/a/a/qp;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_0
    :goto_0
    move v1, v2

    .line 440
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 441
    iget-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 440
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 438
    :cond_1
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 443
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 444
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 443
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 446
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 447
    iget v0, p0, Lcom/google/d/a/a/qp;->e:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 449
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    .line 450
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/d/a/a/qp;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_7

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 452
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/qp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 453
    return-void

    .line 447
    :cond_6
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 450
    :cond_7
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 414
    iget-byte v0, p0, Lcom/google/d/a/a/qp;->h:B

    .line 415
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 431
    :cond_0
    :goto_0
    return v2

    .line 416
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 418
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 419
    iget-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 420
    iput-byte v2, p0, Lcom/google/d/a/a/qp;->h:B

    goto :goto_0

    .line 418
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 424
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 425
    iget-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 426
    iput-byte v2, p0, Lcom/google/d/a/a/qp;->h:B

    goto :goto_0

    .line 424
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 430
    :cond_5
    iput-byte v3, p0, Lcom/google/d/a/a/qp;->h:B

    move v2, v3

    .line 431
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 457
    iget v0, p0, Lcom/google/d/a/a/qp;->i:I

    .line 458
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 483
    :goto_0
    return v0

    .line 461
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 462
    iget v0, p0, Lcom/google/d/a/a/qp;->b:I

    .line 463
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v3, v2

    move v4, v0

    .line 465
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 466
    iget-object v0, p0, Lcom/google/d/a/a/qp;->c:Ljava/util/List;

    .line 467
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 465
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 463
    goto :goto_1

    :cond_2
    move v3, v2

    .line 469
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 470
    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/qp;->d:Ljava/util/List;

    .line 471
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 469
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 473
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 474
    iget v0, p0, Lcom/google/d/a/a/qp;->e:I

    .line 475
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 477
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/qp;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_6

    .line 478
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/d/a/a/qp;->f:I

    .line 479
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v3, :cond_5

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 481
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/qp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 482
    iput v0, p0, Lcom/google/d/a/a/qp;->i:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 475
    goto :goto_5

    :cond_8
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/d/a/a/qp;->newBuilder()Lcom/google/d/a/a/qr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/qr;->a(Lcom/google/d/a/a/qp;)Lcom/google/d/a/a/qr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/d/a/a/qp;->newBuilder()Lcom/google/d/a/a/qr;

    move-result-object v0

    return-object v0
.end method
