.class public final Lcom/google/d/a/a/jz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ke;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/jz;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/d/a/a/jz;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field d:I

.field e:Ljava/lang/Object;

.field f:F

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Z

.field i:Ljava/lang/Object;

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/ao;

.field l:Lcom/google/n/ao;

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 259
    new-instance v0, Lcom/google/d/a/a/ka;

    invoke-direct {v0}, Lcom/google/d/a/a/ka;-><init>()V

    sput-object v0, Lcom/google/d/a/a/jz;->PARSER:Lcom/google/n/ax;

    .line 974
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/jz;->p:Lcom/google/n/aw;

    .line 1891
    new-instance v0, Lcom/google/d/a/a/jz;

    invoke-direct {v0}, Lcom/google/d/a/a/jz;-><init>()V

    sput-object v0, Lcom/google/d/a/a/jz;->m:Lcom/google/d/a/a/jz;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 140
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 611
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    .line 799
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    .line 815
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->k:Lcom/google/n/ao;

    .line 831
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->l:Lcom/google/n/ao;

    .line 846
    iput-byte v3, p0, Lcom/google/d/a/a/jz;->n:B

    .line 917
    iput v3, p0, Lcom/google/d/a/a/jz;->o:I

    .line 141
    iput v2, p0, Lcom/google/d/a/a/jz;->b:I

    .line 142
    iget-object v0, p0, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 143
    iput v4, p0, Lcom/google/d/a/a/jz;->d:I

    .line 144
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jz;->e:Ljava/lang/Object;

    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/jz;->f:F

    .line 146
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    .line 147
    iput-boolean v4, p0, Lcom/google/d/a/a/jz;->h:Z

    .line 148
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jz;->i:Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 150
    iget-object v0, p0, Lcom/google/d/a/a/jz;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 151
    iget-object v0, p0, Lcom/google/d/a/a/jz;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 152
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 158
    invoke-direct {p0}, Lcom/google/d/a/a/jz;-><init>()V

    .line 161
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 164
    :cond_0
    :goto_0
    if-nez v4, :cond_5

    .line 165
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 166
    sparse-switch v0, :sswitch_data_0

    .line 171
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 173
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 169
    goto :goto_0

    .line 178
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 179
    invoke-static {v0}, Lcom/google/d/a/a/kc;->a(I)Lcom/google/d/a/a/kc;

    move-result-object v6

    .line 180
    if-nez v6, :cond_2

    .line 181
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x20

    if-ne v1, v10, :cond_1

    .line 254
    iget-object v1, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    .line 256
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/jz;->au:Lcom/google/n/bn;

    throw v0

    .line 183
    :cond_2
    :try_start_2
    iget v6, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/d/a/a/jz;->a:I

    .line 184
    iput v0, p0, Lcom/google/d/a/a/jz;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 249
    :catch_1
    move-exception v0

    .line 250
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 251
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 189
    :sswitch_2
    :try_start_4
    iget-object v0, p0, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 190
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/jz;->a:I

    goto :goto_0

    .line 194
    :sswitch_3
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/jz;->a:I

    .line 195
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/jz;->d:I

    goto :goto_0

    .line 199
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 200
    iget v6, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/d/a/a/jz;->a:I

    .line 201
    iput-object v0, p0, Lcom/google/d/a/a/jz;->e:Ljava/lang/Object;

    goto :goto_0

    .line 205
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/jz;->a:I

    .line 206
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/jz;->f:F

    goto/16 :goto_0

    .line 210
    :sswitch_6
    and-int/lit8 v0, v1, 0x20

    if-eq v0, v10, :cond_3

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    .line 213
    or-int/lit8 v1, v1, 0x20

    .line 215
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 216
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 215
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 220
    :sswitch_7
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/jz;->a:I

    .line 221
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/jz;->h:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    .line 225
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 226
    iget v6, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/d/a/a/jz;->a:I

    .line 227
    iput-object v0, p0, Lcom/google/d/a/a/jz;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 231
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 232
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/jz;->a:I

    goto/16 :goto_0

    .line 236
    :sswitch_a
    iget-object v0, p0, Lcom/google/d/a/a/jz;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 237
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/jz;->a:I

    goto/16 :goto_0

    .line 241
    :sswitch_b
    iget-object v0, p0, Lcom/google/d/a/a/jz;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 242
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/d/a/a/jz;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 253
    :cond_5
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v10, :cond_6

    .line 254
    iget-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    .line 256
    :cond_6
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jz;->au:Lcom/google/n/bn;

    .line 257
    return-void

    .line 166
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2d -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x7a -> :sswitch_8
        0x82 -> :sswitch_9
        0x8a -> :sswitch_a
        0xfa2 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 138
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 611
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    .line 799
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    .line 815
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->k:Lcom/google/n/ao;

    .line 831
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jz;->l:Lcom/google/n/ao;

    .line 846
    iput-byte v1, p0, Lcom/google/d/a/a/jz;->n:B

    .line 917
    iput v1, p0, Lcom/google/d/a/a/jz;->o:I

    .line 139
    return-void
.end method

.method public static d()Lcom/google/d/a/a/jz;
    .locals 1

    .prologue
    .line 1894
    sget-object v0, Lcom/google/d/a/a/jz;->m:Lcom/google/d/a/a/jz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/kb;
    .locals 1

    .prologue
    .line 1036
    new-instance v0, Lcom/google/d/a/a/kb;

    invoke-direct {v0}, Lcom/google/d/a/a/kb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/jz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    sget-object v0, Lcom/google/d/a/a/jz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 880
    invoke-virtual {p0}, Lcom/google/d/a/a/jz;->c()I

    .line 881
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 882
    iget v0, p0, Lcom/google/d/a/a/jz;->b:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 884
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 885
    iget-object v0, p0, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 887
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 888
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/jz;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_6

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 890
    :cond_2
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 891
    iget-object v0, p0, Lcom/google/d/a/a/jz;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jz;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 893
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 894
    iget v0, p0, Lcom/google/d/a/a/jz;->f:F

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    :cond_4
    move v1, v2

    .line 896
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 897
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 896
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 882
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 888
    :cond_6
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 891
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 899
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 900
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/d/a/a/jz;->h:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_9

    move v2, v3

    :cond_9
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 902
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 903
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/d/a/a/jz;->i:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jz;->i:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 905
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 906
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 908
    :cond_c
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_d

    .line 909
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/d/a/a/jz;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 911
    :cond_d
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_e

    .line 912
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/jz;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 914
    :cond_e
    iget-object v0, p0, Lcom/google/d/a/a/jz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 915
    return-void

    .line 903
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 848
    iget-byte v0, p0, Lcom/google/d/a/a/jz;->n:B

    .line 849
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 875
    :cond_0
    :goto_0
    return v2

    .line 850
    :cond_1
    if-eqz v0, :cond_0

    .line 852
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    .line 853
    iput-byte v2, p0, Lcom/google/d/a/a/jz;->n:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 852
    goto :goto_1

    .line 856
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 857
    iget-object v0, p0, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 858
    iput-byte v2, p0, Lcom/google/d/a/a/jz;->n:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 856
    goto :goto_2

    :cond_5
    move v1, v2

    .line 862
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 863
    iget-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 864
    iput-byte v2, p0, Lcom/google/d/a/a/jz;->n:B

    goto :goto_0

    .line 862
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 868
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_9

    .line 869
    iget-object v0, p0, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 870
    iput-byte v2, p0, Lcom/google/d/a/a/jz;->n:B

    goto :goto_0

    :cond_8
    move v0, v2

    .line 868
    goto :goto_4

    .line 874
    :cond_9
    iput-byte v3, p0, Lcom/google/d/a/a/jz;->n:B

    move v2, v3

    .line 875
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v1, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 919
    iget v0, p0, Lcom/google/d/a/a/jz;->o:I

    .line 920
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 969
    :goto_0
    return v0

    .line 923
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_f

    .line 924
    iget v0, p0, Lcom/google/d/a/a/jz;->b:I

    .line 925
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 927
    :goto_2
    iget v3, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 928
    iget-object v3, p0, Lcom/google/d/a/a/jz;->c:Lcom/google/n/ao;

    .line 929
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 931
    :cond_1
    iget v3, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_e

    .line 932
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/d/a/a/jz;->d:I

    .line 933
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 935
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 937
    iget-object v0, p0, Lcom/google/d/a/a/jz;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jz;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 939
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 940
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/d/a/a/jz;->f:F

    .line 941
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v1, v0

    :cond_4
    move v3, v1

    move v1, v2

    .line 943
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 944
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/jz;->g:Ljava/util/List;

    .line 945
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 943
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_5
    move v0, v1

    .line 925
    goto/16 :goto_1

    .line 937
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 947
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 948
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/d/a/a/jz;->h:Z

    .line 949
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 951
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 952
    const/16 v1, 0xf

    .line 953
    iget-object v0, p0, Lcom/google/d/a/a/jz;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/jz;->i:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 955
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 956
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/d/a/a/jz;->j:Lcom/google/n/ao;

    .line 957
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 959
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 960
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/d/a/a/jz;->k:Lcom/google/n/ao;

    .line 961
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 963
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/jz;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_c

    .line 964
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/jz;->l:Lcom/google/n/ao;

    .line 965
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 967
    :cond_c
    iget-object v0, p0, Lcom/google/d/a/a/jz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 968
    iput v0, p0, Lcom/google/d/a/a/jz;->o:I

    goto/16 :goto_0

    .line 953
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_e
    move v1, v0

    goto/16 :goto_3

    :cond_f
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 132
    invoke-static {}, Lcom/google/d/a/a/jz;->newBuilder()Lcom/google/d/a/a/kb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/kb;->a(Lcom/google/d/a/a/jz;)Lcom/google/d/a/a/kb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 132
    invoke-static {}, Lcom/google/d/a/a/jz;->newBuilder()Lcom/google/d/a/a/kb;

    move-result-object v0

    return-object v0
.end method
