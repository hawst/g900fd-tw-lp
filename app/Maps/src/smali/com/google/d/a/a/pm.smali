.class public final enum Lcom/google/d/a/a/pm;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/pm;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/pm;

.field public static final enum b:Lcom/google/d/a/a/pm;

.field private static final synthetic d:[Lcom/google/d/a/a/pm;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1526
    new-instance v0, Lcom/google/d/a/a/pm;

    const-string v1, "TYPE_OCCASION"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/d/a/a/pm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pm;->a:Lcom/google/d/a/a/pm;

    .line 1530
    new-instance v0, Lcom/google/d/a/a/pm;

    const-string v1, "TYPE_RANGE"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/pm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pm;->b:Lcom/google/d/a/a/pm;

    .line 1521
    new-array v0, v4, [Lcom/google/d/a/a/pm;

    sget-object v1, Lcom/google/d/a/a/pm;->a:Lcom/google/d/a/a/pm;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/pm;->b:Lcom/google/d/a/a/pm;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/d/a/a/pm;->d:[Lcom/google/d/a/a/pm;

    .line 1560
    new-instance v0, Lcom/google/d/a/a/pn;

    invoke-direct {v0}, Lcom/google/d/a/a/pn;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1569
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1570
    iput p3, p0, Lcom/google/d/a/a/pm;->c:I

    .line 1571
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/pm;
    .locals 1

    .prologue
    .line 1548
    packed-switch p0, :pswitch_data_0

    .line 1551
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1549
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/pm;->a:Lcom/google/d/a/a/pm;

    goto :goto_0

    .line 1550
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/pm;->b:Lcom/google/d/a/a/pm;

    goto :goto_0

    .line 1548
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/pm;
    .locals 1

    .prologue
    .line 1521
    const-class v0, Lcom/google/d/a/a/pm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pm;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/pm;
    .locals 1

    .prologue
    .line 1521
    sget-object v0, Lcom/google/d/a/a/pm;->d:[Lcom/google/d/a/a/pm;

    invoke-virtual {v0}, [Lcom/google/d/a/a/pm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/pm;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1544
    iget v0, p0, Lcom/google/d/a/a/pm;->c:I

    return v0
.end method
