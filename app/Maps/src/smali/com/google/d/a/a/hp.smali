.class public final Lcom/google/d/a/a/hp;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/hs;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hp;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/d/a/a/hp;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/google/d/a/a/hq;

    invoke-direct {v0}, Lcom/google/d/a/a/hq;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hp;->PARSER:Lcom/google/n/ax;

    .line 272
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/hp;->i:Lcom/google/n/aw;

    .line 625
    new-instance v0, Lcom/google/d/a/a/hp;

    invoke-direct {v0}, Lcom/google/d/a/a/hp;-><init>()V

    sput-object v0, Lcom/google/d/a/a/hp;->f:Lcom/google/d/a/a/hp;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 170
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hp;->d:Lcom/google/n/ao;

    .line 186
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    .line 201
    iput-byte v2, p0, Lcom/google/d/a/a/hp;->g:B

    .line 243
    iput v2, p0, Lcom/google/d/a/a/hp;->h:I

    .line 63
    iput v3, p0, Lcom/google/d/a/a/hp;->b:I

    .line 64
    iput v3, p0, Lcom/google/d/a/a/hp;->c:I

    .line 65
    iget-object v0, p0, Lcom/google/d/a/a/hp;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 66
    iget-object v0, p0, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 67
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Lcom/google/d/a/a/hp;-><init>()V

    .line 74
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 79
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 80
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 81
    sparse-switch v3, :sswitch_data_0

    .line 86
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 88
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 84
    goto :goto_0

    .line 93
    :sswitch_1
    iget v3, p0, Lcom/google/d/a/a/hp;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/hp;->a:I

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/hp;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/hp;->au:Lcom/google/n/bn;

    throw v0

    .line 98
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/d/a/a/hp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/hp;->a:I

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/hp;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 116
    :catch_1
    move-exception v0

    .line 117
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 118
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 104
    iget v3, p0, Lcom/google/d/a/a/hp;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/hp;->a:I

    goto :goto_0

    .line 108
    :sswitch_4
    iget-object v3, p0, Lcom/google/d/a/a/hp;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 109
    iget v3, p0, Lcom/google/d/a/a/hp;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/hp;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hp;->au:Lcom/google/n/bn;

    .line 121
    return-void

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x7a -> :sswitch_3
        0xfa2 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 60
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 170
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hp;->d:Lcom/google/n/ao;

    .line 186
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    .line 201
    iput-byte v1, p0, Lcom/google/d/a/a/hp;->g:B

    .line 243
    iput v1, p0, Lcom/google/d/a/a/hp;->h:I

    .line 61
    return-void
.end method

.method public static a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;
    .locals 1

    .prologue
    .line 337
    invoke-static {}, Lcom/google/d/a/a/hp;->newBuilder()Lcom/google/d/a/a/hr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/d/a/a/hp;
    .locals 1

    .prologue
    .line 628
    sget-object v0, Lcom/google/d/a/a/hp;->f:Lcom/google/d/a/a/hp;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/hr;
    .locals 1

    .prologue
    .line 334
    new-instance v0, Lcom/google/d/a/a/hr;

    invoke-direct {v0}, Lcom/google/d/a/a/hr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/hp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    sget-object v0, Lcom/google/d/a/a/hp;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 227
    invoke-virtual {p0}, Lcom/google/d/a/a/hp;->c()I

    .line 228
    iget v0, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 229
    iget v0, p0, Lcom/google/d/a/a/hp;->b:I

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 231
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 232
    iget v0, p0, Lcom/google/d/a/a/hp;->c:I

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 234
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 235
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 237
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 238
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/hp;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 240
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/hp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 241
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    iget-byte v0, p0, Lcom/google/d/a/a/hp;->g:B

    .line 204
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 222
    :goto_0
    return v0

    .line 205
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 207
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 208
    iput-byte v2, p0, Lcom/google/d/a/a/hp;->g:B

    move v0, v2

    .line 209
    goto :goto_0

    :cond_2
    move v0, v2

    .line 207
    goto :goto_1

    .line 211
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    .line 212
    iput-byte v2, p0, Lcom/google/d/a/a/hp;->g:B

    move v0, v2

    .line 213
    goto :goto_0

    :cond_4
    move v0, v2

    .line 211
    goto :goto_2

    .line 215
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 216
    iget-object v0, p0, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 217
    iput-byte v2, p0, Lcom/google/d/a/a/hp;->g:B

    move v0, v2

    .line 218
    goto :goto_0

    :cond_6
    move v0, v2

    .line 215
    goto :goto_3

    .line 221
    :cond_7
    iput-byte v1, p0, Lcom/google/d/a/a/hp;->g:B

    move v0, v1

    .line 222
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 245
    iget v0, p0, Lcom/google/d/a/a/hp;->h:I

    .line 246
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 267
    :goto_0
    return v0

    .line 249
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 250
    iget v0, p0, Lcom/google/d/a/a/hp;->b:I

    .line 251
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 253
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 254
    iget v2, p0, Lcom/google/d/a/a/hp;->c:I

    .line 255
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 257
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 258
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/d/a/a/hp;->e:Lcom/google/n/ao;

    .line 259
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 261
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/hp;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 262
    const/16 v2, 0x1f4

    iget-object v3, p0, Lcom/google/d/a/a/hp;->d:Lcom/google/n/ao;

    .line 263
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 265
    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/hp;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 266
    iput v0, p0, Lcom/google/d/a/a/hp;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/d/a/a/hp;->newBuilder()Lcom/google/d/a/a/hr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/d/a/a/hp;->newBuilder()Lcom/google/d/a/a/hr;

    move-result-object v0

    return-object v0
.end method
