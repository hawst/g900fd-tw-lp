.class public final Lcom/google/d/a/a/pj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/pq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/pj;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/d/a/a/pj;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Z

.field d:I

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1503
    new-instance v0, Lcom/google/d/a/a/pk;

    invoke-direct {v0}, Lcom/google/d/a/a/pk;-><init>()V

    sput-object v0, Lcom/google/d/a/a/pj;->PARSER:Lcom/google/n/ax;

    .line 1985
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/pj;->j:Lcom/google/n/aw;

    .line 2373
    new-instance v0, Lcom/google/d/a/a/pj;

    invoke-direct {v0}, Lcom/google/d/a/a/pj;-><init>()V

    sput-object v0, Lcom/google/d/a/a/pj;->g:Lcom/google/d/a/a/pj;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1424
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1890
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    .line 1906
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    .line 1921
    iput-byte v3, p0, Lcom/google/d/a/a/pj;->h:B

    .line 1952
    iput v3, p0, Lcom/google/d/a/a/pj;->i:I

    .line 1425
    iput v2, p0, Lcom/google/d/a/a/pj;->b:I

    .line 1426
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/d/a/a/pj;->c:Z

    .line 1427
    iput v2, p0, Lcom/google/d/a/a/pj;->d:I

    .line 1428
    iget-object v0, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 1429
    iget-object v0, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 1430
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1436
    invoke-direct {p0}, Lcom/google/d/a/a/pj;-><init>()V

    .line 1437
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 1442
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 1443
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1444
    sparse-switch v0, :sswitch_data_0

    .line 1449
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 1451
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 1447
    goto :goto_0

    .line 1456
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 1457
    invoke-static {v0}, Lcom/google/d/a/a/pm;->a(I)Lcom/google/d/a/a/pm;

    move-result-object v5

    .line 1458
    if-nez v5, :cond_1

    .line 1459
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1494
    :catch_0
    move-exception v0

    .line 1495
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1500
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/pj;->au:Lcom/google/n/bn;

    throw v0

    .line 1461
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/d/a/a/pj;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/pj;->a:I

    .line 1462
    iput v0, p0, Lcom/google/d/a/a/pj;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1496
    :catch_1
    move-exception v0

    .line 1497
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1498
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1467
    :sswitch_2
    :try_start_4
    iget v0, p0, Lcom/google/d/a/a/pj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/pj;->a:I

    .line 1468
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/pj;->c:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 1472
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 1473
    invoke-static {v0}, Lcom/google/d/a/a/po;->a(I)Lcom/google/d/a/a/po;

    move-result-object v5

    .line 1474
    if-nez v5, :cond_3

    .line 1475
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 1477
    :cond_3
    iget v5, p0, Lcom/google/d/a/a/pj;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/d/a/a/pj;->a:I

    .line 1478
    iput v0, p0, Lcom/google/d/a/a/pj;->d:I

    goto :goto_0

    .line 1483
    :sswitch_4
    iget-object v0, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 1484
    iget v0, p0, Lcom/google/d/a/a/pj;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/pj;->a:I

    goto/16 :goto_0

    .line 1488
    :sswitch_5
    iget-object v0, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 1489
    iget v0, p0, Lcom/google/d/a/a/pj;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/pj;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1500
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/pj;->au:Lcom/google/n/bn;

    .line 1501
    return-void

    .line 1444
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1422
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1890
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    .line 1906
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    .line 1921
    iput-byte v1, p0, Lcom/google/d/a/a/pj;->h:B

    .line 1952
    iput v1, p0, Lcom/google/d/a/a/pj;->i:I

    .line 1423
    return-void
.end method

.method public static d()Lcom/google/d/a/a/pj;
    .locals 1

    .prologue
    .line 2376
    sget-object v0, Lcom/google/d/a/a/pj;->g:Lcom/google/d/a/a/pj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/pl;
    .locals 1

    .prologue
    .line 2047
    new-instance v0, Lcom/google/d/a/a/pl;

    invoke-direct {v0}, Lcom/google/d/a/a/pl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/pj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1515
    sget-object v0, Lcom/google/d/a/a/pj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 1933
    invoke-virtual {p0}, Lcom/google/d/a/a/pj;->c()I

    .line 1934
    iget v2, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 1935
    iget v2, p0, Lcom/google/d/a/a/pj;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_5

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 1937
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1938
    iget-boolean v2, p0, Lcom/google/d/a/a/pj;->c:Z

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_6

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1940
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    .line 1941
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/d/a/a/pj;->d:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_7

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 1943
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1944
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1946
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1947
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1949
    :cond_4
    iget-object v0, p0, Lcom/google/d/a/a/pj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1950
    return-void

    .line 1935
    :cond_5
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 1938
    goto :goto_1

    .line 1941
    :cond_7
    int-to-long v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1923
    iget-byte v1, p0, Lcom/google/d/a/a/pj;->h:B

    .line 1924
    if-ne v1, v0, :cond_0

    .line 1928
    :goto_0
    return v0

    .line 1925
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1927
    :cond_1
    iput-byte v0, p0, Lcom/google/d/a/a/pj;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1954
    iget v0, p0, Lcom/google/d/a/a/pj;->i:I

    .line 1955
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1980
    :goto_0
    return v0

    .line 1958
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 1959
    iget v0, p0, Lcom/google/d/a/a/pj;->b:I

    .line 1960
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 1962
    :goto_2
    iget v3, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 1963
    iget-boolean v3, p0, Lcom/google/d/a/a/pj;->c:Z

    .line 1964
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 1966
    :cond_1
    iget v3, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    .line 1967
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/d/a/a/pj;->d:I

    .line 1968
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1970
    :cond_3
    iget v1, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    .line 1971
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    .line 1972
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1974
    :cond_4
    iget v1, p0, Lcom/google/d/a/a/pj;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_5

    .line 1975
    const/4 v1, 0x7

    iget-object v3, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    .line 1976
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1978
    :cond_5
    iget-object v1, p0, Lcom/google/d/a/a/pj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1979
    iput v0, p0, Lcom/google/d/a/a/pj;->i:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 1960
    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1416
    invoke-static {}, Lcom/google/d/a/a/pj;->newBuilder()Lcom/google/d/a/a/pl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/pl;->a(Lcom/google/d/a/a/pj;)Lcom/google/d/a/a/pl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1416
    invoke-static {}, Lcom/google/d/a/a/pj;->newBuilder()Lcom/google/d/a/a/pl;

    move-result-object v0

    return-object v0
.end method
