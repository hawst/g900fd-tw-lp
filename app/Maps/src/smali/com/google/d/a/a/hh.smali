.class public final Lcom/google/d/a/a/hh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/hi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/hf;",
        "Lcom/google/d/a/a/hh;",
        ">;",
        "Lcom/google/d/a/a/hi;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:I

.field private f:I

.field private g:Z

.field private h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 633
    sget-object v0, Lcom/google/d/a/a/hf;->l:Lcom/google/d/a/a/hf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 804
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hh;->b:Lcom/google/n/ao;

    .line 863
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hh;->c:Lcom/google/n/ao;

    .line 1083
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    .line 1219
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hh;->j:Lcom/google/n/ao;

    .line 1278
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/hh;->k:Lcom/google/n/ao;

    .line 634
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/hf;)Lcom/google/d/a/a/hh;
    .locals 5

    .prologue
    const/16 v4, 0x80

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 722
    invoke-static {}, Lcom/google/d/a/a/hf;->d()Lcom/google/d/a/a/hf;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 765
    :goto_0
    return-object p0

    .line 723
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 724
    iget-object v2, p0, Lcom/google/d/a/a/hh;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 725
    iget v2, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/hh;->a:I

    .line 727
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 728
    iget-object v2, p0, Lcom/google/d/a/a/hh;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 729
    iget v2, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/hh;->a:I

    .line 731
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 732
    iget v2, p1, Lcom/google/d/a/a/hf;->d:I

    iget v3, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/hh;->a:I

    iput v2, p0, Lcom/google/d/a/a/hh;->d:I

    .line 734
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 735
    iget v2, p1, Lcom/google/d/a/a/hf;->e:I

    iget v3, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/hh;->a:I

    iput v2, p0, Lcom/google/d/a/a/hh;->e:I

    .line 737
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 738
    iget v2, p1, Lcom/google/d/a/a/hf;->f:I

    iget v3, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/hh;->a:I

    iput v2, p0, Lcom/google/d/a/a/hh;->f:I

    .line 740
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 741
    iget-boolean v2, p1, Lcom/google/d/a/a/hf;->g:Z

    iget v3, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/hh;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/hh;->g:Z

    .line 743
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 744
    iget v2, p1, Lcom/google/d/a/a/hf;->h:I

    iget v3, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/hh;->a:I

    iput v2, p0, Lcom/google/d/a/a/hh;->h:I

    .line 746
    :cond_7
    iget-object v2, p1, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 747
    iget-object v2, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 748
    iget-object v2, p1, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    .line 749
    iget v2, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/d/a/a/hh;->a:I

    .line 756
    :cond_8
    :goto_8
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit16 v2, v2, 0x80

    if-ne v2, v4, :cond_14

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 757
    iget-object v2, p0, Lcom/google/d/a/a/hh;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 758
    iget v2, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/d/a/a/hh;->a:I

    .line 760
    :cond_9
    iget v2, p1, Lcom/google/d/a/a/hf;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_15

    :goto_a
    if-eqz v0, :cond_a

    .line 761
    iget-object v0, p0, Lcom/google/d/a/a/hh;->k:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 762
    iget v0, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/d/a/a/hh;->a:I

    .line 764
    :cond_a
    iget-object v0, p1, Lcom/google/d/a/a/hf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 723
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 727
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 731
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 734
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 737
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 740
    goto/16 :goto_6

    :cond_11
    move v2, v1

    .line 743
    goto :goto_7

    .line 751
    :cond_12
    iget v2, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eq v2, v4, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/hh;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/d/a/a/hh;->a:I

    .line 752
    :cond_13
    iget-object v2, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_14
    move v2, v1

    .line 756
    goto :goto_9

    :cond_15
    move v0, v1

    .line 760
    goto :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 625
    new-instance v2, Lcom/google/d/a/a/hf;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/hf;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/hf;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/hh;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/hh;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/d/a/a/hf;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/hh;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/hh;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/hh;->d:I

    iput v4, v2, Lcom/google/d/a/a/hf;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/d/a/a/hh;->e:I

    iput v4, v2, Lcom/google/d/a/a/hf;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/d/a/a/hh;->f:I

    iput v4, v2, Lcom/google/d/a/a/hf;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v4, p0, Lcom/google/d/a/a/hh;->g:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/hf;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/d/a/a/hh;->h:I

    iput v4, v2, Lcom/google/d/a/a/hf;->h:I

    iget v4, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/d/a/a/hh;->a:I

    :cond_6
    iget-object v4, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/hf;->i:Ljava/util/List;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v4, v2, Lcom/google/d/a/a/hf;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/hh;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/hh;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v3, v2, Lcom/google/d/a/a/hf;->k:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/hh;->k:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/hh;->k:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/hf;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 625
    check-cast p1, Lcom/google/d/a/a/hf;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/hh;->a(Lcom/google/d/a/a/hf;)Lcom/google/d/a/a/hh;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 769
    iget v0, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 770
    iget-object v0, p0, Lcom/google/d/a/a/hh;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 799
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 769
    goto :goto_0

    .line 775
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 776
    iget-object v0, p0, Lcom/google/d/a/a/hh;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 781
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 782
    iget-object v0, p0, Lcom/google/d/a/a/hh;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 775
    goto :goto_2

    .line 787
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_6

    .line 788
    iget-object v0, p0, Lcom/google/d/a/a/hh;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hb;->d()Lcom/google/d/a/a/hb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hb;

    invoke-virtual {v0}, Lcom/google/d/a/a/hb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 793
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/hh;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_5
    if-eqz v0, :cond_7

    .line 794
    iget-object v0, p0, Lcom/google/d/a/a/hh;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hb;->d()Lcom/google/d/a/a/hb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hb;

    invoke-virtual {v0}, Lcom/google/d/a/a/hb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_7
    move v2, v3

    .line 799
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 787
    goto :goto_4

    :cond_9
    move v0, v2

    .line 793
    goto :goto_5
.end method
