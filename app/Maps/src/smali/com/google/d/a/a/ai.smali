.class public final Lcom/google/d/a/a/ai;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/an;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final v:Lcom/google/d/a/a/ai;

.field private static volatile y:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:I

.field f:Ljava/lang/Object;

.field g:I

.field h:J

.field i:I

.field j:D

.field k:F

.field l:Z

.field m:Ljava/lang/Object;

.field n:Lcom/google/n/ao;

.field o:Ljava/lang/Object;

.field p:I

.field q:Ljava/lang/Object;

.field r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field t:Lcom/google/n/ao;

.field u:Lcom/google/n/ao;

.field private w:B

.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 419
    new-instance v0, Lcom/google/d/a/a/aj;

    invoke-direct {v0}, Lcom/google/d/a/a/aj;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ai;->PARSER:Lcom/google/n/ax;

    .line 1249
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/ai;->y:Lcom/google/n/aw;

    .line 2812
    new-instance v0, Lcom/google/d/a/a/ai;

    invoke-direct {v0}, Lcom/google/d/a/a/ai;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ai;->v:Lcom/google/d/a/a/ai;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 236
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 576
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    .line 592
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    .line 608
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    .line 814
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    .line 1015
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    .line 1031
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->u:Lcom/google/n/ao;

    .line 1046
    iput-byte v1, p0, Lcom/google/d/a/a/ai;->w:B

    .line 1156
    iput v1, p0, Lcom/google/d/a/a/ai;->x:I

    .line 237
    iget-object v0, p0, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 238
    iget-object v0, p0, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 239
    iget-object v0, p0, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 240
    iput v4, p0, Lcom/google/d/a/a/ai;->e:I

    .line 241
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ai;->f:Ljava/lang/Object;

    .line 242
    iput v4, p0, Lcom/google/d/a/a/ai;->g:I

    .line 243
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/ai;->h:J

    .line 244
    iput v4, p0, Lcom/google/d/a/a/ai;->i:I

    .line 245
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/ai;->j:D

    .line 246
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/ai;->k:F

    .line 247
    iput-boolean v4, p0, Lcom/google/d/a/a/ai;->l:Z

    .line 248
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ai;->m:Ljava/lang/Object;

    .line 249
    iget-object v0, p0, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 250
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ai;->o:Ljava/lang/Object;

    .line 251
    iput v4, p0, Lcom/google/d/a/a/ai;->p:I

    .line 252
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ai;->q:Ljava/lang/Object;

    .line 253
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    .line 254
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    .line 255
    iget-object v0, p0, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 256
    iget-object v0, p0, Lcom/google/d/a/a/ai;->u:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 257
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/high16 v11, 0x20000

    const/high16 v10, 0x10000

    const/4 v3, 0x0

    .line 263
    invoke-direct {p0}, Lcom/google/d/a/a/ai;-><init>()V

    .line 266
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 269
    :cond_0
    :goto_0
    if-nez v4, :cond_7

    .line 270
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 271
    sparse-switch v0, :sswitch_data_0

    .line 276
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 278
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 274
    goto :goto_0

    .line 283
    :sswitch_1
    iget-object v0, p0, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 284
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 404
    :catch_0
    move-exception v0

    .line 405
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410
    :catchall_0
    move-exception v0

    and-int v2, v1, v10

    if-ne v2, v10, :cond_1

    .line 411
    iget-object v2, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    .line 413
    :cond_1
    and-int/2addr v1, v11

    if-ne v1, v11, :cond_2

    .line 414
    iget-object v1, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    .line 416
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ai;->au:Lcom/google/n/bn;

    throw v0

    .line 288
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 289
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 406
    :catch_1
    move-exception v0

    .line 407
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 408
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 293
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 294
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    goto :goto_0

    .line 298
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 299
    invoke-static {v0}, Lcom/google/d/a/a/al;->a(I)Lcom/google/d/a/a/al;

    move-result-object v6

    .line 300
    if-nez v6, :cond_3

    .line 301
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 303
    :cond_3
    iget v6, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/d/a/a/ai;->a:I

    .line 304
    iput v0, p0, Lcom/google/d/a/a/ai;->e:I

    goto/16 :goto_0

    .line 309
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 310
    iget v6, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/d/a/a/ai;->a:I

    .line 311
    iput-object v0, p0, Lcom/google/d/a/a/ai;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 315
    :sswitch_6
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    .line 316
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/ai;->g:I

    goto/16 :goto_0

    .line 320
    :sswitch_7
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    .line 321
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/d/a/a/ai;->j:D

    goto/16 :goto_0

    .line 325
    :sswitch_8
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    .line 326
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/ai;->l:Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    .line 330
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 331
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    goto/16 :goto_0

    .line 335
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 336
    iget v6, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit16 v6, v6, 0x2000

    iput v6, p0, Lcom/google/d/a/a/ai;->a:I

    .line 337
    iput-object v0, p0, Lcom/google/d/a/a/ai;->o:Ljava/lang/Object;

    goto/16 :goto_0

    .line 341
    :sswitch_b
    and-int v0, v1, v10

    if-eq v0, v10, :cond_5

    .line 342
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    .line 344
    or-int/2addr v1, v10

    .line 346
    :cond_5
    iget-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 347
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 346
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 351
    :sswitch_c
    and-int v0, v1, v11

    if-eq v0, v11, :cond_6

    .line 352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    .line 354
    or-int/2addr v1, v11

    .line 356
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 357
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 356
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 361
    :sswitch_d
    iget-object v0, p0, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 362
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/2addr v0, v10

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    goto/16 :goto_0

    .line 366
    :sswitch_e
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    .line 367
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/d/a/a/ai;->h:J

    goto/16 :goto_0

    .line 371
    :sswitch_f
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    .line 372
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/ai;->k:F

    goto/16 :goto_0

    .line 376
    :sswitch_10
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    .line 377
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/ai;->p:I

    goto/16 :goto_0

    .line 381
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 382
    iget v6, p0, Lcom/google/d/a/a/ai;->a:I

    const v7, 0x8000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/d/a/a/ai;->a:I

    .line 383
    iput-object v0, p0, Lcom/google/d/a/a/ai;->q:Ljava/lang/Object;

    goto/16 :goto_0

    .line 387
    :sswitch_12
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I

    .line 388
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    iput v0, p0, Lcom/google/d/a/a/ai;->i:I

    goto/16 :goto_0

    .line 392
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 393
    iget v6, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/d/a/a/ai;->a:I

    .line 394
    iput-object v0, p0, Lcom/google/d/a/a/ai;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 398
    :sswitch_14
    iget-object v0, p0, Lcom/google/d/a/a/ai;->u:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 399
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    or-int/2addr v0, v11

    iput v0, p0, Lcom/google/d/a/a/ai;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 410
    :cond_7
    and-int v0, v1, v10

    if-ne v0, v10, :cond_8

    .line 411
    iget-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    .line 413
    :cond_8
    and-int v0, v1, v11

    if-ne v0, v11, :cond_9

    .line 414
    iget-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    .line 416
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->au:Lcom/google/n/bn;

    .line 417
    return-void

    .line 271
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x39 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x7d -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x95 -> :sswitch_12
        0x9a -> :sswitch_13
        0xfa2 -> :sswitch_14
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 234
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 576
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    .line 592
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    .line 608
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    .line 814
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    .line 1015
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    .line 1031
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ai;->u:Lcom/google/n/ao;

    .line 1046
    iput-byte v1, p0, Lcom/google/d/a/a/ai;->w:B

    .line 1156
    iput v1, p0, Lcom/google/d/a/a/ai;->x:I

    .line 235
    return-void
.end method

.method public static d()Lcom/google/d/a/a/ai;
    .locals 1

    .prologue
    .line 2815
    sget-object v0, Lcom/google/d/a/a/ai;->v:Lcom/google/d/a/a/ai;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ak;
    .locals 1

    .prologue
    .line 1311
    new-instance v0, Lcom/google/d/a/a/ak;

    invoke-direct {v0}, Lcom/google/d/a/a/ak;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 431
    sget-object v0, Lcom/google/d/a/a/ai;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v4, 0x4

    const/4 v7, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 1092
    invoke-virtual {p0}, Lcom/google/d/a/a/ai;->c()I

    .line 1093
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1094
    iget-object v0, p0, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1096
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 1097
    iget-object v0, p0, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1099
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 1100
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1102
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 1103
    iget v0, p0, Lcom/google/d/a/a/ai;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_a

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1105
    :cond_3
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1106
    iget-object v0, p0, Lcom/google/d/a/a/ai;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->f:Ljava/lang/Object;

    :goto_1
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1108
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 1109
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/d/a/a/ai;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_c

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 1111
    :cond_5
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_6

    .line 1112
    const/4 v0, 0x7

    iget-wide v4, p0, Lcom/google/d/a/a/ai;->j:D

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->c(J)V

    .line 1114
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_7

    .line 1115
    const/16 v0, 0x8

    iget-boolean v3, p0, Lcom/google/d/a/a/ai;->l:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_d

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1117
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_8

    .line 1118
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1120
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_9

    .line 1121
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/d/a/a/ai;->o:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->o:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_9
    move v1, v2

    .line 1123
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 1124
    const/16 v3, 0xb

    iget-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1123
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1103
    :cond_a
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 1106
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1109
    :cond_c
    int-to-long v4, v3

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    :cond_d
    move v0, v2

    .line 1115
    goto/16 :goto_3

    .line 1121
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_f
    move v1, v2

    .line 1126
    :goto_6
    iget-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 1127
    const/16 v3, 0xc

    iget-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1126
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1129
    :cond_10
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_11

    .line 1130
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1132
    :cond_11
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_12

    .line 1133
    const/16 v0, 0xe

    iget-wide v4, p0, Lcom/google/d/a/a/ai;->h:J

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 1135
    :cond_12
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_13

    .line 1136
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/d/a/a/ai;->k:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 1138
    :cond_13
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_14

    .line 1139
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/d/a/a/ai;->p:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_19

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1141
    :cond_14
    :goto_7
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_15

    .line 1142
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/d/a/a/ai;->q:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->q:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1144
    :cond_15
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_16

    .line 1145
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/d/a/a/ai;->i:I

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 1147
    :cond_16
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_17

    .line 1148
    const/16 v1, 0x13

    iget-object v0, p0, Lcom/google/d/a/a/ai;->m:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->m:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1150
    :cond_17
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_18

    .line 1151
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/ai;->u:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1153
    :cond_18
    iget-object v0, p0, Lcom/google/d/a/a/ai;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1154
    return-void

    .line 1139
    :cond_19
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_7

    .line 1142
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 1148
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto :goto_9
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/high16 v4, 0x10000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1048
    iget-byte v0, p0, Lcom/google/d/a/a/ai;->w:B

    .line 1049
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1087
    :goto_0
    return v0

    .line 1050
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1052
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 1053
    iput-byte v2, p0, Lcom/google/d/a/a/ai;->w:B

    move v0, v2

    .line 1054
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1052
    goto :goto_1

    .line 1056
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 1057
    iget-object v0, p0, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ac;->d()Lcom/google/d/a/a/ac;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ac;

    invoke-virtual {v0}, Lcom/google/d/a/a/ac;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1058
    iput-byte v2, p0, Lcom/google/d/a/a/ai;->w:B

    move v0, v2

    .line 1059
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1056
    goto :goto_2

    .line 1062
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 1063
    iget-object v0, p0, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ac;->d()Lcom/google/d/a/a/ac;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ac;

    invoke-virtual {v0}, Lcom/google/d/a/a/ac;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1064
    iput-byte v2, p0, Lcom/google/d/a/a/ai;->w:B

    move v0, v2

    .line 1065
    goto :goto_0

    :cond_6
    move v0, v2

    .line 1062
    goto :goto_3

    .line 1068
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 1069
    iget-object v0, p0, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ac;->d()Lcom/google/d/a/a/ac;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ac;

    invoke-virtual {v0}, Lcom/google/d/a/a/ac;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1070
    iput-byte v2, p0, Lcom/google/d/a/a/ai;->w:B

    move v0, v2

    .line 1071
    goto :goto_0

    :cond_8
    move v0, v2

    .line 1068
    goto :goto_4

    .line 1074
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_b

    .line 1075
    iget-object v0, p0, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1076
    iput-byte v2, p0, Lcom/google/d/a/a/ai;->w:B

    move v0, v2

    .line 1077
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 1074
    goto :goto_5

    .line 1080
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_c

    move v0, v1

    :goto_6
    if-eqz v0, :cond_d

    .line 1081
    iget-object v0, p0, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1082
    iput-byte v2, p0, Lcom/google/d/a/a/ai;->w:B

    move v0, v2

    .line 1083
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 1080
    goto :goto_6

    .line 1086
    :cond_d
    iput-byte v1, p0, Lcom/google/d/a/a/ai;->w:B

    move v0, v1

    .line 1087
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1158
    iget v0, p0, Lcom/google/d/a/a/ai;->x:I

    .line 1159
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1244
    :goto_0
    return v0

    .line 1162
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1b

    .line 1163
    iget-object v0, p0, Lcom/google/d/a/a/ai;->b:Lcom/google/n/ao;

    .line 1164
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1166
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 1167
    iget-object v2, p0, Lcom/google/d/a/a/ai;->c:Lcom/google/n/ao;

    .line 1168
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1170
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 1171
    const/4 v2, 0x3

    iget-object v4, p0, Lcom/google/d/a/a/ai;->d:Lcom/google/n/ao;

    .line 1172
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1174
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_1a

    .line 1175
    iget v2, p0, Lcom/google/d/a/a/ai;->e:I

    .line 1176
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 1178
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_3

    .line 1179
    const/4 v4, 0x5

    .line 1180
    iget-object v0, p0, Lcom/google/d/a/a/ai;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1182
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_4

    .line 1183
    const/4 v0, 0x6

    iget v4, p0, Lcom/google/d/a/a/ai;->g:I

    .line 1184
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1186
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_5

    .line 1187
    const/4 v0, 0x7

    iget-wide v4, p0, Lcom/google/d/a/a/ai;->j:D

    .line 1188
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v2, v0

    .line 1190
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_6

    .line 1191
    const/16 v0, 0x8

    iget-boolean v4, p0, Lcom/google/d/a/a/ai;->l:Z

    .line 1192
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 1194
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_7

    .line 1195
    const/16 v0, 0x9

    iget-object v4, p0, Lcom/google/d/a/a/ai;->n:Lcom/google/n/ao;

    .line 1196
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1198
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v0, v4, :cond_8

    .line 1200
    iget-object v0, p0, Lcom/google/d/a/a/ai;->o:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->o:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    :cond_8
    move v4, v2

    move v2, v1

    .line 1202
    :goto_7
    iget-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_d

    .line 1203
    const/16 v5, 0xb

    iget-object v0, p0, Lcom/google/d/a/a/ai;->r:Ljava/util/List;

    .line 1204
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1202
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    :cond_9
    move v2, v3

    .line 1176
    goto/16 :goto_2

    .line 1180
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    :cond_b
    move v0, v3

    .line 1184
    goto/16 :goto_5

    .line 1200
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_d
    move v2, v1

    .line 1206
    :goto_8
    iget-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 1207
    const/16 v5, 0xc

    iget-object v0, p0, Lcom/google/d/a/a/ai;->s:Ljava/util/List;

    .line 1208
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1206
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 1210
    :cond_e
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_f

    .line 1211
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/google/d/a/a/ai;->t:Lcom/google/n/ao;

    .line 1212
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1214
    :cond_f
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_10

    .line 1215
    const/16 v0, 0xe

    iget-wide v6, p0, Lcom/google/d/a/a/ai;->h:J

    .line 1216
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v6, v7}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1218
    :cond_10
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_11

    .line 1219
    const/16 v0, 0xf

    iget v2, p0, Lcom/google/d/a/a/ai;->k:F

    .line 1220
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 1222
    :cond_11
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v2, 0x4000

    if-ne v0, v2, :cond_13

    .line 1223
    const/16 v0, 0x10

    iget v2, p0, Lcom/google/d/a/a/ai;->p:I

    .line 1224
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v2, :cond_12

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_12
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 1226
    :cond_13
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    const v2, 0x8000

    and-int/2addr v0, v2

    const v2, 0x8000

    if-ne v0, v2, :cond_14

    .line 1227
    const/16 v2, 0x11

    .line 1228
    iget-object v0, p0, Lcom/google/d/a/a/ai;->q:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->q:Ljava/lang/Object;

    :goto_9
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1230
    :cond_14
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_15

    .line 1231
    const/16 v0, 0x12

    iget v2, p0, Lcom/google/d/a/a/ai;->i:I

    .line 1232
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 1234
    :cond_15
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_16

    .line 1235
    const/16 v2, 0x13

    .line 1236
    iget-object v0, p0, Lcom/google/d/a/a/ai;->m:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ai;->m:Ljava/lang/Object;

    :goto_a
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1238
    :cond_16
    iget v0, p0, Lcom/google/d/a/a/ai;->a:I

    const/high16 v2, 0x20000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000

    if-ne v0, v2, :cond_17

    .line 1239
    const/16 v0, 0x1f4

    iget-object v2, p0, Lcom/google/d/a/a/ai;->u:Lcom/google/n/ao;

    .line 1240
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1242
    :cond_17
    iget-object v0, p0, Lcom/google/d/a/a/ai;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1243
    iput v0, p0, Lcom/google/d/a/a/ai;->x:I

    goto/16 :goto_0

    .line 1228
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 1236
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto :goto_a

    :cond_1a
    move v2, v0

    goto/16 :goto_3

    :cond_1b
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 228
    invoke-static {}, Lcom/google/d/a/a/ai;->newBuilder()Lcom/google/d/a/a/ak;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ak;->a(Lcom/google/d/a/a/ai;)Lcom/google/d/a/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 228
    invoke-static {}, Lcom/google/d/a/a/ai;->newBuilder()Lcom/google/d/a/a/ak;

    move-result-object v0

    return-object v0
.end method
