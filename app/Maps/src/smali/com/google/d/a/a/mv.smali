.class public final enum Lcom/google/d/a/a/mv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/mv;

.field public static final enum b:Lcom/google/d/a/a/mv;

.field public static final enum c:Lcom/google/d/a/a/mv;

.field public static final enum d:Lcom/google/d/a/a/mv;

.field public static final enum e:Lcom/google/d/a/a/mv;

.field public static final enum f:Lcom/google/d/a/a/mv;

.field public static final enum g:Lcom/google/d/a/a/mv;

.field public static final enum h:Lcom/google/d/a/a/mv;

.field public static final enum i:Lcom/google/d/a/a/mv;

.field private static final synthetic k:[Lcom/google/d/a/a/mv;


# instance fields
.field public final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1381
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_NON_TRAFFIC"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->a:Lcom/google/d/a/a/mv;

    .line 1385
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_TERMINAL"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->b:Lcom/google/d/a/a/mv;

    .line 1389
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_LOCAL"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->c:Lcom/google/d/a/a/mv;

    .line 1393
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_MINOR_ARTERIAL"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->d:Lcom/google/d/a/a/mv;

    .line 1397
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_MAJOR_ARTERIAL"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->e:Lcom/google/d/a/a/mv;

    .line 1401
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_SECONDARY_ROAD"

    const/4 v2, 0x5

    const/16 v3, 0x60

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->f:Lcom/google/d/a/a/mv;

    .line 1405
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_PRIMARY_HIGHWAY"

    const/4 v2, 0x6

    const/16 v3, 0x70

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->g:Lcom/google/d/a/a/mv;

    .line 1409
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_LIMITED_ACCESS"

    const/4 v2, 0x7

    const/16 v3, 0x80

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->h:Lcom/google/d/a/a/mv;

    .line 1413
    new-instance v0, Lcom/google/d/a/a/mv;

    const-string v1, "PRIORITY_CONTROLLED_ACCESS"

    const/16 v2, 0x8

    const/16 v3, 0x90

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mv;->i:Lcom/google/d/a/a/mv;

    .line 1376
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/d/a/a/mv;

    sget-object v1, Lcom/google/d/a/a/mv;->a:Lcom/google/d/a/a/mv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mv;->b:Lcom/google/d/a/a/mv;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/mv;->c:Lcom/google/d/a/a/mv;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/mv;->d:Lcom/google/d/a/a/mv;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/mv;->e:Lcom/google/d/a/a/mv;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/mv;->f:Lcom/google/d/a/a/mv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/mv;->g:Lcom/google/d/a/a/mv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/mv;->h:Lcom/google/d/a/a/mv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/mv;->i:Lcom/google/d/a/a/mv;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/mv;->k:[Lcom/google/d/a/a/mv;

    .line 1478
    new-instance v0, Lcom/google/d/a/a/mw;

    invoke-direct {v0}, Lcom/google/d/a/a/mw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1487
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1488
    iput p3, p0, Lcom/google/d/a/a/mv;->j:I

    .line 1489
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mv;
    .locals 1

    .prologue
    .line 1459
    sparse-switch p0, :sswitch_data_0

    .line 1469
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1460
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/mv;->a:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1461
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/mv;->b:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1462
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/mv;->c:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1463
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/mv;->d:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1464
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/mv;->e:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1465
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/mv;->f:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1466
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/mv;->g:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1467
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/mv;->h:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1468
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/mv;->i:Lcom/google/d/a/a/mv;

    goto :goto_0

    .line 1459
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x30 -> :sswitch_2
        0x40 -> :sswitch_3
        0x50 -> :sswitch_4
        0x60 -> :sswitch_5
        0x70 -> :sswitch_6
        0x80 -> :sswitch_7
        0x90 -> :sswitch_8
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mv;
    .locals 1

    .prologue
    .line 1376
    const-class v0, Lcom/google/d/a/a/mv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mv;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mv;
    .locals 1

    .prologue
    .line 1376
    sget-object v0, Lcom/google/d/a/a/mv;->k:[Lcom/google/d/a/a/mv;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1455
    iget v0, p0, Lcom/google/d/a/a/mv;->j:I

    return v0
.end method
