.class public final Lcom/google/d/a/a/jd;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/jg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/jb;",
        "Lcom/google/d/a/a/jd;",
        ">;",
        "Lcom/google/d/a/a/jg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:F

.field private d:F

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1047
    sget-object v0, Lcom/google/d/a/a/jb;->g:Lcom/google/d/a/a/jb;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1132
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/jd;->b:I

    .line 1232
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/jd;->e:Ljava/lang/Object;

    .line 1308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/jd;->f:Lcom/google/n/ao;

    .line 1048
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/jb;)Lcom/google/d/a/a/jd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1099
    invoke-static {}, Lcom/google/d/a/a/jb;->d()Lcom/google/d/a/a/jb;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1119
    :goto_0
    return-object p0

    .line 1100
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1101
    iget v2, p1, Lcom/google/d/a/a/jb;->b:I

    invoke-static {v2}, Lcom/google/d/a/a/je;->a(I)Lcom/google/d/a/a/je;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/d/a/a/je;->a:Lcom/google/d/a/a/je;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1100
    goto :goto_1

    .line 1101
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/jd;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/jd;->a:I

    iget v2, v2, Lcom/google/d/a/a/je;->al:I

    iput v2, p0, Lcom/google/d/a/a/jd;->b:I

    .line 1103
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1104
    iget v2, p1, Lcom/google/d/a/a/jb;->c:F

    iget v3, p0, Lcom/google/d/a/a/jd;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/jd;->a:I

    iput v2, p0, Lcom/google/d/a/a/jd;->c:F

    .line 1106
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 1107
    iget v2, p1, Lcom/google/d/a/a/jb;->d:F

    iget v3, p0, Lcom/google/d/a/a/jd;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/jd;->a:I

    iput v2, p0, Lcom/google/d/a/a/jd;->d:F

    .line 1109
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 1110
    iget v2, p0, Lcom/google/d/a/a/jd;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/jd;->a:I

    .line 1111
    iget-object v2, p1, Lcom/google/d/a/a/jb;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/jd;->e:Ljava/lang/Object;

    .line 1114
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/jb;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    :goto_5
    if-eqz v0, :cond_8

    .line 1115
    iget-object v0, p0, Lcom/google/d/a/a/jd;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/jb;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1116
    iget v0, p0, Lcom/google/d/a/a/jd;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/d/a/a/jd;->a:I

    .line 1118
    :cond_8
    iget-object v0, p1, Lcom/google/d/a/a/jb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 1103
    goto :goto_2

    :cond_a
    move v2, v1

    .line 1106
    goto :goto_3

    :cond_b
    move v2, v1

    .line 1109
    goto :goto_4

    :cond_c
    move v0, v1

    .line 1114
    goto :goto_5
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1039
    new-instance v2, Lcom/google/d/a/a/jb;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/jb;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/jd;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/jd;->b:I

    iput v4, v2, Lcom/google/d/a/a/jb;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/d/a/a/jd;->c:F

    iput v4, v2, Lcom/google/d/a/a/jb;->c:F

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/jd;->d:F

    iput v4, v2, Lcom/google/d/a/a/jb;->d:F

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/d/a/a/jd;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/jb;->e:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v3, v2, Lcom/google/d/a/a/jb;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/jd;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/jd;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/jb;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1039
    check-cast p1, Lcom/google/d/a/a/jb;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/jd;->a(Lcom/google/d/a/a/jb;)Lcom/google/d/a/a/jd;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1123
    iget v2, p0, Lcom/google/d/a/a/jd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 1127
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 1123
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1127
    goto :goto_1
.end method
