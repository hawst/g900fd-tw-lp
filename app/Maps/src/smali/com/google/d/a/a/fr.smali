.class public final Lcom/google/d/a/a/fr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/fs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/fp;",
        "Lcom/google/d/a/a/fr;",
        ">;",
        "Lcom/google/d/a/a/fs;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/d/a/a/ds;

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 790
    sget-object v0, Lcom/google/d/a/a/fp;->d:Lcom/google/d/a/a/fp;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 845
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    .line 791
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/fp;)Lcom/google/d/a/a/fr;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 822
    invoke-static {}, Lcom/google/d/a/a/fp;->d()Lcom/google/d/a/a/fp;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 830
    :goto_0
    return-object p0

    .line 823
    :cond_0
    iget v0, p1, Lcom/google/d/a/a/fp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 824
    iget-object v0, p1, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/d/a/a/fr;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_5

    iget-object v3, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    invoke-static {v3}, Lcom/google/d/a/a/ds;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/d/a/a/du;->c()Lcom/google/d/a/a/ds;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    :goto_3
    iget v0, p0, Lcom/google/d/a/a/fr;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/fr;->a:I

    .line 826
    :cond_1
    iget v0, p1, Lcom/google/d/a/a/fp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 827
    iget v0, p1, Lcom/google/d/a/a/fp;->c:I

    iget v1, p0, Lcom/google/d/a/a/fr;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/d/a/a/fr;->a:I

    iput v0, p0, Lcom/google/d/a/a/fr;->c:I

    .line 829
    :cond_2
    iget-object v0, p1, Lcom/google/d/a/a/fp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 823
    goto :goto_1

    .line 824
    :cond_4
    iget-object v0, p1, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    goto :goto_3

    :cond_6
    move v0, v2

    .line 826
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 782
    new-instance v2, Lcom/google/d/a/a/fp;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/fp;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/fr;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    iput-object v1, v2, Lcom/google/d/a/a/fp;->b:Lcom/google/d/a/a/ds;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/d/a/a/fr;->c:I

    iput v1, v2, Lcom/google/d/a/a/fp;->c:I

    iput v0, v2, Lcom/google/d/a/a/fp;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 782
    check-cast p1, Lcom/google/d/a/a/fp;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/fr;->a(Lcom/google/d/a/a/fp;)Lcom/google/d/a/a/fr;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 834
    iget v0, p0, Lcom/google/d/a/a/fr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 835
    iget-object v0, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 840
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 834
    goto :goto_0

    .line 835
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/fr;->b:Lcom/google/d/a/a/ds;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 840
    goto :goto_2
.end method
