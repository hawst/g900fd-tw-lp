.class public final enum Lcom/google/d/a/a/mp;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mp;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/mp;

.field public static final enum b:Lcom/google/d/a/a/mp;

.field public static final enum c:Lcom/google/d/a/a/mp;

.field public static final enum d:Lcom/google/d/a/a/mp;

.field public static final enum e:Lcom/google/d/a/a/mp;

.field public static final enum f:Lcom/google/d/a/a/mp;

.field public static final enum g:Lcom/google/d/a/a/mp;

.field public static final enum h:Lcom/google/d/a/a/mp;

.field public static final enum i:Lcom/google/d/a/a/mp;

.field public static final enum j:Lcom/google/d/a/a/mp;

.field public static final enum k:Lcom/google/d/a/a/mp;

.field public static final enum l:Lcom/google/d/a/a/mp;

.field public static final enum m:Lcom/google/d/a/a/mp;

.field public static final enum n:Lcom/google/d/a/a/mp;

.field public static final enum o:Lcom/google/d/a/a/mp;

.field private static final synthetic q:[Lcom/google/d/a/a/mp;


# instance fields
.field final p:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 946
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_UNKNOWN"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->a:Lcom/google/d/a/a/mp;

    .line 950
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_UNRESTRICTED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->b:Lcom/google/d/a/a/mp;

    .line 954
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_UNCONTROLLED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->c:Lcom/google/d/a/a/mp;

    .line 958
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_STOP_SIGN"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->d:Lcom/google/d/a/a/mp;

    .line 962
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_ALL_WAY_STOP"

    const/16 v2, 0x141

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->e:Lcom/google/d/a/a/mp;

    .line 966
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_TRAFFIC_LIGHT"

    const/4 v2, 0x5

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->f:Lcom/google/d/a/a/mp;

    .line 970
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_THREE_WAY"

    const/4 v2, 0x6

    const/16 v3, 0x151

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->g:Lcom/google/d/a/a/mp;

    .line 974
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_FLASHING_RED"

    const/4 v2, 0x7

    const/16 v3, 0x152

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->h:Lcom/google/d/a/a/mp;

    .line 978
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_FLASHING_YELLOW"

    const/16 v2, 0x8

    const/16 v3, 0x153

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->i:Lcom/google/d/a/a/mp;

    .line 982
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_YIELD"

    const/16 v2, 0x9

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->j:Lcom/google/d/a/a/mp;

    .line 986
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_MERGE"

    const/16 v2, 0xa

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->k:Lcom/google/d/a/a/mp;

    .line 990
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_ROUNDABOUT"

    const/16 v2, 0xb

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->l:Lcom/google/d/a/a/mp;

    .line 994
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_RAILROAD_CROSSING"

    const/16 v2, 0xc

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->m:Lcom/google/d/a/a/mp;

    .line 998
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_NO_EXIT"

    const/16 v2, 0xd

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->n:Lcom/google/d/a/a/mp;

    .line 1002
    new-instance v0, Lcom/google/d/a/a/mp;

    const-string v1, "ENDPOINT_WRONG_WAY"

    const/16 v2, 0xe

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mp;->o:Lcom/google/d/a/a/mp;

    .line 941
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/google/d/a/a/mp;

    sget-object v1, Lcom/google/d/a/a/mp;->a:Lcom/google/d/a/a/mp;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mp;->b:Lcom/google/d/a/a/mp;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/mp;->c:Lcom/google/d/a/a/mp;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/mp;->d:Lcom/google/d/a/a/mp;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/mp;->e:Lcom/google/d/a/a/mp;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/mp;->f:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/mp;->g:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/mp;->h:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/mp;->i:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/mp;->j:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/mp;->k:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/mp;->l:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/mp;->m:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/d/a/a/mp;->n:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/d/a/a/mp;->o:Lcom/google/d/a/a/mp;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/mp;->q:[Lcom/google/d/a/a/mp;

    .line 1097
    new-instance v0, Lcom/google/d/a/a/mq;

    invoke-direct {v0}, Lcom/google/d/a/a/mq;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1106
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1107
    iput p3, p0, Lcom/google/d/a/a/mp;->p:I

    .line 1108
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mp;
    .locals 1

    .prologue
    .line 1072
    sparse-switch p0, :sswitch_data_0

    .line 1088
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1073
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/mp;->a:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1074
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/mp;->b:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1075
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/mp;->c:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1076
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/mp;->d:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1077
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/mp;->e:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1078
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/mp;->f:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1079
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/mp;->g:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1080
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/mp;->h:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1081
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/mp;->i:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1082
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/mp;->j:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1083
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/mp;->k:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1084
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/mp;->l:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1085
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/mp;->m:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1086
    :sswitch_d
    sget-object v0, Lcom/google/d/a/a/mp;->n:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1087
    :sswitch_e
    sget-object v0, Lcom/google/d/a/a/mp;->o:Lcom/google/d/a/a/mp;

    goto :goto_0

    .line 1072
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_5
        0x16 -> :sswitch_9
        0x17 -> :sswitch_a
        0x18 -> :sswitch_b
        0x19 -> :sswitch_c
        0x1a -> :sswitch_d
        0x1b -> :sswitch_e
        0x141 -> :sswitch_4
        0x151 -> :sswitch_6
        0x152 -> :sswitch_7
        0x153 -> :sswitch_8
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mp;
    .locals 1

    .prologue
    .line 941
    const-class v0, Lcom/google/d/a/a/mp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mp;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mp;
    .locals 1

    .prologue
    .line 941
    sget-object v0, Lcom/google/d/a/a/mp;->q:[Lcom/google/d/a/a/mp;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mp;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1068
    iget v0, p0, Lcom/google/d/a/a/mp;->p:I

    return v0
.end method
