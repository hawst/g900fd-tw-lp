.class public final enum Lcom/google/d/a/a/nt;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/nt;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/nt;

.field public static final enum b:Lcom/google/d/a/a/nt;

.field public static final enum c:Lcom/google/d/a/a/nt;

.field public static final enum d:Lcom/google/d/a/a/nt;

.field public static final enum e:Lcom/google/d/a/a/nt;

.field private static final synthetic g:[Lcom/google/d/a/a/nt;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 311
    new-instance v0, Lcom/google/d/a/a/nt;

    const-string v1, "VOICE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/d/a/a/nt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nt;->a:Lcom/google/d/a/a/nt;

    .line 315
    new-instance v0, Lcom/google/d/a/a/nt;

    const-string v1, "FAX"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/a/a/nt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nt;->b:Lcom/google/d/a/a/nt;

    .line 319
    new-instance v0, Lcom/google/d/a/a/nt;

    const-string v1, "TDD"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/nt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nt;->c:Lcom/google/d/a/a/nt;

    .line 323
    new-instance v0, Lcom/google/d/a/a/nt;

    const-string v1, "DATA"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/nt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nt;->d:Lcom/google/d/a/a/nt;

    .line 327
    new-instance v0, Lcom/google/d/a/a/nt;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/d/a/a/nt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/nt;->e:Lcom/google/d/a/a/nt;

    .line 306
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/d/a/a/nt;

    sget-object v1, Lcom/google/d/a/a/nt;->a:Lcom/google/d/a/a/nt;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/nt;->b:Lcom/google/d/a/a/nt;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/nt;->c:Lcom/google/d/a/a/nt;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/nt;->d:Lcom/google/d/a/a/nt;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/nt;->e:Lcom/google/d/a/a/nt;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/d/a/a/nt;->g:[Lcom/google/d/a/a/nt;

    .line 372
    new-instance v0, Lcom/google/d/a/a/nu;

    invoke-direct {v0}, Lcom/google/d/a/a/nu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 382
    iput p3, p0, Lcom/google/d/a/a/nt;->f:I

    .line 383
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/nt;
    .locals 1

    .prologue
    .line 357
    packed-switch p0, :pswitch_data_0

    .line 363
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 358
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/nt;->a:Lcom/google/d/a/a/nt;

    goto :goto_0

    .line 359
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/nt;->b:Lcom/google/d/a/a/nt;

    goto :goto_0

    .line 360
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/nt;->c:Lcom/google/d/a/a/nt;

    goto :goto_0

    .line 361
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/nt;->d:Lcom/google/d/a/a/nt;

    goto :goto_0

    .line 362
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/nt;->e:Lcom/google/d/a/a/nt;

    goto :goto_0

    .line 357
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/nt;
    .locals 1

    .prologue
    .line 306
    const-class v0, Lcom/google/d/a/a/nt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nt;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/nt;
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lcom/google/d/a/a/nt;->g:[Lcom/google/d/a/a/nt;

    invoke-virtual {v0}, [Lcom/google/d/a/a/nt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/nt;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 353
    iget v0, p0, Lcom/google/d/a/a/nt;->f:I

    return v0
.end method
