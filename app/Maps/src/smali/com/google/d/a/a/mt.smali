.class public final enum Lcom/google/d/a/a/mt;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mt;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/mt;

.field public static final enum b:Lcom/google/d/a/a/mt;

.field public static final enum c:Lcom/google/d/a/a/mt;

.field private static final synthetic e:[Lcom/google/d/a/a/mt;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2232
    new-instance v0, Lcom/google/d/a/a/mt;

    const-string v1, "PEDESTRIAN_GRADE_FLAT"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/mt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mt;->a:Lcom/google/d/a/a/mt;

    .line 2236
    new-instance v0, Lcom/google/d/a/a/mt;

    const-string v1, "PEDESTRIAN_GRADE_UP"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mt;->b:Lcom/google/d/a/a/mt;

    .line 2240
    new-instance v0, Lcom/google/d/a/a/mt;

    const-string v1, "PEDESTRIAN_GRADE_DOWN"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/d/a/a/mt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mt;->c:Lcom/google/d/a/a/mt;

    .line 2227
    new-array v0, v5, [Lcom/google/d/a/a/mt;

    sget-object v1, Lcom/google/d/a/a/mt;->a:Lcom/google/d/a/a/mt;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mt;->b:Lcom/google/d/a/a/mt;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/mt;->c:Lcom/google/d/a/a/mt;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/d/a/a/mt;->e:[Lcom/google/d/a/a/mt;

    .line 2275
    new-instance v0, Lcom/google/d/a/a/mu;

    invoke-direct {v0}, Lcom/google/d/a/a/mu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2284
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2285
    iput p3, p0, Lcom/google/d/a/a/mt;->d:I

    .line 2286
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mt;
    .locals 1

    .prologue
    .line 2262
    packed-switch p0, :pswitch_data_0

    .line 2266
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2263
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/mt;->a:Lcom/google/d/a/a/mt;

    goto :goto_0

    .line 2264
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/mt;->b:Lcom/google/d/a/a/mt;

    goto :goto_0

    .line 2265
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/mt;->c:Lcom/google/d/a/a/mt;

    goto :goto_0

    .line 2262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mt;
    .locals 1

    .prologue
    .line 2227
    const-class v0, Lcom/google/d/a/a/mt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mt;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mt;
    .locals 1

    .prologue
    .line 2227
    sget-object v0, Lcom/google/d/a/a/mt;->e:[Lcom/google/d/a/a/mt;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mt;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2258
    iget v0, p0, Lcom/google/d/a/a/mt;->d:I

    return v0
.end method
