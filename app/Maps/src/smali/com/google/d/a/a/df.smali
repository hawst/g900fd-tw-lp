.class public final enum Lcom/google/d/a/a/df;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/df;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/df;

.field public static final enum b:Lcom/google/d/a/a/df;

.field public static final enum c:Lcom/google/d/a/a/df;

.field private static final synthetic e:[Lcom/google/d/a/a/df;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 376
    new-instance v0, Lcom/google/d/a/a/df;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/d/a/a/df;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/df;->a:Lcom/google/d/a/a/df;

    .line 380
    new-instance v0, Lcom/google/d/a/a/df;

    const-string v1, "MOVED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/a/a/df;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/df;->b:Lcom/google/d/a/a/df;

    .line 384
    new-instance v0, Lcom/google/d/a/a/df;

    const-string v1, "REBRANDED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/df;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/df;->c:Lcom/google/d/a/a/df;

    .line 371
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/d/a/a/df;

    sget-object v1, Lcom/google/d/a/a/df;->a:Lcom/google/d/a/a/df;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/df;->b:Lcom/google/d/a/a/df;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/df;->c:Lcom/google/d/a/a/df;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/d/a/a/df;->e:[Lcom/google/d/a/a/df;

    .line 419
    new-instance v0, Lcom/google/d/a/a/dg;

    invoke-direct {v0}, Lcom/google/d/a/a/dg;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 428
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 429
    iput p3, p0, Lcom/google/d/a/a/df;->d:I

    .line 430
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/df;
    .locals 1

    .prologue
    .line 406
    packed-switch p0, :pswitch_data_0

    .line 410
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 407
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/df;->a:Lcom/google/d/a/a/df;

    goto :goto_0

    .line 408
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/df;->b:Lcom/google/d/a/a/df;

    goto :goto_0

    .line 409
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/df;->c:Lcom/google/d/a/a/df;

    goto :goto_0

    .line 406
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/df;
    .locals 1

    .prologue
    .line 371
    const-class v0, Lcom/google/d/a/a/df;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/df;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/df;
    .locals 1

    .prologue
    .line 371
    sget-object v0, Lcom/google/d/a/a/df;->e:[Lcom/google/d/a/a/df;

    invoke-virtual {v0}, [Lcom/google/d/a/a/df;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/df;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Lcom/google/d/a/a/df;->d:I

    return v0
.end method
