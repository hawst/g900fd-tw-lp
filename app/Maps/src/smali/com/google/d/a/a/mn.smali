.class public final enum Lcom/google/d/a/a/mn;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/mn;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/mn;

.field public static final enum b:Lcom/google/d/a/a/mn;

.field public static final enum c:Lcom/google/d/a/a/mn;

.field public static final enum d:Lcom/google/d/a/a/mn;

.field public static final enum e:Lcom/google/d/a/a/mn;

.field public static final enum f:Lcom/google/d/a/a/mn;

.field public static final enum g:Lcom/google/d/a/a/mn;

.field public static final enum h:Lcom/google/d/a/a/mn;

.field public static final enum i:Lcom/google/d/a/a/mn;

.field private static final synthetic k:[Lcom/google/d/a/a/mn;


# instance fields
.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1121
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_NORMAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->a:Lcom/google/d/a/a/mn;

    .line 1125
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_BRIDGE"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->b:Lcom/google/d/a/a/mn;

    .line 1129
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_TUNNEL"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->c:Lcom/google/d/a/a/mn;

    .line 1133
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_SKYWAY"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->d:Lcom/google/d/a/a/mn;

    .line 1137
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_STAIRWAY"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->e:Lcom/google/d/a/a/mn;

    .line 1141
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_ESCALATOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->f:Lcom/google/d/a/a/mn;

    .line 1145
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_ELEVATOR"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->g:Lcom/google/d/a/a/mn;

    .line 1149
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_SLOPEWAY"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->h:Lcom/google/d/a/a/mn;

    .line 1153
    new-instance v0, Lcom/google/d/a/a/mn;

    const-string v1, "ELEVATION_MOVING_WALKWAY"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/mn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/mn;->i:Lcom/google/d/a/a/mn;

    .line 1116
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/d/a/a/mn;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/d/a/a/mn;->a:Lcom/google/d/a/a/mn;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/d/a/a/mn;->b:Lcom/google/d/a/a/mn;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/mn;->c:Lcom/google/d/a/a/mn;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/mn;->d:Lcom/google/d/a/a/mn;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/mn;->e:Lcom/google/d/a/a/mn;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/mn;->f:Lcom/google/d/a/a/mn;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/mn;->g:Lcom/google/d/a/a/mn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/mn;->h:Lcom/google/d/a/a/mn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/mn;->i:Lcom/google/d/a/a/mn;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/mn;->k:[Lcom/google/d/a/a/mn;

    .line 1218
    new-instance v0, Lcom/google/d/a/a/mo;

    invoke-direct {v0}, Lcom/google/d/a/a/mo;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1227
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1228
    iput p3, p0, Lcom/google/d/a/a/mn;->j:I

    .line 1229
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/mn;
    .locals 1

    .prologue
    .line 1199
    packed-switch p0, :pswitch_data_0

    .line 1209
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1200
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/mn;->a:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1201
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/mn;->b:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1202
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/mn;->c:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1203
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/mn;->d:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1204
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/mn;->e:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1205
    :pswitch_5
    sget-object v0, Lcom/google/d/a/a/mn;->f:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1206
    :pswitch_6
    sget-object v0, Lcom/google/d/a/a/mn;->g:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1207
    :pswitch_7
    sget-object v0, Lcom/google/d/a/a/mn;->h:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1208
    :pswitch_8
    sget-object v0, Lcom/google/d/a/a/mn;->i:Lcom/google/d/a/a/mn;

    goto :goto_0

    .line 1199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/mn;
    .locals 1

    .prologue
    .line 1116
    const-class v0, Lcom/google/d/a/a/mn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/mn;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/mn;
    .locals 1

    .prologue
    .line 1116
    sget-object v0, Lcom/google/d/a/a/mn;->k:[Lcom/google/d/a/a/mn;

    invoke-virtual {v0}, [Lcom/google/d/a/a/mn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/mn;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1195
    iget v0, p0, Lcom/google/d/a/a/mn;->j:I

    return v0
.end method
