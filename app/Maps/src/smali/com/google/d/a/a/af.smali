.class public final enum Lcom/google/d/a/a/af;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/af;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/af;

.field public static final enum b:Lcom/google/d/a/a/af;

.field public static final enum c:Lcom/google/d/a/a/af;

.field public static final enum d:Lcom/google/d/a/a/af;

.field private static final synthetic f:[Lcom/google/d/a/a/af;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2962
    new-instance v0, Lcom/google/d/a/a/af;

    const-string v1, "ITEMCLASS"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/af;->a:Lcom/google/d/a/a/af;

    .line 2966
    new-instance v0, Lcom/google/d/a/a/af;

    const-string v1, "ATTRIBUTE"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/af;->b:Lcom/google/d/a/a/af;

    .line 2970
    new-instance v0, Lcom/google/d/a/a/af;

    const-string v1, "VALUESPACE"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/d/a/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/af;->c:Lcom/google/d/a/a/af;

    .line 2974
    new-instance v0, Lcom/google/d/a/a/af;

    const-string v1, "DATASTORE"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/d/a/a/af;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/af;->d:Lcom/google/d/a/a/af;

    .line 2957
    new-array v0, v6, [Lcom/google/d/a/a/af;

    sget-object v1, Lcom/google/d/a/a/af;->a:Lcom/google/d/a/a/af;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/af;->b:Lcom/google/d/a/a/af;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/af;->c:Lcom/google/d/a/a/af;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/af;->d:Lcom/google/d/a/a/af;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/d/a/a/af;->f:[Lcom/google/d/a/a/af;

    .line 3014
    new-instance v0, Lcom/google/d/a/a/ag;

    invoke-direct {v0}, Lcom/google/d/a/a/ag;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 3023
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3024
    iput p3, p0, Lcom/google/d/a/a/af;->e:I

    .line 3025
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/af;
    .locals 1

    .prologue
    .line 3000
    packed-switch p0, :pswitch_data_0

    .line 3005
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 3001
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/af;->a:Lcom/google/d/a/a/af;

    goto :goto_0

    .line 3002
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/af;->b:Lcom/google/d/a/a/af;

    goto :goto_0

    .line 3003
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/af;->c:Lcom/google/d/a/a/af;

    goto :goto_0

    .line 3004
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/af;->d:Lcom/google/d/a/a/af;

    goto :goto_0

    .line 3000
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/af;
    .locals 1

    .prologue
    .line 2957
    const-class v0, Lcom/google/d/a/a/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/af;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/af;
    .locals 1

    .prologue
    .line 2957
    sget-object v0, Lcom/google/d/a/a/af;->f:[Lcom/google/d/a/a/af;

    invoke-virtual {v0}, [Lcom/google/d/a/a/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/af;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2996
    iget v0, p0, Lcom/google/d/a/a/af;->e:I

    return v0
.end method
