.class public final Lcom/google/d/a/a/in;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/iq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/in;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/d/a/a/in;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 858
    new-instance v0, Lcom/google/d/a/a/io;

    invoke-direct {v0}, Lcom/google/d/a/a/io;-><init>()V

    sput-object v0, Lcom/google/d/a/a/in;->PARSER:Lcom/google/n/ax;

    .line 1055
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/in;->i:Lcom/google/n/aw;

    .line 1626
    new-instance v0, Lcom/google/d/a/a/in;

    invoke-direct {v0}, Lcom/google/d/a/a/in;-><init>()V

    sput-object v0, Lcom/google/d/a/a/in;->f:Lcom/google/d/a/a/in;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 781
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 918
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/in;->c:Lcom/google/n/ao;

    .line 934
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/in;->d:Lcom/google/n/ao;

    .line 992
    iput-byte v2, p0, Lcom/google/d/a/a/in;->g:B

    .line 1026
    iput v2, p0, Lcom/google/d/a/a/in;->h:I

    .line 782
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    .line 783
    iget-object v0, p0, Lcom/google/d/a/a/in;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 784
    iget-object v0, p0, Lcom/google/d/a/a/in;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 785
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    .line 786
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/16 v7, 0x8

    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 792
    invoke-direct {p0}, Lcom/google/d/a/a/in;-><init>()V

    .line 795
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 798
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 799
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 800
    sparse-switch v1, :sswitch_data_0

    .line 805
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 807
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 803
    goto :goto_0

    .line 812
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_7

    .line 813
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 815
    or-int/lit8 v1, v0, 0x1

    .line 817
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 818
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 817
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 819
    goto :goto_0

    .line 822
    :sswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/google/d/a/a/in;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 823
    iget v1, p0, Lcom/google/d/a/a/in;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/d/a/a/in;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 843
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 844
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 849
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_1

    .line 850
    iget-object v2, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    .line 852
    :cond_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_2

    .line 853
    iget-object v1, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    .line 855
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/in;->au:Lcom/google/n/bn;

    throw v0

    .line 827
    :sswitch_3
    :try_start_4
    iget-object v1, p0, Lcom/google/d/a/a/in;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 828
    iget v1, p0, Lcom/google/d/a/a/in;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/d/a/a/in;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 845
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 846
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 847
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 832
    :sswitch_4
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v7, :cond_3

    .line 833
    :try_start_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    .line 835
    or-int/lit8 v0, v0, 0x8

    .line 837
    :cond_3
    iget-object v1, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 838
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 837
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 849
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_3

    :cond_4
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_5

    .line 850
    iget-object v1, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    .line 852
    :cond_5
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_6

    .line 853
    iget-object v0, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    .line 855
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/in;->au:Lcom/google/n/bn;

    .line 856
    return-void

    .line 845
    :catch_2
    move-exception v0

    goto :goto_4

    .line 843
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_7
    move v1, v0

    goto/16 :goto_1

    .line 800
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 779
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 918
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/in;->c:Lcom/google/n/ao;

    .line 934
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/in;->d:Lcom/google/n/ao;

    .line 992
    iput-byte v1, p0, Lcom/google/d/a/a/in;->g:B

    .line 1026
    iput v1, p0, Lcom/google/d/a/a/in;->h:I

    .line 780
    return-void
.end method

.method public static d()Lcom/google/d/a/a/in;
    .locals 1

    .prologue
    .line 1629
    sget-object v0, Lcom/google/d/a/a/in;->f:Lcom/google/d/a/a/in;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ip;
    .locals 1

    .prologue
    .line 1117
    new-instance v0, Lcom/google/d/a/a/ip;

    invoke-direct {v0}, Lcom/google/d/a/a/ip;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/in;",
            ">;"
        }
    .end annotation

    .prologue
    .line 870
    sget-object v0, Lcom/google/d/a/a/in;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 1010
    invoke-virtual {p0}, Lcom/google/d/a/a/in;->c()I

    move v1, v2

    .line 1011
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1012
    iget-object v0, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1011
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1014
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/in;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_1

    .line 1015
    iget-object v0, p0, Lcom/google/d/a/a/in;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1017
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/in;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 1018
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/d/a/a/in;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1020
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1021
    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1020
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1023
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/in;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1024
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 994
    iget-byte v0, p0, Lcom/google/d/a/a/in;->g:B

    .line 995
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1005
    :goto_0
    return v0

    .line 996
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 998
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/in;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 999
    iget-object v0, p0, Lcom/google/d/a/a/in;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sh;->d()Lcom/google/d/a/a/sh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sh;

    invoke-virtual {v0}, Lcom/google/d/a/a/sh;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1000
    iput-byte v2, p0, Lcom/google/d/a/a/in;->g:B

    move v0, v2

    .line 1001
    goto :goto_0

    :cond_2
    move v0, v2

    .line 998
    goto :goto_1

    .line 1004
    :cond_3
    iput-byte v1, p0, Lcom/google/d/a/a/in;->g:B

    move v0, v1

    .line 1005
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1028
    iget v0, p0, Lcom/google/d/a/a/in;->h:I

    .line 1029
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1050
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1032
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1033
    iget-object v0, p0, Lcom/google/d/a/a/in;->b:Ljava/util/List;

    .line 1034
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1032
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1036
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/in;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 1037
    iget-object v0, p0, Lcom/google/d/a/a/in;->c:Lcom/google/n/ao;

    .line 1038
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1040
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/in;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 1041
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/d/a/a/in;->d:Lcom/google/n/ao;

    .line 1042
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_3
    move v1, v2

    .line 1044
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1045
    const/4 v4, 0x4

    iget-object v0, p0, Lcom/google/d/a/a/in;->e:Ljava/util/List;

    .line 1046
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1044
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1048
    :cond_4
    iget-object v0, p0, Lcom/google/d/a/a/in;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1049
    iput v0, p0, Lcom/google/d/a/a/in;->h:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 773
    invoke-static {}, Lcom/google/d/a/a/in;->newBuilder()Lcom/google/d/a/a/ip;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ip;->a(Lcom/google/d/a/a/in;)Lcom/google/d/a/a/ip;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 773
    invoke-static {}, Lcom/google/d/a/a/in;->newBuilder()Lcom/google/d/a/a/ip;

    move-result-object v0

    return-object v0
.end method
