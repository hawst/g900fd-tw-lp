.class public final Lcom/google/d/a/a/cs;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/cv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/cq;",
        "Lcom/google/d/a/a/cs;",
        ">;",
        "Lcom/google/d/a/a/cv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/n/ao;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Object;

.field private f:Z

.field private g:Lcom/google/n/ao;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4026
    sget-object v0, Lcom/google/d/a/a/cq;->k:Lcom/google/d/a/a/cq;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4186
    const/16 v0, 0xf1

    iput v0, p0, Lcom/google/d/a/a/cs;->b:I

    .line 4222
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cs;->c:Lcom/google/n/ao;

    .line 4282
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    .line 4418
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/cs;->e:Ljava/lang/Object;

    .line 4526
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cs;->g:Lcom/google/n/ao;

    .line 4586
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    .line 4722
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cs;->i:Lcom/google/n/ao;

    .line 4781
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/cs;->j:Lcom/google/n/ao;

    .line 4027
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/cq;)Lcom/google/d/a/a/cs;
    .locals 6

    .prologue
    const/16 v5, 0x40

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4110
    invoke-static {}, Lcom/google/d/a/a/cq;->d()Lcom/google/d/a/a/cq;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4159
    :goto_0
    return-object p0

    .line 4111
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 4112
    iget v2, p1, Lcom/google/d/a/a/cq;->b:I

    invoke-static {v2}, Lcom/google/d/a/a/ct;->a(I)Lcom/google/d/a/a/ct;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/d/a/a/ct;->b:Lcom/google/d/a/a/ct;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 4111
    goto :goto_1

    .line 4112
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/cs;->a:I

    iget v2, v2, Lcom/google/d/a/a/ct;->gf:I

    iput v2, p0, Lcom/google/d/a/a/cs;->b:I

    .line 4114
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 4115
    iget-object v2, p0, Lcom/google/d/a/a/cs;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/cq;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4116
    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4118
    :cond_5
    iget-object v2, p1, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 4119
    iget-object v2, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 4120
    iget-object v2, p1, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    .line 4121
    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4128
    :cond_6
    :goto_3
    iget v2, p1, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_10

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 4129
    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4130
    iget-object v2, p1, Lcom/google/d/a/a/cq;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/cs;->e:Ljava/lang/Object;

    .line 4133
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 4134
    iget-boolean v2, p1, Lcom/google/d/a/a/cq;->f:Z

    iget v3, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/cs;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/cs;->f:Z

    .line 4136
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 4137
    iget-object v2, p0, Lcom/google/d/a/a/cs;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/cq;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4138
    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4140
    :cond_9
    iget-object v2, p1, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 4141
    iget-object v2, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 4142
    iget-object v2, p1, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    .line 4143
    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4150
    :cond_a
    :goto_7
    iget v2, p1, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_b

    .line 4151
    iget-object v2, p0, Lcom/google/d/a/a/cs;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/cq;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4152
    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4154
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/cq;->a:I

    and-int/lit8 v2, v2, 0x40

    if-ne v2, v5, :cond_16

    :goto_9
    if-eqz v0, :cond_c

    .line 4155
    iget-object v0, p0, Lcom/google/d/a/a/cs;->j:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4156
    iget v0, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4158
    :cond_c
    iget-object v0, p1, Lcom/google/d/a/a/cq;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 4114
    goto/16 :goto_2

    .line 4123
    :cond_e
    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4124
    :cond_f
    iget-object v2, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    :cond_10
    move v2, v1

    .line 4128
    goto/16 :goto_4

    :cond_11
    move v2, v1

    .line 4133
    goto/16 :goto_5

    :cond_12
    move v2, v1

    .line 4136
    goto/16 :goto_6

    .line 4145
    :cond_13
    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v2, v2, 0x40

    if-eq v2, v5, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/cs;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/cs;->a:I

    .line 4146
    :cond_14
    iget-object v2, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_7

    :cond_15
    move v2, v1

    .line 4150
    goto :goto_8

    :cond_16
    move v0, v1

    .line 4154
    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4018
    new-instance v2, Lcom/google/d/a/a/cq;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/cq;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/cs;->b:I

    iput v4, v2, Lcom/google/d/a/a/cq;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/d/a/a/cq;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/cs;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/cs;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/d/a/a/cs;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/cq;->d:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, p0, Lcom/google/d/a/a/cs;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/cq;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-boolean v4, p0, Lcom/google/d/a/a/cs;->f:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/cq;->f:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/d/a/a/cq;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/cs;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/cs;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/d/a/a/cs;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/cq;->h:Ljava/util/List;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v4, v2, Lcom/google/d/a/a/cq;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/cs;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/cs;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget-object v3, v2, Lcom/google/d/a/a/cq;->j:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/cs;->j:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/cs;->j:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/cq;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4018
    check-cast p1, Lcom/google/d/a/a/cq;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/cs;->a(Lcom/google/d/a/a/cq;)Lcom/google/d/a/a/cs;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4163
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4164
    iget-object v0, p0, Lcom/google/d/a/a/cs;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/nn;->d()Lcom/google/d/a/a/nn;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nn;

    invoke-virtual {v0}, Lcom/google/d/a/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4181
    :cond_0
    :goto_1
    return v2

    .line 4163
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 4169
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 4170
    iget-object v0, p0, Lcom/google/d/a/a/cs;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4169
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 4175
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/cs;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 4176
    iget-object v0, p0, Lcom/google/d/a/a/cs;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ij;->d()Lcom/google/d/a/a/ij;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ij;

    invoke-virtual {v0}, Lcom/google/d/a/a/ij;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 4181
    goto :goto_1

    :cond_5
    move v0, v2

    .line 4175
    goto :goto_3
.end method
