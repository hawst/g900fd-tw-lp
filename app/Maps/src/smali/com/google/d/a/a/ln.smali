.class public final enum Lcom/google/d/a/a/ln;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ln;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ln;

.field public static final enum b:Lcom/google/d/a/a/ln;

.field public static final enum c:Lcom/google/d/a/a/ln;

.field public static final enum d:Lcom/google/d/a/a/ln;

.field public static final enum e:Lcom/google/d/a/a/ln;

.field public static final enum f:Lcom/google/d/a/a/ln;

.field public static final enum g:Lcom/google/d/a/a/ln;

.field public static final enum h:Lcom/google/d/a/a/ln;

.field public static final enum i:Lcom/google/d/a/a/ln;

.field private static final synthetic k:[Lcom/google/d/a/a/ln;


# instance fields
.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 19
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_NONE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->a:Lcom/google/d/a/a/ln;

    .line 23
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_NORTH"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->b:Lcom/google/d/a/a/ln;

    .line 27
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_EAST"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->c:Lcom/google/d/a/a/ln;

    .line 31
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_SOUTH"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->d:Lcom/google/d/a/a/ln;

    .line 35
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_WEST"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->e:Lcom/google/d/a/a/ln;

    .line 39
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_NORTHEAST"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->f:Lcom/google/d/a/a/ln;

    .line 43
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_NORTHWEST"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->g:Lcom/google/d/a/a/ln;

    .line 47
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_SOUTHEAST"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->h:Lcom/google/d/a/a/ln;

    .line 51
    new-instance v0, Lcom/google/d/a/a/ln;

    const-string v1, "DIRECTION_SOUTHWEST"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ln;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ln;->i:Lcom/google/d/a/a/ln;

    .line 14
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/d/a/a/ln;

    sget-object v1, Lcom/google/d/a/a/ln;->a:Lcom/google/d/a/a/ln;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/ln;->b:Lcom/google/d/a/a/ln;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/ln;->c:Lcom/google/d/a/a/ln;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/ln;->d:Lcom/google/d/a/a/ln;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/ln;->e:Lcom/google/d/a/a/ln;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/ln;->f:Lcom/google/d/a/a/ln;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/ln;->g:Lcom/google/d/a/a/ln;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/ln;->h:Lcom/google/d/a/a/ln;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/ln;->i:Lcom/google/d/a/a/ln;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/ln;->k:[Lcom/google/d/a/a/ln;

    .line 116
    new-instance v0, Lcom/google/d/a/a/lo;

    invoke-direct {v0}, Lcom/google/d/a/a/lo;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    iput p3, p0, Lcom/google/d/a/a/ln;->j:I

    .line 127
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ln;
    .locals 1

    .prologue
    .line 97
    packed-switch p0, :pswitch_data_0

    .line 107
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 98
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/ln;->a:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 99
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/ln;->b:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 100
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/ln;->c:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 101
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/ln;->d:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 102
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/ln;->e:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 103
    :pswitch_5
    sget-object v0, Lcom/google/d/a/a/ln;->f:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 104
    :pswitch_6
    sget-object v0, Lcom/google/d/a/a/ln;->g:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 105
    :pswitch_7
    sget-object v0, Lcom/google/d/a/a/ln;->h:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 106
    :pswitch_8
    sget-object v0, Lcom/google/d/a/a/ln;->i:Lcom/google/d/a/a/ln;

    goto :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ln;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/d/a/a/ln;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ln;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ln;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/d/a/a/ln;->k:[Lcom/google/d/a/a/ln;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ln;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ln;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/d/a/a/ln;->j:I

    return v0
.end method
