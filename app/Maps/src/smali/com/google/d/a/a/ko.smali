.class public final enum Lcom/google/d/a/a/ko;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ko;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ko;

.field public static final enum b:Lcom/google/d/a/a/ko;

.field public static final enum c:Lcom/google/d/a/a/ko;

.field public static final enum d:Lcom/google/d/a/a/ko;

.field public static final enum e:Lcom/google/d/a/a/ko;

.field public static final enum f:Lcom/google/d/a/a/ko;

.field public static final enum g:Lcom/google/d/a/a/ko;

.field public static final enum h:Lcom/google/d/a/a/ko;

.field public static final enum i:Lcom/google/d/a/a/ko;

.field public static final enum j:Lcom/google/d/a/a/ko;

.field public static final enum k:Lcom/google/d/a/a/ko;

.field public static final enum l:Lcom/google/d/a/a/ko;

.field public static final enum m:Lcom/google/d/a/a/ko;

.field private static final synthetic o:[Lcom/google/d/a/a/ko;


# instance fields
.field private final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 450
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_ANY"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->a:Lcom/google/d/a/a/ko;

    .line 454
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_MOTOR_VEHICLE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->b:Lcom/google/d/a/a/ko;

    .line 458
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_AUTO"

    const/16 v2, 0x111

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->c:Lcom/google/d/a/a/ko;

    .line 462
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_CARPOOL"

    const/16 v2, 0x112

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->d:Lcom/google/d/a/a/ko;

    .line 466
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_MOTORCYCLE"

    const/16 v2, 0x113

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->e:Lcom/google/d/a/a/ko;

    .line 470
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_BUS"

    const/4 v2, 0x5

    const/16 v3, 0x114

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->f:Lcom/google/d/a/a/ko;

    .line 474
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_TRUCK"

    const/4 v2, 0x6

    const/16 v3, 0x115

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->g:Lcom/google/d/a/a/ko;

    .line 478
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_DELIVERY"

    const/4 v2, 0x7

    const/16 v3, 0x116

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->h:Lcom/google/d/a/a/ko;

    .line 482
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_TAXI"

    const/16 v2, 0x8

    const/16 v3, 0x117

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->i:Lcom/google/d/a/a/ko;

    .line 486
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_EMERGENCY"

    const/16 v2, 0x9

    const/16 v3, 0x118

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->j:Lcom/google/d/a/a/ko;

    .line 490
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_THROUGH_TRAFFIC"

    const/16 v2, 0xa

    const/16 v3, 0x119

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->k:Lcom/google/d/a/a/ko;

    .line 494
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_PEDESTRIAN"

    const/16 v2, 0xb

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->l:Lcom/google/d/a/a/ko;

    .line 498
    new-instance v0, Lcom/google/d/a/a/ko;

    const-string v1, "TRAVEL_BICYCLE"

    const/16 v2, 0xc

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/ko;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ko;->m:Lcom/google/d/a/a/ko;

    .line 445
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/d/a/a/ko;

    sget-object v1, Lcom/google/d/a/a/ko;->a:Lcom/google/d/a/a/ko;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/ko;->b:Lcom/google/d/a/a/ko;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/ko;->c:Lcom/google/d/a/a/ko;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/ko;->d:Lcom/google/d/a/a/ko;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/ko;->e:Lcom/google/d/a/a/ko;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/ko;->f:Lcom/google/d/a/a/ko;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/ko;->g:Lcom/google/d/a/a/ko;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/ko;->h:Lcom/google/d/a/a/ko;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/ko;->i:Lcom/google/d/a/a/ko;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/ko;->j:Lcom/google/d/a/a/ko;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/ko;->k:Lcom/google/d/a/a/ko;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/ko;->l:Lcom/google/d/a/a/ko;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/ko;->m:Lcom/google/d/a/a/ko;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/ko;->o:[Lcom/google/d/a/a/ko;

    .line 583
    new-instance v0, Lcom/google/d/a/a/kp;

    invoke-direct {v0}, Lcom/google/d/a/a/kp;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 592
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 593
    iput p3, p0, Lcom/google/d/a/a/ko;->n:I

    .line 594
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ko;
    .locals 1

    .prologue
    .line 560
    sparse-switch p0, :sswitch_data_0

    .line 574
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 561
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/ko;->a:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 562
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/ko;->b:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 563
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/ko;->c:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 564
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/ko;->d:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 565
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/ko;->e:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 566
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/ko;->f:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 567
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/ko;->g:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 568
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/ko;->h:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 569
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/ko;->i:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 570
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/ko;->j:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 571
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/ko;->k:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 572
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/ko;->l:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 573
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/ko;->m:Lcom/google/d/a/a/ko;

    goto :goto_0

    .line 560
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x11 -> :sswitch_1
        0x12 -> :sswitch_b
        0x13 -> :sswitch_c
        0x111 -> :sswitch_2
        0x112 -> :sswitch_3
        0x113 -> :sswitch_4
        0x114 -> :sswitch_5
        0x115 -> :sswitch_6
        0x116 -> :sswitch_7
        0x117 -> :sswitch_8
        0x118 -> :sswitch_9
        0x119 -> :sswitch_a
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ko;
    .locals 1

    .prologue
    .line 445
    const-class v0, Lcom/google/d/a/a/ko;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ko;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ko;
    .locals 1

    .prologue
    .line 445
    sget-object v0, Lcom/google/d/a/a/ko;->o:[Lcom/google/d/a/a/ko;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ko;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ko;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 556
    iget v0, p0, Lcom/google/d/a/a/ko;->n:I

    return v0
.end method
