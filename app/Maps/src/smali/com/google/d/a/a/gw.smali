.class public final Lcom/google/d/a/a/gw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/gz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/gt;",
        "Lcom/google/d/a/a/gw;",
        ">;",
        "Lcom/google/d/a/a/gz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1102
    sget-object v0, Lcom/google/d/a/a/gt;->k:Lcom/google/d/a/a/gt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1249
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gw;->b:Ljava/lang/Object;

    .line 1325
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gw;->c:Ljava/lang/Object;

    .line 1402
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    .line 1475
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/gw;->e:I

    .line 1511
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gw;->f:Ljava/lang/Object;

    .line 1587
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gw;->g:Ljava/lang/Object;

    .line 1663
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gw;->h:Lcom/google/n/ao;

    .line 1722
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gw;->i:Lcom/google/n/ao;

    .line 1781
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gw;->j:Lcom/google/n/ao;

    .line 1103
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/gt;)Lcom/google/d/a/a/gw;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1183
    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1230
    :goto_0
    return-object p0

    .line 1184
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1185
    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1186
    iget-object v2, p1, Lcom/google/d/a/a/gt;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/gw;->b:Ljava/lang/Object;

    .line 1189
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1190
    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1191
    iget-object v2, p1, Lcom/google/d/a/a/gt;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/gw;->c:Ljava/lang/Object;

    .line 1194
    :cond_2
    iget-object v2, p1, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1195
    iget-object v2, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1196
    iget-object v2, p1, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    .line 1197
    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1204
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 1205
    iget v2, p1, Lcom/google/d/a/a/gt;->e:I

    invoke-static {v2}, Lcom/google/d/a/a/ln;->a(I)Lcom/google/d/a/a/ln;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/d/a/a/ln;->a:Lcom/google/d/a/a/ln;

    :cond_4
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 1184
    goto :goto_1

    :cond_6
    move v2, v1

    .line 1189
    goto :goto_2

    .line 1199
    :cond_7
    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1200
    :cond_8
    iget-object v2, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_9
    move v2, v1

    .line 1204
    goto :goto_4

    .line 1205
    :cond_a
    iget v3, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/gw;->a:I

    iget v2, v2, Lcom/google/d/a/a/ln;->j:I

    iput v2, p0, Lcom/google/d/a/a/gw;->e:I

    .line 1207
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 1208
    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1209
    iget-object v2, p1, Lcom/google/d/a/a/gt;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/gw;->f:Ljava/lang/Object;

    .line 1212
    :cond_c
    iget v2, p1, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 1213
    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1214
    iget-object v2, p1, Lcom/google/d/a/a/gt;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/gw;->g:Ljava/lang/Object;

    .line 1217
    :cond_d
    iget v2, p1, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_7
    if-eqz v2, :cond_e

    .line 1218
    iget-object v2, p0, Lcom/google/d/a/a/gw;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/gt;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1219
    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1221
    :cond_e
    iget v2, p1, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_8
    if-eqz v2, :cond_f

    .line 1222
    iget-object v2, p0, Lcom/google/d/a/a/gw;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/gt;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1223
    iget v2, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1225
    :cond_f
    iget v2, p1, Lcom/google/d/a/a/gt;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    :goto_9
    if-eqz v0, :cond_10

    .line 1226
    iget-object v0, p0, Lcom/google/d/a/a/gw;->j:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1227
    iget v0, p0, Lcom/google/d/a/a/gw;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/gw;->a:I

    .line 1229
    :cond_10
    iget-object v0, p1, Lcom/google/d/a/a/gt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_11
    move v2, v1

    .line 1207
    goto :goto_5

    :cond_12
    move v2, v1

    .line 1212
    goto :goto_6

    :cond_13
    move v2, v1

    .line 1217
    goto :goto_7

    :cond_14
    move v2, v1

    .line 1221
    goto :goto_8

    :cond_15
    move v0, v1

    .line 1225
    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1094
    new-instance v2, Lcom/google/d/a/a/gt;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/gt;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/gw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-object v4, p0, Lcom/google/d/a/a/gw;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/gt;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/gw;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/gt;->c:Ljava/lang/Object;

    iget v4, p0, Lcom/google/d/a/a/gw;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/gw;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/d/a/a/gw;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/gw;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/d/a/a/gw;->e:I

    iput v4, v2, Lcom/google/d/a/a/gt;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/gw;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/gt;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, p0, Lcom/google/d/a/a/gw;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/gt;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, v2, Lcom/google/d/a/a/gt;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/gw;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/gw;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v4, v2, Lcom/google/d/a/a/gt;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/gw;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/gw;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v3, v2, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/gw;->j:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/gw;->j:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/gt;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1094
    check-cast p1, Lcom/google/d/a/a/gt;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/gw;->a(Lcom/google/d/a/a/gt;)Lcom/google/d/a/a/gw;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1234
    iget v0, p0, Lcom/google/d/a/a/gw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 1244
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1234
    goto :goto_0

    .line 1238
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/gw;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 1239
    iget-object v0, p0, Lcom/google/d/a/a/gw;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1241
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1238
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1244
    goto :goto_1
.end method
