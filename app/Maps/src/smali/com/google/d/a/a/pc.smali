.class public final enum Lcom/google/d/a/a/pc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/pc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/pc;

.field public static final enum b:Lcom/google/d/a/a/pc;

.field public static final enum c:Lcom/google/d/a/a/pc;

.field private static final synthetic e:[Lcom/google/d/a/a/pc;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 351
    new-instance v0, Lcom/google/d/a/a/pc;

    const-string v1, "DAY_OF_WEEK"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/d/a/a/pc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pc;->a:Lcom/google/d/a/a/pc;

    .line 355
    new-instance v0, Lcom/google/d/a/a/pc;

    const-string v1, "DAY_OF_MONTH"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/d/a/a/pc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pc;->b:Lcom/google/d/a/a/pc;

    .line 359
    new-instance v0, Lcom/google/d/a/a/pc;

    const-string v1, "DAY_OF_YEAR"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/pc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/pc;->c:Lcom/google/d/a/a/pc;

    .line 346
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/d/a/a/pc;

    sget-object v1, Lcom/google/d/a/a/pc;->a:Lcom/google/d/a/a/pc;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/d/a/a/pc;->b:Lcom/google/d/a/a/pc;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/pc;->c:Lcom/google/d/a/a/pc;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/d/a/a/pc;->e:[Lcom/google/d/a/a/pc;

    .line 394
    new-instance v0, Lcom/google/d/a/a/pd;

    invoke-direct {v0}, Lcom/google/d/a/a/pd;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 403
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 404
    iput p3, p0, Lcom/google/d/a/a/pc;->d:I

    .line 405
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/pc;
    .locals 1

    .prologue
    .line 381
    packed-switch p0, :pswitch_data_0

    .line 385
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 382
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/pc;->a:Lcom/google/d/a/a/pc;

    goto :goto_0

    .line 383
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/pc;->b:Lcom/google/d/a/a/pc;

    goto :goto_0

    .line 384
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/pc;->c:Lcom/google/d/a/a/pc;

    goto :goto_0

    .line 381
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/pc;
    .locals 1

    .prologue
    .line 346
    const-class v0, Lcom/google/d/a/a/pc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pc;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/pc;
    .locals 1

    .prologue
    .line 346
    sget-object v0, Lcom/google/d/a/a/pc;->e:[Lcom/google/d/a/a/pc;

    invoke-virtual {v0}, [Lcom/google/d/a/a/pc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/pc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 377
    iget v0, p0, Lcom/google/d/a/a/pc;->d:I

    return v0
.end method
