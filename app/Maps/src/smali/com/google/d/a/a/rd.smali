.class public final Lcom/google/d/a/a/rd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/rg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/rd;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/d/a/a/rd;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/google/d/a/a/re;

    invoke-direct {v0}, Lcom/google/d/a/a/re;-><init>()V

    sput-object v0, Lcom/google/d/a/a/rd;->PARSER:Lcom/google/n/ax;

    .line 294
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/rd;->h:Lcom/google/n/aw;

    .line 662
    new-instance v0, Lcom/google/d/a/a/rd;

    invoke-direct {v0}, Lcom/google/d/a/a/rd;-><init>()V

    sput-object v0, Lcom/google/d/a/a/rd;->e:Lcom/google/d/a/a/rd;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 137
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    .line 236
    iput-byte v2, p0, Lcom/google/d/a/a/rd;->f:B

    .line 269
    iput v2, p0, Lcom/google/d/a/a/rd;->g:I

    .line 64
    iget-object v0, p0, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/rd;->c:Ljava/lang/Object;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/rd;->d:Ljava/lang/Object;

    .line 67
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Lcom/google/d/a/a/rd;-><init>()V

    .line 74
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 79
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 80
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 81
    sparse-switch v3, :sswitch_data_0

    .line 86
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 88
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 84
    goto :goto_0

    .line 93
    :sswitch_1
    iget-object v3, p0, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 94
    iget v3, p0, Lcom/google/d/a/a/rd;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/rd;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/rd;->au:Lcom/google/n/bn;

    throw v0

    .line 98
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 99
    iget v4, p0, Lcom/google/d/a/a/rd;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/rd;->a:I

    .line 100
    iput-object v3, p0, Lcom/google/d/a/a/rd;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 113
    :catch_1
    move-exception v0

    .line 114
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 115
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 104
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 105
    iget v4, p0, Lcom/google/d/a/a/rd;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/rd;->a:I

    .line 106
    iput-object v3, p0, Lcom/google/d/a/a/rd;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 117
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rd;->au:Lcom/google/n/bn;

    .line 118
    return-void

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 61
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 137
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    .line 236
    iput-byte v1, p0, Lcom/google/d/a/a/rd;->f:B

    .line 269
    iput v1, p0, Lcom/google/d/a/a/rd;->g:I

    .line 62
    return-void
.end method

.method public static d()Lcom/google/d/a/a/rd;
    .locals 1

    .prologue
    .line 665
    sget-object v0, Lcom/google/d/a/a/rd;->e:Lcom/google/d/a/a/rd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/rf;
    .locals 1

    .prologue
    .line 356
    new-instance v0, Lcom/google/d/a/a/rf;

    invoke-direct {v0}, Lcom/google/d/a/a/rf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/rd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lcom/google/d/a/a/rd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x2

    .line 256
    invoke-virtual {p0}, Lcom/google/d/a/a/rd;->c()I

    .line 257
    iget v0, p0, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 260
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 261
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/rd;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rd;->c:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 263
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 264
    iget-object v0, p0, Lcom/google/d/a/a/rd;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rd;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 266
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/rd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 267
    return-void

    .line 261
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 264
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 238
    iget-byte v0, p0, Lcom/google/d/a/a/rd;->f:B

    .line 239
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 251
    :goto_0
    return v0

    .line 240
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 242
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 243
    iput-byte v2, p0, Lcom/google/d/a/a/rd;->f:B

    move v0, v2

    .line 244
    goto :goto_0

    :cond_2
    move v0, v2

    .line 242
    goto :goto_1

    .line 246
    :cond_3
    iget-object v0, p0, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 247
    iput-byte v2, p0, Lcom/google/d/a/a/rd;->f:B

    move v0, v2

    .line 248
    goto :goto_0

    .line 250
    :cond_4
    iput-byte v1, p0, Lcom/google/d/a/a/rd;->f:B

    move v0, v1

    .line 251
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 271
    iget v0, p0, Lcom/google/d/a/a/rd;->g:I

    .line 272
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 289
    :goto_0
    return v0

    .line 275
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 276
    iget-object v0, p0, Lcom/google/d/a/a/rd;->b:Lcom/google/n/ao;

    .line 277
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 279
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 280
    const/4 v3, 0x3

    .line 281
    iget-object v0, p0, Lcom/google/d/a/a/rd;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rd;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 283
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/rd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 285
    iget-object v0, p0, Lcom/google/d/a/a/rd;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/rd;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/rd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 288
    iput v0, p0, Lcom/google/d/a/a/rd;->g:I

    goto :goto_0

    .line 281
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 285
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/google/d/a/a/rd;->newBuilder()Lcom/google/d/a/a/rf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/rf;->a(Lcom/google/d/a/a/rd;)Lcom/google/d/a/a/rf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/google/d/a/a/rd;->newBuilder()Lcom/google/d/a/a/rf;

    move-result-object v0

    return-object v0
.end method
