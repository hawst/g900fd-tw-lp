.class public final Lcom/google/d/a/a/sh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/sk;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/sh;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/d/a/a/sh;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/d/a/a/si;

    invoke-direct {v0}, Lcom/google/d/a/a/si;-><init>()V

    sput-object v0, Lcom/google/d/a/a/sh;->PARSER:Lcom/google/n/ax;

    .line 191
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/sh;->e:Lcom/google/n/aw;

    .line 464
    new-instance v0, Lcom/google/d/a/a/sh;

    invoke-direct {v0}, Lcom/google/d/a/a/sh;-><init>()V

    sput-object v0, Lcom/google/d/a/a/sh;->b:Lcom/google/d/a/a/sh;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 40
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 149
    iput-byte v0, p0, Lcom/google/d/a/a/sh;->c:B

    .line 174
    iput v0, p0, Lcom/google/d/a/a/sh;->d:I

    .line 41
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    .line 42
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 48
    invoke-direct {p0}, Lcom/google/d/a/a/sh;-><init>()V

    .line 51
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 54
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 55
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 56
    sparse-switch v4, :sswitch_data_0

    .line 61
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 63
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 59
    goto :goto_0

    .line 68
    :sswitch_1
    and-int/lit8 v4, v0, 0x1

    if-eq v4, v2, :cond_1

    .line 69
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    .line 71
    or-int/lit8 v0, v0, 0x1

    .line 73
    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 73
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 79
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 80
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 86
    iget-object v1, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    .line 88
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/sh;->au:Lcom/google/n/bn;

    throw v0

    .line 85
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 86
    iget-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    .line 88
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/sh;->au:Lcom/google/n/bn;

    .line 89
    return-void

    .line 81
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 82
    :try_start_2
    new-instance v4, Lcom/google/n/ak;

    .line 83
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 85
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 38
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 149
    iput-byte v0, p0, Lcom/google/d/a/a/sh;->c:B

    .line 174
    iput v0, p0, Lcom/google/d/a/a/sh;->d:I

    .line 39
    return-void
.end method

.method public static d()Lcom/google/d/a/a/sh;
    .locals 1

    .prologue
    .line 467
    sget-object v0, Lcom/google/d/a/a/sh;->b:Lcom/google/d/a/a/sh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/sj;
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/google/d/a/a/sj;

    invoke-direct {v0}, Lcom/google/d/a/a/sj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/sh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/google/d/a/a/sh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/d/a/a/sh;->c()I

    .line 168
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 169
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 168
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/google/d/a/a/sh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 172
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 151
    iget-byte v0, p0, Lcom/google/d/a/a/sh;->c:B

    .line 152
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 162
    :cond_0
    :goto_0
    return v2

    .line 153
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 155
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 156
    iget-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/sc;->d()Lcom/google/d/a/a/sc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/sc;

    invoke-virtual {v0}, Lcom/google/d/a/a/sc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 157
    iput-byte v2, p0, Lcom/google/d/a/a/sh;->c:B

    goto :goto_0

    .line 155
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 161
    :cond_3
    iput-byte v3, p0, Lcom/google/d/a/a/sh;->c:B

    move v2, v3

    .line 162
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 176
    iget v0, p0, Lcom/google/d/a/a/sh;->d:I

    .line 177
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 186
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 180
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 181
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/d/a/a/sh;->a:Ljava/util/List;

    .line 182
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 180
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/sh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 185
    iput v0, p0, Lcom/google/d/a/a/sh;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/google/d/a/a/sh;->newBuilder()Lcom/google/d/a/a/sj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/sj;->a(Lcom/google/d/a/a/sh;)Lcom/google/d/a/a/sj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/google/d/a/a/sh;->newBuilder()Lcom/google/d/a/a/sj;

    move-result-object v0

    return-object v0
.end method
