.class public final Lcom/google/d/a/a/li;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ll;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/lg;",
        "Lcom/google/d/a/a/li;",
        ">;",
        "Lcom/google/d/a/a/ll;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:I

.field private h:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 524
    sget-object v0, Lcom/google/d/a/a/lg;->i:Lcom/google/d/a/a/lg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 700
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/li;->d:I

    .line 736
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/li;->e:Lcom/google/n/ao;

    .line 795
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/li;->f:Lcom/google/n/ao;

    .line 886
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/li;->h:I

    .line 525
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/lg;)Lcom/google/d/a/a/li;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 590
    invoke-static {}, Lcom/google/d/a/a/lg;->d()Lcom/google/d/a/a/lg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 615
    :goto_0
    return-object p0

    .line 591
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 592
    iget v2, p1, Lcom/google/d/a/a/lg;->b:I

    iget v3, p0, Lcom/google/d/a/a/li;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/li;->a:I

    iput v2, p0, Lcom/google/d/a/a/li;->b:I

    .line 594
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 595
    iget v2, p1, Lcom/google/d/a/a/lg;->c:I

    iget v3, p0, Lcom/google/d/a/a/li;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/li;->a:I

    iput v2, p0, Lcom/google/d/a/a/li;->c:I

    .line 597
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 598
    iget v2, p1, Lcom/google/d/a/a/lg;->d:I

    invoke-static {v2}, Lcom/google/d/a/a/lj;->a(I)Lcom/google/d/a/a/lj;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/d/a/a/lj;->a:Lcom/google/d/a/a/lj;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 591
    goto :goto_1

    :cond_5
    move v2, v1

    .line 594
    goto :goto_2

    :cond_6
    move v2, v1

    .line 597
    goto :goto_3

    .line 598
    :cond_7
    iget v3, p0, Lcom/google/d/a/a/li;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/li;->a:I

    iget v2, v2, Lcom/google/d/a/a/lj;->b:I

    iput v2, p0, Lcom/google/d/a/a/li;->d:I

    .line 600
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    .line 601
    iget-object v2, p0, Lcom/google/d/a/a/li;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 602
    iget v2, p0, Lcom/google/d/a/a/li;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/d/a/a/li;->a:I

    .line 604
    :cond_9
    iget v2, p1, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_a

    .line 605
    iget-object v2, p0, Lcom/google/d/a/a/li;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 606
    iget v2, p0, Lcom/google/d/a/a/li;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/li;->a:I

    .line 608
    :cond_a
    iget v2, p1, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_b

    .line 609
    iget v2, p1, Lcom/google/d/a/a/lg;->g:I

    iget v3, p0, Lcom/google/d/a/a/li;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/li;->a:I

    iput v2, p0, Lcom/google/d/a/a/li;->g:I

    .line 611
    :cond_b
    iget v2, p1, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    :goto_7
    if-eqz v0, :cond_12

    .line 612
    iget v0, p1, Lcom/google/d/a/a/lg;->h:I

    invoke-static {v0}, Lcom/google/d/a/a/ln;->a(I)Lcom/google/d/a/a/ln;

    move-result-object v0

    if-nez v0, :cond_c

    sget-object v0, Lcom/google/d/a/a/ln;->a:Lcom/google/d/a/a/ln;

    :cond_c
    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    move v2, v1

    .line 600
    goto :goto_4

    :cond_e
    move v2, v1

    .line 604
    goto :goto_5

    :cond_f
    move v2, v1

    .line 608
    goto :goto_6

    :cond_10
    move v0, v1

    .line 611
    goto :goto_7

    .line 612
    :cond_11
    iget v1, p0, Lcom/google/d/a/a/li;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/d/a/a/li;->a:I

    iget v0, v0, Lcom/google/d/a/a/ln;->j:I

    iput v0, p0, Lcom/google/d/a/a/li;->h:I

    .line 614
    :cond_12
    iget-object v0, p1, Lcom/google/d/a/a/lg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 516
    new-instance v2, Lcom/google/d/a/a/lg;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/lg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/li;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget v4, p0, Lcom/google/d/a/a/li;->b:I

    iput v4, v2, Lcom/google/d/a/a/lg;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/d/a/a/li;->c:I

    iput v4, v2, Lcom/google/d/a/a/lg;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/li;->d:I

    iput v4, v2, Lcom/google/d/a/a/lg;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/li;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/li;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/li;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/li;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/d/a/a/li;->g:I

    iput v1, v2, Lcom/google/d/a/a/lg;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/d/a/a/li;->h:I

    iput v1, v2, Lcom/google/d/a/a/lg;->h:I

    iput v0, v2, Lcom/google/d/a/a/lg;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 516
    check-cast p1, Lcom/google/d/a/a/lg;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/li;->a(Lcom/google/d/a/a/lg;)Lcom/google/d/a/a/li;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 619
    iget v0, p0, Lcom/google/d/a/a/li;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 620
    iget-object v0, p0, Lcom/google/d/a/a/li;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 631
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 619
    goto :goto_0

    .line 625
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/li;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 626
    iget-object v0, p0, Lcom/google/d/a/a/li;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 628
    goto :goto_1

    :cond_2
    move v0, v1

    .line 625
    goto :goto_2

    :cond_3
    move v0, v2

    .line 631
    goto :goto_1
.end method
