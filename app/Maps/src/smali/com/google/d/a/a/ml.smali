.class public final enum Lcom/google/d/a/a/ml;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/ml;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/ml;

.field public static final enum b:Lcom/google/d/a/a/ml;

.field public static final enum c:Lcom/google/d/a/a/ml;

.field public static final enum d:Lcom/google/d/a/a/ml;

.field public static final enum e:Lcom/google/d/a/a/ml;

.field private static final synthetic g:[Lcom/google/d/a/a/ml;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1843
    new-instance v0, Lcom/google/d/a/a/ml;

    const-string v1, "CONSTRUCTION_PLANNED"

    invoke-direct {v0, v1, v7, v3}, Lcom/google/d/a/a/ml;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ml;->a:Lcom/google/d/a/a/ml;

    .line 1847
    new-instance v0, Lcom/google/d/a/a/ml;

    const-string v1, "CONSTRUCTION_STARTED"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/d/a/a/ml;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ml;->b:Lcom/google/d/a/a/ml;

    .line 1851
    new-instance v0, Lcom/google/d/a/a/ml;

    const-string v1, "CONSTRUCTION_COMPLETE"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/d/a/a/ml;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ml;->c:Lcom/google/d/a/a/ml;

    .line 1855
    new-instance v0, Lcom/google/d/a/a/ml;

    const-string v1, "CONSTRUCTION_CLOSED_FOR_MAINTENANCE"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/d/a/a/ml;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ml;->d:Lcom/google/d/a/a/ml;

    .line 1859
    new-instance v0, Lcom/google/d/a/a/ml;

    const-string v1, "CONSTRUCTION_DISTURBED_BY_MAINTENANCE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/ml;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/ml;->e:Lcom/google/d/a/a/ml;

    .line 1838
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/d/a/a/ml;

    sget-object v1, Lcom/google/d/a/a/ml;->a:Lcom/google/d/a/a/ml;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/ml;->b:Lcom/google/d/a/a/ml;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/d/a/a/ml;->c:Lcom/google/d/a/a/ml;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/ml;->d:Lcom/google/d/a/a/ml;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/ml;->e:Lcom/google/d/a/a/ml;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/d/a/a/ml;->g:[Lcom/google/d/a/a/ml;

    .line 1904
    new-instance v0, Lcom/google/d/a/a/mm;

    invoke-direct {v0}, Lcom/google/d/a/a/mm;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1913
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1914
    iput p3, p0, Lcom/google/d/a/a/ml;->f:I

    .line 1915
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/ml;
    .locals 1

    .prologue
    .line 1889
    packed-switch p0, :pswitch_data_0

    .line 1895
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1890
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/ml;->a:Lcom/google/d/a/a/ml;

    goto :goto_0

    .line 1891
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/ml;->b:Lcom/google/d/a/a/ml;

    goto :goto_0

    .line 1892
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/ml;->c:Lcom/google/d/a/a/ml;

    goto :goto_0

    .line 1893
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/ml;->d:Lcom/google/d/a/a/ml;

    goto :goto_0

    .line 1894
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/ml;->e:Lcom/google/d/a/a/ml;

    goto :goto_0

    .line 1889
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/ml;
    .locals 1

    .prologue
    .line 1838
    const-class v0, Lcom/google/d/a/a/ml;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ml;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/ml;
    .locals 1

    .prologue
    .line 1838
    sget-object v0, Lcom/google/d/a/a/ml;->g:[Lcom/google/d/a/a/ml;

    invoke-virtual {v0}, [Lcom/google/d/a/a/ml;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/ml;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1885
    iget v0, p0, Lcom/google/d/a/a/ml;->f:I

    return v0
.end method
