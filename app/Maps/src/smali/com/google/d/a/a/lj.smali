.class public final enum Lcom/google/d/a/a/lj;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/lj;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/lj;

.field private static final synthetic c:[Lcom/google/d/a/a/lj;


# instance fields
.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 203
    new-instance v0, Lcom/google/d/a/a/lj;

    const-string v1, "TYPE_TEXT"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/lj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/lj;->a:Lcom/google/d/a/a/lj;

    .line 198
    new-array v0, v3, [Lcom/google/d/a/a/lj;

    sget-object v1, Lcom/google/d/a/a/lj;->a:Lcom/google/d/a/a/lj;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/d/a/a/lj;->c:[Lcom/google/d/a/a/lj;

    .line 228
    new-instance v0, Lcom/google/d/a/a/lk;

    invoke-direct {v0}, Lcom/google/d/a/a/lk;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 237
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 238
    iput p3, p0, Lcom/google/d/a/a/lj;->b:I

    .line 239
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/lj;
    .locals 1

    .prologue
    .line 217
    packed-switch p0, :pswitch_data_0

    .line 219
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 218
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/lj;->a:Lcom/google/d/a/a/lj;

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/lj;
    .locals 1

    .prologue
    .line 198
    const-class v0, Lcom/google/d/a/a/lj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/lj;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/lj;
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/google/d/a/a/lj;->c:[Lcom/google/d/a/a/lj;

    invoke-virtual {v0}, [Lcom/google/d/a/a/lj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/lj;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/google/d/a/a/lj;->b:I

    return v0
.end method
