.class public final Lcom/google/d/a/a/ae;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ah;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ac;",
        "Lcom/google/d/a/a/ae;",
        ">;",
        "Lcom/google/d/a/a/ah;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3273
    sget-object v0, Lcom/google/d/a/a/ac;->e:Lcom/google/d/a/a/ac;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3347
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ae;->b:Ljava/lang/Object;

    .line 3423
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/ae;->c:Ljava/lang/Object;

    .line 3499
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/ae;->d:I

    .line 3274
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ac;)Lcom/google/d/a/a/ae;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3311
    invoke-static {}, Lcom/google/d/a/a/ac;->d()Lcom/google/d/a/a/ac;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3326
    :goto_0
    return-object p0

    .line 3312
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 3313
    iget v2, p0, Lcom/google/d/a/a/ae;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/ae;->a:I

    .line 3314
    iget-object v2, p1, Lcom/google/d/a/a/ac;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/ae;->b:Ljava/lang/Object;

    .line 3317
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 3318
    iget v2, p0, Lcom/google/d/a/a/ae;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/ae;->a:I

    .line 3319
    iget-object v2, p1, Lcom/google/d/a/a/ac;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/ae;->c:Ljava/lang/Object;

    .line 3322
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/ac;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_8

    .line 3323
    iget v0, p1, Lcom/google/d/a/a/ac;->d:I

    invoke-static {v0}, Lcom/google/d/a/a/af;->a(I)Lcom/google/d/a/a/af;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/d/a/a/af;->a:Lcom/google/d/a/a/af;

    :cond_3
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 3312
    goto :goto_1

    :cond_5
    move v2, v1

    .line 3317
    goto :goto_2

    :cond_6
    move v0, v1

    .line 3322
    goto :goto_3

    .line 3323
    :cond_7
    iget v1, p0, Lcom/google/d/a/a/ae;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/d/a/a/ae;->a:I

    iget v0, v0, Lcom/google/d/a/a/af;->e:I

    iput v0, p0, Lcom/google/d/a/a/ae;->d:I

    .line 3325
    :cond_8
    iget-object v0, p1, Lcom/google/d/a/a/ac;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3265
    new-instance v2, Lcom/google/d/a/a/ac;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ac;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ae;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/d/a/a/ae;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/ac;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/d/a/a/ae;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/d/a/a/ac;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/d/a/a/ae;->d:I

    iput v1, v2, Lcom/google/d/a/a/ac;->d:I

    iput v0, v2, Lcom/google/d/a/a/ac;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3265
    check-cast p1, Lcom/google/d/a/a/ac;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ae;->a(Lcom/google/d/a/a/ac;)Lcom/google/d/a/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3330
    iget v2, p0, Lcom/google/d/a/a/ae;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 3342
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 3330
    goto :goto_0

    .line 3334
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/ae;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    .line 3338
    iget v2, p0, Lcom/google/d/a/a/ae;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    move v0, v1

    .line 3342
    goto :goto_1

    :cond_3
    move v2, v0

    .line 3334
    goto :goto_2

    :cond_4
    move v2, v0

    .line 3338
    goto :goto_3
.end method
