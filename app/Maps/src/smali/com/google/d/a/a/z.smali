.class public final Lcom/google/d/a/a/z;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/aa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/x;",
        "Lcom/google/d/a/a/z;",
        ">;",
        "Lcom/google/d/a/a/aa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 587
    sget-object v0, Lcom/google/d/a/a/x;->i:Lcom/google/d/a/a/x;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 710
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    .line 776
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    .line 874
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/d/a/a/z;->e:Z

    .line 906
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/z;->f:Ljava/lang/Object;

    .line 982
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/z;->g:Ljava/lang/Object;

    .line 1058
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/z;->h:Lcom/google/n/ao;

    .line 588
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/x;)Lcom/google/d/a/a/z;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 653
    invoke-static {}, Lcom/google/d/a/a/x;->d()Lcom/google/d/a/a/x;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 695
    :goto_0
    return-object p0

    .line 654
    :cond_0
    iget-object v2, p1, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 655
    iget-object v2, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 656
    iget-object v2, p1, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    .line 657
    iget v2, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/d/a/a/z;->a:I

    .line 664
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 665
    iget-object v2, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 666
    iget-object v2, p1, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    .line 667
    iget v2, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/d/a/a/z;->a:I

    .line 674
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 675
    iget-boolean v2, p1, Lcom/google/d/a/a/x;->d:Z

    iget v3, p0, Lcom/google/d/a/a/z;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/z;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/z;->d:Z

    .line 677
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 678
    iget-boolean v2, p1, Lcom/google/d/a/a/x;->e:Z

    iget v3, p0, Lcom/google/d/a/a/z;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/z;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/z;->e:Z

    .line 680
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 681
    iget v2, p0, Lcom/google/d/a/a/z;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/z;->a:I

    .line 682
    iget-object v2, p1, Lcom/google/d/a/a/x;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/z;->f:Ljava/lang/Object;

    .line 685
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 686
    iget v2, p0, Lcom/google/d/a/a/z;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/z;->a:I

    .line 687
    iget-object v2, p1, Lcom/google/d/a/a/x;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/d/a/a/z;->g:Ljava/lang/Object;

    .line 690
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/x;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    :goto_7
    if-eqz v0, :cond_7

    .line 691
    iget-object v0, p0, Lcom/google/d/a/a/z;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 692
    iget v0, p0, Lcom/google/d/a/a/z;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/z;->a:I

    .line 694
    :cond_7
    iget-object v0, p1, Lcom/google/d/a/a/x;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 659
    :cond_8
    iget v2, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/z;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/z;->a:I

    .line 660
    :cond_9
    iget-object v2, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 669
    :cond_a
    iget v2, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/z;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/z;->a:I

    .line 670
    :cond_b
    iget-object v2, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 674
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 677
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 680
    goto/16 :goto_5

    :cond_f
    move v2, v1

    .line 685
    goto :goto_6

    :cond_10
    move v0, v1

    .line 690
    goto :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 579
    new-instance v2, Lcom/google/d/a/a/x;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/x;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/z;->a:I

    iget v4, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/z;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/z;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/x;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/d/a/a/z;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/z;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/x;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_6

    :goto_0
    iget-boolean v4, p0, Lcom/google/d/a/a/z;->d:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/x;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget-boolean v4, p0, Lcom/google/d/a/a/z;->e:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/x;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v4, p0, Lcom/google/d/a/a/z;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/x;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v4, p0, Lcom/google/d/a/a/z;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/d/a/a/x;->g:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v3, v2, Lcom/google/d/a/a/x;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/z;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/z;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/x;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 579
    check-cast p1, Lcom/google/d/a/a/x;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/z;->a(Lcom/google/d/a/a/x;)Lcom/google/d/a/a/z;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 699
    iget v0, p0, Lcom/google/d/a/a/z;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/google/d/a/a/z;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 705
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 699
    goto :goto_0

    :cond_1
    move v0, v2

    .line 705
    goto :goto_1
.end method
