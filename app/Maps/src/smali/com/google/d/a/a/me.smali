.class public final enum Lcom/google/d/a/a/me;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/me;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/me;

.field public static final enum b:Lcom/google/d/a/a/me;

.field public static final enum c:Lcom/google/d/a/a/me;

.field public static final enum d:Lcom/google/d/a/a/me;

.field public static final enum e:Lcom/google/d/a/a/me;

.field public static final enum f:Lcom/google/d/a/a/me;

.field private static final synthetic h:[Lcom/google/d/a/a/me;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1986
    new-instance v0, Lcom/google/d/a/a/me;

    const-string v1, "BICYCLE_FACILITY_SEPARATE_TRAIL"

    invoke-direct {v0, v1, v7, v4}, Lcom/google/d/a/a/me;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/me;->a:Lcom/google/d/a/a/me;

    .line 1990
    new-instance v0, Lcom/google/d/a/a/me;

    const-string v1, "BICYCLE_FACILITY_PEDESTRIAN_PATH"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/d/a/a/me;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/me;->b:Lcom/google/d/a/a/me;

    .line 1994
    new-instance v0, Lcom/google/d/a/a/me;

    const-string v1, "BICYCLE_FACILITY_SHARED_ROAD"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/d/a/a/me;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/me;->c:Lcom/google/d/a/a/me;

    .line 1998
    new-instance v0, Lcom/google/d/a/a/me;

    const-string v1, "BICYCLE_FACILITY_BIKE_LANE"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/me;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/me;->d:Lcom/google/d/a/a/me;

    .line 2002
    new-instance v0, Lcom/google/d/a/a/me;

    const-string v1, "BICYCLE_FACILITY_WIDE_SHOULDER"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/me;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/me;->e:Lcom/google/d/a/a/me;

    .line 2006
    new-instance v0, Lcom/google/d/a/a/me;

    const-string v1, "BICYCLE_FACILITY_SHARROW"

    const/4 v2, 0x5

    const/16 v3, 0x33

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/me;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/me;->f:Lcom/google/d/a/a/me;

    .line 1981
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/d/a/a/me;

    sget-object v1, Lcom/google/d/a/a/me;->a:Lcom/google/d/a/a/me;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/me;->b:Lcom/google/d/a/a/me;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/me;->c:Lcom/google/d/a/a/me;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/me;->d:Lcom/google/d/a/a/me;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/me;->e:Lcom/google/d/a/a/me;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/me;->f:Lcom/google/d/a/a/me;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/me;->h:[Lcom/google/d/a/a/me;

    .line 2056
    new-instance v0, Lcom/google/d/a/a/mf;

    invoke-direct {v0}, Lcom/google/d/a/a/mf;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2065
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2066
    iput p3, p0, Lcom/google/d/a/a/me;->g:I

    .line 2067
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/me;
    .locals 1

    .prologue
    .line 2040
    sparse-switch p0, :sswitch_data_0

    .line 2047
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2041
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/me;->a:Lcom/google/d/a/a/me;

    goto :goto_0

    .line 2042
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/me;->b:Lcom/google/d/a/a/me;

    goto :goto_0

    .line 2043
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/me;->c:Lcom/google/d/a/a/me;

    goto :goto_0

    .line 2044
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/me;->d:Lcom/google/d/a/a/me;

    goto :goto_0

    .line 2045
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/me;->e:Lcom/google/d/a/a/me;

    goto :goto_0

    .line 2046
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/me;->f:Lcom/google/d/a/a/me;

    goto :goto_0

    .line 2040
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x31 -> :sswitch_3
        0x32 -> :sswitch_4
        0x33 -> :sswitch_5
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/me;
    .locals 1

    .prologue
    .line 1981
    const-class v0, Lcom/google/d/a/a/me;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/me;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/me;
    .locals 1

    .prologue
    .line 1981
    sget-object v0, Lcom/google/d/a/a/me;->h:[Lcom/google/d/a/a/me;

    invoke-virtual {v0}, [Lcom/google/d/a/a/me;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/me;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2036
    iget v0, p0, Lcom/google/d/a/a/me;->g:I

    return v0
.end method
