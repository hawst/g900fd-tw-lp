.class public final Lcom/google/d/a/a/ez;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/fa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ex;",
        "Lcom/google/d/a/a/ez;",
        ">;",
        "Lcom/google/d/a/a/fa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 400
    sget-object v0, Lcom/google/d/a/a/ex;->e:Lcom/google/d/a/a/ex;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 496
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    .line 633
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    .line 769
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/ez;->d:Lcom/google/n/ao;

    .line 401
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ex;)Lcom/google/d/a/a/ez;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 442
    invoke-static {}, Lcom/google/d/a/a/ex;->d()Lcom/google/d/a/a/ex;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 468
    :goto_0
    return-object p0

    .line 443
    :cond_0
    iget-object v1, p1, Lcom/google/d/a/a/ex;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 444
    iget-object v1, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 445
    iget-object v1, p1, Lcom/google/d/a/a/ex;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    .line 446
    iget v1, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/d/a/a/ez;->a:I

    .line 453
    :cond_1
    :goto_1
    iget-object v1, p1, Lcom/google/d/a/a/ex;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 454
    iget-object v1, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 455
    iget-object v1, p1, Lcom/google/d/a/a/ex;->c:Ljava/util/List;

    iput-object v1, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    .line 456
    iget v1, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/d/a/a/ez;->a:I

    .line 463
    :cond_2
    :goto_2
    iget v1, p1, Lcom/google/d/a/a/ex;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_3
    if-eqz v0, :cond_3

    .line 464
    iget-object v0, p0, Lcom/google/d/a/a/ez;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/ex;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 465
    iget v0, p0, Lcom/google/d/a/a/ez;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/d/a/a/ez;->a:I

    .line 467
    :cond_3
    iget-object v0, p1, Lcom/google/d/a/a/ex;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 448
    :cond_4
    iget v1, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/ez;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/d/a/a/ez;->a:I

    .line 449
    :cond_5
    iget-object v1, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/d/a/a/ex;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 458
    :cond_6
    iget v1, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/ez;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/d/a/a/ez;->a:I

    .line 459
    :cond_7
    iget-object v1, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    iget-object v2, p1, Lcom/google/d/a/a/ex;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 463
    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 392
    new-instance v2, Lcom/google/d/a/a/ex;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ex;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/ez;->a:I

    iget v4, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/d/a/a/ez;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ex;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/d/a/a/ez;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/ex;->c:Ljava/util/List;

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    :goto_0
    iget-object v3, v2, Lcom/google/d/a/a/ex;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/ez;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/ez;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/ex;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 392
    check-cast p1, Lcom/google/d/a/a/ex;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/ez;->a(Lcom/google/d/a/a/ex;)Lcom/google/d/a/a/ez;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 472
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 473
    iget-object v0, p0, Lcom/google/d/a/a/ez;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 490
    :cond_0
    :goto_1
    return v2

    .line 472
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 478
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 479
    iget-object v0, p0, Lcom/google/d/a/a/ez;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 484
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/ez;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 485
    iget-object v0, p0, Lcom/google/d/a/a/ez;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 490
    goto :goto_1

    :cond_5
    move v0, v2

    .line 484
    goto :goto_3
.end method
