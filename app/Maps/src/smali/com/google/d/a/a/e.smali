.class public final Lcom/google/d/a/a/e;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/b;",
        "Lcom/google/d/a/a/e;",
        ">;",
        "Lcom/google/d/a/a/j;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:F

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Z

.field private i:Z

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 905
    sget-object v0, Lcom/google/d/a/a/b;->n:Lcom/google/d/a/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1105
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/e;->b:Lcom/google/n/ao;

    .line 1164
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/e;->c:Lcom/google/n/ao;

    .line 1287
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/e;->f:Lcom/google/n/ao;

    .line 1346
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/e;->g:Lcom/google/n/ao;

    .line 1470
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    .line 1606
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/e;->k:I

    .line 1643
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    .line 1716
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/e;->m:Lcom/google/n/ao;

    .line 906
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/b;)Lcom/google/d/a/a/e;
    .locals 5

    .prologue
    const/16 v4, 0x100

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1009
    invoke-static {}, Lcom/google/d/a/a/b;->d()Lcom/google/d/a/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1066
    :goto_0
    return-object p0

    .line 1010
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1011
    iget-object v2, p0, Lcom/google/d/a/a/e;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1012
    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/d/a/a/e;->a:I

    .line 1014
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1015
    iget-object v2, p0, Lcom/google/d/a/a/e;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1016
    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/d/a/a/e;->a:I

    .line 1018
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1019
    iget v2, p1, Lcom/google/d/a/a/b;->d:I

    iget v3, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/e;->a:I

    iput v2, p0, Lcom/google/d/a/a/e;->d:I

    .line 1021
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1022
    iget v2, p1, Lcom/google/d/a/a/b;->e:F

    iget v3, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/e;->a:I

    iput v2, p0, Lcom/google/d/a/a/e;->e:F

    .line 1024
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1025
    iget-object v2, p0, Lcom/google/d/a/a/e;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1026
    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/d/a/a/e;->a:I

    .line 1028
    :cond_5
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1029
    iget-object v2, p0, Lcom/google/d/a/a/e;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1030
    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/d/a/a/e;->a:I

    .line 1032
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1033
    iget-boolean v2, p1, Lcom/google/d/a/a/b;->h:Z

    iget v3, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/e;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/e;->h:Z

    .line 1035
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1036
    iget-boolean v2, p1, Lcom/google/d/a/a/b;->i:Z

    iget v3, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/d/a/a/e;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/e;->i:Z

    .line 1038
    :cond_8
    iget-object v2, p1, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1039
    iget-object v2, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1040
    iget-object v2, p1, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    .line 1041
    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit16 v2, v2, -0x101

    iput v2, p0, Lcom/google/d/a/a/e;->a:I

    .line 1048
    :cond_9
    :goto_9
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v2, v2, 0x100

    if-ne v2, v4, :cond_15

    move v2, v0

    :goto_a
    if-eqz v2, :cond_17

    .line 1049
    iget v2, p1, Lcom/google/d/a/a/b;->k:I

    invoke-static {v2}, Lcom/google/d/a/a/f;->a(I)Lcom/google/d/a/a/f;

    move-result-object v2

    if-nez v2, :cond_a

    sget-object v2, Lcom/google/d/a/a/f;->a:Lcom/google/d/a/a/f;

    :cond_a
    if-nez v2, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move v2, v1

    .line 1010
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 1014
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 1018
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 1021
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 1024
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 1028
    goto :goto_6

    :cond_11
    move v2, v1

    .line 1032
    goto :goto_7

    :cond_12
    move v2, v1

    .line 1035
    goto :goto_8

    .line 1043
    :cond_13
    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit16 v2, v2, 0x100

    if-eq v2, v4, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/d/a/a/e;->a:I

    .line 1044
    :cond_14
    iget-object v2, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9

    :cond_15
    move v2, v1

    .line 1048
    goto :goto_a

    .line 1049
    :cond_16
    iget v3, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/d/a/a/e;->a:I

    iget v2, v2, Lcom/google/d/a/a/f;->c:I

    iput v2, p0, Lcom/google/d/a/a/e;->k:I

    .line 1051
    :cond_17
    iget-object v2, p1, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_18

    .line 1052
    iget-object v2, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1053
    iget-object v2, p1, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    iput-object v2, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    .line 1054
    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit16 v2, v2, -0x401

    iput v2, p0, Lcom/google/d/a/a/e;->a:I

    .line 1061
    :cond_18
    :goto_b
    iget v2, p1, Lcom/google/d/a/a/b;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1c

    :goto_c
    if-eqz v0, :cond_19

    .line 1062
    iget-object v0, p0, Lcom/google/d/a/a/e;->m:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/b;->m:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1063
    iget v0, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/d/a/a/e;->a:I

    .line 1065
    :cond_19
    iget-object v0, p1, Lcom/google/d/a/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 1056
    :cond_1a
    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-eq v2, v3, :cond_1b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    iget v2, p0, Lcom/google/d/a/a/e;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/d/a/a/e;->a:I

    .line 1057
    :cond_1b
    iget-object v2, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    iget-object v3, p1, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_b

    :cond_1c
    move v0, v1

    .line 1061
    goto :goto_c
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 897
    new-instance v2, Lcom/google/d/a/a/b;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_b

    :goto_0
    iget-object v4, v2, Lcom/google/d/a/a/b;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/e;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/e;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/d/a/a/b;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/e;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/e;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/d/a/a/e;->d:I

    iput v4, v2, Lcom/google/d/a/a/b;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/d/a/a/e;->e:F

    iput v4, v2, Lcom/google/d/a/a/b;->e:F

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/d/a/a/b;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/e;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/e;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/d/a/a/b;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/e;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/e;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v4, p0, Lcom/google/d/a/a/e;->h:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/b;->h:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v4, p0, Lcom/google/d/a/a/e;->i:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/b;->i:Z

    iget v4, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit16 v4, v4, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    iget-object v4, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit16 v4, v4, -0x101

    iput v4, p0, Lcom/google/d/a/a/e;->a:I

    :cond_7
    iget-object v4, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/b;->j:Ljava/util/List;

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget v4, p0, Lcom/google/d/a/a/e;->k:I

    iput v4, v2, Lcom/google/d/a/a/b;->k:I

    iget v4, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    iget v4, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit16 v4, v4, -0x401

    iput v4, p0, Lcom/google/d/a/a/e;->a:I

    :cond_9
    iget-object v4, p0, Lcom/google/d/a/a/e;->l:Ljava/util/List;

    iput-object v4, v2, Lcom/google/d/a/a/b;->l:Ljava/util/List;

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_a

    or-int/lit16 v0, v0, 0x200

    :cond_a
    iget-object v3, v2, Lcom/google/d/a/a/b;->m:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/e;->m:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/e;->m:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/d/a/a/b;->a:I

    return-object v2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 897
    check-cast p1, Lcom/google/d/a/a/b;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/e;->a(Lcom/google/d/a/a/b;)Lcom/google/d/a/a/e;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1070
    iget v0, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1071
    iget-object v0, p0, Lcom/google/d/a/a/e;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1100
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1070
    goto :goto_0

    .line 1076
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1077
    iget-object v0, p0, Lcom/google/d/a/a/e;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1083
    iget-object v0, p0, Lcom/google/d/a/a/e;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1088
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/e;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_5

    .line 1089
    iget-object v0, p0, Lcom/google/d/a/a/e;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    move v1, v2

    .line 1094
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 1095
    iget-object v0, p0, Lcom/google/d/a/a/e;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1094
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_6
    move v0, v2

    .line 1076
    goto :goto_2

    :cond_7
    move v0, v2

    .line 1082
    goto :goto_3

    :cond_8
    move v0, v2

    .line 1088
    goto :goto_4

    :cond_9
    move v2, v3

    .line 1100
    goto/16 :goto_1
.end method
