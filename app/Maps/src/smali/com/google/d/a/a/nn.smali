.class public final Lcom/google/d/a/a/nn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/nx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/nn;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/d/a/a/nn;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:I

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/n/aq;

.field g:Z

.field h:I

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lcom/google/d/a/a/no;

    invoke-direct {v0}, Lcom/google/d/a/a/no;-><init>()V

    sput-object v0, Lcom/google/d/a/a/nn;->PARSER:Lcom/google/n/ax;

    .line 668
    new-instance v0, Lcom/google/d/a/a/np;

    invoke-direct {v0}, Lcom/google/d/a/a/np;-><init>()V

    .line 853
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/nn;->n:Lcom/google/n/aw;

    .line 1762
    new-instance v0, Lcom/google/d/a/a/nn;

    invoke-direct {v0}, Lcom/google/d/a/a/nn;-><init>()V

    sput-object v0, Lcom/google/d/a/a/nn;->k:Lcom/google/d/a/a/nn;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 131
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 515
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nn;->b:Lcom/google/n/ao;

    .line 531
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    .line 739
    iput-byte v3, p0, Lcom/google/d/a/a/nn;->l:B

    .line 794
    iput v3, p0, Lcom/google/d/a/a/nn;->m:I

    .line 132
    iget-object v0, p0, Lcom/google/d/a/a/nn;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 133
    iget-object v0, p0, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 134
    iput v2, p0, Lcom/google/d/a/a/nn;->d:I

    .line 135
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    .line 136
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    .line 137
    iput-boolean v2, p0, Lcom/google/d/a/a/nn;->g:Z

    .line 138
    iput v2, p0, Lcom/google/d/a/a/nn;->h:I

    .line 139
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    .line 140
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    .line 141
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const/16 v12, 0x10

    const/16 v11, 0x8

    const/4 v2, 0x1

    const/16 v10, 0x80

    const/4 v3, 0x0

    .line 147
    invoke-direct {p0}, Lcom/google/d/a/a/nn;-><init>()V

    .line 150
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 153
    :cond_0
    :goto_0
    if-nez v4, :cond_11

    .line 154
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 155
    sparse-switch v0, :sswitch_data_0

    .line 160
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 162
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 158
    goto :goto_0

    .line 167
    :sswitch_1
    iget-object v0, p0, Lcom/google/d/a/a/nn;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 168
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/nn;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x8

    if-ne v2, v11, :cond_1

    .line 274
    iget-object v2, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    .line 276
    :cond_1
    and-int/lit8 v2, v1, 0x10

    if-ne v2, v12, :cond_2

    .line 277
    iget-object v2, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    .line 279
    :cond_2
    and-int/lit16 v2, v1, 0x80

    if-ne v2, v10, :cond_3

    .line 280
    iget-object v2, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    .line 282
    :cond_3
    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_4

    .line 283
    iget-object v1, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    .line 285
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/nn;->au:Lcom/google/n/bn;

    throw v0

    .line 172
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 173
    invoke-static {v0}, Lcom/google/d/a/a/nt;->a(I)Lcom/google/d/a/a/nt;

    move-result-object v6

    .line 174
    if-nez v6, :cond_5

    .line 175
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 269
    :catch_1
    move-exception v0

    .line 270
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 271
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    :cond_5
    :try_start_4
    iget v6, p0, Lcom/google/d/a/a/nn;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/d/a/a/nn;->a:I

    .line 178
    iput v0, p0, Lcom/google/d/a/a/nn;->d:I

    goto/16 :goto_0

    .line 183
    :sswitch_3
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v11, :cond_6

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    .line 186
    or-int/lit8 v1, v1, 0x8

    .line 188
    :cond_6
    iget-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 189
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 188
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 193
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 194
    and-int/lit8 v6, v1, 0x10

    if-eq v6, v12, :cond_7

    .line 195
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    .line 196
    or-int/lit8 v1, v1, 0x10

    .line 198
    :cond_7
    iget-object v6, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 202
    :sswitch_5
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/d/a/a/nn;->a:I

    .line 203
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/d/a/a/nn;->g:Z

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_1

    .line 207
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 208
    invoke-static {v0}, Lcom/google/d/a/a/nv;->a(I)Lcom/google/d/a/a/nv;

    move-result-object v6

    .line 209
    if-nez v6, :cond_9

    .line 210
    const/4 v6, 0x6

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 212
    :cond_9
    iget v6, p0, Lcom/google/d/a/a/nn;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/d/a/a/nn;->a:I

    .line 213
    iput v0, p0, Lcom/google/d/a/a/nn;->h:I

    goto/16 :goto_0

    .line 218
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 219
    invoke-static {v0}, Lcom/google/d/a/a/nr;->a(I)Lcom/google/d/a/a/nr;

    move-result-object v6

    .line 220
    if-nez v6, :cond_a

    .line 221
    const/4 v6, 0x7

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 223
    :cond_a
    and-int/lit16 v6, v1, 0x80

    if-eq v6, v10, :cond_b

    .line 224
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    .line 225
    or-int/lit16 v1, v1, 0x80

    .line 227
    :cond_b
    iget-object v6, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 232
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 233
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 234
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_c

    const/4 v0, -0x1

    :goto_3
    if-lez v0, :cond_f

    .line 235
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 236
    invoke-static {v0}, Lcom/google/d/a/a/nr;->a(I)Lcom/google/d/a/a/nr;

    move-result-object v7

    .line 237
    if-nez v7, :cond_d

    .line 238
    const/4 v7, 0x7

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_2

    .line 234
    :cond_c
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 240
    :cond_d
    and-int/lit16 v7, v1, 0x80

    if-eq v7, v10, :cond_e

    .line 241
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    .line 242
    or-int/lit16 v1, v1, 0x80

    .line 244
    :cond_e
    iget-object v7, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 247
    :cond_f
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 251
    :sswitch_9
    and-int/lit16 v0, v1, 0x100

    const/16 v6, 0x100

    if-eq v0, v6, :cond_10

    .line 252
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    .line 254
    or-int/lit16 v1, v1, 0x100

    .line 256
    :cond_10
    iget-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 257
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 256
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 261
    :sswitch_a
    iget-object v0, p0, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 262
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/d/a/a/nn;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 273
    :cond_11
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v11, :cond_12

    .line 274
    iget-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    .line 276
    :cond_12
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v12, :cond_13

    .line 277
    iget-object v0, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    .line 279
    :cond_13
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v10, :cond_14

    .line 280
    iget-object v0, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    .line 282
    :cond_14
    and-int/lit16 v0, v1, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_15

    .line 283
    iget-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    .line 285
    :cond_15
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/nn;->au:Lcom/google/n/bn;

    .line 286
    return-void

    .line 155
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 129
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 515
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nn;->b:Lcom/google/n/ao;

    .line 531
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    .line 739
    iput-byte v1, p0, Lcom/google/d/a/a/nn;->l:B

    .line 794
    iput v1, p0, Lcom/google/d/a/a/nn;->m:I

    .line 130
    return-void
.end method

.method public static d()Lcom/google/d/a/a/nn;
    .locals 1

    .prologue
    .line 1765
    sget-object v0, Lcom/google/d/a/a/nn;->k:Lcom/google/d/a/a/nn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/nq;
    .locals 1

    .prologue
    .line 915
    new-instance v0, Lcom/google/d/a/a/nq;

    invoke-direct {v0}, Lcom/google/d/a/a/nq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/nn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 300
    sget-object v0, Lcom/google/d/a/a/nn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 763
    invoke-virtual {p0}, Lcom/google/d/a/a/nn;->c()I

    .line 764
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 765
    iget-object v0, p0, Lcom/google/d/a/a/nn;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 767
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 768
    iget v0, p0, Lcom/google/d/a/a/nn;->d:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_1
    :goto_0
    move v1, v2

    .line 770
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 771
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 770
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 768
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 773
    :goto_2
    iget-object v1, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 774
    iget-object v1, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v1, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v1

    invoke-static {v5, v6}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 773
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 776
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_5

    .line 777
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/d/a/a/nn;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_7

    move v0, v3

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 779
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 780
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/d/a/a/nn;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_8

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_6
    :goto_4
    move v1, v2

    .line 782
    :goto_5
    iget-object v0, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 783
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 782
    :goto_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_7
    move v0, v2

    .line 777
    goto :goto_3

    .line 780
    :cond_8
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 783
    :cond_9
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_6

    .line 785
    :cond_a
    :goto_7
    iget-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 786
    iget-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 785
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 788
    :cond_b
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_c

    .line 789
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 791
    :cond_c
    iget-object v0, p0, Lcom/google/d/a/a/nn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 792
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 741
    iget-byte v0, p0, Lcom/google/d/a/a/nn;->l:B

    .line 742
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 758
    :cond_0
    :goto_0
    return v2

    .line 743
    :cond_1
    if-eqz v0, :cond_0

    .line 745
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 746
    iget-object v0, p0, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/g/a/b;->d()Lcom/google/g/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/g/a/b;

    invoke-virtual {v0}, Lcom/google/g/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 747
    iput-byte v2, p0, Lcom/google/d/a/a/nn;->l:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 745
    goto :goto_1

    :cond_3
    move v1, v2

    .line 751
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 752
    iget-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 753
    iput-byte v2, p0, Lcom/google/d/a/a/nn;->l:B

    goto :goto_0

    .line 751
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 757
    :cond_5
    iput-byte v3, p0, Lcom/google/d/a/a/nn;->l:B

    move v2, v3

    .line 758
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 796
    iget v0, p0, Lcom/google/d/a/a/nn;->m:I

    .line 797
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 848
    :goto_0
    return v0

    .line 800
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 801
    iget-object v0, p0, Lcom/google/d/a/a/nn;->b:Lcom/google/n/ao;

    .line 802
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 804
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_1

    .line 805
    iget v2, p0, Lcom/google/d/a/a/nn;->d:I

    .line 806
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v4, v0

    .line 808
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 809
    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/nn;->e:Ljava/util/List;

    .line 810
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 808
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_2
    move v2, v3

    .line 806
    goto :goto_2

    :cond_3
    move v0, v1

    move v2, v1

    .line 814
    :goto_4
    iget-object v5, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_4

    .line 815
    iget-object v5, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    .line 816
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 814
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 818
    :cond_4
    add-int v0, v4, v2

    .line 819
    iget-object v2, p0, Lcom/google/d/a/a/nn;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 821
    iget v2, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v8, :cond_5

    .line 822
    const/4 v2, 0x5

    iget-boolean v4, p0, Lcom/google/d/a/a/nn;->g:Z

    .line 823
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 825
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_b

    .line 826
    const/4 v2, 0x6

    iget v4, p0, Lcom/google/d/a/a/nn;->h:I

    .line 827
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_5
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    move v2, v0

    :goto_6
    move v4, v1

    move v5, v1

    .line 831
    :goto_7
    iget-object v0, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_8

    .line 832
    iget-object v0, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    .line 833
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v5, v0

    .line 831
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_7

    :cond_6
    move v2, v3

    .line 827
    goto :goto_5

    :cond_7
    move v0, v3

    .line 833
    goto :goto_8

    .line 835
    :cond_8
    add-int v0, v2, v5

    .line 836
    iget-object v2, p0, Lcom/google/d/a/a/nn;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v1

    move v3, v0

    .line 838
    :goto_9
    iget-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 839
    iget-object v0, p0, Lcom/google/d/a/a/nn;->j:Ljava/util/List;

    .line 840
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 838
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 842
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/nn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_a

    .line 843
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/d/a/a/nn;->c:Lcom/google/n/ao;

    .line 844
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 846
    :cond_a
    iget-object v0, p0, Lcom/google/d/a/a/nn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 847
    iput v0, p0, Lcom/google/d/a/a/nn;->m:I

    goto/16 :goto_0

    :cond_b
    move v2, v0

    goto :goto_6

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 123
    invoke-static {}, Lcom/google/d/a/a/nn;->newBuilder()Lcom/google/d/a/a/nq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/nq;->a(Lcom/google/d/a/a/nn;)Lcom/google/d/a/a/nq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 123
    invoke-static {}, Lcom/google/d/a/a/nn;->newBuilder()Lcom/google/d/a/a/nq;

    move-result-object v0

    return-object v0
.end method
