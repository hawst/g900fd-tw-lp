.class public final Lcom/google/d/a/a/fz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/gc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/fz;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/a/a/fz;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/google/d/a/a/ga;

    invoke-direct {v0}, Lcom/google/d/a/a/ga;-><init>()V

    sput-object v0, Lcom/google/d/a/a/fz;->PARSER:Lcom/google/n/ax;

    .line 256
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/fz;->g:Lcom/google/n/aw;

    .line 553
    new-instance v0, Lcom/google/d/a/a/fz;

    invoke-direct {v0}, Lcom/google/d/a/a/fz;-><init>()V

    sput-object v0, Lcom/google/d/a/a/fz;->d:Lcom/google/d/a/a/fz;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 54
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 205
    iput-byte v0, p0, Lcom/google/d/a/a/fz;->e:B

    .line 235
    iput v0, p0, Lcom/google/d/a/a/fz;->f:I

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/fz;->b:Ljava/lang/Object;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/fz;->c:Ljava/lang/Object;

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 63
    invoke-direct {p0}, Lcom/google/d/a/a/fz;-><init>()V

    .line 64
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 68
    const/4 v0, 0x0

    .line 69
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 71
    sparse-switch v3, :sswitch_data_0

    .line 76
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 78
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 84
    iget v4, p0, Lcom/google/d/a/a/fz;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/fz;->a:I

    .line 85
    iput-object v3, p0, Lcom/google/d/a/a/fz;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/fz;->au:Lcom/google/n/bn;

    throw v0

    .line 89
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 90
    iget v4, p0, Lcom/google/d/a/a/fz;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/fz;->a:I

    .line 91
    iput-object v3, p0, Lcom/google/d/a/a/fz;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 98
    :catch_1
    move-exception v0

    .line 99
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 100
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 102
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fz;->au:Lcom/google/n/bn;

    .line 103
    return-void

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 52
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 205
    iput-byte v0, p0, Lcom/google/d/a/a/fz;->e:B

    .line 235
    iput v0, p0, Lcom/google/d/a/a/fz;->f:I

    .line 53
    return-void
.end method

.method public static d()Lcom/google/d/a/a/fz;
    .locals 1

    .prologue
    .line 556
    sget-object v0, Lcom/google/d/a/a/fz;->d:Lcom/google/d/a/a/fz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/gb;
    .locals 1

    .prologue
    .line 318
    new-instance v0, Lcom/google/d/a/a/gb;

    invoke-direct {v0}, Lcom/google/d/a/a/gb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/fz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    sget-object v0, Lcom/google/d/a/a/fz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 225
    invoke-virtual {p0}, Lcom/google/d/a/a/fz;->c()I

    .line 226
    iget v0, p0, Lcom/google/d/a/a/fz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/d/a/a/fz;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fz;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 229
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/fz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 230
    iget-object v0, p0, Lcom/google/d/a/a/fz;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fz;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/fz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 233
    return-void

    .line 227
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 230
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 207
    iget-byte v2, p0, Lcom/google/d/a/a/fz;->e:B

    .line 208
    if-ne v2, v0, :cond_0

    .line 220
    :goto_0
    return v0

    .line 209
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 211
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/fz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 212
    iput-byte v1, p0, Lcom/google/d/a/a/fz;->e:B

    move v0, v1

    .line 213
    goto :goto_0

    :cond_2
    move v2, v1

    .line 211
    goto :goto_1

    .line 215
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/fz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 216
    iput-byte v1, p0, Lcom/google/d/a/a/fz;->e:B

    move v0, v1

    .line 217
    goto :goto_0

    :cond_4
    move v2, v1

    .line 215
    goto :goto_2

    .line 219
    :cond_5
    iput-byte v0, p0, Lcom/google/d/a/a/fz;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 237
    iget v0, p0, Lcom/google/d/a/a/fz;->f:I

    .line 238
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 251
    :goto_0
    return v0

    .line 241
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/fz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 243
    iget-object v0, p0, Lcom/google/d/a/a/fz;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fz;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 245
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/fz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 247
    iget-object v0, p0, Lcom/google/d/a/a/fz;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/fz;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/fz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 250
    iput v0, p0, Lcom/google/d/a/a/fz;->f:I

    goto :goto_0

    .line 243
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 247
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/d/a/a/fz;->newBuilder()Lcom/google/d/a/a/gb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/gb;->a(Lcom/google/d/a/a/fz;)Lcom/google/d/a/a/gb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/d/a/a/fz;->newBuilder()Lcom/google/d/a/a/gb;

    move-result-object v0

    return-object v0
.end method
