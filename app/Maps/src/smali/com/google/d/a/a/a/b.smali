.class public final Lcom/google/d/a/a/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/a/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/d/a/a/a/b;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3961
    new-instance v0, Lcom/google/d/a/a/a/c;

    invoke-direct {v0}, Lcom/google/d/a/a/a/c;-><init>()V

    sput-object v0, Lcom/google/d/a/a/a/b;->PARSER:Lcom/google/n/ax;

    .line 4237
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/a/b;->n:Lcom/google/n/aw;

    .line 5015
    new-instance v0, Lcom/google/d/a/a/a/b;

    invoke-direct {v0}, Lcom/google/d/a/a/a/b;-><init>()V

    sput-object v0, Lcom/google/d/a/a/a/b;->k:Lcom/google/d/a/a/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3870
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3978
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->b:Lcom/google/n/ao;

    .line 3994
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->c:Lcom/google/n/ao;

    .line 4010
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->d:Lcom/google/n/ao;

    .line 4026
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->e:Lcom/google/n/ao;

    .line 4042
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->f:Lcom/google/n/ao;

    .line 4058
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->g:Lcom/google/n/ao;

    .line 4074
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->h:Lcom/google/n/ao;

    .line 4090
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->i:Lcom/google/n/ao;

    .line 4106
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->j:Lcom/google/n/ao;

    .line 4121
    iput-byte v3, p0, Lcom/google/d/a/a/a/b;->l:B

    .line 4188
    iput v3, p0, Lcom/google/d/a/a/a/b;->m:I

    .line 3871
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3872
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3873
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3874
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3875
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3876
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3877
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3878
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3879
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3880
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3886
    invoke-direct {p0}, Lcom/google/d/a/a/a/b;-><init>()V

    .line 3887
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 3892
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 3893
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 3894
    sparse-switch v3, :sswitch_data_0

    .line 3899
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 3901
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 3897
    goto :goto_0

    .line 3906
    :sswitch_1
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3907
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3952
    :catch_0
    move-exception v0

    .line 3953
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3958
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 3911
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3912
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3954
    :catch_1
    move-exception v0

    .line 3955
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3956
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3916
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3917
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I

    goto :goto_0

    .line 3921
    :sswitch_4
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3922
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I

    goto :goto_0

    .line 3926
    :sswitch_5
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3927
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I

    goto/16 :goto_0

    .line 3931
    :sswitch_6
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3932
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I

    goto/16 :goto_0

    .line 3936
    :sswitch_7
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3937
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I

    goto/16 :goto_0

    .line 3941
    :sswitch_8
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3942
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I

    goto/16 :goto_0

    .line 3946
    :sswitch_9
    iget-object v3, p0, Lcom/google/d/a/a/a/b;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3947
    iget v3, p0, Lcom/google/d/a/a/a/b;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/d/a/a/a/b;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 3958
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->au:Lcom/google/n/bn;

    .line 3959
    return-void

    .line 3894
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x52 -> :sswitch_7
        0x5a -> :sswitch_8
        0x62 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 3868
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3978
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->b:Lcom/google/n/ao;

    .line 3994
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->c:Lcom/google/n/ao;

    .line 4010
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->d:Lcom/google/n/ao;

    .line 4026
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->e:Lcom/google/n/ao;

    .line 4042
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->f:Lcom/google/n/ao;

    .line 4058
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->g:Lcom/google/n/ao;

    .line 4074
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->h:Lcom/google/n/ao;

    .line 4090
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->i:Lcom/google/n/ao;

    .line 4106
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/a/b;->j:Lcom/google/n/ao;

    .line 4121
    iput-byte v1, p0, Lcom/google/d/a/a/a/b;->l:B

    .line 4188
    iput v1, p0, Lcom/google/d/a/a/a/b;->m:I

    .line 3869
    return-void
.end method

.method public static d()Lcom/google/d/a/a/a/b;
    .locals 1

    .prologue
    .line 5018
    sget-object v0, Lcom/google/d/a/a/a/b;->k:Lcom/google/d/a/a/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/a/d;
    .locals 1

    .prologue
    .line 4299
    new-instance v0, Lcom/google/d/a/a/a/d;

    invoke-direct {v0}, Lcom/google/d/a/a/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3973
    sget-object v0, Lcom/google/d/a/a/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 4157
    invoke-virtual {p0}, Lcom/google/d/a/a/a/b;->c()I

    .line 4158
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 4159
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4161
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4162
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/d/a/a/a/b;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4164
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 4165
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4167
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 4168
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/d/a/a/a/b;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4170
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 4171
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/d/a/a/a/b;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4173
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 4174
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4176
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 4177
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/d/a/a/a/b;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4179
    :cond_6
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 4180
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/d/a/a/a/b;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4182
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 4183
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/d/a/a/a/b;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4185
    :cond_8
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4186
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4123
    iget-byte v0, p0, Lcom/google/d/a/a/a/b;->l:B

    .line 4124
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 4152
    :goto_0
    return v0

    .line 4125
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 4127
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 4128
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/a/f;->d()Lcom/google/d/a/a/a/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/a/f;

    invoke-virtual {v0}, Lcom/google/d/a/a/a/f;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4129
    iput-byte v2, p0, Lcom/google/d/a/a/a/b;->l:B

    move v0, v2

    .line 4130
    goto :goto_0

    :cond_2
    move v0, v2

    .line 4127
    goto :goto_1

    .line 4133
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 4134
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 4135
    iput-byte v2, p0, Lcom/google/d/a/a/a/b;->l:B

    move v0, v2

    .line 4136
    goto :goto_0

    :cond_4
    move v0, v2

    .line 4133
    goto :goto_2

    .line 4139
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 4140
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 4141
    iput-byte v2, p0, Lcom/google/d/a/a/a/b;->l:B

    move v0, v2

    .line 4142
    goto :goto_0

    :cond_6
    move v0, v2

    .line 4139
    goto :goto_3

    .line 4145
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 4146
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 4147
    iput-byte v2, p0, Lcom/google/d/a/a/a/b;->l:B

    move v0, v2

    .line 4148
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 4145
    goto :goto_4

    .line 4151
    :cond_9
    iput-byte v1, p0, Lcom/google/d/a/a/a/b;->l:B

    move v0, v1

    .line 4152
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 4190
    iget v0, p0, Lcom/google/d/a/a/a/b;->m:I

    .line 4191
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4232
    :goto_0
    return v0

    .line 4194
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_9

    .line 4195
    iget-object v0, p0, Lcom/google/d/a/a/a/b;->b:Lcom/google/n/ao;

    .line 4196
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 4198
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v3, :cond_1

    .line 4199
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/d/a/a/a/b;->c:Lcom/google/n/ao;

    .line 4200
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4202
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_2

    .line 4203
    iget-object v2, p0, Lcom/google/d/a/a/a/b;->d:Lcom/google/n/ao;

    .line 4204
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4206
    :cond_2
    iget v2, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_3

    .line 4207
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/d/a/a/a/b;->e:Lcom/google/n/ao;

    .line 4208
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4210
    :cond_3
    iget v2, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 4211
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/d/a/a/a/b;->f:Lcom/google/n/ao;

    .line 4212
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4214
    :cond_4
    iget v2, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 4215
    iget-object v2, p0, Lcom/google/d/a/a/a/b;->g:Lcom/google/n/ao;

    .line 4216
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4218
    :cond_5
    iget v2, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 4219
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/d/a/a/a/b;->h:Lcom/google/n/ao;

    .line 4220
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4222
    :cond_6
    iget v2, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_7

    .line 4223
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/d/a/a/a/b;->i:Lcom/google/n/ao;

    .line 4224
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4226
    :cond_7
    iget v2, p0, Lcom/google/d/a/a/a/b;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_8

    .line 4227
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/d/a/a/a/b;->j:Lcom/google/n/ao;

    .line 4228
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 4230
    :cond_8
    iget-object v1, p0, Lcom/google/d/a/a/a/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4231
    iput v0, p0, Lcom/google/d/a/a/a/b;->m:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3862
    invoke-static {}, Lcom/google/d/a/a/a/b;->newBuilder()Lcom/google/d/a/a/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/a/d;->a(Lcom/google/d/a/a/a/b;)Lcom/google/d/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3862
    invoke-static {}, Lcom/google/d/a/a/a/b;->newBuilder()Lcom/google/d/a/a/a/d;

    move-result-object v0

    return-object v0
.end method
