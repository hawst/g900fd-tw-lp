.class public final Lcom/google/d/a/a/de;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/dj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/dc;",
        "Lcom/google/d/a/a/de;",
        ">;",
        "Lcom/google/d/a/a/dj;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:I

.field private d:J

.field private e:Z

.field private f:I

.field private g:Z

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 793
    sget-object v0, Lcom/google/d/a/a/dc;->k:Lcom/google/d/a/a/dc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 969
    iput v1, p0, Lcom/google/d/a/a/de;->c:I

    .line 1069
    iput v1, p0, Lcom/google/d/a/a/de;->f:I

    .line 1137
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/de;->h:Lcom/google/n/ao;

    .line 1196
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/de;->i:Lcom/google/n/ao;

    .line 1256
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    .line 794
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/dc;)Lcom/google/d/a/a/de;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 872
    invoke-static {}, Lcom/google/d/a/a/dc;->d()Lcom/google/d/a/a/dc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 910
    :goto_0
    return-object p0

    .line 873
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 874
    iget-boolean v2, p1, Lcom/google/d/a/a/dc;->b:Z

    iget v3, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/de;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/de;->b:Z

    .line 876
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 877
    iget v2, p1, Lcom/google/d/a/a/dc;->c:I

    invoke-static {v2}, Lcom/google/d/a/a/dh;->a(I)Lcom/google/d/a/a/dh;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/d/a/a/dh;->a:Lcom/google/d/a/a/dh;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 873
    goto :goto_1

    :cond_4
    move v2, v1

    .line 876
    goto :goto_2

    .line 877
    :cond_5
    iget v3, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/de;->a:I

    iget v2, v2, Lcom/google/d/a/a/dh;->k:I

    iput v2, p0, Lcom/google/d/a/a/de;->c:I

    .line 879
    :cond_6
    iget v2, p1, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 880
    iget-wide v2, p1, Lcom/google/d/a/a/dc;->d:J

    iget v4, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/de;->a:I

    iput-wide v2, p0, Lcom/google/d/a/a/de;->d:J

    .line 882
    :cond_7
    iget v2, p1, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 883
    iget-boolean v2, p1, Lcom/google/d/a/a/dc;->e:Z

    iget v3, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/de;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/de;->e:Z

    .line 885
    :cond_8
    iget v2, p1, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_e

    .line 886
    iget v2, p1, Lcom/google/d/a/a/dc;->f:I

    invoke-static {v2}, Lcom/google/d/a/a/df;->a(I)Lcom/google/d/a/a/df;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/d/a/a/df;->a:Lcom/google/d/a/a/df;

    :cond_9
    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 879
    goto :goto_3

    :cond_b
    move v2, v1

    .line 882
    goto :goto_4

    :cond_c
    move v2, v1

    .line 885
    goto :goto_5

    .line 886
    :cond_d
    iget v3, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/de;->a:I

    iget v2, v2, Lcom/google/d/a/a/df;->d:I

    iput v2, p0, Lcom/google/d/a/a/de;->f:I

    .line 888
    :cond_e
    iget v2, p1, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_6
    if-eqz v2, :cond_f

    .line 889
    iget-boolean v2, p1, Lcom/google/d/a/a/dc;->g:Z

    iget v3, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/de;->a:I

    iput-boolean v2, p0, Lcom/google/d/a/a/de;->g:Z

    .line 891
    :cond_f
    iget v2, p1, Lcom/google/d/a/a/dc;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_10

    .line 892
    iget-object v2, p0, Lcom/google/d/a/a/de;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 893
    iget v2, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/d/a/a/de;->a:I

    .line 895
    :cond_10
    iget v2, p1, Lcom/google/d/a/a/dc;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    :goto_8
    if-eqz v0, :cond_11

    .line 896
    iget-object v0, p0, Lcom/google/d/a/a/de;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 897
    iget v0, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/de;->a:I

    .line 899
    :cond_11
    iget-object v0, p1, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 900
    iget-object v0, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 901
    iget-object v0, p1, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    .line 902
    iget v0, p0, Lcom/google/d/a/a/de;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/d/a/a/de;->a:I

    .line 909
    :cond_12
    :goto_9
    iget-object v0, p1, Lcom/google/d/a/a/dc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v2, v1

    .line 888
    goto :goto_6

    :cond_14
    move v2, v1

    .line 891
    goto :goto_7

    :cond_15
    move v0, v1

    .line 895
    goto :goto_8

    .line 904
    :cond_16
    iget v0, p0, Lcom/google/d/a/a/de;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_17

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    iget v0, p0, Lcom/google/d/a/a/de;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/d/a/a/de;->a:I

    .line 905
    :cond_17
    iget-object v0, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    iget-object v1, p1, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 785
    new-instance v2, Lcom/google/d/a/a/dc;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/dc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/de;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-boolean v4, p0, Lcom/google/d/a/a/de;->b:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/dc;->b:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/d/a/a/de;->c:I

    iput v4, v2, Lcom/google/d/a/a/dc;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/d/a/a/de;->d:J

    iput-wide v4, v2, Lcom/google/d/a/a/dc;->d:J

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v4, p0, Lcom/google/d/a/a/de;->e:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/dc;->e:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/d/a/a/de;->f:I

    iput v4, v2, Lcom/google/d/a/a/dc;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v4, p0, Lcom/google/d/a/a/de;->g:Z

    iput-boolean v4, v2, Lcom/google/d/a/a/dc;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/d/a/a/dc;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/d/a/a/de;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/d/a/a/de;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v3, v2, Lcom/google/d/a/a/dc;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/d/a/a/de;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/d/a/a/de;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/d/a/a/de;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/d/a/a/de;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/d/a/a/de;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/d/a/a/dc;->j:Ljava/util/List;

    iput v0, v2, Lcom/google/d/a/a/dc;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 785
    check-cast p1, Lcom/google/d/a/a/dc;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/de;->a(Lcom/google/d/a/a/dc;)Lcom/google/d/a/a/de;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 914
    iget v0, p0, Lcom/google/d/a/a/de;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 915
    iget-object v0, p0, Lcom/google/d/a/a/de;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 932
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 914
    goto :goto_0

    .line 920
    :cond_2
    iget v0, p0, Lcom/google/d/a/a/de;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 921
    iget-object v0, p0, Lcom/google/d/a/a/de;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/bu;->d()Lcom/google/d/a/a/bu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/bu;

    invoke-virtual {v0}, Lcom/google/d/a/a/bu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 926
    :goto_3
    iget-object v0, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 927
    iget-object v0, p0, Lcom/google/d/a/a/de;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/nz;->d()Lcom/google/d/a/a/nz;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/nz;

    invoke-virtual {v0}, Lcom/google/d/a/a/nz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 926
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 920
    goto :goto_2

    :cond_5
    move v2, v3

    .line 932
    goto :goto_1
.end method
