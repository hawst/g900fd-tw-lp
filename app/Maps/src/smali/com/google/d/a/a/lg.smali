.class public final Lcom/google/d/a/a/lg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/ll;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/lg;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/d/a/a/lg;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:I

.field h:I

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcom/google/d/a/a/lh;

    invoke-direct {v0}, Lcom/google/d/a/a/lh;-><init>()V

    sput-object v0, Lcom/google/d/a/a/lg;->PARSER:Lcom/google/n/ax;

    .line 444
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/lg;->l:Lcom/google/n/aw;

    .line 926
    new-instance v0, Lcom/google/d/a/a/lg;

    invoke-direct {v0}, Lcom/google/d/a/a/lg;-><init>()V

    sput-object v0, Lcom/google/d/a/a/lg;->i:Lcom/google/d/a/a/lg;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 292
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    .line 308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    .line 354
    iput-byte v4, p0, Lcom/google/d/a/a/lg;->j:B

    .line 403
    iput v4, p0, Lcom/google/d/a/a/lg;->k:I

    .line 90
    iput v2, p0, Lcom/google/d/a/a/lg;->b:I

    .line 91
    iput v2, p0, Lcom/google/d/a/a/lg;->c:I

    .line 92
    iput v3, p0, Lcom/google/d/a/a/lg;->d:I

    .line 93
    iget-object v0, p0, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 94
    iget-object v0, p0, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 95
    iput v2, p0, Lcom/google/d/a/a/lg;->g:I

    .line 96
    iput v2, p0, Lcom/google/d/a/a/lg;->h:I

    .line 97
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 103
    invoke-direct {p0}, Lcom/google/d/a/a/lg;-><init>()V

    .line 104
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 109
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 110
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 111
    sparse-switch v3, :sswitch_data_0

    .line 116
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 118
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 114
    goto :goto_0

    .line 123
    :sswitch_1
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/lg;->a:I

    .line 124
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/lg;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/lg;->au:Lcom/google/n/bn;

    throw v0

    .line 128
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/lg;->a:I

    .line 129
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/lg;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 173
    :catch_1
    move-exception v0

    .line 174
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 175
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 133
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 134
    invoke-static {v3}, Lcom/google/d/a/a/lj;->a(I)Lcom/google/d/a/a/lj;

    move-result-object v4

    .line 135
    if-nez v4, :cond_1

    .line 136
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 138
    :cond_1
    iget v4, p0, Lcom/google/d/a/a/lg;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/d/a/a/lg;->a:I

    .line 139
    iput v3, p0, Lcom/google/d/a/a/lg;->d:I

    goto :goto_0

    .line 144
    :sswitch_4
    iget-object v3, p0, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 145
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/lg;->a:I

    goto :goto_0

    .line 149
    :sswitch_5
    iget-object v3, p0, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 150
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/lg;->a:I

    goto/16 :goto_0

    .line 154
    :sswitch_6
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/lg;->a:I

    .line 155
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/lg;->g:I

    goto/16 :goto_0

    .line 159
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 160
    invoke-static {v3}, Lcom/google/d/a/a/ln;->a(I)Lcom/google/d/a/a/ln;

    move-result-object v4

    .line 161
    if-nez v4, :cond_2

    .line 162
    const/16 v4, 0x8

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 164
    :cond_2
    iget v4, p0, Lcom/google/d/a/a/lg;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/d/a/a/lg;->a:I

    .line 165
    iput v3, p0, Lcom/google/d/a/a/lg;->h:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 177
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/lg;->au:Lcom/google/n/bn;

    .line 178
    return-void

    .line 111
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 87
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 292
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    .line 308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    .line 354
    iput-byte v1, p0, Lcom/google/d/a/a/lg;->j:B

    .line 403
    iput v1, p0, Lcom/google/d/a/a/lg;->k:I

    .line 88
    return-void
.end method

.method public static d()Lcom/google/d/a/a/lg;
    .locals 1

    .prologue
    .line 929
    sget-object v0, Lcom/google/d/a/a/lg;->i:Lcom/google/d/a/a/lg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/li;
    .locals 1

    .prologue
    .line 506
    new-instance v0, Lcom/google/d/a/a/li;

    invoke-direct {v0}, Lcom/google/d/a/a/li;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/lg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    sget-object v0, Lcom/google/d/a/a/lg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 378
    invoke-virtual {p0}, Lcom/google/d/a/a/lg;->c()I

    .line 379
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 380
    iget v0, p0, Lcom/google/d/a/a/lg;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 382
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 383
    iget v0, p0, Lcom/google/d/a/a/lg;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 385
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 386
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/lg;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 388
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 389
    iget-object v0, p0, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 391
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 392
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 394
    :cond_4
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 395
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/d/a/a/lg;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 397
    :cond_5
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 398
    iget v0, p0, Lcom/google/d/a/a/lg;->h:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_b

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 400
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/lg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 401
    return-void

    .line 380
    :cond_7
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 383
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 386
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 395
    :cond_a
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 398
    :cond_b
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 356
    iget-byte v0, p0, Lcom/google/d/a/a/lg;->j:B

    .line 357
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 373
    :goto_0
    return v0

    .line 358
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 360
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 361
    iget-object v0, p0, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/gt;->d()Lcom/google/d/a/a/gt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 362
    iput-byte v2, p0, Lcom/google/d/a/a/lg;->j:B

    move v0, v2

    .line 363
    goto :goto_0

    :cond_2
    move v0, v2

    .line 360
    goto :goto_1

    .line 366
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 367
    iget-object v0, p0, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 368
    iput-byte v2, p0, Lcom/google/d/a/a/lg;->j:B

    move v0, v2

    .line 369
    goto :goto_0

    :cond_4
    move v0, v2

    .line 366
    goto :goto_2

    .line 372
    :cond_5
    iput-byte v1, p0, Lcom/google/d/a/a/lg;->j:B

    move v0, v1

    .line 373
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 405
    iget v0, p0, Lcom/google/d/a/a/lg;->k:I

    .line 406
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 439
    :goto_0
    return v0

    .line 409
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 410
    iget v0, p0, Lcom/google/d/a/a/lg;->b:I

    .line 411
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 413
    :goto_2
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 414
    iget v3, p0, Lcom/google/d/a/a/lg;->c:I

    .line 415
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 417
    :cond_1
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 418
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/d/a/a/lg;->d:I

    .line 419
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 421
    :cond_2
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 422
    iget-object v3, p0, Lcom/google/d/a/a/lg;->e:Lcom/google/n/ao;

    .line 423
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 425
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 426
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/d/a/a/lg;->f:Lcom/google/n/ao;

    .line 427
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 429
    :cond_4
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 430
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/d/a/a/lg;->g:I

    .line 431
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 433
    :cond_5
    iget v3, p0, Lcom/google/d/a/a/lg;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_7

    .line 434
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/d/a/a/lg;->h:I

    .line 435
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_6
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 437
    :cond_7
    iget-object v1, p0, Lcom/google/d/a/a/lg;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    iput v0, p0, Lcom/google/d/a/a/lg;->k:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 411
    goto/16 :goto_1

    :cond_9
    move v3, v1

    .line 415
    goto/16 :goto_3

    :cond_a
    move v3, v1

    .line 419
    goto :goto_4

    :cond_b
    move v3, v1

    .line 431
    goto :goto_5

    :cond_c
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 81
    invoke-static {}, Lcom/google/d/a/a/lg;->newBuilder()Lcom/google/d/a/a/li;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/li;->a(Lcom/google/d/a/a/lg;)Lcom/google/d/a/a/li;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 81
    invoke-static {}, Lcom/google/d/a/a/lg;->newBuilder()Lcom/google/d/a/a/li;

    move-result-object v0

    return-object v0
.end method
