.class public final Lcom/google/d/a/a/kw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/kz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/kw;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/a/a/kw;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:F

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 541
    new-instance v0, Lcom/google/d/a/a/kx;

    invoke-direct {v0}, Lcom/google/d/a/a/kx;-><init>()V

    sput-object v0, Lcom/google/d/a/a/kw;->PARSER:Lcom/google/n/ax;

    .line 664
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/kw;->g:Lcom/google/n/aw;

    .line 980
    new-instance v0, Lcom/google/d/a/a/kw;

    invoke-direct {v0}, Lcom/google/d/a/a/kw;-><init>()V

    sput-object v0, Lcom/google/d/a/a/kw;->d:Lcom/google/d/a/a/kw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 484
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 615
    iput-byte v0, p0, Lcom/google/d/a/a/kw;->e:B

    .line 643
    iput v0, p0, Lcom/google/d/a/a/kw;->f:I

    .line 485
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    .line 486
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/kw;->c:F

    .line 487
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 493
    invoke-direct {p0}, Lcom/google/d/a/a/kw;-><init>()V

    .line 496
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 499
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 500
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 501
    sparse-switch v4, :sswitch_data_0

    .line 506
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 508
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 504
    goto :goto_0

    .line 513
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 514
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    .line 516
    or-int/lit8 v1, v1, 0x1

    .line 518
    :cond_1
    iget-object v4, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 519
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 518
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 529
    :catch_0
    move-exception v0

    .line 530
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 536
    iget-object v1, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    .line 538
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/kw;->au:Lcom/google/n/bn;

    throw v0

    .line 523
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/d/a/a/kw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/d/a/a/kw;->a:I

    .line 524
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/d/a/a/kw;->c:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 531
    :catch_1
    move-exception v0

    .line 532
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 533
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 535
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 536
    iget-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    .line 538
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/kw;->au:Lcom/google/n/bn;

    .line 539
    return-void

    .line 501
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 482
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 615
    iput-byte v0, p0, Lcom/google/d/a/a/kw;->e:B

    .line 643
    iput v0, p0, Lcom/google/d/a/a/kw;->f:I

    .line 483
    return-void
.end method

.method public static d()Lcom/google/d/a/a/kw;
    .locals 1

    .prologue
    .line 983
    sget-object v0, Lcom/google/d/a/a/kw;->d:Lcom/google/d/a/a/kw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/ky;
    .locals 1

    .prologue
    .line 726
    new-instance v0, Lcom/google/d/a/a/ky;

    invoke-direct {v0}, Lcom/google/d/a/a/ky;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/kw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 553
    sget-object v0, Lcom/google/d/a/a/kw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 633
    invoke-virtual {p0}, Lcom/google/d/a/a/kw;->c()I

    .line 634
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 634
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 637
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/kw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 638
    iget v0, p0, Lcom/google/d/a/a/kw;->c:F

    const/4 v1, 0x5

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 640
    :cond_1
    iget-object v0, p0, Lcom/google/d/a/a/kw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 641
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 617
    iget-byte v0, p0, Lcom/google/d/a/a/kw;->e:B

    .line 618
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 628
    :cond_0
    :goto_0
    return v2

    .line 619
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 621
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 622
    iget-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ks;->d()Lcom/google/d/a/a/ks;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ks;

    invoke-virtual {v0}, Lcom/google/d/a/a/ks;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 623
    iput-byte v2, p0, Lcom/google/d/a/a/kw;->e:B

    goto :goto_0

    .line 621
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 627
    :cond_3
    iput-byte v3, p0, Lcom/google/d/a/a/kw;->e:B

    move v2, v3

    .line 628
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 645
    iget v0, p0, Lcom/google/d/a/a/kw;->f:I

    .line 646
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 659
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 649
    :goto_1
    iget-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 650
    iget-object v0, p0, Lcom/google/d/a/a/kw;->b:Ljava/util/List;

    .line 651
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 649
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 653
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/kw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 654
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/d/a/a/kw;->c:F

    .line 655
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 657
    :cond_2
    iget-object v0, p0, Lcom/google/d/a/a/kw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 658
    iput v0, p0, Lcom/google/d/a/a/kw;->f:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 476
    invoke-static {}, Lcom/google/d/a/a/kw;->newBuilder()Lcom/google/d/a/a/ky;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/ky;->a(Lcom/google/d/a/a/kw;)Lcom/google/d/a/a/ky;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 476
    invoke-static {}, Lcom/google/d/a/a/kw;->newBuilder()Lcom/google/d/a/a/ky;

    move-result-object v0

    return-object v0
.end method
