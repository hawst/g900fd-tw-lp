.class public final Lcom/google/d/a/a/bu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/bz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/bu;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/d/a/a/bu;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:D

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/google/d/a/a/bv;

    invoke-direct {v0}, Lcom/google/d/a/a/bv;-><init>()V

    sput-object v0, Lcom/google/d/a/a/bu;->PARSER:Lcom/google/n/ax;

    .line 305
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/bu;->g:Lcom/google/n/aw;

    .line 510
    new-instance v0, Lcom/google/d/a/a/bu;

    invoke-direct {v0}, Lcom/google/d/a/a/bu;-><init>()V

    sput-object v0, Lcom/google/d/a/a/bu;->d:Lcom/google/d/a/a/bu;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 44
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 258
    iput-byte v0, p0, Lcom/google/d/a/a/bu;->e:B

    .line 284
    iput v0, p0, Lcom/google/d/a/a/bu;->f:I

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/d/a/a/bu;->b:D

    .line 46
    const/16 v0, 0x320

    iput v0, p0, Lcom/google/d/a/a/bu;->c:I

    .line 47
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 53
    invoke-direct {p0}, Lcom/google/d/a/a/bu;-><init>()V

    .line 54
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 58
    const/4 v0, 0x0

    .line 59
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 60
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 61
    sparse-switch v3, :sswitch_data_0

    .line 66
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 68
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 64
    goto :goto_0

    .line 73
    :sswitch_1
    iget v3, p0, Lcom/google/d/a/a/bu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/bu;->a:I

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/d/a/a/bu;->b:D
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/bu;->au:Lcom/google/n/bn;

    throw v0

    .line 78
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 79
    invoke-static {v3}, Lcom/google/d/a/a/bx;->a(I)Lcom/google/d/a/a/bx;

    move-result-object v4

    .line 80
    if-nez v4, :cond_1

    .line 81
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 92
    :catch_1
    move-exception v0

    .line 93
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 94
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 83
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/d/a/a/bu;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/d/a/a/bu;->a:I

    .line 84
    iput v3, p0, Lcom/google/d/a/a/bu;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 96
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bu;->au:Lcom/google/n/bn;

    .line 97
    return-void

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 42
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 258
    iput-byte v0, p0, Lcom/google/d/a/a/bu;->e:B

    .line 284
    iput v0, p0, Lcom/google/d/a/a/bu;->f:I

    .line 43
    return-void
.end method

.method public static d()Lcom/google/d/a/a/bu;
    .locals 1

    .prologue
    .line 513
    sget-object v0, Lcom/google/d/a/a/bu;->d:Lcom/google/d/a/a/bu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/bw;
    .locals 1

    .prologue
    .line 367
    new-instance v0, Lcom/google/d/a/a/bw;

    invoke-direct {v0}, Lcom/google/d/a/a/bw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/bu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lcom/google/d/a/a/bu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 274
    invoke-virtual {p0}, Lcom/google/d/a/a/bu;->c()I

    .line 275
    iget v0, p0, Lcom/google/d/a/a/bu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 276
    iget-wide v0, p0, Lcom/google/d/a/a/bu;->b:D

    invoke-static {v2, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 278
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/bu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 279
    iget v0, p0, Lcom/google/d/a/a/bu;->c:I

    const/4 v1, 0x0

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 281
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/bu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 282
    return-void

    .line 279
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 260
    iget-byte v2, p0, Lcom/google/d/a/a/bu;->e:B

    .line 261
    if-ne v2, v0, :cond_0

    .line 269
    :goto_0
    return v0

    .line 262
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 264
    :cond_1
    iget v2, p0, Lcom/google/d/a/a/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 265
    iput-byte v1, p0, Lcom/google/d/a/a/bu;->e:B

    move v0, v1

    .line 266
    goto :goto_0

    :cond_2
    move v2, v1

    .line 264
    goto :goto_1

    .line 268
    :cond_3
    iput-byte v0, p0, Lcom/google/d/a/a/bu;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 286
    iget v0, p0, Lcom/google/d/a/a/bu;->f:I

    .line 287
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 300
    :goto_0
    return v0

    .line 290
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/bu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    .line 291
    iget-wide v2, p0, Lcom/google/d/a/a/bu;->b:D

    .line 292
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 294
    :goto_1
    iget v2, p0, Lcom/google/d/a/a/bu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 295
    iget v2, p0, Lcom/google/d/a/a/bu;->c:I

    .line 296
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 298
    :cond_1
    iget-object v1, p0, Lcom/google/d/a/a/bu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    iput v0, p0, Lcom/google/d/a/a/bu;->f:I

    goto :goto_0

    .line 296
    :cond_2
    const/16 v1, 0xa

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/d/a/a/bu;->newBuilder()Lcom/google/d/a/a/bw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/bw;->a(Lcom/google/d/a/a/bu;)Lcom/google/d/a/a/bw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/d/a/a/bu;->newBuilder()Lcom/google/d/a/a/bw;

    move-result-object v0

    return-object v0
.end method
