.class public final enum Lcom/google/d/a/a/be;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/be;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/be;

.field public static final enum b:Lcom/google/d/a/a/be;

.field public static final enum c:Lcom/google/d/a/a/be;

.field public static final enum d:Lcom/google/d/a/a/be;

.field public static final enum e:Lcom/google/d/a/a/be;

.field public static final enum f:Lcom/google/d/a/a/be;

.field public static final enum g:Lcom/google/d/a/a/be;

.field private static final synthetic i:[Lcom/google/d/a/a/be;


# instance fields
.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 210
    new-instance v0, Lcom/google/d/a/a/be;

    const-string v1, "STRUCTURE_ANY"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/d/a/a/be;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/be;->a:Lcom/google/d/a/a/be;

    .line 214
    new-instance v0, Lcom/google/d/a/a/be;

    const-string v1, "STRUCTURE_TOWER"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/d/a/a/be;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/be;->b:Lcom/google/d/a/a/be;

    .line 218
    new-instance v0, Lcom/google/d/a/a/be;

    const-string v1, "STRUCTURE_DOME"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/d/a/a/be;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/be;->c:Lcom/google/d/a/a/be;

    .line 222
    new-instance v0, Lcom/google/d/a/a/be;

    const-string v1, "STRUCTURE_CASTLE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/d/a/a/be;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/be;->d:Lcom/google/d/a/a/be;

    .line 226
    new-instance v0, Lcom/google/d/a/a/be;

    const-string v1, "STRUCTURE_SHRINE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/d/a/a/be;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/be;->e:Lcom/google/d/a/a/be;

    .line 230
    new-instance v0, Lcom/google/d/a/a/be;

    const-string v1, "STRUCTURE_TEMPLE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/be;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/be;->f:Lcom/google/d/a/a/be;

    .line 234
    new-instance v0, Lcom/google/d/a/a/be;

    const-string v1, "STRUCTURE_TANK"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/be;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/be;->g:Lcom/google/d/a/a/be;

    .line 205
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/d/a/a/be;

    sget-object v1, Lcom/google/d/a/a/be;->a:Lcom/google/d/a/a/be;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/be;->b:Lcom/google/d/a/a/be;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/be;->c:Lcom/google/d/a/a/be;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/be;->d:Lcom/google/d/a/a/be;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/be;->e:Lcom/google/d/a/a/be;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/be;->f:Lcom/google/d/a/a/be;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/be;->g:Lcom/google/d/a/a/be;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/be;->i:[Lcom/google/d/a/a/be;

    .line 289
    new-instance v0, Lcom/google/d/a/a/bf;

    invoke-direct {v0}, Lcom/google/d/a/a/bf;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 298
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 299
    iput p3, p0, Lcom/google/d/a/a/be;->h:I

    .line 300
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/be;
    .locals 1

    .prologue
    .line 272
    packed-switch p0, :pswitch_data_0

    .line 280
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 273
    :pswitch_0
    sget-object v0, Lcom/google/d/a/a/be;->a:Lcom/google/d/a/a/be;

    goto :goto_0

    .line 274
    :pswitch_1
    sget-object v0, Lcom/google/d/a/a/be;->b:Lcom/google/d/a/a/be;

    goto :goto_0

    .line 275
    :pswitch_2
    sget-object v0, Lcom/google/d/a/a/be;->c:Lcom/google/d/a/a/be;

    goto :goto_0

    .line 276
    :pswitch_3
    sget-object v0, Lcom/google/d/a/a/be;->d:Lcom/google/d/a/a/be;

    goto :goto_0

    .line 277
    :pswitch_4
    sget-object v0, Lcom/google/d/a/a/be;->e:Lcom/google/d/a/a/be;

    goto :goto_0

    .line 278
    :pswitch_5
    sget-object v0, Lcom/google/d/a/a/be;->f:Lcom/google/d/a/a/be;

    goto :goto_0

    .line 279
    :pswitch_6
    sget-object v0, Lcom/google/d/a/a/be;->g:Lcom/google/d/a/a/be;

    goto :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/be;
    .locals 1

    .prologue
    .line 205
    const-class v0, Lcom/google/d/a/a/be;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/be;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/be;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/google/d/a/a/be;->i:[Lcom/google/d/a/a/be;

    invoke-virtual {v0}, [Lcom/google/d/a/a/be;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/be;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/google/d/a/a/be;->h:I

    return v0
.end method
