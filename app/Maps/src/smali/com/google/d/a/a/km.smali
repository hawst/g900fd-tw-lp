.class public final enum Lcom/google/d/a/a/km;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/d/a/a/km;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/d/a/a/km;

.field public static final enum b:Lcom/google/d/a/a/km;

.field public static final enum c:Lcom/google/d/a/a/km;

.field public static final enum d:Lcom/google/d/a/a/km;

.field public static final enum e:Lcom/google/d/a/a/km;

.field public static final enum f:Lcom/google/d/a/a/km;

.field public static final enum g:Lcom/google/d/a/a/km;

.field public static final enum h:Lcom/google/d/a/a/km;

.field public static final enum i:Lcom/google/d/a/a/km;

.field public static final enum j:Lcom/google/d/a/a/km;

.field public static final enum k:Lcom/google/d/a/a/km;

.field public static final enum l:Lcom/google/d/a/a/km;

.field public static final enum m:Lcom/google/d/a/a/km;

.field public static final enum n:Lcom/google/d/a/a/km;

.field public static final enum o:Lcom/google/d/a/a/km;

.field public static final enum p:Lcom/google/d/a/a/km;

.field private static final synthetic r:[Lcom/google/d/a/a/km;


# instance fields
.field final q:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 266
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_TRAVEL_RESTRICTED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->a:Lcom/google/d/a/a/km;

    .line 270
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_ILLEGAL"

    const/16 v2, 0x111

    invoke-direct {v0, v1, v5, v2}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->b:Lcom/google/d/a/a/km;

    .line 274
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_PHYSICAL"

    const/16 v2, 0x112

    invoke-direct {v0, v1, v6, v2}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->c:Lcom/google/d/a/a/km;

    .line 278
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_LOGICAL"

    const/16 v2, 0x113

    invoke-direct {v0, v1, v7, v2}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->d:Lcom/google/d/a/a/km;

    .line 282
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_GATE"

    const/16 v2, 0x114

    invoke-direct {v0, v1, v8, v2}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->e:Lcom/google/d/a/a/km;

    .line 286
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_CONSTRUCTION"

    const/4 v2, 0x5

    const/16 v3, 0x115

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->f:Lcom/google/d/a/a/km;

    .line 290
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_SEASONAL_CLOSURE"

    const/4 v2, 0x6

    const/16 v3, 0x116

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->g:Lcom/google/d/a/a/km;

    .line 294
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_PRIVATE"

    const/4 v2, 0x7

    const/16 v3, 0x117

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->h:Lcom/google/d/a/a/km;

    .line 298
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_WRONG_WAY"

    const/16 v2, 0x8

    const/16 v3, 0x118

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->i:Lcom/google/d/a/a/km;

    .line 302
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_PAYMENT_REQUIRED"

    const/16 v2, 0x9

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->j:Lcom/google/d/a/a/km;

    .line 306
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_TOLL_BOOTH"

    const/16 v2, 0xa

    const/16 v3, 0x121

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->k:Lcom/google/d/a/a/km;

    .line 310
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_USAGE_FEE_REQUIRED"

    const/16 v2, 0xb

    const/16 v3, 0x122

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->l:Lcom/google/d/a/a/km;

    .line 314
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_ENTRANCE_FEE_REQUIRED"

    const/16 v2, 0xc

    const/16 v3, 0x123

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->m:Lcom/google/d/a/a/km;

    .line 318
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_ADVISORY"

    const/16 v2, 0xd

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->n:Lcom/google/d/a/a/km;

    .line 322
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_HIGH_CRIME"

    const/16 v2, 0xe

    const/16 v3, 0x131

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->o:Lcom/google/d/a/a/km;

    .line 326
    new-instance v0, Lcom/google/d/a/a/km;

    const-string v1, "RESTRICTION_POLITICALLY_SENSITIVE"

    const/16 v2, 0xf

    const/16 v3, 0x132

    invoke-direct {v0, v1, v2, v3}, Lcom/google/d/a/a/km;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/d/a/a/km;->p:Lcom/google/d/a/a/km;

    .line 261
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/d/a/a/km;

    sget-object v1, Lcom/google/d/a/a/km;->a:Lcom/google/d/a/a/km;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/d/a/a/km;->b:Lcom/google/d/a/a/km;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/d/a/a/km;->c:Lcom/google/d/a/a/km;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/d/a/a/km;->d:Lcom/google/d/a/a/km;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/d/a/a/km;->e:Lcom/google/d/a/a/km;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/d/a/a/km;->f:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/d/a/a/km;->g:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/d/a/a/km;->h:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/d/a/a/km;->i:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/d/a/a/km;->j:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/d/a/a/km;->k:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/d/a/a/km;->l:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/d/a/a/km;->m:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/d/a/a/km;->n:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/d/a/a/km;->o:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/d/a/a/km;->p:Lcom/google/d/a/a/km;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/d/a/a/km;->r:[Lcom/google/d/a/a/km;

    .line 426
    new-instance v0, Lcom/google/d/a/a/kn;

    invoke-direct {v0}, Lcom/google/d/a/a/kn;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 435
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 436
    iput p3, p0, Lcom/google/d/a/a/km;->q:I

    .line 437
    return-void
.end method

.method public static a(I)Lcom/google/d/a/a/km;
    .locals 1

    .prologue
    .line 400
    sparse-switch p0, :sswitch_data_0

    .line 417
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 401
    :sswitch_0
    sget-object v0, Lcom/google/d/a/a/km;->a:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 402
    :sswitch_1
    sget-object v0, Lcom/google/d/a/a/km;->b:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 403
    :sswitch_2
    sget-object v0, Lcom/google/d/a/a/km;->c:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 404
    :sswitch_3
    sget-object v0, Lcom/google/d/a/a/km;->d:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 405
    :sswitch_4
    sget-object v0, Lcom/google/d/a/a/km;->e:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 406
    :sswitch_5
    sget-object v0, Lcom/google/d/a/a/km;->f:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 407
    :sswitch_6
    sget-object v0, Lcom/google/d/a/a/km;->g:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 408
    :sswitch_7
    sget-object v0, Lcom/google/d/a/a/km;->h:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 409
    :sswitch_8
    sget-object v0, Lcom/google/d/a/a/km;->i:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 410
    :sswitch_9
    sget-object v0, Lcom/google/d/a/a/km;->j:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 411
    :sswitch_a
    sget-object v0, Lcom/google/d/a/a/km;->k:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 412
    :sswitch_b
    sget-object v0, Lcom/google/d/a/a/km;->l:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 413
    :sswitch_c
    sget-object v0, Lcom/google/d/a/a/km;->m:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 414
    :sswitch_d
    sget-object v0, Lcom/google/d/a/a/km;->n:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 415
    :sswitch_e
    sget-object v0, Lcom/google/d/a/a/km;->o:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 416
    :sswitch_f
    sget-object v0, Lcom/google/d/a/a/km;->p:Lcom/google/d/a/a/km;

    goto :goto_0

    .line 400
    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_9
        0x13 -> :sswitch_d
        0x111 -> :sswitch_1
        0x112 -> :sswitch_2
        0x113 -> :sswitch_3
        0x114 -> :sswitch_4
        0x115 -> :sswitch_5
        0x116 -> :sswitch_6
        0x117 -> :sswitch_7
        0x118 -> :sswitch_8
        0x121 -> :sswitch_a
        0x122 -> :sswitch_b
        0x123 -> :sswitch_c
        0x131 -> :sswitch_e
        0x132 -> :sswitch_f
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/d/a/a/km;
    .locals 1

    .prologue
    .line 261
    const-class v0, Lcom/google/d/a/a/km;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/km;

    return-object v0
.end method

.method public static values()[Lcom/google/d/a/a/km;
    .locals 1

    .prologue
    .line 261
    sget-object v0, Lcom/google/d/a/a/km;->r:[Lcom/google/d/a/a/km;

    invoke-virtual {v0}, [Lcom/google/d/a/a/km;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/d/a/a/km;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 396
    iget v0, p0, Lcom/google/d/a/a/km;->q:I

    return v0
.end method
