.class public final Lcom/google/d/a/a/oz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/pi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/d/a/a/ox;",
        "Lcom/google/d/a/a/oz;",
        ">;",
        "Lcom/google/d/a/a/pi;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 936
    sget-object v0, Lcom/google/d/a/a/ox;->k:Lcom/google/d/a/a/ox;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1176
    iput v1, p0, Lcom/google/d/a/a/oz;->f:I

    .line 1244
    iput v1, p0, Lcom/google/d/a/a/oz;->h:I

    .line 1280
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/oz;->i:I

    .line 937
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/d/a/a/ox;)Lcom/google/d/a/a/oz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1010
    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1039
    :goto_0
    return-object p0

    .line 1011
    :cond_0
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1012
    iget v2, p1, Lcom/google/d/a/a/ox;->b:I

    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/oz;->a:I

    iput v2, p0, Lcom/google/d/a/a/oz;->d:I

    .line 1014
    :cond_1
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1015
    iget v2, p1, Lcom/google/d/a/a/ox;->c:I

    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/oz;->a:I

    iput v2, p0, Lcom/google/d/a/a/oz;->b:I

    .line 1017
    :cond_2
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1018
    iget v2, p1, Lcom/google/d/a/a/ox;->d:I

    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/oz;->a:I

    iput v2, p0, Lcom/google/d/a/a/oz;->c:I

    .line 1020
    :cond_3
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1021
    iget v2, p1, Lcom/google/d/a/a/ox;->e:I

    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/oz;->a:I

    iput v2, p0, Lcom/google/d/a/a/oz;->e:I

    .line 1023
    :cond_4
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 1024
    iget v2, p1, Lcom/google/d/a/a/ox;->f:I

    invoke-static {v2}, Lcom/google/d/a/a/pc;->a(I)Lcom/google/d/a/a/pc;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/d/a/a/pc;->a:Lcom/google/d/a/a/pc;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 1011
    goto :goto_1

    :cond_7
    move v2, v1

    .line 1014
    goto :goto_2

    :cond_8
    move v2, v1

    .line 1017
    goto :goto_3

    :cond_9
    move v2, v1

    .line 1020
    goto :goto_4

    :cond_a
    move v2, v1

    .line 1023
    goto :goto_5

    .line 1024
    :cond_b
    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/d/a/a/oz;->a:I

    iget v2, v2, Lcom/google/d/a/a/pc;->d:I

    iput v2, p0, Lcom/google/d/a/a/oz;->f:I

    .line 1026
    :cond_c
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 1027
    iget v2, p1, Lcom/google/d/a/a/ox;->g:I

    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/oz;->a:I

    iput v2, p0, Lcom/google/d/a/a/oz;->g:I

    .line 1029
    :cond_d
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_12

    .line 1030
    iget v2, p1, Lcom/google/d/a/a/ox;->h:I

    invoke-static {v2}, Lcom/google/d/a/a/pg;->a(I)Lcom/google/d/a/a/pg;

    move-result-object v2

    if-nez v2, :cond_e

    sget-object v2, Lcom/google/d/a/a/pg;->a:Lcom/google/d/a/a/pg;

    :cond_e
    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    move v2, v1

    .line 1026
    goto :goto_6

    :cond_10
    move v2, v1

    .line 1029
    goto :goto_7

    .line 1030
    :cond_11
    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/d/a/a/oz;->a:I

    iget v2, v2, Lcom/google/d/a/a/pg;->c:I

    iput v2, p0, Lcom/google/d/a/a/oz;->h:I

    .line 1032
    :cond_12
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_8
    if-eqz v2, :cond_16

    .line 1033
    iget v2, p1, Lcom/google/d/a/a/ox;->i:I

    invoke-static {v2}, Lcom/google/d/a/a/pe;->a(I)Lcom/google/d/a/a/pe;

    move-result-object v2

    if-nez v2, :cond_13

    sget-object v2, Lcom/google/d/a/a/pe;->a:Lcom/google/d/a/a/pe;

    :cond_13
    if-nez v2, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    move v2, v1

    .line 1032
    goto :goto_8

    .line 1033
    :cond_15
    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/d/a/a/oz;->a:I

    iget v2, v2, Lcom/google/d/a/a/pe;->n:I

    iput v2, p0, Lcom/google/d/a/a/oz;->i:I

    .line 1035
    :cond_16
    iget v2, p1, Lcom/google/d/a/a/ox;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    :goto_9
    if-eqz v0, :cond_17

    .line 1036
    iget v0, p1, Lcom/google/d/a/a/ox;->j:I

    iget v1, p0, Lcom/google/d/a/a/oz;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/d/a/a/oz;->a:I

    iput v0, p0, Lcom/google/d/a/a/oz;->j:I

    .line 1038
    :cond_17
    iget-object v0, p1, Lcom/google/d/a/a/ox;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_18
    move v0, v1

    .line 1035
    goto :goto_9
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 928
    new-instance v2, Lcom/google/d/a/a/ox;

    invoke-direct {v2, p0}, Lcom/google/d/a/a/ox;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/d/a/a/oz;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget v1, p0, Lcom/google/d/a/a/oz;->d:I

    iput v1, v2, Lcom/google/d/a/a/ox;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/d/a/a/oz;->b:I

    iput v1, v2, Lcom/google/d/a/a/ox;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/d/a/a/oz;->c:I

    iput v1, v2, Lcom/google/d/a/a/ox;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/d/a/a/oz;->e:I

    iput v1, v2, Lcom/google/d/a/a/ox;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/d/a/a/oz;->f:I

    iput v1, v2, Lcom/google/d/a/a/ox;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/d/a/a/oz;->g:I

    iput v1, v2, Lcom/google/d/a/a/ox;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/d/a/a/oz;->h:I

    iput v1, v2, Lcom/google/d/a/a/ox;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/d/a/a/oz;->i:I

    iput v1, v2, Lcom/google/d/a/a/ox;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v1, p0, Lcom/google/d/a/a/oz;->j:I

    iput v1, v2, Lcom/google/d/a/a/ox;->j:I

    iput v0, v2, Lcom/google/d/a/a/ox;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 928
    check-cast p1, Lcom/google/d/a/a/ox;

    invoke-virtual {p0, p1}, Lcom/google/d/a/a/oz;->a(Lcom/google/d/a/a/ox;)Lcom/google/d/a/a/oz;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1043
    const/4 v0, 0x1

    return v0
.end method
