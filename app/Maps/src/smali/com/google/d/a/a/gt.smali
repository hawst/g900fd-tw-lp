.class public final Lcom/google/d/a/a/gt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/gz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/gt;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/d/a/a/gt;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field e:I

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 263
    new-instance v0, Lcom/google/d/a/a/gu;

    invoke-direct {v0}, Lcom/google/d/a/a/gu;-><init>()V

    sput-object v0, Lcom/google/d/a/a/gt;->PARSER:Lcom/google/n/ax;

    .line 739
    new-instance v0, Lcom/google/d/a/a/gv;

    invoke-direct {v0}, Lcom/google/d/a/a/gv;-><init>()V

    .line 1022
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/gt;->n:Lcom/google/n/aw;

    .line 1844
    new-instance v0, Lcom/google/d/a/a/gt;

    invoke-direct {v0}, Lcom/google/d/a/a/gt;-><init>()V

    sput-object v0, Lcom/google/d/a/a/gt;->k:Lcom/google/d/a/a/gt;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 868
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gt;->h:Lcom/google/n/ao;

    .line 884
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gt;->i:Lcom/google/n/ao;

    .line 900
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    .line 915
    iput-byte v3, p0, Lcom/google/d/a/a/gt;->l:B

    .line 968
    iput v3, p0, Lcom/google/d/a/a/gt;->m:I

    .line 132
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gt;->b:Ljava/lang/Object;

    .line 133
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gt;->c:Ljava/lang/Object;

    .line 134
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/gt;->e:I

    .line 136
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gt;->f:Ljava/lang/Object;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/d/a/a/gt;->g:Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lcom/google/d/a/a/gt;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 139
    iget-object v0, p0, Lcom/google/d/a/a/gt;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 140
    iget-object v0, p0, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 141
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x4

    const/4 v0, 0x0

    .line 147
    invoke-direct {p0}, Lcom/google/d/a/a/gt;-><init>()V

    .line 150
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    move v1, v0

    .line 153
    :cond_0
    :goto_0
    if-nez v2, :cond_9

    .line 154
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 155
    sparse-switch v0, :sswitch_data_0

    .line 160
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 162
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 158
    goto :goto_0

    .line 167
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 168
    iget v5, p0, Lcom/google/d/a/a/gt;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/d/a/a/gt;->a:I

    .line 169
    iput-object v0, p0, Lcom/google/d/a/a/gt;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 251
    :catch_0
    move-exception v0

    .line 252
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_1

    .line 258
    iget-object v1, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    .line 260
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/gt;->au:Lcom/google/n/bn;

    throw v0

    .line 173
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 174
    iget v5, p0, Lcom/google/d/a/a/gt;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/d/a/a/gt;->a:I

    .line 175
    iput-object v0, p0, Lcom/google/d/a/a/gt;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 253
    :catch_1
    move-exception v0

    .line 254
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 255
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 179
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 180
    invoke-static {v0}, Lcom/google/d/a/a/gx;->a(I)Lcom/google/d/a/a/gx;

    move-result-object v5

    .line 181
    if-nez v5, :cond_2

    .line 182
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 184
    :cond_2
    and-int/lit8 v5, v1, 0x4

    if-eq v5, v7, :cond_3

    .line 185
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    .line 186
    or-int/lit8 v1, v1, 0x4

    .line 188
    :cond_3
    iget-object v5, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 193
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 194
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 195
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_4

    const/4 v0, -0x1

    :goto_2
    if-lez v0, :cond_7

    .line 196
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 197
    invoke-static {v0}, Lcom/google/d/a/a/gx;->a(I)Lcom/google/d/a/a/gx;

    move-result-object v6

    .line 198
    if-nez v6, :cond_5

    .line 199
    const/4 v6, 0x3

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 195
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_2

    .line 201
    :cond_5
    and-int/lit8 v6, v1, 0x4

    if-eq v6, v7, :cond_6

    .line 202
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    .line 203
    or-int/lit8 v1, v1, 0x4

    .line 205
    :cond_6
    iget-object v6, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 208
    :cond_7
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 212
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 213
    invoke-static {v0}, Lcom/google/d/a/a/ln;->a(I)Lcom/google/d/a/a/ln;

    move-result-object v5

    .line 214
    if-nez v5, :cond_8

    .line 215
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 217
    :cond_8
    iget v5, p0, Lcom/google/d/a/a/gt;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/d/a/a/gt;->a:I

    .line 218
    iput v0, p0, Lcom/google/d/a/a/gt;->e:I

    goto/16 :goto_0

    .line 223
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 224
    iget v5, p0, Lcom/google/d/a/a/gt;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/d/a/a/gt;->a:I

    .line 225
    iput-object v0, p0, Lcom/google/d/a/a/gt;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 229
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 230
    iget v5, p0, Lcom/google/d/a/a/gt;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/d/a/a/gt;->a:I

    .line 231
    iput-object v0, p0, Lcom/google/d/a/a/gt;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 235
    :sswitch_8
    iget-object v0, p0, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 236
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/d/a/a/gt;->a:I

    goto/16 :goto_0

    .line 240
    :sswitch_9
    iget-object v0, p0, Lcom/google/d/a/a/gt;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 241
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/d/a/a/gt;->a:I

    goto/16 :goto_0

    .line 245
    :sswitch_a
    iget-object v0, p0, Lcom/google/d/a/a/gt;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 246
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/d/a/a/gt;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 257
    :cond_9
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v7, :cond_a

    .line 258
    iget-object v0, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    .line 260
    :cond_a
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->au:Lcom/google/n/bn;

    .line 261
    return-void

    .line 155
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x7a -> :sswitch_8
        0x322 -> :sswitch_9
        0xfa2 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 129
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 868
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gt;->h:Lcom/google/n/ao;

    .line 884
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gt;->i:Lcom/google/n/ao;

    .line 900
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    .line 915
    iput-byte v1, p0, Lcom/google/d/a/a/gt;->l:B

    .line 968
    iput v1, p0, Lcom/google/d/a/a/gt;->m:I

    .line 130
    return-void
.end method

.method public static d()Lcom/google/d/a/a/gt;
    .locals 1

    .prologue
    .line 1847
    sget-object v0, Lcom/google/d/a/a/gt;->k:Lcom/google/d/a/a/gt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/gw;
    .locals 1

    .prologue
    .line 1084
    new-instance v0, Lcom/google/d/a/a/gw;

    invoke-direct {v0}, Lcom/google/d/a/a/gw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/gt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    sget-object v0, Lcom/google/d/a/a/gt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 937
    invoke-virtual {p0}, Lcom/google/d/a/a/gt;->c()I

    .line 938
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 939
    iget-object v0, p0, Lcom/google/d/a/a/gt;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 941
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 942
    iget-object v0, p0, Lcom/google/d/a/a/gt;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 944
    :goto_2
    iget-object v0, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 945
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 944
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 939
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 942
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 945
    :cond_4
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 947
    :cond_5
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_6

    .line 948
    iget v0, p0, Lcom/google/d/a/a/gt;->e:I

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_c

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 950
    :cond_6
    :goto_4
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 951
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/d/a/a/gt;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 953
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 954
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/d/a/a/gt;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 956
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 957
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 959
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 960
    const/16 v0, 0x64

    iget-object v1, p0, Lcom/google/d/a/a/gt;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 962
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 963
    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/google/d/a/a/gt;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 965
    :cond_b
    iget-object v0, p0, Lcom/google/d/a/a/gt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 966
    return-void

    .line 948
    :cond_c
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 951
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 954
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 917
    iget-byte v0, p0, Lcom/google/d/a/a/gt;->l:B

    .line 918
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 932
    :goto_0
    return v0

    .line 919
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 921
    :cond_1
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 922
    iput-byte v2, p0, Lcom/google/d/a/a/gt;->l:B

    move v0, v2

    .line 923
    goto :goto_0

    :cond_2
    move v0, v2

    .line 921
    goto :goto_1

    .line 925
    :cond_3
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 926
    iget-object v0, p0, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 927
    iput-byte v2, p0, Lcom/google/d/a/a/gt;->l:B

    move v0, v2

    .line 928
    goto :goto_0

    :cond_4
    move v0, v2

    .line 925
    goto :goto_2

    .line 931
    :cond_5
    iput-byte v1, p0, Lcom/google/d/a/a/gt;->l:B

    move v0, v1

    .line 932
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v5, 0xa

    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 970
    iget v0, p0, Lcom/google/d/a/a/gt;->m:I

    .line 971
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1017
    :goto_0
    return v0

    .line 974
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 976
    iget-object v0, p0, Lcom/google/d/a/a/gt;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 978
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 980
    iget-object v0, p0, Lcom/google/d/a/a/gt;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v3, v2

    move v4, v2

    .line 984
    :goto_4
    iget-object v0, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 985
    iget-object v0, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    .line 986
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v4, v0

    .line 984
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 976
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 980
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v0, v5

    .line 986
    goto :goto_5

    .line 988
    :cond_5
    add-int v0, v1, v4

    .line 989
    iget-object v1, p0, Lcom/google/d/a/a/gt;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 991
    iget v1, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_e

    .line 992
    iget v1, p0, Lcom/google/d/a/a/gt;->e:I

    .line 993
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v1, :cond_6

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v5

    :cond_6
    add-int v1, v3, v5

    add-int/2addr v0, v1

    move v1, v0

    .line 995
    :goto_6
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_7

    .line 996
    const/4 v3, 0x5

    .line 997
    iget-object v0, p0, Lcom/google/d/a/a/gt;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->f:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 999
    :cond_7
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_8

    .line 1000
    const/4 v3, 0x6

    .line 1001
    iget-object v0, p0, Lcom/google/d/a/a/gt;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/gt;->g:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1003
    :cond_8
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_9

    .line 1004
    const/16 v0, 0xf

    iget-object v3, p0, Lcom/google/d/a/a/gt;->j:Lcom/google/n/ao;

    .line 1005
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1007
    :cond_9
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_a

    .line 1008
    const/16 v0, 0x64

    iget-object v3, p0, Lcom/google/d/a/a/gt;->h:Lcom/google/n/ao;

    .line 1009
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1011
    :cond_a
    iget v0, p0, Lcom/google/d/a/a/gt;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_b

    .line 1012
    const/16 v0, 0x1f4

    iget-object v3, p0, Lcom/google/d/a/a/gt;->i:Lcom/google/n/ao;

    .line 1013
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1015
    :cond_b
    iget-object v0, p0, Lcom/google/d/a/a/gt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1016
    iput v0, p0, Lcom/google/d/a/a/gt;->m:I

    goto/16 :goto_0

    .line 997
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 1001
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_e
    move v1, v0

    goto/16 :goto_6

    :cond_f
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 123
    invoke-static {}, Lcom/google/d/a/a/gt;->newBuilder()Lcom/google/d/a/a/gw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/gw;->a(Lcom/google/d/a/a/gt;)Lcom/google/d/a/a/gw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 123
    invoke-static {}, Lcom/google/d/a/a/gt;->newBuilder()Lcom/google/d/a/a/gw;

    move-result-object v0

    return-object v0
.end method
