.class public final Lcom/google/d/a/a/ox;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/pi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ox;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/d/a/a/ox;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field g:I

.field h:I

.field i:I

.field j:I

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216
    new-instance v0, Lcom/google/d/a/a/oy;

    invoke-direct {v0}, Lcom/google/d/a/a/oy;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ox;->PARSER:Lcom/google/n/ax;

    .line 856
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/ox;->n:Lcom/google/n/aw;

    .line 1352
    new-instance v0, Lcom/google/d/a/a/ox;

    invoke-direct {v0}, Lcom/google/d/a/a/ox;-><init>()V

    sput-object v0, Lcom/google/d/a/a/ox;->k:Lcom/google/d/a/a/ox;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 764
    iput-byte v0, p0, Lcom/google/d/a/a/ox;->l:B

    .line 807
    iput v0, p0, Lcom/google/d/a/a/ox;->m:I

    .line 108
    iput v1, p0, Lcom/google/d/a/a/ox;->b:I

    .line 109
    iput v1, p0, Lcom/google/d/a/a/ox;->c:I

    .line 110
    iput v1, p0, Lcom/google/d/a/a/ox;->d:I

    .line 111
    iput v1, p0, Lcom/google/d/a/a/ox;->e:I

    .line 112
    iput v1, p0, Lcom/google/d/a/a/ox;->f:I

    .line 113
    iput v1, p0, Lcom/google/d/a/a/ox;->g:I

    .line 114
    iput v1, p0, Lcom/google/d/a/a/ox;->h:I

    .line 115
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/d/a/a/ox;->i:I

    .line 116
    iput v1, p0, Lcom/google/d/a/a/ox;->j:I

    .line 117
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 123
    invoke-direct {p0}, Lcom/google/d/a/a/ox;-><init>()V

    .line 124
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 128
    const/4 v0, 0x0

    .line 129
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 130
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 131
    sparse-switch v3, :sswitch_data_0

    .line 136
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 138
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 134
    goto :goto_0

    .line 143
    :sswitch_1
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/d/a/a/ox;->a:I

    .line 144
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/ox;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .line 208
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/ox;->au:Lcom/google/n/bn;

    throw v0

    .line 148
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/d/a/a/ox;->a:I

    .line 149
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/ox;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 209
    :catch_1
    move-exception v0

    .line 210
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 211
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 153
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/d/a/a/ox;->a:I

    .line 154
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/ox;->d:I

    goto :goto_0

    .line 158
    :sswitch_4
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/d/a/a/ox;->a:I

    .line 159
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/ox;->e:I

    goto :goto_0

    .line 163
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 164
    invoke-static {v3}, Lcom/google/d/a/a/pc;->a(I)Lcom/google/d/a/a/pc;

    move-result-object v4

    .line 165
    if-nez v4, :cond_1

    .line 166
    const/4 v4, 0x5

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 168
    :cond_1
    iget v4, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/d/a/a/ox;->a:I

    .line 169
    iput v3, p0, Lcom/google/d/a/a/ox;->f:I

    goto :goto_0

    .line 174
    :sswitch_6
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/d/a/a/ox;->a:I

    .line 175
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/ox;->g:I

    goto/16 :goto_0

    .line 179
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 180
    invoke-static {v3}, Lcom/google/d/a/a/pg;->a(I)Lcom/google/d/a/a/pg;

    move-result-object v4

    .line 181
    if-nez v4, :cond_2

    .line 182
    const/4 v4, 0x7

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 184
    :cond_2
    iget v4, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/d/a/a/ox;->a:I

    .line 185
    iput v3, p0, Lcom/google/d/a/a/ox;->h:I

    goto/16 :goto_0

    .line 190
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 191
    invoke-static {v3}, Lcom/google/d/a/a/pe;->a(I)Lcom/google/d/a/a/pe;

    move-result-object v4

    .line 192
    if-nez v4, :cond_3

    .line 193
    const/16 v4, 0x8

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 195
    :cond_3
    iget v4, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/d/a/a/ox;->a:I

    .line 196
    iput v3, p0, Lcom/google/d/a/a/ox;->i:I

    goto/16 :goto_0

    .line 201
    :sswitch_9
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/d/a/a/ox;->a:I

    .line 202
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/d/a/a/ox;->j:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 213
    :cond_4
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/ox;->au:Lcom/google/n/bn;

    .line 214
    return-void

    .line 131
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 105
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 764
    iput-byte v0, p0, Lcom/google/d/a/a/ox;->l:B

    .line 807
    iput v0, p0, Lcom/google/d/a/a/ox;->m:I

    .line 106
    return-void
.end method

.method public static d()Lcom/google/d/a/a/ox;
    .locals 1

    .prologue
    .line 1355
    sget-object v0, Lcom/google/d/a/a/ox;->k:Lcom/google/d/a/a/ox;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/oz;
    .locals 1

    .prologue
    .line 918
    new-instance v0, Lcom/google/d/a/a/oz;

    invoke-direct {v0}, Lcom/google/d/a/a/oz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/ox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    sget-object v0, Lcom/google/d/a/a/ox;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 776
    invoke-virtual {p0}, Lcom/google/d/a/a/ox;->c()I

    .line 777
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 778
    iget v0, p0, Lcom/google/d/a/a/ox;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_9

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 780
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 781
    iget v0, p0, Lcom/google/d/a/a/ox;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_a

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 783
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 784
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/d/a/a/ox;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 786
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 787
    iget v0, p0, Lcom/google/d/a/a/ox;->e:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_c

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 789
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 790
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/d/a/a/ox;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_d

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 792
    :cond_4
    :goto_4
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 793
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/d/a/a/ox;->g:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_e

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 795
    :cond_5
    :goto_5
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 796
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/d/a/a/ox;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_f

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 798
    :cond_6
    :goto_6
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 799
    iget v0, p0, Lcom/google/d/a/a/ox;->i:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_10

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 801
    :cond_7
    :goto_7
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 802
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/d/a/a/ox;->j:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_11

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 804
    :cond_8
    :goto_8
    iget-object v0, p0, Lcom/google/d/a/a/ox;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 805
    return-void

    .line 778
    :cond_9
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 781
    :cond_a
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 784
    :cond_b
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 787
    :cond_c
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_3

    .line 790
    :cond_d
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 793
    :cond_e
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_5

    .line 796
    :cond_f
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_6

    .line 799
    :cond_10
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_7

    .line 802
    :cond_11
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_8
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 766
    iget-byte v1, p0, Lcom/google/d/a/a/ox;->l:B

    .line 767
    if-ne v1, v0, :cond_0

    .line 771
    :goto_0
    return v0

    .line 768
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 770
    :cond_1
    iput-byte v0, p0, Lcom/google/d/a/a/ox;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 809
    iget v0, p0, Lcom/google/d/a/a/ox;->m:I

    .line 810
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 851
    :goto_0
    return v0

    .line 813
    :cond_0
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_12

    .line 814
    iget v0, p0, Lcom/google/d/a/a/ox;->b:I

    .line 815
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 817
    :goto_2
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 818
    iget v3, p0, Lcom/google/d/a/a/ox;->c:I

    .line 819
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_b

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 821
    :cond_1
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 822
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/d/a/a/ox;->d:I

    .line 823
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 825
    :cond_2
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 826
    iget v3, p0, Lcom/google/d/a/a/ox;->e:I

    .line 827
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_d

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 829
    :cond_3
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 830
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/d/a/a/ox;->f:I

    .line 831
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 833
    :cond_4
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 834
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/d/a/a/ox;->g:I

    .line 835
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 837
    :cond_5
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 838
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/d/a/a/ox;->h:I

    .line 839
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 841
    :cond_6
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 842
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/d/a/a/ox;->i:I

    .line 843
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_11

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_9
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 845
    :cond_7
    iget v3, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_9

    .line 846
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/d/a/a/ox;->j:I

    .line 847
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_8
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 849
    :cond_9
    iget-object v1, p0, Lcom/google/d/a/a/ox;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 850
    iput v0, p0, Lcom/google/d/a/a/ox;->m:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 815
    goto/16 :goto_1

    :cond_b
    move v3, v1

    .line 819
    goto/16 :goto_3

    :cond_c
    move v3, v1

    .line 823
    goto/16 :goto_4

    :cond_d
    move v3, v1

    .line 827
    goto/16 :goto_5

    :cond_e
    move v3, v1

    .line 831
    goto/16 :goto_6

    :cond_f
    move v3, v1

    .line 835
    goto :goto_7

    :cond_10
    move v3, v1

    .line 839
    goto :goto_8

    :cond_11
    move v3, v1

    .line 843
    goto :goto_9

    :cond_12
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/d/a/a/ox;->newBuilder()Lcom/google/d/a/a/oz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/oz;->a(Lcom/google/d/a/a/ox;)Lcom/google/d/a/a/oz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/d/a/a/ox;->newBuilder()Lcom/google/d/a/a/oz;

    move-result-object v0

    return-object v0
.end method
