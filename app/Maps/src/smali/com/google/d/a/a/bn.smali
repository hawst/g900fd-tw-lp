.class public final Lcom/google/d/a/a/bn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/d/a/a/bs;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/bn;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/d/a/a/bn;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/d/a/a/bo;

    invoke-direct {v0}, Lcom/google/d/a/a/bo;-><init>()V

    sput-object v0, Lcom/google/d/a/a/bn;->PARSER:Lcom/google/n/ax;

    .line 4125
    const/4 v0, 0x0

    sput-object v0, Lcom/google/d/a/a/bn;->d:Lcom/google/n/aw;

    .line 4236
    new-instance v0, Lcom/google/d/a/a/bn;

    invoke-direct {v0}, Lcom/google/d/a/a/bn;-><init>()V

    sput-object v0, Lcom/google/d/a/a/bn;->a:Lcom/google/d/a/a/bn;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 26
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4096
    iput-byte v0, p0, Lcom/google/d/a/a/bn;->b:B

    .line 4112
    iput v0, p0, Lcom/google/d/a/a/bn;->c:I

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 33
    invoke-direct {p0}, Lcom/google/d/a/a/bn;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 37
    const/4 v0, 0x0

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 40
    packed-switch v3, :pswitch_data_0

    .line 45
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 47
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 43
    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/d/a/a/bn;->au:Lcom/google/n/bn;

    .line 60
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 54
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 59
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/d/a/a/bn;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :catch_1
    move-exception v0

    .line 56
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 57
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 24
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4096
    iput-byte v0, p0, Lcom/google/d/a/a/bn;->b:B

    .line 4112
    iput v0, p0, Lcom/google/d/a/a/bn;->c:I

    .line 25
    return-void
.end method

.method public static d()Lcom/google/d/a/a/bn;
    .locals 1

    .prologue
    .line 4239
    sget-object v0, Lcom/google/d/a/a/bn;->a:Lcom/google/d/a/a/bn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/d/a/a/bp;
    .locals 1

    .prologue
    .line 4187
    new-instance v0, Lcom/google/d/a/a/bp;

    invoke-direct {v0}, Lcom/google/d/a/a/bp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/d/a/a/bn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    sget-object v0, Lcom/google/d/a/a/bn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 4108
    iget v0, p0, Lcom/google/d/a/a/bn;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4109
    :goto_0
    iget-object v0, p0, Lcom/google/d/a/a/bn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4110
    return-void

    .line 4108
    :cond_0
    iget-object v0, p0, Lcom/google/d/a/a/bn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/d/a/a/bn;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4098
    iget-byte v1, p0, Lcom/google/d/a/a/bn;->b:B

    .line 4099
    if-ne v1, v0, :cond_0

    .line 4103
    :goto_0
    return v0

    .line 4100
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 4102
    :cond_1
    iput-byte v0, p0, Lcom/google/d/a/a/bn;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 4114
    iget v0, p0, Lcom/google/d/a/a/bn;->c:I

    .line 4115
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4120
    :goto_0
    return v0

    .line 4117
    :cond_0
    iget-object v0, p0, Lcom/google/d/a/a/bn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4119
    iput v0, p0, Lcom/google/d/a/a/bn;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 18
    invoke-static {}, Lcom/google/d/a/a/bn;->newBuilder()Lcom/google/d/a/a/bp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/d/a/a/bp;->a(Lcom/google/d/a/a/bn;)Lcom/google/d/a/a/bp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 18
    invoke-static {}, Lcom/google/d/a/a/bn;->newBuilder()Lcom/google/d/a/a/bp;

    move-result-object v0

    return-object v0
.end method
