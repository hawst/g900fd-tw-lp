.class public abstract Lcom/google/n/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/n/ax;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType::",
        "Lcom/google/n/at;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/n/ax",
        "<TMessageType;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/google/n/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/google/n/o;->a()Lcom/google/n/o;

    move-result-object v0

    sput-object v0, Lcom/google/n/e;->a:Lcom/google/n/o;

    .line 56
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/n/at;)Lcom/google/n/at;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)TMessageType;"
        }
    .end annotation

    .prologue
    .line 48
    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/google/n/at;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 49
    instance-of v0, p1, Lcom/google/n/a;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/n/a;

    new-instance v0, Lcom/google/n/bm;

    invoke-direct {v0}, Lcom/google/n/bm;-><init>()V

    .line 50
    :goto_0
    new-instance v1, Lcom/google/n/ak;

    invoke-virtual {v0}, Lcom/google/n/bm;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    .line 51
    iput-object p1, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1

    .line 49
    :cond_0
    instance-of v0, p1, Lcom/google/n/d;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/n/d;

    new-instance v0, Lcom/google/n/bm;

    invoke-direct {v0}, Lcom/google/n/bm;-><init>()V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/n/bm;

    invoke-direct {v0}, Lcom/google/n/bm;-><init>()V

    goto :goto_0

    .line 53
    :cond_2
    return-object p1
.end method

.method private a(Ljava/io/InputStream;Lcom/google/n/o;)Lcom/google/n/at;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/google/n/o;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 169
    new-instance v1, Lcom/google/n/j;

    invoke-direct {v1, p1}, Lcom/google/n/j;-><init>(Ljava/io/InputStream;)V

    .line 170
    invoke-virtual {p0, v1, p2}, Lcom/google/n/e;->a(Lcom/google/n/j;Lcom/google/n/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 172
    const/4 v2, 0x0

    :try_start_0
    iget v1, v1, Lcom/google/n/j;->d:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v1

    throw v1
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :catch_0
    move-exception v1

    .line 174
    iput-object v0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1

    .line 176
    :cond_0
    return-object v0
.end method

.method private a([BIILcom/google/n/o;)Lcom/google/n/at;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Lcom/google/n/o;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 114
    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/n/j;->a([BII)Lcom/google/n/j;

    move-result-object v1

    .line 115
    invoke-virtual {p0, v1, p4}, Lcom/google/n/e;->a(Lcom/google/n/j;Lcom/google/n/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_1

    .line 117
    const/4 v2, 0x0

    :try_start_1
    iget v1, v1, Lcom/google/n/j;->d:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v1

    throw v1
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_0

    .line 118
    :catch_0
    move-exception v1

    .line 119
    :try_start_2
    iput-object v0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_1

    .line 122
    :catch_1
    move-exception v0

    .line 123
    throw v0

    .line 121
    :cond_0
    return-object v0
.end method

.method private b(Lcom/google/n/f;Lcom/google/n/o;)Lcom/google/n/at;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/f;",
            "Lcom/google/n/o;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 81
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/f;->f()Lcom/google/n/j;

    move-result-object v1

    .line 82
    invoke-virtual {p0, v1, p2}, Lcom/google/n/e;->a(Lcom/google/n/j;Lcom/google/n/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_1

    .line 84
    const/4 v2, 0x0

    :try_start_1
    iget v1, v1, Lcom/google/n/j;->d:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v1

    throw v1
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_0

    .line 85
    :catch_0
    move-exception v1

    .line 86
    :try_start_2
    iput-object v0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_1

    .line 89
    :catch_1
    move-exception v0

    .line 90
    throw v0

    .line 88
    :cond_0
    return-object v0
.end method

.method private b(Ljava/io/InputStream;Lcom/google/n/o;)Lcom/google/n/at;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/google/n/o;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 202
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 203
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 204
    const/4 v0, 0x0

    .line 211
    :goto_0
    return-object v0

    .line 206
    :cond_0
    invoke-static {v0, p1}, Lcom/google/n/j;->a(ILjava/io/InputStream;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 210
    new-instance v1, Lcom/google/n/c;

    invoke-direct {v1, p1, v0}, Lcom/google/n/c;-><init>(Ljava/io/InputStream;I)V

    .line 211
    invoke-direct {p0, v1, p2}, Lcom/google/n/e;->a(Ljava/io/InputStream;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .line 208
    new-instance v1, Lcom/google/n/ak;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/f;Lcom/google/n/o;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/n/e;->b(Lcom/google/n/f;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/n/e;->a(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/n/e;->a:Lcom/google/n/o;

    invoke-direct {p0, p1, v0}, Lcom/google/n/e;->b(Ljava/io/InputStream;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/n/e;->a(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a([B)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x0

    array-length v1, p1

    sget-object v2, Lcom/google/n/e;->a:Lcom/google/n/o;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/n/e;->a([BIILcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/n/e;->a:Lcom/google/n/o;

    invoke-direct {p0, p1, v0}, Lcom/google/n/e;->a(Ljava/io/InputStream;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/n/e;->a(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b([B)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 21
    sget-object v0, Lcom/google/n/e;->a:Lcom/google/n/o;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/google/n/e;->a([BIILcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/n/e;->a(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    return-object v0
.end method
