.class public abstract Lcom/google/n/t;
.super Lcom/google/n/a;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public au:Lcom/google/n/bn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/n/a;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/t;->au:Lcom/google/n/bn;

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/n/a;-><init>()V

    .line 35
    iget-object v0, p1, Lcom/google/n/v;->I:Lcom/google/n/bn;

    iput-object v0, p0, Lcom/google/n/t;->au:Lcom/google/n/bn;

    .line 36
    return-void
.end method

.method public static a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MessageType::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/n/q",
            "<",
            "Lcom/google/n/aa;",
            ">;TMessageType;",
            "Lcom/google/n/j;",
            "Lcom/google/n/bo;",
            "Lcom/google/n/o;",
            "I)Z"
        }
    .end annotation

    .prologue
    const v8, 0x7fffffff

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 584
    invoke-static {p5}, Lcom/google/n/bt;->a(I)I

    move-result v1

    .line 585
    invoke-static {p5}, Lcom/google/n/bt;->b(I)I

    move-result v6

    .line 588
    iget-object v0, p4, Lcom/google/n/o;->a:Ljava/util/Map;

    new-instance v5, Lcom/google/n/p;

    invoke-direct {v5, p1, v6}, Lcom/google/n/p;-><init>(Ljava/lang/Object;I)V

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ab;

    .line 593
    if-nez v0, :cond_0

    move v1, v4

    move v5, v2

    .line 609
    :goto_0
    if-eqz v5, :cond_3

    .line 610
    invoke-virtual {p3, p5, p2}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    .line 692
    :goto_1
    return v0

    .line 595
    :cond_0
    iget-object v5, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 596
    iget-object v5, v5, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    .line 595
    invoke-static {v5, v4}, Lcom/google/n/q;->a(Lcom/google/n/bu;Z)I

    move-result v5

    if-ne v1, v5, :cond_1

    move v1, v4

    move v5, v4

    .line 598
    goto :goto_0

    .line 599
    :cond_1
    iget-object v5, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-boolean v5, v5, Lcom/google/n/aa;->d:Z

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-object v5, v5, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    .line 600
    invoke-virtual {v5}, Lcom/google/n/bu;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 602
    iget-object v5, v5, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    .line 601
    invoke-static {v5, v2}, Lcom/google/n/q;->a(Lcom/google/n/bu;Z)I

    move-result v5

    if-ne v1, v5, :cond_2

    move v1, v2

    move v5, v4

    .line 604
    goto :goto_0

    :cond_2
    move v1, v4

    move v5, v2

    .line 606
    goto :goto_0

    .line 613
    :cond_3
    if-eqz v1, :cond_a

    .line 614
    invoke-virtual {p2}, Lcom/google/n/j;->l()I

    move-result v1

    .line 615
    invoke-virtual {p2, v1}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 616
    iget-object v1, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-object v1, v1, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    sget-object v6, Lcom/google/n/bu;->n:Lcom/google/n/bu;

    if-ne v1, v6, :cond_7

    .line 617
    :goto_2
    iget v1, p2, Lcom/google/n/j;->f:I

    if-ne v1, v8, :cond_4

    move v1, v3

    :goto_3
    if-lez v1, :cond_9

    .line 618
    invoke-virtual {p2}, Lcom/google/n/j;->l()I

    move-result v1

    .line 619
    iget-object v4, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 620
    iget-object v4, v4, Lcom/google/n/aa;->a:Lcom/google/n/ah;

    invoke-interface {v4, v1}, Lcom/google/n/ah;->a(I)Lcom/google/n/ag;

    move-result-object v1

    .line 621
    if-nez v1, :cond_5

    move v0, v2

    .line 624
    goto :goto_1

    .line 617
    :cond_4
    iget v1, p2, Lcom/google/n/j;->e:I

    iget v4, p2, Lcom/google/n/j;->c:I

    add-int/2addr v1, v4

    iget v4, p2, Lcom/google/n/j;->f:I

    sub-int v1, v4, v1

    goto :goto_3

    .line 626
    :cond_5
    iget-object v4, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 627
    iget-object v6, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-object v6, v6, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    iget-object v6, v6, Lcom/google/n/bu;->s:Lcom/google/n/bz;

    sget-object v7, Lcom/google/n/bz;->h:Lcom/google/n/bz;

    if-ne v6, v7, :cond_6

    check-cast v1, Lcom/google/n/ag;

    invoke-interface {v1}, Lcom/google/n/ag;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 626
    :cond_6
    invoke-virtual {p0, v4, v1}, Lcom/google/n/q;->b(Lcom/google/n/s;Ljava/lang/Object;)V

    goto :goto_2

    .line 630
    :cond_7
    :goto_4
    iget v1, p2, Lcom/google/n/j;->f:I

    if-ne v1, v8, :cond_8

    move v1, v3

    :goto_5
    if-lez v1, :cond_9

    .line 631
    iget-object v1, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 633
    iget-object v1, v1, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    .line 632
    invoke-static {p2, v1, v4}, Lcom/google/n/q;->a(Lcom/google/n/j;Lcom/google/n/bu;Z)Ljava/lang/Object;

    move-result-object v1

    .line 635
    iget-object v6, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    invoke-virtual {p0, v6, v1}, Lcom/google/n/q;->b(Lcom/google/n/s;Ljava/lang/Object;)V

    goto :goto_4

    .line 630
    :cond_8
    iget v1, p2, Lcom/google/n/j;->e:I

    iget v6, p2, Lcom/google/n/j;->c:I

    add-int/2addr v1, v6

    iget v6, p2, Lcom/google/n/j;->f:I

    sub-int v1, v6, v1

    goto :goto_5

    .line 638
    :cond_9
    iput v5, p2, Lcom/google/n/j;->f:I

    invoke-virtual {p2}, Lcom/google/n/j;->p()V

    :goto_6
    move v0, v2

    .line 692
    goto/16 :goto_1

    .line 641
    :cond_a
    sget-object v1, Lcom/google/n/u;->a:[I

    iget-object v3, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-object v3, v3, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    iget-object v3, v3, Lcom/google/n/bu;->s:Lcom/google/n/bz;

    invoke-virtual {v3}, Lcom/google/n/bz;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 677
    iget-object v1, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 678
    iget-object v1, v1, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    .line 677
    invoke-static {p2, v1, v4}, Lcom/google/n/q;->a(Lcom/google/n/j;Lcom/google/n/bu;Z)Ljava/lang/Object;

    move-result-object v1

    .line 683
    :cond_b
    :goto_7
    iget-object v3, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-boolean v3, v3, Lcom/google/n/aa;->d:Z

    if-eqz v3, :cond_14

    .line 684
    iget-object v3, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 685
    iget-object v0, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-object v0, v0, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    iget-object v0, v0, Lcom/google/n/bu;->s:Lcom/google/n/bz;

    sget-object v4, Lcom/google/n/bz;->h:Lcom/google/n/bz;

    if-ne v0, v4, :cond_c

    move-object v0, v1

    check-cast v0, Lcom/google/n/ag;

    invoke-interface {v0}, Lcom/google/n/ag;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 684
    :cond_c
    invoke-virtual {p0, v3, v1}, Lcom/google/n/q;->b(Lcom/google/n/s;Ljava/lang/Object;)V

    goto :goto_6

    .line 643
    :pswitch_0
    const/4 v3, 0x0

    .line 644
    iget-object v1, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-boolean v1, v1, Lcom/google/n/aa;->d:Z

    if-nez v1, :cond_16

    .line 645
    iget-object v1, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 646
    iget-object v5, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v5, v1}, Lcom/google/n/be;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v5, v1, Lcom/google/n/al;

    if-eqz v5, :cond_d

    check-cast v1, Lcom/google/n/al;

    iget-object v5, v1, Lcom/google/n/al;->a:Lcom/google/n/at;

    invoke-virtual {v1, v5}, Lcom/google/n/al;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    :cond_d
    check-cast v1, Lcom/google/n/at;

    .line 647
    if-eqz v1, :cond_16

    .line 648
    invoke-interface {v1}, Lcom/google/n/at;->e()Lcom/google/n/au;

    move-result-object v1

    .line 651
    :goto_8
    if-nez v1, :cond_e

    .line 652
    iget-object v1, v0, Lcom/google/n/ab;->a:Lcom/google/n/at;

    .line 653
    invoke-interface {v1}, Lcom/google/n/at;->f()Lcom/google/n/au;

    move-result-object v1

    .line 655
    :cond_e
    iget-object v3, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-object v3, v3, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    sget-object v5, Lcom/google/n/bu;->j:Lcom/google/n/bu;

    if-ne v3, v5, :cond_11

    .line 657
    iget-object v3, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget v3, v3, Lcom/google/n/aa;->b:I

    iget v4, p2, Lcom/google/n/j;->g:I

    iget v5, p2, Lcom/google/n/j;->h:I

    if-lt v4, v5, :cond_f

    invoke-static {}, Lcom/google/n/ak;->g()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_f
    iget v4, p2, Lcom/google/n/j;->g:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p2, Lcom/google/n/j;->g:I

    invoke-interface {v1, p2, p4}, Lcom/google/n/au;->b(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/au;

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    iget v4, p2, Lcom/google/n/j;->d:I

    if-eq v4, v3, :cond_10

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_10
    iget v3, p2, Lcom/google/n/j;->g:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p2, Lcom/google/n/j;->g:I

    .line 662
    :goto_9
    invoke-interface {v1}, Lcom/google/n/au;->h()Lcom/google/n/at;

    move-result-object v1

    goto/16 :goto_7

    .line 660
    :cond_11
    invoke-virtual {p2}, Lcom/google/n/j;->l()I

    move-result v3

    iget v5, p2, Lcom/google/n/j;->g:I

    iget v6, p2, Lcom/google/n/j;->h:I

    if-lt v5, v6, :cond_12

    invoke-static {}, Lcom/google/n/ak;->g()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_12
    invoke-virtual {p2, v3}, Lcom/google/n/j;->a(I)I

    move-result v3

    iget v5, p2, Lcom/google/n/j;->g:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p2, Lcom/google/n/j;->g:I

    invoke-interface {v1, p2, p4}, Lcom/google/n/au;->b(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/au;

    iget v5, p2, Lcom/google/n/j;->d:I

    if-eq v5, v4, :cond_13

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_13
    iget v4, p2, Lcom/google/n/j;->g:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p2, Lcom/google/n/j;->g:I

    iput v3, p2, Lcom/google/n/j;->f:I

    invoke-virtual {p2}, Lcom/google/n/j;->p()V

    goto :goto_9

    .line 666
    :pswitch_1
    invoke-virtual {p2}, Lcom/google/n/j;->l()I

    move-result v3

    .line 667
    iget-object v1, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-object v1, v1, Lcom/google/n/aa;->a:Lcom/google/n/ah;

    .line 668
    invoke-interface {v1, v3}, Lcom/google/n/ah;->a(I)Lcom/google/n/ag;

    move-result-object v1

    .line 671
    if-nez v1, :cond_b

    .line 672
    invoke-virtual {p3, v6, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    move v0, v2

    .line 673
    goto/16 :goto_1

    .line 687
    :cond_14
    iget-object v3, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    .line 688
    iget-object v0, v0, Lcom/google/n/ab;->b:Lcom/google/n/aa;

    iget-object v0, v0, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    iget-object v0, v0, Lcom/google/n/bu;->s:Lcom/google/n/bz;

    sget-object v4, Lcom/google/n/bz;->h:Lcom/google/n/bz;

    if-ne v0, v4, :cond_15

    check-cast v1, Lcom/google/n/ag;

    invoke-interface {v1}, Lcom/google/n/ag;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 687
    :cond_15
    invoke-virtual {p0, v3, v1}, Lcom/google/n/q;->a(Lcom/google/n/s;Ljava/lang/Object;)V

    goto/16 :goto_6

    :cond_16
    move-object v1, v3

    goto/16 :goto_8

    .line 641
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a()Lcom/google/n/ax;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<+",
            "Lcom/google/n/at;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1035
    new-instance v0, Lcom/google/n/ac;

    invoke-direct {v0, p0}, Lcom/google/n/ac;-><init>(Lcom/google/n/at;)V

    return-object v0
.end method
