.class Lcom/google/n/ba;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/n/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 577
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    return-void
.end method


# virtual methods
.method a(Lcom/google/n/f;)V
    .locals 4

    .prologue
    .line 599
    invoke-virtual {p1}, Lcom/google/n/f;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 600
    invoke-virtual {p1}, Lcom/google/n/f;->b()I

    move-result v0

    sget-object v1, Lcom/google/n/az;->c:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-gez v0, :cond_7

    add-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    sget-object v0, Lcom/google/n/az;->c:[I

    add-int/lit8 v2, v1, 0x1

    aget v2, v0, v2

    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    if-lt v0, v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    :goto_1
    return-void

    .line 600
    :cond_1
    sget-object v0, Lcom/google/n/az;->c:[I

    aget v3, v0, v1

    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/f;

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    if-ge v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/f;

    new-instance v2, Lcom/google/n/az;

    invoke-direct {v2, v0, v1}, Lcom/google/n/az;-><init>(Lcom/google/n/f;Lcom/google/n/f;)V

    move-object v1, v2

    goto :goto_2

    :cond_2
    new-instance v0, Lcom/google/n/az;

    invoke-direct {v0, v1, p1}, Lcom/google/n/az;-><init>(Lcom/google/n/f;Lcom/google/n/f;)V

    move-object v1, v0

    :goto_3
    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    sget-object v2, Lcom/google/n/az;->c:[I

    invoke-static {v2, v0}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-gez v0, :cond_3

    add-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    :cond_3
    sget-object v2, Lcom/google/n/az;->c:[I

    add-int/lit8 v0, v0, 0x1

    aget v2, v2, v0

    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    if-ge v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/f;

    new-instance v2, Lcom/google/n/az;

    invoke-direct {v2, v0, v1}, Lcom/google/n/az;-><init>(Lcom/google/n/f;Lcom/google/n/f;)V

    move-object v1, v2

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/n/ba;->a:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 601
    :cond_5
    instance-of v0, p1, Lcom/google/n/az;

    if-eqz v0, :cond_6

    .line 602
    check-cast p1, Lcom/google/n/az;

    .line 603
    iget-object v0, p1, Lcom/google/n/az;->e:Lcom/google/n/f;

    invoke-virtual {p0, v0}, Lcom/google/n/ba;->a(Lcom/google/n/f;)V

    .line 604
    iget-object v0, p1, Lcom/google/n/az;->f:Lcom/google/n/f;

    invoke-virtual {p0, v0}, Lcom/google/n/ba;->a(Lcom/google/n/f;)V

    goto/16 :goto_1

    .line 606
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 608
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Has a new type of ByteString been created? Found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v1, v0

    goto/16 :goto_0
.end method
