.class Lcom/google/n/bb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/n/ar;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/google/n/ar;

.field private final b:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/n/az;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/n/f;)V
    .locals 1

    .prologue
    .line 693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 689
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/n/bb;->b:Ljava/util/Stack;

    .line 694
    invoke-direct {p0, p1}, Lcom/google/n/bb;->a(Lcom/google/n/f;)Lcom/google/n/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    .line 695
    return-void
.end method

.method private a(Lcom/google/n/f;)Lcom/google/n/ar;
    .locals 2

    .prologue
    .line 698
    move-object v0, p1

    .line 699
    :goto_0
    instance-of v1, v0, Lcom/google/n/az;

    if-eqz v1, :cond_0

    .line 700
    check-cast v0, Lcom/google/n/az;

    .line 701
    iget-object v1, p0, Lcom/google/n/bb;->b:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 702
    iget-object v0, v0, Lcom/google/n/az;->e:Lcom/google/n/f;

    goto :goto_0

    .line 704
    :cond_0
    check-cast v0, Lcom/google/n/ar;

    return-object v0
.end method


# virtual methods
.method a()Lcom/google/n/ar;
    .locals 2

    .prologue
    .line 711
    :cond_0
    iget-object v0, p0, Lcom/google/n/bb;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    const/4 v0, 0x0

    .line 716
    :goto_0
    return-object v0

    .line 714
    :cond_1
    iget-object v0, p0, Lcom/google/n/bb;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/az;

    iget-object v0, v0, Lcom/google/n/az;->f:Lcom/google/n/f;

    invoke-direct {p0, v0}, Lcom/google/n/bb;->a(Lcom/google/n/f;)Lcom/google/n/ar;

    move-result-object v0

    .line 715
    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    invoke-virtual {p0}, Lcom/google/n/bb;->a()Lcom/google/n/ar;

    move-result-object v1

    iput-object v1, p0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 741
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
