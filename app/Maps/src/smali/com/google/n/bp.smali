.class public Lcom/google/n/bp;
.super Ljava/util/AbstractList;
.source "PG"

# interfaces
.implements Lcom/google/n/aq;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/google/n/aq;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/n/aq;


# direct methods
.method public constructor <init>(Lcom/google/n/aq;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/n/bp;->a:Lcom/google/n/aq;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/n/f;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/n/bp;->a:Lcom/google/n/aq;

    invoke-interface {v0, p1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/n/bp;->a:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/n/aq;
    .locals 0

    .prologue
    .line 175
    return-object p0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/n/bp;->a:Lcom/google/n/aq;

    invoke-interface {v0, p1}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    new-instance v0, Lcom/google/n/br;

    invoke-direct {v0, p0}, Lcom/google/n/br;-><init>(Lcom/google/n/bp;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lcom/google/n/bq;

    invoke-direct {v0, p0, p1}, Lcom/google/n/bq;-><init>(Lcom/google/n/bp;I)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/n/bp;->a:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    return v0
.end method
