.class final Lcom/google/n/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[B


# direct methods
.method constructor <init>(Lcom/google/n/at;)V
    .locals 1

    .prologue
    .line 997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 998
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/ac;->a:Ljava/lang/String;

    .line 999
    invoke-interface {p1}, Lcom/google/n/at;->l()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/ac;->b:[B

    .line 1000
    return-void
.end method


# virtual methods
.method protected final readResolve()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1010
    :try_start_0
    iget-object v0, p0, Lcom/google/n/ac;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1011
    const-string v1, "PARSER"

    .line 1012
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ax;

    .line 1013
    iget-object v1, p0, Lcom/google/n/ac;->b:[B

    invoke-interface {v0, v1}, Lcom/google/n/ax;->a([B)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    return-object v0

    .line 1014
    :catch_0
    move-exception v0

    .line 1015
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to find proto buffer class"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1016
    :catch_1
    move-exception v0

    .line 1017
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to find PARSER"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1018
    :catch_2
    move-exception v0

    .line 1019
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to call PARSER"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1020
    :catch_3
    move-exception v0

    .line 1021
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to call parseFrom method"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1022
    :catch_4
    move-exception v0

    .line 1023
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to understand proto buffer"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
