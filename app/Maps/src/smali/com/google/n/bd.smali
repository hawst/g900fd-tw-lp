.class Lcom/google/n/bd;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/n/az;

.field private b:Lcom/google/n/bb;

.field private c:Lcom/google/n/ar;

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/n/az;)V
    .locals 0

    .prologue
    .line 804
    iput-object p1, p0, Lcom/google/n/bd;->a:Lcom/google/n/az;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 805
    invoke-direct {p0}, Lcom/google/n/bd;->a()V

    .line 806
    return-void
.end method

.method private a([BII)I
    .locals 5

    .prologue
    .line 839
    move v1, p3

    move v0, p2

    .line 840
    :goto_0
    if-lez v1, :cond_2

    .line 841
    invoke-direct {p0}, Lcom/google/n/bd;->b()V

    .line 842
    iget-object v2, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    if-nez v2, :cond_0

    .line 843
    if-ne v1, p3, :cond_2

    .line 845
    const/4 v0, -0x1

    .line 861
    :goto_1
    return v0

    .line 850
    :cond_0
    iget v2, p0, Lcom/google/n/bd;->d:I

    iget v3, p0, Lcom/google/n/bd;->e:I

    sub-int/2addr v2, v3

    .line 851
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 852
    if-eqz p1, :cond_1

    .line 853
    iget-object v3, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    iget v4, p0, Lcom/google/n/bd;->e:I

    invoke-virtual {v3, p1, v4, v0, v2}, Lcom/google/n/ar;->a([BIII)V

    .line 854
    add-int/2addr v0, v2

    .line 856
    :cond_1
    iget v3, p0, Lcom/google/n/bd;->e:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/google/n/bd;->e:I

    .line 857
    sub-int/2addr v1, v2

    .line 858
    goto :goto_0

    .line 861
    :cond_2
    sub-int v0, p3, v1

    goto :goto_1
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 900
    new-instance v0, Lcom/google/n/bb;

    iget-object v1, p0, Lcom/google/n/bd;->a:Lcom/google/n/az;

    invoke-direct {v0, v1}, Lcom/google/n/bb;-><init>(Lcom/google/n/f;)V

    iput-object v0, p0, Lcom/google/n/bd;->b:Lcom/google/n/bb;

    .line 901
    iget-object v0, p0, Lcom/google/n/bd;->b:Lcom/google/n/bb;

    iget-object v1, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    if-nez v1, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    invoke-virtual {v0}, Lcom/google/n/bb;->a()Lcom/google/n/ar;

    move-result-object v2

    iput-object v2, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    iput-object v1, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    .line 902
    iget-object v0, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    invoke-virtual {v0}, Lcom/google/n/ar;->b()I

    move-result v0

    iput v0, p0, Lcom/google/n/bd;->d:I

    .line 903
    iput v3, p0, Lcom/google/n/bd;->e:I

    .line 904
    iput v3, p0, Lcom/google/n/bd;->f:I

    .line 905
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 913
    iget-object v0, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/n/bd;->e:I

    iget v1, p0, Lcom/google/n/bd;->d:I

    if-ne v0, v1, :cond_1

    .line 916
    iget v0, p0, Lcom/google/n/bd;->f:I

    iget v1, p0, Lcom/google/n/bd;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/bd;->f:I

    .line 917
    iput v2, p0, Lcom/google/n/bd;->e:I

    .line 918
    iget-object v0, p0, Lcom/google/n/bd;->b:Lcom/google/n/bb;

    invoke-virtual {v0}, Lcom/google/n/bb;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 919
    iget-object v0, p0, Lcom/google/n/bd;->b:Lcom/google/n/bb;

    iget-object v1, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    if-nez v1, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    invoke-virtual {v0}, Lcom/google/n/bb;->a()Lcom/google/n/ar;

    move-result-object v2

    iput-object v2, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    iput-object v1, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    .line 920
    iget-object v0, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    invoke-virtual {v0}, Lcom/google/n/ar;->b()I

    move-result v0

    iput v0, p0, Lcom/google/n/bd;->d:I

    .line 926
    :cond_1
    :goto_0
    return-void

    .line 922
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    .line 923
    iput v2, p0, Lcom/google/n/bd;->d:I

    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 2

    .prologue
    .line 876
    iget v0, p0, Lcom/google/n/bd;->f:I

    iget v1, p0, Lcom/google/n/bd;->e:I

    add-int/2addr v0, v1

    .line 877
    iget-object v1, p0, Lcom/google/n/bd;->a:Lcom/google/n/az;

    iget v1, v1, Lcom/google/n/az;->d:I

    sub-int v0, v1, v0

    return v0
.end method

.method public mark(I)V
    .locals 2

    .prologue
    .line 888
    iget v0, p0, Lcom/google/n/bd;->f:I

    iget v1, p0, Lcom/google/n/bd;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/bd;->g:I

    .line 889
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 882
    const/4 v0, 0x1

    return v0
.end method

.method public read()I
    .locals 3

    .prologue
    .line 866
    invoke-direct {p0}, Lcom/google/n/bd;->b()V

    .line 867
    iget-object v0, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    if-nez v0, :cond_0

    .line 868
    const/4 v0, -0x1

    .line 870
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/n/bd;->c:Lcom/google/n/ar;

    iget v1, p0, Lcom/google/n/bd;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/n/bd;->e:I

    invoke-virtual {v0, v1}, Lcom/google/n/ar;->c(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 1

    .prologue
    .line 810
    if-nez p1, :cond_0

    .line 811
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 812
    :cond_0
    if-ltz p2, :cond_1

    if-ltz p3, :cond_1

    array-length v0, p1

    sub-int/2addr v0, p2

    if-le p3, v0, :cond_2

    .line 813
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 815
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/google/n/bd;->a([BII)I

    move-result v0

    return v0
.end method

.method public declared-synchronized reset()V
    .locals 3

    .prologue
    .line 894
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/n/bd;->a()V

    .line 895
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/n/bd;->g:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/n/bd;->a([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 896
    monitor-exit p0

    return-void

    .line 894
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public skip(J)J
    .locals 5

    .prologue
    const-wide/32 v0, 0x7fffffff

    .line 820
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    .line 821
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 822
    :cond_0
    cmp-long v2, p1, v0

    if-lez v2, :cond_1

    move-wide p1, v0

    .line 825
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    long-to-int v2, p1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/n/bd;->a([BII)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
