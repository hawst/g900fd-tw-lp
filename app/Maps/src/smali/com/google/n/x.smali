.class public abstract Lcom/google/n/x;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/n/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lcom/google/n/x",
        "<TMessageType;>;>",
        "Lcom/google/n/t;",
        "Lcom/google/n/z",
        "<TMessageType;>;"
    }
.end annotation


# instance fields
.field public final as:Lcom/google/n/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/q",
            "<",
            "Lcom/google/n/aa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 231
    invoke-static {}, Lcom/google/n/q;->a()Lcom/google/n/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    .line 232
    return-void
.end method

.method public constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<TMessageType;*>;)V"
        }
    .end annotation

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 235
    iget-object v0, p1, Lcom/google/n/w;->d:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/q;->b:Z

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/google/n/w;->e:Z

    iget-object v0, p1, Lcom/google/n/w;->d:Lcom/google/n/q;

    iput-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    .line 236
    return-void
.end method


# virtual methods
.method public final g()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 382
    iget-object v4, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    move v2, v0

    move v3, v0

    :goto_0
    iget-object v0, v4, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-object v0, v4, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/s;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/n/q;->c(Lcom/google/n/s;Ljava/lang/Object;)I

    move-result v0

    add-int v1, v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_0

    :cond_0
    iget-object v0, v4, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v1, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/n/bg;->a()Ljava/lang/Iterable;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/s;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/n/q;->c(Lcom/google/n/s;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v3, v0

    goto :goto_2

    :cond_1
    iget-object v0, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_1

    :cond_2
    return v3
.end method
