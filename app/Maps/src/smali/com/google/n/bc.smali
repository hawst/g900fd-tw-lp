.class Lcom/google/n/bc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/n/g;


# instance fields
.field a:I

.field private final b:Lcom/google/n/bb;

.field private c:Lcom/google/n/g;


# direct methods
.method constructor <init>(Lcom/google/n/az;)V
    .locals 3

    .prologue
    .line 759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 760
    new-instance v0, Lcom/google/n/bb;

    invoke-direct {v0, p1}, Lcom/google/n/bb;-><init>(Lcom/google/n/f;)V

    iput-object v0, p0, Lcom/google/n/bc;->b:Lcom/google/n/bb;

    .line 761
    iget-object v0, p0, Lcom/google/n/bc;->b:Lcom/google/n/bb;

    iget-object v1, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    if-nez v1, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    invoke-virtual {v0}, Lcom/google/n/bb;->a()Lcom/google/n/ar;

    move-result-object v2

    iput-object v2, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    invoke-virtual {v1}, Lcom/google/n/ar;->a()Lcom/google/n/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/bc;->c:Lcom/google/n/g;

    .line 762
    iget v0, p1, Lcom/google/n/az;->d:I

    iput v0, p0, Lcom/google/n/bc;->a:I

    .line 763
    return-void
.end method


# virtual methods
.method public final a()B
    .locals 3

    .prologue
    .line 774
    iget-object v0, p0, Lcom/google/n/bc;->c:Lcom/google/n/g;

    invoke-interface {v0}, Lcom/google/n/g;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 775
    iget-object v0, p0, Lcom/google/n/bc;->b:Lcom/google/n/bb;

    iget-object v1, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    if-nez v1, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    invoke-virtual {v0}, Lcom/google/n/bb;->a()Lcom/google/n/ar;

    move-result-object v2

    iput-object v2, v0, Lcom/google/n/bb;->a:Lcom/google/n/ar;

    invoke-virtual {v1}, Lcom/google/n/ar;->a()Lcom/google/n/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/bc;->c:Lcom/google/n/g;

    .line 777
    :cond_1
    iget v0, p0, Lcom/google/n/bc;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/n/bc;->a:I

    .line 778
    iget-object v0, p0, Lcom/google/n/bc;->c:Lcom/google/n/g;

    invoke-interface {v0}, Lcom/google/n/g;->a()B

    move-result v0

    return v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 766
    iget v0, p0, Lcom/google/n/bc;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 753
    invoke-virtual {p0}, Lcom/google/n/bc;->a()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 782
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
