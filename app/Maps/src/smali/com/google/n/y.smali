.class public Lcom/google/n/y;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/n/x;

.field private final b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/google/n/aa;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/google/n/aa;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/n/x;Z)V
    .locals 2

    .prologue
    .line 345
    iput-object p1, p0, Lcom/google/n/y;->a:Lcom/google/n/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    iget-object v0, p0, Lcom/google/n/y;->a:Lcom/google/n/x;

    iget-object v1, v0, Lcom/google/n/x;->as:Lcom/google/n/q;

    .line 341
    iget-boolean v0, v1, Lcom/google/n/q;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/n/an;

    iget-object v1, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/n/an;-><init>(Ljava/util/Iterator;)V

    :goto_0
    iput-object v0, p0, Lcom/google/n/y;->b:Ljava/util/Iterator;

    .line 346
    iget-object v0, p0, Lcom/google/n/y;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/google/n/y;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/google/n/y;->c:Ljava/util/Map$Entry;

    .line 349
    :cond_0
    iput-boolean p2, p0, Lcom/google/n/y;->d:Z

    .line 350
    return-void

    .line 341
    :cond_1
    iget-object v0, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v0}, Lcom/google/n/be;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 354
    :goto_0
    iget-object v0, p0, Lcom/google/n/y;->c:Ljava/util/Map$Entry;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/n/y;->c:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/aa;

    iget v0, v0, Lcom/google/n/aa;->b:I

    if-ge v0, p1, :cond_2

    .line 355
    iget-object v0, p0, Lcom/google/n/y;->c:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/aa;

    .line 356
    iget-boolean v1, p0, Lcom/google/n/y;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/n/aa;->c:Lcom/google/n/bu;

    iget-object v1, v1, Lcom/google/n/bu;->s:Lcom/google/n/bz;

    sget-object v2, Lcom/google/n/bz;->i:Lcom/google/n/bz;

    if-ne v1, v2, :cond_0

    .line 358
    iget-boolean v1, v0, Lcom/google/n/aa;->d:Z

    if-nez v1, :cond_0

    .line 359
    iget v1, v0, Lcom/google/n/aa;->b:I

    iget-object v0, p0, Lcom/google/n/y;->c:Ljava/util/Map$Entry;

    .line 360
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 359
    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/google/n/l;->b(I)V

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p2, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p2, v5, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    const/4 v0, 0x4

    invoke-static {v4, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/n/l;->b(I)V

    .line 364
    :goto_1
    iget-object v0, p0, Lcom/google/n/y;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/google/n/y;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/google/n/y;->c:Ljava/util/Map$Entry;

    goto :goto_0

    .line 362
    :cond_0
    iget-object v1, p0, Lcom/google/n/y;->c:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/n/q;->a(Lcom/google/n/s;Ljava/lang/Object;Lcom/google/n/l;)V

    goto :goto_1

    .line 367
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/n/y;->c:Ljava/util/Map$Entry;

    goto :goto_0

    .line 370
    :cond_2
    return-void
.end method
