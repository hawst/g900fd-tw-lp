.class public final Lcom/google/n/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:[B

.field b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field g:I

.field h:I

.field private final i:Z

.field private j:I

.field private final k:Ljava/io/InputStream;

.field private l:Z

.field private m:I

.field private n:Lcom/google/n/k;


# direct methods
.method private constructor <init>(Lcom/google/n/ar;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 895
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/n/j;->l:Z

    .line 907
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/n/j;->f:I

    .line 911
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/n/j;->h:I

    .line 914
    const/high16 v0, 0x4000000

    iput v0, p0, Lcom/google/n/j;->m:I

    .line 1087
    iput-object v2, p0, Lcom/google/n/j;->n:Lcom/google/n/k;

    .line 939
    iget-object v0, p1, Lcom/google/n/ar;->c:[B

    iput-object v0, p0, Lcom/google/n/j;->a:[B

    .line 940
    invoke-virtual {p1}, Lcom/google/n/ar;->j()I

    move-result v0

    iput v0, p0, Lcom/google/n/j;->c:I

    .line 941
    iget v0, p0, Lcom/google/n/j;->c:I

    invoke-virtual {p1}, Lcom/google/n/ar;->b()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/j;->b:I

    .line 942
    iget v0, p0, Lcom/google/n/j;->c:I

    neg-int v0, v0

    iput v0, p0, Lcom/google/n/j;->e:I

    .line 943
    iput-object v2, p0, Lcom/google/n/j;->k:Ljava/io/InputStream;

    .line 944
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/n/j;->i:Z

    .line 945
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 895
    iput-boolean v1, p0, Lcom/google/n/j;->l:Z

    .line 907
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/n/j;->f:I

    .line 911
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/n/j;->h:I

    .line 914
    const/high16 v0, 0x4000000

    iput v0, p0, Lcom/google/n/j;->m:I

    .line 1087
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/n/j;->n:Lcom/google/n/k;

    .line 930
    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/n/j;->a:[B

    .line 931
    iput v1, p0, Lcom/google/n/j;->b:I

    .line 932
    iput v1, p0, Lcom/google/n/j;->c:I

    .line 933
    iput v1, p0, Lcom/google/n/j;->e:I

    .line 934
    iput-object p1, p0, Lcom/google/n/j;->k:Ljava/io/InputStream;

    .line 935
    iput-boolean v1, p0, Lcom/google/n/j;->i:Z

    .line 936
    return-void
.end method

.method private constructor <init>([BII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 895
    iput-boolean v1, p0, Lcom/google/n/j;->l:Z

    .line 907
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/n/j;->f:I

    .line 911
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/n/j;->h:I

    .line 914
    const/high16 v0, 0x4000000

    iput v0, p0, Lcom/google/n/j;->m:I

    .line 1087
    iput-object v2, p0, Lcom/google/n/j;->n:Lcom/google/n/k;

    .line 921
    iput-object p1, p0, Lcom/google/n/j;->a:[B

    .line 922
    add-int v0, p2, p3

    iput v0, p0, Lcom/google/n/j;->b:I

    .line 923
    iput p2, p0, Lcom/google/n/j;->c:I

    .line 924
    neg-int v0, p2

    iput v0, p0, Lcom/google/n/j;->e:I

    .line 925
    iput-object v2, p0, Lcom/google/n/j;->k:Ljava/io/InputStream;

    .line 926
    iput-boolean v1, p0, Lcom/google/n/j;->i:Z

    .line 927
    return-void
.end method

.method public static a(ILjava/io/InputStream;)I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 716
    and-int/lit16 v0, p0, 0x80

    if-nez v0, :cond_1

    .line 739
    :cond_0
    :goto_0
    return p0

    .line 720
    :cond_1
    and-int/lit8 p0, p0, 0x7f

    .line 721
    const/4 v0, 0x7

    .line 722
    :goto_1
    const/16 v1, 0x20

    if-ge v0, v1, :cond_4

    .line 723
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 724
    if-ne v1, v3, :cond_2

    .line 725
    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 727
    :cond_2
    and-int/lit8 v2, v1, 0x7f

    shl-int/2addr v2, v0

    or-int/2addr p0, v2

    .line 728
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_0

    .line 722
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 733
    :cond_3
    add-int/lit8 v0, v0, 0x7

    :cond_4
    const/16 v1, 0x40

    if-ge v0, v1, :cond_6

    .line 734
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 735
    if-ne v1, v3, :cond_5

    .line 736
    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 738
    :cond_5
    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_3

    goto :goto_0

    .line 742
    :cond_6
    invoke-static {}, Lcom/google/n/ak;->c()Lcom/google/n/ak;

    move-result-object v0

    throw v0
.end method

.method static a(Lcom/google/n/ar;)Lcom/google/n/j;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lcom/google/n/j;

    invoke-direct {v0, p0}, Lcom/google/n/j;-><init>(Lcom/google/n/ar;)V

    .line 99
    :try_start_0
    invoke-virtual {p0}, Lcom/google/n/ar;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/n/j;->a(I)I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    return-object v0

    .line 100
    :catch_0
    move-exception v0

    .line 108
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a([BII)Lcom/google/n/j;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/google/n/j;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/n/j;-><init>([BII)V

    .line 52
    :try_start_0
    invoke-virtual {v0, p2}, Lcom/google/n/j;->a(I)I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    return-object v0

    .line 53
    :catch_0
    move-exception v0

    .line 61
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private c(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1126
    :cond_0
    iget v1, p0, Lcom/google/n/j;->c:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/google/n/j;->b:I

    if-gt v1, v2, :cond_1

    .line 1127
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x4d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "refillBuffer() called when "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes were already available in buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1132
    :cond_1
    iget v1, p0, Lcom/google/n/j;->e:I

    iget v2, p0, Lcom/google/n/j;->c:I

    add-int/2addr v1, v2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/google/n/j;->f:I

    if-le v1, v2, :cond_3

    .line 1169
    :cond_2
    :goto_0
    return v0

    .line 1137
    :cond_3
    iget-object v1, p0, Lcom/google/n/j;->k:Ljava/io/InputStream;

    if-eqz v1, :cond_2

    .line 1142
    iget v1, p0, Lcom/google/n/j;->c:I

    .line 1143
    if-lez v1, :cond_5

    .line 1144
    iget v2, p0, Lcom/google/n/j;->b:I

    if-le v2, v1, :cond_4

    .line 1145
    iget-object v2, p0, Lcom/google/n/j;->a:[B

    iget-object v3, p0, Lcom/google/n/j;->a:[B

    iget v4, p0, Lcom/google/n/j;->b:I

    sub-int/2addr v4, v1

    invoke-static {v2, v1, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1147
    :cond_4
    iget v2, p0, Lcom/google/n/j;->e:I

    add-int/2addr v2, v1

    iput v2, p0, Lcom/google/n/j;->e:I

    .line 1148
    iget v2, p0, Lcom/google/n/j;->b:I

    sub-int v1, v2, v1

    iput v1, p0, Lcom/google/n/j;->b:I

    .line 1149
    iput v0, p0, Lcom/google/n/j;->c:I

    .line 1152
    :cond_5
    iget-object v1, p0, Lcom/google/n/j;->k:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/google/n/j;->a:[B

    iget v3, p0, Lcom/google/n/j;->b:I

    iget-object v4, p0, Lcom/google/n/j;->a:[B

    array-length v4, v4

    iget v5, p0, Lcom/google/n/j;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 1153
    if-eqz v1, :cond_6

    const/4 v2, -0x1

    if-lt v1, v2, :cond_6

    iget-object v2, p0, Lcom/google/n/j;->a:[B

    array-length v2, v2

    if-le v1, v2, :cond_7

    .line 1154
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x66

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "InputStream#read(byte[]) returned invalid result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nThe InputStream implementation is buggy."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1158
    :cond_7
    if-lez v1, :cond_2

    .line 1159
    iget v2, p0, Lcom/google/n/j;->b:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/n/j;->b:I

    .line 1161
    iget v1, p0, Lcom/google/n/j;->e:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/google/n/j;->m:I

    sub-int/2addr v1, v2

    if-lez v1, :cond_8

    .line 1162
    invoke-static {}, Lcom/google/n/ak;->h()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 1164
    :cond_8
    invoke-virtual {p0}, Lcom/google/n/j;->p()V

    .line 1165
    iget v1, p0, Lcom/google/n/j;->b:I

    if-lt v1, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1303
    iget v0, p0, Lcom/google/n/j;->b:I

    iget v1, p0, Lcom/google/n/j;->c:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    if-ltz p1, :cond_0

    .line 1305
    iget v0, p0, Lcom/google/n/j;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/n/j;->c:I

    .line 1309
    :goto_0
    return-void

    .line 1307
    :cond_0
    if-gez p1, :cond_1

    invoke-static {}, Lcom/google/n/ak;->b()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/n/j;->e:I

    iget v1, p0, Lcom/google/n/j;->c:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/n/j;->f:I

    if-le v0, v1, :cond_2

    iget v0, p0, Lcom/google/n/j;->f:I

    iget v1, p0, Lcom/google/n/j;->e:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/n/j;->c:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/n/j;->d(I)V

    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_2
    iget v0, p0, Lcom/google/n/j;->b:I

    iget v1, p0, Lcom/google/n/j;->c:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/n/j;->b:I

    iput v1, p0, Lcom/google/n/j;->c:I

    invoke-direct {p0, v3}, Lcom/google/n/j;->c(I)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_3
    sub-int v1, p1, v0

    iget v2, p0, Lcom/google/n/j;->b:I

    if-le v1, v2, :cond_4

    iget v1, p0, Lcom/google/n/j;->b:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/n/j;->b:I

    iput v1, p0, Lcom/google/n/j;->c:I

    invoke-direct {p0, v3}, Lcom/google/n/j;->c(I)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_4
    sub-int v0, p1, v0

    iput v0, p0, Lcom/google/n/j;->c:I

    goto :goto_0
.end method

.method private q()J
    .locals 6

    .prologue
    .line 807
    const-wide/16 v2, 0x0

    .line 808
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x40

    if-ge v0, v1, :cond_2

    .line 809
    iget v1, p0, Lcom/google/n/j;->c:I

    iget v4, p0, Lcom/google/n/j;->b:I

    if-ne v1, v4, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/n/j;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/n/j;->a:[B

    iget v4, p0, Lcom/google/n/j;->c:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/n/j;->c:I

    aget-byte v1, v1, v4

    .line 810
    and-int/lit8 v4, v1, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v0

    or-long/2addr v2, v4

    .line 811
    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_1

    .line 812
    return-wide v2

    .line 808
    :cond_1
    add-int/lit8 v0, v0, 0x7

    goto :goto_0

    .line 815
    :cond_2
    invoke-static {}, Lcom/google/n/ak;->c()Lcom/google/n/ak;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 121
    iget v2, p0, Lcom/google/n/j;->c:I

    iget v3, p0, Lcom/google/n/j;->b:I

    if-ne v2, v3, :cond_0

    invoke-direct {p0, v1}, Lcom/google/n/j;->c(I)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    if-eqz v1, :cond_1

    .line 122
    iput v0, p0, Lcom/google/n/j;->d:I

    .line 132
    :goto_1
    return v0

    :cond_0
    move v1, v0

    .line 121
    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/n/j;->d:I

    .line 127
    iget v0, p0, Lcom/google/n/j;->d:I

    invoke-static {v0}, Lcom/google/n/bt;->b(I)I

    move-result v0

    if-nez v0, :cond_2

    .line 130
    invoke-static {}, Lcom/google/n/ak;->d()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 132
    :cond_2
    iget v0, p0, Lcom/google/n/j;->d:I

    goto :goto_1
.end method

.method public final a(I)I
    .locals 2

    .prologue
    .line 1016
    if-gez p1, :cond_0

    .line 1017
    invoke-static {}, Lcom/google/n/ak;->b()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 1019
    :cond_0
    iget v0, p0, Lcom/google/n/j;->e:I

    iget v1, p0, Lcom/google/n/j;->c:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    .line 1020
    iget v1, p0, Lcom/google/n/j;->f:I

    .line 1021
    if-le v0, v1, :cond_1

    .line 1022
    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 1024
    :cond_1
    iput v0, p0, Lcom/google/n/j;->f:I

    .line 1026
    invoke-virtual {p0}, Lcom/google/n/j;->p()V

    .line 1028
    return v1
.end method

.method public final a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">(I",
            "Lcom/google/n/ax",
            "<TT;>;",
            "Lcom/google/n/o;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 448
    iget v0, p0, Lcom/google/n/j;->g:I

    iget v1, p0, Lcom/google/n/j;->h:I

    if-lt v0, v1, :cond_0

    .line 449
    invoke-static {}, Lcom/google/n/ak;->g()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 451
    :cond_0
    iget v0, p0, Lcom/google/n/j;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/n/j;->g:I

    .line 452
    invoke-interface {p2, p0, p3}, Lcom/google/n/ax;->a(Lcom/google/n/j;Lcom/google/n/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 453
    const/4 v1, 0x4

    .line 454
    invoke-static {p1, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    .line 453
    iget v2, p0, Lcom/google/n/j;->d:I

    if-eq v2, v1, :cond_1

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 455
    :cond_1
    iget v1, p0, Lcom/google/n/j;->g:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/n/j;->g:I

    .line 456
    return-object v0
.end method

.method public final a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/n/ax",
            "<TT;>;",
            "Lcom/google/n/o;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v0

    .line 521
    iget v1, p0, Lcom/google/n/j;->g:I

    iget v2, p0, Lcom/google/n/j;->h:I

    if-lt v1, v2, :cond_0

    .line 522
    invoke-static {}, Lcom/google/n/ak;->g()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 524
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/n/j;->a(I)I

    move-result v1

    .line 525
    iget v0, p0, Lcom/google/n/j;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/n/j;->g:I

    .line 526
    invoke-interface {p1, p0, p2}, Lcom/google/n/ax;->a(Lcom/google/n/j;Lcom/google/n/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 527
    const/4 v2, 0x0

    iget v3, p0, Lcom/google/n/j;->d:I

    if-eq v3, v2, :cond_1

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 528
    :cond_1
    iget v2, p0, Lcom/google/n/j;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/n/j;->g:I

    .line 529
    iput v1, p0, Lcom/google/n/j;->f:I

    invoke-virtual {p0}, Lcom/google/n/j;->p()V

    .line 530
    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/google/n/j;->m()J

    move-result-wide v0

    return-wide v0
.end method

.method b(I)[B
    .locals 11

    .prologue
    const/16 v10, 0x1000

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 1206
    if-gtz p1, :cond_1

    .line 1207
    if-nez p1, :cond_0

    .line 1208
    sget-object v0, Lcom/google/n/af;->a:[B

    .line 1292
    :goto_0
    return-object v0

    .line 1210
    :cond_0
    invoke-static {}, Lcom/google/n/ak;->b()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 1214
    :cond_1
    iget v0, p0, Lcom/google/n/j;->e:I

    iget v2, p0, Lcom/google/n/j;->c:I

    add-int/2addr v0, v2

    add-int/2addr v0, p1

    iget v2, p0, Lcom/google/n/j;->f:I

    if-le v0, v2, :cond_2

    .line 1216
    iget v0, p0, Lcom/google/n/j;->f:I

    iget v1, p0, Lcom/google/n/j;->e:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/n/j;->c:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/n/j;->d(I)V

    .line 1218
    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 1221
    :cond_2
    if-ge p1, v10, :cond_4

    .line 1226
    new-array v0, p1, [B

    .line 1227
    iget v2, p0, Lcom/google/n/j;->b:I

    iget v3, p0, Lcom/google/n/j;->c:I

    sub-int/2addr v2, v3

    .line 1228
    iget-object v3, p0, Lcom/google/n/j;->a:[B

    iget v4, p0, Lcom/google/n/j;->c:I

    invoke-static {v3, v4, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1229
    iget v3, p0, Lcom/google/n/j;->b:I

    iput v3, p0, Lcom/google/n/j;->c:I

    .line 1234
    sub-int v3, p1, v2

    iget v4, p0, Lcom/google/n/j;->b:I

    iget v5, p0, Lcom/google/n/j;->c:I

    sub-int/2addr v4, v5

    if-ge v4, v3, :cond_3

    invoke-direct {p0, v3}, Lcom/google/n/j;->c(I)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 1235
    :cond_3
    iget-object v3, p0, Lcom/google/n/j;->a:[B

    sub-int v4, p1, v2

    invoke-static {v3, v1, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1236
    sub-int v1, p1, v2

    iput v1, p0, Lcom/google/n/j;->c:I

    goto :goto_0

    .line 1250
    :cond_4
    iget v5, p0, Lcom/google/n/j;->c:I

    .line 1251
    iget v6, p0, Lcom/google/n/j;->b:I

    .line 1254
    iget v0, p0, Lcom/google/n/j;->e:I

    iget v2, p0, Lcom/google/n/j;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/n/j;->e:I

    .line 1255
    iput v1, p0, Lcom/google/n/j;->c:I

    .line 1256
    iput v1, p0, Lcom/google/n/j;->b:I

    .line 1259
    sub-int v0, v6, v5

    sub-int v0, p1, v0

    .line 1260
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v4, v0

    .line 1262
    :goto_1
    if-lez v4, :cond_8

    .line 1263
    invoke-static {v4, v10}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v8, v0, [B

    move v0, v1

    .line 1265
    :goto_2
    array-length v2, v8

    if-ge v0, v2, :cond_7

    .line 1266
    iget-object v2, p0, Lcom/google/n/j;->k:Ljava/io/InputStream;

    if-nez v2, :cond_5

    move v2, v3

    .line 1268
    :goto_3
    if-ne v2, v3, :cond_6

    .line 1269
    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 1266
    :cond_5
    iget-object v2, p0, Lcom/google/n/j;->k:Ljava/io/InputStream;

    array-length v9, v8

    sub-int/2addr v9, v0

    .line 1267
    invoke-virtual {v2, v8, v0, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    goto :goto_3

    .line 1271
    :cond_6
    iget v9, p0, Lcom/google/n/j;->e:I

    add-int/2addr v9, v2

    iput v9, p0, Lcom/google/n/j;->e:I

    .line 1272
    add-int/2addr v0, v2

    .line 1273
    goto :goto_2

    .line 1274
    :cond_7
    array-length v0, v8

    sub-int v0, v4, v0

    .line 1275
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, v0

    .line 1276
    goto :goto_1

    .line 1279
    :cond_8
    new-array v3, p1, [B

    .line 1282
    sub-int v0, v6, v5

    .line 1283
    iget-object v2, p0, Lcom/google/n/j;->a:[B

    invoke-static {v2, v5, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1286
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 1287
    array-length v5, v0

    invoke-static {v0, v1, v3, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1288
    array-length v0, v0

    add-int/2addr v0, v2

    move v2, v0

    .line 1289
    goto :goto_4

    :cond_9
    move-object v0, v3

    .line 1292
    goto/16 :goto_0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/n/j;->m()J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 5

    .prologue
    .line 364
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v1

    .line 365
    iget v0, p0, Lcom/google/n/j;->b:I

    iget v2, p0, Lcom/google/n/j;->c:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_0

    if-lez v1, :cond_0

    .line 368
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/n/j;->a:[B

    iget v3, p0, Lcom/google/n/j;->c:I

    const-string v4, "UTF-8"

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 369
    iget v2, p0, Lcom/google/n/j;->c:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/n/j;->c:I

    .line 375
    :goto_0
    return-object v0

    .line 371
    :cond_0
    if-nez v1, :cond_1

    .line 372
    const-string v0, ""

    goto :goto_0

    .line 375
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/n/j;->b(I)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_0
.end method

.method public final g()Lcom/google/n/f;
    .locals 3

    .prologue
    .line 535
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v1

    .line 536
    iget v0, p0, Lcom/google/n/j;->b:I

    iget v2, p0, Lcom/google/n/j;->c:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_0

    if-lez v1, :cond_0

    .line 539
    iget-boolean v0, p0, Lcom/google/n/j;->i:Z

    iget-object v0, p0, Lcom/google/n/j;->a:[B

    iget v2, p0, Lcom/google/n/j;->c:I

    .line 541
    invoke-static {v0, v2, v1}, Lcom/google/n/f;->a([BII)Lcom/google/n/f;

    move-result-object v0

    .line 542
    iget v2, p0, Lcom/google/n/j;->c:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/n/j;->c:I

    .line 548
    :goto_0
    return-object v0

    .line 544
    :cond_0
    if-nez v1, :cond_1

    .line 545
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    goto :goto_0

    .line 548
    :cond_1
    new-instance v0, Lcom/google/n/ar;

    invoke-virtual {p0, v1}, Lcom/google/n/j;->b(I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/n/ar;-><init>([B)V

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 616
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v0

    ushr-int/lit8 v1, v0, 0x1

    and-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    xor-int/2addr v0, v1

    return v0
.end method

.method public final k()J
    .locals 6

    .prologue
    .line 621
    invoke-virtual {p0}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const/4 v2, 0x1

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x1

    and-long/2addr v0, v4

    neg-long v0, v0

    xor-long/2addr v0, v2

    return-wide v0
.end method

.method public final l()I
    .locals 5

    .prologue
    .line 633
    iget v0, p0, Lcom/google/n/j;->c:I

    .line 635
    iget v1, p0, Lcom/google/n/j;->b:I

    if-eq v1, v0, :cond_5

    .line 636
    iget-object v3, p0, Lcom/google/n/j;->a:[B

    .line 641
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, v3, v0

    if-ltz v0, :cond_0

    .line 642
    iput v2, p0, Lcom/google/n/j;->c:I

    .line 668
    :goto_0
    return v0

    .line 644
    :cond_0
    iget v1, p0, Lcom/google/n/j;->b:I

    sub-int/2addr v1, v2

    const/16 v4, 0x9

    if-lt v1, v4, :cond_5

    .line 645
    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    shl-int/lit8 v2, v2, 0x7

    xor-int/2addr v0, v2

    if-gez v0, :cond_2

    .line 647
    xor-int/lit8 v0, v0, -0x80

    .line 662
    :cond_1
    :goto_1
    iput v1, p0, Lcom/google/n/j;->c:I

    goto :goto_0

    .line 648
    :cond_2
    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v3, v1

    shl-int/lit8 v1, v1, 0xe

    xor-int/2addr v0, v1

    if-ltz v0, :cond_3

    .line 649
    xor-int/lit16 v0, v0, 0x3f80

    move v1, v2

    goto :goto_1

    .line 650
    :cond_3
    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    shl-int/lit8 v2, v2, 0x15

    xor-int/2addr v0, v2

    if-gez v0, :cond_4

    .line 651
    const v2, -0x1fc080

    xor-int/2addr v0, v2

    goto :goto_1

    .line 653
    :cond_4
    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v3, v1

    .line 654
    shl-int/lit8 v4, v1, 0x1c

    xor-int/2addr v0, v4

    .line 655
    const v4, 0xfe03f80

    xor-int/2addr v0, v4

    .line 656
    if-gez v1, :cond_6

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    if-gez v2, :cond_1

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v3, v1

    if-gez v1, :cond_6

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    if-gez v2, :cond_1

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v3, v1

    if-gez v1, :cond_6

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    if-gez v2, :cond_1

    .line 668
    :cond_5
    invoke-direct {p0}, Lcom/google/n/j;->q()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method public final m()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 759
    iget v0, p0, Lcom/google/n/j;->c:I

    .line 761
    iget v1, p0, Lcom/google/n/j;->b:I

    if-eq v1, v0, :cond_9

    .line 762
    iget-object v4, p0, Lcom/google/n/j;->a:[B

    .line 768
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, v4, v0

    if-ltz v0, :cond_0

    .line 769
    iput v1, p0, Lcom/google/n/j;->c:I

    .line 770
    int-to-long v0, v0

    .line 801
    :goto_0
    return-wide v0

    .line 771
    :cond_0
    iget v2, p0, Lcom/google/n/j;->b:I

    sub-int/2addr v2, v1

    const/16 v3, 0x9

    if-lt v2, v3, :cond_9

    .line 772
    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v4, v1

    shl-int/lit8 v1, v1, 0x7

    xor-int/2addr v0, v1

    if-gez v0, :cond_2

    .line 774
    xor-int/lit8 v0, v0, -0x80

    int-to-long v0, v0

    .line 794
    :cond_1
    :goto_1
    iput v2, p0, Lcom/google/n/j;->c:I

    goto :goto_0

    .line 775
    :cond_2
    add-int/lit8 v3, v2, 0x1

    aget-byte v1, v4, v2

    shl-int/lit8 v1, v1, 0xe

    xor-int/2addr v0, v1

    if-ltz v0, :cond_3

    .line 776
    xor-int/lit16 v0, v0, 0x3f80

    int-to-long v0, v0

    move v2, v3

    goto :goto_1

    .line 777
    :cond_3
    add-int/lit8 v2, v3, 0x1

    aget-byte v1, v4, v3

    shl-int/lit8 v1, v1, 0x15

    xor-int/2addr v0, v1

    if-gez v0, :cond_4

    .line 778
    const v1, -0x1fc080

    xor-int/2addr v0, v1

    int-to-long v0, v0

    goto :goto_1

    .line 779
    :cond_4
    int-to-long v0, v0

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v4, v2

    int-to-long v6, v2

    const/16 v2, 0x1c

    shl-long/2addr v6, v2

    xor-long/2addr v0, v6

    cmp-long v2, v0, v8

    if-ltz v2, :cond_5

    .line 780
    const-wide/32 v4, 0xfe03f80

    xor-long/2addr v0, v4

    move v2, v3

    goto :goto_1

    .line 781
    :cond_5
    add-int/lit8 v2, v3, 0x1

    aget-byte v3, v4, v3

    int-to-long v6, v3

    const/16 v3, 0x23

    shl-long/2addr v6, v3

    xor-long/2addr v0, v6

    cmp-long v3, v0, v8

    if-gez v3, :cond_6

    .line 782
    const-wide v4, -0x7f01fc080L

    xor-long/2addr v0, v4

    goto :goto_1

    .line 783
    :cond_6
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v4, v2

    int-to-long v6, v2

    const/16 v2, 0x2a

    shl-long/2addr v6, v2

    xor-long/2addr v0, v6

    cmp-long v2, v0, v8

    if-ltz v2, :cond_7

    .line 784
    const-wide v4, 0x3f80fe03f80L

    xor-long/2addr v0, v4

    move v2, v3

    goto :goto_1

    .line 785
    :cond_7
    add-int/lit8 v2, v3, 0x1

    aget-byte v3, v4, v3

    int-to-long v6, v3

    const/16 v3, 0x31

    shl-long/2addr v6, v3

    xor-long/2addr v0, v6

    cmp-long v3, v0, v8

    if-gez v3, :cond_8

    .line 786
    const-wide v4, -0x1fc07f01fc080L

    xor-long/2addr v0, v4

    goto :goto_1

    .line 789
    :cond_8
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v4, v2

    int-to-long v6, v2

    const/16 v2, 0x38

    shl-long/2addr v6, v2

    xor-long/2addr v0, v6

    .line 790
    const-wide v6, 0xfe03f80fe03f80L

    xor-long/2addr v0, v6

    .line 792
    cmp-long v2, v0, v8

    if-gez v2, :cond_a

    .line 793
    add-int/lit8 v2, v3, 0x1

    aget-byte v3, v4, v3

    int-to-long v4, v3

    cmp-long v3, v4, v8

    if-gez v3, :cond_1

    .line 801
    :cond_9
    invoke-direct {p0}, Lcom/google/n/j;->q()J

    move-result-wide v0

    goto/16 :goto_0

    :cond_a
    move v2, v3

    goto/16 :goto_1
.end method

.method public final n()I
    .locals 4

    .prologue
    const/4 v2, 0x4

    .line 820
    iget v0, p0, Lcom/google/n/j;->c:I

    .line 823
    iget v1, p0, Lcom/google/n/j;->b:I

    sub-int/2addr v1, v0

    if-ge v1, v2, :cond_1

    .line 824
    invoke-direct {p0, v2}, Lcom/google/n/j;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 825
    :cond_0
    iget v0, p0, Lcom/google/n/j;->c:I

    .line 828
    :cond_1
    iget-object v1, p0, Lcom/google/n/j;->a:[B

    .line 829
    add-int/lit8 v2, v0, 0x4

    iput v2, p0, Lcom/google/n/j;->c:I

    .line 830
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    add-int/lit8 v3, v0, 0x2

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x3

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v2

    return v0
.end method

.method public final o()J
    .locals 10

    .prologue
    const/16 v6, 0x8

    const-wide/16 v8, 0xff

    .line 838
    iget v0, p0, Lcom/google/n/j;->c:I

    .line 841
    iget v1, p0, Lcom/google/n/j;->b:I

    sub-int/2addr v1, v0

    if-ge v1, v6, :cond_1

    .line 842
    invoke-direct {p0, v6}, Lcom/google/n/j;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/n/ak;->a()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 843
    :cond_0
    iget v0, p0, Lcom/google/n/j;->c:I

    .line 846
    :cond_1
    iget-object v1, p0, Lcom/google/n/j;->a:[B

    .line 847
    add-int/lit8 v2, v0, 0x8

    iput v2, p0, Lcom/google/n/j;->c:I

    .line 848
    aget-byte v2, v1, v0

    int-to-long v2, v2

    and-long/2addr v2, v8

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x10

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x3

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x18

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x4

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x20

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x5

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x28

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x6

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x30

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v0, v0, 0x7

    aget-byte v0, v1, v0

    int-to-long v0, v0

    and-long/2addr v0, v8

    const/16 v4, 0x38

    shl-long/2addr v0, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public p()V
    .locals 2

    .prologue
    .line 1032
    iget v0, p0, Lcom/google/n/j;->b:I

    iget v1, p0, Lcom/google/n/j;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/j;->b:I

    .line 1033
    iget v0, p0, Lcom/google/n/j;->e:I

    iget v1, p0, Lcom/google/n/j;->b:I

    add-int/2addr v0, v1

    .line 1034
    iget v1, p0, Lcom/google/n/j;->f:I

    if-le v0, v1, :cond_0

    .line 1036
    iget v1, p0, Lcom/google/n/j;->f:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/j;->j:I

    .line 1037
    iget v0, p0, Lcom/google/n/j;->b:I

    iget v1, p0, Lcom/google/n/j;->j:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/j;->b:I

    .line 1041
    :goto_0
    return-void

    .line 1039
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/n/j;->j:I

    goto :goto_0
.end method
