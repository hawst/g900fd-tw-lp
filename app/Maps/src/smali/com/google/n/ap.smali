.class public Lcom/google/n/ap;
.super Ljava/util/AbstractList;
.source "PG"

# interfaces
.implements Lcom/google/n/aq;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/google/n/aq;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/n/aq;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/google/n/ap;

    invoke-direct {v0}, Lcom/google/n/ap;-><init>()V

    .line 41
    new-instance v1, Lcom/google/n/bp;

    invoke-direct {v1, v0}, Lcom/google/n/bp;-><init>(Lcom/google/n/aq;)V

    sput-object v1, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/google/n/aq;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Lcom/google/n/aq;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    .line 55
    invoke-virtual {p0, p1}, Lcom/google/n/ap;->addAll(Ljava/util/Collection;)Z

    .line 56
    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 193
    check-cast p0, Ljava/lang/String;

    .line 197
    :goto_0
    return-object p0

    .line 194
    :cond_0
    instance-of v0, p0, Lcom/google/n/f;

    if-eqz v0, :cond_1

    .line 195
    check-cast p0, Lcom/google/n/f;

    invoke-virtual {p0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 197
    :cond_1
    check-cast p0, [B

    invoke-static {p0}, Lcom/google/n/af;->b([B)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/n/f;
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 163
    instance-of v0, v1, Lcom/google/n/f;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/google/n/f;

    .line 164
    :goto_0
    if-eq v0, v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 167
    :cond_0
    return-object v0

    .line 163
    :cond_1
    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    check-cast v0, [B

    invoke-static {v0}, Lcom/google/n/f;->a([B)Lcom/google/n/f;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 37
    check-cast p2, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget v0, p0, Lcom/google/n/ap;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/n/ap;->modCount:I

    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 114
    instance-of v0, p2, Lcom/google/n/aq;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/n/aq;

    .line 115
    invoke-interface {p2}, Lcom/google/n/aq;->a()Ljava/util/List;

    move-result-object p2

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 117
    iget v1, p0, Lcom/google/n/ap;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/n/ap;->modCount:I

    .line 118
    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/n/ap;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/n/ap;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/google/n/aq;
    .locals 1

    .prologue
    .line 340
    new-instance v0, Lcom/google/n/bp;

    invoke-direct {v0, p0}, Lcom/google/n/bp;-><init>(Lcom/google/n/aq;)V

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 145
    iget v0, p0, Lcom/google/n/ap;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/n/ap;->modCount:I

    .line 146
    return-void
.end method

.method public synthetic get(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    instance-of v1, v0, Lcom/google/n/f;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    check-cast v0, [B

    invoke-static {v0}, Lcom/google/n/af;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/n/af;->a([B)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public synthetic remove(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/google/n/ap;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/n/ap;->modCount:I

    invoke-static {v0}, Lcom/google/n/ap;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    check-cast p2, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/n/ap;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/n/ap;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
