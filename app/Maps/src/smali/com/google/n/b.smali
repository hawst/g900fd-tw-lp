.class public abstract Lcom/google/n/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/n/au;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<BuilderType:",
        "Lcom/google/n/b;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/n/au;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    return-void
.end method

.method private static a(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 326
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 327
    if-nez v1, :cond_0

    .line 328
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 331
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Iterable;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TT;>;",
            "Ljava/util/Collection",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 307
    instance-of v0, p0, Lcom/google/n/aq;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 310
    check-cast v0, Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/n/b;->a(Ljava/lang/Iterable;)V

    .line 311
    check-cast p0, Ljava/util/Collection;

    invoke-interface {p1, p0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_2

    .line 313
    invoke-static {p0}, Lcom/google/n/b;->a(Ljava/lang/Iterable;)V

    .line 314
    check-cast p0, Ljava/util/Collection;

    invoke-interface {p1, p0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 316
    :cond_2
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 317
    if-nez v1, :cond_3

    .line 318
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 320
    :cond_3
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/n/f;)Lcom/google/n/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/f;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 116
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/f;->f()Lcom/google/n/j;

    move-result-object v0

    .line 117
    invoke-static {}, Lcom/google/n/o;->a()Lcom/google/n/o;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/n/b;->a(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/b;

    .line 118
    const/4 v1, 0x0

    iget v0, v0, Lcom/google/n/j;->d:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    throw v0

    .line 122
    :catch_1
    move-exception v0

    .line 123
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 119
    :cond_0
    return-object p0
.end method

.method public abstract a(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/j;",
            "Lcom/google/n/o;",
            ")TBuilderType;"
        }
    .end annotation
.end method

.method public final a([BII)Lcom/google/n/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII)TBuilderType;"
        }
    .end annotation

    .prologue
    .line 156
    .line 157
    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/n/j;->a([BII)Lcom/google/n/j;

    move-result-object v0

    .line 158
    invoke-static {}, Lcom/google/n/o;->a()Lcom/google/n/o;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/n/b;->a(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/b;

    .line 159
    const/4 v1, 0x0

    iget v0, v0, Lcom/google/n/j;->d:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 161
    :catch_0
    move-exception v0

    .line 162
    throw v0

    .line 163
    :catch_1
    move-exception v0

    .line 164
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 160
    :cond_0
    return-object p0
.end method

.method public synthetic b(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/au;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0, p1, p2}, Lcom/google/n/b;->a(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/n/b;->e()Lcom/google/n/b;

    move-result-object v0

    return-object v0
.end method

.method public abstract e()Lcom/google/n/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation
.end method
