.class public final Lcom/google/n/l;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/io/OutputStream;

.field private final b:[B

.field private final c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;[B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput v0, p0, Lcom/google/n/l;->e:I

    .line 58
    iput-object p1, p0, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    .line 59
    iput-object p2, p0, Lcom/google/n/l;->b:[B

    .line 60
    iput v0, p0, Lcom/google/n/l;->d:I

    .line 61
    array-length v0, p2

    iput v0, p0, Lcom/google/n/l;->c:I

    .line 62
    return-void
.end method

.method constructor <init>([BII)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/n/l;->e:I

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    .line 52
    iput-object p1, p0, Lcom/google/n/l;->b:[B

    .line 53
    iput p2, p0, Lcom/google/n/l;->d:I

    .line 54
    add-int v0, p2, p3

    iput v0, p0, Lcom/google/n/l;->c:I

    .line 55
    return-void
.end method

.method public static a(Lcom/google/n/ao;)I
    .locals 2

    .prologue
    .line 892
    iget-boolean v0, p0, Lcom/google/n/ao;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    .line 893
    :goto_0
    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 892
    :cond_0
    iget-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 849
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 850
    array-length v1, v0

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v1

    return v0

    .line 852
    :catch_0
    move-exception v0

    .line 853
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "UTF-8 not supported."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static b(J)I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1250
    const-wide/16 v0, -0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1259
    :goto_0
    return v0

    .line 1251
    :cond_0
    const-wide/16 v0, -0x4000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 1252
    :cond_1
    const-wide/32 v0, -0x200000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 1253
    :cond_2
    const-wide/32 v0, -0x10000000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 1254
    :cond_3
    const-wide v0, -0x800000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const/4 v0, 0x5

    goto :goto_0

    .line 1255
    :cond_4
    const-wide v0, -0x40000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    const/4 v0, 0x6

    goto :goto_0

    .line 1256
    :cond_5
    const-wide/high16 v0, -0x2000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    const/4 v0, 0x7

    goto :goto_0

    .line 1257
    :cond_6
    const-wide/high16 v0, -0x100000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    const/16 v0, 0x8

    goto :goto_0

    .line 1258
    :cond_7
    const-wide/high16 v0, -0x8000000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    const/16 v0, 0x9

    goto :goto_0

    .line 1259
    :cond_8
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 1228
    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1232
    :goto_0
    return v0

    .line 1229
    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 1230
    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 1231
    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 1232
    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 976
    iget-object v0, p0, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 978
    new-instance v0, Lcom/google/n/m;

    invoke-direct {v0}, Lcom/google/n/m;-><init>()V

    throw v0

    .line 983
    :cond_0
    iget-object v0, p0, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/n/l;->b:[B

    iget v2, p0, Lcom/google/n/l;->d:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 984
    iput v3, p0, Lcom/google/n/l;->d:I

    .line 985
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 1060
    int-to-byte v0, p1

    iget v1, p0, Lcom/google/n/l;->d:I

    iget v2, p0, Lcom/google/n/l;->c:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/n/l;->a()V

    :cond_0
    iget-object v1, p0, Lcom/google/n/l;->b:[B

    iget v2, p0, Lcom/google/n/l;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/n/l;->d:I

    aput-byte v0, v1, v2

    iget v0, p0, Lcom/google/n/l;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/n/l;->e:I

    .line 1061
    return-void
.end method

.method public final a(ID)V
    .locals 2

    .prologue
    .line 142
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 143
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->c(J)V

    .line 144
    return-void
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x5

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 150
    invoke-static {p2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->d(I)V

    .line 151
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 171
    if-ltz p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/n/l;->b(I)V

    .line 172
    :goto_0
    return-void

    .line 171
    :cond_0
    int-to-long v0, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 157
    invoke-virtual {p0, p2, p3}, Lcom/google/n/l;->a(J)V

    .line 158
    return-void
.end method

.method public final a(ILcom/google/n/at;)V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 206
    invoke-interface {p2, p0}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 207
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 208
    return-void
.end method

.method public final a(ILcom/google/n/f;)V
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 261
    invoke-virtual {p2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p0, p2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 262
    return-void
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 191
    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/n/l;->b(I)V

    .line 192
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 193
    return-void
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 1238
    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1239
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1240
    return-void

    .line 1242
    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1243
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/n/f;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1065
    invoke-virtual {p1}, Lcom/google/n/f;->b()I

    move-result v0

    iget v1, p0, Lcom/google/n/l;->c:I

    iget v2, p0, Lcom/google/n/l;->d:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/n/l;->b:[B

    iget v2, p0, Lcom/google/n/l;->d:I

    invoke-virtual {p1, v1, v4, v2, v0}, Lcom/google/n/f;->a([BIII)V

    iget v1, p0, Lcom/google/n/l;->d:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/n/l;->d:I

    iget v1, p0, Lcom/google/n/l;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/l;->e:I

    .line 1066
    :goto_0
    return-void

    .line 1065
    :cond_0
    iget v1, p0, Lcom/google/n/l;->c:I

    iget v2, p0, Lcom/google/n/l;->d:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/n/l;->b:[B

    iget v3, p0, Lcom/google/n/l;->d:I

    invoke-virtual {p1, v2, v4, v3, v1}, Lcom/google/n/f;->a([BIII)V

    add-int v2, v4, v1

    sub-int/2addr v0, v1

    iget v3, p0, Lcom/google/n/l;->c:I

    iput v3, p0, Lcom/google/n/l;->d:I

    iget v3, p0, Lcom/google/n/l;->e:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/n/l;->e:I

    invoke-virtual {p0}, Lcom/google/n/l;->a()V

    iget v1, p0, Lcom/google/n/l;->c:I

    if-gt v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/n/l;->b:[B

    invoke-virtual {p1, v1, v2, v4, v0}, Lcom/google/n/f;->a([BIII)V

    iput v0, p0, Lcom/google/n/l;->d:I

    :cond_1
    :goto_1
    iget v1, p0, Lcom/google/n/l;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/l;->e:I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    if-gez v2, :cond_3

    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Source offset < 0: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-gez v0, :cond_4

    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Length < 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    add-int v3, v2, v0

    invoke-virtual {p1}, Lcom/google/n/f;->b()I

    move-result v4

    if-le v3, v4, :cond_5

    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    add-int/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Source end offset exceeded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    if-lez v0, :cond_1

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/n/f;->a(Ljava/io/OutputStream;II)V

    goto :goto_1
.end method

.method public final a([B)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1070
    array-length v0, p1

    iget v1, p0, Lcom/google/n/l;->c:I

    iget v2, p0, Lcom/google/n/l;->d:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/n/l;->b:[B

    iget v2, p0, Lcom/google/n/l;->d:I

    invoke-static {p1, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lcom/google/n/l;->d:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/n/l;->d:I

    iget v1, p0, Lcom/google/n/l;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/l;->e:I

    .line 1071
    :goto_0
    return-void

    .line 1070
    :cond_0
    iget v1, p0, Lcom/google/n/l;->c:I

    iget v2, p0, Lcom/google/n/l;->d:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/n/l;->b:[B

    iget v3, p0, Lcom/google/n/l;->d:I

    invoke-static {p1, v4, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int v2, v4, v1

    sub-int/2addr v0, v1

    iget v3, p0, Lcom/google/n/l;->c:I

    iput v3, p0, Lcom/google/n/l;->d:I

    iget v3, p0, Lcom/google/n/l;->e:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/n/l;->e:I

    invoke-virtual {p0}, Lcom/google/n/l;->a()V

    iget v1, p0, Lcom/google/n/l;->c:I

    if-gt v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/n/l;->b:[B

    invoke-static {p1, v2, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v0, p0, Lcom/google/n/l;->d:I

    :goto_1
    iget v1, p0, Lcom/google/n/l;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/n/l;->e:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, v2, v0}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/n/l;->c:I

    iget v1, p0, Lcom/google/n/l;->d:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 1020
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1019
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1023
    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1212
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    .line 1213
    invoke-virtual {p0, p1}, Lcom/google/n/l;->a(I)V

    .line 1214
    return-void

    .line 1216
    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1217
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 310
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 311
    if-ltz p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/n/l;->b(I)V

    .line 312
    :goto_0
    return-void

    .line 311
    :cond_0
    int-to-long v0, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0
.end method

.method public final b(IJ)V
    .locals 2

    .prologue
    .line 163
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 164
    invoke-virtual {p0, p2, p3}, Lcom/google/n/l;->a(J)V

    .line 165
    return-void
.end method

.method public final b(ILcom/google/n/at;)V
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 241
    invoke-interface {p2}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    invoke-interface {p2, p0}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 242
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 331
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 332
    shl-int/lit8 v0, p2, 0x1

    shr-int/lit8 v1, p2, 0x1f

    xor-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 333
    return-void
.end method

.method public final c(IJ)V
    .locals 2

    .prologue
    .line 177
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 178
    invoke-virtual {p0, p2, p3}, Lcom/google/n/l;->c(J)V

    .line 179
    return-void
.end method

.method public final c(J)V
    .locals 3

    .prologue
    .line 1274
    long-to-int v0, p1

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1275
    const/16 v0, 0x8

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1276
    const/16 v0, 0x10

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1277
    const/16 v0, 0x18

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1278
    const/16 v0, 0x20

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1279
    const/16 v0, 0x28

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1280
    const/16 v0, 0x30

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1281
    const/16 v0, 0x38

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1282
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 1264
    and-int/lit16 v0, p1, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1265
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1266
    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1267
    ushr-int/lit8 v0, p1, 0x18

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    .line 1268
    return-void
.end method

.method public final d(IJ)V
    .locals 4

    .prologue
    .line 338
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 339
    const/4 v0, 0x1

    shl-long v0, p2, v0

    const/16 v2, 0x3f

    shr-long v2, p2, v2

    xor-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    .line 340
    return-void
.end method
