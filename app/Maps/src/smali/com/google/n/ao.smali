.class public Lcom/google/n/ao;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public b:Lcom/google/n/f;

.field public c:Lcom/google/n/o;

.field public volatile d:Z

.field public volatile e:Lcom/google/n/at;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/n/ao;->d:Z

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/google/n/o;Lcom/google/n/f;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/n/ao;->d:Z

    .line 24
    iput-object p1, p0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 25
    iput-object p2, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    .line 26
    return-void
.end method

.method public static a(Lcom/google/n/at;)Lcom/google/n/ao;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    .line 33
    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p0, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 34
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/n/ao;)V
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/google/n/ao;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    if-nez v0, :cond_1

    .line 78
    iget-object v0, p1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    .line 82
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/n/ao;->d:Z

    goto :goto_0

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    invoke-virtual {p1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/f;->a(Lcom/google/n/f;)Lcom/google/n/f;

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/n/at;)Lcom/google/n/at;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    if-nez v0, :cond_0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    if-eqz v0, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    return-object v0

    .line 56
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/google/n/at;->a()Lcom/google/n/ax;

    move-result-object v0

    iget-object v1, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iget-object v2, p0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    invoke-interface {v0, v1, v2}, Lcom/google/n/ax;->a(Lcom/google/n/f;Lcom/google/n/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    iput-object v0, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_2
    :try_start_3
    iput-object p1, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final b()Lcom/google/n/f;
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/n/ao;->d:Z

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    .line 121
    :goto_0
    return-object v0

    .line 111
    :cond_0
    monitor-enter p0

    .line 112
    :try_start_0
    iget-boolean v0, p0, Lcom/google/n/ao;->d:Z

    if-nez v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    monitor-exit p0

    goto :goto_0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 115
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    if-nez v0, :cond_2

    .line 116
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    .line 120
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/n/ao;->d:Z

    .line 121
    iget-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    monitor-exit p0

    goto :goto_0

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    invoke-interface {v0}, Lcom/google/n/at;->k()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/ao;->b:Lcom/google/n/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
