.class public Lcom/google/n/ai;
.super Ljava/util/AbstractList;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TF;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<TF;TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/n/aj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TF;>;",
            "Lcom/google/n/aj",
            "<TF;TT;>;)V"
        }
    .end annotation

    .prologue
    .line 419
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 420
    iput-object p1, p0, Lcom/google/n/ai;->a:Ljava/util/List;

    .line 421
    iput-object p2, p0, Lcom/google/n/ai;->b:Lcom/google/n/aj;

    .line 422
    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/n/ai;->b:Lcom/google/n/aj;

    iget-object v1, p0, Lcom/google/n/ai;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/n/aj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/n/ai;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
