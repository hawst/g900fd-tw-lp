.class public final Lcom/google/n/q;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<FieldDescriptorType::",
        "Lcom/google/n/s",
        "<TFieldDescriptorType;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final d:Lcom/google/n/q;


# instance fields
.field public final a:Lcom/google/n/be;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/be",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/google/n/q;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/n/q;-><init>(B)V

    sput-object v0, Lcom/google/n/q;->d:Lcom/google/n/q;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/n/q;->c:Z

    .line 53
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/n/be;->a(I)Lcom/google/n/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    .line 54
    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-boolean v0, p0, Lcom/google/n/q;->c:Z

    .line 61
    invoke-static {v0}, Lcom/google/n/be;->a(I)Lcom/google/n/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    .line 62
    iget-boolean v0, p0, Lcom/google/n/q;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v0}, Lcom/google/n/be;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/n/q;->b:Z

    .line 63
    :cond_0
    return-void
.end method

.method private static a(Lcom/google/n/bu;ILjava/lang/Object;)I
    .locals 2

    .prologue
    .line 950
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    .line 951
    sget-object v0, Lcom/google/n/bu;->j:Lcom/google/n/bu;

    if-ne p0, v0, :cond_0

    move-object v0, p2

    .line 955
    check-cast v0, Lcom/google/n/at;

    invoke-static {v0}, Lcom/google/n/af;->a(Lcom/google/n/at;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 957
    shl-int/lit8 v0, v1, 0x1

    .line 962
    :goto_0
    invoke-static {p0, p2}, Lcom/google/n/q;->b(Lcom/google/n/bu;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static a(Lcom/google/n/bu;Z)I
    .locals 1

    .prologue
    .line 564
    if-eqz p1, :cond_0

    .line 565
    const/4 v0, 0x2

    .line 567
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/n/bu;->t:I

    goto :goto_0
.end method

.method public static a()Lcom/google/n/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/s",
            "<TT;>;>()",
            "Lcom/google/n/q",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lcom/google/n/q;

    invoke-direct {v0}, Lcom/google/n/q;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/google/n/j;Lcom/google/n/bu;Z)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 665
    sget-object v2, Lcom/google/n/r;->b:[I

    invoke-virtual {p1}, Lcom/google/n/bu;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 699
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There is no way to get here, but the compiler thinks otherwise."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 666
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/n/j;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 684
    :goto_0
    return-object v0

    .line 667
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 668
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/n/j;->m()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 669
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/n/j;->m()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 670
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 671
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/n/j;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 672
    :pswitch_6
    invoke-virtual {p0}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 673
    :pswitch_7
    invoke-virtual {p0}, Lcom/google/n/j;->m()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    .line 674
    :pswitch_8
    if-eqz p2, :cond_5

    .line 675
    invoke-virtual {p0}, Lcom/google/n/j;->l()I

    move-result v4

    iget v2, p0, Lcom/google/n/j;->c:I

    iget v3, p0, Lcom/google/n/j;->b:I

    sub-int/2addr v3, v2

    if-gt v4, v3, :cond_1

    if-lez v4, :cond_1

    iget-object v3, p0, Lcom/google/n/j;->a:[B

    add-int v5, v2, v4

    iput v5, p0, Lcom/google/n/j;->c:I

    :goto_2
    add-int v5, v2, v4

    invoke-static {v3, v2, v5}, Lcom/google/n/bs;->a([BII)I

    move-result v5

    if-nez v5, :cond_3

    :goto_3
    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/n/ak;->i()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    :cond_1
    if-nez v4, :cond_2

    const-string v0, ""

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/n/j;->b(I)[B

    move-result-object v2

    move-object v3, v2

    move v2, v1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, v3, v2, v4, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    goto/16 :goto_0

    .line 677
    :cond_5
    invoke-virtual {p0}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 679
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    goto/16 :goto_0

    .line 680
    :pswitch_a
    invoke-virtual {p0}, Lcom/google/n/j;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 681
    :pswitch_b
    invoke-virtual {p0}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 682
    :pswitch_c
    invoke-virtual {p0}, Lcom/google/n/j;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 683
    :pswitch_d
    invoke-virtual {p0}, Lcom/google/n/j;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 684
    :pswitch_e
    invoke-virtual {p0}, Lcom/google/n/j;->k()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 687
    :pswitch_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle nested groups."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 690
    :pswitch_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle embedded messages."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 695
    :pswitch_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle enums."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 665
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 587
    instance-of v0, p0, Lcom/google/n/aw;

    if-eqz v0, :cond_1

    .line 588
    check-cast p0, Lcom/google/n/aw;

    invoke-interface {p0}, Lcom/google/n/aw;->h()Lcom/google/n/aw;

    move-result-object p0

    .line 597
    :cond_0
    :goto_0
    return-object p0

    .line 591
    :cond_1
    instance-of v0, p0, [B

    if-eqz v0, :cond_0

    .line 592
    check-cast p0, [B

    .line 593
    array-length v0, p0

    new-array v0, v0, [B

    .line 594
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 595
    goto :goto_0
.end method

.method private static a(Lcom/google/n/bu;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 463
    if-nez p1, :cond_0

    .line 464
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 468
    :cond_0
    sget-object v2, Lcom/google/n/r;->a:[I

    iget-object v3, p0, Lcom/google/n/bu;->s:Lcom/google/n/bz;

    invoke-virtual {v3}, Lcom/google/n/bz;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 490
    :cond_1
    :goto_0
    if-nez v0, :cond_5

    .line 498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 469
    :pswitch_0
    instance-of v0, p1, Ljava/lang/Integer;

    goto :goto_0

    .line 470
    :pswitch_1
    instance-of v0, p1, Ljava/lang/Long;

    goto :goto_0

    .line 471
    :pswitch_2
    instance-of v0, p1, Ljava/lang/Float;

    goto :goto_0

    .line 472
    :pswitch_3
    instance-of v0, p1, Ljava/lang/Double;

    goto :goto_0

    .line 473
    :pswitch_4
    instance-of v0, p1, Ljava/lang/Boolean;

    goto :goto_0

    .line 474
    :pswitch_5
    instance-of v0, p1, Ljava/lang/String;

    goto :goto_0

    .line 476
    :pswitch_6
    instance-of v2, p1, Lcom/google/n/f;

    if-nez v2, :cond_2

    instance-of v2, p1, [B

    if-eqz v2, :cond_1

    :cond_2
    move v0, v1

    goto :goto_0

    .line 480
    :pswitch_7
    instance-of v2, p1, Ljava/lang/Integer;

    if-nez v2, :cond_3

    instance-of v2, p1, Lcom/google/n/ag;

    if-eqz v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 485
    :pswitch_8
    instance-of v2, p1, Lcom/google/n/at;

    if-nez v2, :cond_4

    instance-of v2, p1, Lcom/google/n/al;

    if-eqz v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0

    .line 501
    :cond_5
    return-void

    .line 468
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static a(Lcom/google/n/l;Lcom/google/n/bu;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 782
    sget-object v0, Lcom/google/n/bu;->j:Lcom/google/n/bu;

    if-ne p1, v0, :cond_1

    move-object v0, p3

    .line 784
    check-cast v0, Lcom/google/n/at;

    invoke-static {v0}, Lcom/google/n/af;->a(Lcom/google/n/at;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 785
    const/4 v0, 0x3

    invoke-static {p2, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 786
    check-cast p3, Lcom/google/n/at;

    invoke-interface {p3, p0}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 799
    :goto_0
    return-void

    .line 791
    :cond_0
    check-cast p3, Lcom/google/n/at;

    invoke-virtual {p0, p2, p3}, Lcom/google/n/l;->a(ILcom/google/n/at;)V

    goto :goto_0

    .line 796
    :cond_1
    iget v0, p1, Lcom/google/n/bu;->t:I

    invoke-static {p2, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    .line 797
    invoke-static {p0, p1, p3}, Lcom/google/n/q;->a(Lcom/google/n/l;Lcom/google/n/bu;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/n/l;Lcom/google/n/bu;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 815
    sget-object v1, Lcom/google/n/r;->b:[I

    invoke-virtual {p1}, Lcom/google/n/bu;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 848
    :goto_0
    return-void

    .line 816
    :pswitch_0
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->c(J)V

    goto :goto_0

    .line 817
    :pswitch_1
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->d(I)V

    goto :goto_0

    .line 818
    :pswitch_2
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 819
    :pswitch_3
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 820
    :pswitch_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    goto :goto_0

    :cond_0
    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 821
    :pswitch_5
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->c(J)V

    goto :goto_0

    .line 822
    :pswitch_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->d(I)V

    goto :goto_0

    .line 823
    :pswitch_7
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/n/l;->a(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 824
    :pswitch_8
    check-cast p2, Ljava/lang/String;

    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p0, v0}, Lcom/google/n/l;->a([B)V

    goto :goto_0

    .line 825
    :pswitch_9
    check-cast p2, Lcom/google/n/at;

    invoke-interface {p2, p0}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    goto :goto_0

    .line 826
    :pswitch_a
    check-cast p2, Lcom/google/n/at;

    invoke-interface {p2}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    invoke-interface {p2, p0}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    goto/16 :goto_0

    .line 828
    :pswitch_b
    instance-of v0, p2, Lcom/google/n/f;

    if-eqz v0, :cond_2

    .line 829
    check-cast p2, Lcom/google/n/f;

    invoke-virtual {p2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p0, p2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    goto/16 :goto_0

    .line 831
    :cond_2
    check-cast p2, [B

    array-length v0, p2

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p0, p2}, Lcom/google/n/l;->a([B)V

    goto/16 :goto_0

    .line 834
    :pswitch_c
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    goto/16 :goto_0

    .line 835
    :pswitch_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/n/l;->d(I)V

    goto/16 :goto_0

    .line 836
    :pswitch_e
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->c(J)V

    goto/16 :goto_0

    .line 837
    :pswitch_f
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v1, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    goto/16 :goto_0

    .line 838
    :pswitch_10
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    shl-long v0, v2, v0

    const/16 v4, 0x3f

    shr-long/2addr v2, v4

    xor-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 841
    :pswitch_11
    instance-of v0, p2, Lcom/google/n/ag;

    if-eqz v0, :cond_4

    .line 842
    check-cast p2, Lcom/google/n/ag;

    invoke-interface {p2}, Lcom/google/n/ag;->a()I

    move-result v0

    if-ltz v0, :cond_3

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    goto/16 :goto_0

    :cond_3
    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 844
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-virtual {p0, v0}, Lcom/google/n/l;->b(I)V

    goto/16 :goto_0

    :cond_5
    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 815
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_9
        :pswitch_a
        :pswitch_11
    .end packed-switch
.end method

.method public static a(Lcom/google/n/s;Ljava/lang/Object;Lcom/google/n/l;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/s",
            "<*>;",
            "Ljava/lang/Object;",
            "Lcom/google/n/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 855
    invoke-interface {p0}, Lcom/google/n/s;->b()Lcom/google/n/bu;

    move-result-object v1

    .line 856
    invoke-interface {p0}, Lcom/google/n/s;->a()I

    move-result v0

    .line 857
    invoke-interface {p0}, Lcom/google/n/s;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 858
    check-cast p1, Ljava/util/List;

    .line 859
    invoke-interface {p0}, Lcom/google/n/s;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 860
    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/n/l;->b(I)V

    .line 862
    const/4 v0, 0x0

    .line 863
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 864
    invoke-static {v1, v3}, Lcom/google/n/q;->b(Lcom/google/n/bu;Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 865
    goto :goto_0

    .line 866
    :cond_0
    invoke-virtual {p2, v0}, Lcom/google/n/l;->b(I)V

    .line 868
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 869
    invoke-static {p2, v1, v2}, Lcom/google/n/q;->a(Lcom/google/n/l;Lcom/google/n/bu;Ljava/lang/Object;)V

    goto :goto_1

    .line 872
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 873
    invoke-static {p2, v1, v0, v3}, Lcom/google/n/q;->a(Lcom/google/n/l;Lcom/google/n/bu;ILjava/lang/Object;)V

    goto :goto_2

    .line 877
    :cond_2
    instance-of v2, p1, Lcom/google/n/al;

    if-eqz v2, :cond_4

    .line 878
    check-cast p1, Lcom/google/n/al;

    iget-object v2, p1, Lcom/google/n/al;->a:Lcom/google/n/at;

    invoke-virtual {p1, v2}, Lcom/google/n/al;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    invoke-static {p2, v1, v0, v2}, Lcom/google/n/q;->a(Lcom/google/n/l;Lcom/google/n/bu;ILjava/lang/Object;)V

    .line 883
    :cond_3
    :goto_3
    return-void

    .line 880
    :cond_4
    invoke-static {p2, v1, v0, p1}, Lcom/google/n/q;->a(Lcom/google/n/l;Lcom/google/n/bu;ILjava/lang/Object;)V

    goto :goto_3
.end method

.method private static b(Lcom/google/n/bu;Ljava/lang/Object;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0xa

    const/16 v0, 0x8

    const/4 v1, 0x4

    .line 977
    sget-object v4, Lcom/google/n/r;->b:[I

    invoke-virtual {p0}, Lcom/google/n/bu;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1018
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There is no way to get here, but the compiler thinks otherwise."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 980
    :pswitch_0
    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    .line 1014
    :goto_0
    return v0

    .line 981
    :pswitch_1
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move v0, v1

    goto :goto_0

    .line 982
    :pswitch_2
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/n/l;->b(J)I

    move-result v0

    goto :goto_0

    .line 983
    :pswitch_3
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/n/l;->b(J)I

    move-result v0

    goto :goto_0

    .line 984
    :pswitch_4
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 985
    :pswitch_5
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    goto :goto_0

    .line 986
    :pswitch_6
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move v0, v1

    goto :goto_0

    .line 987
    :pswitch_7
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move v0, v3

    goto :goto_0

    .line 988
    :pswitch_8
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/google/n/l;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 989
    :pswitch_9
    check-cast p1, Lcom/google/n/at;

    invoke-interface {p1}, Lcom/google/n/at;->c()I

    move-result v0

    goto :goto_0

    .line 991
    :pswitch_a
    instance-of v0, p1, Lcom/google/n/f;

    if-eqz v0, :cond_1

    .line 992
    check-cast p1, Lcom/google/n/f;

    invoke-virtual {p1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {p1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 994
    :cond_1
    check-cast p1, [B

    array-length v0, p1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    array-length v1, p1

    add-int/2addr v0, v1

    goto :goto_0

    .line 996
    :pswitch_b
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    goto :goto_0

    .line 997
    :pswitch_c
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move v0, v1

    goto :goto_0

    .line 998
    :pswitch_d
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    goto/16 :goto_0

    .line 999
    :pswitch_e
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v1, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    goto/16 :goto_0

    .line 1000
    :pswitch_f
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    shl-long v2, v0, v3

    const/16 v4, 0x3f

    shr-long/2addr v0, v4

    xor-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/n/l;->b(J)I

    move-result v0

    goto/16 :goto_0

    .line 1003
    :pswitch_10
    instance-of v0, p1, Lcom/google/n/al;

    if-eqz v0, :cond_2

    .line 1004
    check-cast p1, Lcom/google/n/al;

    invoke-static {p1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    goto/16 :goto_0

    .line 1006
    :cond_2
    check-cast p1, Lcom/google/n/at;

    invoke-interface {p1}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    goto/16 :goto_0

    .line 1010
    :pswitch_11
    instance-of v0, p1, Lcom/google/n/ag;

    if-eqz v0, :cond_4

    .line 1011
    check-cast p1, Lcom/google/n/ag;

    .line 1012
    invoke-interface {p1}, Lcom/google/n/ag;->a()I

    move-result v0

    .line 1011
    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_0

    .line 1014
    :cond_4
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_0

    .line 977
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_9
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static b(Ljava/util/Map$Entry;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 920
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/s;

    .line 921
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 922
    invoke-interface {v0}, Lcom/google/n/s;->c()Lcom/google/n/bz;

    move-result-object v2

    sget-object v3, Lcom/google/n/bz;->i:Lcom/google/n/bz;

    if-ne v2, v3, :cond_1

    .line 923
    invoke-interface {v0}, Lcom/google/n/s;->d()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v0}, Lcom/google/n/s;->e()Z

    move-result v2

    if-nez v2, :cond_1

    .line 924
    instance-of v0, v1, Lcom/google/n/al;

    if-eqz v0, :cond_0

    .line 926
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/s;

    invoke-interface {v0}, Lcom/google/n/s;->a()I

    move-result v2

    move-object v0, v1

    check-cast v0, Lcom/google/n/al;

    .line 925
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    shl-int/lit8 v1, v1, 0x1

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    invoke-static {v7, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 932
    :goto_0
    return v0

    .line 929
    :cond_0
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/s;

    invoke-interface {v0}, Lcom/google/n/s;->a()I

    move-result v0

    check-cast v1, Lcom/google/n/at;

    .line 928
    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    invoke-static {v7, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v1}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0

    .line 932
    :cond_1
    invoke-static {v0, v1}, Lcom/google/n/q;->c(Lcom/google/n/s;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public static b()Lcom/google/n/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/s",
            "<TT;>;>()",
            "Lcom/google/n/q",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lcom/google/n/q;->d:Lcom/google/n/q;

    return-object v0
.end method

.method public static c(Lcom/google/n/s;Ljava/lang/Object;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/s",
            "<*>;",
            "Ljava/lang/Object;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1027
    invoke-interface {p0}, Lcom/google/n/s;->b()Lcom/google/n/bu;

    move-result-object v2

    .line 1028
    invoke-interface {p0}, Lcom/google/n/s;->a()I

    move-result v3

    .line 1029
    invoke-interface {p0}, Lcom/google/n/s;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1030
    invoke-interface {p0}, Lcom/google/n/s;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1032
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v1

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 1033
    invoke-static {v2, v5}, Lcom/google/n/q;->b(Lcom/google/n/bu;Ljava/lang/Object;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1034
    goto :goto_0

    .line 1036
    :cond_0
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/2addr v1, v0

    .line 1037
    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v1, v0

    .line 1046
    :cond_1
    :goto_1
    return v1

    .line 1040
    :cond_2
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 1041
    invoke-static {v2, v3, v4}, Lcom/google/n/q;->a(Lcom/google/n/bu;ILjava/lang/Object;)I

    move-result v4

    add-int/2addr v1, v4

    .line 1042
    goto :goto_2

    .line 1046
    :cond_3
    invoke-static {v2, v3, p1}, Lcom/google/n/q;->a(Lcom/google/n/bu;ILjava/lang/Object;)I

    move-result v1

    goto :goto_1
.end method

.method private static c(Ljava/util/Map$Entry;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 530
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/s;

    .line 531
    invoke-interface {v0}, Lcom/google/n/s;->c()Lcom/google/n/bz;

    move-result-object v3

    sget-object v4, Lcom/google/n/bz;->i:Lcom/google/n/bz;

    if-ne v3, v4, :cond_4

    .line 532
    invoke-interface {v0}, Lcom/google/n/s;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 535
    invoke-interface {v0}, Lcom/google/n/at;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 553
    :goto_0
    return v0

    .line 540
    :cond_1
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 541
    instance-of v3, v0, Lcom/google/n/at;

    if-eqz v3, :cond_2

    .line 542
    check-cast v0, Lcom/google/n/at;

    invoke-interface {v0}, Lcom/google/n/at;->b()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 543
    goto :goto_0

    .line 545
    :cond_2
    instance-of v0, v0, Lcom/google/n/al;

    if-eqz v0, :cond_3

    move v0, v2

    .line 546
    goto :goto_0

    .line 548
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    .line 553
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/n/s;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 333
    invoke-interface {p1}, Lcom/google/n/s;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 334
    instance-of v0, p2, Ljava/util/List;

    if-nez v0, :cond_0

    .line 335
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 342
    check-cast p2, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 343
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 344
    invoke-interface {p1}, Lcom/google/n/s;->b()Lcom/google/n/bu;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/n/q;->a(Lcom/google/n/bu;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move-object p2, v0

    .line 351
    :goto_1
    instance-of v0, p2, Lcom/google/n/al;

    if-eqz v0, :cond_2

    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/n/q;->c:Z

    .line 354
    :cond_2
    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v0, p1, p2}, Lcom/google/n/be;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    return-void

    .line 348
    :cond_3
    invoke-interface {p1}, Lcom/google/n/s;->b()Lcom/google/n/bu;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/n/q;->a(Lcom/google/n/bu;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method a(Ljava/util/Map$Entry;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 604
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/s;

    .line 605
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 606
    instance-of v2, v1, Lcom/google/n/al;

    if-eqz v2, :cond_8

    .line 607
    check-cast v1, Lcom/google/n/al;

    iget-object v2, v1, Lcom/google/n/al;->a:Lcom/google/n/at;

    invoke-virtual {v1, v2}, Lcom/google/n/al;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    .line 610
    :goto_0
    invoke-interface {v0}, Lcom/google/n/s;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 611
    iget-object v1, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1, v0}, Lcom/google/n/be;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Lcom/google/n/al;

    if-eqz v3, :cond_1

    check-cast v1, Lcom/google/n/al;

    iget-object v3, v1, Lcom/google/n/al;->a:Lcom/google/n/at;

    invoke-virtual {v1, v3}, Lcom/google/n/al;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    .line 612
    :goto_1
    if-nez v3, :cond_0

    .line 613
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    move-object v1, v2

    .line 615
    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v1, v3

    .line 616
    check-cast v1, Ljava/util/List;

    invoke-static {v4}, Lcom/google/n/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move-object v3, v1

    .line 611
    goto :goto_1

    .line 618
    :cond_2
    iget-object v1, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1, v0, v3}, Lcom/google/n/be;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 643
    :goto_3
    return-void

    .line 619
    :cond_3
    invoke-interface {v0}, Lcom/google/n/s;->c()Lcom/google/n/bz;

    move-result-object v1

    sget-object v3, Lcom/google/n/bz;->i:Lcom/google/n/bz;

    if-ne v1, v3, :cond_7

    .line 620
    iget-object v1, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1, v0}, Lcom/google/n/be;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Lcom/google/n/al;

    if-eqz v3, :cond_4

    check-cast v1, Lcom/google/n/al;

    iget-object v3, v1, Lcom/google/n/al;->a:Lcom/google/n/at;

    invoke-virtual {v1, v3}, Lcom/google/n/al;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    .line 621
    :cond_4
    if-nez v1, :cond_5

    .line 622
    iget-object v1, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-static {v2}, Lcom/google/n/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/n/be;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 626
    :cond_5
    instance-of v3, v1, Lcom/google/n/aw;

    if-eqz v3, :cond_6

    .line 627
    check-cast v1, Lcom/google/n/aw;

    check-cast v2, Lcom/google/n/aw;

    invoke-interface {v0, v1, v2}, Lcom/google/n/s;->a(Lcom/google/n/aw;Lcom/google/n/aw;)Lcom/google/n/aw;

    move-result-object v1

    .line 638
    :goto_4
    iget-object v2, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2, v0, v1}, Lcom/google/n/be;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 631
    :cond_6
    check-cast v1, Lcom/google/n/at;

    .line 632
    invoke-interface {v1}, Lcom/google/n/at;->e()Lcom/google/n/au;

    move-result-object v1

    check-cast v2, Lcom/google/n/at;

    .line 631
    invoke-interface {v0, v1, v2}, Lcom/google/n/s;->a(Lcom/google/n/au;Lcom/google/n/at;)Lcom/google/n/au;

    move-result-object v1

    .line 633
    invoke-interface {v1}, Lcom/google/n/au;->h()Lcom/google/n/at;

    move-result-object v1

    goto :goto_4

    .line 641
    :cond_7
    iget-object v1, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-static {v2}, Lcom/google/n/q;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/n/be;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_8
    move-object v2, v1

    goto/16 :goto_0
.end method

.method public final b(Lcom/google/n/s;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 435
    invoke-interface {p1}, Lcom/google/n/s;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 436
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addRepeatedField() can only be called on repeated fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440
    :cond_0
    invoke-interface {p1}, Lcom/google/n/s;->b()Lcom/google/n/bu;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/n/q;->a(Lcom/google/n/bu;Ljava/lang/Object;)V

    .line 442
    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v0, p1}, Lcom/google/n/be;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/google/n/al;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/n/al;

    iget-object v1, v0, Lcom/google/n/al;->a:Lcom/google/n/at;

    invoke-virtual {v0, v1}, Lcom/google/n/al;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    .line 444
    :cond_1
    if-nez v0, :cond_2

    .line 445
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 446
    iget-object v1, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1, p1, v0}, Lcom/google/n/be;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    :goto_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 452
    return-void

    .line 448
    :cond_2
    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public final c()Lcom/google/n/q;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/q",
            "<TFieldDescriptorType;>;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v3, Lcom/google/n/q;

    invoke-direct {v3}, Lcom/google/n/q;-><init>()V

    .line 111
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 113
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/s;

    .line 114
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/n/q;->a(Lcom/google/n/s;Ljava/lang/Object;)V

    .line 111
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v1, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/n/bg;->a()Ljava/lang/Iterable;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 118
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/s;

    .line 119
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/n/q;->a(Lcom/google/n/s;Ljava/lang/Object;)V

    goto :goto_2

    .line 117
    :cond_1
    iget-object v0, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_1

    .line 121
    :cond_2
    iget-boolean v0, p0, Lcom/google/n/q;->c:Z

    iput-boolean v0, v3, Lcom/google/n/q;->c:Z

    .line 122
    return-object v3
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/n/q;->c()Lcom/google/n/q;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 513
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 514
    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-static {v0}, Lcom/google/n/q;->c(Ljava/util/Map$Entry;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    :goto_1
    return v2

    .line 513
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 519
    :cond_1
    iget-object v0, p0, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v1, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/google/n/bg;->a()Ljava/lang/Iterable;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 520
    invoke-static {v0}, Lcom/google/n/q;->c(Ljava/util/Map$Entry;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    .line 519
    :cond_3
    iget-object v0, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_2

    .line 524
    :cond_4
    const/4 v2, 0x1

    goto :goto_1
.end method
