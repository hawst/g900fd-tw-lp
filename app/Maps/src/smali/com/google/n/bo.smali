.class public final Lcom/google/n/bo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lcom/google/n/i;

.field private b:Lcom/google/n/l;

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/google/n/bo;->c:Z

    if-eqz v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Do not reuse UnknownFieldSetLite Builders."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/n/bo;->a:Lcom/google/n/i;

    if-nez v0, :cond_1

    .line 126
    const/16 v0, 0x64

    invoke-static {v0}, Lcom/google/n/f;->a(I)Lcom/google/n/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/bo;->a:Lcom/google/n/i;

    .line 127
    iget-object v0, p0, Lcom/google/n/bo;->a:Lcom/google/n/i;

    const/16 v1, 0x1000

    new-instance v2, Lcom/google/n/l;

    new-array v1, v1, [B

    invoke-direct {v2, v0, v1}, Lcom/google/n/l;-><init>(Ljava/io/OutputStream;[B)V

    iput-object v2, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    .line 129
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/n/bn;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 224
    iget-boolean v1, p0, Lcom/google/n/bo;->c:Z

    if-eqz v1, :cond_0

    .line 225
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Do not reuse UnknownFieldSetLite Builders."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :cond_0
    iput-boolean v0, p0, Lcom/google/n/bo;->c:Z

    .line 232
    iget-object v1, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    if-nez v1, :cond_1

    .line 233
    invoke-static {}, Lcom/google/n/bn;->a()Lcom/google/n/bn;

    move-result-object v0

    .line 249
    :goto_0
    iput-object v3, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    .line 250
    iput-object v3, p0, Lcom/google/n/bo;->a:Lcom/google/n/i;

    .line 251
    return-object v0

    .line 236
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    iget-object v2, v1, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/n/l;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/n/bo;->a:Lcom/google/n/i;

    invoke-virtual {v1}, Lcom/google/n/i;->a()Lcom/google/n/f;

    move-result-object v1

    .line 241
    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    if-eqz v0, :cond_4

    .line 242
    invoke-static {}, Lcom/google/n/bn;->a()Lcom/google/n/bn;

    move-result-object v0

    goto :goto_0

    .line 241
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 244
    :cond_4
    new-instance v0, Lcom/google/n/bn;

    invoke-direct {v0, v1}, Lcom/google/n/bn;-><init>(Lcom/google/n/f;)V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public final a(II)Lcom/google/n/bo;
    .locals 4

    .prologue
    .line 182
    if-nez p1, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_0
    invoke-direct {p0}, Lcom/google/n/bo;->b()V

    .line 187
    :try_start_0
    iget-object v0, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/n/l;->a(IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(ILcom/google/n/j;)Z
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v0, 0x1

    .line 141
    invoke-direct {p0}, Lcom/google/n/bo;->b()V

    .line 143
    invoke-static {p1}, Lcom/google/n/bt;->b(I)I

    move-result v1

    .line 144
    invoke-static {p1}, Lcom/google/n/bt;->a(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 170
    invoke-static {}, Lcom/google/n/ak;->f()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 146
    :pswitch_0
    iget-object v2, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    invoke-virtual {p2}, Lcom/google/n/j;->m()J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/n/l;->a(IJ)V

    .line 168
    :goto_0
    return v0

    .line 149
    :pswitch_1
    iget-object v2, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    invoke-virtual {p2}, Lcom/google/n/j;->n()I

    move-result v3

    const/4 v4, 0x5

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2, v3}, Lcom/google/n/l;->d(I)V

    goto :goto_0

    .line 152
    :pswitch_2
    iget-object v2, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    invoke-virtual {p2}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/n/l;->c(IJ)V

    goto :goto_0

    .line 155
    :pswitch_3
    iget-object v2, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    invoke-virtual {p2}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    goto :goto_0

    .line 158
    :pswitch_4
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 159
    :cond_0
    invoke-virtual {p2}, Lcom/google/n/j;->a()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v3, p2}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 161
    :cond_1
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    .line 160
    iget v4, p2, Lcom/google/n/j;->d:I

    if-eq v4, v3, :cond_2

    invoke-static {}, Lcom/google/n/ak;->e()Lcom/google/n/ak;

    move-result-object v0

    throw v0

    .line 163
    :cond_2
    iget-object v3, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    const/4 v4, 0x3

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/n/l;->b(I)V

    .line 164
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v2

    iget-object v3, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    iget-object v2, v2, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v3, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 165
    iget-object v2, p0, Lcom/google/n/bo;->b:Lcom/google/n/l;

    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/n/l;->b(I)V

    goto :goto_0

    .line 168
    :pswitch_5
    const/4 v0, 0x0

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method
