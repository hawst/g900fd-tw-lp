.class Lcom/google/n/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/n/g;


# instance fields
.field final synthetic a:Lcom/google/n/ar;

.field private b:I

.field private final c:I


# direct methods
.method constructor <init>(Lcom/google/n/ar;)V
    .locals 1

    .prologue
    .line 294
    iput-object p1, p0, Lcom/google/n/as;->a:Lcom/google/n/ar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/n/as;->b:I

    .line 296
    invoke-virtual {p1}, Lcom/google/n/ar;->b()I

    move-result v0

    iput v0, p0, Lcom/google/n/as;->c:I

    .line 297
    return-void
.end method


# virtual methods
.method public final a()B
    .locals 3

    .prologue
    .line 310
    :try_start_0
    iget-object v0, p0, Lcom/google/n/as;->a:Lcom/google/n/ar;

    iget-object v0, v0, Lcom/google/n/ar;->c:[B

    iget v1, p0, Lcom/google/n/as;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/n/as;->b:I

    aget-byte v0, v0, v1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 311
    :catch_0
    move-exception v0

    .line 312
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 300
    iget v0, p0, Lcom/google/n/as;->b:I

    iget v1, p0, Lcom/google/n/as;->c:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/google/n/as;->a()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 317
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
