.class public abstract Lcom/google/n/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/n/at;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    const/16 v0, 0x1000

    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/google/n/a;->c()I

    move-result v1

    if-le v1, v0, :cond_1

    .line 52
    :goto_0
    new-instance v1, Lcom/google/n/l;

    new-array v0, v0, [B

    invoke-direct {v1, p1, v0}, Lcom/google/n/l;-><init>(Ljava/io/OutputStream;[B)V

    .line 53
    invoke-virtual {p0, v1}, Lcom/google/n/a;->a(Lcom/google/n/l;)V

    .line 54
    iget-object v0, v1, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/n/l;->a()V

    .line 55
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public final b(Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    const/16 v0, 0x1000

    .line 58
    invoke-virtual {p0}, Lcom/google/n/a;->c()I

    move-result v2

    .line 60
    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/2addr v1, v2

    .line 59
    if-le v1, v0, :cond_1

    .line 62
    :goto_0
    new-instance v1, Lcom/google/n/l;

    new-array v0, v0, [B

    invoke-direct {v1, p1, v0}, Lcom/google/n/l;-><init>(Ljava/io/OutputStream;[B)V

    .line 63
    invoke-virtual {v1, v2}, Lcom/google/n/l;->b(I)V

    .line 64
    invoke-virtual {p0, v1}, Lcom/google/n/a;->a(Lcom/google/n/l;)V

    .line 65
    iget-object v0, v1, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/n/l;->a()V

    .line 66
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 59
    goto :goto_0
.end method

.method public final k()Lcom/google/n/f;
    .locals 3

    .prologue
    .line 23
    .line 24
    :try_start_0
    invoke-virtual {p0}, Lcom/google/n/a;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/f;->b(I)Lcom/google/n/h;

    move-result-object v0

    .line 25
    iget-object v1, v0, Lcom/google/n/h;->a:Lcom/google/n/l;

    invoke-virtual {p0, v1}, Lcom/google/n/a;->a(Lcom/google/n/l;)V

    .line 26
    iget-object v1, v0, Lcom/google/n/h;->a:Lcom/google/n/l;

    invoke-virtual {v1}, Lcom/google/n/l;->b()V

    new-instance v1, Lcom/google/n/ar;

    iget-object v0, v0, Lcom/google/n/h;->b:[B

    invoke-direct {v1, v0}, Lcom/google/n/ar;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 27
    :catch_0
    move-exception v0

    .line 28
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final l()[B
    .locals 4

    .prologue
    .line 36
    :try_start_0
    invoke-virtual {p0}, Lcom/google/n/a;->c()I

    move-result v0

    new-array v0, v0, [B

    .line 37
    const/4 v1, 0x0

    array-length v2, v0

    new-instance v3, Lcom/google/n/l;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/n/l;-><init>([BII)V

    .line 38
    invoke-virtual {p0, v3}, Lcom/google/n/a;->a(Lcom/google/n/l;)V

    .line 39
    invoke-virtual {v3}, Lcom/google/n/l;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
