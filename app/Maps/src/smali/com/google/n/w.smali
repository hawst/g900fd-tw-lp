.class public abstract Lcom/google/n/w;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/n/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lcom/google/n/x",
        "<TMessageType;>;BuilderType:",
        "Lcom/google/n/w",
        "<TMessageType;TBuilderType;>;>",
        "Lcom/google/n/v",
        "<TMessageType;TBuilderType;>;",
        "Lcom/google/n/z",
        "<TMessageType;>;"
    }
.end annotation


# instance fields
.field public d:Lcom/google/n/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/q",
            "<",
            "Lcom/google/n/aa;",
            ">;"
        }
    .end annotation
.end field

.field e:Z


# direct methods
.method public constructor <init>(Lcom/google/n/x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)V"
        }
    .end annotation

    .prologue
    .line 399
    invoke-direct {p0, p1}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 402
    invoke-static {}, Lcom/google/n/q;->b()Lcom/google/n/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    .line 400
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/n/x;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)V"
        }
    .end annotation

    .prologue
    .line 565
    iget-boolean v0, p0, Lcom/google/n/w;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->c()Lcom/google/n/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/n/w;->e:Z

    .line 566
    :cond_0
    iget-object v2, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    iget-object v3, p1, Lcom/google/n/x;->as:Lcom/google/n/q;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, v3, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v3, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-virtual {v2, v0}, Lcom/google/n/q;->a(Ljava/util/Map$Entry;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, v3, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v1, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/google/n/bg;->a()Ljava/lang/Iterable;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-virtual {v2, v0}, Lcom/google/n/q;->a(Ljava/util/Map$Entry;)V

    goto :goto_2

    :cond_2
    iget-object v0, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_1

    .line 567
    :cond_3
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 392
    invoke-super {p0}, Lcom/google/n/v;->f()Lcom/google/n/v;

    move-result-object v0

    check-cast v0, Lcom/google/n/w;

    return-object v0
.end method

.method public final synthetic e()Lcom/google/n/b;
    .locals 1

    .prologue
    .line 392
    invoke-super {p0}, Lcom/google/n/v;->f()Lcom/google/n/v;

    move-result-object v0

    check-cast v0, Lcom/google/n/w;

    return-object v0
.end method

.method public final bridge synthetic f()Lcom/google/n/v;
    .locals 1

    .prologue
    .line 392
    invoke-super {p0}, Lcom/google/n/v;->f()Lcom/google/n/v;

    move-result-object v0

    check-cast v0, Lcom/google/n/w;

    return-object v0
.end method
