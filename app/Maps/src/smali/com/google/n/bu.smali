.class public enum Lcom/google/n/bu;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/n/bu;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/n/bu;

.field public static final enum b:Lcom/google/n/bu;

.field public static final enum c:Lcom/google/n/bu;

.field public static final enum d:Lcom/google/n/bu;

.field public static final enum e:Lcom/google/n/bu;

.field public static final enum f:Lcom/google/n/bu;

.field public static final enum g:Lcom/google/n/bu;

.field public static final enum h:Lcom/google/n/bu;

.field public static final enum i:Lcom/google/n/bu;

.field public static final enum j:Lcom/google/n/bu;

.field public static final enum k:Lcom/google/n/bu;

.field public static final enum l:Lcom/google/n/bu;

.field public static final enum m:Lcom/google/n/bu;

.field public static final enum n:Lcom/google/n/bu;

.field public static final enum o:Lcom/google/n/bu;

.field public static final enum p:Lcom/google/n/bu;

.field public static final enum q:Lcom/google/n/bu;

.field public static final enum r:Lcom/google/n/bu;

.field private static final synthetic u:[Lcom/google/n/bu;


# instance fields
.field final s:Lcom/google/n/bz;

.field final t:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 80
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "DOUBLE"

    sget-object v2, Lcom/google/n/bz;->d:Lcom/google/n/bz;

    invoke-direct {v0, v1, v4, v2, v5}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->a:Lcom/google/n/bu;

    .line 81
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "FLOAT"

    sget-object v2, Lcom/google/n/bz;->c:Lcom/google/n/bz;

    invoke-direct {v0, v1, v5, v2, v7}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->b:Lcom/google/n/bu;

    .line 82
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "INT64"

    sget-object v2, Lcom/google/n/bz;->b:Lcom/google/n/bz;

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->c:Lcom/google/n/bu;

    .line 83
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "UINT64"

    sget-object v2, Lcom/google/n/bz;->b:Lcom/google/n/bz;

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->d:Lcom/google/n/bu;

    .line 84
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "INT32"

    const/4 v2, 0x4

    sget-object v3, Lcom/google/n/bz;->a:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->e:Lcom/google/n/bu;

    .line 85
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "FIXED64"

    sget-object v2, Lcom/google/n/bz;->b:Lcom/google/n/bz;

    invoke-direct {v0, v1, v7, v2, v5}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->f:Lcom/google/n/bu;

    .line 86
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "FIXED32"

    const/4 v2, 0x6

    sget-object v3, Lcom/google/n/bz;->a:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->g:Lcom/google/n/bu;

    .line 87
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "BOOL"

    const/4 v2, 0x7

    sget-object v3, Lcom/google/n/bz;->e:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->h:Lcom/google/n/bu;

    .line 88
    new-instance v0, Lcom/google/n/bv;

    const-string v1, "STRING"

    const/16 v2, 0x8

    sget-object v3, Lcom/google/n/bz;->f:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/n/bv;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->i:Lcom/google/n/bu;

    .line 91
    new-instance v0, Lcom/google/n/bw;

    const-string v1, "GROUP"

    const/16 v2, 0x9

    sget-object v3, Lcom/google/n/bz;->i:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/google/n/bw;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->j:Lcom/google/n/bu;

    .line 94
    new-instance v0, Lcom/google/n/bx;

    const-string v1, "MESSAGE"

    const/16 v2, 0xa

    sget-object v3, Lcom/google/n/bz;->i:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/n/bx;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->k:Lcom/google/n/bu;

    .line 97
    new-instance v0, Lcom/google/n/by;

    const-string v1, "BYTES"

    const/16 v2, 0xb

    sget-object v3, Lcom/google/n/bz;->g:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/n/by;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->l:Lcom/google/n/bu;

    .line 100
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "UINT32"

    const/16 v2, 0xc

    sget-object v3, Lcom/google/n/bz;->a:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->m:Lcom/google/n/bu;

    .line 101
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "ENUM"

    const/16 v2, 0xd

    sget-object v3, Lcom/google/n/bz;->h:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->n:Lcom/google/n/bu;

    .line 102
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "SFIXED32"

    const/16 v2, 0xe

    sget-object v3, Lcom/google/n/bz;->a:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->o:Lcom/google/n/bu;

    .line 103
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "SFIXED64"

    const/16 v2, 0xf

    sget-object v3, Lcom/google/n/bz;->b:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->p:Lcom/google/n/bu;

    .line 104
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "SINT32"

    const/16 v2, 0x10

    sget-object v3, Lcom/google/n/bz;->a:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->q:Lcom/google/n/bu;

    .line 105
    new-instance v0, Lcom/google/n/bu;

    const-string v1, "SINT64"

    const/16 v2, 0x11

    sget-object v3, Lcom/google/n/bz;->b:Lcom/google/n/bz;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/n/bu;-><init>(Ljava/lang/String;ILcom/google/n/bz;I)V

    sput-object v0, Lcom/google/n/bu;->r:Lcom/google/n/bu;

    .line 79
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/google/n/bu;

    sget-object v1, Lcom/google/n/bu;->a:Lcom/google/n/bu;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/n/bu;->b:Lcom/google/n/bu;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/n/bu;->c:Lcom/google/n/bu;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/n/bu;->d:Lcom/google/n/bu;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/google/n/bu;->e:Lcom/google/n/bu;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/n/bu;->f:Lcom/google/n/bu;

    aput-object v1, v0, v7

    const/4 v1, 0x6

    sget-object v2, Lcom/google/n/bu;->g:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/n/bu;->h:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/n/bu;->i:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/n/bu;->j:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/n/bu;->k:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/n/bu;->l:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/n/bu;->m:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/n/bu;->n:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/n/bu;->o:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/n/bu;->p:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/n/bu;->q:Lcom/google/n/bu;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/n/bu;->r:Lcom/google/n/bu;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/n/bu;->u:[Lcom/google/n/bu;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILcom/google/n/bz;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/bz;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 108
    iput-object p3, p0, Lcom/google/n/bu;->s:Lcom/google/n/bz;

    .line 109
    iput p4, p0, Lcom/google/n/bu;->t:I

    .line 110
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/n/bu;
    .locals 1

    .prologue
    .line 79
    const-class v0, Lcom/google/n/bu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/n/bu;

    return-object v0
.end method

.method public static values()[Lcom/google/n/bu;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/n/bu;->u:[Lcom/google/n/bu;

    invoke-virtual {v0}, [Lcom/google/n/bu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/n/bu;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x1

    return v0
.end method
