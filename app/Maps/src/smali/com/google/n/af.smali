.class public Lcom/google/n/af;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 366
    sput-object v0, Lcom/google/n/af;->a:[B

    .line 367
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    .line 366
    return-void
.end method

.method static a(Lcom/google/n/at;)Z
    .locals 1

    .prologue
    .line 398
    instance-of v0, p0, Lcom/google/n/d;

    if-eqz v0, :cond_0

    .line 399
    invoke-static {}, Lcom/google/n/d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([B)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 165
    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/google/n/bs;->a([BII)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static b([B)Ljava/lang/String;
    .locals 3

    .prologue
    .line 184
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 185
    :catch_0
    move-exception v0

    .line 186
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "UTF-8 not supported?"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
