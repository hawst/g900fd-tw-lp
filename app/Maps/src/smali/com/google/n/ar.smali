.class Lcom/google/n/ar;
.super Lcom/google/n/f;
.source "PG"


# instance fields
.field public final c:[B

.field private d:I


# direct methods
.method constructor <init>([B)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/n/f;-><init>()V

    .line 221
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/n/ar;->d:I

    .line 34
    iput-object p1, p0, Lcom/google/n/ar;->c:[B

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(III)I
    .locals 11

    .prologue
    const/16 v10, -0xc

    const/16 v9, -0x20

    const/16 v4, -0x60

    const/4 v2, -0x1

    const/16 v8, -0x41

    .line 140
    invoke-virtual {p0}, Lcom/google/n/ar;->j()I

    move-result v0

    add-int v1, v0, p2

    .line 141
    iget-object v5, p0, Lcom/google/n/ar;->c:[B

    add-int v6, v1, p3

    if-eqz p1, :cond_11

    if-lt v1, v6, :cond_1

    move v2, p1

    :cond_0
    :goto_0
    return v2

    :cond_1
    int-to-byte v7, p1

    if-ge v7, v9, :cond_3

    const/16 v0, -0x3e

    if-lt v7, v0, :cond_0

    add-int/lit8 v0, v1, 0x1

    aget-byte v1, v5, v1

    if-gt v1, v8, :cond_0

    :cond_2
    :goto_1
    invoke-static {v5, v0, v6}, Lcom/google/n/bs;->a([BII)I

    move-result v0

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_3
    const/16 v0, -0x10

    if-ge v7, v0, :cond_a

    shr-int/lit8 v0, p1, 0x8

    xor-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    if-nez v0, :cond_7

    add-int/lit8 v3, v1, 0x1

    aget-byte v0, v5, v1

    if-lt v3, v6, :cond_6

    if-gt v7, v10, :cond_4

    if-le v0, v8, :cond_5

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    shl-int/lit8 v0, v0, 0x8

    xor-int v2, v7, v0

    goto :goto_0

    :cond_6
    move v1, v3

    :cond_7
    if-gt v0, v8, :cond_0

    if-ne v7, v9, :cond_8

    if-lt v0, v4, :cond_0

    :cond_8
    const/16 v3, -0x13

    if-ne v7, v3, :cond_9

    if-ge v0, v4, :cond_0

    :cond_9
    add-int/lit8 v0, v1, 0x1

    aget-byte v1, v5, v1

    if-le v1, v8, :cond_2

    goto :goto_0

    :cond_a
    shr-int/lit8 v0, p1, 0x8

    xor-int/lit8 v0, v0, -0x1

    int-to-byte v3, v0

    const/4 v0, 0x0

    if-nez v3, :cond_d

    add-int/lit8 v3, v1, 0x1

    aget-byte v1, v5, v1

    if-lt v3, v6, :cond_10

    if-gt v7, v10, :cond_b

    if-le v1, v8, :cond_c

    :cond_b
    move v0, v2

    goto :goto_2

    :cond_c
    shl-int/lit8 v0, v1, 0x8

    xor-int v2, v7, v0

    goto :goto_0

    :cond_d
    shr-int/lit8 v0, p1, 0x10

    int-to-byte v0, v0

    move v4, v3

    move v3, v1

    :goto_3
    if-nez v0, :cond_e

    add-int/lit8 v1, v3, 0x1

    aget-byte v0, v5, v3

    if-lt v1, v6, :cond_f

    invoke-static {v7, v4, v0}, Lcom/google/n/bs;->a(III)I

    move-result v2

    goto :goto_0

    :cond_e
    move v1, v3

    :cond_f
    if-gt v4, v8, :cond_0

    shl-int/lit8 v3, v7, 0x1c

    add-int/lit8 v4, v4, 0x70

    add-int/2addr v3, v4

    shr-int/lit8 v3, v3, 0x1e

    if-nez v3, :cond_0

    if-gt v0, v8, :cond_0

    add-int/lit8 v0, v1, 0x1

    aget-byte v1, v5, v1

    if-le v1, v8, :cond_2

    goto/16 :goto_0

    :cond_10
    move v4, v1

    goto :goto_3

    :cond_11
    move v0, v1

    goto :goto_1
.end method

.method public a()Lcom/google/n/g;
    .locals 1

    .prologue
    .line 287
    new-instance v0, Lcom/google/n/as;

    invoke-direct {v0, p0}, Lcom/google/n/as;-><init>(Lcom/google/n/ar;)V

    return-object v0
.end method

.method final a(Ljava/io/OutputStream;II)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/n/ar;->c:[B

    invoke-virtual {p0}, Lcom/google/n/ar;->j()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p1, v0, v1, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 121
    return-void
.end method

.method final a(Lcom/google/n/ar;II)Z
    .locals 7

    .prologue
    .line 193
    invoke-virtual {p1}, Lcom/google/n/ar;->b()I

    move-result v0

    if-le p3, v0, :cond_0

    .line 194
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 195
    invoke-virtual {p0}, Lcom/google/n/ar;->b()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Length too large: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    add-int v0, p2, p3

    invoke-virtual {p1}, Lcom/google/n/ar;->b()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 198
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 200
    invoke-virtual {p1}, Lcom/google/n/ar;->b()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x3b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Ran off end of other: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_1
    iget-object v2, p0, Lcom/google/n/ar;->c:[B

    .line 204
    iget-object v3, p1, Lcom/google/n/ar;->c:[B

    .line 205
    invoke-virtual {p0}, Lcom/google/n/ar;->j()I

    move-result v0

    add-int v4, v0, p3

    .line 206
    invoke-virtual {p0}, Lcom/google/n/ar;->j()I

    move-result v1

    .line 207
    invoke-virtual {p1}, Lcom/google/n/ar;->j()I

    move-result v0

    add-int/2addr v0, p2

    .line 208
    :goto_0
    if-ge v1, v4, :cond_3

    .line 209
    aget-byte v5, v2, v1

    aget-byte v6, v3, v0

    if-eq v5, v6, :cond_2

    .line 210
    const/4 v0, 0x0

    .line 213
    :goto_1
    return v0

    .line 208
    :cond_2
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public b()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/n/ar;->c:[B

    array-length v0, v0

    return v0
.end method

.method protected final b(III)I
    .locals 5

    .prologue
    .line 251
    iget-object v2, p0, Lcom/google/n/ar;->c:[B

    invoke-virtual {p0}, Lcom/google/n/ar;->j()I

    move-result v0

    add-int v1, v0, p2

    move v0, v1

    :goto_0
    add-int v3, v1, p3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, p1, 0x1f

    aget-byte v4, v2, v0

    add-int p1, v3, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return p1
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 126
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/n/ar;->c:[B

    invoke-virtual {p0}, Lcom/google/n/ar;->j()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/n/ar;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3, p1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    return-object v0
.end method

.method protected b([BIII)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/n/ar;->c:[B

    invoke-static {v0, p2, p1, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    return-void
.end method

.method public c(I)B
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/n/ar;->c:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/n/ar;->j()I

    move-result v0

    .line 135
    iget-object v1, p0, Lcom/google/n/ar;->c:[B

    invoke-virtual {p0}, Lcom/google/n/ar;->b()I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {v1, v0, v2}, Lcom/google/n/bs;->a([BII)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 149
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 175
    :goto_0
    return v0

    .line 152
    :cond_0
    instance-of v0, p1, Lcom/google/n/f;

    if-nez v0, :cond_1

    move v0, v2

    .line 153
    goto :goto_0

    .line 156
    :cond_1
    invoke-virtual {p0}, Lcom/google/n/ar;->b()I

    move-result v3

    move-object v0, p1

    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    if-eq v3, v0, :cond_2

    move v0, v2

    .line 157
    goto :goto_0

    .line 159
    :cond_2
    invoke-virtual {p0}, Lcom/google/n/ar;->b()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 160
    goto :goto_0

    .line 163
    :cond_3
    instance-of v0, p1, Lcom/google/n/ar;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 164
    check-cast v0, Lcom/google/n/ar;

    .line 167
    iget v1, p0, Lcom/google/n/ar;->d:I

    if-eqz v1, :cond_4

    iget v1, v0, Lcom/google/n/ar;->d:I

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/google/n/ar;->d:I

    iget v0, v0, Lcom/google/n/ar;->d:I

    if-eq v1, v0, :cond_4

    move v0, v2

    .line 170
    goto :goto_0

    .line 173
    :cond_4
    check-cast p1, Lcom/google/n/ar;

    invoke-virtual {p0}, Lcom/google/n/ar;->b()I

    move-result v0

    invoke-virtual {p0, p1, v2, v0}, Lcom/google/n/ar;->a(Lcom/google/n/ar;II)Z

    move-result v0

    goto :goto_0

    .line 174
    :cond_5
    instance-of v0, p1, Lcom/google/n/az;

    if-eqz v0, :cond_6

    .line 175
    invoke-virtual {p1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 177
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 179
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Has a new type of ByteString been created? Found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f()Lcom/google/n/j;
    .locals 1

    .prologue
    .line 279
    invoke-static {p0}, Lcom/google/n/j;->a(Lcom/google/n/ar;)Lcom/google/n/j;

    move-result-object v0

    return-object v0
.end method

.method protected final g()I
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x0

    return v0
.end method

.method protected final h()Z
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 231
    iget v0, p0, Lcom/google/n/ar;->d:I

    .line 233
    if-nez v0, :cond_1

    .line 234
    invoke-virtual {p0}, Lcom/google/n/ar;->b()I

    move-result v0

    .line 235
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v0}, Lcom/google/n/ar;->b(III)I

    move-result v0

    .line 236
    if-nez v0, :cond_0

    .line 237
    const/4 v0, 0x1

    .line 239
    :cond_0
    iput v0, p0, Lcom/google/n/ar;->d:I

    .line 241
    :cond_1
    return v0
.end method

.method protected final i()I
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/google/n/ar;->d:I

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/google/n/ar;->a()Lcom/google/n/g;

    move-result-object v0

    return-object v0
.end method

.method protected j()I
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x0

    return v0
.end method
