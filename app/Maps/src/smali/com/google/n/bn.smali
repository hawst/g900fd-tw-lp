.class public final Lcom/google/n/bn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Lcom/google/n/bn;


# instance fields
.field public final a:Lcom/google/n/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/google/n/bn;

    sget-object v1, Lcom/google/n/f;->a:Lcom/google/n/f;

    invoke-direct {v0, v1}, Lcom/google/n/bn;-><init>(Lcom/google/n/f;)V

    sput-object v0, Lcom/google/n/bn;->b:Lcom/google/n/bn;

    return-void
.end method

.method constructor <init>(Lcom/google/n/f;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    .line 59
    return-void
.end method

.method public static a()Lcom/google/n/bn;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/n/bn;->b:Lcom/google/n/bn;

    return-object v0
.end method

.method public static a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    iget-object v2, p1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1, v2}, Lcom/google/n/f;->a(Lcom/google/n/f;)Lcom/google/n/f;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/n/bn;-><init>(Lcom/google/n/f;)V

    return-object v0
.end method

.method public static b()Lcom/google/n/bo;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/n/bo;

    invoke-direct {v0}, Lcom/google/n/bo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 82
    if-ne p0, p1, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    .line 86
    :cond_0
    instance-of v0, p1, Lcom/google/n/bn;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    check-cast p1, Lcom/google/n/bn;

    iget-object v1, p1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0, v1}, Lcom/google/n/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->hashCode()I

    move-result v0

    return v0
.end method
