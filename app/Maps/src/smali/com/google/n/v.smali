.class public abstract Lcom/google/n/v;
.super Lcom/google/n/b;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lcom/google/n/t;",
        "BuilderType:",
        "Lcom/google/n/v;",
        ">",
        "Lcom/google/n/b",
        "<TBuilderType;>;"
    }
.end annotation


# instance fields
.field public I:Lcom/google/n/bn;

.field private final a:Lcom/google/n/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageType;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/n/t;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/n/b;-><init>()V

    .line 63
    invoke-static {}, Lcom/google/n/bn;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    .line 66
    iput-object p1, p0, Lcom/google/n/v;->a:Lcom/google/n/t;

    .line 67
    return-void
.end method

.method private c(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/v;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/j;",
            "Lcom/google/n/o;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 130
    const/4 v2, 0x0

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/google/n/v;->a:Lcom/google/n/t;

    invoke-virtual {v0}, Lcom/google/n/t;->a()Lcom/google/n/ax;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/n/ax;->a(Lcom/google/n/j;Lcom/google/n/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/t;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 139
    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0, v0}, Lcom/google/n/v;->a(Lcom/google/n/t;)Lcom/google/n/v;

    .line 143
    :cond_0
    return-object p0

    .line 135
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 136
    :try_start_1
    iget-object v0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    check-cast v0, Lcom/google/n/t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 137
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 139
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 140
    invoke-virtual {p0, v1}, Lcom/google/n/v;->a(Lcom/google/n/t;)Lcom/google/n/v;

    :cond_1
    throw v0

    .line 139
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/b;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/n/v;->c(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/v;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()Lcom/google/n/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/n/t;)Lcom/google/n/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)TBuilderType;"
        }
    .end annotation
.end method

.method public final synthetic b(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/au;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/n/v;->c(Lcom/google/n/j;Lcom/google/n/o;)Lcom/google/n/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/n/v;->f()Lcom/google/n/v;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lcom/google/n/b;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/n/v;->f()Lcom/google/n/v;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/google/n/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation

    .prologue
    .line 77
    .line 78
    iget-object v0, p0, Lcom/google/n/v;->a:Lcom/google/n/t;

    invoke-virtual {v0}, Lcom/google/n/t;->f()Lcom/google/n/au;

    move-result-object v0

    check-cast v0, Lcom/google/n/v;

    .line 79
    invoke-virtual {p0}, Lcom/google/n/v;->a()Lcom/google/n/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/v;->a(Lcom/google/n/t;)Lcom/google/n/v;

    .line 80
    return-object v0
.end method

.method public final g()Lcom/google/n/t;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageType;"
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/n/v;->a()Lcom/google/n/t;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/google/n/t;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    new-instance v0, Lcom/google/n/bm;

    invoke-direct {v0}, Lcom/google/n/bm;-><init>()V

    throw v0

    .line 92
    :cond_0
    return-object v0
.end method

.method public final synthetic h()Lcom/google/n/at;
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/n/v;->a()Lcom/google/n/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/n/t;->b()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/n/bm;

    invoke-direct {v0}, Lcom/google/n/bm;-><init>()V

    throw v0

    :cond_0
    return-object v0
.end method
