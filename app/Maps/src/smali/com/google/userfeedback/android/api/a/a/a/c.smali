.class public Lcom/google/userfeedback/android/api/a/a/a/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:[Lcom/google/userfeedback/android/api/a/a/a/d;


# instance fields
.field final a:Lcom/google/userfeedback/android/api/a/b/a;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 65
    const/16 v1, 0xa8

    new-array v1, v1, [Lcom/google/userfeedback/android/api/a/a/a/d;

    sput-object v1, Lcom/google/userfeedback/android/api/a/a/a/c;->b:[Lcom/google/userfeedback/android/api/a/a/a/d;

    move v3, v0

    move v1, v0

    .line 70
    :goto_0
    const/4 v0, 0x7

    if-gt v3, v0, :cond_1

    .line 71
    const/16 v0, 0x10

    :goto_1
    const/16 v2, 0x25

    if-ge v0, v2, :cond_0

    .line 72
    sget-object v4, Lcom/google/userfeedback/android/api/a/a/a/c;->b:[Lcom/google/userfeedback/android/api/a/a/a/d;

    add-int/lit8 v2, v1, 0x1

    new-instance v5, Lcom/google/userfeedback/android/api/a/a/a/d;

    shl-int/lit8 v6, v3, 0x8

    add-int/2addr v6, v0

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/google/userfeedback/android/api/a/a/a/d;-><init>(ILjava/lang/Object;)V

    aput-object v5, v4, v1

    .line 71
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_1

    .line 70
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 75
    :cond_1
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Lcom/google/userfeedback/android/api/a/b/a;

    invoke-direct {v0}, Lcom/google/userfeedback/android/api/a/b/a;-><init>()V

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/c;->c:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/Object;)Lcom/google/userfeedback/android/api/a/a/a/c;
    .locals 3

    .prologue
    .line 158
    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    if-nez p3, :cond_0

    .line 159
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v0, v0, 0x15

    and-int/lit16 v2, p1, 0xff

    add-int/lit8 v2, v2, -0x10

    add-int/2addr v0, v2

    sget-object v2, Lcom/google/userfeedback/android/api/a/a/a/c;->b:[Lcom/google/userfeedback/android/api/a/a/a/d;

    aget-object v0, v2, v0

    .line 158
    :goto_0
    invoke-virtual {v1, p2, v0}, Lcom/google/userfeedback/android/api/a/b/a;->a(ILjava/lang/Object;)V

    .line 161
    return-object p0

    .line 159
    :cond_0
    new-instance v0, Lcom/google/userfeedback/android/api/a/a/a/d;

    invoke-direct {v0, p1, p3}, Lcom/google/userfeedback/android/api/a/a/a/d;-><init>(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 244
    if-nez p1, :cond_1

    .line 256
    :cond_0
    :goto_0
    return v0

    .line 247
    :cond_1
    if-ne p0, p1, :cond_2

    .line 249
    const/4 v0, 0x1

    goto :goto_0

    .line 250
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 254
    check-cast p1, Lcom/google/userfeedback/android/api/a/a/a/c;

    .line 256
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    iget-object v1, p1, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, v1}, Lcom/google/userfeedback/android/api/a/b/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    .line 264
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0}, Lcom/google/userfeedback/android/api/a/b/a;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 206
    const-string v1, "ProtoBufType Name: "

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
