.class public final Lcom/google/userfeedback/android/api/R$color;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0a0255

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0a0256

.field public static final abc_input_method_navigation_guard:I = 0x7f0a0063

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0a0257

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0a0258

.field public static final abc_primary_text_material_dark:I = 0x7f0a0259

.field public static final abc_primary_text_material_light:I = 0x7f0a025a

.field public static final abc_search_url_text:I = 0x7f0a025b

.field public static final abc_search_url_text_normal:I = 0x7f0a0060

.field public static final abc_search_url_text_pressed:I = 0x7f0a0062

.field public static final abc_search_url_text_selected:I = 0x7f0a0061

.field public static final abc_secondary_text_material_dark:I = 0x7f0a025c

.field public static final abc_secondary_text_material_light:I = 0x7f0a025d

.field public static final accent_material_dark:I = 0x7f0a006f

.field public static final accent_material_light:I = 0x7f0a006e

.field public static final account_text_color:I = 0x7f0a0094

.field public static final activated_background:I = 0x7f0a00c2

.field public static final ad_background:I = 0x7f0a00c3

.field public static final ad_badge_foreground:I = 0x7f0a00c4

.field public static final add_accounts_text_color:I = 0x7f0a0093

.field public static final background_floating_material_dark:I = 0x7f0a0066

.field public static final background_floating_material_light:I = 0x7f0a0067

.field public static final background_grey:I = 0x7f0a00ac

.field public static final background_material_dark:I = 0x7f0a0064

.field public static final background_material_light:I = 0x7f0a0065

.field public static final background_transit_station:I = 0x7f0a00d4

.field public static final black:I = 0x7f0a0095

.field public static final black_almost_transparent:I = 0x7f0a00b1

.field public static final black_semi_transparent:I = 0x7f0a00b2

.field public static final black_transparent:I = 0x7f0a00b0

.field public static final blue:I = 0x7f0a00a3

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0a0076

.field public static final bright_foreground_disabled_material_light:I = 0x7f0a0077

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0a0078

.field public static final bright_foreground_inverse_material_light:I = 0x7f0a0079

.field public static final bright_foreground_material_dark:I = 0x7f0a0074

.field public static final bright_foreground_material_light:I = 0x7f0a0075

.field public static final bright_red:I = 0x7f0a00b6

.field public static final button_material_dark:I = 0x7f0a0070

.field public static final button_material_light:I = 0x7f0a0071

.field public static final car_700:I = 0x7f0a0000

.field public static final car_900:I = 0x7f0a0001

.field public static final car_blue_300:I = 0x7f0a0002

.field public static final car_blue_500:I = 0x7f0a0003

.field public static final car_blue_grey_900:I = 0x7f0a0004

.field public static final car_body1:I = 0x7f0a0005

.field public static final car_body1_dark:I = 0x7f0a0006

.field public static final car_body1_light:I = 0x7f0a0007

.field public static final car_body2:I = 0x7f0a0008

.field public static final car_body2_light:I = 0x7f0a0009

.field public static final car_caption:I = 0x7f0a000a

.field public static final car_caption_dark:I = 0x7f0a000b

.field public static final car_caption_light:I = 0x7f0a000c

.field public static final car_card:I = 0x7f0a000d

.field public static final car_card_dark:I = 0x7f0a000e

.field public static final car_card_light:I = 0x7f0a000f

.field public static final car_card_ripple_background:I = 0x7f0a0010

.field public static final car_card_ripple_background_dark:I = 0x7f0a0011

.field public static final car_card_ripple_background_light:I = 0x7f0a0012

.field public static final car_dark_blue_grey_1000:I = 0x7f0a0013

.field public static final car_dark_blue_grey_600:I = 0x7f0a0014

.field public static final car_dark_blue_grey_700:I = 0x7f0a0015

.field public static final car_dark_blue_grey_800:I = 0x7f0a0016

.field public static final car_dark_blue_grey_900:I = 0x7f0a0017

.field public static final car_dr_car_headline_dark:I = 0x7f0a0104

.field public static final car_dr_car_headline_light:I = 0x7f0a0105

.field public static final car_green_500:I = 0x7f0a0018

.field public static final car_green_700:I = 0x7f0a0019

.field public static final car_grey_100:I = 0x7f0a001a

.field public static final car_grey_1000:I = 0x7f0a001b

.field public static final car_grey_400:I = 0x7f0a001c

.field public static final car_grey_50:I = 0x7f0a001d

.field public static final car_grey_600:I = 0x7f0a001e

.field public static final car_grey_700:I = 0x7f0a001f

.field public static final car_grey_800:I = 0x7f0a0020

.field public static final car_grey_900:I = 0x7f0a0021

.field public static final car_headline1:I = 0x7f0a0022

.field public static final car_headline1_dark:I = 0x7f0a0023

.field public static final car_headline1_light:I = 0x7f0a0024

.field public static final car_headline2:I = 0x7f0a0025

.field public static final car_headline2_dark:I = 0x7f0a0026

.field public static final car_headline2_light:I = 0x7f0a0027

.field public static final car_light_blue_300:I = 0x7f0a0028

.field public static final car_light_blue_500:I = 0x7f0a0029

.field public static final car_light_blue_600:I = 0x7f0a002a

.field public static final car_light_blue_800:I = 0x7f0a002b

.field public static final car_list_divider:I = 0x7f0a002c

.field public static final car_list_divider_dark:I = 0x7f0a002d

.field public static final car_list_divider_light:I = 0x7f0a002e

.field public static final car_micro:I = 0x7f0a002f

.field public static final car_micro_dark:I = 0x7f0a0030

.field public static final car_micro_light:I = 0x7f0a0031

.field public static final car_overscroll_glow:I = 0x7f0a0032

.field public static final car_overscroll_glow_dark:I = 0x7f0a0033

.field public static final car_overscroll_glow_light:I = 0x7f0a0034

.field public static final car_red_500:I = 0x7f0a0035

.field public static final car_ripple:I = 0x7f0a0036

.field public static final car_snackbar:I = 0x7f0a0037

.field public static final car_snackbar_dark:I = 0x7f0a0038

.field public static final car_snackbar_light:I = 0x7f0a0039

.field public static final car_srt_background_inactive:I = 0x7f0a0106

.field public static final car_tint:I = 0x7f0a003a

.field public static final car_tint_dark:I = 0x7f0a003b

.field public static final car_tint_light:I = 0x7f0a003c

.field public static final car_title:I = 0x7f0a003d

.field public static final car_title_dark:I = 0x7f0a003e

.field public static final car_title_light:I = 0x7f0a003f

.field public static final car_white_1000:I = 0x7f0a0040

.field public static final car_yellow_500:I = 0x7f0a0041

.field public static final card_divider_grey:I = 0x7f0a009f

.field public static final card_divider_white:I = 0x7f0a00a2

.field public static final cardview_dark_background:I = 0x7f0a005d

.field public static final cardview_light_background:I = 0x7f0a005c

.field public static final cardview_shadow_end_color:I = 0x7f0a005f

.field public static final cardview_shadow_start_color:I = 0x7f0a005e

.field public static final common_action_bar_splitter:I = 0x7f0a004b

.field public static final common_signin_btn_dark_text_default:I = 0x7f0a0042

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f0a0044

.field public static final common_signin_btn_dark_text_focused:I = 0x7f0a0045

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f0a0043

.field public static final common_signin_btn_default_background:I = 0x7f0a004a

.field public static final common_signin_btn_light_text_default:I = 0x7f0a0046

.field public static final common_signin_btn_light_text_disabled:I = 0x7f0a0048

.field public static final common_signin_btn_light_text_focused:I = 0x7f0a0049

.field public static final common_signin_btn_light_text_pressed:I = 0x7f0a0047

.field public static final common_signin_btn_text_dark:I = 0x7f0a025e

.field public static final common_signin_btn_text_light:I = 0x7f0a025f

.field public static final confidential_red:I = 0x7f0a00b9

.field public static final dark_blue:I = 0x7f0a00b7

.field public static final dark_burgundy:I = 0x7f0a00b5

.field public static final default_account_details_color:I = 0x7f0a0091

.field public static final dialog_background:I = 0x7f0a00bd

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0a007c

.field public static final dim_foreground_disabled_material_light:I = 0x7f0a007d

.field public static final dim_foreground_material_dark:I = 0x7f0a007a

.field public static final dim_foreground_material_light:I = 0x7f0a007b

.field public static final directions_elevation_fill_color:I = 0x7f0a00f4

.field public static final directions_elevation_scale_text_color:I = 0x7f0a00f5

.field public static final directions_elevation_stroke_color:I = 0x7f0a00f3

.field public static final directions_express_text:I = 0x7f0a00f0

.field public static final directions_greentraffic_nightmode_text:I = 0x7f0a00e7

.field public static final directions_greentraffic_text:I = 0x7f0a00e6

.field public static final directions_lightgray_background:I = 0x7f0a00ef

.field public static final directions_redtraffic_nightmode_text:I = 0x7f0a00eb

.field public static final directions_redtraffic_text:I = 0x7f0a00ea

.field public static final directions_unknowntraffic_nightmode_text:I = 0x7f0a00ed

.field public static final directions_unknowntraffic_text:I = 0x7f0a00ec

.field public static final directions_white_background:I = 0x7f0a00ee

.field public static final directions_yellowtraffic_nightmode_text:I = 0x7f0a00e9

.field public static final directions_yellowtraffic_text:I = 0x7f0a00e8

.field public static final divider_grey:I = 0x7f0a009d

.field public static final divider_grey_nightmode:I = 0x7f0a00e4

.field public static final follow_button_font_selector:I = 0x7f0a0260

.field public static final generic_button_font_selector:I = 0x7f0a0261

.field public static final gf_foreground_content:I = 0x7f0a005b

.field public static final gf_header_background:I = 0x7f0a0059

.field public static final gf_shadow:I = 0x7f0a005a

.field public static final gplus_font_color:I = 0x7f0a00fe

.field public static final gplus_font_color_disabled:I = 0x7f0a00ff

.field public static final green:I = 0x7f0a00a5

.field public static final green_nav:I = 0x7f0a00a6

.field public static final guide_header_default_background:I = 0x7f0a0103

.field public static final hats_entry_point_background:I = 0x7f0a010b

.field public static final heads_up_navigation_notification_background:I = 0x7f0a00e5

.field public static final highlighted_text_material_dark:I = 0x7f0a0080

.field public static final highlighted_text_material_light:I = 0x7f0a0081

.field public static final hint_foreground_material_dark:I = 0x7f0a007e

.field public static final hint_foreground_material_light:I = 0x7f0a007f

.field public static final image_placeholder_background:I = 0x7f0a00f8

.field public static final inverse_primary_grey:I = 0x7f0a009a

.field public static final inverse_secondary_grey:I = 0x7f0a009b

.field public static final inverse_tertiary_grey:I = 0x7f0a009c

.field public static final kitkat_blue:I = 0x7f0a00a4

.field public static final layers_button_text:I = 0x7f0a0262

.field public static final layers_buttondark_text:I = 0x7f0a0263

.field public static final light_divider_grey:I = 0x7f0a009e

.field public static final light_grey:I = 0x7f0a00ab

.field public static final light_grey_transparent:I = 0x7f0a00af

.field public static final link_text_material_dark:I = 0x7f0a0082

.field public static final link_text_material_light:I = 0x7f0a0083

.field public static final location_accuracy_fill:I = 0x7f0a0107

.field public static final location_accuracy_line:I = 0x7f0a0108

.field public static final login_background:I = 0x7f0a00bf

.field public static final manage_accounts_text_color:I = 0x7f0a0092

.field public static final map_copyright_background_end:I = 0x7f0a00c7

.field public static final map_copyright_background_start:I = 0x7f0a00c6

.field public static final map_copyright_default:I = 0x7f0a00ca

.field public static final map_copyright_hybrid:I = 0x7f0a00cb

.field public static final map_copyright_hybrid_background_end:I = 0x7f0a00c9

.field public static final map_copyright_hybrid_background_start:I = 0x7f0a00c8

.field public static final map_copyright_night:I = 0x7f0a00cc

.field public static final map_tablet_mask:I = 0x7f0a00cd

.field public static final material_blue_grey_800:I = 0x7f0a008e

.field public static final material_blue_grey_900:I = 0x7f0a008f

.field public static final material_blue_grey_950:I = 0x7f0a0090

.field public static final material_deep_teal_200:I = 0x7f0a008c

.field public static final material_deep_teal_500:I = 0x7f0a008d

.field public static final menu_divider_grey:I = 0x7f0a00a0

.field public static final monroe_ad:I = 0x7f0a00c5

.field public static final navigation_lane_icon_off_route:I = 0x7f0a00db

.field public static final navigation_lane_icon_off_route_night:I = 0x7f0a00de

.field public static final navigation_lane_icon_recommended:I = 0x7f0a00da

.field public static final navigation_lane_icon_recommended_night:I = 0x7f0a00dd

.field public static final navigation_lane_rmi_privacy_notice:I = 0x7f0a00e0

.field public static final navigation_lane_tickmark:I = 0x7f0a00dc

.field public static final navigation_lane_tickmark_night:I = 0x7f0a00df

.field public static final navigation_next_turn_text:I = 0x7f0a00d8

.field public static final navigation_next_turn_text_nightmode:I = 0x7f0a00d9

.field public static final navigation_primary_text:I = 0x7f0a00d5

.field public static final navigation_secondary_text:I = 0x7f0a00d6

.field public static final navigation_stepslider_arrow:I = 0x7f0a00e1

.field public static final navigation_text_divider:I = 0x7f0a00e2

.field public static final navigation_warning_text:I = 0x7f0a00d7

.field public static final new_location_accuracy_fill:I = 0x7f0a0109

.field public static final new_location_accuracy_line:I = 0x7f0a010a

.field public static final nightmode_background:I = 0x7f0a00e3

.field public static final no_type_grey:I = 0x7f0a0099

.field public static final off_white:I = 0x7f0a00ae

.field public static final orange:I = 0x7f0a00a7

.field public static final owner_response_border:I = 0x7f0a0100

.field public static final page_background:I = 0x7f0a00ba

.field public static final pale_grey:I = 0x7f0a00ad

.field public static final place_permanently_closed_text:I = 0x7f0a00d3

.field public static final primary_dark_material_dark:I = 0x7f0a006a

.field public static final primary_dark_material_light:I = 0x7f0a006b

.field public static final primary_grey:I = 0x7f0a0096

.field public static final primary_material_dark:I = 0x7f0a0068

.field public static final primary_material_light:I = 0x7f0a0069

.field public static final primary_text_default_material_dark:I = 0x7f0a0086

.field public static final primary_text_default_material_light:I = 0x7f0a0084

.field public static final primary_text_disabled_material_dark:I = 0x7f0a008a

.field public static final primary_text_disabled_material_light:I = 0x7f0a0088

.field public static final qu_amber_100:I = 0x7f0a0238

.field public static final qu_amber_200:I = 0x7f0a0239

.field public static final qu_amber_300:I = 0x7f0a023a

.field public static final qu_amber_400:I = 0x7f0a023b

.field public static final qu_amber_50:I = 0x7f0a0237

.field public static final qu_amber_500:I = 0x7f0a0236

.field public static final qu_amber_600:I = 0x7f0a023c

.field public static final qu_amber_700:I = 0x7f0a023d

.field public static final qu_amber_800:I = 0x7f0a023e

.field public static final qu_amber_900:I = 0x7f0a023f

.field public static final qu_amber_accent_100:I = 0x7f0a0240

.field public static final qu_amber_accent_200:I = 0x7f0a0241

.field public static final qu_amber_accent_400:I = 0x7f0a0242

.field public static final qu_amber_accent_700:I = 0x7f0a0243

.field public static final qu_black_alpha_16:I = 0x7f0a024a

.field public static final qu_black_alpha_40:I = 0x7f0a024b

.field public static final qu_black_alpha_54:I = 0x7f0a024c

.field public static final qu_black_alpha_70:I = 0x7f0a024d

.field public static final qu_black_alpha_75:I = 0x7f0a024e

.field public static final qu_black_alpha_87:I = 0x7f0a024f

.field public static final qu_blue_grey_100:I = 0x7f0a0204

.field public static final qu_blue_grey_200:I = 0x7f0a0205

.field public static final qu_blue_grey_300:I = 0x7f0a0206

.field public static final qu_blue_grey_400:I = 0x7f0a0207

.field public static final qu_blue_grey_50:I = 0x7f0a0203

.field public static final qu_blue_grey_500:I = 0x7f0a0202

.field public static final qu_blue_grey_600:I = 0x7f0a0208

.field public static final qu_blue_grey_700:I = 0x7f0a0209

.field public static final qu_blue_grey_800:I = 0x7f0a020a

.field public static final qu_blue_grey_900:I = 0x7f0a020b

.field public static final qu_brown_100:I = 0x7f0a01ee

.field public static final qu_brown_200:I = 0x7f0a01ef

.field public static final qu_brown_300:I = 0x7f0a01f0

.field public static final qu_brown_400:I = 0x7f0a01f1

.field public static final qu_brown_50:I = 0x7f0a01ed

.field public static final qu_brown_500:I = 0x7f0a01ec

.field public static final qu_brown_600:I = 0x7f0a01f2

.field public static final qu_brown_700:I = 0x7f0a01f3

.field public static final qu_brown_800:I = 0x7f0a01f4

.field public static final qu_brown_900:I = 0x7f0a01f5

.field public static final qu_deep_orange_100:I = 0x7f0a01e0

.field public static final qu_deep_orange_200:I = 0x7f0a01e1

.field public static final qu_deep_orange_300:I = 0x7f0a01e2

.field public static final qu_deep_orange_400:I = 0x7f0a01e3

.field public static final qu_deep_orange_50:I = 0x7f0a01df

.field public static final qu_deep_orange_500:I = 0x7f0a01de

.field public static final qu_deep_orange_600:I = 0x7f0a01e4

.field public static final qu_deep_orange_700:I = 0x7f0a01e5

.field public static final qu_deep_orange_800:I = 0x7f0a01e6

.field public static final qu_deep_orange_900:I = 0x7f0a01e7

.field public static final qu_deep_orange_accent_100:I = 0x7f0a01e8

.field public static final qu_deep_orange_accent_200:I = 0x7f0a01e9

.field public static final qu_deep_orange_accent_400:I = 0x7f0a01ea

.field public static final qu_deep_orange_accent_700:I = 0x7f0a01eb

.field public static final qu_deep_purple_100:I = 0x7f0a0138

.field public static final qu_deep_purple_200:I = 0x7f0a0139

.field public static final qu_deep_purple_300:I = 0x7f0a013a

.field public static final qu_deep_purple_400:I = 0x7f0a013b

.field public static final qu_deep_purple_50:I = 0x7f0a0137

.field public static final qu_deep_purple_500:I = 0x7f0a0136

.field public static final qu_deep_purple_600:I = 0x7f0a013c

.field public static final qu_deep_purple_700:I = 0x7f0a013d

.field public static final qu_deep_purple_800:I = 0x7f0a013e

.field public static final qu_deep_purple_900:I = 0x7f0a013f

.field public static final qu_deep_purple_accent_100:I = 0x7f0a0140

.field public static final qu_deep_purple_accent_200:I = 0x7f0a0141

.field public static final qu_deep_purple_accent_400:I = 0x7f0a0142

.field public static final qu_deep_purple_accent_700:I = 0x7f0a0143

.field public static final qu_deep_teal_100:I = 0x7f0a017e

.field public static final qu_deep_teal_200:I = 0x7f0a017f

.field public static final qu_deep_teal_300:I = 0x7f0a0180

.field public static final qu_deep_teal_400:I = 0x7f0a0181

.field public static final qu_deep_teal_50:I = 0x7f0a017d

.field public static final qu_deep_teal_500:I = 0x7f0a017c

.field public static final qu_deep_teal_600:I = 0x7f0a0182

.field public static final qu_deep_teal_700:I = 0x7f0a0183

.field public static final qu_deep_teal_800:I = 0x7f0a0184

.field public static final qu_deep_teal_900:I = 0x7f0a0185

.field public static final qu_deep_teal_accent_100:I = 0x7f0a0186

.field public static final qu_deep_teal_accent_200:I = 0x7f0a0187

.field public static final qu_deep_teal_accent_400:I = 0x7f0a0188

.field public static final qu_deep_teal_accent_700:I = 0x7f0a0189

.field public static final qu_gc_rating_red:I = 0x7f0a0244

.field public static final qu_google_blue_100:I = 0x7f0a0154

.field public static final qu_google_blue_200:I = 0x7f0a0155

.field public static final qu_google_blue_300:I = 0x7f0a0156

.field public static final qu_google_blue_400:I = 0x7f0a0157

.field public static final qu_google_blue_50:I = 0x7f0a0153

.field public static final qu_google_blue_500:I = 0x7f0a0152

.field public static final qu_google_blue_600:I = 0x7f0a0158

.field public static final qu_google_blue_700:I = 0x7f0a0159

.field public static final qu_google_blue_800:I = 0x7f0a015a

.field public static final qu_google_blue_900:I = 0x7f0a015b

.field public static final qu_google_blue_accent_100:I = 0x7f0a015c

.field public static final qu_google_blue_accent_200:I = 0x7f0a015d

.field public static final qu_google_blue_accent_400:I = 0x7f0a015e

.field public static final qu_google_blue_accent_700:I = 0x7f0a015f

.field public static final qu_google_green_100:I = 0x7f0a018c

.field public static final qu_google_green_200:I = 0x7f0a018d

.field public static final qu_google_green_300:I = 0x7f0a018e

.field public static final qu_google_green_400:I = 0x7f0a018f

.field public static final qu_google_green_50:I = 0x7f0a018b

.field public static final qu_google_green_500:I = 0x7f0a018a

.field public static final qu_google_green_600:I = 0x7f0a0190

.field public static final qu_google_green_700:I = 0x7f0a0191

.field public static final qu_google_green_800:I = 0x7f0a0192

.field public static final qu_google_green_900:I = 0x7f0a0193

.field public static final qu_google_green_accent_100:I = 0x7f0a0194

.field public static final qu_google_green_accent_200:I = 0x7f0a0195

.field public static final qu_google_green_accent_400:I = 0x7f0a0196

.field public static final qu_google_green_accent_700:I = 0x7f0a0197

.field public static final qu_google_red_100:I = 0x7f0a010e

.field public static final qu_google_red_200:I = 0x7f0a010f

.field public static final qu_google_red_300:I = 0x7f0a0110

.field public static final qu_google_red_400:I = 0x7f0a0111

.field public static final qu_google_red_50:I = 0x7f0a010d

.field public static final qu_google_red_500:I = 0x7f0a010c

.field public static final qu_google_red_600:I = 0x7f0a0112

.field public static final qu_google_red_700:I = 0x7f0a0113

.field public static final qu_google_red_800:I = 0x7f0a0114

.field public static final qu_google_red_900:I = 0x7f0a0115

.field public static final qu_google_red_accent_100:I = 0x7f0a0116

.field public static final qu_google_red_accent_200:I = 0x7f0a0117

.field public static final qu_google_red_accent_400:I = 0x7f0a0118

.field public static final qu_google_red_accent_700:I = 0x7f0a0119

.field public static final qu_google_yellow_100:I = 0x7f0a01c4

.field public static final qu_google_yellow_200:I = 0x7f0a01c5

.field public static final qu_google_yellow_300:I = 0x7f0a01c6

.field public static final qu_google_yellow_400:I = 0x7f0a01c7

.field public static final qu_google_yellow_50:I = 0x7f0a01c3

.field public static final qu_google_yellow_500:I = 0x7f0a01c2

.field public static final qu_google_yellow_600:I = 0x7f0a01c8

.field public static final qu_google_yellow_700:I = 0x7f0a01c9

.field public static final qu_google_yellow_800:I = 0x7f0a01ca

.field public static final qu_google_yellow_900:I = 0x7f0a01cb

.field public static final qu_google_yellow_accent_100:I = 0x7f0a01cc

.field public static final qu_google_yellow_accent_200:I = 0x7f0a01cd

.field public static final qu_google_yellow_accent_400:I = 0x7f0a01ce

.field public static final qu_google_yellow_accent_700:I = 0x7f0a01cf

.field public static final qu_grey_100:I = 0x7f0a01f8

.field public static final qu_grey_200:I = 0x7f0a01f9

.field public static final qu_grey_200_alpha_75:I = 0x7f0a0254

.field public static final qu_grey_300:I = 0x7f0a01fa

.field public static final qu_grey_400:I = 0x7f0a01fb

.field public static final qu_grey_50:I = 0x7f0a01f7

.field public static final qu_grey_500:I = 0x7f0a01f6

.field public static final qu_grey_600:I = 0x7f0a01fc

.field public static final qu_grey_700:I = 0x7f0a01fd

.field public static final qu_grey_800:I = 0x7f0a01fe

.field public static final qu_grey_900:I = 0x7f0a01ff

.field public static final qu_grey_alpha_50:I = 0x7f0a0250

.field public static final qu_grey_black_1000:I = 0x7f0a0200

.field public static final qu_grey_white_1000:I = 0x7f0a0201

.field public static final qu_indigo_100:I = 0x7f0a0146

.field public static final qu_indigo_200:I = 0x7f0a0147

.field public static final qu_indigo_300:I = 0x7f0a0148

.field public static final qu_indigo_400:I = 0x7f0a0149

.field public static final qu_indigo_50:I = 0x7f0a0145

.field public static final qu_indigo_500:I = 0x7f0a0144

.field public static final qu_indigo_600:I = 0x7f0a014a

.field public static final qu_indigo_700:I = 0x7f0a014b

.field public static final qu_indigo_800:I = 0x7f0a014c

.field public static final qu_indigo_900:I = 0x7f0a014d

.field public static final qu_indigo_accent_100:I = 0x7f0a014e

.field public static final qu_indigo_accent_200:I = 0x7f0a014f

.field public static final qu_indigo_accent_400:I = 0x7f0a0150

.field public static final qu_indigo_accent_700:I = 0x7f0a0151

.field public static final qu_light_blue_100:I = 0x7f0a0162

.field public static final qu_light_blue_200:I = 0x7f0a0163

.field public static final qu_light_blue_300:I = 0x7f0a0164

.field public static final qu_light_blue_400:I = 0x7f0a0165

.field public static final qu_light_blue_50:I = 0x7f0a0161

.field public static final qu_light_blue_500:I = 0x7f0a0160

.field public static final qu_light_blue_600:I = 0x7f0a0166

.field public static final qu_light_blue_700:I = 0x7f0a0167

.field public static final qu_light_blue_800:I = 0x7f0a0168

.field public static final qu_light_blue_900:I = 0x7f0a0169

.field public static final qu_light_blue_accent_100:I = 0x7f0a016a

.field public static final qu_light_blue_accent_200:I = 0x7f0a016b

.field public static final qu_light_blue_accent_400:I = 0x7f0a016c

.field public static final qu_light_blue_accent_700:I = 0x7f0a016d

.field public static final qu_light_green_100:I = 0x7f0a019a

.field public static final qu_light_green_200:I = 0x7f0a019b

.field public static final qu_light_green_300:I = 0x7f0a019c

.field public static final qu_light_green_400:I = 0x7f0a019d

.field public static final qu_light_green_50:I = 0x7f0a0199

.field public static final qu_light_green_500:I = 0x7f0a0198

.field public static final qu_light_green_600:I = 0x7f0a019e

.field public static final qu_light_green_700:I = 0x7f0a019f

.field public static final qu_light_green_800:I = 0x7f0a01a0

.field public static final qu_light_green_900:I = 0x7f0a01a1

.field public static final qu_light_green_accent_100:I = 0x7f0a01a2

.field public static final qu_light_green_accent_200:I = 0x7f0a01a3

.field public static final qu_light_green_accent_400:I = 0x7f0a01a4

.field public static final qu_light_green_accent_700:I = 0x7f0a01a5

.field public static final qu_lime_100:I = 0x7f0a01a8

.field public static final qu_lime_200:I = 0x7f0a01a9

.field public static final qu_lime_300:I = 0x7f0a01aa

.field public static final qu_lime_400:I = 0x7f0a01ab

.field public static final qu_lime_50:I = 0x7f0a01a7

.field public static final qu_lime_500:I = 0x7f0a01a6

.field public static final qu_lime_600:I = 0x7f0a01ac

.field public static final qu_lime_700:I = 0x7f0a01ad

.field public static final qu_lime_800:I = 0x7f0a01ae

.field public static final qu_lime_900:I = 0x7f0a01af

.field public static final qu_lime_accent_100:I = 0x7f0a01b0

.field public static final qu_lime_accent_200:I = 0x7f0a01b1

.field public static final qu_lime_accent_400:I = 0x7f0a01b2

.field public static final qu_lime_accent_700:I = 0x7f0a01b3

.field public static final qu_oob_sky_blue:I = 0x7f0a0246

.field public static final qu_orange_100:I = 0x7f0a01d2

.field public static final qu_orange_200:I = 0x7f0a01d3

.field public static final qu_orange_300:I = 0x7f0a01d4

.field public static final qu_orange_400:I = 0x7f0a01d5

.field public static final qu_orange_50:I = 0x7f0a01d1

.field public static final qu_orange_500:I = 0x7f0a01d0

.field public static final qu_orange_600:I = 0x7f0a01d6

.field public static final qu_orange_700:I = 0x7f0a01d7

.field public static final qu_orange_800:I = 0x7f0a01d8

.field public static final qu_orange_900:I = 0x7f0a01d9

.field public static final qu_orange_accent_100:I = 0x7f0a01da

.field public static final qu_orange_accent_200:I = 0x7f0a01db

.field public static final qu_orange_accent_400:I = 0x7f0a01dc

.field public static final qu_orange_accent_700:I = 0x7f0a01dd

.field public static final qu_pink_100:I = 0x7f0a011c

.field public static final qu_pink_200:I = 0x7f0a011d

.field public static final qu_pink_300:I = 0x7f0a011e

.field public static final qu_pink_400:I = 0x7f0a011f

.field public static final qu_pink_50:I = 0x7f0a011b

.field public static final qu_pink_500:I = 0x7f0a011a

.field public static final qu_pink_600:I = 0x7f0a0120

.field public static final qu_pink_700:I = 0x7f0a0121

.field public static final qu_pink_800:I = 0x7f0a0122

.field public static final qu_pink_900:I = 0x7f0a0123

.field public static final qu_pink_accent_100:I = 0x7f0a0124

.field public static final qu_pink_accent_200:I = 0x7f0a0125

.field public static final qu_pink_accent_400:I = 0x7f0a0126

.field public static final qu_pink_accent_700:I = 0x7f0a0127

.field public static final qu_purple_100:I = 0x7f0a012a

.field public static final qu_purple_200:I = 0x7f0a012b

.field public static final qu_purple_300:I = 0x7f0a012c

.field public static final qu_purple_400:I = 0x7f0a012d

.field public static final qu_purple_50:I = 0x7f0a0129

.field public static final qu_purple_500:I = 0x7f0a0128

.field public static final qu_purple_600:I = 0x7f0a012e

.field public static final qu_purple_700:I = 0x7f0a012f

.field public static final qu_purple_800:I = 0x7f0a0130

.field public static final qu_purple_900:I = 0x7f0a0131

.field public static final qu_purple_accent_100:I = 0x7f0a0132

.field public static final qu_purple_accent_200:I = 0x7f0a0133

.field public static final qu_purple_accent_400:I = 0x7f0a0134

.field public static final qu_purple_accent_700:I = 0x7f0a0135

.field public static final qu_riddler_question_dark_grey:I = 0x7f0a0249

.field public static final qu_riddler_snackbar_background:I = 0x7f0a0248

.field public static final qu_teal_100:I = 0x7f0a0170

.field public static final qu_teal_200:I = 0x7f0a0171

.field public static final qu_teal_300:I = 0x7f0a0172

.field public static final qu_teal_400:I = 0x7f0a0173

.field public static final qu_teal_50:I = 0x7f0a016f

.field public static final qu_teal_500:I = 0x7f0a016e

.field public static final qu_teal_600:I = 0x7f0a0174

.field public static final qu_teal_700:I = 0x7f0a0175

.field public static final qu_teal_800:I = 0x7f0a0176

.field public static final qu_teal_900:I = 0x7f0a0177

.field public static final qu_teal_accent_100:I = 0x7f0a0178

.field public static final qu_teal_accent_200:I = 0x7f0a0179

.field public static final qu_teal_accent_400:I = 0x7f0a017a

.field public static final qu_teal_accent_700:I = 0x7f0a017b

.field public static final qu_tutorial_background:I = 0x7f0a0247

.field public static final qu_vanilla_blue_100:I = 0x7f0a021c

.field public static final qu_vanilla_blue_200:I = 0x7f0a021d

.field public static final qu_vanilla_blue_300:I = 0x7f0a021e

.field public static final qu_vanilla_blue_400:I = 0x7f0a021f

.field public static final qu_vanilla_blue_50:I = 0x7f0a021b

.field public static final qu_vanilla_blue_500:I = 0x7f0a021a

.field public static final qu_vanilla_blue_600:I = 0x7f0a0220

.field public static final qu_vanilla_blue_700:I = 0x7f0a0221

.field public static final qu_vanilla_blue_800:I = 0x7f0a0222

.field public static final qu_vanilla_blue_900:I = 0x7f0a0223

.field public static final qu_vanilla_blue_accent_100:I = 0x7f0a0224

.field public static final qu_vanilla_blue_accent_200:I = 0x7f0a0225

.field public static final qu_vanilla_blue_accent_400:I = 0x7f0a0226

.field public static final qu_vanilla_blue_accent_700:I = 0x7f0a0227

.field public static final qu_vanilla_green_100:I = 0x7f0a022a

.field public static final qu_vanilla_green_200:I = 0x7f0a022b

.field public static final qu_vanilla_green_300:I = 0x7f0a022c

.field public static final qu_vanilla_green_400:I = 0x7f0a022d

.field public static final qu_vanilla_green_50:I = 0x7f0a0229

.field public static final qu_vanilla_green_500:I = 0x7f0a0228

.field public static final qu_vanilla_green_600:I = 0x7f0a022e

.field public static final qu_vanilla_green_700:I = 0x7f0a022f

.field public static final qu_vanilla_green_800:I = 0x7f0a0230

.field public static final qu_vanilla_green_900:I = 0x7f0a0231

.field public static final qu_vanilla_green_accent_100:I = 0x7f0a0232

.field public static final qu_vanilla_green_accent_200:I = 0x7f0a0233

.field public static final qu_vanilla_green_accent_400:I = 0x7f0a0234

.field public static final qu_vanilla_green_accent_700:I = 0x7f0a0235

.field public static final qu_vanilla_red_100:I = 0x7f0a020e

.field public static final qu_vanilla_red_200:I = 0x7f0a020f

.field public static final qu_vanilla_red_300:I = 0x7f0a0210

.field public static final qu_vanilla_red_400:I = 0x7f0a0211

.field public static final qu_vanilla_red_50:I = 0x7f0a020d

.field public static final qu_vanilla_red_500:I = 0x7f0a020c

.field public static final qu_vanilla_red_600:I = 0x7f0a0212

.field public static final qu_vanilla_red_700:I = 0x7f0a0213

.field public static final qu_vanilla_red_800:I = 0x7f0a0214

.field public static final qu_vanilla_red_900:I = 0x7f0a0215

.field public static final qu_vanilla_red_accent_100:I = 0x7f0a0216

.field public static final qu_vanilla_red_accent_200:I = 0x7f0a0217

.field public static final qu_vanilla_red_accent_400:I = 0x7f0a0218

.field public static final qu_vanilla_red_accent_700:I = 0x7f0a0219

.field public static final qu_white_alpha_20:I = 0x7f0a0253

.field public static final qu_white_alpha_54:I = 0x7f0a0251

.field public static final qu_white_alpha_87:I = 0x7f0a0252

.field public static final qu_yellow_100:I = 0x7f0a01b6

.field public static final qu_yellow_200:I = 0x7f0a01b7

.field public static final qu_yellow_300:I = 0x7f0a01b8

.field public static final qu_yellow_400:I = 0x7f0a01b9

.field public static final qu_yellow_50:I = 0x7f0a01b5

.field public static final qu_yellow_500:I = 0x7f0a01b4

.field public static final qu_yellow_600:I = 0x7f0a01ba

.field public static final qu_yellow_700:I = 0x7f0a01bb

.field public static final qu_yellow_800:I = 0x7f0a01bc

.field public static final qu_yellow_900:I = 0x7f0a01bd

.field public static final qu_yellow_accent_100:I = 0x7f0a01be

.field public static final qu_yellow_accent_200:I = 0x7f0a01bf

.field public static final qu_yellow_accent_400:I = 0x7f0a01c0

.field public static final qu_yellow_accent_700:I = 0x7f0a01c1

.field public static final qu_zagat_dark_red:I = 0x7f0a0245

.field public static final quantum_button_font_selector:I = 0x7f0a0264

.field public static final rap_clickable_text:I = 0x7f0a0101

.field public static final rap_crossed_text:I = 0x7f0a0102

.field public static final rating_orange:I = 0x7f0a00a9

.field public static final rating_red:I = 0x7f0a00aa

.field public static final red:I = 0x7f0a00a8

.field public static final ripple_material_dark:I = 0x7f0a006c

.field public static final ripple_material_light:I = 0x7f0a006d

.field public static final scalebar_on_dark_background:I = 0x7f0a00fa

.field public static final scalebar_on_light_background:I = 0x7f0a00f9

.field public static final scalebar_shadow_for_dark_background:I = 0x7f0a00fc

.field public static final scalebar_shadow_for_light_background:I = 0x7f0a00fb

.field public static final search_list_item_score_text:I = 0x7f0a00d2

.field public static final secondary_grey:I = 0x7f0a0097

.field public static final secondary_text_default_material_dark:I = 0x7f0a0087

.field public static final secondary_text_default_material_light:I = 0x7f0a0085

.field public static final secondary_text_disabled_material_dark:I = 0x7f0a008b

.field public static final secondary_text_disabled_material_light:I = 0x7f0a0089

.field public static final selected_background:I = 0x7f0a00c0

.field public static final selected_background_transparent:I = 0x7f0a00c1

.field public static final shadow_background_end:I = 0x7f0a00cf

.field public static final shadow_background_nightmode_end:I = 0x7f0a00d1

.field public static final shadow_background_nightmode_start:I = 0x7f0a00d0

.field public static final shadow_background_start:I = 0x7f0a00ce

.field public static final sheet_background_end:I = 0x7f0a00bc

.field public static final sheet_background_start:I = 0x7f0a00bb

.field public static final sign_in_blue_button_dark:I = 0x7f0a00f7

.field public static final sign_in_blue_button_light:I = 0x7f0a00f6

.field public static final startpage_compacticonwithheader_text_shadow:I = 0x7f0a00fd

.field public static final switch_thumb_normal_material_dark:I = 0x7f0a0072

.field public static final switch_thumb_normal_material_light:I = 0x7f0a0073

.field public static final tertiary_grey:I = 0x7f0a0098

.field public static final transit_notice_severity_information:I = 0x7f0a00f2

.field public static final transit_notice_severity_warning:I = 0x7f0a00f1

.field public static final tutorial_background:I = 0x7f0a00be

.field public static final wallet_bright_foreground_disabled_holo_light:I = 0x7f0a0051

.field public static final wallet_bright_foreground_holo_dark:I = 0x7f0a004c

.field public static final wallet_bright_foreground_holo_light:I = 0x7f0a0052

.field public static final wallet_dim_foreground_disabled_holo_dark:I = 0x7f0a004e

.field public static final wallet_dim_foreground_holo_dark:I = 0x7f0a004d

.field public static final wallet_dim_foreground_inverse_disabled_holo_dark:I = 0x7f0a0050

.field public static final wallet_dim_foreground_inverse_holo_dark:I = 0x7f0a004f

.field public static final wallet_highlighted_text_holo_dark:I = 0x7f0a0056

.field public static final wallet_highlighted_text_holo_light:I = 0x7f0a0055

.field public static final wallet_hint_foreground_holo_dark:I = 0x7f0a0054

.field public static final wallet_hint_foreground_holo_light:I = 0x7f0a0053

.field public static final wallet_holo_blue_light:I = 0x7f0a0057

.field public static final wallet_link_text_light:I = 0x7f0a0058

.field public static final wallet_primary_text_holo_light:I = 0x7f0a0265

.field public static final wallet_secondary_text_holo_dark:I = 0x7f0a0266

.field public static final white:I = 0x7f0a00a1

.field public static final white_semi_transparent:I = 0x7f0a00b4

.field public static final white_slightly_transparent:I = 0x7f0a00b3

.field public static final zagat_red:I = 0x7f0a00b8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
