.class public Lcom/google/userfeedback/android/api/a/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:[Ljava/lang/Object;

.field public b:Ljava/util/Hashtable;

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 106
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/userfeedback/android/api/a/b/a;-><init>(I)V

    .line 107
    return-void
.end method

.method private constructor <init>(I)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/16 v0, 0x10

    .line 117
    if-lez p1, :cond_0

    .line 118
    const/16 v0, 0x80

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 120
    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    .line 121
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    .line 122
    iput v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->d:I

    .line 123
    iput v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->c:I

    .line 124
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 191
    iget v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->c:I

    if-gt p1, v1, :cond_1

    if-ltz p1, :cond_1

    .line 192
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    .line 198
    :cond_0
    :goto_0
    return-object v0

    .line 193
    :cond_1
    iget v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->d:I

    if-gt p1, v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    invoke-static {p1}, Lcom/google/userfeedback/android/api/a/b/c;->a(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/16 v2, 0x80

    const/4 v0, 0x0

    .line 213
    if-nez p2, :cond_3

    .line 214
    if-ltz p1, :cond_2

    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    :cond_0
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 235
    :cond_1
    :goto_0
    return-void

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    invoke-static {p1}, Lcom/google/userfeedback/android/api/a/b/c;->a(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 217
    :cond_3
    iget v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->d:I

    if-le p1, v1, :cond_4

    .line 218
    iput p1, p0, Lcom/google/userfeedback/android/api/a/b/a;->d:I

    .line 220
    :cond_4
    if-ltz p1, :cond_5

    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-lt p1, v1, :cond_8

    :cond_5
    if-ge p1, v2, :cond_7

    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-lt p1, v1, :cond_7

    if-lez p1, :cond_7

    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v1, v1

    :cond_6
    shl-int/lit8 v1, v1, 0x1

    if-le v1, p1, :cond_6

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v3, v3

    invoke-static {v2, v0, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    const/4 v0, 0x1

    :cond_7
    if-eqz v0, :cond_b

    .line 221
    :cond_8
    iget v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->c:I

    if-le p1, v0, :cond_a

    .line 222
    iput p1, p0, Lcom/google/userfeedback/android/api/a/b/a;->c:I

    .line 224
    iget v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    .line 228
    :cond_9
    :goto_1
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    aput-object p2, v0, p1

    goto :goto_0

    .line 225
    :cond_a
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    if-nez v0, :cond_9

    .line 226
    iget v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    goto :goto_1

    .line 230
    :cond_b
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    if-nez v0, :cond_c

    .line 231
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    .line 233
    :cond_c
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    invoke-static {p1}, Lcom/google/userfeedback/android/api/a/b/c;->a(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 301
    if-ne p0, p1, :cond_0

    move v0, v1

    .line 312
    :goto_0
    return v0

    .line 304
    :cond_0
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/google/userfeedback/android/api/a/b/a;

    if-nez v0, :cond_2

    :cond_1
    move v0, v2

    .line 305
    goto :goto_0

    .line 307
    :cond_2
    check-cast p1, Lcom/google/userfeedback/android/api/a/b/a;

    .line 308
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    :goto_1
    iget-object v3, p1, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    if-nez v3, :cond_4

    iget v3, p1, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    :goto_2
    if-eq v0, v3, :cond_5

    move v0, v2

    .line 309
    goto :goto_0

    .line 308
    :cond_3
    iget v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    iget-object v3, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->size()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_1

    :cond_4
    iget v3, p1, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    iget-object v4, p1, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljava/util/Hashtable;->size()I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_2

    .line 311
    :cond_5
    iget-object v4, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v0, v4

    array-length v3, v5

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v3, v2

    :goto_3
    if-ge v3, v0, :cond_9

    aget-object v6, v4, v3

    if-nez v6, :cond_6

    aget-object v6, v5, v3

    if-nez v6, :cond_7

    :cond_6
    aget-object v6, v4, v3

    if-eqz v6, :cond_8

    aget-object v6, v4, v3

    aget-object v7, v5, v3

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    :cond_7
    move v0, v2

    :goto_4
    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    iget-object v3, p1, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    .line 312
    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_5
    if-eqz v0, :cond_14

    move v0, v1

    goto :goto_0

    .line 311
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_9
    array-length v3, v4

    array-length v6, v5

    if-le v3, v6, :cond_b

    :goto_6
    array-length v3, v4

    if-ge v0, v3, :cond_d

    aget-object v3, v4, v0

    if-eqz v3, :cond_a

    move v0, v2

    goto :goto_4

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    array-length v3, v4

    array-length v4, v5

    if-ge v3, v4, :cond_d

    :goto_7
    array-length v3, v5

    if-ge v0, v3, :cond_d

    aget-object v3, v5, v0

    if-eqz v3, :cond_c

    move v0, v2

    goto :goto_4

    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_d
    move v0, v1

    goto :goto_4

    .line 312
    :cond_e
    if-eqz v0, :cond_f

    if-nez v3, :cond_10

    :cond_f
    move v0, v2

    goto :goto_5

    :cond_10
    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v4

    invoke-virtual {v3}, Ljava/util/Hashtable;->size()I

    move-result v5

    if-eq v4, v5, :cond_11

    move v0, v2

    goto :goto_5

    :cond_11
    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v4

    :cond_12
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v3, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    move v0, v2

    goto :goto_5

    :cond_13
    move v0, v1

    goto :goto_5

    :cond_14
    move v0, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 279
    const/4 v1, 0x1

    .line 280
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 281
    iget-object v2, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    .line 282
    if-eqz v2, :cond_0

    .line 283
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v0

    .line 280
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    if-nez v0, :cond_2

    :goto_1
    return v1

    :cond_2
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 386
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v0, "IntMap{lower:"

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 387
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 388
    iget-object v2, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 389
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 390
    const-string v2, "=>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 391
    iget-object v2, p0, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 392
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 387
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :cond_1
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, ", higher:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 396
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
