.class public Lcom/google/userfeedback/android/api/a/a/a/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/Boolean;

.field public static final b:Ljava/lang/Boolean;

.field public static final c:[B


# instance fields
.field public final d:Lcom/google/userfeedback/android/api/a/b/a;

.field private e:Lcom/google/userfeedback/android/api/a/a/a/c;

.field private f:Lcom/google/userfeedback/android/api/a/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v2}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/userfeedback/android/api/a/a/a/a;->a:Ljava/lang/Boolean;

    .line 54
    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/userfeedback/android/api/a/a/a/a;->b:Ljava/lang/Boolean;

    .line 55
    new-array v0, v2, [B

    sput-object v0, Lcom/google/userfeedback/android/api/a/a/a/a;->c:[B

    .line 382
    new-instance v0, Lcom/google/userfeedback/android/api/a/a/a/b;

    invoke-direct {v0}, Lcom/google/userfeedback/android/api/a/a/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/userfeedback/android/api/a/a/a/c;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->e:Lcom/google/userfeedback/android/api/a/a/a/c;

    .line 98
    new-instance v0, Lcom/google/userfeedback/android/api/a/b/a;

    invoke-direct {v0}, Lcom/google/userfeedback/android/api/a/b/a;-><init>()V

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    .line 99
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, p1}, Lcom/google/userfeedback/android/api/a/b/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v1, v0, Ljava/util/Vector;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(ILcom/google/userfeedback/android/api/a/a/h;)I
    .locals 17

    .prologue
    .line 801
    invoke-direct/range {p0 .. p1}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(I)I

    move-result v11

    .line 802
    invoke-direct/range {p0 .. p1}, Lcom/google/userfeedback/android/api/a/a/a/a;->b(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Unsupp.Type:"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/userfeedback/android/api/a/a/a/a;->e:Lcom/google/userfeedback/android/api/a/a/a/c;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x18

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_1
    move v3, v2

    .line 803
    :goto_0
    shl-int/lit8 v2, p1, 0x3

    or-int v12, v2, v3

    .line 806
    const/4 v4, 0x0

    .line 808
    const/4 v2, 0x0

    move v8, v2

    move v2, v4

    :goto_1
    if-ge v8, v11, :cond_8

    .line 809
    int-to-long v4, v12

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/OutputStream;J)I

    move-result v4

    add-int v10, v2, v4

    .line 810
    const/4 v9, 0x0

    .line 811
    move-object/from16 v0, p2

    iget v13, v0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    .line 812
    packed-switch v3, :pswitch_data_1

    .line 865
    :pswitch_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 802
    :pswitch_3
    const/4 v2, 0x0

    move v3, v2

    goto :goto_0

    :pswitch_4
    const/4 v2, 0x2

    move v3, v2

    goto :goto_0

    :pswitch_5
    const/4 v2, 0x1

    move v3, v2

    goto :goto_0

    :pswitch_6
    const/4 v2, 0x5

    move v3, v2

    goto :goto_0

    :pswitch_7
    const/4 v2, 0x3

    move v3, v2

    goto :goto_0

    .line 815
    :pswitch_8
    const/16 v2, 0x13

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v8, v2}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 816
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 817
    const/4 v2, 0x5

    if-ne v3, v2, :cond_0

    const/4 v2, 0x4

    .line 818
    :goto_2
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v2, :cond_1

    .line 819
    const-wide/16 v14, 0xff

    and-long/2addr v14, v6

    long-to-int v5, v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/google/userfeedback/android/api/a/a/h;->write(I)V

    .line 820
    const/16 v5, 0x8

    shr-long/2addr v6, v5

    .line 818
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 817
    :cond_0
    const/16 v2, 0x8

    goto :goto_2

    :cond_1
    move v4, v9

    move v2, v10

    .line 867
    :goto_4
    if-nez v4, :cond_2

    .line 868
    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    sub-int/2addr v4, v13

    add-int/2addr v2, v4

    .line 808
    :cond_2
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    .line 825
    :pswitch_9
    const/16 v2, 0x13

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v8, v2}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 826
    invoke-direct/range {p0 .. p1}, Lcom/google/userfeedback/android/api/a/a/a/a;->b(I)I

    move-result v2

    const/16 v6, 0x21

    if-eq v2, v6, :cond_3

    const/16 v6, 0x22

    if-ne v2, v6, :cond_5

    :cond_3
    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_4

    .line 827
    const/4 v2, 0x1

    shl-long v6, v4, v2

    const/16 v2, 0x3f

    ushr-long/2addr v4, v2

    neg-long v4, v4

    xor-long/2addr v4, v6

    .line 829
    :cond_4
    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/OutputStream;J)I

    move v4, v9

    move v2, v10

    .line 830
    goto :goto_4

    .line 826
    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    .line 834
    :pswitch_a
    invoke-direct/range {p0 .. p1}, Lcom/google/userfeedback/android/api/a/a/a/a;->b(I)I

    move-result v2

    const/16 v4, 0x1b

    if-ne v2, v4, :cond_6

    const/16 v2, 0x10

    .line 833
    :goto_6
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v8, v2}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(III)Ljava/lang/Object;

    move-result-object v2

    .line 838
    instance-of v4, v2, [B

    if-eqz v4, :cond_7

    .line 839
    check-cast v2, [B

    .line 840
    array-length v4, v2

    int-to-long v4, v4

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/OutputStream;J)I

    .line 841
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/userfeedback/android/api/a/a/h;->write([B)V

    move v4, v9

    move v2, v10

    .line 842
    goto :goto_4

    .line 834
    :cond_6
    const/16 v2, 0x19

    goto :goto_6

    .line 843
    :cond_7
    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/userfeedback/android/api/a/a/h;->a(I)V

    .line 845
    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/userfeedback/android/api/a/a/h;->d:I

    .line 846
    const/4 v5, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/google/userfeedback/android/api/a/a/h;->a(I)V

    .line 847
    check-cast v2, Lcom/google/userfeedback/android/api/a/a/a/a;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Lcom/google/userfeedback/android/api/a/a/h;)I

    move-result v2

    .line 849
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    aput v2, v5, v4

    .line 851
    int-to-long v4, v2

    invoke-static {v4, v5}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(J)I

    move-result v4

    add-int/2addr v2, v4

    add-int v4, v10, v2

    .line 852
    const/4 v2, 0x1

    move/from16 v16, v2

    move v2, v4

    move/from16 v4, v16

    .line 854
    goto/16 :goto_4

    .line 858
    :pswitch_b
    const/16 v2, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v8, v2}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/userfeedback/android/api/a/a/a/a;

    .line 859
    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Lcom/google/userfeedback/android/api/a/a/h;)I

    move-result v2

    add-int/2addr v2, v10

    .line 860
    shl-int/lit8 v4, p1, 0x3

    or-int/lit8 v4, v4, 0x4

    int-to-long v4, v4

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/OutputStream;J)I

    move-result v4

    add-int/2addr v4, v2

    .line 861
    const/4 v2, 0x1

    move/from16 v16, v2

    move v2, v4

    move/from16 v4, v16

    .line 862
    goto/16 :goto_4

    .line 871
    :cond_8
    return v2

    .line 802
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 812
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_8
    .end packed-switch
.end method

.method private static a(J)I
    .locals 4

    .prologue
    .line 704
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 705
    const/16 v0, 0xa

    .line 712
    :cond_0
    return v0

    .line 707
    :cond_1
    const/4 v0, 0x1

    .line 708
    :goto_0
    const-wide/16 v2, 0x80

    cmp-long v1, p0, v2

    if-ltz v1, :cond_0

    .line 709
    add-int/lit8 v0, v0, 0x1

    .line 710
    const/4 v1, 0x7

    shr-long/2addr p0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/userfeedback/android/api/a/a/h;)I
    .locals 4

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    new-instance v1, Lcom/google/userfeedback/android/api/a/b/b;

    invoke-direct {v1, v0}, Lcom/google/userfeedback/android/api/a/b/b;-><init>(Lcom/google/userfeedback/android/api/a/b/a;)V

    .line 783
    const/4 v0, 0x0

    .line 784
    :goto_0
    invoke-virtual {v1}, Lcom/google/userfeedback/android/api/a/b/b;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 785
    invoke-virtual {v1}, Lcom/google/userfeedback/android/api/a/b/b;->a()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget v2, v1, Lcom/google/userfeedback/android/api/a/b/b;->a:I

    const/high16 v3, -0x80000000

    iput v3, v1, Lcom/google/userfeedback/android/api/a/b/b;->a:I

    .line 786
    invoke-direct {p0, v2, p1}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(ILcom/google/userfeedback/android/api/a/a/h;)I

    move-result v2

    add-int/2addr v0, v2

    .line 787
    goto :goto_0

    .line 788
    :cond_1
    return v0
.end method

.method private a(Ljava/io/InputStream;IZLcom/google/userfeedback/android/api/a/a/a/b;)I
    .locals 11

    .prologue
    .line 416
    if-eqz p3, :cond_2

    .line 417
    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, v1, Lcom/google/userfeedback/android/api/a/b/a;->a:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/userfeedback/android/api/a/b/a;->b:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    :cond_1
    const/high16 v0, -0x80000000

    iput v0, v1, Lcom/google/userfeedback/android/api/a/b/a;->d:I

    const/high16 v0, -0x80000000

    iput v0, v1, Lcom/google/userfeedback/android/api/a/b/a;->c:I

    const/4 v0, 0x0

    iput v0, v1, Lcom/google/userfeedback/android/api/a/b/a;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->f:Lcom/google/userfeedback/android/api/a/b/a;

    :cond_2
    move v0, p2

    .line 419
    :goto_1
    if-lez v0, :cond_10

    .line 420
    const/4 v1, 0x1

    invoke-static {p1, v1, p4}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/InputStream;ZLcom/google/userfeedback/android/api/a/a/a/b;)J

    move-result-wide v4

    .line 422
    const-wide/16 v2, -0x1

    cmp-long v1, v4, v2

    if-eqz v1, :cond_10

    .line 423
    iget v1, p4, Lcom/google/userfeedback/android/api/a/a/a/b;->a:I

    sub-int v2, v0, v1

    .line 426
    long-to-int v0, v4

    and-int/lit8 v0, v0, 0x7

    .line 427
    const/4 v1, 0x4

    if-eq v0, v1, :cond_f

    .line 428
    const/4 v1, 0x3

    ushr-long/2addr v4, v1

    long-to-int v6, v4

    .line 432
    invoke-direct {p0, v6}, Lcom/google/userfeedback/android/api/a/a/a/a;->b(I)I

    move-result v1

    .line 433
    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 434
    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->f:Lcom/google/userfeedback/android/api/a/b/a;

    if-nez v1, :cond_3

    .line 435
    new-instance v1, Lcom/google/userfeedback/android/api/a/b/a;

    invoke-direct {v1}, Lcom/google/userfeedback/android/api/a/b/a;-><init>()V

    iput-object v1, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->f:Lcom/google/userfeedback/android/api/a/b/a;

    .line 437
    :cond_3
    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->f:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-static {v0}, Lcom/google/userfeedback/android/api/a/b/c;->a(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v6, v3}, Lcom/google/userfeedback/android/api/a/b/a;->a(ILjava/lang/Object;)V

    .line 438
    :cond_4
    packed-switch v0, :pswitch_data_0

    .line 497
    :pswitch_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown wire type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", reading garbage data?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 445
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p1, v0, p4}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/InputStream;ZLcom/google/userfeedback/android/api/a/a/a/b;)J

    move-result-wide v0

    .line 446
    iget v3, p4, Lcom/google/userfeedback/android/api/a/a/a/b;->a:I

    sub-int/2addr v2, v3

    .line 447
    invoke-direct {p0, v6}, Lcom/google/userfeedback/android/api/a/a/a/a;->b(I)I

    move-result v3

    const/16 v4, 0x21

    if-eq v3, v4, :cond_5

    const/16 v4, 0x22

    if-ne v3, v4, :cond_8

    :cond_5
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_6

    .line 448
    const/4 v3, 0x1

    ushr-long v4, v0, v3

    const-wide/16 v8, 0x1

    and-long/2addr v0, v8

    neg-long v0, v0

    xor-long/2addr v0, v4

    .line 450
    :cond_6
    invoke-static {v0, v1}, Lcom/google/userfeedback/android/api/a/b/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    move v1, v2

    .line 500
    :cond_7
    :goto_3
    invoke-virtual {p0, v6, v0}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(ILjava/lang/Object;)V

    move v0, v1

    .line 501
    goto/16 :goto_1

    .line 447
    :cond_8
    const/4 v3, 0x0

    goto :goto_2

    .line 456
    :pswitch_2
    const-wide/16 v4, 0x0

    .line 457
    const/4 v3, 0x0

    .line 458
    const/4 v1, 0x5

    if-ne v0, v1, :cond_9

    const/4 v0, 0x4

    .line 459
    :goto_4
    sub-int v1, v2, v0

    .line 461
    :goto_5
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_a

    .line 462
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-long v8, v0

    .line 463
    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 464
    add-int/lit8 v0, v3, 0x8

    move v3, v0

    move v0, v2

    .line 465
    goto :goto_5

    .line 458
    :cond_9
    const/16 v0, 0x8

    goto :goto_4

    .line 467
    :cond_a
    invoke-static {v4, v5}, Lcom/google/userfeedback/android/api/a/b/c;->a(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3

    .line 471
    :pswitch_3
    const/4 v0, 0x0

    invoke-static {p1, v0, p4}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/InputStream;ZLcom/google/userfeedback/android/api/a/a/a/b;)J

    move-result-wide v0

    long-to-int v3, v0

    .line 472
    iget v0, p4, Lcom/google/userfeedback/android/api/a/a/a/b;->a:I

    sub-int v0, v2, v0

    .line 473
    sub-int v1, v0, v3

    .line 475
    if-nez v3, :cond_b

    sget-object v0, Lcom/google/userfeedback/android/api/a/a/a/a;->c:[B

    .line 476
    :goto_6
    const/4 v2, 0x0

    .line 477
    :goto_7
    if-ge v2, v3, :cond_7

    .line 478
    sub-int v4, v3, v2

    invoke-virtual {p1, v0, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 479
    if-gtz v4, :cond_c

    .line 480
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexp.EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 475
    :cond_b
    new-array v0, v3, [B

    goto :goto_6

    .line 482
    :cond_c
    add-int/2addr v2, v4

    goto :goto_7

    .line 488
    :pswitch_4
    new-instance v1, Lcom/google/userfeedback/android/api/a/a/a/a;

    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->e:Lcom/google/userfeedback/android/api/a/a/a/c;

    if-nez v0, :cond_d

    const/4 v0, 0x0

    .line 490
    :goto_8
    invoke-direct {v1, v0}, Lcom/google/userfeedback/android/api/a/a/a/a;-><init>(Lcom/google/userfeedback/android/api/a/a/a/c;)V

    .line 492
    const/4 v0, 0x0

    invoke-direct {v1, p1, v2, v0, p4}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/InputStream;IZLcom/google/userfeedback/android/api/a/a/a/b;)I

    move-result v0

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 494
    goto :goto_3

    .line 488
    :cond_d
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->e:Lcom/google/userfeedback/android/api/a/a/a/c;

    .line 490
    iget-object v0, v0, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, v6}, Lcom/google/userfeedback/android/api/a/b/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/userfeedback/android/api/a/a/a/d;

    if-nez v0, :cond_e

    :goto_9
    check-cast v0, Lcom/google/userfeedback/android/api/a/a/a/c;

    goto :goto_8

    :cond_e
    iget-object v0, v0, Lcom/google/userfeedback/android/api/a/a/a/d;->b:Ljava/lang/Object;

    goto :goto_9

    :cond_f
    move v0, v2

    .line 503
    :cond_10
    if-gez v0, :cond_11

    .line 504
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 507
    :cond_11
    return v0

    .line 438
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/io/OutputStream;J)I
    .locals 5

    .prologue
    .line 1576
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 1578
    const-wide/16 v2, 0x7f

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 1580
    const/4 v2, 0x7

    ushr-long/2addr p1, v2

    .line 1582
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 1583
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1584
    add-int/lit8 v0, v0, 0x1

    .line 1589
    :cond_0
    return v0

    .line 1586
    :cond_1
    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1576
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/io/InputStream;ZLcom/google/userfeedback/android/api/a/a/a/b;)J
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1474
    const-wide/16 v2, 0x0

    .line 1477
    iput v0, p2, Lcom/google/userfeedback/android/api/a/a/a/b;->a:I

    move v8, v0

    move v9, v0

    move-wide v0, v2

    move v2, v8

    move v3, v9

    .line 1481
    :goto_0
    const/16 v4, 0xa

    if-ge v2, v4, :cond_2

    .line 1482
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 1484
    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 1485
    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    .line 1486
    const-wide/16 v0, -0x1

    .line 1500
    :goto_1
    return-wide v0

    .line 1488
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1491
    :cond_1
    and-int/lit8 v5, v4, 0x7f

    int-to-long v6, v5

    shl-long/2addr v6, v3

    or-long/2addr v0, v6

    .line 1493
    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_2

    .line 1494
    add-int/lit8 v3, v3, 0x7

    .line 1481
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1499
    :cond_2
    add-int/lit8 v2, v2, 0x1

    iput v2, p2, Lcom/google/userfeedback/android/api/a/a/a/b;->a:I

    goto :goto_1
.end method

.method private a(III)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, p1}, Lcom/google/userfeedback/android/api/a/b/a;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 1245
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 1247
    :goto_0
    if-lt p2, v0, :cond_2

    .line 1248
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1245
    :cond_0
    instance-of v0, v1, Ljava/util/Vector;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1250
    :cond_2
    const/4 v0, 0x0

    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_6

    check-cast v1, Ljava/util/Vector;

    invoke-virtual {v1, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v0, p3, p1}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/lang/Object;II)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, v0, :cond_4

    if-eqz v0, :cond_4

    if-nez v1, :cond_5

    if-gez p1, :cond_3

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, p1, v2}, Lcom/google/userfeedback/android/api/a/b/a;->a(ILjava/lang/Object;)V

    :cond_4
    :goto_2
    return-object v2

    :cond_5
    invoke-virtual {v1, v2, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_2

    :cond_6
    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    goto :goto_1
.end method

.method private a(Ljava/lang/Object;II)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1369
    packed-switch p2, :pswitch_data_0

    .line 1437
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupp.Type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1374
    :pswitch_1
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1434
    :cond_0
    :goto_0
    :pswitch_2
    return-object p1

    .line 1377
    :cond_1
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_1

    .line 1383
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Type mismatch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1379
    :pswitch_3
    sget-object p1, Lcom/google/userfeedback/android/api/a/a/a/a;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 1381
    :pswitch_4
    sget-object p1, Lcom/google/userfeedback/android/api/a/a/a/a;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1393
    :pswitch_5
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1394
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x1

    :goto_1
    invoke-static {v0, v1}, Lcom/google/userfeedback/android/api/a/b/c;->a(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 1399
    :pswitch_6
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1400
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/google/userfeedback/android/api/a/a/g;->a(Ljava/lang/String;)[B

    move-result-object p1

    goto :goto_0

    .line 1401
    :cond_3
    instance-of v0, p1, Lcom/google/userfeedback/android/api/a/a/a/a;

    if-eqz v0, :cond_0

    .line 1402
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1404
    :try_start_0
    check-cast p1, Lcom/google/userfeedback/android/api/a/a/a/a;

    invoke-virtual {p1, v0}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/OutputStream;)V

    .line 1405
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 1406
    :catch_0
    move-exception v0

    .line 1407
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1413
    :pswitch_7
    instance-of v0, p1, [B

    if-eqz v0, :cond_0

    .line 1414
    check-cast p1, [B

    .line 1415
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p1, v0, v1, v2}, Lcom/google/userfeedback/android/api/a/a/g;->a([BIIZ)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1420
    :pswitch_8
    instance-of v0, p1, [B

    if-eqz v0, :cond_0

    .line 1423
    if-lez p3, :cond_5

    :try_start_1
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->e:Lcom/google/userfeedback/android/api/a/a/a/c;

    if-eqz v0, :cond_5

    .line 1424
    new-instance v1, Lcom/google/userfeedback/android/api/a/a/a/a;

    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->e:Lcom/google/userfeedback/android/api/a/a/a/c;

    iget-object v0, v0, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, p3}, Lcom/google/userfeedback/android/api/a/b/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/userfeedback/android/api/a/a/a/d;

    if-nez v0, :cond_4

    :goto_2
    check-cast v0, Lcom/google/userfeedback/android/api/a/a/a/c;

    invoke-direct {v1, v0}, Lcom/google/userfeedback/android/api/a/a/a/a;-><init>(Lcom/google/userfeedback/android/api/a/a/a/c;)V

    move-object v0, v1

    .line 1429
    :goto_3
    check-cast p1, [B

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v2, p1

    const/4 v3, 0x1

    new-instance v4, Lcom/google/userfeedback/android/api/a/a/a/b;

    invoke-direct {v4}, Lcom/google/userfeedback/android/api/a/a/a/b;-><init>()V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/InputStream;IZLcom/google/userfeedback/android/api/a/a/a/b;)I

    move-object p1, v0

    goto/16 :goto_0

    .line 1424
    :cond_4
    iget-object v0, v0, Lcom/google/userfeedback/android/api/a/a/a/d;->b:Ljava/lang/Object;

    goto :goto_2

    .line 1426
    :cond_5
    new-instance v0, Lcom/google/userfeedback/android/api/a/a/a/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/userfeedback/android/api/a/a/a/a;-><init>(Lcom/google/userfeedback/android/api/a/a/a/c;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 1430
    :catch_1
    move-exception v0

    .line 1431
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1369
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 1377
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private b(I)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x10

    .line 550
    .line 551
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->e:Lcom/google/userfeedback/android/api/a/a/a/c;

    if-eqz v0, :cond_6

    .line 552
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->e:Lcom/google/userfeedback/android/api/a/a/a/c;

    iget-object v0, v0, Lcom/google/userfeedback/android/api/a/a/a/c;->a:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, p1}, Lcom/google/userfeedback/android/api/a/b/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/userfeedback/android/api/a/a/a/d;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    move v3, v0

    .line 555
    :goto_1
    if-ne v3, v1, :cond_5

    .line 556
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->f:Lcom/google/userfeedback/android/api/a/b/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->f:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, p1}, Lcom/google/userfeedback/android/api/a/b/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 557
    :goto_2
    if-eqz v0, :cond_5

    .line 558
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 562
    :goto_3
    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(I)I

    move-result v3

    if-lez v3, :cond_1

    .line 563
    invoke-direct {p0, p1, v2, v1}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(III)Ljava/lang/Object;

    move-result-object v0

    .line 565
    instance-of v1, v0, Ljava/lang/Long;

    if-nez v1, :cond_0

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    .line 569
    :cond_1
    :goto_4
    return v0

    .line 552
    :cond_2
    iget v0, v0, Lcom/google/userfeedback/android/api/a/a/a/d;->a:I

    and-int/lit16 v0, v0, 0xff

    goto :goto_0

    .line 556
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 565
    :cond_4
    const/4 v0, 0x2

    goto :goto_4

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    move v3, v1

    goto :goto_1
.end method


# virtual methods
.method public a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 1361
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, p1}, Lcom/google/userfeedback/android/api/a/b/a;->a(I)Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x0

    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    :cond_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    if-gez p1, :cond_2

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/userfeedback/android/api/a/b/a;->a(ILjava/lang/Object;)V

    .line 1362
    :goto_0
    return-void

    .line 1361
    :cond_3
    if-nez v0, :cond_4

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/a/a/a;->d:Lcom/google/userfeedback/android/api/a/b/a;

    invoke-virtual {v1, p1, v0}, Lcom/google/userfeedback/android/api/a/b/a;->a(ILjava/lang/Object;)V

    :cond_4
    invoke-virtual {v0, p2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 734
    new-instance v3, Lcom/google/userfeedback/android/api/a/a/h;

    invoke-direct {v3}, Lcom/google/userfeedback/android/api/a/a/h;-><init>()V

    invoke-direct {p0, v3}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Lcom/google/userfeedback/android/api/a/a/h;)I

    iget v4, v3, Lcom/google/userfeedback/android/api/a/a/h;->d:I

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    iget-object v2, v3, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    aget v2, v2, v0

    sub-int v5, v2, v1

    iget-object v6, v3, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    invoke-virtual {p1, v6, v1, v5}, Ljava/io/OutputStream;->write([BII)V

    add-int/lit8 v1, v0, 0x1

    iget-object v5, v3, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    aget v1, v5, v1

    int-to-long v6, v1

    invoke-static {p1, v6, v7}, Lcom/google/userfeedback/android/api/a/a/a/a;->a(Ljava/io/OutputStream;J)I

    add-int/lit8 v0, v0, 0x2

    move v1, v2

    goto :goto_0

    :cond_0
    iget v0, v3, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    if-ge v1, v0, :cond_1

    iget v0, v3, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    sub-int/2addr v0, v1

    iget-object v2, v3, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    invoke-virtual {p1, v2, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 735
    :cond_1
    return-void
.end method
