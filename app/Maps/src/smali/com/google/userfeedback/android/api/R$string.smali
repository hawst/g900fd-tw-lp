.class public final Lcom/google/userfeedback/android/api/R$string;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final AAP_ADDRESS:I = 0x7f0c01ee

.field public static final AAP_ADDRESS_HINT:I = 0x7f0c01ef

.field public static final AAP_CATEGORY:I = 0x7f0c01f1

.field public static final AAP_MAP_OVERLAY:I = 0x7f0c01f0

.field public static final AAP_NAME:I = 0x7f0c01ec

.field public static final AAP_NAME_HINT:I = 0x7f0c01ed

.field public static final AAP_PHONE_HINT:I = 0x7f0c01f4

.field public static final AAP_SELECT:I = 0x7f0c01f2

.field public static final AAP_SELECT_CATEGORY:I = 0x7f0c01f3

.field public static final AAP_TITLE:I = 0x7f0c01eb

.field public static final AAP_WEBSITE_HINT:I = 0x7f0c01f5

.field public static final ABOUT:I = 0x7f0c025a

.field public static final ABOUT_VERSION_SUMMARY:I = 0x7f0c03e8

.field public static final ACCEPT_AND_CONTINUE:I = 0x7f0c0270

.field public static final ACCESSIBILITY_ACTIVATE_TO_CLOSE_STEP_LIST:I = 0x7f0c0394

.field public static final ACCESSIBILITY_ACTIVATE_TO_EXPAND:I = 0x7f0c045f

.field public static final ACCESSIBILITY_ACTIVATE_TO_OPEN_STEP_LIST:I = 0x7f0c0393

.field public static final ACCESSIBILITY_ADD_A_REVIEW_WITH_STARS:I = 0x7f0c03d2

.field public static final ACCESSIBILITY_ARRIVING_AT:I = 0x7f0c03d3

.field public static final ACCESSIBILITY_BLUE_DOT_OPEN_CURRENT_PLACE_DETAILS:I = 0x7f0c0458

.field public static final ACCESSIBILITY_BLUE_DOT_SELECT_CURRENT_PLACE:I = 0x7f0c045a

.field public static final ACCESSIBILITY_CLEAR:I = 0x7f0c0392

.field public static final ACCESSIBILITY_CLOSE_DIRECTIONS:I = 0x7f0c03ab

.field public static final ACCESSIBILITY_COMPASS:I = 0x7f0c0399

.field public static final ACCESSIBILITY_DEPARTING_AT:I = 0x7f0c03d4

.field public static final ACCESSIBILITY_DIRECTIONS_TO:I = 0x7f0c03d9

.field public static final ACCESSIBILITY_ENTER_COMPASS_MODE:I = 0x7f0c039a

.field public static final ACCESSIBILITY_ENTER_LOOK_AROUND:I = 0x7f0c039c

.field public static final ACCESSIBILITY_EXIT_COMPASS_MODE:I = 0x7f0c039b

.field public static final ACCESSIBILITY_EXIT_LOOK_AROUND:I = 0x7f0c039d

.field public static final ACCESSIBILITY_EXPLORE_AROUND_HERE:I = 0x7f0c03db

.field public static final ACCESSIBILITY_FLOOR:I = 0x7f0c03ad

.field public static final ACCESSIBILITY_FLOOR_WITH_LOCATION:I = 0x7f0c03af

.field public static final ACCESSIBILITY_FLOOR_WITH_RESULT:I = 0x7f0c03ae

.field public static final ACCESSIBILITY_FLOOR_WITH_RESULT_AND_LOCATION:I = 0x7f0c03b0

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_DIRECTION_BICYCLE:I = 0x7f0c03bd

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_DIRECTION_DRIVE:I = 0x7f0c03bc

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_DIRECTION_TRANSIT:I = 0x7f0c03bf

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_DIRECTION_WALK:I = 0x7f0c03be

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_INNERSPACE:I = 0x7f0c03cb

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_MAP:I = 0x7f0c03bb

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_MY_PROFILE:I = 0x7f0c03c6

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_REPORT_MAP_ISSUE:I = 0x7f0c03c7

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_REPORT_MAP_ISSUE_DESCRIPTION:I = 0x7f0c03c8

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_SEND_FEEDBACK:I = 0x7f0c03c9

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_SETTING:I = 0x7f0c03c0

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_SETTING_ABOUT:I = 0x7f0c03c2

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_SETTING_EDIT_HOME_WORK:I = 0x7f0c03c3

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_SETTING_HISTORY:I = 0x7f0c03c4

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_SETTING_TERMS_AND_PRIVACY:I = 0x7f0c03c5

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_STREETVIEW:I = 0x7f0c03ca

.field public static final ACCESSIBILITY_FRAGMENT_TRANSITION_TUTORIALS:I = 0x7f0c03c1

.field public static final ACCESSIBILITY_INNERSPACE_ANNOUNCE:I = 0x7f0c009d

.field public static final ACCESSIBILITY_INSTRUCTION_ACTIONBAR:I = 0x7f0c03cf

.field public static final ACCESSIBILITY_INSTRUCTION_COLLAPSE:I = 0x7f0c03cd

.field public static final ACCESSIBILITY_INSTRUCTION_EXPAND:I = 0x7f0c03cc

.field public static final ACCESSIBILITY_INSTRUCTION_SEE_NEXT:I = 0x7f0c03ce

.field public static final ACCESSIBILITY_LAST_TRANSPORTATION:I = 0x7f0c03a8

.field public static final ACCESSIBILITY_LAYER_BICYCLING:I = 0x7f0c00b1

.field public static final ACCESSIBILITY_LAYER_PUBLIC_TRANSIT:I = 0x7f0c00b0

.field public static final ACCESSIBILITY_LAYER_SATELLITE:I = 0x7f0c00b2

.field public static final ACCESSIBILITY_LAYER_TERRAIN:I = 0x7f0c00b4

.field public static final ACCESSIBILITY_LAYER_TRAFFIC:I = 0x7f0c00af

.field public static final ACCESSIBILITY_LONG_PAUSE:I = 0x7f0c03e1

.field public static final ACCESSIBILITY_MAP_SHOWING_AROUND:I = 0x7f0c0397

.field public static final ACCESSIBILITY_MAP_VIEW:I = 0x7f0c0395

.field public static final ACCESSIBILITY_MENU:I = 0x7f0c038f

.field public static final ACCESSIBILITY_MENU_CLOSED:I = 0x7f0c0390

.field public static final ACCESSIBILITY_MENU_DIRECTIONS:I = 0x7f0c03ac

.field public static final ACCESSIBILITY_MENU_OPENED:I = 0x7f0c0391

.field public static final ACCESSIBILITY_MOVE_TO_MY_LOCATION:I = 0x7f0c0398

.field public static final ACCESSIBILITY_NAVIGATE_UP_TO_ZAGAT_LIST:I = 0x7f0c03b1

.field public static final ACCESSIBILITY_NEXT_DATE:I = 0x7f0c039f

.field public static final ACCESSIBILITY_NEXT_DEPARTURES_AT:I = 0x7f0c03d5

.field public static final ACCESSIBILITY_NEXT_DEPARTURES_AT_WITH_STATION:I = 0x7f0c03d6

.field public static final ACCESSIBILITY_NEXT_NAVIGATION:I = 0x7f0c03a9

.field public static final ACCESSIBILITY_NOW_AT:I = 0x7f0c0456

.field public static final ACCESSIBILITY_NO_DATA:I = 0x7f0c03b7

.field public static final ACCESSIBILITY_NO_REVIEWS:I = 0x7f0c03b8

.field public static final ACCESSIBILITY_OPEN_CURRENT_PLACE_DETAILS:I = 0x7f0c0457

.field public static final ACCESSIBILITY_OVERFLOW_MENU:I = 0x7f0c03f3

.field public static final ACCESSIBILITY_PICK_PLACE_FOR_CURRENT_LOCATION:I = 0x7f0c03da

.field public static final ACCESSIBILITY_PLACE_ADDRESS:I = 0x7f0c0106

.field public static final ACCESSIBILITY_PLACE_CARD_COLLAPSED:I = 0x7f0c03b3

.field public static final ACCESSIBILITY_PLACE_CARD_EXPANDED:I = 0x7f0c03b2

.field public static final ACCESSIBILITY_PLACE_CATEGORY:I = 0x7f0c010b

.field public static final ACCESSIBILITY_PLACE_OPEN_HOURS:I = 0x7f0c0105

.field public static final ACCESSIBILITY_PLACE_PHONE_NUMBER:I = 0x7f0c0107

.field public static final ACCESSIBILITY_PLACE_PHONE_NUMBER_OPEN_HOURS_COST:I = 0x7f0c0109

.field public static final ACCESSIBILITY_PLACE_PICKER:I = 0x7f0c045b

.field public static final ACCESSIBILITY_PLACE_SUMMARY:I = 0x7f0c010a

.field public static final ACCESSIBILITY_PLACE_WEBSITE:I = 0x7f0c0108

.field public static final ACCESSIBILITY_PREVIOUS_DATE:I = 0x7f0c039e

.field public static final ACCESSIBILITY_PREVIOUS_NAVIGATION:I = 0x7f0c03aa

.field public static final ACCESSIBILITY_PROFILE_WITH_REVIEW_COUNT:I = 0x7f0c03d8

.field public static final ACCESSIBILITY_RESET_TIME:I = 0x7f0c03a0

.field public static final ACCESSIBILITY_REVIEW:I = 0x7f0c03d0

.field public static final ACCESSIBILITY_REVIEW_BY_AUTHOR:I = 0x7f0c03d1

.field public static final ACCESSIBILITY_REVIEW_SUMMARY:I = 0x7f0c03b4

.field public static final ACCESSIBILITY_SATELLITE_VIEW:I = 0x7f0c0396

.field public static final ACCESSIBILITY_SEARCHING:I = 0x7f0c03b5

.field public static final ACCESSIBILITY_SEARCH_FAILED:I = 0x7f0c03b6

.field public static final ACCESSIBILITY_SELECT_CURRENT_PLACE:I = 0x7f0c0459

.field public static final ACCESSIBILITY_SHORT_PAUSE:I = 0x7f0c03e2

.field public static final ACCESSIBILITY_SOMEWHERE_ELSE:I = 0x7f0c045c

.field public static final ACCESSIBILITY_START_NAVIGATION:I = 0x7f0c03a1

.field public static final ACCESSIBILITY_START_PREVIEW:I = 0x7f0c03a2

.field public static final ACCESSIBILITY_STEPS_COLLAPSED:I = 0x7f0c03ba

.field public static final ACCESSIBILITY_STEPS_EXPANDED:I = 0x7f0c03b9

.field public static final ACCESSIBILITY_SWAP_DESTINATION:I = 0x7f0c03a7

.field public static final ACCESSIBILITY_SWITCH_TO_BICYCLING:I = 0x7f0c03a5

.field public static final ACCESSIBILITY_SWITCH_TO_DRIVING:I = 0x7f0c03a3

.field public static final ACCESSIBILITY_SWITCH_TO_TRANSIT:I = 0x7f0c03a4

.field public static final ACCESSIBILITY_SWITCH_TO_WALKING:I = 0x7f0c03a6

.field public static final ACCESSIBILITY_VIEW_MAP:I = 0x7f0c00b3

.field public static final ACCESSIBILITY_VIEW_PROFILE_OF:I = 0x7f0c03d7

.field public static final ACCESSIBILITY_WHERE_ARE_YOU:I = 0x7f0c045d

.field public static final ACCESSIBILITY_YOU_ARE_AT:I = 0x7f0c045e

.field public static final AD:I = 0x7f0c0120

.field public static final ADDRESS:I = 0x7f0c01be

.field public static final ADDRESS_HINT:I = 0x7f0c01bf

.field public static final ADDRESS_SEPARATOR:I = 0x7f0c049a

.field public static final ADD_A_MISSING_PLACE:I = 0x7f0c0210

.field public static final ADD_A_MISSING_PLACE_DESCRIPTION:I = 0x7f0c0211

.field public static final ADD_A_NOTE:I = 0x7f0c01ca

.field public static final ADD_A_PLACE_MISSING_REQUIRED_FIELDS:I = 0x7f0c01f6

.field public static final ADD_A_PLACE_SET_CATEGORY:I = 0x7f0c048e

.field public static final ADD_PHONE_NUMBER:I = 0x7f0c01e5

.field public static final ADD_PHOTO_CAMERA:I = 0x7f0c0113

.field public static final ADD_PHOTO_GALLERY:I = 0x7f0c0114

.field public static final ADD_WEBSITE:I = 0x7f0c01e6

.field public static final ADS:I = 0x7f0c0121

.field public static final ADS_PREFERENCE_MANAGER:I = 0x7f0c0447

.field public static final ADS_PREFERENCE_MANAGER_VIEWPORT:I = 0x7f0c0448

.field public static final AGENCY_CONTACT_INFO_BUTTON:I = 0x7f0c0141

.field public static final AGENCY_CONTACT_INFO_TITLE:I = 0x7f0c0142

.field public static final ALL_REVIEWS:I = 0x7f0c010c

.field public static final ALL_TRAFFIC:I = 0x7f0c00ac

.field public static final APPLY_BUTTON:I = 0x7f0c00c8

.field public static final APP_FULL_NAME:I = 0x7f0c0260

.field public static final BACK_BUTTON:I = 0x7f0c00c9

.field public static final BACK_TO:I = 0x7f0c0195

.field public static final BAD_WAYPOINT_COUNT:I = 0x7f0c0180

.field public static final BICYCLE_MOSTLY_FLAT:I = 0x7f0c018e

.field public static final BICYCLING_DURATION:I = 0x7f0c018d

.field public static final BICYCLING_LAYER_UNAVAILABLE:I = 0x7f0c00ae

.field public static final BICYCLING_TRAVEL_MODE_LABEL:I = 0x7f0c018b

.field public static final CALL:I = 0x7f0c0092

.field public static final CANCEL_BUTTON:I = 0x7f0c00ca

.field public static final CAR_DEMO_MODE:I = 0x7f0c0488

.field public static final CAR_DRAWER_TRAFFIC:I = 0x7f0c046c

.field public static final CAR_ERROR_FETCHING_DIRECTIONS:I = 0x7f0c0481

.field public static final CAR_ERROR_FETCHING_TRAFFIC_INCIDENT:I = 0x7f0c0484

.field public static final CAR_ERROR_LOCKOUT:I = 0x7f0c0489

.field public static final CAR_ERROR_STARTING_NAVIGATION:I = 0x7f0c0482

.field public static final CAR_LOADING_ROUTE:I = 0x7f0c0480

.field public static final CAR_LOADING_SEARCH_RESULTS:I = 0x7f0c0479

.field public static final CAR_LOADING_SUGGESTIONS:I = 0x7f0c047c

.field public static final CAR_LOADING_TRAFFIC_INCIDENT_DESCRIPTION:I = 0x7f0c0483

.field public static final CAR_MAP_INTERACTION_LOCKOUT:I = 0x7f0c048a

.field public static final CAR_NAVIGATION_OPTIONS:I = 0x7f0c046d

.field public static final CAR_NO_SEARCH_RESULTS_FOUND:I = 0x7f0c047a

.field public static final CAR_NO_SUGGESTIONS_FOUND:I = 0x7f0c047d

.field public static final CAR_PROJECTION_GPS_LOCKOUT:I = 0x7f0c048b

.field public static final CAR_RETURN_TO_RESULT_LIST:I = 0x7f0c047f

.field public static final CAR_ROUTE_OPTIONS:I = 0x7f0c046f

.field public static final CAR_ROUTE_OVERVIEW:I = 0x7f0c046e

.field public static final CAR_SEARCH_CATEGORY_ATM:I = 0x7f0c0476

.field public static final CAR_SEARCH_CATEGORY_CAFE:I = 0x7f0c0473

.field public static final CAR_SEARCH_CATEGORY_EMERGENCY:I = 0x7f0c0477

.field public static final CAR_SEARCH_CATEGORY_FASTFOOD:I = 0x7f0c0474

.field public static final CAR_SEARCH_CATEGORY_GAS:I = 0x7f0c0471

.field public static final CAR_SEARCH_CATEGORY_GROCERIES:I = 0x7f0c0475

.field public static final CAR_SEARCH_CATEGORY_PARKING:I = 0x7f0c0470

.field public static final CAR_SEARCH_CATEGORY_RESTAURANT:I = 0x7f0c0472

.field public static final CAR_SEARCH_HINT:I = 0x7f0c0478

.field public static final CAR_TITLE_CATEGORIES:I = 0x7f0c046b

.field public static final CAR_TITLE_DEFAULT:I = 0x7f0c0469

.field public static final CAR_TITLE_SUGGESTIONS:I = 0x7f0c046a

.field public static final CAR_TRAFFIC_INCIDENT_CANCEL_BUTTON:I = 0x7f0c0485

.field public static final CAR_TRAFFIC_INCIDENT_READ_INCIDENT_BUTTON:I = 0x7f0c0486

.field public static final CAR_TRAFFIC_INCIDENT_STOP_READING_INCIDENT_BUTTON:I = 0x7f0c0487

.field public static final CAR_UNABLE_TO_RETRIEVE_SEARCH_RESULTS:I = 0x7f0c047b

.field public static final CAR_UNABLE_TO_RETRIEVE_SUGGESTIONS:I = 0x7f0c047e

.field public static final CHECK_IN_DATE_TEXT:I = 0x7f0c049b

.field public static final CHECK_OUT_DATE_TEXT:I = 0x7f0c049c

.field public static final CLOSE:I = 0x7f0c0271

.field public static final CLOSED:I = 0x7f0c00f5

.field public static final CLOSE_BUTTON:I = 0x7f0c00cc

.field public static final COMPASS_CALIBRATION_DIALOG_TITLE:I = 0x7f0c049e

.field public static final COMPASS_CALIBRATION_FINISHED_DIALOG_TITLE:I = 0x7f0c049f

.field public static final CONFIDENTIAL_INTERNAL_LABEL:I = 0x7f0c0277

.field public static final CONFIRM_BUTTON:I = 0x7f0c00cd

.field public static final CONFIRM_DISCARD_OWNER_RESPONSE:I = 0x7f0c0364

.field public static final CONFIRM_DISCARD_OWNER_RESPONSE_CHANGES:I = 0x7f0c0365

.field public static final CONFIRM_DISCARD_REVIEW:I = 0x7f0c0358

.field public static final CONFIRM_DISCARD_REVIEW_CHANGES:I = 0x7f0c0359

.field public static final CONFIRM_RESERVATION:I = 0x7f0c0419

.field public static final COPIED_TEXT_LABEL:I = 0x7f0c0446

.field public static final COPYRIGHT:I = 0x7f0c0261

.field public static final COPY_ADDRESS:I = 0x7f0c0445

.field public static final COPY_PLACE_NAME:I = 0x7f0c0444

.field public static final DASHER_ADMIN_DISABLED_DIALOG_MESSAGE:I = 0x7f0c022f

.field public static final DATA_REQUEST_ERROR:I = 0x7f0c00de

.field public static final DATA_REQUEST_ERROR_GAIA:I = 0x7f0c00e0

.field public static final DATA_REQUEST_ERROR_NO_CONNECTIVITY:I = 0x7f0c00df

.field public static final DA_COMBINED_MANEUVER:I = 0x7f0c02d0

.field public static final DA_CONFIRM_STOP_TEXT:I = 0x7f0c0318

.field public static final DA_CONFIRM_STOP_TITLE:I = 0x7f0c0317

.field public static final DA_DATA_CONNECTION_LOST:I = 0x7f0c030c

.field public static final DA_DAYS_ABBREVIATED:I = 0x7f0c02c6

.field public static final DA_DESTINATION_ON_THE_LEFT:I = 0x7f0c02d4

.field public static final DA_DESTINATION_ON_THE_RIGHT:I = 0x7f0c02d5

.field public static final DA_DESTINATION_REACHED:I = 0x7f0c02d6

.field public static final DA_DESTINATION_WILL_BE_ON_THE_LEFT:I = 0x7f0c02d1

.field public static final DA_DESTINATION_WILL_BE_ON_THE_RIGHT:I = 0x7f0c02d2

.field public static final DA_DIALOG_ACCEPT:I = 0x7f0c02a3

.field public static final DA_DIALOG_DECLINE:I = 0x7f0c02a5

.field public static final DA_DIALOG_ENABLE:I = 0x7f0c02a4

.field public static final DA_DIRECTION_EAST:I = 0x7f0c0321

.field public static final DA_DIRECTION_NORTH:I = 0x7f0c031b

.field public static final DA_DIRECTION_NORTH_EAST:I = 0x7f0c0322

.field public static final DA_DIRECTION_NORTH_WEST:I = 0x7f0c031c

.field public static final DA_DIRECTION_SOUTH:I = 0x7f0c031f

.field public static final DA_DIRECTION_SOUTH_EAST:I = 0x7f0c0320

.field public static final DA_DIRECTION_SOUTH_WEST:I = 0x7f0c031e

.field public static final DA_DIRECTION_WEST:I = 0x7f0c031d

.field public static final DA_DISTANCE_FORMAT_FEET_ABBREVIATED:I = 0x7f0c0287

.field public static final DA_DISTANCE_FORMAT_FEET_EXTENDED:I = 0x7f0c0288

.field public static final DA_DISTANCE_FORMAT_KILOMETERS_ABBREVIATED:I = 0x7f0c028b

.field public static final DA_DISTANCE_FORMAT_KILOMETERS_EXTENDED:I = 0x7f0c028c

.field public static final DA_DISTANCE_FORMAT_METERS_ABBREVIATED:I = 0x7f0c028d

.field public static final DA_DISTANCE_FORMAT_METERS_EXTENDED:I = 0x7f0c028e

.field public static final DA_DISTANCE_FORMAT_MILES_ABBREVIATED:I = 0x7f0c0285

.field public static final DA_DISTANCE_FORMAT_MILES_EXTENDED:I = 0x7f0c0286

.field public static final DA_DISTANCE_FORMAT_MODE:I = 0x7f0c028f

.field public static final DA_DISTANCE_FORMAT_YARDS_ABBREVIATED:I = 0x7f0c0289

.field public static final DA_DISTANCE_FORMAT_YARDS_EXTENDED:I = 0x7f0c028a

.field public static final DA_EXIT:I = 0x7f0c02b0

.field public static final DA_EXIT_NAVIGATION:I = 0x7f0c0315

.field public static final DA_HOURS_ABBREVIATED:I = 0x7f0c02ca

.field public static final DA_MINUTES_ABBREVIATED:I = 0x7f0c02cb

.field public static final DA_NAME_DELIMITER:I = 0x7f0c02b3

.field public static final DA_NOTIFICATION_STEP_FORMAT:I = 0x7f0c02b7

.field public static final DA_ONTO:I = 0x7f0c02b2

.field public static final DA_POINT_ON_MAP:I = 0x7f0c02b8

.field public static final DA_POSITION_IN_STEP_LIST:I = 0x7f0c0292

.field public static final DA_PREPARE_EVENT:I = 0x7f0c02d7

.field public static final DA_RELATIVE_EQUIVALENT:I = 0x7f0c02c9

.field public static final DA_RELATIVE_FASTER:I = 0x7f0c02c7

.field public static final DA_RELATIVE_SLOWER:I = 0x7f0c02c8

.field public static final DA_REPORT_LANE_ARROW_DESCRIPTION:I = 0x7f0c02c0

.field public static final DA_REPORT_LANE_ARROW_ISSUE:I = 0x7f0c02bf

.field public static final DA_REPORT_LANE_ARROW_OPS_DESCRIPTION:I = 0x7f0c02c1

.field public static final DA_REPORT_LANE_COUNT_DESCRIPTION:I = 0x7f0c02bc

.field public static final DA_REPORT_LANE_COUNT_ISSUE:I = 0x7f0c02bb

.field public static final DA_REPORT_LANE_COUNT_OPS_DESCRIPTION:I = 0x7f0c02bd

.field public static final DA_REPORT_LANE_DATA_ISSUE_PRIVACY_NOTICE:I = 0x7f0c02ba

.field public static final DA_REPORT_LANE_DATA_ISSUE_PROMPT:I = 0x7f0c02b9

.field public static final DA_REPORT_LANE_SHARED_OPS_DESCRIPTION:I = 0x7f0c02be

.field public static final DA_REROUTING:I = 0x7f0c030d

.field public static final DA_ROUTE_PREFIXES:I = 0x7f0c02c2

.field public static final DA_ROUTE_SUFFIXES:I = 0x7f0c02c3

.field public static final DA_ROUTING:I = 0x7f0c030e

.field public static final DA_SEARCHING_FOR_GPS:I = 0x7f0c030b

.field public static final DA_SPEECH_ALTERNATES_CONFIRMATION:I = 0x7f0c02ef

.field public static final DA_SPEECH_CONTINUE_FOR_A_HALF_MILE:I = 0x7f0c02e5

.field public static final DA_SPEECH_CONTINUE_FOR_A_QUARTER_MILE:I = 0x7f0c02e4

.field public static final DA_SPEECH_CONTINUE_FOR_ONE_AND_A_HALF_KILOMETERS:I = 0x7f0c02da

.field public static final DA_SPEECH_CONTINUE_FOR_ONE_AND_A_HALF_MILES:I = 0x7f0c02e7

.field public static final DA_SPEECH_CONTINUE_FOR_THREE_QUARTERS_OF_A_MILE:I = 0x7f0c02e6

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_A_HALF_MILE:I = 0x7f0c02e9

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_A_QUARTER_MILE:I = 0x7f0c02e8

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_ONE_AND_A_HALF_KILOMETERS:I = 0x7f0c02db

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_ONE_AND_A_HALF_MILES:I = 0x7f0c02eb

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_THREE_QUARTERS_OF_A_MILE:I = 0x7f0c02ea

.field public static final DA_SPEECH_CURRENT_ROAD:I = 0x7f0c02f0

.field public static final DA_SPEECH_CURRENT_ROAD_NOT_FOUND:I = 0x7f0c02f3

.field public static final DA_SPEECH_DESTINATION:I = 0x7f0c02f7

.field public static final DA_SPEECH_DESTINATION_NOT_FOUND:I = 0x7f0c02f8

.field public static final DA_SPEECH_HIDE_TRAFFIC_CONFIRMATION:I = 0x7f0c02f2

.field public static final DA_SPEECH_IN_A_HALF_MILE:I = 0x7f0c02de

.field public static final DA_SPEECH_IN_A_QUARTER_MILE:I = 0x7f0c02dc

.field public static final DA_SPEECH_IN_ONE_AND_A_HALF_KILOMETERS:I = 0x7f0c02d8

.field public static final DA_SPEECH_IN_ONE_AND_A_HALF_MILES:I = 0x7f0c02e2

.field public static final DA_SPEECH_IN_THREE_QUARTERS_OF_A_MILE:I = 0x7f0c02e0

.field public static final DA_SPEECH_LOST_DATA_CONNECTION:I = 0x7f0c02cd

.field public static final DA_SPEECH_LOST_GPS:I = 0x7f0c02cc

.field public static final DA_SPEECH_MUTE_CONFIRMATION:I = 0x7f0c02ec

.field public static final DA_SPEECH_MY_LOCATION_CONFIRMATION:I = 0x7f0c02f9

.field public static final DA_SPEECH_NAVIGATION_RESUMED:I = 0x7f0c02ce

.field public static final DA_SPEECH_PLEASE_DESCRIBE_PROBLEM:I = 0x7f0c02cf

.field public static final DA_SPEECH_ROUTE_OVERVIEW_CONFIRMATION:I = 0x7f0c02ee

.field public static final DA_SPEECH_SHOW_TRAFFIC_CONFIRMATION:I = 0x7f0c02f1

.field public static final DA_SPEECH_TRAFFIC_HEAVY:I = 0x7f0c02f4

.field public static final DA_SPEECH_TRAFFIC_LIGHT:I = 0x7f0c02f6

.field public static final DA_SPEECH_TRAFFIC_MEDIUM:I = 0x7f0c02f5

.field public static final DA_SPEECH_UNMUTE_CONFIRMATION:I = 0x7f0c02ed

.field public static final DA_SPEECH_UNSUPPORTED_VOICE_ACTION:I = 0x7f0c02fa

.field public static final DA_SPEECH_UNSUPPORTED_VOICE_ACTION_NAVIGATION:I = 0x7f0c02fb

.field public static final DA_SPEECH_VIA_NAME_SEPARATOR:I = 0x7f0c0291

.field public static final DA_SPEECH_YOU_SHOULD_ARRIVE_IN:I = 0x7f0c02fd

.field public static final DA_SPEECH_YOU_SHOULD_GET_THERE_BY:I = 0x7f0c02fc

.field public static final DA_STAY_ON_ROAD_PRIMARY:I = 0x7f0c02c4

.field public static final DA_STAY_ON_ROAD_SECONDARY_MANEUVER:I = 0x7f0c02c5

.field public static final DA_STEP_CONTINUE_ONTO:I = 0x7f0c033e

.field public static final DA_STEP_CONTINUE_STRAIGHT:I = 0x7f0c033f

.field public static final DA_STEP_DEPART:I = 0x7f0c0319

.field public static final DA_STEP_DEPART_ON:I = 0x7f0c031a

.field public static final DA_STEP_FORK_LEFT:I = 0x7f0c033c

.field public static final DA_STEP_FORK_RIGHT:I = 0x7f0c033d

.field public static final DA_STEP_SHARP_LEFT:I = 0x7f0c0327

.field public static final DA_STEP_SHARP_LEFT_ONTO:I = 0x7f0c032d

.field public static final DA_STEP_SHARP_RIGHT:I = 0x7f0c0328

.field public static final DA_STEP_SHARP_RIGHT_ONTO:I = 0x7f0c032e

.field public static final DA_STEP_SLIGHT_LEFT:I = 0x7f0c0323

.field public static final DA_STEP_SLIGHT_LEFT_ONTO:I = 0x7f0c0329

.field public static final DA_STEP_SLIGHT_RIGHT:I = 0x7f0c0324

.field public static final DA_STEP_SLIGHT_RIGHT_ONTO:I = 0x7f0c032a

.field public static final DA_STEP_TAKE_EXIT_N:I = 0x7f0c033b

.field public static final DA_STEP_TAKE_THE_EXIT:I = 0x7f0c0335

.field public static final DA_STEP_TAKE_THE_EXIT_ONTO:I = 0x7f0c0336

.field public static final DA_STEP_TAKE_THE_EXIT_ON_THE_LEFT:I = 0x7f0c0337

.field public static final DA_STEP_TAKE_THE_EXIT_ON_THE_LEFT_ONTO:I = 0x7f0c0338

.field public static final DA_STEP_TAKE_THE_EXIT_ON_THE_RIGHT:I = 0x7f0c0339

.field public static final DA_STEP_TAKE_THE_EXIT_ON_THE_RIGHT_ONTO:I = 0x7f0c033a

.field public static final DA_STEP_TAKE_THE_RAMP:I = 0x7f0c032f

.field public static final DA_STEP_TAKE_THE_RAMP_ONTO:I = 0x7f0c0330

.field public static final DA_STEP_TAKE_THE_RAMP_ON_THE_LEFT:I = 0x7f0c0331

.field public static final DA_STEP_TAKE_THE_RAMP_ON_THE_LEFT_ONTO:I = 0x7f0c0332

.field public static final DA_STEP_TAKE_THE_RAMP_ON_THE_RIGHT:I = 0x7f0c0333

.field public static final DA_STEP_TAKE_THE_RAMP_ON_THE_RIGHT_ONTO:I = 0x7f0c0334

.field public static final DA_STEP_TURN_LEFT:I = 0x7f0c0325

.field public static final DA_STEP_TURN_LEFT_ONTO:I = 0x7f0c032b

.field public static final DA_STEP_TURN_RIGHT:I = 0x7f0c0326

.field public static final DA_STEP_TURN_RIGHT_ONTO:I = 0x7f0c032c

.field public static final DA_STEP_U_TURN:I = 0x7f0c0340

.field public static final DA_THEN:I = 0x7f0c02af

.field public static final DA_TIME_FORMAT_CLOCK:I = 0x7f0c02b6

.field public static final DA_TIME_FORMAT_DAYS_AND_HOURS:I = 0x7f0c02b4

.field public static final DA_TIME_FORMAT_HOURS_AND_MINUTES:I = 0x7f0c02b5

.field public static final DA_TOWARD:I = 0x7f0c02b1

.field public static final DA_UNNAMED_ROAD:I = 0x7f0c0293

.field public static final DA_VIA_NAME_SEPARATOR:I = 0x7f0c0290

.field public static final DA_YOUR_DESTINATION_IS_A_HALF_MILE_AHEAD:I = 0x7f0c02df

.field public static final DA_YOUR_DESTINATION_IS_A_QUARTER_MILE_AHEAD:I = 0x7f0c02dd

.field public static final DA_YOUR_DESTINATION_IS_ONE_AND_A_HALF_KILOMETERS_AHEAD:I = 0x7f0c02d9

.field public static final DA_YOUR_DESTINATION_IS_ONE_AND_A_HALF_MILES_AHEAD:I = 0x7f0c02e3

.field public static final DA_YOUR_DESTINATION_IS_THREE_QUARTERS_OF_A_MILE_AHEAD:I = 0x7f0c02e1

.field public static final DA_YOU_WILL_REACH_DESTINATION:I = 0x7f0c02d3

.field public static final DELETE_BUTTON:I = 0x7f0c0355

.field public static final DELETE_OWNER_RESPONSE_FAILURE:I = 0x7f0c0363

.field public static final DELETE_OWNER_RESPONSE_SUCCESS:I = 0x7f0c0362

.field public static final DELETE_RATING:I = 0x7f0c0377

.field public static final DELETE_REVIEW:I = 0x7f0c0379

.field public static final DELETE_REVIEW_FAILED:I = 0x7f0c0357

.field public static final DELETE_REVIEW_SUCCESS:I = 0x7f0c0356

.field public static final DEPART:I = 0x7f0c016a

.field public static final DEPARTS_AT_TIME_FROM:I = 0x7f0c016b

.field public static final DEPARTS_FROM:I = 0x7f0c016c

.field public static final DIALOG_INSTALL:I = 0x7f0c038c

.field public static final DIALOG_UPDATE:I = 0x7f0c038d

.field public static final DID_YOU_MEAN_CLEAR_BUTTON:I = 0x7f0c00d8

.field public static final DID_YOU_MEAN_EDIT_BUTTON:I = 0x7f0c00d7

.field public static final DIRECTIONS_APPBAR_FROM:I = 0x7f0c014d

.field public static final DIRECTIONS_APPBAR_TO:I = 0x7f0c014e

.field public static final DIRECTIONS_ARRIVE_BY:I = 0x7f0c0149

.field public static final DIRECTIONS_ARRIVE_BY_BUTTON:I = 0x7f0c0145

.field public static final DIRECTIONS_CARD_CHOOSE_FROM_MAP:I = 0x7f0c012a

.field public static final DIRECTIONS_CHOOSE_END_POINT:I = 0x7f0c0125

.field public static final DIRECTIONS_CHOOSE_END_POINT_FROM_MAP:I = 0x7f0c012d

.field public static final DIRECTIONS_CHOOSE_STARTING_POINT_FROM_MAP:I = 0x7f0c012c

.field public static final DIRECTIONS_CHOOSE_START_POINT:I = 0x7f0c0124

.field public static final DIRECTIONS_DEPART_AT:I = 0x7f0c0148

.field public static final DIRECTIONS_DEPART_AT_BUTTON:I = 0x7f0c0144

.field public static final DIRECTIONS_DURATION_WITHOUT_TRAFFIC:I = 0x7f0c0151

.field public static final DIRECTIONS_ELEVATION_CLIMB_PERCENTAGE_FORMAT:I = 0x7f0c03e7

.field public static final DIRECTIONS_ERROR_INVALID_END_POINT:I = 0x7f0c0127

.field public static final DIRECTIONS_ERROR_INVALID_START_POINT:I = 0x7f0c0126

.field public static final DIRECTIONS_FLOATING_BAR_TAP_ON_MAP:I = 0x7f0c012f

.field public static final DIRECTIONS_HEADER_PAN_ZOOM_MAP_UNDER_PIN:I = 0x7f0c012e

.field public static final DIRECTIONS_HIDE_ALTERNATE_ROUTES:I = 0x7f0c0150

.field public static final DIRECTIONS_INTENT_BUTTON_TEXT:I = 0x7f0c0123

.field public static final DIRECTIONS_LAST_AVAILABLE_TIME:I = 0x7f0c014a

.field public static final DIRECTIONS_LAST_AVAILABLE_TIME_BUTTON:I = 0x7f0c0146

.field public static final DIRECTIONS_LAST_AVAILABLE_TRANSIT_LABEL:I = 0x7f0c0147

.field public static final DIRECTIONS_MY_LOCATION:I = 0x7f0c0128

.field public static final DIRECTIONS_OMNIBOX_FROM:I = 0x7f0c014b

.field public static final DIRECTIONS_OMNIBOX_TO:I = 0x7f0c014c

.field public static final DIRECTIONS_OPTIONS_AVOID_FERRIES:I = 0x7f0c0132

.field public static final DIRECTIONS_OPTIONS_AVOID_HIGHWAYS:I = 0x7f0c0131

.field public static final DIRECTIONS_OPTIONS_AVOID_TOLLS:I = 0x7f0c0130

.field public static final DIRECTIONS_OPTIONS_TITLE:I = 0x7f0c0133

.field public static final DIRECTIONS_OPTIONS_TITLE_QP:I = 0x7f0c0134

.field public static final DIRECTIONS_PIN_LOCATION:I = 0x7f0c012b

.field public static final DIRECTIONS_START_NAVIGATION_BUTTON:I = 0x7f0c0143

.field public static final DIRECTIONS_TAXI_TRAVEL_TIME:I = 0x7f0c0152

.field public static final DIRECTIONS_TAXI_WAITING_TIME:I = 0x7f0c0153

.field public static final DIRECTIONS_TRANSIT_DATETIME_OPTIONS_TITLE:I = 0x7f0c0135

.field public static final DIRECTIONS_TRANSIT_OPTIONS_PREFERRED_MODE_CAPTION:I = 0x7f0c0136

.field public static final DIRECTIONS_TRANSIT_OPTIONS_PREFERRED_RAIL_TRANSIT_MODE_TEXT:I = 0x7f0c013c

.field public static final DIRECTIONS_TRANSIT_OPTIONS_PREFERRED_TRAM_LIGHT_RAIL_TRANSIT_MODE_TEXT:I = 0x7f0c013b

.field public static final DIRECTIONS_TRANSIT_OPTIONS_ROUTES_SORTING_BEST_ROUTE:I = 0x7f0c013e

.field public static final DIRECTIONS_TRANSIT_OPTIONS_ROUTES_SORTING_FEWER_TRANSFERS:I = 0x7f0c013f

.field public static final DIRECTIONS_TRANSIT_OPTIONS_ROUTES_SORTING_LESS_WALKINS:I = 0x7f0c0140

.field public static final DIRECTIONS_TRANSIT_OPTIONS_ROUTES_SORTING_ORDER_CAPTION:I = 0x7f0c013d

.field public static final DIRECTIONS_TRIGGER_BUTTON_TEXT:I = 0x7f0c0122

.field public static final DIRECTIONS_VIEW_ALTERNATE_ROUTES:I = 0x7f0c014f

.field public static final DIRECTIONS_WIDGET_NAME:I = 0x7f0c03dc

.field public static final DIRECTIONS_WIDGET_NAVIGATION_CHECKBOX:I = 0x7f0c03de

.field public static final DIRECTIONS_WIDGET_SAVE:I = 0x7f0c03e0

.field public static final DIRECTIONS_WIDGET_SHORTCUT_NAME:I = 0x7f0c03df

.field public static final DIRECTIONS_WIDGET_TITLE:I = 0x7f0c03dd

.field public static final DIRECTIONS_YOUR_LOCATION:I = 0x7f0c0129

.field public static final DISABLE_SHAKE_DIALOG_MESSAGE:I = 0x7f0c0200

.field public static final DISABLE_SHAKE_DIALOG_NO:I = 0x7f0c0202

.field public static final DISABLE_SHAKE_DIALOG_TITLE:I = 0x7f0c01ff

.field public static final DISABLE_SHAKE_DIALOG_YES:I = 0x7f0c0201

.field public static final DISCLAIMER:I = 0x7f0c0295

.field public static final DISCLAIMER_CHECKBOX:I = 0x7f0c0296

.field public static final DISCLAIMER_TITLE:I = 0x7f0c0294

.field public static final DISMISS:I = 0x7f0c0222

.field public static final DISTANCE_TO_DESTINATION:I = 0x7f0c0311

.field public static final DONE:I = 0x7f0c00d6

.field public static final DONT_SHOW_AGAIN:I = 0x7f0c00e4

.field public static final DRIVING_TRAVEL_MODE_LABEL:I = 0x7f0c0188

.field public static final DURATION_AND_DISTANCE_TO_DESTINATION:I = 0x7f0c0310

.field public static final EDIT_HOME_LOCATION:I = 0x7f0c019b

.field public static final EDIT_HOME_WORK:I = 0x7f0c0234

.field public static final EDIT_OWNER_RESPONSE:I = 0x7f0c035e

.field public static final EDIT_PLACE_ALIAS:I = 0x7f0c0235

.field public static final EDIT_RATING:I = 0x7f0c0376

.field public static final EDIT_REVIEW:I = 0x7f0c0378

.field public static final EDIT_REVIEW_TITLE:I = 0x7f0c034d

.field public static final EDIT_WORK_LOCATION:I = 0x7f0c019c

.field public static final ELLIPSIS:I = 0x7f0c00c4

.field public static final ENABLE_PLAY_SERVICES_FOR_LOCATION_MESSAGE:I = 0x7f0c0385

.field public static final ENABLE_PLAY_SERVICES_FOR_LOCATION_TITLE:I = 0x7f0c0384

.field public static final ENABLE_PLAY_SERVICES_GENERIC_MESSAGE:I = 0x7f0c0388

.field public static final ENHANCE_GOOGLE_MAPS_EXPERIENCE:I = 0x7f0c0272

.field public static final ENTER_RESPONSE_AS_OWNER:I = 0x7f0c0368

.field public static final ERROR_GPS_HARDWARE:I = 0x7f0c02a2

.field public static final ERROR_GPS_LOCATION:I = 0x7f0c029e

.field public static final ERROR_GPS_LOCATION_KITKAT:I = 0x7f0c02a0

.field public static final ERROR_LOCATION_SERVICES:I = 0x7f0c029c

.field public static final ERROR_TITLE_GPS_HARDWARE:I = 0x7f0c02a1

.field public static final ERROR_TITLE_GPS_LOCATION:I = 0x7f0c029d

.field public static final ERROR_TITLE_GPS_LOCATION_KITKAT:I = 0x7f0c029f

.field public static final ERROR_TITLE_LOCATION_SERVICES:I = 0x7f0c029b

.field public static final ESTIMATED_TIME_OF_ARRIVAL:I = 0x7f0c0312

.field public static final ESTIMATED_TIME_OF_ARRIVAL_SHORT:I = 0x7f0c0313

.field public static final EXTERNAL_INVOCATION_UNSUPPORTED_LINK_MESSAGE:I = 0x7f0c0463

.field public static final EXTERNAL_INVOCATION_UNSUPPORTED_LINK_TITLE:I = 0x7f0c0462

.field public static final EXTERNAL_LINKS:I = 0x7f0c00ff

.field public static final FACEPILE_HEADER:I = 0x7f0c0497

.field public static final FACEPILE_TEXT_PLACEHOLDER:I = 0x7f0c0496

.field public static final FAILED_TO_FETCH_PHOTOS:I = 0x7f0c011e

.field public static final FAILED_TO_FETCH_SELF_REVIEW:I = 0x7f0c010d

.field public static final FAILED_TO_LAUNCH_APPLICATION:I = 0x7f0c048d

.field public static final FAILED_TO_REPORT_PROBLEM:I = 0x7f0c01bc

.field public static final FAILED_TO_SAVE_OFFLINE_MAPS:I = 0x7f0c0441

.field public static final FAILED_TO_SAVE_PLACE:I = 0x7f0c0098

.field public static final FAILED_TO_UPDATE_HOME_LOCATION:I = 0x7f0c019d

.field public static final FAILED_TO_UPDATE_WORK_LOCATION:I = 0x7f0c019e

.field public static final FEATURED_BY_ZAGAT:I = 0x7f0c036c

.field public static final FEATURE_SELECTION_HINT:I = 0x7f0c01e1

.field public static final FEATURE_SELECTION_HINT_ADD_A_PLACE:I = 0x7f0c01e2

.field public static final FEATURE_SELECTION_HINT_REPORT_A_PROBLEM:I = 0x7f0c01e3

.field public static final FEATURE_SELECTION_TITLE:I = 0x7f0c01e0

.field public static final FORM_FIELD_REQUIRED:I = 0x7f0c0417

.field public static final FPR_RESULT:I = 0x7f0c0498

.field public static final FRIDAY:I = 0x7f0c00f3

.field public static final GESTURES:I = 0x7f0c0257

.field public static final GOOGLE_EARTH_NOT_AVAILABLE:I = 0x7f0c00bf

.field public static final GOOGLE_LOCATION_SETTINGS:I = 0x7f0c0236

.field public static final GOOGLE_PLAY_SERVICES_ENABLE:I = 0x7f0c037c

.field public static final GOOGLE_PLAY_SERVICES_INSTALL:I = 0x7f0c037b

.field public static final GOOGLE_PLAY_SERVICES_NOTIFICATION_TICKER:I = 0x7f0c037f

.field public static final GOOGLE_PLAY_SERVICES_NOTIFICATION_TITLE:I = 0x7f0c037e

.field public static final GOOGLE_PLAY_SERVICES_UNKNOWN_ISSUE:I = 0x7f0c037a

.field public static final GOOGLE_PLAY_SERVICES_UPDATE:I = 0x7f0c037d

.field public static final HEADS_UP_GUIDANCE_NOTIFICATION_TITLE_FORMAT:I = 0x7f0c0316

.field public static final HELP:I = 0x7f0c0259

.field public static final HELP_AND_FEEDBACK:I = 0x7f0c0254

.field public static final HOME_LOCATION:I = 0x7f0c0197

.field public static final HOTEL_BOOK:I = 0x7f0c03f1

.field public static final HOTEL_BOOKING_HEADER:I = 0x7f0c0371

.field public static final HOTEL_BOOK_FROM:I = 0x7f0c03f0

.field public static final HOTEL_CHECK_IN:I = 0x7f0c03e9

.field public static final HOTEL_CHECK_OUT:I = 0x7f0c03ea

.field public static final HOTEL_CURRENCY_DISCLAIMER:I = 0x7f0c03ed

.field public static final HOTEL_INCLUSIVE_PRICE:I = 0x7f0c03f2

.field public static final HOTEL_MORE_PRICES:I = 0x7f0c03ec

.field public static final HOTEL_NO_AVAILABILITY:I = 0x7f0c03ee

.field public static final HOTEL_PLACE_BOOK_LINK_TITLE:I = 0x7f0c03ef

.field public static final HOTEL_PRICE_PER_NIGHT:I = 0x7f0c03eb

.field public static final HOURS:I = 0x7f0c0104

.field public static final HOW_TO_GET_STARTED:I = 0x7f0c0255

.field public static final HOW_TO_SEARCH_AND_MANAGE_CONTACTS:I = 0x7f0c0256

.field public static final IAMHERE_ADD_A_PLACE:I = 0x7f0c0449

.field public static final IAMHERE_ARE_YOU_AT:I = 0x7f0c044b

.field public static final IAMHERE_ARE_YOU_AT_PLAIN:I = 0x7f0c044d

.field public static final IAMHERE_EXPLORE_NEARBY:I = 0x7f0c0451

.field public static final IAMHERE_INTERNAL_ONLY:I = 0x7f0c044a

.field public static final IAMHERE_NOT_AT_THIS_PLACE:I = 0x7f0c0452

.field public static final IAMHERE_NOW_AT:I = 0x7f0c044e

.field public static final IAMHERE_NOW_AT_PLAIN:I = 0x7f0c044f

.field public static final IAMHERE_OR_ARE_YOU_AT:I = 0x7f0c044c

.field public static final IAMHERE_REPORT_BLUE_DOT_PROBLEMS:I = 0x7f0c0455

.field public static final IAMHERE_SOMEWHERE_ELSE:I = 0x7f0c0454

.field public static final IAMHERE_VIEW_MORE:I = 0x7f0c0450

.field public static final IAMHERE_WHERE_ARE_YOU:I = 0x7f0c0453

.field public static final IGNORE_BUTTON:I = 0x7f0c00cb

.field public static final IMPROVE_YOUR_LOCATION:I = 0x7f0c0237

.field public static final INCORRECTLY_DRAWN:I = 0x7f0c01d7

.field public static final INNERSPACE:I = 0x7f0c009c

.field public static final INSTALL_PLAY_SERVICES_FOR_LOCATION_MESSAGE:I = 0x7f0c0381

.field public static final INSTALL_PLAY_SERVICES_FOR_LOCATION_TITLE:I = 0x7f0c0380

.field public static final INSTALL_PLAY_SERVICES_GENERIC_MESSAGE:I = 0x7f0c0386

.field public static final JOIN_LOCAL_GUIDES:I = 0x7f0c04a6

.field public static final KOREAN_LOCATION_TERMS_OF_SERVICE:I = 0x7f0c026b

.field public static final KOREA_LEGAL_TEXT:I = 0x7f0c026a

.field public static final LABELED_AS:I = 0x7f0c038e

.field public static final LAYERS_TERRAIN:I = 0x7f0c00a8

.field public static final LAYER_BICYCLING:I = 0x7f0c00a9

.field public static final LAYER_GOOGLE_EARTH:I = 0x7f0c00aa

.field public static final LAYER_PUBLIC_TRANSIT:I = 0x7f0c00a7

.field public static final LAYER_SATELLITE:I = 0x7f0c00ab

.field public static final LAYER_TRAFFIC:I = 0x7f0c00a6

.field public static final LEARN_MORE:I = 0x7f0c026c

.field public static final LEARN_MORE_ABOUT_GMM:I = 0x7f0c026d

.field public static final LEGAL_NOTICES:I = 0x7f0c0265

.field public static final LEGAL_TEXT:I = 0x7f0c0269

.field public static final LIST_BULLET:I = 0x7f0c023e

.field public static final LOADING:I = 0x7f0c0093

.field public static final LOADING_ADDRESS_CONTEXT_LABEL:I = 0x7f0c0094

.field public static final LOCAL_ACTIVITY_NAME:I = 0x7f0c008f

.field public static final LOCAL_GUIDES_DESCRIPTION:I = 0x7f0c04a5

.field public static final LOCAL_GUIDES_TITLE:I = 0x7f0c04a4

.field public static final LOCAL_TIME_TITLE:I = 0x7f0c049d

.field public static final LOCATION:I = 0x7f0c01c1

.field public static final LOCATION_DATA_ERROR:I = 0x7f0c0284

.field public static final LOCATION_FEEDBACK_CORRECT:I = 0x7f0c021b

.field public static final LOCATION_FEEDBACK_DISABLED:I = 0x7f0c0218

.field public static final LOCATION_FEEDBACK_HINT_PICK_LOCATION:I = 0x7f0c021d

.field public static final LOCATION_FEEDBACK_IMPROVE_LOCATION:I = 0x7f0c021e

.field public static final LOCATION_FEEDBACK_LESS_500:I = 0x7f0c0216

.field public static final LOCATION_FEEDBACK_MORE_500:I = 0x7f0c0215

.field public static final LOCATION_FEEDBACK_OTHER:I = 0x7f0c021a

.field public static final LOCATION_FEEDBACK_PICK_LOCATION:I = 0x7f0c021c

.field public static final LOCATION_FEEDBACK_TITLE:I = 0x7f0c0214

.field public static final LOCATION_FEEDBACK_WRONG_DIRECTION:I = 0x7f0c0217

.field public static final LOCATION_FEEDBACK_WRONG_PLACE:I = 0x7f0c0219

.field public static final LOCATION_MODE_HIGH_ACCURACY:I = 0x7f0c023a

.field public static final LOCATION_NOT_YET_AVAILABLE:I = 0x7f0c030f

.field public static final LOCATION_REPORT_TEXT:I = 0x7f0c026e

.field public static final LOCATION_SETTING_IS_ALREADY_OPTIMIZED:I = 0x7f0c023d

.field public static final LOCATION_SOURCE_GPS:I = 0x7f0c023c

.field public static final LOCATION_SOURCE_NETWORK_LOCATION:I = 0x7f0c0239

.field public static final LOCATION_SOURCE_WIFI:I = 0x7f0c023b

.field public static final LOGIN_DIALOG_ADD_ACCOUNT_LABEL:I = 0x7f0c022e

.field public static final LOGIN_DIALOG_TITLE:I = 0x7f0c022d

.field public static final LOGIN_ERROR_MESSAGE:I = 0x7f0c0468

.field public static final LOGIN_ERROR_TITLE:I = 0x7f0c0467

.field public static final LOGIN_PROMPT_PANEL_BUTTON_LABEL:I = 0x7f0c022b

.field public static final LOGIN_PROMPT_PANEL_LEARN_MORE_LINK:I = 0x7f0c022c

.field public static final LOGIN_PROMPT_PANEL_MESSAGE:I = 0x7f0c0225

.field public static final LOGIN_PROMPT_PANEL_MESSAGE_MYPROFILE:I = 0x7f0c0226

.field public static final LOGIN_PROMPT_PANEL_MESSAGE_MYPROFILE_HEADER:I = 0x7f0c0228

.field public static final LOGIN_PROMPT_PANEL_MESSAGE_MYPROFILE_SUBHEADER:I = 0x7f0c0229

.field public static final LOGIN_PROMPT_PANEL_MESSAGE_MYPROFILE_WITH_OFFLINE:I = 0x7f0c0227

.field public static final LOGIN_PROMPT_PANEL_MESSAGE_TRANSIT:I = 0x7f0c022a

.field public static final LOGIN_PROMPT_PANEL_OOB_TITLE:I = 0x7f0c0223

.field public static final LOGIN_PROMPT_PANEL_TITLE:I = 0x7f0c0224

.field public static final MANAGE_LOCATION_REPORT_TEXT:I = 0x7f0c026f

.field public static final MAPS_ACTIVITY_TODO_ALL_CAUGHT_UP:I = 0x7f0c0460

.field public static final MAPS_ACTIVITY_TODO_KEEP_EXPLORING:I = 0x7f0c0461

.field public static final MAPS_APP_NAME:I = 0x7f0c008e

.field public static final MAPS_HISTORY:I = 0x7f0c023f

.field public static final MAP_COPYRIGHTS_FULL:I = 0x7f0c0342

.field public static final MAP_COPYRIGHTS_GOOGLE_ONLY:I = 0x7f0c0341

.field public static final MAP_COPYRIGHTS_IMAGERY_ONLY:I = 0x7f0c0343

.field public static final MAP_COPYRIGHTS_MAP_DATA_ONLY:I = 0x7f0c0344

.field public static final MARKER_IS_INCORRECT:I = 0x7f0c01c2

.field public static final MARKER_UPDATED:I = 0x7f0c01c3

.field public static final MENU_ALTERNATES:I = 0x7f0c02ad

.field public static final MENU_DESTINATION_INFO:I = 0x7f0c02ae

.field public static final MENU_DIRECTIONS_LIST:I = 0x7f0c02a9

.field public static final MENU_MIC_BUTTON:I = 0x7f0c02a8

.field public static final MENU_MUTE:I = 0x7f0c02aa

.field public static final MENU_MUTE_VOICE:I = 0x7f0c02ab

.field public static final MENU_ROUTE_OVERVIEW:I = 0x7f0c02a7

.field public static final MENU_UNMUTE_VOICE:I = 0x7f0c02ac

.field public static final MONDAY:I = 0x7f0c00ef

.field public static final MORE_REVIEWS:I = 0x7f0c010e

.field public static final MY_PLACES_DELETE_CONFIRM:I = 0x7f0c01a1

.field public static final MY_PLACES_DELETE_CONFIRM_TITLE:I = 0x7f0c019f

.field public static final MY_PLACES_DELETE_ITEM_COMMAND:I = 0x7f0c01a0

.field public static final MY_PLACES_DELETE_PLACE_CONFIRM:I = 0x7f0c01a2

.field public static final MY_PLACES_DELETE_SERVICE_UNAVAILABLE:I = 0x7f0c01a3

.field public static final MY_PROFILE:I = 0x7f0c0196

.field public static final NATURAL_OUTLINE:I = 0x7f0c01db

.field public static final NAVIGATE_UP:I = 0x7f0c00e5

.field public static final NAVIGATION:I = 0x7f0c0193

.field public static final NAVIGATION_ACTIVITY_NAME:I = 0x7f0c0090

.field public static final NAVIGATION_NOT_ALLOWED:I = 0x7f0c0182

.field public static final NAVIGATION_SETTINGS:I = 0x7f0c00b9

.field public static final NAVIGATION_SETTING_HEADING_UP_PREFERRED:I = 0x7f0c00be

.field public static final NAVIGATION_STEP_LIST_DISTANCE_TO_NEXT_STEP:I = 0x7f0c0314

.field public static final NAV_HIDE_LAYER_SATELLITE:I = 0x7f0c00b6

.field public static final NAV_HIDE_LAYER_TRAFFIC:I = 0x7f0c00b8

.field public static final NAV_LAYER_SATELLITE:I = 0x7f0c00b5

.field public static final NAV_LAYER_TRAFFIC:I = 0x7f0c00b7

.field public static final NETWORK_UNAVAILABLE:I = 0x7f0c00e1

.field public static final NET_FAIL_TITLE:I = 0x7f0c0091

.field public static final NEXT:I = 0x7f0c01ea

.field public static final NOT_OFFLINEABLE:I = 0x7f0c042c

.field public static final NO_BUTTON:I = 0x7f0c00c6

.field public static final NO_ROUTE_FOUND:I = 0x7f0c017f

.field public static final NO_THANKS:I = 0x7f0c0275

.field public static final NO_TRIPS_ON_GIVEN_DATE:I = 0x7f0c0181

.field public static final OFFLINE_AREA_EXPIRED:I = 0x7f0c0436

.field public static final OFFLINE_AREA_EXPIRES_IN_DAYS:I = 0x7f0c0435

.field public static final OFFLINE_AREA_TOUCH_TO_UPDATE:I = 0x7f0c0438

.field public static final OFFLINE_AREA_UPDATE_REQUIRED:I = 0x7f0c0437

.field public static final OFFLINE_CACHE_AREA_TOO_LARGE:I = 0x7f0c042b

.field public static final OFFLINE_CACHE_CANCELING_DOWNLOAD:I = 0x7f0c0429

.field public static final OFFLINE_CACHE_SAVED:I = 0x7f0c042a

.field public static final OFFLINE_CACHE_SAVING:I = 0x7f0c0428

.field public static final OFFLINE_CACHE_SELECT_AREA_HELP:I = 0x7f0c0426

.field public static final OFFLINE_CACHE_SELECT_AREA_TITLE:I = 0x7f0c0425

.field public static final OFFLINE_CACHE_START_SELECTION_BUTTON:I = 0x7f0c0422

.field public static final OFFLINE_CACHE_START_SELECTION_BUTTON2:I = 0x7f0c0423

.field public static final OFFLINE_CACHE_START_SELECTION_BUTTON_IN_MENU:I = 0x7f0c0424

.field public static final OFFLINE_DOWNLOAD_CANCELLED:I = 0x7f0c0434

.field public static final OFFLINE_DOWNLOAD_SUCCEEDED:I = 0x7f0c0433

.field public static final OFFLINE_MAPS_CARD_MANAGE_BUTTON:I = 0x7f0c0431

.field public static final OFFLINE_MAPS_CARD_PROMOTION_BODY:I = 0x7f0c0432

.field public static final OFFLINE_MAPS_CARD_SIGN_IN_PROMPT:I = 0x7f0c0440

.field public static final OFFLINE_MAPS_DELETE_AREA_CONFIRM:I = 0x7f0c043d

.field public static final OFFLINE_MAPS_HELP_TEXT:I = 0x7f0c042d

.field public static final OFFLINE_MAPS_LOW_STORAGE_SPACE_ALERT_BODY:I = 0x7f0c0443

.field public static final OFFLINE_MAPS_LOW_STORAGE_SPACE_ALERT_TITLE:I = 0x7f0c0442

.field public static final OFFLINE_MAPS_REDOWNLOAD_PROMPT_BODY:I = 0x7f0c043f

.field public static final OFFLINE_MAPS_REDOWNLOAD_PROMPT_TITLE:I = 0x7f0c043e

.field public static final OFFLINE_MAPS_RENAME_AREA_TITLE:I = 0x7f0c043c

.field public static final OFFLINE_MAPS_TITLE:I = 0x7f0c0430

.field public static final OFFLINE_MENU_DELETE_AREA:I = 0x7f0c043b

.field public static final OFFLINE_MENU_RENAME_AREA:I = 0x7f0c0439

.field public static final OFFLINE_MENU_UPDATE_AREA:I = 0x7f0c043a

.field public static final OFFLINE_NAME_AREA_TITLE:I = 0x7f0c0427

.field public static final OFFLINE_SAVING_CANCEL_DIALOG_BODY:I = 0x7f0c042f

.field public static final OFFLINE_SAVING_CANCEL_DIALOG_TITLE:I = 0x7f0c042e

.field public static final OK_BUTTON:I = 0x7f0c00c7

.field public static final ONE_WAY_INCORRECT:I = 0x7f0c01d6

.field public static final OPEN_24_HOURS:I = 0x7f0c00f8

.field public static final OPEN_SOURCE_LICENSES:I = 0x7f0c0266

.field public static final OTHER:I = 0x7f0c01c9

.field public static final OWNER_RESPONDING_AS:I = 0x7f0c0366

.field public static final OWNER_RESPONSE_TITLE:I = 0x7f0c035f

.field public static final PAGING_INFO:I = 0x7f0c011d

.field public static final PANOS_REQUIRE_GPLUS_PROFILE:I = 0x7f0c038b

.field public static final PANO_PHOTO_UPLOAD_NO_GEO_TAG:I = 0x7f0c0118

.field public static final PANO_PHOTO_UPLOAD_TITLE:I = 0x7f0c0116

.field public static final PANO_UPLOADED_MSG:I = 0x7f0c011b

.field public static final PERSONAL_EVENT_EMAIL:I = 0x7f0c03fc

.field public static final PERSONAL_FLIGHT_ARRIVAL_AIRPORT:I = 0x7f0c0401

.field public static final PERSONAL_FLIGHT_ARRIVES:I = 0x7f0c0405

.field public static final PERSONAL_FLIGHT_BOARDS:I = 0x7f0c0403

.field public static final PERSONAL_FLIGHT_CONFIRMATION:I = 0x7f0c0402

.field public static final PERSONAL_FLIGHT_DEPARTS:I = 0x7f0c03ff

.field public static final PERSONAL_FLIGHT_DEPARTURE_AIRPORT:I = 0x7f0c0400

.field public static final PERSONAL_FLIGHT_DETAILS_SEPARATOR:I = 0x7f0c0404

.field public static final PERSONAL_FLIGHT_EMAIL:I = 0x7f0c0407

.field public static final PERSONAL_FLIGHT_GATE:I = 0x7f0c0406

.field public static final PERSONAL_FLIGHT_NUMBER_AND_DATE:I = 0x7f0c03fe

.field public static final PERSONAL_HOTEL_CHECK_IN:I = 0x7f0c03f6

.field public static final PERSONAL_HOTEL_CHECK_IN_TIME:I = 0x7f0c03f8

.field public static final PERSONAL_HOTEL_CHECK_OUT:I = 0x7f0c03f7

.field public static final PERSONAL_HOTEL_CHECK_OUT_TIME:I = 0x7f0c03f9

.field public static final PERSONAL_RESERVATION_EMAIL:I = 0x7f0c03fa

.field public static final PERSONAL_RESTAURANT_RESERVATION:I = 0x7f0c03f4

.field public static final PERSONAL_RESTAURANT_RESERVATION_SIZE:I = 0x7f0c03f5

.field public static final PERSONAL_UPCOMING_EVENT:I = 0x7f0c03fb

.field public static final PERSONAL_UPCOMING_FLIGHT:I = 0x7f0c03fd

.field public static final PERSONAL_VISIBLE_ONLY_TO_YOU:I = 0x7f0c0408

.field public static final PHOTOS_REQUIRE_GPLUS_PROFILE:I = 0x7f0c038a

.field public static final PHOTO_BY:I = 0x7f0c010f

.field public static final PHOTO_UPLOADED_TITLE:I = 0x7f0c011a

.field public static final PICK_GALLERY_PHOTO:I = 0x7f0c0112

.field public static final PLACE_CLOSED:I = 0x7f0c01e9

.field public static final PLACE_CLOSED_NOW:I = 0x7f0c00f7

.field public static final PLACE_CLOSED_TODAY:I = 0x7f0c00f6

.field public static final PLACE_CLOSES_AT:I = 0x7f0c0101

.field public static final PLACE_CLOSING_SOON:I = 0x7f0c0100

.field public static final PLACE_MENU:I = 0x7f0c00fb

.field public static final PLACE_MORE_INFO:I = 0x7f0c00fc

.field public static final PLACE_MORE_INFO_SUBTITLE:I = 0x7f0c00fd

.field public static final PLACE_NO_REVIEWS:I = 0x7f0c00ea

.field public static final PLACE_OPEN_NOW:I = 0x7f0c0102

.field public static final PLACE_OPEN_TODAY:I = 0x7f0c0103

.field public static final PLACE_PAGE_COLLAPSED_NUMBER_OF_REVIEWS:I = 0x7f0c0346

.field public static final PLACE_PAGE_DISTANCE_BUTTON_DESCRIPTION:I = 0x7f0c0345

.field public static final PLACE_PERMANENTLY_CLOSED:I = 0x7f0c00eb

.field public static final PLACE_PHONE:I = 0x7f0c01e4

.field public static final PLACE_RELOCATED:I = 0x7f0c00ec

.field public static final PLACE_RELOCATED_TO:I = 0x7f0c00ed

.field public static final PLACE_RESERVATION:I = 0x7f0c00fa

.field public static final PLACE_SUGGEST_AN_EDIT:I = 0x7f0c00fe

.field public static final PLACE_TITLE_SANTAS_VILLAGE:I = 0x7f0c00e6

.field public static final PLACE_TITLE_VISIT_SANTAS_VILLAGE:I = 0x7f0c00e7

.field public static final PLACE_UPCOMING_CARD_TITLE:I = 0x7f0c00e8

.field public static final PLACE_UPCOMING_MORE_EVENTS:I = 0x7f0c00e9

.field public static final PLACE_WEBSITE:I = 0x7f0c00f9

.field public static final POLITICAL_BORDER:I = 0x7f0c01da

.field public static final POST_BUTTON:I = 0x7f0c0351

.field public static final PREFETCH_AREA_TOO_LARGE:I = 0x7f0c03e6

.field public static final PREFETCH_DOWNLOAD_PERCENT:I = 0x7f0c03e3

.field public static final PREFETCH_NOT_ALLOWED:I = 0x7f0c03e5

.field public static final PREFETCH_SUCCESS:I = 0x7f0c03e4

.field public static final PRIVACY_POLICY:I = 0x7f0c0264

.field public static final PROBLEM_CONNECTING_TO_THE_NETWORK_TEXT:I = 0x7f0c00e2

.field public static final PROBLEM_REPORTED:I = 0x7f0c01bb

.field public static final PUBLIC_ON_THE_WEB:I = 0x7f0c0367

.field public static final PUBLISH_BUTTON:I = 0x7f0c0350

.field public static final PUBLISH_PHOTOS:I = 0x7f0c0119

.field public static final QUICK_FACTS_ATTRIBUTE_VALUE_SEPARATOR:I = 0x7f0c041e

.field public static final QUICK_FACTS_NAME:I = 0x7f0c041f

.field public static final QUICK_FACTS_SOURCES_INCLUDE:I = 0x7f0c0420

.field public static final QUICK_FACTS_VALUE_SEPARATOR:I = 0x7f0c0421

.field public static final RAP_MARKER_LOCATION:I = 0x7f0c01c5

.field public static final RAP_MARKER_LOCATION_INCORRECT:I = 0x7f0c01c6

.field public static final RAP_MARKER_LOCATION_UPDATED:I = 0x7f0c01c8

.field public static final RAP_SELECT_INCORRECT_INFORMATION:I = 0x7f0c01a5

.field public static final RAP_TYPE_CORRECT_ADDRESS:I = 0x7f0c01c0

.field public static final RAP_TYPE_CORRECT_NAME:I = 0x7f0c01d3

.field public static final RAP_TYPE_CORRECT_PHONE_NUMBER:I = 0x7f0c01e8

.field public static final RAP_TYPE_CORRECT_ROAD_NAME:I = 0x7f0c01d5

.field public static final RAP_TYPE_CORRECT_WEBSITE:I = 0x7f0c01dd

.field public static final RAP_UPDATE_LOCATION_ON_MAP:I = 0x7f0c01c7

.field public static final RATE_AND_REVIEW:I = 0x7f0c0347

.field public static final RATE_AND_REVIEW_NO_REVIEWS:I = 0x7f0c0348

.field public static final RATE_AND_REVIEW_NO_REVIEWS_BODY:I = 0x7f0c0349

.field public static final READ_MORE:I = 0x7f0c01fd

.field public static final REMOVE:I = 0x7f0c009e

.field public static final REPORTED_VIA_WAZE_APP:I = 0x7f0c0495

.field public static final REPORT_A_PROBLEM:I = 0x7f0c01a4

.field public static final REPORT_A_PROBLEM_NO_INFORMATION_SELECTED:I = 0x7f0c01b9

.field public static final REPORT_DIRECTION_ISSUE:I = 0x7f0c020a

.field public static final REPORT_DIRECTION_ISSUE_DESCRIPTION:I = 0x7f0c020b

.field public static final REPORT_INAPPROPRIATE_PHOTO:I = 0x7f0c0220

.field public static final REPORT_INAPPROPRIATE_REVIEW:I = 0x7f0c0221

.field public static final REPORT_MAPPING_ISSUE:I = 0x7f0c0206

.field public static final REPORT_MAPPING_ISSUE_DESCRIPTION:I = 0x7f0c0207

.field public static final REPORT_MAP_ISSUE_DESCRIPTION_HINT:I = 0x7f0c01a6

.field public static final REPORT_MAP_ISSUE_EMAIL_ME_PRIVACY_NOTICE:I = 0x7f0c01b7

.field public static final REPORT_MAP_ISSUE_EMAIL_ME_TEXT:I = 0x7f0c01b6

.field public static final REPORT_MAP_ISSUE_SUBMIT:I = 0x7f0c01b5

.field public static final REPORT_MAP_ISSUE_TYPE_ADDRESS:I = 0x7f0c01af

.field public static final REPORT_MAP_ISSUE_TYPE_ADDRESS_CITY:I = 0x7f0c01b2

.field public static final REPORT_MAP_ISSUE_TYPE_ADDRESS_LOCATION:I = 0x7f0c01b0

.field public static final REPORT_MAP_ISSUE_TYPE_ADDRESS_OTHER:I = 0x7f0c01b3

.field public static final REPORT_MAP_ISSUE_TYPE_ADDRESS_ZIP:I = 0x7f0c01b1

.field public static final REPORT_MAP_ISSUE_TYPE_OTHER:I = 0x7f0c01b4

.field public static final REPORT_MAP_ISSUE_TYPE_STREET:I = 0x7f0c01a8

.field public static final REPORT_MAP_ISSUE_TYPE_STREET_CLOSED:I = 0x7f0c01aa

.field public static final REPORT_MAP_ISSUE_TYPE_STREET_INACCURATE:I = 0x7f0c01a9

.field public static final REPORT_MAP_ISSUE_TYPE_STREET_MISSING:I = 0x7f0c01ad

.field public static final REPORT_MAP_ISSUE_TYPE_STREET_ONEWAY:I = 0x7f0c01ab

.field public static final REPORT_MAP_ISSUE_TYPE_STREET_OTHER:I = 0x7f0c01ae

.field public static final REPORT_MAP_ISSUE_TYPE_STREET_PRIVATE:I = 0x7f0c01ac

.field public static final REPORT_OWNER_RESPONSE:I = 0x7f0c035c

.field public static final REPORT_STREETVIEW_ISSUE:I = 0x7f0c0208

.field public static final REPORT_STREETVIEW_ISSUE_DESCRIPTION:I = 0x7f0c0209

.field public static final REPORT_SUGGESTION_ISSUE:I = 0x7f0c020c

.field public static final REPORT_SUGGESTION_ISSUE_DESCRIPTION:I = 0x7f0c020d

.field public static final RESERVATION_CONFIRMATION:I = 0x7f0c041c

.field public static final RESERVATION_CONFIRMED:I = 0x7f0c041a

.field public static final RESERVATION_CONFIRMED_HEADER:I = 0x7f0c041b

.field public static final RESERVATION_DETAILS_TITLE:I = 0x7f0c0412

.field public static final RESERVATION_DISCLAIMER:I = 0x7f0c0418

.field public static final RESERVATION_EMAIL:I = 0x7f0c0415

.field public static final RESERVATION_FIRST_NAME:I = 0x7f0c0413

.field public static final RESERVATION_LAST_NAME:I = 0x7f0c0414

.field public static final RESERVATION_PHONE_NUMBER:I = 0x7f0c0416

.field public static final RESERVATION_POWERED_BY:I = 0x7f0c040e

.field public static final RESERVATION_SEARCH_UNKNOWN_ERROR:I = 0x7f0c040d

.field public static final RESERVATION_SELECT_DATE_TIME:I = 0x7f0c040b

.field public static final RESERVATION_SELECT_PARTY_SIZE:I = 0x7f0c040c

.field public static final RESERVATION_SELECT_PREFERRED_TIME:I = 0x7f0c0409

.field public static final RESERVATION_SELECT_TABLE:I = 0x7f0c040a

.field public static final RESERVATION_SIGN_IN_EXPLANATION:I = 0x7f0c040f

.field public static final RESERVATION_SIGN_IN_PROMPT:I = 0x7f0c0410

.field public static final RESERVATION_SIGN_IN_TITLE:I = 0x7f0c0411

.field public static final RESPOND_AS_OWNER:I = 0x7f0c035d

.field public static final RESTRICTION_ADJUST_FILTERS:I = 0x7f0c0247

.field public static final RESTRICTION_ANY:I = 0x7f0c0242

.field public static final RESTRICTION_AT_LEAST:I = 0x7f0c024a

.field public static final RESTRICTION_CUISINE:I = 0x7f0c0241

.field public static final RESTRICTION_FILTER:I = 0x7f0c0240

.field public static final RESTRICTION_FILTER_COUNT:I = 0x7f0c0243

.field public static final RESTRICTION_FILTER_RESULTS:I = 0x7f0c0244

.field public static final RESTRICTION_HOTEL_CLASS:I = 0x7f0c024b

.field public static final RESTRICTION_NO_RESULTS:I = 0x7f0c0245

.field public static final RESTRICTION_NO_RESULTS_ADJUST_FILTERS:I = 0x7f0c0248

.field public static final RESTRICTION_OPEN_NOW:I = 0x7f0c024e

.field public static final RESTRICTION_PRICE:I = 0x7f0c024c

.field public static final RESTRICTION_RESET:I = 0x7f0c024d

.field public static final RESTRICTION_TRY_ADJUSTING_YOUR_FILTERS:I = 0x7f0c0249

.field public static final RESTRICTION_USER_RATING:I = 0x7f0c0250

.field public static final RESTRICTION_YOUR_PLACES:I = 0x7f0c024f

.field public static final RESUME_FOLLOW_MODE:I = 0x7f0c02a6

.field public static final RESUME_NAVIGATION_TO_DESTINATION:I = 0x7f0c0169

.field public static final REVIEWS_REQUIRE_GPLUS_PROFILE:I = 0x7f0c0389

.field public static final REVIEW_ASPECT_OVERALL:I = 0x7f0c01fe

.field public static final REVIEW_CHAR_LIMIT_INLINE:I = 0x7f0c0464

.field public static final REVIEW_CHAR_LIMIT_MESSAGE:I = 0x7f0c0466

.field public static final REVIEW_CHAR_LIMIT_TITLE:I = 0x7f0c0465

.field public static final REVIEW_SUMMARY_TITLE:I = 0x7f0c035b

.field public static final RIDDLER_HELP_AGAIN:I = 0x7f0c04a1

.field public static final RIDDLER_SNACKBAR_THANKS:I = 0x7f0c04a0

.field public static final RIDDLER_TUTORIAL_THANK_YOU:I = 0x7f0c04a2

.field public static final RIDDLER_TUTORIAL_YOUR_CONTRIBUTION:I = 0x7f0c04a3

.field public static final RMI_INCORRECT:I = 0x7f0c01df

.field public static final RMI_NAME:I = 0x7f0c01d1

.field public static final RMI_NAME_HINT:I = 0x7f0c01d2

.field public static final RMI_NOT_SUPPORTED:I = 0x7f0c01a7

.field public static final RMI_SCHEDULE:I = 0x7f0c01de

.field public static final RMI_WEBSITE_HINT:I = 0x7f0c01dc

.field public static final ROAD_CLOSED:I = 0x7f0c01d9

.field public static final ROAD_IS_PRIVATE:I = 0x7f0c01d8

.field public static final ROAD_NAME_INCORRECT:I = 0x7f0c01d4

.field public static final ROUTE_AROUND_CLOSURE_CALLOUT_CLOSED:I = 0x7f0c0174

.field public static final ROUTE_AROUND_CLOSURE_CALLOUT_NEW:I = 0x7f0c0173

.field public static final ROUTE_AROUND_CLOSURE_MAIN_MESSAGE:I = 0x7f0c0170

.field public static final ROUTE_AROUND_CLOSURE_REROUTING_VIA:I = 0x7f0c0171

.field public static final ROUTE_AROUND_CLOSURE_SPEECH_ALERT:I = 0x7f0c0172

.field public static final ROUTE_AROUND_TRAFFIC_CALLOUT_CURRENT:I = 0x7f0c017b

.field public static final ROUTE_AROUND_TRAFFIC_CALLOUT_FASTER:I = 0x7f0c017c

.field public static final ROUTE_AROUND_TRAFFIC_DECLINE:I = 0x7f0c0177

.field public static final ROUTE_AROUND_TRAFFIC_MAIN_MESSAGE:I = 0x7f0c0178

.field public static final ROUTE_AROUND_TRAFFIC_REROUTE:I = 0x7f0c0175

.field public static final ROUTE_AROUND_TRAFFIC_SPEECH_MESSAGE:I = 0x7f0c0179

.field public static final ROUTE_AROUND_TRAFFIC_SPEECH_MESSAGE_WITH_VIA:I = 0x7f0c017a

.field public static final ROUTE_AROUND_TRAFFIC_TIME_SAVINGS:I = 0x7f0c0176

.field public static final ROUTE_PREVIEW_TITLE:I = 0x7f0c0499

.field public static final ROUTE_SELECTION_PAGE:I = 0x7f0c0194

.field public static final ROUTE_WITH_HEAVY_TRAFFIC:I = 0x7f0c016d

.field public static final ROUTE_WITH_LIGHT_TRAFFIC:I = 0x7f0c016f

.field public static final ROUTE_WITH_NORMAL_TRAFFIC:I = 0x7f0c016e

.field public static final SATURDAY:I = 0x7f0c00f4

.field public static final SAVE:I = 0x7f0c0096

.field public static final SAVED:I = 0x7f0c0097

.field public static final SAVED_FROM:I = 0x7f0c0099

.field public static final SAVE_PLACE_PROMOTION_MESSAGE:I = 0x7f0c0205

.field public static final SAVE_REVIEW_BUTTON:I = 0x7f0c0352

.field public static final SEARCH:I = 0x7f0c00a2

.field public static final SEARCH_ALL_RESULTS:I = 0x7f0c00d4

.field public static final SEARCH_DID_YOU_MEAN:I = 0x7f0c00d9

.field public static final SEARCH_FAILED:I = 0x7f0c00da

.field public static final SEARCH_FAILED_TEXT:I = 0x7f0c00db

.field public static final SEARCH_LIST_RESULTS:I = 0x7f0c00d3

.field public static final SEARCH_NO_RESULTS:I = 0x7f0c00d1

.field public static final SEARCH_NO_REVIEWS:I = 0x7f0c00dc

.field public static final SEARCH_RESULTS_LIST:I = 0x7f0c00d2

.field public static final SEARCH_RESULTS_QUERY:I = 0x7f0c00d5

.field public static final SEARCH_TRY_ADJUSTING_YOUR_MAP:I = 0x7f0c0246

.field public static final SELECT_INCORRECT_INFO:I = 0x7f0c01bd

.field public static final SELF_REVIEW_USERNAME:I = 0x7f0c0372

.field public static final SENDING:I = 0x7f0c01b8

.field public static final SENDING_REPORT:I = 0x7f0c01ba

.field public static final SEND_APP_FEEDBACK:I = 0x7f0c020e

.field public static final SEND_APP_FEEDBACK_DESCRIPTION:I = 0x7f0c020f

.field public static final SEND_FEEDBACK:I = 0x7f0c0251

.field public static final SEND_FEEDBACK_HELP_TEXT:I = 0x7f0c021f

.field public static final SEND_LOCATION_FEEDBACK:I = 0x7f0c0212

.field public static final SEND_LOCATION_FEEDBACK_DESCRIPTION:I = 0x7f0c0213

.field public static final SESAME_OTHER:I = 0x7f0c01fc

.field public static final SESAME_PLACE_CLOSED:I = 0x7f0c01f7

.field public static final SESAME_WRONG_ADDRESS:I = 0x7f0c01f9

.field public static final SESAME_WRONG_MARKER_LOCATION:I = 0x7f0c01fb

.field public static final SESAME_WRONG_NAME:I = 0x7f0c01f8

.field public static final SESAME_WRONG_PHONE:I = 0x7f0c01fa

.field public static final SETTINGS:I = 0x7f0c0230

.field public static final SET_HOME_LOCATION:I = 0x7f0c0199

.field public static final SET_WORK_LOCATION:I = 0x7f0c019a

.field public static final SHAKE_DIALOG_MESSAGE:I = 0x7f0c0204

.field public static final SHAKE_DIALOG_TITLE:I = 0x7f0c0203

.field public static final SHAKE_TO_SEND_FEEDBACK:I = 0x7f0c0252

.field public static final SHARE:I = 0x7f0c0095

.field public static final SHARE_DIALOG_TITLE:I = 0x7f0c011f

.field public static final SHARE_REVIEW:I = 0x7f0c034a

.field public static final SHOW_ZAGAT_LIST:I = 0x7f0c036d

.field public static final SIDE_MENU_GUIDE:I = 0x7f0c00a5

.field public static final SIGNED_IN_AS:I = 0x7f0c0276

.field public static final SIGN_IN:I = 0x7f0c0231

.field public static final SIGN_OUT_BUTTON:I = 0x7f0c00cf

.field public static final SIGN_OUT_OF_GOOGLE_MAPS:I = 0x7f0c0233

.field public static final SKIP_BUTTON:I = 0x7f0c00d0

.field public static final START_NAVIGATION:I = 0x7f0c017d

.field public static final START_PREVIEW:I = 0x7f0c017e

.field public static final STATION_VIEW_MORE:I = 0x7f0c009f

.field public static final STEP_LIST_INTERACTION_LOCKOUT:I = 0x7f0c048c

.field public static final STOP_BUTTON:I = 0x7f0c00ce

.field public static final STREETVIEW_NOT_AVAILABLE:I = 0x7f0c009b

.field public static final STREET_VIEW:I = 0x7f0c009a

.field public static final SUBMIT_OWNER_RESPONSE_FAILURE:I = 0x7f0c0361

.field public static final SUBMIT_OWNER_RESPONSE_SUCCESS:I = 0x7f0c0360

.field public static final SUBMIT_REVIEW_FOOTER:I = 0x7f0c034b

.field public static final SUBMIT_REVIEW_PUBLICLY:I = 0x7f0c034c

.field public static final SUBMIT_REVIEW_RETRY:I = 0x7f0c035a

.field public static final SUBMIT_REVIEW_SUCCESS:I = 0x7f0c0353

.field public static final SUBMIT_REVIEW_SUCCESS_THANKS:I = 0x7f0c0354

.field public static final SUNDAY:I = 0x7f0c00ee

.field public static final SWITCH_ACCOUNT:I = 0x7f0c0232

.field public static final TAKE_NEW_PHOTO:I = 0x7f0c0111

.field public static final TERMS_AND_PRIVACY:I = 0x7f0c025b

.field public static final TERMS_OF_SERVICE:I = 0x7f0c0263

.field public static final TERMS_OF_USE:I = 0x7f0c01d0

.field public static final THANK_YOU_FOR_IMPROVING_GOOGLE_MAPS:I = 0x7f0c01cb

.field public static final THURSDAY:I = 0x7f0c00f2

.field public static final TIME_REMAINING:I = 0x7f0c018f

.field public static final TIPS_AND_TRICKS:I = 0x7f0c0258

.field public static final TODAY:I = 0x7f0c00c2

.field public static final TOMORROW:I = 0x7f0c00c3

.field public static final TRAFFIC_INCIDENT_CANCEL_BUTTON:I = 0x7f0c008c

.field public static final TRAFFIC_INCIDENT_CLEARED_UP:I = 0x7f0c008d

.field public static final TRAFFIC_LAYER_UNAVAILABLE:I = 0x7f0c00ad

.field public static final TRANSIT_BLOCKTRANSFER_DESCRIPTION:I = 0x7f0c0185

.field public static final TRANSIT_BUY_TICKET:I = 0x7f0c0192

.field public static final TRANSIT_DETAILS_TRAIN_PLATFORM:I = 0x7f0c0155

.field public static final TRANSIT_DRIVE_DESCRIPTION:I = 0x7f0c0187

.field public static final TRANSIT_EVERY:I = 0x7f0c0154

.field public static final TRANSIT_MORE_DEPARTURES:I = 0x7f0c00a0

.field public static final TRANSIT_OTHER_LINES:I = 0x7f0c00a1

.field public static final TRANSIT_PARTIAL_COVERAGE_WARNING:I = 0x7f0c0137

.field public static final TRANSIT_PHONE:I = 0x7f0c0191

.field public static final TRANSIT_REALTIME_DELAY_SEPARATOR:I = 0x7f0c0159

.field public static final TRANSIT_REALTIME_INFORMATION_CANCELED:I = 0x7f0c0158

.field public static final TRANSIT_REALTIME_INFORMATION_CHANGED:I = 0x7f0c0157

.field public static final TRANSIT_REALTIME_INFORMATION_ON_TIME:I = 0x7f0c0156

.field public static final TRANSIT_STEPTHROUGH_BLOCKTRANSFER_TRANSPORTATION_AT:I = 0x7f0c015e

.field public static final TRANSIT_STEPTHROUGH_BOARD_TRANSPORTATION_AT:I = 0x7f0c015c

.field public static final TRANSIT_STEPTHROUGH_GET_OFF_AT:I = 0x7f0c015f

.field public static final TRANSIT_STEPTHROUGH_TRANSFER_TRANSPORTATION_AT:I = 0x7f0c015d

.field public static final TRANSIT_TRANSFER_DESCRIPTION:I = 0x7f0c0183

.field public static final TRANSIT_TRANSFER_DESCRIPTION_WITH_EXPRESS_TYPE:I = 0x7f0c0184

.field public static final TRANSIT_TRAVEL_MODE_LABEL:I = 0x7f0c0189

.field public static final TRANSIT_TYPE_FILTER_BUS:I = 0x7f0c0139

.field public static final TRANSIT_TYPE_FILTER_SUBWAY:I = 0x7f0c0138

.field public static final TRANSIT_TYPE_FILTER_TRAIN:I = 0x7f0c013a

.field public static final TRANSIT_VEHICLE_DELAY:I = 0x7f0c015a

.field public static final TRANSIT_VEHICLE_EARLY:I = 0x7f0c015b

.field public static final TRANSIT_WALKING_DESCRIPTION:I = 0x7f0c0186

.field public static final TRANSIT_WEBSITE:I = 0x7f0c0190

.field public static final TRY_AGAIN:I = 0x7f0c00e3

.field public static final TUESDAY:I = 0x7f0c00f0

.field public static final TURNING_ON_FOLLOWINGS_TO_IMPROVE_LOCATION:I = 0x7f0c0238

.field public static final TUTORIALS_AND_HELP:I = 0x7f0c0253

.field public static final TUTORIAL_BLUE_DOT_HINT:I = 0x7f0c0283

.field public static final TUTORIAL_BLUE_DOT_TITLE:I = 0x7f0c0282

.field public static final TUTORIAL_GOT_IT:I = 0x7f0c027c

.field public static final TUTORIAL_PULL_UP:I = 0x7f0c0279

.field public static final TUTORIAL_PULL_UP_HINT:I = 0x7f0c027e

.field public static final TUTORIAL_PULL_UP_TITLE:I = 0x7f0c027d

.field public static final TUTORIAL_SEARCH_RESULTS:I = 0x7f0c027b

.field public static final TUTORIAL_SIDE_MENU_HINT:I = 0x7f0c0280

.field public static final TUTORIAL_SIDE_MENU_TITLE:I = 0x7f0c027f

.field public static final TUTORIAL_SWIPE:I = 0x7f0c027a

.field public static final TUTORIAL_SWIPE_TITLE:I = 0x7f0c0281

.field public static final TUTORIAL_TAP_HERE:I = 0x7f0c0278

.field public static final TYPE_CORRECT_PHONE:I = 0x7f0c01e7

.field public static final UNITS:I = 0x7f0c025c

.field public static final UNITS_AUTOMATIC:I = 0x7f0c025d

.field public static final UNITS_IMPERIAL:I = 0x7f0c025f

.field public static final UNITS_METRIC:I = 0x7f0c025e

.field public static final UNKNOWN_ERROR:I = 0x7f0c00dd

.field public static final UPDATE_LOCATION_ON_MAP:I = 0x7f0c01c4

.field public static final UPDATE_PLAY_SERVICES_FOR_LOCATION_MESSAGE:I = 0x7f0c0383

.field public static final UPDATE_PLAY_SERVICES_FOR_LOCATION_TITLE:I = 0x7f0c0382

.field public static final UPDATE_PLAY_SERVICES_GENERIC_MESSAGE:I = 0x7f0c0387

.field public static final UPLOAD_OWN_PHOTO:I = 0x7f0c011c

.field public static final UPLOAD_PANORAMA_DISCLAIMER:I = 0x7f0c0117

.field public static final UPLOAD_PHOTO:I = 0x7f0c0110

.field public static final UPLOAD_PHOTO_DISCLAIMER:I = 0x7f0c0115

.field public static final USER_LOCATION_REPORTING_TEXT:I = 0x7f0c0273

.field public static final USE_POWER_SAVINGS_ACCEPT:I = 0x7f0c0299

.field public static final USE_POWER_SAVINGS_DECLINE:I = 0x7f0c029a

.field public static final USE_POWER_SAVINGS_MESSAGE:I = 0x7f0c0298

.field public static final USE_POWER_SAVINGS_TITLE:I = 0x7f0c0297

.field public static final VERSION:I = 0x7f0c0262

.field public static final VIA_HEAVY_TRAFFIC_ROADS:I = 0x7f0c0163

.field public static final VIA_HEAVY_TRAFFIC_ROADS_IN_DRIVING:I = 0x7f0c0164

.field public static final VIA_LIGHT_TRAFFIC_ROADS:I = 0x7f0c0167

.field public static final VIA_LIGHT_TRAFFIC_ROADS_IN_DRIVING:I = 0x7f0c0168

.field public static final VIA_NORMAL_TRAFFIC_ROADS:I = 0x7f0c0165

.field public static final VIA_NORMAL_TRAFFIC_ROADS_IN_DRIVING:I = 0x7f0c0166

.field public static final VIA_ROADS:I = 0x7f0c0160

.field public static final VIA_ROADS_CLAUSE:I = 0x7f0c0162

.field public static final VIA_ROADS_IN_DRIVING:I = 0x7f0c0161

.field public static final VOICE_ACTION_CANNOT_HANDLE:I = 0x7f0c02fe

.field public static final VOICE_ACTION_CANNOT_HANDLE_WITH_QUERY:I = 0x7f0c02ff

.field public static final VOICE_ACTION_DIRECTIONS_TO:I = 0x7f0c0300

.field public static final VOICE_ACTION_ETA:I = 0x7f0c0308

.field public static final VOICE_ACTION_EXAMPLE_SPECIFIC_PLACE:I = 0x7f0c0303

.field public static final VOICE_ACTION_HOW_TO:I = 0x7f0c0301

.field public static final VOICE_ACTION_MUTE:I = 0x7f0c0307

.field public static final VOICE_ACTION_NAVIGATE_TO:I = 0x7f0c0302

.field public static final VOICE_ACTION_QUERY_NEXT_TURN:I = 0x7f0c0304

.field public static final VOICE_ACTION_ROUTE_OVERVIEW:I = 0x7f0c030a

.field public static final VOICE_ACTION_SHOW_ALTERNATES:I = 0x7f0c0306

.field public static final VOICE_ACTION_SHOW_TRAFFIC:I = 0x7f0c0305

.field public static final VOICE_ACTION_TRAFFIC_REPORT:I = 0x7f0c0309

.field public static final VOICE_LEVEL_LOUDER:I = 0x7f0c00bb

.field public static final VOICE_LEVEL_NORMAL:I = 0x7f0c00bc

.field public static final VOICE_LEVEL_SOFTER:I = 0x7f0c00bd

.field public static final VOICE_LEVEL_TITLE:I = 0x7f0c00ba

.field public static final VOICE_SEARCH:I = 0x7f0c00a3

.field public static final WALKING_DURATION:I = 0x7f0c018c

.field public static final WALKING_TRAVEL_MODE_LABEL:I = 0x7f0c018a

.field public static final WAZE_ATTRIBUTION_DIALOG_CONTENT_ALREADY_INSTALLED:I = 0x7f0c0491

.field public static final WAZE_ATTRIBUTION_DIALOG_CONTENT_NOT_INSTALLED:I = 0x7f0c0490

.field public static final WAZE_ATTRIBUTION_DIALOG_DISMISS:I = 0x7f0c0492

.field public static final WAZE_ATTRIBUTION_DIALOG_GET_WAZE:I = 0x7f0c0493

.field public static final WAZE_ATTRIBUTION_DIALOG_OPEN_WAZE:I = 0x7f0c0494

.field public static final WAZE_ATTRIBUTION_DIALOG_TITLE:I = 0x7f0c048f

.field public static final WAZE_NOT_AVAILABLE:I = 0x7f0c00c0

.field public static final WEB_HISTORY:I = 0x7f0c0267

.field public static final WEDNESDAY:I = 0x7f0c00f1

.field public static final WELCOME_TO_GOOGLE_MAPS:I = 0x7f0c0268

.field public static final WHY_THIS_AD:I = 0x7f0c041d

.field public static final WORK_LOCATION:I = 0x7f0c0198

.field public static final WRITE_REVIEW_HINT:I = 0x7f0c034e

.field public static final WRITE_REVIEW_HINT_WITH_PLACE_NAME:I = 0x7f0c034f

.field public static final YESTERDAY:I = 0x7f0c00c1

.field public static final YES_BUTTON:I = 0x7f0c00c5

.field public static final YES_IM_IN:I = 0x7f0c0274

.field public static final YOU:I = 0x7f0c0373

.field public static final YOUR_CHANGES_MAY_BE_PUBLISHED:I = 0x7f0c01ce

.field public static final YOUR_CHANGES_MAY_BE_PUBLISHED_ANONYMOUSLY:I = 0x7f0c01cf

.field public static final YOUR_PLACES:I = 0x7f0c00a4

.field public static final YOUR_REVIEW:I = 0x7f0c0374

.field public static final YOUR_REVIEW_ADD_TEXT:I = 0x7f0c0375

.field public static final YOU_WILL_RECEIVE_UPDATES_VIA_EMAIL:I = 0x7f0c01cc

.field public static final YOU_WILL_RECEIVE_UPDATES_VIA_EMAIL_WITH_ADDRESS:I = 0x7f0c01cd

.field public static final ZAGAT_EDITORIAL_HEADER:I = 0x7f0c036f

.field public static final ZAGAT_FLOATING_BAR_TITLE:I = 0x7f0c036e

.field public static final ZAGAT_LISTS_HEADER:I = 0x7f0c0370

.field public static final ZAGAT_LIST_MORE:I = 0x7f0c036b

.field public static final ZAGAT_LIST_TITLE:I = 0x7f0c0369

.field public static final ZAGAT_LIST_VIEW_MORE:I = 0x7f0c036a

.field public static final abc_action_bar_home_description:I = 0x7f0c0075

.field public static final abc_action_bar_home_description_format:I = 0x7f0c0079

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0c007a

.field public static final abc_action_bar_up_description:I = 0x7f0c0076

.field public static final abc_action_menu_overflow_description:I = 0x7f0c0077

.field public static final abc_action_mode_done:I = 0x7f0c0074

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0c0081

.field public static final abc_activitychooserview_choose_application:I = 0x7f0c0080

.field public static final abc_searchview_description_clear:I = 0x7f0c007d

.field public static final abc_searchview_description_query:I = 0x7f0c007c

.field public static final abc_searchview_description_search:I = 0x7f0c007b

.field public static final abc_searchview_description_submit:I = 0x7f0c007e

.field public static final abc_searchview_description_voice:I = 0x7f0c007f

.field public static final abc_shareactionprovider_share_with:I = 0x7f0c0083

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0c0082

.field public static final abc_toolbar_collapse_description:I = 0x7f0c0078

.field public static final accept:I = 0x7f0c0004

.field public static final account_item:I = 0x7f0c0089

.field public static final account_list:I = 0x7f0c008b

.field public static final add_account:I = 0x7f0c0084

.field public static final application:I = 0x7f0c0025

.field public static final car_clock_24_hours_format:I = 0x7f0c0000

.field public static final car_world_clock_12_hours_format:I = 0x7f0c0001

.field public static final common_android_wear_notification_needs_update_text:I = 0x7f0c000b

.field public static final common_android_wear_update_text:I = 0x7f0c0018

.field public static final common_android_wear_update_title:I = 0x7f0c0016

.field public static final common_google_play_services_enable_button:I = 0x7f0c0014

.field public static final common_google_play_services_enable_text:I = 0x7f0c0013

.field public static final common_google_play_services_enable_title:I = 0x7f0c0012

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f0c000d

.field public static final common_google_play_services_install_button:I = 0x7f0c0011

.field public static final common_google_play_services_install_text_phone:I = 0x7f0c000f

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0c0010

.field public static final common_google_play_services_install_title:I = 0x7f0c000e

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0c001c

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0c001b

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f0c000c

.field public static final common_google_play_services_network_error_text:I = 0x7f0c001a

.field public static final common_google_play_services_network_error_title:I = 0x7f0c0019

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f0c0009

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f0c000a

.field public static final common_google_play_services_notification_ticker:I = 0x7f0c0008

.field public static final common_google_play_services_unknown_issue:I = 0x7f0c001d

.field public static final common_google_play_services_unsupported_text:I = 0x7f0c001f

.field public static final common_google_play_services_unsupported_title:I = 0x7f0c001e

.field public static final common_google_play_services_update_button:I = 0x7f0c0020

.field public static final common_google_play_services_update_text:I = 0x7f0c0017

.field public static final common_google_play_services_update_title:I = 0x7f0c0015

.field public static final common_open_on_phone:I = 0x7f0c0023

.field public static final common_signin_button_text:I = 0x7f0c0021

.field public static final common_signin_button_text_long:I = 0x7f0c0022

.field public static final create_calendar_message:I = 0x7f0c0007

.field public static final create_calendar_title:I = 0x7f0c0006

.field public static final decline:I = 0x7f0c0005

.field public static final gf_anonymous:I = 0x7f0c002c

.field public static final gf_app_name:I = 0x7f0c0028

.field public static final gf_back:I = 0x7f0c0029

.field public static final gf_build_view:I = 0x7f0c0042

.field public static final gf_choose_an_account:I = 0x7f0c002d

.field public static final gf_crash_header:I = 0x7f0c0062

.field public static final gf_error_report_board:I = 0x7f0c0056

.field public static final gf_error_report_brand:I = 0x7f0c0057

.field public static final gf_error_report_build_id:I = 0x7f0c004e

.field public static final gf_error_report_build_type:I = 0x7f0c004f

.field public static final gf_error_report_codename:I = 0x7f0c0055

.field public static final gf_error_report_description:I = 0x7f0c004c

.field public static final gf_error_report_device:I = 0x7f0c004d

.field public static final gf_error_report_incremental:I = 0x7f0c0054

.field public static final gf_error_report_installed_packages:I = 0x7f0c0059

.field public static final gf_error_report_installer_package_name:I = 0x7f0c0049

.field public static final gf_error_report_model:I = 0x7f0c0050

.field public static final gf_error_report_package_name:I = 0x7f0c0048

.field public static final gf_error_report_package_version:I = 0x7f0c0046

.field public static final gf_error_report_package_version_name:I = 0x7f0c0047

.field public static final gf_error_report_process_name:I = 0x7f0c004a

.field public static final gf_error_report_product:I = 0x7f0c0051

.field public static final gf_error_report_release:I = 0x7f0c0053

.field public static final gf_error_report_running_apps:I = 0x7f0c005a

.field public static final gf_error_report_running_service_details:I = 0x7f0c005d

.field public static final gf_error_report_sdk_version:I = 0x7f0c0052

.field public static final gf_error_report_system:I = 0x7f0c0045

.field public static final gf_error_report_system_app:I = 0x7f0c005c

.field public static final gf_error_report_system_log:I = 0x7f0c005b

.field public static final gf_error_report_time:I = 0x7f0c004b

.field public static final gf_error_report_user_accounts:I = 0x7f0c0058

.field public static final gf_exception_class_name:I = 0x7f0c0064

.field public static final gf_exception_message:I = 0x7f0c0069

.field public static final gf_feedback:I = 0x7f0c002a

.field public static final gf_include_screenshot:I = 0x7f0c0036

.field public static final gf_include_system_data:I = 0x7f0c0035

.field public static final gf_network_data:I = 0x7f0c0043

.field public static final gf_network_name:I = 0x7f0c0044

.field public static final gf_no:I = 0x7f0c0061

.field public static final gf_no_data:I = 0x7f0c0033

.field public static final gf_optional_description:I = 0x7f0c003d

.field public static final gf_preview:I = 0x7f0c0030

.field public static final gf_preview_feedback:I = 0x7f0c0032

.field public static final gf_privacy:I = 0x7f0c0034

.field public static final gf_privacy_policy:I = 0x7f0c0040

.field public static final gf_privacy_text:I = 0x7f0c0041

.field public static final gf_receiver_host:I = 0x7f0c006b

.field public static final gf_receiver_path:I = 0x7f0c006d

.field public static final gf_receiver_port:I = 0x7f0c006c

.field public static final gf_receiver_transport_scheme:I = 0x7f0c006a

.field public static final gf_report_being_sent:I = 0x7f0c003a

.field public static final gf_report_feedback:I = 0x7f0c002e

.field public static final gf_report_queued:I = 0x7f0c003c

.field public static final gf_report_sent_failure:I = 0x7f0c003b

.field public static final gf_screenshot_preview:I = 0x7f0c002f

.field public static final gf_send:I = 0x7f0c0031

.field public static final gf_should_submit_anonymously:I = 0x7f0c005f

.field public static final gf_should_submit_on_empty_description:I = 0x7f0c005e

.field public static final gf_stack_trace:I = 0x7f0c0063

.field public static final gf_sys_logs:I = 0x7f0c003f

.field public static final gf_system_log:I = 0x7f0c003e

.field public static final gf_this_will_help:I = 0x7f0c0037

.field public static final gf_this_will_help_screenshot:I = 0x7f0c0038

.field public static final gf_throw_class_name:I = 0x7f0c0067

.field public static final gf_throw_file_name:I = 0x7f0c0065

.field public static final gf_throw_line_number:I = 0x7f0c0066

.field public static final gf_throw_method_name:I = 0x7f0c0068

.field public static final gf_unknown_app:I = 0x7f0c0039

.field public static final gf_user_account:I = 0x7f0c002b

.field public static final gf_yes:I = 0x7f0c0060

.field public static final hide_account_list:I = 0x7f0c0088

.field public static final manage_accounts:I = 0x7f0c0085

.field public static final mr_media_route_button_content_description:I = 0x7f0c0070

.field public static final mr_media_route_chooser_searching:I = 0x7f0c0072

.field public static final mr_media_route_chooser_title:I = 0x7f0c0071

.field public static final mr_media_route_controller_disconnect:I = 0x7f0c0073

.field public static final mr_system_route_name:I = 0x7f0c006e

.field public static final mr_user_route_category_name:I = 0x7f0c006f

.field public static final reinstall_prompt_dialog_message:I = 0x7f0c0027

.field public static final reinstall_prompt_dialog_title:I = 0x7f0c0026

.field public static final selected_account:I = 0x7f0c008a

.field public static final show_account_list:I = 0x7f0c0087

.field public static final sign_in:I = 0x7f0c0086

.field public static final store_picture_message:I = 0x7f0c0003

.field public static final store_picture_title:I = 0x7f0c0002

.field public static final wallet_buy_button_place_holder:I = 0x7f0c0024


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
