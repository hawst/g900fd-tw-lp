.class public Lcom/google/userfeedback/android/api/a/a/h;
.super Ljava/io/OutputStream;
.source "PG"


# instance fields
.field public a:[B

.field public b:[I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 30
    iput v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    .line 35
    iput v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->d:I

    .line 38
    iput v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    iput v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->d:I

    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    .line 39
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 123
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    array-length v1, v0

    iget v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    sub-int v0, v1, v0

    :goto_0
    if-ge v0, p1, :cond_0

    add-int/2addr v0, v1

    shl-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    array-length v0, v0

    if-le v1, v0, :cond_1

    .line 125
    new-array v0, v1, [B

    .line 126
    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    iget v2, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    .line 129
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 105
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    array-length v1, v0

    iget v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->d:I

    sub-int v0, v1, v0

    :goto_0
    if-ge v0, v2, :cond_0

    add-int/2addr v0, v1

    shl-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    array-length v0, v0

    if-le v1, v0, :cond_1

    new-array v0, v1, [I

    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    iget v2, p0, Lcom/google/userfeedback/android/api/a/a/h;->d:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->b:[I

    iget v1, p0, Lcom/google/userfeedback/android/api/a/a/h;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/userfeedback/android/api/a/a/h;->d:I

    aput p1, v0, v1

    .line 107
    return-void
.end method

.method public write(I)V
    .locals 3

    .prologue
    .line 140
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/userfeedback/android/api/a/a/h;->b(I)V

    .line 141
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    iget v1, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 142
    return-void
.end method

.method public write([B)V
    .locals 4

    .prologue
    .line 145
    array-length v0, p1

    invoke-direct {p0, v0}, Lcom/google/userfeedback/android/api/a/a/h;->b(I)V

    .line 146
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    iget v2, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    iget v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    .line 148
    return-void
.end method

.method public write([BII)V
    .locals 2

    .prologue
    .line 151
    invoke-direct {p0, p3}, Lcom/google/userfeedback/android/api/a/a/h;->b(I)V

    .line 152
    iget-object v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->a:[B

    iget v1, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    iget v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/userfeedback/android/api/a/a/h;->c:I

    .line 154
    return-void
.end method
