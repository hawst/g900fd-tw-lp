.class public final Lcom/google/userfeedback/android/api/R$raw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final com_google_android_apps_maps_wearable:I = 0x7f090000

.field public static final compass_calibration:I = 0x7f090001

.field public static final da_act:I = 0x7f090002

.field public static final da_error:I = 0x7f090003

.field public static final da_lane_normal:I = 0x7f090004

.field public static final da_lane_normal_short:I = 0x7f090005

.field public static final da_lane_sharp:I = 0x7f090006

.field public static final da_lane_sharp_short:I = 0x7f090007

.field public static final da_lane_slight:I = 0x7f090008

.field public static final da_lane_slight_tall:I = 0x7f090009

.field public static final da_lane_straight:I = 0x7f09000a

.field public static final da_lane_straight_tall:I = 0x7f09000b

.field public static final da_lane_stub:I = 0x7f09000c

.field public static final da_lane_uturn:I = 0x7f09000d

.field public static final da_lane_uturn_short:I = 0x7f09000e

.field public static final da_laneguidance_tick:I = 0x7f09000f

.field public static final da_menu_alternates:I = 0x7f090010

.field public static final da_menu_list:I = 0x7f090011

.field public static final da_menu_mute:I = 0x7f090012

.field public static final da_menu_satellite:I = 0x7f090013

.field public static final da_menu_traffic:I = 0x7f090014

.field public static final da_menu_unmute:I = 0x7f090015

.field public static final da_prepare:I = 0x7f090016

.field public static final da_turn_arrive:I = 0x7f090017

.field public static final da_turn_arrive_right:I = 0x7f090018

.field public static final da_turn_depart:I = 0x7f090019

.field public static final da_turn_ferry:I = 0x7f09001a

.field public static final da_turn_fork_right:I = 0x7f09001b

.field public static final da_turn_generic_merge:I = 0x7f09001c

.field public static final da_turn_generic_roundabout:I = 0x7f09001d

.field public static final da_turn_mylocation:I = 0x7f09001e

.field public static final da_turn_ramp_right:I = 0x7f09001f

.field public static final da_turn_right:I = 0x7f090020

.field public static final da_turn_roundabout_1:I = 0x7f090021

.field public static final da_turn_roundabout_2:I = 0x7f090022

.field public static final da_turn_roundabout_3:I = 0x7f090023

.field public static final da_turn_roundabout_4:I = 0x7f090024

.field public static final da_turn_roundabout_5:I = 0x7f090025

.field public static final da_turn_roundabout_6:I = 0x7f090026

.field public static final da_turn_roundabout_7:I = 0x7f090027

.field public static final da_turn_roundabout_8:I = 0x7f090028

.field public static final da_turn_roundabout_exit:I = 0x7f090029

.field public static final da_turn_sharp_right:I = 0x7f09002a

.field public static final da_turn_slight_right:I = 0x7f09002b

.field public static final da_turn_straight:I = 0x7f09002c

.field public static final da_turn_unknown:I = 0x7f09002d

.field public static final da_turn_uturn:I = 0x7f09002e

.field public static final dav_k2:I = 0x7f09002f

.field public static final elevation_down_arrow:I = 0x7f090030

.field public static final elevation_up_arrow:I = 0x7f090031

.field public static final gtm_analytics:I = 0x7f090032

.field public static final ic_mic:I = 0x7f090033

.field public static final ic_navchevron:I = 0x7f090034

.field public static final qu_da_menu_alternates:I = 0x7f090035

.field public static final qu_da_menu_mute:I = 0x7f090036

.field public static final qu_da_menu_satellite:I = 0x7f090037

.field public static final qu_da_menu_traffic:I = 0x7f090038

.field public static final qu_da_menu_unmute:I = 0x7f090039

.field public static final qu_da_nextarrow_left:I = 0x7f09003a

.field public static final qu_da_nextarrow_right:I = 0x7f09003b

.field public static final robot:I = 0x7f09003c

.field public static final roboto2draft_bold:I = 0x7f09003d

.field public static final roboto2draft_italic:I = 0x7f09003e

.field public static final roboto2draft_light:I = 0x7f09003f

.field public static final roboto2draft_medium:I = 0x7f090040

.field public static final roboto2draft_regular:I = 0x7f090041

.field public static final roboto2draft_thin:I = 0x7f090042


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
