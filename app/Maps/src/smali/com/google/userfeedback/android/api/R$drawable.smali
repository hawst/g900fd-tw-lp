.class public final Lcom/google/userfeedback/android/api/R$drawable;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final abc_ab_share_pack_holo_dark:I = 0x7f020000

.field public static final abc_ab_share_pack_holo_light:I = 0x7f020001

.field public static final abc_btn_check_material:I = 0x7f020002

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f020003

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f020004

.field public static final abc_btn_radio_material:I = 0x7f020005

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f020006

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f020007

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f020008

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f020009

.field public static final abc_cab_background_internal_bg:I = 0x7f02000a

.field public static final abc_cab_background_top_material:I = 0x7f02000b

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f02000c

.field public static final abc_edit_text_material:I = 0x7f02000d

.field public static final abc_ic_ab_back_mtrl_am_alpha:I = 0x7f02000e

.field public static final abc_ic_clear_mtrl_alpha:I = 0x7f02000f

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f020010

.field public static final abc_ic_go_search_api_mtrl_alpha:I = 0x7f020011

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f020012

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f020013

.field public static final abc_ic_menu_moreoverflow_mtrl_alpha:I = 0x7f020014

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f020015

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f020016

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f020017

.field public static final abc_ic_search_api_mtrl_alpha:I = 0x7f020018

.field public static final abc_ic_voice_search_api_mtrl_alpha:I = 0x7f020019

.field public static final abc_item_background_holo_dark:I = 0x7f02001a

.field public static final abc_item_background_holo_light:I = 0x7f02001b

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f02001c

.field public static final abc_list_focused_holo:I = 0x7f02001d

.field public static final abc_list_longpressed_holo:I = 0x7f02001e

.field public static final abc_list_pressed_holo_dark:I = 0x7f02001f

.field public static final abc_list_pressed_holo_light:I = 0x7f020020

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f020021

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f020022

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f020023

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020024

.field public static final abc_list_selector_holo_dark:I = 0x7f020025

.field public static final abc_list_selector_holo_light:I = 0x7f020026

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f020027

.field public static final abc_popup_background_mtrl_mult:I = 0x7f020028

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f020029

.field public static final abc_switch_thumb_material:I = 0x7f02002a

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f02002b

.field public static final abc_tab_indicator_material:I = 0x7f02002c

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f02002d

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f02002e

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f02002f

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f020030

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f020031

.field public static final abc_textfield_search_material:I = 0x7f020032

.field public static final account_switcher_blue:I = 0x7f020033

.field public static final actionbar:I = 0x7f020034

.field public static final ad_badge_background:I = 0x7f020035

.field public static final addaplace_overlay:I = 0x7f020036

.field public static final addreview_illustration:I = 0x7f020037

.field public static final ads_info:I = 0x7f020038

.field public static final appwidget_modebicycle_button:I = 0x7f020039

.field public static final appwidget_modedrive_button:I = 0x7f02003a

.field public static final appwidget_modetransit_button:I = 0x7f02003b

.field public static final appwidget_modewalk_button:I = 0x7f02003c

.field public static final avatar_placeholder:I = 0x7f02003d

.field public static final back_button_dark:I = 0x7f02003e

.field public static final back_button_light:I = 0x7f02003f

.field public static final blue_dot:I = 0x7f020040

.field public static final blue_dot_glow:I = 0x7f020041

.field public static final breadcrumb_green_dot:I = 0x7f020042

.field public static final breadcrumb_green_pointer:I = 0x7f020043

.field public static final breadcrumb_orange_dot:I = 0x7f020044

.field public static final breadcrumb_orange_pointer:I = 0x7f020045

.field public static final breadcrumb_red_dot:I = 0x7f020046

.field public static final breadcrumb_red_pointer:I = 0x7f020047

.field public static final button:I = 0x7f020048

.field public static final button_compass:I = 0x7f020049

.field public static final button_compass_highlighted:I = 0x7f02004a

.field public static final button_compass_night:I = 0x7f02004b

.field public static final button_compass_night_highlighted:I = 0x7f02004c

.field public static final button_compass_night_selector:I = 0x7f02004d

.field public static final button_compass_selector:I = 0x7f02004e

.field public static final button_following:I = 0x7f02004f

.field public static final button_highlighted:I = 0x7f020050

.field public static final button_mylocation_selector:I = 0x7f020051

.field public static final button_mylocationtracking_selector:I = 0x7f020052

.field public static final button_selector:I = 0x7f020053

.field public static final calendar_badge:I = 0x7f020054

.field public static final calibrate_compass_dialog_horizontal_bar:I = 0x7f020055

.field public static final callout_corner_black:I = 0x7f020056

.field public static final callout_corner_grey:I = 0x7f020057

.field public static final callout_corner_grey_night:I = 0x7f020058

.field public static final callout_corner_white:I = 0x7f020059

.field public static final callout_side_black:I = 0x7f02005a

.field public static final callout_side_grey:I = 0x7f02005b

.field public static final callout_side_grey_night:I = 0x7f02005c

.field public static final callout_side_white:I = 0x7f02005d

.field public static final car_check_off:I = 0x7f02005e

.field public static final car_check_on:I = 0x7f02005f

.field public static final car_focus_box:I = 0x7f020060

.field public static final car_header_button_background:I = 0x7f020061

.field public static final car_headergradient_dark:I = 0x7f020062

.field public static final car_headergradient_light:I = 0x7f020063

.field public static final car_ic_alternateroutes:I = 0x7f020064

.field public static final car_ic_arrow_left_light:I = 0x7f020065

.field public static final car_ic_arrow_left_light_normal:I = 0x7f020066

.field public static final car_ic_arrow_left_light_pressed:I = 0x7f020067

.field public static final car_ic_arrow_right:I = 0x7f020068

.field public static final car_ic_arrow_right_light:I = 0x7f020069

.field public static final car_ic_arrow_right_light_normal:I = 0x7f02006a

.field public static final car_ic_arrow_right_light_pressed:I = 0x7f02006b

.field public static final car_ic_category_atm:I = 0x7f02006c

.field public static final car_ic_category_coffee:I = 0x7f02006d

.field public static final car_ic_category_fastfood:I = 0x7f02006e

.field public static final car_ic_category_gas:I = 0x7f02006f

.field public static final car_ic_category_hospital:I = 0x7f020070

.field public static final car_ic_category_parking:I = 0x7f020071

.field public static final car_ic_category_restaurants:I = 0x7f020072

.field public static final car_ic_category_shopping:I = 0x7f020073

.field public static final car_ic_checkmark:I = 0x7f020074

.field public static final car_ic_close:I = 0x7f020075

.field public static final car_ic_crosshair:I = 0x7f020076

.field public static final car_ic_error:I = 0x7f020077

.field public static final car_ic_ferry:I = 0x7f020078

.field public static final car_ic_mylocation:I = 0x7f020079

.field public static final car_ic_nav_settings:I = 0x7f02007a

.field public static final car_ic_navigate:I = 0x7f02007b

.field public static final car_ic_options:I = 0x7f02007c

.field public static final car_ic_options_circle:I = 0x7f02007d

.field public static final car_ic_pan:I = 0x7f02007e

.field public static final car_ic_pan_axis:I = 0x7f02007f

.field public static final car_ic_pan_axis_normal:I = 0x7f020080

.field public static final car_ic_pan_axis_pressed:I = 0x7f020081

.field public static final car_ic_phone:I = 0x7f020082

.field public static final car_ic_pin:I = 0x7f020083

.field public static final car_ic_pin_error:I = 0x7f020084

.field public static final car_ic_refresh:I = 0x7f020085

.field public static final car_ic_search:I = 0x7f020086

.field public static final car_ic_sound_off:I = 0x7f020087

.field public static final car_ic_sound_on:I = 0x7f020088

.field public static final car_ic_toll:I = 0x7f020089

.field public static final car_ic_zoom_in:I = 0x7f02008a

.field public static final car_ic_zoom_out:I = 0x7f02008b

.field public static final car_list_item_background:I = 0x7f02008c

.field public static final car_pagination_background:I = 0x7f02008d

.field public static final car_pagination_background_dark:I = 0x7f02008e

.field public static final car_pagination_background_light:I = 0x7f02008f

.field public static final car_pin:I = 0x7f020090

.field public static final car_search_measle_large:I = 0x7f020091

.field public static final car_star_blank:I = 0x7f020092

.field public static final car_star_filled:I = 0x7f020093

.field public static final car_star_half:I = 0x7f020094

.field public static final car_startpoint_measle:I = 0x7f020095

.field public static final car_toast_background:I = 0x7f020096

.field public static final car_transitpoint_measle:I = 0x7f020097

.field public static final card_bottom_selector:I = 0x7f020098

.field public static final card_middle_selector:I = 0x7f020099

.field public static final card_selector:I = 0x7f02009a

.field public static final card_top_selector:I = 0x7f02009b

.field public static final cardlist_divider:I = 0x7f02009c

.field public static final cardlist_middle_padding:I = 0x7f02009d

.field public static final cardui_vertical_divider:I = 0x7f02009e

.field public static final chevron_navigation_chevron:I = 0x7f02009f

.field public static final chevron_navigation_chevron_night:I = 0x7f0200a0

.field public static final chevron_navigation_disc:I = 0x7f0200a1

.field public static final chevron_navigation_disc_night:I = 0x7f0200a2

.field public static final common_full_open_on_phone:I = 0x7f0200a3

.field public static final common_ic_googleplayservices:I = 0x7f0200a4

.field public static final common_signin_btn_icon_dark:I = 0x7f0200a5

.field public static final common_signin_btn_icon_disabled_dark:I = 0x7f0200a6

.field public static final common_signin_btn_icon_disabled_focus_dark:I = 0x7f0200a7

.field public static final common_signin_btn_icon_disabled_focus_light:I = 0x7f0200a8

.field public static final common_signin_btn_icon_disabled_light:I = 0x7f0200a9

.field public static final common_signin_btn_icon_focus_dark:I = 0x7f0200aa

.field public static final common_signin_btn_icon_focus_light:I = 0x7f0200ab

.field public static final common_signin_btn_icon_light:I = 0x7f0200ac

.field public static final common_signin_btn_icon_normal_dark:I = 0x7f0200ad

.field public static final common_signin_btn_icon_normal_light:I = 0x7f0200ae

.field public static final common_signin_btn_icon_pressed_dark:I = 0x7f0200af

.field public static final common_signin_btn_icon_pressed_light:I = 0x7f0200b0

.field public static final common_signin_btn_text_dark:I = 0x7f0200b1

.field public static final common_signin_btn_text_disabled_dark:I = 0x7f0200b2

.field public static final common_signin_btn_text_disabled_focus_dark:I = 0x7f0200b3

.field public static final common_signin_btn_text_disabled_focus_light:I = 0x7f0200b4

.field public static final common_signin_btn_text_disabled_light:I = 0x7f0200b5

.field public static final common_signin_btn_text_focus_dark:I = 0x7f0200b6

.field public static final common_signin_btn_text_focus_light:I = 0x7f0200b7

.field public static final common_signin_btn_text_light:I = 0x7f0200b8

.field public static final common_signin_btn_text_normal_dark:I = 0x7f0200b9

.field public static final common_signin_btn_text_normal_light:I = 0x7f0200ba

.field public static final common_signin_btn_text_pressed_dark:I = 0x7f0200bb

.field public static final common_signin_btn_text_pressed_light:I = 0x7f0200bc

.field public static final compasssuccess:I = 0x7f0200bd

.field public static final da_generic_exit_left:I = 0x7f0200be

.field public static final da_generic_exit_right:I = 0x7f0200bf

.field public static final da_generic_exit_unknown:I = 0x7f0200c0

.field public static final da_lane_normal:I = 0x7f0200c1

.field public static final da_lane_normal_short:I = 0x7f0200c2

.field public static final da_lane_sharp:I = 0x7f0200c3

.field public static final da_lane_sharp_short:I = 0x7f0200c4

.field public static final da_lane_slight:I = 0x7f0200c5

.field public static final da_lane_slight_tall:I = 0x7f0200c6

.field public static final da_lane_straight:I = 0x7f0200c7

.field public static final da_lane_straight_tall:I = 0x7f0200c8

.field public static final da_lane_stub:I = 0x7f0200c9

.field public static final da_lane_uturn:I = 0x7f0200ca

.field public static final da_lane_uturn_short:I = 0x7f0200cb

.field public static final da_laneguidance_tick:I = 0x7f0200cc

.field public static final da_turn_destination:I = 0x7f0200cd

.field public static final dav_arrow_line_internal:I = 0x7f0200ce

.field public static final dav_one_way_64_1024_internal:I = 0x7f0200cf

.field public static final dav_road_24_64_internal:I = 0x7f0200d0

.field public static final dialog_button_selector:I = 0x7f0200d1

.field public static final direction_pointer:I = 0x7f0200d2

.field public static final directions_polyline_colors_texture:I = 0x7f0200d3

.field public static final directions_polyline_colors_texture_32x128:I = 0x7f0200d4

.field public static final directions_polyline_colors_texture_dim:I = 0x7f0200d5

.field public static final directions_polyline_colors_texture_dim_32x128:I = 0x7f0200d6

.field public static final directions_transitdetails_time_padding:I = 0x7f0200d7

.field public static final divider_holo_light:I = 0x7f0200d8

.field public static final dot_floorpicker_blue:I = 0x7f0200d9

.field public static final dot_floorpicker_red:I = 0x7f0200da

.field public static final drawer_shadow:I = 0x7f0200db

.field public static final dropdown_arrow:I = 0x7f0200dc

.field public static final elevation_down_arrow:I = 0x7f0200dd

.field public static final elevation_up_arrow:I = 0x7f0200de

.field public static final empty:I = 0x7f0200df

.field public static final empty_internal:I = 0x7f0200e0

.field public static final expander_close_dark:I = 0x7f0200e1

.field public static final expander_group:I = 0x7f0200e2

.field public static final expander_ic_minimized:I = 0x7f0200e3

.field public static final expander_open_dark:I = 0x7f0200e4

.field public static final expanding_scroll_view_shadow:I = 0x7f0200e5

.field public static final floating_bar_button_divider:I = 0x7f0200e6

.field public static final floor_picker:I = 0x7f0200e7

.field public static final floorpicker_bg_selected:I = 0x7f0200e8

.field public static final generic_1dp_transparent_vertical_divider:I = 0x7f0200e9

.field public static final generic_4dp_transparent_vertical_divider:I = 0x7f0200ea

.field public static final generic_above_shadow:I = 0x7f0200eb

.field public static final generic_below_shadow:I = 0x7f0200ec

.field public static final generic_below_shadow_nightmode:I = 0x7f0200ed

.field public static final generic_card_background_highlighted:I = 0x7f0200ee

.field public static final generic_card_background_selector:I = 0x7f0200ef

.field public static final generic_card_bottom_selector:I = 0x7f0200f0

.field public static final generic_card_middle_selector:I = 0x7f0200f1

.field public static final generic_card_selector:I = 0x7f0200f2

.field public static final generic_card_smaller_side_bottom_selector:I = 0x7f0200f3

.field public static final generic_card_smaller_side_middle_selector:I = 0x7f0200f4

.field public static final generic_card_smaller_side_selector:I = 0x7f0200f5

.field public static final generic_card_smaller_side_top_selector:I = 0x7f0200f6

.field public static final generic_card_top_selector:I = 0x7f0200f7

.field public static final generic_horizontalsolid_divider:I = 0x7f0200f8

.field public static final generic_image_placeholder:I = 0x7f0200f9

.field public static final generic_invisible_divider:I = 0x7f0200fa

.field public static final generic_item_selector:I = 0x7f0200fb

.field public static final generic_left_shadow:I = 0x7f0200fc

.field public static final generic_listcard_divider:I = 0x7f0200fd

.field public static final generic_overlay_fade_up:I = 0x7f0200fe

.field public static final generic_paddedlistcard_divider:I = 0x7f0200ff

.field public static final generic_progressbar:I = 0x7f020100

.field public static final generic_right_shadow:I = 0x7f020101

.field public static final generic_sheet_background:I = 0x7f020102

.field public static final generic_sheetheader_background:I = 0x7f020103

.field public static final generic_textbox_divider:I = 0x7f020104

.field public static final generic_transparentitem_selector:I = 0x7f020105

.field public static final generic_vertical_divider:I = 0x7f020106

.field public static final generic_verticallistcard_divider:I = 0x7f020107

.field public static final generic_verticalsolid_divider:I = 0x7f020108

.field public static final gf_expand:I = 0x7f020109

.field public static final gf_icon:I = 0x7f02010a

.field public static final gradient_card_internal:I = 0x7f02010b

.field public static final gray_dot:I = 0x7f02010c

.field public static final gripper_01:I = 0x7f02010d

.field public static final gripper_02:I = 0x7f02010e

.field public static final gripper_03:I = 0x7f02010f

.field public static final gripper_04:I = 0x7f020110

.field public static final guide_no_image_blue:I = 0x7f020111

.field public static final guide_no_image_green:I = 0x7f020112

.field public static final guide_no_image_yellow:I = 0x7f020113

.field public static final highlight_border:I = 0x7f020114

.field public static final histogrambar:I = 0x7f020115

.field public static final ic_add:I = 0x7f020116

.field public static final ic_add_another_photo:I = 0x7f020117

.field public static final ic_airplane:I = 0x7f020118

.field public static final ic_blank_replacement:I = 0x7f020119

.field public static final ic_bluedot_disambig:I = 0x7f02011a

.field public static final ic_bluedot_placepage:I = 0x7f02011b

.field public static final ic_calibratecompass:I = 0x7f02011c

.field public static final ic_call:I = 0x7f02011d

.field public static final ic_compass_mode:I = 0x7f02011e

.field public static final ic_compass_needle:I = 0x7f02011f

.field public static final ic_compass_north:I = 0x7f020120

.field public static final ic_compass_north_night:I = 0x7f020121

.field public static final ic_directions_bicycling_bk:I = 0x7f020122

.field public static final ic_directions_car_bk:I = 0x7f020123

.field public static final ic_directions_form_destination:I = 0x7f020124

.field public static final ic_directions_form_destination_notselected:I = 0x7f020125

.field public static final ic_directions_form_dots:I = 0x7f020126

.field public static final ic_directions_form_mylocation:I = 0x7f020127

.field public static final ic_directions_form_startpoint:I = 0x7f020128

.field public static final ic_directions_form_swap:I = 0x7f020129

.field public static final ic_directions_walking_bk:I = 0x7f02012a

.field public static final ic_dogfood_car:I = 0x7f02012b

.field public static final ic_download_small:I = 0x7f02012c

.field public static final ic_edit:I = 0x7f02012d

.field public static final ic_edit_action:I = 0x7f02012e

.field public static final ic_email:I = 0x7f02012f

.field public static final ic_error_large:I = 0x7f020130

.field public static final ic_error_small:I = 0x7f020131

.field public static final ic_explore:I = 0x7f020132

.field public static final ic_explore_active:I = 0x7f020133

.field public static final ic_filter_blue:I = 0x7f020134

.field public static final ic_fishfood_car:I = 0x7f020135

.field public static final ic_flake:I = 0x7f020136

.field public static final ic_guide:I = 0x7f020137

.field public static final ic_guide_blue:I = 0x7f020138

.field public static final ic_hats_close_btn:I = 0x7f020139

.field public static final ic_intent_drink_black:I = 0x7f02013a

.field public static final ic_intent_drink_black_selector:I = 0x7f02013b

.field public static final ic_intent_drink_white:I = 0x7f02013c

.field public static final ic_intent_drink_white_selector:I = 0x7f02013d

.field public static final ic_intent_eat_black:I = 0x7f02013e

.field public static final ic_intent_eat_black_selector:I = 0x7f02013f

.field public static final ic_intent_eat_white:I = 0x7f020140

.field public static final ic_intent_eat_white_selector:I = 0x7f020141

.field public static final ic_intent_more_black:I = 0x7f020142

.field public static final ic_intent_more_black_selector:I = 0x7f020143

.field public static final ic_intent_more_white:I = 0x7f020144

.field public static final ic_intent_more_white_selector:I = 0x7f020145

.field public static final ic_intent_offer_black:I = 0x7f020146

.field public static final ic_intent_offer_white:I = 0x7f020147

.field public static final ic_intent_play_black:I = 0x7f020148

.field public static final ic_intent_play_black_selector:I = 0x7f020149

.field public static final ic_intent_play_white:I = 0x7f02014a

.field public static final ic_intent_play_white_selector:I = 0x7f02014b

.field public static final ic_intent_shop_black:I = 0x7f02014c

.field public static final ic_intent_shop_black_selector:I = 0x7f02014d

.field public static final ic_intent_shop_white:I = 0x7f02014e

.field public static final ic_intent_shop_white_selector:I = 0x7f02014f

.field public static final ic_intent_sleep_black:I = 0x7f020150

.field public static final ic_intent_sleep_black_selector:I = 0x7f020151

.field public static final ic_intent_sleep_white:I = 0x7f020152

.field public static final ic_intent_sleep_white_selector:I = 0x7f020153

.field public static final ic_launcher_bicycling:I = 0x7f020154

.field public static final ic_launcher_driving:I = 0x7f020155

.field public static final ic_launcher_transit:I = 0x7f020156

.field public static final ic_launcher_walking:I = 0x7f020157

.field public static final ic_layers_bicycling:I = 0x7f020158

.field public static final ic_layers_bicycling_selected:I = 0x7f020159

.field public static final ic_layers_bicycling_selector:I = 0x7f02015a

.field public static final ic_layers_google_earth:I = 0x7f02015b

.field public static final ic_layers_google_earth_shortcut:I = 0x7f02015c

.field public static final ic_layers_places:I = 0x7f02015d

.field public static final ic_layers_places_selected:I = 0x7f02015e

.field public static final ic_layers_satellite:I = 0x7f02015f

.field public static final ic_layers_satellite_selected:I = 0x7f020160

.field public static final ic_layers_satellite_selector:I = 0x7f020161

.field public static final ic_layers_terrain:I = 0x7f020162

.field public static final ic_layers_terrain_selected:I = 0x7f020163

.field public static final ic_layers_terrain_selector:I = 0x7f020164

.field public static final ic_layers_traffic:I = 0x7f020165

.field public static final ic_layers_traffic_selected:I = 0x7f020166

.field public static final ic_layers_traffic_selector:I = 0x7f020167

.field public static final ic_layers_transit:I = 0x7f020168

.field public static final ic_layers_transit_selected:I = 0x7f020169

.field public static final ic_layers_transit_selector:I = 0x7f02016a

.field public static final ic_learn_more:I = 0x7f02016b

.field public static final ic_local_search_more:I = 0x7f02016c

.field public static final ic_location:I = 0x7f02016d

.field public static final ic_location_active:I = 0x7f02016e

.field public static final ic_mail:I = 0x7f02016f

.field public static final ic_menu:I = 0x7f020170

.field public static final ic_mirth:I = 0x7f020171

.field public static final ic_navigation_cancel:I = 0x7f020172

.field public static final ic_offline:I = 0x7f020173

.field public static final ic_omni_box_bicycling:I = 0x7f020174

.field public static final ic_omni_box_bicycling_selector:I = 0x7f020175

.field public static final ic_omni_box_cancel:I = 0x7f020176

.field public static final ic_omni_box_cancel_highlighted:I = 0x7f020177

.field public static final ic_omni_box_cancel_selector:I = 0x7f020178

.field public static final ic_omni_box_car:I = 0x7f020179

.field public static final ic_omni_box_car_selector:I = 0x7f02017a

.field public static final ic_omni_box_directions:I = 0x7f02017b

.field public static final ic_omni_box_directions_highlighted:I = 0x7f02017c

.field public static final ic_omni_box_directions_selector:I = 0x7f02017d

.field public static final ic_omni_box_home_selector:I = 0x7f02017e

.field public static final ic_omni_box_list:I = 0x7f02017f

.field public static final ic_omni_box_list_dark_selector:I = 0x7f020180

.field public static final ic_omni_box_list_exp1:I = 0x7f020181

.field public static final ic_omni_box_list_exp2:I = 0x7f020182

.field public static final ic_omni_box_list_exp3:I = 0x7f020183

.field public static final ic_omni_box_list_highlight_selector:I = 0x7f020184

.field public static final ic_omni_box_list_highlighted:I = 0x7f020185

.field public static final ic_omni_box_list_selector:I = 0x7f020186

.field public static final ic_omni_box_list_thick_selector:I = 0x7f020187

.field public static final ic_omni_box_search:I = 0x7f020188

.field public static final ic_omni_box_search_highlighted:I = 0x7f020189

.field public static final ic_omni_box_search_selector:I = 0x7f02018a

.field public static final ic_omni_box_speech:I = 0x7f02018b

.field public static final ic_omni_box_speech_highlighted:I = 0x7f02018c

.field public static final ic_omni_box_speech_selector:I = 0x7f02018d

.field public static final ic_omni_box_transit:I = 0x7f02018e

.field public static final ic_omni_box_transit_selector:I = 0x7f02018f

.field public static final ic_omni_box_walking:I = 0x7f020190

.field public static final ic_omni_box_walking_selector:I = 0x7f020191

.field public static final ic_omni_box_work_selector:I = 0x7f020192

.field public static final ic_overflow:I = 0x7f020193

.field public static final ic_overflow_highlighted:I = 0x7f020194

.field public static final ic_overflow_selector:I = 0x7f020195

.field public static final ic_owner_response_add:I = 0x7f020196

.field public static final ic_plusone_medium_off_client:I = 0x7f020197

.field public static final ic_plusone_small_off_client:I = 0x7f020198

.field public static final ic_plusone_standard_off_client:I = 0x7f020199

.field public static final ic_plusone_tall_off_client:I = 0x7f02019a

.field public static final ic_qu_add:I = 0x7f02019b

.field public static final ic_qu_addplace:I = 0x7f02019c

.field public static final ic_qu_app_feedback:I = 0x7f02019d

.field public static final ic_qu_appbar_back:I = 0x7f02019e

.field public static final ic_qu_appbar_check:I = 0x7f02019f

.field public static final ic_qu_appbar_check_wht:I = 0x7f0201a0

.field public static final ic_qu_appbar_close:I = 0x7f0201a1

.field public static final ic_qu_appbar_close_night:I = 0x7f0201a2

.field public static final ic_qu_appbar_overflow:I = 0x7f0201a3

.field public static final ic_qu_arrow_preview:I = 0x7f0201a4

.field public static final ic_qu_arrow_right:I = 0x7f0201a5

.field public static final ic_qu_avatar_fab_small_color:I = 0x7f0201a6

.field public static final ic_qu_battery:I = 0x7f0201a7

.field public static final ic_qu_battery_night:I = 0x7f0201a8

.field public static final ic_qu_biking:I = 0x7f0201a9

.field public static final ic_qu_calendar:I = 0x7f0201aa

.field public static final ic_qu_call:I = 0x7f0201ab

.field public static final ic_qu_camera:I = 0x7f0201ac

.field public static final ic_qu_category:I = 0x7f0201ad

.field public static final ic_qu_clock:I = 0x7f0201ae

.field public static final ic_qu_close:I = 0x7f0201af

.field public static final ic_qu_compass_mode:I = 0x7f0201b0

.field public static final ic_qu_default_hero:I = 0x7f0201b1

.field public static final ic_qu_default_hero_tablet:I = 0x7f0201b2

.field public static final ic_qu_direction_dot:I = 0x7f0201b3

.field public static final ic_qu_direction_mylocation:I = 0x7f0201b4

.field public static final ic_qu_direction_reverse:I = 0x7f0201b5

.field public static final ic_qu_direction_setting_small:I = 0x7f0201b6

.field public static final ic_qu_directions:I = 0x7f0201b7

.field public static final ic_qu_done:I = 0x7f0201b8

.field public static final ic_qu_dot:I = 0x7f0201b9

.field public static final ic_qu_download:I = 0x7f0201ba

.field public static final ic_qu_download_white:I = 0x7f0201bb

.field public static final ic_qu_drive:I = 0x7f0201bc

.field public static final ic_qu_edit:I = 0x7f0201bd

.field public static final ic_qu_event:I = 0x7f0201be

.field public static final ic_qu_event_small:I = 0x7f0201bf

.field public static final ic_qu_exit_to_app:I = 0x7f0201c0

.field public static final ic_qu_expand_more:I = 0x7f0201c1

.field public static final ic_qu_explore:I = 0x7f0201c2

.field public static final ic_qu_explore_here:I = 0x7f0201c3

.field public static final ic_qu_explore_here_white:I = 0x7f0201c4

.field public static final ic_qu_fab_circle:I = 0x7f0201c5

.field public static final ic_qu_fab_shadow:I = 0x7f0201c6

.field public static final ic_qu_gallery:I = 0x7f0201c7

.field public static final ic_qu_google_earth:I = 0x7f0201c8

.field public static final ic_qu_halfstar_12:I = 0x7f0201c9

.field public static final ic_qu_halfstar_14:I = 0x7f0201ca

.field public static final ic_qu_halfstar_34:I = 0x7f0201cb

.field public static final ic_qu_hats_map:I = 0x7f0201cc

.field public static final ic_qu_home_large:I = 0x7f0201cd

.field public static final ic_qu_hotel:I = 0x7f0201ce

.field public static final ic_qu_hotel_white:I = 0x7f0201cf

.field public static final ic_qu_imagery_lookaround_active:I = 0x7f0201d0

.field public static final ic_qu_imagery_lookaround_inactive:I = 0x7f0201d1

.field public static final ic_qu_lists:I = 0x7f0201d2

.field public static final ic_qu_lists_circle_grey:I = 0x7f0201d3

.field public static final ic_qu_lists_white:I = 0x7f0201d4

.field public static final ic_qu_local_airport:I = 0x7f0201d5

.field public static final ic_qu_local_airport_s:I = 0x7f0201d6

.field public static final ic_qu_local_attraction:I = 0x7f0201d7

.field public static final ic_qu_local_bar:I = 0x7f0201d8

.field public static final ic_qu_local_cafe:I = 0x7f0201d9

.field public static final ic_qu_local_carwash:I = 0x7f0201da

.field public static final ic_qu_local_convenience_store:I = 0x7f0201db

.field public static final ic_qu_local_drink:I = 0x7f0201dc

.field public static final ic_qu_local_florist:I = 0x7f0201dd

.field public static final ic_qu_local_gas_station:I = 0x7f0201de

.field public static final ic_qu_local_grocery_store:I = 0x7f0201df

.field public static final ic_qu_local_home:I = 0x7f0201e0

.field public static final ic_qu_local_hospital:I = 0x7f0201e1

.field public static final ic_qu_local_hotel:I = 0x7f0201e2

.field public static final ic_qu_local_laundry_service:I = 0x7f0201e3

.field public static final ic_qu_local_library:I = 0x7f0201e4

.field public static final ic_qu_local_mall:I = 0x7f0201e5

.field public static final ic_qu_local_movies:I = 0x7f0201e6

.field public static final ic_qu_local_parking:I = 0x7f0201e7

.field public static final ic_qu_local_pharmacy:I = 0x7f0201e8

.field public static final ic_qu_local_pizza:I = 0x7f0201e9

.field public static final ic_qu_local_post_office:I = 0x7f0201ea

.field public static final ic_qu_local_print_shop:I = 0x7f0201eb

.field public static final ic_qu_local_restaurant:I = 0x7f0201ec

.field public static final ic_qu_local_restaurant_white:I = 0x7f0201ed

.field public static final ic_qu_local_search_atm:I = 0x7f0201ee

.field public static final ic_qu_local_search_more_selector:I = 0x7f0201ef

.field public static final ic_qu_local_see:I = 0x7f0201f0

.field public static final ic_qu_local_shipping:I = 0x7f0201f1

.field public static final ic_qu_local_taxi:I = 0x7f0201f2

.field public static final ic_qu_localguide:I = 0x7f0201f3

.field public static final ic_qu_map:I = 0x7f0201f4

.field public static final ic_qu_map_carto:I = 0x7f0201f5

.field public static final ic_qu_map_downloading:I = 0x7f0201f6

.field public static final ic_qu_map_error:I = 0x7f0201f7

.field public static final ic_qu_map_satellite:I = 0x7f0201f8

.field public static final ic_qu_map_waiting:I = 0x7f0201f9

.field public static final ic_qu_map_white_circle:I = 0x7f0201fa

.field public static final ic_qu_maps_color:I = 0x7f0201fb

.field public static final ic_qu_menu:I = 0x7f0201fc

.field public static final ic_qu_menu_grabber:I = 0x7f0201fd

.field public static final ic_qu_mic:I = 0x7f0201fe

.field public static final ic_qu_mic_none:I = 0x7f0201ff

.field public static final ic_qu_moreinfo:I = 0x7f020200

.field public static final ic_qu_nav_alternates_day:I = 0x7f020201

.field public static final ic_qu_nav_alternates_night:I = 0x7f020202

.field public static final ic_qu_nav_closebtn_day:I = 0x7f020203

.field public static final ic_qu_nav_closebtn_night:I = 0x7f020204

.field public static final ic_qu_nav_overflow_day:I = 0x7f020205

.field public static final ic_qu_nav_overflow_night:I = 0x7f020206

.field public static final ic_qu_navigation:I = 0x7f020207

.field public static final ic_qu_nextarrow_left_small:I = 0x7f020208

.field public static final ic_qu_nextarrow_right_small:I = 0x7f020209

.field public static final ic_qu_offline_error_nopadding:I = 0x7f02020a

.field public static final ic_qu_offline_promo:I = 0x7f02020b

.field public static final ic_qu_offline_white_circle:I = 0x7f02020c

.field public static final ic_qu_phone:I = 0x7f02020d

.field public static final ic_qu_phone_white:I = 0x7f02020e

.field public static final ic_qu_place:I = 0x7f02020f

.field public static final ic_qu_place_large:I = 0x7f020210

.field public static final ic_qu_place_small:I = 0x7f020211

.field public static final ic_qu_place_small_ads:I = 0x7f020212

.field public static final ic_qu_place_white:I = 0x7f020213

.field public static final ic_qu_pulse_location:I = 0x7f020214

.field public static final ic_qu_pulse_menu:I = 0x7f020215

.field public static final ic_qu_santa_face:I = 0x7f020216

.field public static final ic_qu_satellite:I = 0x7f020217

.field public static final ic_qu_save:I = 0x7f020218

.field public static final ic_qu_save_white:I = 0x7f020219

.field public static final ic_qu_savedplace:I = 0x7f02021a

.field public static final ic_qu_search:I = 0x7f02021b

.field public static final ic_qu_search_google_small_dark:I = 0x7f02021c

.field public static final ic_qu_search_result_busstop:I = 0x7f02021d

.field public static final ic_qu_search_result_busstop_white:I = 0x7f02021e

.field public static final ic_qu_search_transit:I = 0x7f02021f

.field public static final ic_qu_search_transit_white:I = 0x7f020220

.field public static final ic_qu_search_white:I = 0x7f020221

.field public static final ic_qu_send:I = 0x7f020222

.field public static final ic_qu_settings:I = 0x7f020223

.field public static final ic_qu_shadow_135c:I = 0x7f020224

.field public static final ic_qu_share:I = 0x7f020225

.field public static final ic_qu_share_white:I = 0x7f020226

.field public static final ic_qu_star_gray_12:I = 0x7f020227

.field public static final ic_qu_star_gray_14:I = 0x7f020228

.field public static final ic_qu_star_orange_12:I = 0x7f020229

.field public static final ic_qu_star_orange_14:I = 0x7f02022a

.field public static final ic_qu_star_orange_34:I = 0x7f02022b

.field public static final ic_qu_star_outlined_34:I = 0x7f02022c

.field public static final ic_qu_star_white100_12:I = 0x7f02022d

.field public static final ic_qu_star_white50_12:I = 0x7f02022e

.field public static final ic_qu_store_mall_directory:I = 0x7f02022f

.field public static final ic_qu_storedirectory:I = 0x7f020230

.field public static final ic_qu_storedirectory_white:I = 0x7f020231

.field public static final ic_qu_terrain:I = 0x7f020232

.field public static final ic_qu_then_green:I = 0x7f020233

.field public static final ic_qu_then_green_night:I = 0x7f020234

.field public static final ic_qu_then_grey:I = 0x7f020235

.field public static final ic_qu_todo:I = 0x7f020236

.field public static final ic_qu_todo_circle:I = 0x7f020237

.field public static final ic_qu_traffic:I = 0x7f020238

.field public static final ic_qu_transit:I = 0x7f020239

.field public static final ic_qu_transit_buyticket:I = 0x7f02023a

.field public static final ic_qu_transit_lasttrain:I = 0x7f02023b

.field public static final ic_qu_transit_timetable:I = 0x7f02023c

.field public static final ic_qu_transit_timetable_white:I = 0x7f02023d

.field public static final ic_qu_tutorialsidearrow:I = 0x7f02023e

.field public static final ic_qu_tutorialuparrow:I = 0x7f02023f

.field public static final ic_qu_walking:I = 0x7f020240

.field public static final ic_qu_warning:I = 0x7f020241

.field public static final ic_qu_website:I = 0x7f020242

.field public static final ic_qu_website_white:I = 0x7f020243

.field public static final ic_qu_work:I = 0x7f020244

.field public static final ic_qu_work_large:I = 0x7f020245

.field public static final ic_qu_work_white:I = 0x7f020246

.field public static final ic_qu_your_activity:I = 0x7f020247

.field public static final ic_qu_yourlocation_s:I = 0x7f020248

.field public static final ic_qu_zagat:I = 0x7f020249

.field public static final ic_resume:I = 0x7f02024a

.field public static final ic_save:I = 0x7f02024b

.field public static final ic_save_active:I = 0x7f02024c

.field public static final ic_save_disabled:I = 0x7f02024d

.field public static final ic_save_transparent:I = 0x7f02024e

.field public static final ic_search_result_check_in:I = 0x7f02024f

.field public static final ic_search_result_contact:I = 0x7f020250

.field public static final ic_search_result_history:I = 0x7f020251

.field public static final ic_search_result_home:I = 0x7f020252

.field public static final ic_search_result_place:I = 0x7f020253

.field public static final ic_search_result_search:I = 0x7f020254

.field public static final ic_search_result_work:I = 0x7f020255

.field public static final ic_search_transit:I = 0x7f020256

.field public static final ic_setting:I = 0x7f020257

.field public static final ic_shake_device:I = 0x7f020258

.field public static final ic_share:I = 0x7f020259

.field public static final ic_start_footer:I = 0x7f02025a

.field public static final ic_take_another_photo:I = 0x7f02025b

.field public static final ic_transit_alert01:I = 0x7f02025c

.field public static final ic_transit_current:I = 0x7f02025d

.field public static final ic_transit_current_nonactive:I = 0x7f02025e

.field public static final ic_transit_current_selector:I = 0x7f02025f

.field public static final ic_transit_notice_alert:I = 0x7f020260

.field public static final ic_transit_notice_information:I = 0x7f020261

.field public static final ic_transit_notice_warning:I = 0x7f020262

.field public static final ic_walking_total:I = 0x7f020263

.field public static final icon_splashscreen:I = 0x7f020264

.field public static final illus_offline_maps_full:I = 0x7f020265

.field public static final image_ad_a_photo:I = 0x7f020266

.field public static final indoor_floorpicker_selector:I = 0x7f020267

.field public static final inset_new_card:I = 0x7f020268

.field public static final layers_background:I = 0x7f020269

.field public static final layers_background_bitmap:I = 0x7f02026a

.field public static final layers_background_selector:I = 0x7f02026b

.field public static final list_entrypoint:I = 0x7f02026c

.field public static final list_entrypoint_highlighted:I = 0x7f02026d

.field public static final list_entrypoint_selector:I = 0x7f02026e

.field public static final list_item_background:I = 0x7f02030f

.field public static final loggedout_direction_illustration:I = 0x7f02026f

.field public static final loggedout_profile_illustration:I = 0x7f020270

.field public static final manage_accounts_icon:I = 0x7f020271

.field public static final menu_divider:I = 0x7f020272

.field public static final menu_divider_old:I = 0x7f020273

.field public static final mode_bike_focused:I = 0x7f020274

.field public static final mode_bike_off:I = 0x7f020275

.field public static final mode_bike_on:I = 0x7f020276

.field public static final mode_driving_focused:I = 0x7f020277

.field public static final mode_driving_off:I = 0x7f020278

.field public static final mode_driving_on:I = 0x7f020279

.field public static final mode_transit_focused:I = 0x7f02027a

.field public static final mode_transit_off:I = 0x7f02027b

.field public static final mode_transit_on:I = 0x7f02027c

.field public static final mode_walk_focused:I = 0x7f02027d

.field public static final mode_walk_off:I = 0x7f02027e

.field public static final mode_walk_on:I = 0x7f02027f

.field public static final mr_ic_audio_vol:I = 0x7f020280

.field public static final mr_ic_media_route_connecting_holo_dark:I = 0x7f020281

.field public static final mr_ic_media_route_connecting_holo_light:I = 0x7f020282

.field public static final mr_ic_media_route_disabled_holo_dark:I = 0x7f020283

.field public static final mr_ic_media_route_disabled_holo_light:I = 0x7f020284

.field public static final mr_ic_media_route_holo_dark:I = 0x7f020285

.field public static final mr_ic_media_route_holo_light:I = 0x7f020286

.field public static final mr_ic_media_route_off_holo_dark:I = 0x7f020287

.field public static final mr_ic_media_route_off_holo_light:I = 0x7f020288

.field public static final mr_ic_media_route_on_0_holo_dark:I = 0x7f020289

.field public static final mr_ic_media_route_on_0_holo_light:I = 0x7f02028a

.field public static final mr_ic_media_route_on_1_holo_dark:I = 0x7f02028b

.field public static final mr_ic_media_route_on_1_holo_light:I = 0x7f02028c

.field public static final mr_ic_media_route_on_2_holo_dark:I = 0x7f02028d

.field public static final mr_ic_media_route_on_2_holo_light:I = 0x7f02028e

.field public static final mr_ic_media_route_on_holo_dark:I = 0x7f02028f

.field public static final mr_ic_media_route_on_holo_light:I = 0x7f020290

.field public static final nav_notification_icon:I = 0x7f020291

.field public static final new_blue_dot:I = 0x7f020292

.field public static final new_blue_dot_disc:I = 0x7f020293

.field public static final new_blue_dot_glow:I = 0x7f020294

.field public static final new_blue_dot_halo:I = 0x7f020295

.field public static final new_card:I = 0x7f020296

.field public static final new_card_bottom:I = 0x7f020297

.field public static final new_card_bottom_fade:I = 0x7f020298

.field public static final new_card_bottom_highlighted:I = 0x7f020299

.field public static final new_card_bottom_mask:I = 0x7f02029a

.field public static final new_card_bottom_selected:I = 0x7f02029b

.field public static final new_card_highlighted:I = 0x7f02029c

.field public static final new_card_mask:I = 0x7f02029d

.field public static final new_card_middle:I = 0x7f02029e

.field public static final new_card_middle_highlighted:I = 0x7f02029f

.field public static final new_card_middle_mask:I = 0x7f0202a0

.field public static final new_card_middle_selected:I = 0x7f0202a1

.field public static final new_card_selected:I = 0x7f0202a2

.field public static final new_card_top:I = 0x7f0202a3

.field public static final new_card_top_highlighted:I = 0x7f0202a4

.field public static final new_card_top_mask:I = 0x7f0202a5

.field public static final new_card_top_selected:I = 0x7f0202a6

.field public static final new_direction_pointer:I = 0x7f0202a7

.field public static final new_gray_dot:I = 0x7f0202a8

.field public static final new_ic_bluedot_generic:I = 0x7f0202a9

.field public static final no_rmi:I = 0x7f0202aa

.field public static final omnibox:I = 0x7f0202ab

.field public static final padded_horizontal_divider:I = 0x7f0202ac

.field public static final pin:I = 0x7f0202ad

.field public static final pin_directionscard:I = 0x7f0202ae

.field public static final pin_iceberg:I = 0x7f0202af

.field public static final pin_santa:I = 0x7f0202b0

.field public static final pin_yellow:I = 0x7f0202b1

.field public static final powered_by_google_dark:I = 0x7f0202b2

.field public static final powered_by_google_light:I = 0x7f0202b3

.field public static final primary_padding_divider:I = 0x7f0202b4

.field public static final profile_micro_placeholder:I = 0x7f0202b5

.field public static final profile_pic_placeholder:I = 0x7f0202b6

.field public static final profile_pic_review_placeholder:I = 0x7f0202b7

.field public static final profile_xmicro_placeholder:I = 0x7f0202b8

.field public static final qu_appwidget_modebicycle_button:I = 0x7f0202b9

.field public static final qu_appwidget_modebicycle_button_selected:I = 0x7f0202ba

.field public static final qu_appwidget_modebicycle_button_unselected:I = 0x7f0202bb

.field public static final qu_appwidget_modedrive_button:I = 0x7f0202bc

.field public static final qu_appwidget_modedrive_button_selected:I = 0x7f0202bd

.field public static final qu_appwidget_modedrive_button_unselected:I = 0x7f0202be

.field public static final qu_appwidget_modetransit_button:I = 0x7f0202bf

.field public static final qu_appwidget_modetransit_button_selected:I = 0x7f0202c0

.field public static final qu_appwidget_modetransit_button_unselected:I = 0x7f0202c1

.field public static final qu_appwidget_modewalk_button:I = 0x7f0202c2

.field public static final qu_appwidget_modewalk_button_selected:I = 0x7f0202c3

.field public static final qu_appwidget_modewalk_button_unselected:I = 0x7f0202c4

.field public static final qu_bt_nightmode:I = 0x7f0202c5

.field public static final qu_bt_white:I = 0x7f0202c6

.field public static final qu_button_histogram:I = 0x7f0202c7

.field public static final qu_floor_picker:I = 0x7f0202c8

.field public static final qu_illusoob_locationhistory:I = 0x7f0202c9

.field public static final qu_illustbckg_yourplaces:I = 0x7f0202ca

.field public static final qu_shadow_opacity12:I = 0x7f0202cb

.field public static final qu_toolbargradient:I = 0x7f0202cc

.field public static final quantum_base_togglebutton_border:I = 0x7f0202cd

.field public static final quantum_blue_background_selector:I = 0x7f0202ce

.field public static final quantum_blue_background_selector_ripple:I = 0x7f0202cf

.field public static final quantum_flat_button_blue_background_selector:I = 0x7f0202d0

.field public static final quantum_flat_button_blue_background_selector_ripple:I = 0x7f0202d1

.field public static final quantum_flat_button_grey_background_selector:I = 0x7f0202d2

.field public static final quantum_flat_button_grey_background_selector_ripple:I = 0x7f0202d3

.field public static final quantum_gradient_bar_bottom:I = 0x7f0202d4

.field public static final quantum_gradient_bar_top:I = 0x7f0202d5

.field public static final quantum_gradient_bar_top_87:I = 0x7f0202d6

.field public static final quantum_highlight_border:I = 0x7f0202d7

.field public static final quantum_horizontal_progress_bar_background:I = 0x7f0202d8

.field public static final quantum_transit_option_btn_selector:I = 0x7f0202d9

.field public static final riddler_drop_shadow_above:I = 0x7f0202da

.field public static final search_measle_large:I = 0x7f0202db

.field public static final search_toggle_background:I = 0x7f0202dc

.field public static final search_toggle_selector:I = 0x7f0202dd

.field public static final secondstartpage_sync:I = 0x7f0202de

.field public static final selected_tab_background:I = 0x7f0202df

.field public static final sign_in_blue_button_selector:I = 0x7f0202e0

.field public static final sign_in_button_white_selector:I = 0x7f0202e1

.field public static final star_header_blank:I = 0x7f0202e2

.field public static final star_header_filled:I = 0x7f0202e3

.field public static final star_header_half:I = 0x7f0202e4

.field public static final star_review_blank:I = 0x7f0202e5

.field public static final star_review_filled:I = 0x7f0202e6

.field public static final star_review_half:I = 0x7f0202e7

.field public static final star_review_input_blank:I = 0x7f0202e8

.field public static final star_review_input_full:I = 0x7f0202e9

.field public static final startpoint_measle:I = 0x7f0202ea

.field public static final toggle:I = 0x7f0202eb

.field public static final transit_details_endnoderow_background:I = 0x7f0202ec

.field public static final transit_details_noderow_background:I = 0x7f0202ed

.field public static final transit_details_rowwithbottomruler_background:I = 0x7f0202ee

.field public static final transit_details_rowwithnoruler_background:I = 0x7f0202ef

.field public static final transit_details_rowwithtopruler_background:I = 0x7f0202f0

.field public static final transit_details_whitenoderow_background:I = 0x7f0202f1

.field public static final transit_lasttrain:I = 0x7f0202f2

.field public static final transit_list_collapse:I = 0x7f0202f3

.field public static final transit_list_expand:I = 0x7f0202f4

.field public static final transit_option_btn_left:I = 0x7f0202f5

.field public static final transit_option_btn_left_pressed:I = 0x7f0202f6

.field public static final transit_option_btn_left_selector:I = 0x7f0202f7

.field public static final transit_option_btn_middle:I = 0x7f0202f8

.field public static final transit_option_btn_middle_pressed:I = 0x7f0202f9

.field public static final transit_option_btn_middle_selector:I = 0x7f0202fa

.field public static final transit_option_btn_right:I = 0x7f0202fb

.field public static final transit_option_btn_right_pressed:I = 0x7f0202fc

.field public static final transit_option_btn_right_selector:I = 0x7f0202fd

.field public static final transit_optionarrow_left:I = 0x7f0202fe

.field public static final transit_optionarrow_right:I = 0x7f0202ff

.field public static final transit_result_rightarrow:I = 0x7f020300

.field public static final transitpoint_measle:I = 0x7f020301

.field public static final tutorial_leftarrow:I = 0x7f020302

.field public static final tutorial_rightarrow:I = 0x7f020303

.field public static final tutorial_uparrow:I = 0x7f020304

.field public static final tutorial_view:I = 0x7f020305

.field public static final undo:I = 0x7f020306

.field public static final view_selected:I = 0x7f020307

.field public static final view_selected_pressed:I = 0x7f020308

.field public static final watermark_dark:I = 0x7f020309

.field public static final watermark_light:I = 0x7f02030a

.field public static final wazelogo:I = 0x7f02030b

.field public static final x_badge:I = 0x7f02030c

.field public static final xmicro_profile_pic_placeholder:I = 0x7f02030d

.field public static final zagat_badge_card:I = 0x7f02030e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
