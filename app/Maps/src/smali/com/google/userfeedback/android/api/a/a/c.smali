.class Lcom/google/userfeedback/android/api/a/a/c;
.super Lorg/apache/http/impl/client/DefaultHttpClient;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/userfeedback/android/api/a/a/a;


# direct methods
.method constructor <init>(Lcom/google/userfeedback/android/api/a/a/a;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/userfeedback/android/api/a/a/c;->a:Lcom/google/userfeedback/android/api/a/a/a;

    invoke-direct {p0, p2, p3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    return-void
.end method


# virtual methods
.method protected createHttpContext()Lorg/apache/http/protocol/HttpContext;
    .locals 3

    .prologue
    .line 162
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 163
    const-string v1, "http.authscheme-registry"

    .line 165
    invoke-virtual {p0}, Lcom/google/userfeedback/android/api/a/a/c;->getAuthSchemes()Lorg/apache/http/auth/AuthSchemeRegistry;

    move-result-object v2

    .line 163
    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 166
    const-string v1, "http.cookiespec-registry"

    .line 168
    invoke-virtual {p0}, Lcom/google/userfeedback/android/api/a/a/c;->getCookieSpecs()Lorg/apache/http/cookie/CookieSpecRegistry;

    move-result-object v2

    .line 166
    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 169
    const-string v1, "http.auth.credentials-provider"

    .line 171
    invoke-virtual {p0}, Lcom/google/userfeedback/android/api/a/a/c;->getCredentialsProvider()Lorg/apache/http/client/CredentialsProvider;

    move-result-object v2

    .line 169
    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 172
    return-object v0
.end method

.method protected createHttpProcessor()Lorg/apache/http/protocol/BasicHttpProcessor;
    .locals 3

    .prologue
    .line 151
    invoke-super {p0}, Lorg/apache/http/impl/client/DefaultHttpClient;->createHttpProcessor()Lorg/apache/http/protocol/BasicHttpProcessor;

    move-result-object v0

    .line 152
    sget-object v1, Lcom/google/userfeedback/android/api/a/a/a;->b:Lorg/apache/http/HttpRequestInterceptor;

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 153
    new-instance v1, Lcom/google/userfeedback/android/api/a/a/d;

    iget-object v2, p0, Lcom/google/userfeedback/android/api/a/a/c;->a:Lcom/google/userfeedback/android/api/a/a/a;

    invoke-direct {v1, v2}, Lcom/google/userfeedback/android/api/a/a/d;-><init>(Lcom/google/userfeedback/android/api/a/a/a;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 155
    return-object v0
.end method
