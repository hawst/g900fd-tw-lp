.class Lcom/google/userfeedback/android/api/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Landroid/app/Activity;

.field final synthetic e:Lcom/google/userfeedback/android/api/UserFeedback;


# direct methods
.method constructor <init>(Lcom/google/userfeedback/android/api/UserFeedback;ZZLjava/lang/String;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/userfeedback/android/api/f;->e:Lcom/google/userfeedback/android/api/UserFeedback;

    iput-boolean p2, p0, Lcom/google/userfeedback/android/api/f;->a:Z

    iput-boolean p3, p0, Lcom/google/userfeedback/android/api/f;->b:Z

    iput-object p4, p0, Lcom/google/userfeedback/android/api/f;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/userfeedback/android/api/f;->d:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/userfeedback/android/api/f;->e:Lcom/google/userfeedback/android/api/UserFeedback;

    iget-boolean v1, p0, Lcom/google/userfeedback/android/api/f;->a:Z

    iget-boolean v2, p0, Lcom/google/userfeedback/android/api/f;->b:Z

    iget-object v3, p0, Lcom/google/userfeedback/android/api/f;->c:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/userfeedback/android/api/UserFeedback;->submitFeedback(ZZLjava/lang/String;Z)V

    .line 257
    iget-object v0, p0, Lcom/google/userfeedback/android/api/f;->d:Landroid/app/Activity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 258
    iget-object v0, p0, Lcom/google/userfeedback/android/api/f;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 259
    return-void
.end method
