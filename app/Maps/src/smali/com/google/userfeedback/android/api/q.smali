.class Lcom/google/userfeedback/android/api/q;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/userfeedback/android/api/UserFeedbackReport;

.field final synthetic b:Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder$BuilderListener;

.field final synthetic c:Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder;


# direct methods
.method constructor <init>(Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder;Lcom/google/userfeedback/android/api/UserFeedbackReport;Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder$BuilderListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/userfeedback/android/api/q;->c:Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder;

    iput-object p2, p0, Lcom/google/userfeedback/android/api/q;->a:Lcom/google/userfeedback/android/api/UserFeedbackReport;

    iput-object p3, p0, Lcom/google/userfeedback/android/api/q;->b:Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder$BuilderListener;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/userfeedback/android/api/q;->c:Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder;

    iget-object v1, p0, Lcom/google/userfeedback/android/api/q;->a:Lcom/google/userfeedback/android/api/UserFeedbackReport;

    # invokes: Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder;->populateReportData(Lcom/google/userfeedback/android/api/UserFeedbackReport;)V
    invoke-static {v0, v1}, Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder;->access$000(Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder;Lcom/google/userfeedback/android/api/UserFeedbackReport;)V

    iget-object v0, p0, Lcom/google/userfeedback/android/api/q;->b:Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder$BuilderListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/userfeedback/android/api/q;->b:Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder$BuilderListener;

    invoke-interface {v0}, Lcom/google/userfeedback/android/api/UserFeedbackReportBuilder$BuilderListener;->onComplete()V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
