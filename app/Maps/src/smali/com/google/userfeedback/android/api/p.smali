.class Lcom/google/userfeedback/android/api/p;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter$Row;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 458
    iput-object p1, p0, Lcom/google/userfeedback/android/api/p;->a:Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 455
    iput v0, p0, Lcom/google/userfeedback/android/api/p;->c:I

    .line 456
    iput v0, p0, Lcom/google/userfeedback/android/api/p;->d:I

    .line 459
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/userfeedback/android/api/p;->b:Ljava/lang/ref/WeakReference;

    .line 460
    invoke-virtual {p2}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/userfeedback/android/api/p;->c:I

    .line 461
    invoke-virtual {p2}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/userfeedback/android/api/p;->d:I

    .line 462
    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 453
    check-cast p1, [Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter$Row;

    iget-object v0, p0, Lcom/google/userfeedback/android/api/p;->a:Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    iget-object v1, v1, Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter$Row;->imageBytes:[B

    iget v2, p0, Lcom/google/userfeedback/android/api/p;->d:I

    iget v3, p0, Lcom/google/userfeedback/android/api/p;->c:I

    # invokes: Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter;->decodeSampledBitmap([BII)Landroid/graphics/Bitmap;
    invoke-static {v0, v1, v2, v3}, Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter;->access$000(Lcom/google/userfeedback/android/api/UserFeedbackReportAdapter;[BII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 453
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/userfeedback/android/api/p;->b:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/userfeedback/android/api/p;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
