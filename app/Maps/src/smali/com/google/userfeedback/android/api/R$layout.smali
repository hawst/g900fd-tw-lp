.class public final Lcom/google/userfeedback/android/api/R$layout;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f040000

.field public static final abc_action_bar_up_container:I = 0x7f040001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f040002

.field public static final abc_action_menu_item_layout:I = 0x7f040003

.field public static final abc_action_menu_layout:I = 0x7f040004

.field public static final abc_action_mode_bar:I = 0x7f040005

.field public static final abc_action_mode_close_item_material:I = 0x7f040006

.field public static final abc_activity_chooser_view:I = 0x7f040007

.field public static final abc_activity_chooser_view_include:I = 0x7f040008

.field public static final abc_activity_chooser_view_list_item:I = 0x7f040009

.field public static final abc_expanded_menu_layout:I = 0x7f04000a

.field public static final abc_list_menu_item_checkbox:I = 0x7f04000b

.field public static final abc_list_menu_item_icon:I = 0x7f04000c

.field public static final abc_list_menu_item_layout:I = 0x7f04000d

.field public static final abc_list_menu_item_radio:I = 0x7f04000e

.field public static final abc_popup_menu_item_layout:I = 0x7f04000f

.field public static final abc_screen_content_include:I = 0x7f040010

.field public static final abc_screen_simple:I = 0x7f040011

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f040012

.field public static final abc_screen_toolbar:I = 0x7f040013

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f040014

.field public static final abc_search_view:I = 0x7f040015

.field public static final abc_simple_dropdown_hint:I = 0x7f040016

.field public static final account_details_with_avatar:I = 0x7f040017

.field public static final account_dialog:I = 0x7f040018

.field public static final account_item_view:I = 0x7f040019

.field public static final account_switcher:I = 0x7f04001a

.field public static final add_account:I = 0x7f04001b

.field public static final avatars:I = 0x7f04001c

.field public static final base_cardstackcontainer_internal:I = 0x7f04001d

.field public static final base_compassbutton_internal:I = 0x7f04001e

.field public static final base_floatingbar_internal:I = 0x7f04001f

.field public static final base_fullscreencontainer_internal:I = 0x7f040020

.field public static final base_gmmactivity_internal:I = 0x7f040021

.field public static final base_header:I = 0x7f040022

.field public static final base_horizontalhistogram_content:I = 0x7f040023

.field public static final base_horizontalhistogramlabel_internal:I = 0x7f040024

.field public static final base_main_internal:I = 0x7f040025

.field public static final base_mapactivity_internal:I = 0x7f040026

.field public static final base_scrollableviewdivider_internal:I = 0x7f040027

.field public static final car_app_header:I = 0x7f040028

.field public static final car_drawer_fragment:I = 0x7f040029

.field public static final car_list_item_1:I = 0x7f04002a

.field public static final car_list_item_1_card:I = 0x7f04002b

.field public static final car_list_item_1_small:I = 0x7f04002c

.field public static final car_list_item_1_small_card:I = 0x7f04002d

.field public static final car_list_item_2:I = 0x7f04002e

.field public static final car_list_item_2_card:I = 0x7f04002f

.field public static final car_list_item_empty:I = 0x7f040030

.field public static final car_paged_recycler_view:I = 0x7f040031

.field public static final cardui_requestfailure_card:I = 0x7f040032

.field public static final directions_agencyinfo_button:I = 0x7f040033

.field public static final directions_bicyclingdetails_internal_header:I = 0x7f040034

.field public static final directions_bicyclingdetails_page:I = 0x7f040035

.field public static final directions_details_cardlist:I = 0x7f040036

.field public static final directions_drivingdetails_internal_header:I = 0x7f040037

.field public static final directions_drivingdetails_page:I = 0x7f040038

.field public static final directions_elevation_content:I = 0x7f040039

.field public static final directions_input_panel:I = 0x7f04003a

.field public static final directions_options_internal_checkbox:I = 0x7f04003b

.field public static final directions_options_internal_empty_view:I = 0x7f04003c

.field public static final directions_options_internal_first_caption_text:I = 0x7f04003d

.field public static final directions_options_internal_radiobutton:I = 0x7f04003e

.field public static final directions_options_internal_radiogroup:I = 0x7f04003f

.field public static final directions_options_internal_routeoptions_listview:I = 0x7f040040

.field public static final directions_options_page:I = 0x7f040041

.field public static final directions_phonestart_page:I = 0x7f040042

.field public static final directions_start_internal_bottom_content:I = 0x7f040043

.field public static final directions_summary_content:I = 0x7f040044

.field public static final directions_swipeabledatepicker_content:I = 0x7f040045

.field public static final directions_swipeabledatepicker_internal_date_text:I = 0x7f040046

.field public static final directions_taxitrip_content:I = 0x7f040047

.field public static final directions_transitdatetimeoptions_dialog:I = 0x7f040048

.field public static final directions_transitdetails_internal_blocktransferrow:I = 0x7f040049

.field public static final directions_transitdetails_internal_expandablenoticerow:I = 0x7f04004a

.field public static final directions_transitdetails_internal_header:I = 0x7f04004b

.field public static final directions_transitdetails_internal_intermediatestoprow:I = 0x7f04004c

.field public static final directions_transitdetails_internal_noderow:I = 0x7f04004d

.field public static final directions_transitdetails_internal_nontransitsteprow:I = 0x7f04004e

.field public static final directions_transitdetails_internal_noticecontent:I = 0x7f04004f

.field public static final directions_transitdetails_internal_segmentrow:I = 0x7f040050

.field public static final directions_transitdetails_internal_transittimescolumn_content:I = 0x7f040051

.field public static final directions_transitdetails_page:I = 0x7f040052

.field public static final directions_transitvehiclesellipsis_textbox:I = 0x7f040053

.field public static final directions_trip_content:I = 0x7f040054

.field public static final directions_walkingdetails_internal_header:I = 0x7f040055

.field public static final directions_walkingdetails_page:I = 0x7f040056

.field public static final generic_alertdialog_content:I = 0x7f040057

.field public static final generic_callaction_button:I = 0x7f040058

.field public static final generic_card:I = 0x7f040059

.field public static final generic_cardlist:I = 0x7f04005a

.field public static final generic_cardlist_divider:I = 0x7f04005b

.field public static final generic_cardlist_internal:I = 0x7f04005c

.field public static final generic_cardui_page:I = 0x7f04005d

.field public static final generic_cardview:I = 0x7f04005e

.field public static final generic_dialog_divider:I = 0x7f04005f

.field public static final generic_dialog_header:I = 0x7f040060

.field public static final generic_dialogicon_listitem:I = 0x7f040061

.field public static final generic_editaliasaction_button:I = 0x7f040062

.field public static final generic_headerbackground_carditem:I = 0x7f040063

.field public static final generic_icon_listitem:I = 0x7f040064

.field public static final generic_link_content:I = 0x7f040065

.field public static final generic_listcard_divider:I = 0x7f040066

.field public static final generic_listcard_footer:I = 0x7f040067

.field public static final generic_listcard_header:I = 0x7f040068

.field public static final generic_listcard_listitem:I = 0x7f040069

.field public static final generic_listcardheader_listitem:I = 0x7f04006a

.field public static final generic_newprogress_content:I = 0x7f04006b

.field public static final generic_nopadding_card:I = 0x7f04006c

.field public static final generic_page:I = 0x7f04006d

.field public static final generic_placesummary_card:I = 0x7f04006e

.field public static final generic_plain_listitem:I = 0x7f04006f

.field public static final generic_progress_content:I = 0x7f040070

.field public static final generic_shareaction_button:I = 0x7f040071

.field public static final generic_sponsoredlink_item:I = 0x7f040072

.field public static final generic_tablet_page:I = 0x7f040073

.field public static final generic_webview_page:I = 0x7f040074

.field public static final gf_expandable_row:I = 0x7f040075

.field public static final gf_feedback_activity:I = 0x7f040076

.field public static final gf_label_value_row:I = 0x7f040077

.field public static final gf_preview_activity:I = 0x7f040078

.field public static final gf_screenshot_row:I = 0x7f040079

.field public static final gf_section_header_row:I = 0x7f04007a

.field public static final gf_show_text_activity:I = 0x7f04007b

.field public static final gf_userfeedback_account_spinner:I = 0x7f04007c

.field public static final gmm_recycler_view:I = 0x7f04007d

.field public static final hotels_priceheader_item:I = 0x7f04007e

.field public static final indoor_floorpicker_listitem:I = 0x7f04007f

.field public static final layers_accountswitcher_internal:I = 0x7f040080

.field public static final layers_menu_container:I = 0x7f040081

.field public static final layers_menu_internal:I = 0x7f040082

.field public static final layers_menu_internal_menu_button:I = 0x7f040083

.field public static final login_loginpromptpanel_internal:I = 0x7f040084

.field public static final login_loginpromptpanel_internal_myprofile:I = 0x7f040085

.field public static final login_loginpromptpanel_internal_offline:I = 0x7f040086

.field public static final login_loginpromptpanel_internal_transit:I = 0x7f040087

.field public static final login_promo_page:I = 0x7f040088

.field public static final manage_accounts:I = 0x7f040089

.field public static final mr_media_route_chooser_dialog:I = 0x7f04008a

.field public static final mr_media_route_controller_dialog:I = 0x7f04008b

.field public static final mr_media_route_list_item:I = 0x7f04008c

.field public static final navigation_genericexit_internal:I = 0x7f04008d

.field public static final navigation_stepcueview_internal:I = 0x7f04008e

.field public static final offlinecache_rename_internal:I = 0x7f04008f

.field public static final offlinecache_rename_page:I = 0x7f040090

.field public static final photo_newphoto_page:I = 0x7f040091

.field public static final photo_viewer_listitem:I = 0x7f040092

.field public static final photo_viewer_page:I = 0x7f040093

.field public static final place_event_card:I = 0x7f040094

.field public static final place_flight_card:I = 0x7f040095

.field public static final place_hotel_card:I = 0x7f040096

.field public static final place_station_internal_servicealert:I = 0x7f040097

.field public static final place_timetabledetails_internal_linelabel:I = 0x7f040098

.field public static final qu_appwidget_directionsshortcut_page:I = 0x7f040099

.field public static final qu_appwidget_travelmode_radiogroup:I = 0x7f04009a

.field public static final qu_commodity_tactileplace_card:I = 0x7f04009b

.field public static final qu_generic_tactileplace_card:I = 0x7f04009c

.field public static final qu_header:I = 0x7f04009d

.field public static final qu_list_item_settings:I = 0x7f04009e

.field public static final qu_navigation_disclaimer_dialog:I = 0x7f04009f

.field public static final qu_navigation_iconmenu_button:I = 0x7f0400a0

.field public static final qu_search_ad_internal:I = 0x7f0400a1

.field public static final qu_search_list_page:I = 0x7f0400a2

.field public static final qu_tutorial_bluedot_internal:I = 0x7f0400a3

.field public static final qu_tutorial_bluedottablet_internal:I = 0x7f0400a4

.field public static final qu_tutorial_layers_internal:I = 0x7f0400a5

.field public static final qu_tutorial_layerstablet_internal:I = 0x7f0400a6

.field public static final qu_tutorial_pullup_internal:I = 0x7f0400a7

.field public static final qu_tutorial_pulluptablet_internal:I = 0x7f0400a8

.field public static final qu_tutorial_swipe_internal:I = 0x7f0400a9

.field public static final qu_tutorial_swipetablet_internal:I = 0x7f0400aa

.field public static final reportaproblem_notavailable_page:I = 0x7f0400ab

.field public static final reportmapissue_address_internal:I = 0x7f0400ac

.field public static final reportmapissue_buttons_internal:I = 0x7f0400ad

.field public static final reportmapissue_legal_internal:I = 0x7f0400ae

.field public static final reportmapissue_location_internal:I = 0x7f0400af

.field public static final reportmapissue_name_internal:I = 0x7f0400b0

.field public static final reportmapissue_other_internal:I = 0x7f0400b1

.field public static final reportmapissue_phone_internal:I = 0x7f0400b2

.field public static final reportmapissue_selectincorrectinfo_internal:I = 0x7f0400b3

.field public static final reportmapissue_website_internal:I = 0x7f0400b4

.field public static final review_ownerresponse_container:I = 0x7f0400b5

.field public static final review_ownerresponse_page:I = 0x7f0400b6

.field public static final review_updateownerresponse_container:I = 0x7f0400b7

.field public static final search_hotelprice_item:I = 0x7f0400b8

.field public static final search_list_page_footer:I = 0x7f0400b9

.field public static final search_loading_page:I = 0x7f0400ba

.field public static final search_place_page:I = 0x7f0400bb

.field public static final search_placeattribution_content:I = 0x7f0400bc

.field public static final search_placemoreinfo_page:I = 0x7f0400bd

.field public static final search_reportproblem_card:I = 0x7f0400be

.field public static final selected_account:I = 0x7f0400bf

.field public static final selected_account_short:I = 0x7f0400c0

.field public static final settings_legal_prefs:I = 0x7f0400c1

.field public static final settings_navigation_prefs:I = 0x7f0400c2

.field public static final settings_opensource_page:I = 0x7f0400c3

.field public static final settings_tutorials_prefs:I = 0x7f0400c4

.field public static final sign_in:I = 0x7f0400c5

.field public static final startpage_cardlist:I = 0x7f0400c6

.field public static final suggest_fragment_internal:I = 0x7f0400c7

.field public static final suggest_qpresult_cardlist:I = 0x7f0400c8

.field public static final suggest_result_cardlist:I = 0x7f0400c9

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0400ca

.field public static final survey_dialog:I = 0x7f0400cb

.field public static final terms_confidentiality_page:I = 0x7f0400cc

.field public static final terms_dialog:I = 0x7f0400cd

.field public static final terms_killswitch_page:I = 0x7f0400ce

.field public static final viewbinder_adbadgetextboxtextview_internal:I = 0x7f0400cf

.field public static final viewbinder_card_internal:I = 0x7f0400d0

.field public static final viewbinder_cardbottomicontextbuttonlinearlayout_internal:I = 0x7f0400d1

.field public static final viewbinder_cardcontentframelayout_internal:I = 0x7f0400d2

.field public static final viewbinder_cardcontentlinearlayout_internal:I = 0x7f0400d3

.field public static final viewbinder_cardlinearlayout_internal:I = 0x7f0400d4

.field public static final viewbinder_cardlistlinearlayout_internal:I = 0x7f0400d5

.field public static final viewbinder_cardpagelinearlayout_internal:I = 0x7f0400d6

.field public static final viewbinder_cardtitletextbox_internal:I = 0x7f0400d7

.field public static final viewbinder_checkedtextview_internal:I = 0x7f0400d8

.field public static final viewbinder_collapsedbodytextbox_internal:I = 0x7f0400d9

.field public static final viewbinder_containerframelayout_internal:I = 0x7f0400da

.field public static final viewbinder_containerhorizontalflowlayout_internal:I = 0x7f0400db

.field public static final viewbinder_containerlinearlayout_internal:I = 0x7f0400dc

.field public static final viewbinder_contentlinearlayout_internal:I = 0x7f0400dd

.field public static final viewbinder_croppedimagewebimageview_internal:I = 0x7f0400de

.field public static final viewbinder_dialogbuttoncontent_internal:I = 0x7f0400df

.field public static final viewbinder_expandedbodytextbox_internal:I = 0x7f0400e0

.field public static final viewbinder_fivestartextview_internal:I = 0x7f0400e1

.field public static final viewbinder_gmmprogressbar_internal:I = 0x7f0400e2

.field public static final viewbinder_headerfivestarcontent_internal:I = 0x7f0400e3

.field public static final viewbinder_horizontalcontainerlinearlayout_internal:I = 0x7f0400e4

.field public static final viewbinder_horizontaldivider_internal:I = 0x7f0400e5

.field public static final viewbinder_horizontaldividercontentlinearlayout_internal:I = 0x7f0400e6

.field public static final viewbinder_horizontalwrapcontentcontainerlinearlayout_internal:I = 0x7f0400e7

.field public static final viewbinder_icontextboxtextview_internal:I = 0x7f0400e8

.field public static final viewbinder_icontextbutton_internal:I = 0x7f0400e9

.field public static final viewbinder_imagewebimageview_internal:I = 0x7f0400ea

.field public static final viewbinder_interactiveinputfivestarcontent_internal:I = 0x7f0400eb

.field public static final viewbinder_invisiblecardframelayout_internal:I = 0x7f0400ec

.field public static final viewbinder_nopaddingbodytextboxtextview_internal:I = 0x7f0400ed

.field public static final viewbinder_nopaddingheadercardlinearlayout_internal:I = 0x7f0400ee

.field public static final viewbinder_notopbottompaddingcardlinearlayout_internal:I = 0x7f0400ef

.field public static final viewbinder_singlelinetextboxtextview_internal:I = 0x7f0400f0

.field public static final viewbinder_singlelinetighttextboxtextview_internal:I = 0x7f0400f1

.field public static final viewbinder_spinnerbutton_internal:I = 0x7f0400f2

.field public static final viewbinder_spinnerstyleframelayout_internal:I = 0x7f0400f3

.field public static final viewbinder_spinnertimepicker_internal:I = 0x7f0400f4

.field public static final viewbinder_standalonenopaddingcardlinearlayout_internal:I = 0x7f0400f5

.field public static final viewbinder_textboxrelativelayout_internal:I = 0x7f0400f6

.field public static final viewbinder_textboxtextview_internal:I = 0x7f0400f7

.field public static final viewbinder_verticaldivider_internal:I = 0x7f0400f8

.field public static final viewbinder_wrapcontentcontainerrelativelayout_internal:I = 0x7f0400f9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
