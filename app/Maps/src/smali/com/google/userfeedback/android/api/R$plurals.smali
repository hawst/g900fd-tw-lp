.class public final Lcom/google/userfeedback/android/api/R$plurals;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final ACCESSIBILITY_DECIMAL_STARS:I = 0x7f110002

.field public static final ACCESSIBILITY_STARS:I = 0x7f110001

.field public static final DA_DAYS:I = 0x7f11000c

.field public static final DA_HOURS:I = 0x7f11000e

.field public static final DA_MINUTES:I = 0x7f110010

.field public static final DA_SPEECH_CONTINUE_FOR_X_FEET:I = 0x7f11001f

.field public static final DA_SPEECH_CONTINUE_FOR_X_KILOMETERS:I = 0x7f110016

.field public static final DA_SPEECH_CONTINUE_FOR_X_METERS:I = 0x7f110015

.field public static final DA_SPEECH_CONTINUE_FOR_X_MILES:I = 0x7f110021

.field public static final DA_SPEECH_CONTINUE_FOR_X_YARDS:I = 0x7f110020

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_X_FEET:I = 0x7f110022

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_X_KILOMETERS:I = 0x7f110018

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_X_METERS:I = 0x7f110017

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_X_MILES:I = 0x7f110024

.field public static final DA_SPEECH_CONTINUE_ON_ROAD_FOR_X_YARDS:I = 0x7f110023

.field public static final DA_SPEECH_DAYS:I = 0x7f11000b

.field public static final DA_SPEECH_HOURS:I = 0x7f11000d

.field public static final DA_SPEECH_IN_X_FEET:I = 0x7f110019

.field public static final DA_SPEECH_IN_X_KILOMETERS:I = 0x7f110013

.field public static final DA_SPEECH_IN_X_METERS:I = 0x7f110011

.field public static final DA_SPEECH_IN_X_MILES:I = 0x7f11001d

.field public static final DA_SPEECH_IN_X_YARDS:I = 0x7f11001b

.field public static final DA_SPEECH_MINUTES:I = 0x7f11000f

.field public static final DA_YOUR_DESTINATION_IS_X_FEET_AHEAD:I = 0x7f11001a

.field public static final DA_YOUR_DESTINATION_IS_X_KILOMETERS_AHEAD:I = 0x7f110014

.field public static final DA_YOUR_DESTINATION_IS_X_METERS_AHEAD:I = 0x7f110012

.field public static final DA_YOUR_DESTINATION_IS_X_MILES_AHEAD:I = 0x7f11001e

.field public static final DA_YOUR_DESTINATION_IS_X_YARDS_AHEAD:I = 0x7f11001c

.field public static final DIRECTIONS_MORE_TRAFFIC_DISRUPTIONS:I = 0x7f110007

.field public static final FIVESTAR_HISTOGRAM_BAR_LABEL:I = 0x7f110025

.field public static final IAMHERE_YOUVE_TAKEN_PHOTO_HERE:I = 0x7f11002c

.field public static final OFFLINE_CACHE_ESTIMATED_SIZE_NUM:I = 0x7f110029

.field public static final OFFLINE_CACHE_PROGRESS_NUM:I = 0x7f11002a

.field public static final OFFLINE_MAPS_CARD_TITLE:I = 0x7f11002b

.field public static final PERSONAL_HOTEL_RESERVATION_NIGHTS:I = 0x7f110027

.field public static final PHOTOS:I = 0x7f110000

.field public static final PHOTO_UPLOADED_MSG:I = 0x7f110006

.field public static final PLACE_PAGE_EXPANDED_NUMBER_OF_REVIEWS:I = 0x7f110026

.field public static final PROFILE_ACTIVITY_NUMBER_OF_RATINGS:I = 0x7f110009

.field public static final PROFILE_ACTIVITY_NUMBER_OF_REVIEWS:I = 0x7f11000a

.field public static final RESERVATION_PARTY_SIZE:I = 0x7f110028

.field public static final REVIEW_SNIPPET_AUTHOR_WITH_SIMILAR_STATEMENTS:I = 0x7f110004

.field public static final REVIEW_SNIPPET_TOTAL_STATEMENTS:I = 0x7f110005

.field public static final STATION_NEARBY_STATIONS:I = 0x7f110003

.field public static final TRANSIT_NUM_STOPS:I = 0x7f110008


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
