.class public final Lcom/google/r/b/a/agn;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ags;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/agl;",
        "Lcom/google/r/b/a/agn;",
        ">;",
        "Lcom/google/r/b/a/ags;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/r/b/a/afn;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/ago;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1802
    sget-object v0, Lcom/google/r/b/a/agl;->e:Lcom/google/r/b/a/agl;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1882
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    .line 1944
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    .line 2069
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    .line 1803
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2071
    iget v0, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 2072
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    .line 2073
    iget v0, p0, Lcom/google/r/b/a/agn;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/agn;->a:I

    .line 2075
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1794
    new-instance v2, Lcom/google/r/b/a/agl;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/agl;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/agn;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    iput-object v1, v2, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    iget v1, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/agn;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/agn;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/agl;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1794
    check-cast p1, Lcom/google/r/b/a/agl;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/agn;->a(Lcom/google/r/b/a/agl;)Lcom/google/r/b/a/agn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/agl;)Lcom/google/r/b/a/agn;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1842
    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1867
    :goto_0
    return-object p0

    .line 1843
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1844
    iget-object v0, p1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_2
    iget v2, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_6

    iget-object v1, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v2

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    invoke-static {v1}, Lcom/google/r/b/a/afn;->a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/afp;->a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/afp;->c()Lcom/google/r/b/a/afn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    :goto_3
    iget v0, p0, Lcom/google/r/b/a/agn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/agn;->a:I

    .line 1846
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1847
    iget-object v0, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1848
    iget-object v0, p1, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    .line 1849
    iget v0, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/agn;->a:I

    .line 1856
    :cond_2
    :goto_4
    iget-object v0, p1, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1857
    iget-object v0, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1858
    iget-object v0, p1, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    .line 1859
    iget v0, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/r/b/a/agn;->a:I

    .line 1866
    :cond_3
    :goto_5
    iget-object v0, p1, Lcom/google/r/b/a/agl;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1843
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1844
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_2

    :cond_6
    iput-object v0, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    goto :goto_3

    .line 1851
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/agn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/agn;->a:I

    .line 1852
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/agn;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 1861
    :cond_9
    invoke-direct {p0}, Lcom/google/r/b/a/agn;->c()V

    .line 1862
    iget-object v0, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/r/b/a/agn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/r/b/a/ada;",
            ">;)",
            "Lcom/google/r/b/a/agn;"
        }
    .end annotation

    .prologue
    .line 2168
    invoke-direct {p0}, Lcom/google/r/b/a/agn;->c()V

    .line 2169
    iget-object v0, p0, Lcom/google/r/b/a/agn;->d:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/n/b;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 2172
    return-object p0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1871
    iget v0, p0, Lcom/google/r/b/a/agn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 1872
    iget-object v0, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/r/b/a/afn;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1877
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 1871
    goto :goto_0

    .line 1872
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1877
    goto :goto_2
.end method
