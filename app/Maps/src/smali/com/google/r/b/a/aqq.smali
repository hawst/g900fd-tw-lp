.class public final Lcom/google/r/b/a/aqq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aqt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aqq;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/r/b/a/aqq;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Ljava/lang/Object;

.field e:Z

.field f:Z

.field g:Z

.field h:Ljava/lang/Object;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/google/r/b/a/aqr;

    invoke-direct {v0}, Lcom/google/r/b/a/aqr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aqq;->PARSER:Lcom/google/n/ax;

    .line 358
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aqq;->l:Lcom/google/n/aw;

    .line 888
    new-instance v0, Lcom/google/r/b/a/aqq;

    invoke-direct {v0}, Lcom/google/r/b/a/aqq;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aqq;->i:Lcom/google/r/b/a/aqq;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 115
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->b:Lcom/google/n/ao;

    .line 274
    iput-byte v3, p0, Lcom/google/r/b/a/aqq;->j:B

    .line 317
    iput v3, p0, Lcom/google/r/b/a/aqq;->k:I

    .line 18
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput v2, p0, Lcom/google/r/b/a/aqq;->c:I

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->d:Ljava/lang/Object;

    .line 21
    iput-boolean v2, p0, Lcom/google/r/b/a/aqq;->e:Z

    .line 22
    iput-boolean v2, p0, Lcom/google/r/b/a/aqq;->f:Z

    .line 23
    iput-boolean v2, p0, Lcom/google/r/b/a/aqq;->g:Z

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->h:Ljava/lang/Object;

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/r/b/a/aqq;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 37
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 39
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 46
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 52
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/aqq;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aqq;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/aqq;->a:I

    .line 57
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aqq;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    .line 92
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 93
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 62
    iget v5, p0, Lcom/google/r/b/a/aqq;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/aqq;->a:I

    .line 63
    iput-object v0, p0, Lcom/google/r/b/a/aqq;->d:Ljava/lang/Object;

    goto :goto_0

    .line 67
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/aqq;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/aqq;->e:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 72
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/aqq;->a:I

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/aqq;->f:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 77
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/aqq;->a:I

    .line 78
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/r/b/a/aqq;->g:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 82
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 83
    iget v5, p0, Lcom/google/r/b/a/aqq;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/r/b/a/aqq;->a:I

    .line 84
    iput-object v0, p0, Lcom/google/r/b/a/aqq;->h:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 95
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->au:Lcom/google/n/bn;

    .line 96
    return-void

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 115
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->b:Lcom/google/n/ao;

    .line 274
    iput-byte v1, p0, Lcom/google/r/b/a/aqq;->j:B

    .line 317
    iput v1, p0, Lcom/google/r/b/a/aqq;->k:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aqq;
    .locals 1

    .prologue
    .line 891
    sget-object v0, Lcom/google/r/b/a/aqq;->i:Lcom/google/r/b/a/aqq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aqs;
    .locals 1

    .prologue
    .line 420
    new-instance v0, Lcom/google/r/b/a/aqs;

    invoke-direct {v0}, Lcom/google/r/b/a/aqs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aqq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    sget-object v0, Lcom/google/r/b/a/aqq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 292
    invoke-virtual {p0}, Lcom/google/r/b/a/aqq;->c()I

    .line 293
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 296
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 297
    iget v0, p0, Lcom/google/r/b/a/aqq;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 299
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 300
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/aqq;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->d:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 302
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 303
    iget-boolean v0, p0, Lcom/google/r/b/a/aqq;->e:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 305
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 306
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/aqq;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 308
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 309
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/r/b/a/aqq;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 311
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 312
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/aqq;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->h:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 314
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 315
    return-void

    .line 300
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 312
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 276
    iget-byte v0, p0, Lcom/google/r/b/a/aqq;->j:B

    .line 277
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 287
    :goto_0
    return v0

    .line 278
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 280
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 281
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 282
    iput-byte v2, p0, Lcom/google/r/b/a/aqq;->j:B

    move v0, v2

    .line 283
    goto :goto_0

    :cond_2
    move v0, v2

    .line 280
    goto :goto_1

    .line 286
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/aqq;->j:B

    move v0, v1

    .line 287
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 319
    iget v0, p0, Lcom/google/r/b/a/aqq;->k:I

    .line 320
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 353
    :goto_0
    return v0

    .line 323
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 324
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->b:Lcom/google/n/ao;

    .line 325
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 327
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_9

    .line 328
    iget v2, p0, Lcom/google/r/b/a/aqq;->c:I

    .line 329
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_6

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 331
    :goto_3
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 332
    const/4 v3, 0x3

    .line 333
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 335
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 336
    iget-boolean v0, p0, Lcom/google/r/b/a/aqq;->e:Z

    .line 337
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 339
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 340
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/r/b/a/aqq;->f:Z

    .line 341
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 343
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 344
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/r/b/a/aqq;->g:Z

    .line 345
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 347
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aqq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 348
    const/4 v3, 0x7

    .line 349
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqq;->h:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 351
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/aqq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 352
    iput v0, p0, Lcom/google/r/b/a/aqq;->k:I

    goto/16 :goto_0

    .line 329
    :cond_6
    const/16 v2, 0xa

    goto/16 :goto_2

    .line 333
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 349
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_9
    move v2, v0

    goto/16 :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/aqq;->newBuilder()Lcom/google/r/b/a/aqs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aqs;->a(Lcom/google/r/b/a/aqq;)Lcom/google/r/b/a/aqs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/aqq;->newBuilder()Lcom/google/r/b/a/aqs;

    move-result-object v0

    return-object v0
.end method
