.class public final Lcom/google/r/b/a/wt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ww;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/wt;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/wt;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/r/b/a/wu;

    invoke-direct {v0}, Lcom/google/r/b/a/wu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/wt;->PARSER:Lcom/google/n/ax;

    .line 211
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/wt;->i:Lcom/google/n/aw;

    .line 490
    new-instance v0, Lcom/google/r/b/a/wt;

    invoke-direct {v0}, Lcom/google/r/b/a/wt;-><init>()V

    sput-object v0, Lcom/google/r/b/a/wt;->f:Lcom/google/r/b/a/wt;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 154
    iput-byte v0, p0, Lcom/google/r/b/a/wt;->g:B

    .line 182
    iput v0, p0, Lcom/google/r/b/a/wt;->h:I

    .line 18
    const v0, 0x2bf20

    iput v0, p0, Lcom/google/r/b/a/wt;->b:I

    .line 19
    iput v1, p0, Lcom/google/r/b/a/wt;->c:I

    .line 20
    const/16 v0, 0x32

    iput v0, p0, Lcom/google/r/b/a/wt;->d:I

    .line 21
    iput v1, p0, Lcom/google/r/b/a/wt;->e:I

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/r/b/a/wt;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 33
    const/4 v0, 0x0

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/wt;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/wt;->a:I

    .line 49
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/wt;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/wt;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/wt;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/wt;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/wt;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 72
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 73
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/wt;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/wt;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/wt;->d:I

    goto :goto_0

    .line 63
    :sswitch_4
    iget v3, p0, Lcom/google/r/b/a/wt;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/wt;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/wt;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wt;->au:Lcom/google/n/bn;

    .line 76
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 154
    iput-byte v0, p0, Lcom/google/r/b/a/wt;->g:B

    .line 182
    iput v0, p0, Lcom/google/r/b/a/wt;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/wt;
    .locals 1

    .prologue
    .line 493
    sget-object v0, Lcom/google/r/b/a/wt;->f:Lcom/google/r/b/a/wt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/wv;
    .locals 1

    .prologue
    .line 273
    new-instance v0, Lcom/google/r/b/a/wv;

    invoke-direct {v0}, Lcom/google/r/b/a/wv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/wt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/google/r/b/a/wt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 166
    invoke-virtual {p0}, Lcom/google/r/b/a/wt;->c()I

    .line 167
    iget v0, p0, Lcom/google/r/b/a/wt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 168
    iget v0, p0, Lcom/google/r/b/a/wt;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 170
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/wt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 171
    iget v0, p0, Lcom/google/r/b/a/wt;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 173
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/wt;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 174
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/wt;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 176
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/wt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 177
    iget v0, p0, Lcom/google/r/b/a/wt;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/wt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 180
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 156
    iget-byte v1, p0, Lcom/google/r/b/a/wt;->g:B

    .line 157
    if-ne v1, v0, :cond_0

    .line 161
    :goto_0
    return v0

    .line 158
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 160
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/wt;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 184
    iget v0, p0, Lcom/google/r/b/a/wt;->h:I

    .line 185
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 206
    :goto_0
    return v0

    .line 188
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/wt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 189
    iget v0, p0, Lcom/google/r/b/a/wt;->b:I

    .line 190
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 192
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/wt;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 193
    iget v3, p0, Lcom/google/r/b/a/wt;->c:I

    .line 194
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 196
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/wt;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 197
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/wt;->d:I

    .line 198
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 200
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/wt;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_4

    .line 201
    iget v3, p0, Lcom/google/r/b/a/wt;->e:I

    .line 202
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 204
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/wt;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    iput v0, p0, Lcom/google/r/b/a/wt;->h:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 190
    goto :goto_1

    :cond_6
    move v3, v1

    .line 194
    goto :goto_3

    :cond_7
    move v3, v1

    .line 198
    goto :goto_4

    :cond_8
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/wt;->newBuilder()Lcom/google/r/b/a/wv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/wv;->a(Lcom/google/r/b/a/wt;)Lcom/google/r/b/a/wv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/wt;->newBuilder()Lcom/google/r/b/a/wv;

    move-result-object v0

    return-object v0
.end method
