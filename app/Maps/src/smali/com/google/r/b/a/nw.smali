.class public final Lcom/google/r/b/a/nw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/nw;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/nw;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/google/r/b/a/nx;

    invoke-direct {v0}, Lcom/google/r/b/a/nx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/nw;->PARSER:Lcom/google/n/ax;

    .line 159
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/nw;->g:Lcom/google/n/aw;

    .line 360
    new-instance v0, Lcom/google/r/b/a/nw;

    invoke-direct {v0}, Lcom/google/r/b/a/nw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/nw;->d:Lcom/google/r/b/a/nw;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 112
    iput-byte v0, p0, Lcom/google/r/b/a/nw;->e:B

    .line 138
    iput v0, p0, Lcom/google/r/b/a/nw;->f:I

    .line 18
    iput v1, p0, Lcom/google/r/b/a/nw;->b:I

    .line 19
    iput v1, p0, Lcom/google/r/b/a/nw;->c:I

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/r/b/a/nw;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 31
    const/4 v0, 0x0

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 34
    sparse-switch v3, :sswitch_data_0

    .line 39
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/nw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/nw;->a:I

    .line 47
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/nw;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/nw;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/nw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/nw;->a:I

    .line 52
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/nw;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 59
    :catch_1
    move-exception v0

    .line 60
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 61
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nw;->au:Lcom/google/n/bn;

    .line 64
    return-void

    .line 34
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 112
    iput-byte v0, p0, Lcom/google/r/b/a/nw;->e:B

    .line 138
    iput v0, p0, Lcom/google/r/b/a/nw;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/nw;
    .locals 1

    .prologue
    .line 363
    sget-object v0, Lcom/google/r/b/a/nw;->d:Lcom/google/r/b/a/nw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ny;
    .locals 1

    .prologue
    .line 221
    new-instance v0, Lcom/google/r/b/a/ny;

    invoke-direct {v0}, Lcom/google/r/b/a/ny;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/nw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/google/r/b/a/nw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 128
    invoke-virtual {p0}, Lcom/google/r/b/a/nw;->c()I

    .line 129
    iget v0, p0, Lcom/google/r/b/a/nw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 130
    iget v0, p0, Lcom/google/r/b/a/nw;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 132
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/nw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 133
    iget v0, p0, Lcom/google/r/b/a/nw;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/nw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 136
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 114
    iget-byte v2, p0, Lcom/google/r/b/a/nw;->e:B

    .line 115
    if-ne v2, v0, :cond_0

    .line 123
    :goto_0
    return v0

    .line 116
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 118
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/nw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 119
    iput-byte v1, p0, Lcom/google/r/b/a/nw;->e:B

    move v0, v1

    .line 120
    goto :goto_0

    :cond_2
    move v2, v1

    .line 118
    goto :goto_1

    .line 122
    :cond_3
    iput-byte v0, p0, Lcom/google/r/b/a/nw;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 140
    iget v0, p0, Lcom/google/r/b/a/nw;->f:I

    .line 141
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 154
    :goto_0
    return v0

    .line 144
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/nw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 145
    iget v0, p0, Lcom/google/r/b/a/nw;->b:I

    .line 146
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 148
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/nw;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 149
    iget v3, p0, Lcom/google/r/b/a/nw;->c:I

    .line 150
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 152
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/nw;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    iput v0, p0, Lcom/google/r/b/a/nw;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 146
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/nw;->newBuilder()Lcom/google/r/b/a/ny;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ny;->a(Lcom/google/r/b/a/nw;)Lcom/google/r/b/a/ny;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/nw;->newBuilder()Lcom/google/r/b/a/ny;

    move-result-object v0

    return-object v0
.end method
