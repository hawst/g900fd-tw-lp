.class public final Lcom/google/r/b/a/yf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/yg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/yd;",
        "Lcom/google/r/b/a/yf;",
        ">;",
        "Lcom/google/r/b/a/yg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3816
    sget-object v0, Lcom/google/r/b/a/yd;->h:Lcom/google/r/b/a/yd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3943
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yf;->c:Ljava/lang/Object;

    .line 4019
    const v0, -0x7f02078e

    iput v0, p0, Lcom/google/r/b/a/yf;->d:I

    .line 4051
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yf;->e:Ljava/lang/Object;

    .line 4127
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yf;->f:Ljava/lang/Object;

    .line 4203
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/yf;->g:I

    .line 3817
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3808
    new-instance v2, Lcom/google/r/b/a/yd;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/yd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/yf;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/yf;->b:I

    iput v1, v2, Lcom/google/r/b/a/yd;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/yf;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/yf;->d:I

    iput v1, v2, Lcom/google/r/b/a/yd;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/yf;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/yf;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/yd;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/r/b/a/yf;->g:I

    iput v1, v2, Lcom/google/r/b/a/yd;->g:I

    iput v0, v2, Lcom/google/r/b/a/yd;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3808
    check-cast p1, Lcom/google/r/b/a/yd;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/yf;->a(Lcom/google/r/b/a/yd;)Lcom/google/r/b/a/yf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/yd;)Lcom/google/r/b/a/yf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3872
    invoke-static {}, Lcom/google/r/b/a/yd;->d()Lcom/google/r/b/a/yd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3898
    :goto_0
    return-object p0

    .line 3873
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 3874
    iget v2, p1, Lcom/google/r/b/a/yd;->b:I

    iget v3, p0, Lcom/google/r/b/a/yf;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/yf;->a:I

    iput v2, p0, Lcom/google/r/b/a/yf;->b:I

    .line 3876
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 3877
    iget v2, p0, Lcom/google/r/b/a/yf;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/yf;->a:I

    .line 3878
    iget-object v2, p1, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/yf;->c:Ljava/lang/Object;

    .line 3881
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 3882
    iget v2, p1, Lcom/google/r/b/a/yd;->d:I

    iget v3, p0, Lcom/google/r/b/a/yf;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/yf;->a:I

    iput v2, p0, Lcom/google/r/b/a/yf;->d:I

    .line 3884
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 3885
    iget v2, p0, Lcom/google/r/b/a/yf;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/yf;->a:I

    .line 3886
    iget-object v2, p1, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/yf;->e:Ljava/lang/Object;

    .line 3889
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 3890
    iget v2, p0, Lcom/google/r/b/a/yf;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/yf;->a:I

    .line 3891
    iget-object v2, p1, Lcom/google/r/b/a/yd;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/yf;->f:Ljava/lang/Object;

    .line 3894
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_e

    .line 3895
    iget v0, p1, Lcom/google/r/b/a/yd;->g:I

    invoke-static {v0}, Lcom/google/r/b/a/yz;->a(I)Lcom/google/r/b/a/yz;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/r/b/a/yz;->a:Lcom/google/r/b/a/yz;

    :cond_6
    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v2, v1

    .line 3873
    goto :goto_1

    :cond_8
    move v2, v1

    .line 3876
    goto :goto_2

    :cond_9
    move v2, v1

    .line 3881
    goto :goto_3

    :cond_a
    move v2, v1

    .line 3884
    goto :goto_4

    :cond_b
    move v2, v1

    .line 3889
    goto :goto_5

    :cond_c
    move v0, v1

    .line 3894
    goto :goto_6

    .line 3895
    :cond_d
    iget v1, p0, Lcom/google/r/b/a/yf;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/r/b/a/yf;->a:I

    iget v0, v0, Lcom/google/r/b/a/yz;->c:I

    iput v0, p0, Lcom/google/r/b/a/yf;->g:I

    .line 3897
    :cond_e
    iget-object v0, p1, Lcom/google/r/b/a/yd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3902
    iget v2, p0, Lcom/google/r/b/a/yf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 3906
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 3902
    goto :goto_0

    :cond_1
    move v0, v1

    .line 3906
    goto :goto_1
.end method
