.class public final Lcom/google/r/b/a/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/a/b;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:J

.field public d:J

.field public e:Lcom/google/n/ao;

.field public f:J

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597
    new-instance v0, Lcom/google/r/b/a/a/c;

    invoke-direct {v0}, Lcom/google/r/b/a/a/c;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/b;->PARSER:Lcom/google/n/ax;

    .line 780
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/b;->j:Lcom/google/n/aw;

    .line 1176
    new-instance v0, Lcom/google/r/b/a/a/b;

    invoke-direct {v0}, Lcom/google/r/b/a/a/b;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/b;->g:Lcom/google/r/b/a/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 529
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 686
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/b;->e:Lcom/google/n/ao;

    .line 716
    iput-byte v4, p0, Lcom/google/r/b/a/a/b;->h:B

    .line 747
    iput v4, p0, Lcom/google/r/b/a/a/b;->i:I

    .line 530
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    .line 531
    iput-wide v2, p0, Lcom/google/r/b/a/a/b;->c:J

    .line 532
    iput-wide v2, p0, Lcom/google/r/b/a/a/b;->d:J

    .line 533
    iget-object v0, p0, Lcom/google/r/b/a/a/b;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 534
    iput-wide v2, p0, Lcom/google/r/b/a/a/b;->f:J

    .line 535
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 541
    invoke-direct {p0}, Lcom/google/r/b/a/a/b;-><init>()V

    .line 542
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 547
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 548
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 549
    sparse-switch v3, :sswitch_data_0

    .line 554
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 556
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 552
    goto :goto_0

    .line 561
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 562
    iget v4, p0, Lcom/google/r/b/a/a/b;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/a/b;->a:I

    .line 563
    iput-object v3, p0, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 588
    :catch_0
    move-exception v0

    .line 589
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 594
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 567
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/a/b;->a:I

    .line 568
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/a/b;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 590
    :catch_1
    move-exception v0

    .line 591
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 592
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 572
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/a/b;->a:I

    .line 573
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/a/b;->d:J

    goto :goto_0

    .line 577
    :sswitch_4
    iget-object v3, p0, Lcom/google/r/b/a/a/b;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 578
    iget v3, p0, Lcom/google/r/b/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/a/b;->a:I

    goto :goto_0

    .line 582
    :sswitch_5
    iget v3, p0, Lcom/google/r/b/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/a/b;->a:I

    .line 583
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/a/b;->f:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 594
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/b;->au:Lcom/google/n/bn;

    .line 595
    return-void

    .line 549
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 527
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 686
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/b;->e:Lcom/google/n/ao;

    .line 716
    iput-byte v1, p0, Lcom/google/r/b/a/a/b;->h:B

    .line 747
    iput v1, p0, Lcom/google/r/b/a/a/b;->i:I

    .line 528
    return-void
.end method

.method public static d()Lcom/google/r/b/a/a/b;
    .locals 1

    .prologue
    .line 1179
    sget-object v0, Lcom/google/r/b/a/a/b;->g:Lcom/google/r/b/a/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/d;
    .locals 1

    .prologue
    .line 842
    new-instance v0, Lcom/google/r/b/a/a/d;

    invoke-direct {v0}, Lcom/google/r/b/a/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609
    sget-object v0, Lcom/google/r/b/a/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 728
    invoke-virtual {p0}, Lcom/google/r/b/a/a/b;->c()I

    .line 729
    iget v0, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 730
    iget-object v0, p0, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 732
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 733
    iget-wide v0, p0, Lcom/google/r/b/a/a/b;->c:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 735
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 736
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/r/b/a/a/b;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 738
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 739
    iget-object v0, p0, Lcom/google/r/b/a/a/b;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 741
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 742
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/r/b/a/a/b;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 744
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 745
    return-void

    .line 730
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 718
    iget-byte v1, p0, Lcom/google/r/b/a/a/b;->h:B

    .line 719
    if-ne v1, v0, :cond_0

    .line 723
    :goto_0
    return v0

    .line 720
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 722
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/a/b;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 749
    iget v0, p0, Lcom/google/r/b/a/a/b;->i:I

    .line 750
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 775
    :goto_0
    return v0

    .line 753
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 755
    iget-object v0, p0, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 757
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 758
    iget-wide v2, p0, Lcom/google/r/b/a/a/b;->c:J

    .line 759
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 761
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 762
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/r/b/a/a/b;->d:J

    .line 763
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 765
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 766
    iget-object v2, p0, Lcom/google/r/b/a/a/b;->e:Lcom/google/n/ao;

    .line 767
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 769
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 770
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/r/b/a/a/b;->f:J

    .line 771
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 773
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/a/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 774
    iput v0, p0, Lcom/google/r/b/a/a/b;->i:I

    goto/16 :goto_0

    .line 755
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 521
    invoke-static {}, Lcom/google/r/b/a/a/b;->newBuilder()Lcom/google/r/b/a/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/d;->a(Lcom/google/r/b/a/a/b;)Lcom/google/r/b/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 521
    invoke-static {}, Lcom/google/r/b/a/a/b;->newBuilder()Lcom/google/r/b/a/a/d;

    move-result-object v0

    return-object v0
.end method
