.class public final enum Lcom/google/r/b/a/amx;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/amx;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/r/b/a/amx;

.field public static final enum B:Lcom/google/r/b/a/amx;

.field public static final enum C:Lcom/google/r/b/a/amx;

.field public static final enum D:Lcom/google/r/b/a/amx;

.field public static final enum E:Lcom/google/r/b/a/amx;

.field private static final synthetic G:[Lcom/google/r/b/a/amx;

.field public static final enum a:Lcom/google/r/b/a/amx;

.field public static final enum b:Lcom/google/r/b/a/amx;

.field public static final enum c:Lcom/google/r/b/a/amx;

.field public static final enum d:Lcom/google/r/b/a/amx;

.field public static final enum e:Lcom/google/r/b/a/amx;

.field public static final enum f:Lcom/google/r/b/a/amx;

.field public static final enum g:Lcom/google/r/b/a/amx;

.field public static final enum h:Lcom/google/r/b/a/amx;

.field public static final enum i:Lcom/google/r/b/a/amx;

.field public static final enum j:Lcom/google/r/b/a/amx;

.field public static final enum k:Lcom/google/r/b/a/amx;

.field public static final enum l:Lcom/google/r/b/a/amx;

.field public static final enum m:Lcom/google/r/b/a/amx;

.field public static final enum n:Lcom/google/r/b/a/amx;

.field public static final enum o:Lcom/google/r/b/a/amx;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum p:Lcom/google/r/b/a/amx;

.field public static final enum q:Lcom/google/r/b/a/amx;

.field public static final enum r:Lcom/google/r/b/a/amx;

.field public static final enum s:Lcom/google/r/b/a/amx;

.field public static final enum t:Lcom/google/r/b/a/amx;

.field public static final enum u:Lcom/google/r/b/a/amx;

.field public static final enum v:Lcom/google/r/b/a/amx;

.field public static final enum w:Lcom/google/r/b/a/amx;

.field public static final enum x:Lcom/google/r/b/a/amx;

.field public static final enum y:Lcom/google/r/b/a/amx;

.field public static final enum z:Lcom/google/r/b/a/amx;


# instance fields
.field final F:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 9174
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "MAP_ATLAS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->a:Lcom/google/r/b/a/amx;

    .line 9178
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "SATELLITE"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v5}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->b:Lcom/google/r/b/a/amx;

    .line 9182
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "TRAFFIC"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->c:Lcom/google/r/b/a/amx;

    .line 9186
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "GIF_ATLAS"

    invoke-direct {v0, v1, v5, v7}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->d:Lcom/google/r/b/a/amx;

    .line 9190
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "HYBRID"

    invoke-direct {v0, v1, v6, v8}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->e:Lcom/google/r/b/a/amx;

    .line 9194
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "TERRAIN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v7, v2}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->f:Lcom/google/r/b/a/amx;

    .line 9198
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "CLICKABLE_LAYER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->g:Lcom/google/r/b/a/amx;

    .line 9202
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "STREET_VIEW"

    const/4 v2, 0x7

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->h:Lcom/google/r/b/a/amx;

    .line 9206
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "VECTOR_ATLAS"

    const/16 v2, 0x8

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->i:Lcom/google/r/b/a/amx;

    .line 9210
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "ROAD_GRAPH"

    const/16 v2, 0x9

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->j:Lcom/google/r/b/a/amx;

    .line 9214
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "TERRAIN_NO_LABELS"

    const/16 v2, 0xa

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->k:Lcom/google/r/b/a/amx;

    .line 9218
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "VECTOR_TRANSIT"

    const/16 v2, 0xb

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->l:Lcom/google/r/b/a/amx;

    .line 9222
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "INDOOR"

    const/16 v2, 0xc

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->m:Lcom/google/r/b/a/amx;

    .line 9226
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "LABELS_ONLY"

    const/16 v2, 0xd

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->n:Lcom/google/r/b/a/amx;

    .line 9230
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "PERSONALIZED_SMARTMAPS"

    const/16 v2, 0xe

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->o:Lcom/google/r/b/a/amx;

    .line 9235
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "RELATED_PLACES"

    const/16 v2, 0xf

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->p:Lcom/google/r/b/a/amx;

    .line 9239
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "VECTOR_BICYCLING"

    const/16 v2, 0x10

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->q:Lcom/google/r/b/a/amx;

    .line 9243
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "USE_TILE_TYPE_SPEC"

    const/16 v2, 0x11

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->r:Lcom/google/r/b/a/amx;

    .line 9247
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "VECTOR_ATLAS_TRANSIT_STYLED"

    const/16 v2, 0x12

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->s:Lcom/google/r/b/a/amx;

    .line 9251
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "LABELS_ONLY_TRANSIT_STYLED"

    const/16 v2, 0x13

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->t:Lcom/google/r/b/a/amx;

    .line 9255
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "VECTOR_ATLAS_DRIVEABOUT"

    const/16 v2, 0x14

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->u:Lcom/google/r/b/a/amx;

    .line 9259
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "TRAFFIC_DRIVEABOUT"

    const/16 v2, 0x15

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->v:Lcom/google/r/b/a/amx;

    .line 9263
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "HIGHLIGHT"

    const/16 v2, 0x16

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->w:Lcom/google/r/b/a/amx;

    .line 9267
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "API_TILE_OVERLAY"

    const/16 v2, 0x17

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->x:Lcom/google/r/b/a/amx;

    .line 9271
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "SPOTLIGHT"

    const/16 v2, 0x18

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->y:Lcom/google/r/b/a/amx;

    .line 9275
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "MAPS_ENGINE_VECTOR"

    const/16 v2, 0x19

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->z:Lcom/google/r/b/a/amx;

    .line 9279
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "MAPS_ENGINE_IMAGE"

    const/16 v2, 0x1a

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->A:Lcom/google/r/b/a/amx;

    .line 9283
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "TRAFFIC_V2"

    const/16 v2, 0x1b

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->B:Lcom/google/r/b/a/amx;

    .line 9287
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "SPOTLIGHT_PERSONALIZED_SMARTMAPS"

    const/16 v2, 0x1c

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->C:Lcom/google/r/b/a/amx;

    .line 9291
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "TRAFFIC_CAR"

    const/16 v2, 0x1d

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->D:Lcom/google/r/b/a/amx;

    .line 9295
    new-instance v0, Lcom/google/r/b/a/amx;

    const-string v1, "VECTOR_BICYCLING_OVERLAY"

    const/16 v2, 0x1e

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amx;->E:Lcom/google/r/b/a/amx;

    .line 9169
    const/16 v0, 0x1f

    new-array v0, v0, [Lcom/google/r/b/a/amx;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/r/b/a/amx;->a:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/r/b/a/amx;->b:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/r/b/a/amx;->c:Lcom/google/r/b/a/amx;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/amx;->d:Lcom/google/r/b/a/amx;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/amx;->e:Lcom/google/r/b/a/amx;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/amx;->f:Lcom/google/r/b/a/amx;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/amx;->g:Lcom/google/r/b/a/amx;

    aput-object v1, v0, v8

    const/4 v1, 0x7

    sget-object v2, Lcom/google/r/b/a/amx;->h:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/amx;->i:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/r/b/a/amx;->j:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/r/b/a/amx;->k:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/r/b/a/amx;->l:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/r/b/a/amx;->m:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/r/b/a/amx;->n:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/r/b/a/amx;->o:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/r/b/a/amx;->p:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/r/b/a/amx;->q:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/r/b/a/amx;->r:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/r/b/a/amx;->s:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/r/b/a/amx;->t:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/r/b/a/amx;->u:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/r/b/a/amx;->v:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/r/b/a/amx;->w:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/r/b/a/amx;->x:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/r/b/a/amx;->y:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/r/b/a/amx;->z:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/r/b/a/amx;->A:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/r/b/a/amx;->B:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/r/b/a/amx;->C:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/r/b/a/amx;->D:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/r/b/a/amx;->E:Lcom/google/r/b/a/amx;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/amx;->G:[Lcom/google/r/b/a/amx;

    .line 9470
    new-instance v0, Lcom/google/r/b/a/amy;

    invoke-direct {v0}, Lcom/google/r/b/a/amy;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 9479
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9480
    iput p3, p0, Lcom/google/r/b/a/amx;->F:I

    .line 9481
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/amx;
    .locals 1

    .prologue
    .line 9429
    packed-switch p0, :pswitch_data_0

    .line 9461
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 9430
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/amx;->a:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9431
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/amx;->b:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9432
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/amx;->c:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9433
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/amx;->d:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9434
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/amx;->e:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9435
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/amx;->f:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9436
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/amx;->g:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9437
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/amx;->h:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9438
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/amx;->i:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9439
    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/amx;->j:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9440
    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/amx;->k:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9441
    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/amx;->l:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9442
    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/amx;->m:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9443
    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/amx;->n:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9444
    :pswitch_e
    sget-object v0, Lcom/google/r/b/a/amx;->o:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9445
    :pswitch_f
    sget-object v0, Lcom/google/r/b/a/amx;->p:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9446
    :pswitch_10
    sget-object v0, Lcom/google/r/b/a/amx;->q:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9447
    :pswitch_11
    sget-object v0, Lcom/google/r/b/a/amx;->r:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9448
    :pswitch_12
    sget-object v0, Lcom/google/r/b/a/amx;->s:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9449
    :pswitch_13
    sget-object v0, Lcom/google/r/b/a/amx;->t:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9450
    :pswitch_14
    sget-object v0, Lcom/google/r/b/a/amx;->u:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9451
    :pswitch_15
    sget-object v0, Lcom/google/r/b/a/amx;->v:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9452
    :pswitch_16
    sget-object v0, Lcom/google/r/b/a/amx;->w:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9453
    :pswitch_17
    sget-object v0, Lcom/google/r/b/a/amx;->x:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9454
    :pswitch_18
    sget-object v0, Lcom/google/r/b/a/amx;->y:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9455
    :pswitch_19
    sget-object v0, Lcom/google/r/b/a/amx;->z:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9456
    :pswitch_1a
    sget-object v0, Lcom/google/r/b/a/amx;->A:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9457
    :pswitch_1b
    sget-object v0, Lcom/google/r/b/a/amx;->B:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9458
    :pswitch_1c
    sget-object v0, Lcom/google/r/b/a/amx;->C:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9459
    :pswitch_1d
    sget-object v0, Lcom/google/r/b/a/amx;->D:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9460
    :pswitch_1e
    sget-object v0, Lcom/google/r/b/a/amx;->E:Lcom/google/r/b/a/amx;

    goto :goto_0

    .line 9429
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/amx;
    .locals 1

    .prologue
    .line 9169
    const-class v0, Lcom/google/r/b/a/amx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/amx;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/amx;
    .locals 1

    .prologue
    .line 9169
    sget-object v0, Lcom/google/r/b/a/amx;->G:[Lcom/google/r/b/a/amx;

    invoke-virtual {v0}, [Lcom/google/r/b/a/amx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/amx;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 9425
    iget v0, p0, Lcom/google/r/b/a/amx;->F:I

    return v0
.end method
