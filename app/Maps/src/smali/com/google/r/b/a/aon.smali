.class public final enum Lcom/google/r/b/a/aon;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/aon;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/aon;

.field public static final enum b:Lcom/google/r/b/a/aon;

.field public static final enum c:Lcom/google/r/b/a/aon;

.field private static final synthetic e:[Lcom/google/r/b/a/aon;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 589
    new-instance v0, Lcom/google/r/b/a/aon;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/aon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aon;->a:Lcom/google/r/b/a/aon;

    .line 593
    new-instance v0, Lcom/google/r/b/a/aon;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/aon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aon;->b:Lcom/google/r/b/a/aon;

    .line 597
    new-instance v0, Lcom/google/r/b/a/aon;

    const-string v1, "BACKEND_FAILURE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/aon;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aon;->c:Lcom/google/r/b/a/aon;

    .line 584
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/aon;

    sget-object v1, Lcom/google/r/b/a/aon;->a:Lcom/google/r/b/a/aon;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/aon;->b:Lcom/google/r/b/a/aon;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/aon;->c:Lcom/google/r/b/a/aon;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/aon;->e:[Lcom/google/r/b/a/aon;

    .line 632
    new-instance v0, Lcom/google/r/b/a/aoo;

    invoke-direct {v0}, Lcom/google/r/b/a/aoo;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 641
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 642
    iput p3, p0, Lcom/google/r/b/a/aon;->d:I

    .line 643
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/aon;
    .locals 1

    .prologue
    .line 619
    packed-switch p0, :pswitch_data_0

    .line 623
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 620
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/aon;->a:Lcom/google/r/b/a/aon;

    goto :goto_0

    .line 621
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/aon;->b:Lcom/google/r/b/a/aon;

    goto :goto_0

    .line 622
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/aon;->c:Lcom/google/r/b/a/aon;

    goto :goto_0

    .line 619
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/aon;
    .locals 1

    .prologue
    .line 584
    const-class v0, Lcom/google/r/b/a/aon;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aon;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/aon;
    .locals 1

    .prologue
    .line 584
    sget-object v0, Lcom/google/r/b/a/aon;->e:[Lcom/google/r/b/a/aon;

    invoke-virtual {v0}, [Lcom/google/r/b/a/aon;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/aon;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 615
    iget v0, p0, Lcom/google/r/b/a/aon;->d:I

    return v0
.end method
