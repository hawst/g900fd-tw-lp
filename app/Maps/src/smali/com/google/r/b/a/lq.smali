.class public final Lcom/google/r/b/a/lq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/lr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/lo;",
        "Lcom/google/r/b/a/lq;",
        ">;",
        "Lcom/google/r/b/a/lr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/co;

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5150
    sget-object v0, Lcom/google/r/b/a/lo;->d:Lcom/google/r/b/a/lo;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/r/b/a/lq;->b:Lcom/google/maps/g/co;

    .line 5151
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5142
    new-instance v2, Lcom/google/r/b/a/lo;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/lo;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/lq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/lq;->b:Lcom/google/maps/g/co;

    iput-object v1, v2, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/r/b/a/lq;->c:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/lo;->c:Z

    iput v0, v2, Lcom/google/r/b/a/lo;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5142
    check-cast p1, Lcom/google/r/b/a/lo;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/lq;->a(Lcom/google/r/b/a/lo;)Lcom/google/r/b/a/lq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/lo;)Lcom/google/r/b/a/lq;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 5182
    invoke-static {}, Lcom/google/r/b/a/lo;->d()Lcom/google/r/b/a/lo;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 5190
    :goto_0
    return-object p0

    .line 5183
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/lo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 5184
    iget-object v0, p1, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/maps/g/co;->d()Lcom/google/maps/g/co;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/r/b/a/lq;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_5

    iget-object v3, p0, Lcom/google/r/b/a/lq;->b:Lcom/google/maps/g/co;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/r/b/a/lq;->b:Lcom/google/maps/g/co;

    invoke-static {}, Lcom/google/maps/g/co;->d()Lcom/google/maps/g/co;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/google/r/b/a/lq;->b:Lcom/google/maps/g/co;

    invoke-static {v3}, Lcom/google/maps/g/co;->a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/cq;->a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/cq;->c()Lcom/google/maps/g/co;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lq;->b:Lcom/google/maps/g/co;

    :goto_3
    iget v0, p0, Lcom/google/r/b/a/lq;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/lq;->a:I

    .line 5186
    :cond_1
    iget v0, p1, Lcom/google/r/b/a/lo;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 5187
    iget-boolean v0, p1, Lcom/google/r/b/a/lo;->c:Z

    iget v1, p0, Lcom/google/r/b/a/lq;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/lq;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/lq;->c:Z

    .line 5189
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/lo;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 5183
    goto :goto_1

    .line 5184
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/r/b/a/lq;->b:Lcom/google/maps/g/co;

    goto :goto_3

    :cond_6
    move v0, v2

    .line 5186
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 5194
    const/4 v0, 0x1

    return v0
.end method
