.class public final Lcom/google/r/b/a/ur;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/us;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/up;",
        "Lcom/google/r/b/a/ur;",
        ">;",
        "Lcom/google/r/b/a/us;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Z

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/google/r/b/a/up;->g:Lcom/google/r/b/a/up;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 459
    const/16 v0, 0xe

    iput v0, p0, Lcom/google/r/b/a/ur;->d:I

    .line 491
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/r/b/a/ur;->e:I

    .line 320
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 311
    new-instance v2, Lcom/google/r/b/a/up;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/up;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ur;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/ur;->b:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/up;->b:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/r/b/a/ur;->c:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/up;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/ur;->d:I

    iput v1, v2, Lcom/google/r/b/a/up;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/ur;->e:I

    iput v1, v2, Lcom/google/r/b/a/up;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/r/b/a/ur;->f:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/up;->f:Z

    iput v0, v2, Lcom/google/r/b/a/up;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 311
    check-cast p1, Lcom/google/r/b/a/up;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ur;->a(Lcom/google/r/b/a/up;)Lcom/google/r/b/a/ur;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/up;)Lcom/google/r/b/a/ur;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 369
    invoke-static {}, Lcom/google/r/b/a/up;->d()Lcom/google/r/b/a/up;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 386
    :goto_0
    return-object p0

    .line 370
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/up;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 371
    iget-boolean v2, p1, Lcom/google/r/b/a/up;->b:Z

    iget v3, p0, Lcom/google/r/b/a/ur;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ur;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/ur;->b:Z

    .line 373
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/up;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 374
    iget-boolean v2, p1, Lcom/google/r/b/a/up;->c:Z

    iget v3, p0, Lcom/google/r/b/a/ur;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ur;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/ur;->c:Z

    .line 376
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/up;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 377
    iget v2, p1, Lcom/google/r/b/a/up;->d:I

    iget v3, p0, Lcom/google/r/b/a/ur;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/ur;->a:I

    iput v2, p0, Lcom/google/r/b/a/ur;->d:I

    .line 379
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/up;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 380
    iget v2, p1, Lcom/google/r/b/a/up;->e:I

    iget v3, p0, Lcom/google/r/b/a/ur;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/ur;->a:I

    iput v2, p0, Lcom/google/r/b/a/ur;->e:I

    .line 382
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/up;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 383
    iget-boolean v0, p1, Lcom/google/r/b/a/up;->f:Z

    iget v1, p0, Lcom/google/r/b/a/ur;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/r/b/a/ur;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/ur;->f:Z

    .line 385
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/up;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 370
    goto :goto_1

    :cond_7
    move v2, v1

    .line 373
    goto :goto_2

    :cond_8
    move v2, v1

    .line 376
    goto :goto_3

    :cond_9
    move v2, v1

    .line 379
    goto :goto_4

    :cond_a
    move v0, v1

    .line 382
    goto :goto_5
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x1

    return v0
.end method
