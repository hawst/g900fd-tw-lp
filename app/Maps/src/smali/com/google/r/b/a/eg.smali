.class public final Lcom/google/r/b/a/eg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/eh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ed;",
        "Lcom/google/r/b/a/eg;",
        ">;",
        "Lcom/google/r/b/a/eh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 287
    sget-object v0, Lcom/google/r/b/a/ed;->e:Lcom/google/r/b/a/ed;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 377
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    .line 288
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 279
    new-instance v2, Lcom/google/r/b/a/ed;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ed;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/eg;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/eg;->b:I

    iput v1, v2, Lcom/google/r/b/a/ed;->b:I

    iget v1, p0, Lcom/google/r/b/a/eg;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/eg;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/eg;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/ed;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 279
    check-cast p1, Lcom/google/r/b/a/ed;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/eg;->a(Lcom/google/r/b/a/ed;)Lcom/google/r/b/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ed;)Lcom/google/r/b/a/eg;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 320
    invoke-static {}, Lcom/google/r/b/a/ed;->d()Lcom/google/r/b/a/ed;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 335
    :goto_0
    return-object p0

    .line 321
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/ed;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 322
    iget v0, p1, Lcom/google/r/b/a/ed;->b:I

    iget v1, p0, Lcom/google/r/b/a/eg;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/eg;->a:I

    iput v0, p0, Lcom/google/r/b/a/eg;->b:I

    .line 324
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 326
    iget-object v0, p1, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    .line 327
    iget v0, p0, Lcom/google/r/b/a/eg;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/eg;->a:I

    .line 334
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/ed;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 321
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 329
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/eg;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/eg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/eg;->a:I

    .line 330
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/eg;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x1

    return v0
.end method
