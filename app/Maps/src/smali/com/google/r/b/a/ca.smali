.class public final enum Lcom/google/r/b/a/ca;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ca;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ca;

.field public static final enum b:Lcom/google/r/b/a/ca;

.field public static final enum c:Lcom/google/r/b/a/ca;

.field public static final enum d:Lcom/google/r/b/a/ca;

.field public static final enum e:Lcom/google/r/b/a/ca;

.field private static final synthetic g:[Lcom/google/r/b/a/ca;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1311
    new-instance v0, Lcom/google/r/b/a/ca;

    const-string v1, "INVALID_STYLE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ca;->a:Lcom/google/r/b/a/ca;

    .line 1315
    new-instance v0, Lcom/google/r/b/a/ca;

    const-string v1, "VERTICAL_LIST"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ca;->b:Lcom/google/r/b/a/ca;

    .line 1319
    new-instance v0, Lcom/google/r/b/a/ca;

    const-string v1, "VERTICAL_LIST_NO_BACKGROUND_NO_MARGIN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ca;->c:Lcom/google/r/b/a/ca;

    .line 1323
    new-instance v0, Lcom/google/r/b/a/ca;

    const-string v1, "VERTICAL_LIST_NO_MARGIN"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ca;->d:Lcom/google/r/b/a/ca;

    .line 1327
    new-instance v0, Lcom/google/r/b/a/ca;

    const-string v1, "HORIZONTAL_LIST_SCROLLABLE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/ca;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ca;->e:Lcom/google/r/b/a/ca;

    .line 1306
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/r/b/a/ca;

    sget-object v1, Lcom/google/r/b/a/ca;->a:Lcom/google/r/b/a/ca;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/ca;->b:Lcom/google/r/b/a/ca;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/ca;->c:Lcom/google/r/b/a/ca;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/ca;->d:Lcom/google/r/b/a/ca;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/ca;->e:Lcom/google/r/b/a/ca;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/r/b/a/ca;->g:[Lcom/google/r/b/a/ca;

    .line 1372
    new-instance v0, Lcom/google/r/b/a/cb;

    invoke-direct {v0}, Lcom/google/r/b/a/cb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1382
    iput p3, p0, Lcom/google/r/b/a/ca;->f:I

    .line 1383
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ca;
    .locals 1

    .prologue
    .line 1357
    packed-switch p0, :pswitch_data_0

    .line 1363
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1358
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/ca;->a:Lcom/google/r/b/a/ca;

    goto :goto_0

    .line 1359
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ca;->b:Lcom/google/r/b/a/ca;

    goto :goto_0

    .line 1360
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/ca;->c:Lcom/google/r/b/a/ca;

    goto :goto_0

    .line 1361
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/ca;->d:Lcom/google/r/b/a/ca;

    goto :goto_0

    .line 1362
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/ca;->e:Lcom/google/r/b/a/ca;

    goto :goto_0

    .line 1357
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ca;
    .locals 1

    .prologue
    .line 1306
    const-class v0, Lcom/google/r/b/a/ca;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ca;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ca;
    .locals 1

    .prologue
    .line 1306
    sget-object v0, Lcom/google/r/b/a/ca;->g:[Lcom/google/r/b/a/ca;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ca;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ca;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1353
    iget v0, p0, Lcom/google/r/b/a/ca;->f:I

    return v0
.end method
