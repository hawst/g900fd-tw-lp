.class public final Lcom/google/r/b/a/ajg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ajj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aje;",
        "Lcom/google/r/b/a/ajg;",
        ">;",
        "Lcom/google/r/b/a/ajj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:I

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2108
    sget-object v0, Lcom/google/r/b/a/aje;->l:Lcom/google/r/b/a/aje;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2246
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ajg;->c:Ljava/lang/Object;

    .line 2322
    iput v1, p0, Lcom/google/r/b/a/ajg;->d:I

    .line 2358
    iput v1, p0, Lcom/google/r/b/a/ajg;->e:I

    .line 2394
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ajg;->f:Ljava/lang/Object;

    .line 2470
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ajg;->g:Ljava/lang/Object;

    .line 2546
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ajg;->b:Ljava/lang/Object;

    .line 2622
    iput v1, p0, Lcom/google/r/b/a/ajg;->h:I

    .line 2658
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ajg;->i:Lcom/google/n/ao;

    .line 2717
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ajg;->j:Lcom/google/n/ao;

    .line 2776
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ajg;->k:Lcom/google/n/ao;

    .line 2109
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2100
    new-instance v2, Lcom/google/r/b/a/aje;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aje;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ajg;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, p0, Lcom/google/r/b/a/ajg;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/r/b/a/ajg;->d:I

    iput v4, v2, Lcom/google/r/b/a/aje;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/r/b/a/ajg;->e:I

    iput v4, v2, Lcom/google/r/b/a/aje;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/ajg;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aje;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/ajg;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aje;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, p0, Lcom/google/r/b/a/ajg;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/r/b/a/ajg;->h:I

    iput v4, v2, Lcom/google/r/b/a/aje;->h:I

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ajg;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ajg;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ajg;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ajg;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v3, v2, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/ajg;->k:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/ajg;->k:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/aje;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2100
    check-cast p1, Lcom/google/r/b/a/aje;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ajg;->a(Lcom/google/r/b/a/aje;)Lcom/google/r/b/a/ajg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aje;)Lcom/google/r/b/a/ajg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2194
    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2237
    :goto_0
    return-object p0

    .line 2195
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2196
    iget v2, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ajg;->a:I

    .line 2197
    iget-object v2, p1, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ajg;->c:Ljava/lang/Object;

    .line 2200
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 2201
    iget v2, p1, Lcom/google/r/b/a/aje;->c:I

    invoke-static {v2}, Lcom/google/maps/g/f/i;->a(I)Lcom/google/maps/g/f/i;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/f/i;->a:Lcom/google/maps/g/f/i;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 2195
    goto :goto_1

    :cond_4
    move v2, v1

    .line 2200
    goto :goto_2

    .line 2201
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ajg;->a:I

    iget v2, v2, Lcom/google/maps/g/f/i;->q:I

    iput v2, p0, Lcom/google/r/b/a/ajg;->d:I

    .line 2203
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 2204
    iget v2, p1, Lcom/google/r/b/a/aje;->d:I

    invoke-static {v2}, Lcom/google/r/b/a/aiy;->a(I)Lcom/google/r/b/a/aiy;

    move-result-object v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/r/b/a/aiy;->a:Lcom/google/r/b/a/aiy;

    :cond_7
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v1

    .line 2203
    goto :goto_3

    .line 2204
    :cond_9
    iget v3, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/ajg;->a:I

    iget v2, v2, Lcom/google/r/b/a/aiy;->d:I

    iput v2, p0, Lcom/google/r/b/a/ajg;->e:I

    .line 2206
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 2207
    iget v2, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/ajg;->a:I

    .line 2208
    iget-object v2, p1, Lcom/google/r/b/a/aje;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ajg;->f:Ljava/lang/Object;

    .line 2211
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 2212
    iget v2, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/ajg;->a:I

    .line 2213
    iget-object v2, p1, Lcom/google/r/b/a/aje;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ajg;->g:Ljava/lang/Object;

    .line 2216
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 2217
    iget v2, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/ajg;->a:I

    .line 2218
    iget-object v2, p1, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ajg;->b:Ljava/lang/Object;

    .line 2221
    :cond_d
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_14

    .line 2222
    iget v2, p1, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v2}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v2

    if-nez v2, :cond_e

    sget-object v2, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_e
    if-nez v2, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    move v2, v1

    .line 2206
    goto :goto_4

    :cond_10
    move v2, v1

    .line 2211
    goto :goto_5

    :cond_11
    move v2, v1

    .line 2216
    goto :goto_6

    :cond_12
    move v2, v1

    .line 2221
    goto :goto_7

    .line 2222
    :cond_13
    iget v3, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/ajg;->a:I

    iget v2, v2, Lcom/google/r/b/a/ajh;->c:I

    iput v2, p0, Lcom/google/r/b/a/ajg;->h:I

    .line 2224
    :cond_14
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_8
    if-eqz v2, :cond_15

    .line 2225
    iget-object v2, p0, Lcom/google/r/b/a/ajg;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2226
    iget v2, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/ajg;->a:I

    .line 2228
    :cond_15
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_9
    if-eqz v2, :cond_16

    .line 2229
    iget-object v2, p0, Lcom/google/r/b/a/ajg;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2230
    iget v2, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/r/b/a/ajg;->a:I

    .line 2232
    :cond_16
    iget v2, p1, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1a

    :goto_a
    if-eqz v0, :cond_17

    .line 2233
    iget-object v0, p0, Lcom/google/r/b/a/ajg;->k:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2234
    iget v0, p0, Lcom/google/r/b/a/ajg;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/ajg;->a:I

    .line 2236
    :cond_17
    iget-object v0, p1, Lcom/google/r/b/a/aje;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_18
    move v2, v1

    .line 2224
    goto :goto_8

    :cond_19
    move v2, v1

    .line 2228
    goto :goto_9

    :cond_1a
    move v0, v1

    .line 2232
    goto :goto_a
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2241
    const/4 v0, 0x1

    return v0
.end method
