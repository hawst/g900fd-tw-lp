.class public final Lcom/google/r/b/a/ahy;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ahz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ahw;",
        "Lcom/google/r/b/a/ahy;",
        ">;",
        "Lcom/google/r/b/a/ahz;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lcom/google/r/b/a/ahw;->d:Lcom/google/r/b/a/ahw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 350
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ahy;->b:Ljava/lang/Object;

    .line 426
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ahy;->c:Lcom/google/n/ao;

    .line 297
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 288
    new-instance v2, Lcom/google/r/b/a/ahw;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ahw;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ahy;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, p0, Lcom/google/r/b/a/ahy;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ahw;->b:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v3, v2, Lcom/google/r/b/a/ahw;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/ahy;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/ahy;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/ahw;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 288
    check-cast p1, Lcom/google/r/b/a/ahw;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ahy;->a(Lcom/google/r/b/a/ahw;)Lcom/google/r/b/a/ahy;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ahw;)Lcom/google/r/b/a/ahy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 330
    invoke-static {}, Lcom/google/r/b/a/ahw;->d()Lcom/google/r/b/a/ahw;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 341
    :goto_0
    return-object p0

    .line 331
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ahw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 332
    iget v2, p0, Lcom/google/r/b/a/ahy;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ahy;->a:I

    .line 333
    iget-object v2, p1, Lcom/google/r/b/a/ahw;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ahy;->b:Ljava/lang/Object;

    .line 336
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ahw;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 337
    iget-object v0, p0, Lcom/google/r/b/a/ahy;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/ahw;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 338
    iget v0, p0, Lcom/google/r/b/a/ahy;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ahy;->a:I

    .line 340
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/ahw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 331
    goto :goto_1

    :cond_4
    move v0, v1

    .line 336
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 345
    const/4 v0, 0x1

    return v0
.end method
