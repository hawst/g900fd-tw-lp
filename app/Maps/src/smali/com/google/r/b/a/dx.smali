.class public final Lcom/google/r/b/a/dx;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/dy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/dv;",
        "Lcom/google/r/b/a/dx;",
        ">;",
        "Lcom/google/r/b/a/dy;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;

.field private k:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 739
    sget-object v0, Lcom/google/r/b/a/dv;->l:Lcom/google/r/b/a/dv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 880
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dx;->b:Ljava/lang/Object;

    .line 956
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dx;->c:Ljava/lang/Object;

    .line 1032
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dx;->d:Ljava/lang/Object;

    .line 1108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dx;->e:Ljava/lang/Object;

    .line 1184
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dx;->f:Ljava/lang/Object;

    .line 1260
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dx;->g:Ljava/lang/Object;

    .line 1336
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dx;->h:Ljava/lang/Object;

    .line 1412
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dx;->i:Ljava/lang/Object;

    .line 1488
    const-string v0, "https://support.google.com/gmm/?p=place_questions"

    iput-object v0, p0, Lcom/google/r/b/a/dx;->j:Ljava/lang/Object;

    .line 1564
    const-string v0, "https://support.google.com/maps/?p=ios_send_to_phone"

    iput-object v0, p0, Lcom/google/r/b/a/dx;->k:Ljava/lang/Object;

    .line 740
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 731
    new-instance v2, Lcom/google/r/b/a/dv;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/dv;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/dx;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/dx;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/dx;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/dx;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/dx;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/dx;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/dx;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/dx;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/dx;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->i:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/dx;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->j:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/dx;->k:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/dv;->k:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/dv;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 731
    check-cast p1, Lcom/google/r/b/a/dv;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/dx;->a(Lcom/google/r/b/a/dv;)Lcom/google/r/b/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/dv;)Lcom/google/r/b/a/dx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 819
    invoke-static {}, Lcom/google/r/b/a/dv;->d()Lcom/google/r/b/a/dv;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 871
    :goto_0
    return-object p0

    .line 820
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 821
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 822
    iget-object v2, p1, Lcom/google/r/b/a/dv;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->b:Ljava/lang/Object;

    .line 825
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 826
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 827
    iget-object v2, p1, Lcom/google/r/b/a/dv;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->c:Ljava/lang/Object;

    .line 830
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 831
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 832
    iget-object v2, p1, Lcom/google/r/b/a/dv;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->d:Ljava/lang/Object;

    .line 835
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 836
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 837
    iget-object v2, p1, Lcom/google/r/b/a/dv;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->e:Ljava/lang/Object;

    .line 840
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 841
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 842
    iget-object v2, p1, Lcom/google/r/b/a/dv;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->f:Ljava/lang/Object;

    .line 845
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 846
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 847
    iget-object v2, p1, Lcom/google/r/b/a/dv;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->g:Ljava/lang/Object;

    .line 850
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 851
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 852
    iget-object v2, p1, Lcom/google/r/b/a/dv;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->h:Ljava/lang/Object;

    .line 855
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 856
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 857
    iget-object v2, p1, Lcom/google/r/b/a/dv;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->i:Ljava/lang/Object;

    .line 860
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 861
    iget v2, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/r/b/a/dx;->a:I

    .line 862
    iget-object v2, p1, Lcom/google/r/b/a/dv;->j:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/dx;->j:Ljava/lang/Object;

    .line 865
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_14

    :goto_a
    if-eqz v0, :cond_a

    .line 866
    iget v0, p0, Lcom/google/r/b/a/dx;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/dx;->a:I

    .line 867
    iget-object v0, p1, Lcom/google/r/b/a/dv;->k:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/dx;->k:Ljava/lang/Object;

    .line 870
    :cond_a
    iget-object v0, p1, Lcom/google/r/b/a/dv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 820
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 825
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 830
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 835
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 840
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 845
    goto :goto_6

    :cond_11
    move v2, v1

    .line 850
    goto :goto_7

    :cond_12
    move v2, v1

    .line 855
    goto :goto_8

    :cond_13
    move v2, v1

    .line 860
    goto :goto_9

    :cond_14
    move v0, v1

    .line 865
    goto :goto_a
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 875
    const/4 v0, 0x1

    return v0
.end method
