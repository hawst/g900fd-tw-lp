.class public final Lcom/google/r/b/a/amu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ams;",
        "Lcom/google/r/b/a/amu;",
        ">;",
        "Lcom/google/r/b/a/amz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:J

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Lcom/google/n/ao;

.field private k:Ljava/lang/Object;

.field private l:Ljava/lang/Object;

.field private m:Ljava/lang/Object;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/Object;

.field private p:Ljava/lang/Object;

.field private q:Ljava/lang/Object;

.field private r:Lcom/google/n/ao;

.field private s:Ljava/lang/Object;

.field private t:Lcom/google/n/ao;

.field private u:I

.field private v:I

.field private w:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 10357
    sget-object v0, Lcom/google/r/b/a/ams;->x:Lcom/google/r/b/a/ams;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 10767
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/amu;->f:I

    .line 10835
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/r/b/a/amu;->h:I

    .line 10899
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amu;->j:Lcom/google/n/ao;

    .line 10958
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amu;->k:Ljava/lang/Object;

    .line 11034
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amu;->l:Ljava/lang/Object;

    .line 11110
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amu;->m:Ljava/lang/Object;

    .line 11186
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    .line 11252
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amu;->o:Ljava/lang/Object;

    .line 11328
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amu;->p:Ljava/lang/Object;

    .line 11404
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amu;->q:Ljava/lang/Object;

    .line 11480
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amu;->r:Lcom/google/n/ao;

    .line 11539
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amu;->s:Ljava/lang/Object;

    .line 11615
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amu;->t:Lcom/google/n/ao;

    .line 11706
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/amu;->v:I

    .line 10358
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 11

    .prologue
    const/high16 v10, 0x40000

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 10349
    new-instance v2, Lcom/google/r/b/a/ams;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ams;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/lit8 v0, v3, 0x1

    const/4 v4, 0x1

    if-ne v0, v4, :cond_15

    const/4 v0, 0x1

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/amu;->b:I

    iput v4, v2, Lcom/google/r/b/a/ams;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/r/b/a/amu;->c:I

    iput v4, v2, Lcom/google/r/b/a/ams;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/r/b/a/amu;->d:I

    iput v4, v2, Lcom/google/r/b/a/ams;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/r/b/a/amu;->e:J

    iput-wide v4, v2, Lcom/google/r/b/a/ams;->e:J

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/r/b/a/amu;->f:I

    iput v4, v2, Lcom/google/r/b/a/ams;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/r/b/a/amu;->g:I

    iput v4, v2, Lcom/google/r/b/a/ams;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/r/b/a/amu;->h:I

    iput v4, v2, Lcom/google/r/b/a/ams;->h:I

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v4, p0, Lcom/google/r/b/a/amu;->i:I

    iput v4, v2, Lcom/google/r/b/a/ams;->i:I

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/r/b/a/ams;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amu;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amu;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v4, p0, Lcom/google/r/b/a/amu;->k:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ams;->k:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v4, p0, Lcom/google/r/b/a/amu;->l:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ams;->l:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-object v4, p0, Lcom/google/r/b/a/amu;->m:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ams;->m:Ljava/lang/Object;

    iget v4, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/lit16 v4, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    iget-object v4, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/lit16 v4, v4, -0x1001

    iput v4, p0, Lcom/google/r/b/a/amu;->a:I

    :cond_b
    iget-object v4, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x1000

    :cond_c
    iget-object v4, p0, Lcom/google/r/b/a/amu;->o:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ams;->o:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x2000

    :cond_d
    iget-object v4, p0, Lcom/google/r/b/a/amu;->p:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ams;->p:Ljava/lang/Object;

    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    or-int/lit16 v0, v0, 0x4000

    :cond_e
    iget-object v4, p0, Lcom/google/r/b/a/amu;->q:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ams;->q:Ljava/lang/Object;

    and-int v4, v3, v8

    if-ne v4, v8, :cond_f

    or-int/2addr v0, v7

    :cond_f
    iget-object v4, v2, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amu;->r:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amu;->r:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int v4, v3, v9

    if-ne v4, v9, :cond_10

    or-int/2addr v0, v8

    :cond_10
    iget-object v4, p0, Lcom/google/r/b/a/amu;->s:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ams;->s:Ljava/lang/Object;

    and-int v4, v3, v10

    if-ne v4, v10, :cond_11

    or-int/2addr v0, v9

    :cond_11
    iget-object v4, v2, Lcom/google/r/b/a/ams;->t:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amu;->t:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amu;->t:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v1, 0x80000

    and-int/2addr v1, v3

    const/high16 v4, 0x80000

    if-ne v1, v4, :cond_12

    or-int/2addr v0, v10

    :cond_12
    iget v1, p0, Lcom/google/r/b/a/amu;->u:I

    iput v1, v2, Lcom/google/r/b/a/ams;->u:I

    const/high16 v1, 0x100000

    and-int/2addr v1, v3

    const/high16 v4, 0x100000

    if-ne v1, v4, :cond_13

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    :cond_13
    iget v1, p0, Lcom/google/r/b/a/amu;->v:I

    iput v1, v2, Lcom/google/r/b/a/ams;->v:I

    const/high16 v1, 0x200000

    and-int/2addr v1, v3

    const/high16 v3, 0x200000

    if-ne v1, v3, :cond_14

    const/high16 v1, 0x100000

    or-int/2addr v0, v1

    :cond_14
    iget v1, p0, Lcom/google/r/b/a/amu;->w:I

    iput v1, v2, Lcom/google/r/b/a/ams;->w:I

    iput v0, v2, Lcom/google/r/b/a/ams;->a:I

    return-object v2

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 10349
    check-cast p1, Lcom/google/r/b/a/ams;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/amu;->a(Lcom/google/r/b/a/ams;)Lcom/google/r/b/a/amu;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ams;)Lcom/google/r/b/a/amu;
    .locals 8

    .prologue
    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 10516
    invoke-static {}, Lcom/google/r/b/a/ams;->d()Lcom/google/r/b/a/ams;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 10608
    :goto_0
    return-object p0

    .line 10517
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 10518
    iget v2, p1, Lcom/google/r/b/a/ams;->b:I

    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iput v2, p0, Lcom/google/r/b/a/amu;->b:I

    .line 10520
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 10521
    iget v2, p1, Lcom/google/r/b/a/ams;->c:I

    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iput v2, p0, Lcom/google/r/b/a/amu;->c:I

    .line 10523
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 10524
    iget v2, p1, Lcom/google/r/b/a/ams;->d:I

    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iput v2, p0, Lcom/google/r/b/a/amu;->d:I

    .line 10526
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 10527
    iget-wide v2, p1, Lcom/google/r/b/a/ams;->e:J

    iget v4, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/amu;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/amu;->e:J

    .line 10529
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 10530
    iget v2, p1, Lcom/google/r/b/a/ams;->f:I

    invoke-static {v2}, Lcom/google/r/b/a/amv;->a(I)Lcom/google/r/b/a/amv;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/r/b/a/amv;->a:Lcom/google/r/b/a/amv;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 10517
    goto :goto_1

    :cond_7
    move v2, v1

    .line 10520
    goto :goto_2

    :cond_8
    move v2, v1

    .line 10523
    goto :goto_3

    :cond_9
    move v2, v1

    .line 10526
    goto :goto_4

    :cond_a
    move v2, v1

    .line 10529
    goto :goto_5

    .line 10530
    :cond_b
    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iget v2, v2, Lcom/google/r/b/a/amv;->d:I

    iput v2, p0, Lcom/google/r/b/a/amu;->f:I

    .line 10532
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 10533
    iget v2, p1, Lcom/google/r/b/a/ams;->g:I

    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iput v2, p0, Lcom/google/r/b/a/amu;->g:I

    .line 10535
    :cond_d
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_7
    if-eqz v2, :cond_e

    .line 10536
    iget v2, p1, Lcom/google/r/b/a/ams;->h:I

    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iput v2, p0, Lcom/google/r/b/a/amu;->h:I

    .line 10538
    :cond_e
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_8
    if-eqz v2, :cond_f

    .line 10539
    iget v2, p1, Lcom/google/r/b/a/ams;->i:I

    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iput v2, p0, Lcom/google/r/b/a/amu;->i:I

    .line 10541
    :cond_f
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_9
    if-eqz v2, :cond_10

    .line 10542
    iget-object v2, p0, Lcom/google/r/b/a/amu;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ams;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 10543
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10545
    :cond_10
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_a
    if-eqz v2, :cond_11

    .line 10546
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10547
    iget-object v2, p1, Lcom/google/r/b/a/ams;->k:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/amu;->k:Ljava/lang/Object;

    .line 10550
    :cond_11
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_b
    if-eqz v2, :cond_12

    .line 10551
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10552
    iget-object v2, p1, Lcom/google/r/b/a/ams;->l:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/amu;->l:Ljava/lang/Object;

    .line 10555
    :cond_12
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_23

    move v2, v0

    :goto_c
    if-eqz v2, :cond_13

    .line 10556
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10557
    iget-object v2, p1, Lcom/google/r/b/a/ams;->m:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/amu;->m:Ljava/lang/Object;

    .line 10560
    :cond_13
    iget-object v2, p1, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    .line 10561
    iget-object v2, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 10562
    iget-object v2, p1, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    .line 10563
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/lit16 v2, v2, -0x1001

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10570
    :cond_14
    :goto_d
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_26

    move v2, v0

    :goto_e
    if-eqz v2, :cond_15

    .line 10571
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10572
    iget-object v2, p1, Lcom/google/r/b/a/ams;->o:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/amu;->o:Ljava/lang/Object;

    .line 10575
    :cond_15
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_27

    move v2, v0

    :goto_f
    if-eqz v2, :cond_16

    .line 10576
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10577
    iget-object v2, p1, Lcom/google/r/b/a/ams;->p:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/amu;->p:Ljava/lang/Object;

    .line 10580
    :cond_16
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_28

    move v2, v0

    :goto_10
    if-eqz v2, :cond_17

    .line 10581
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/2addr v2, v5

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10582
    iget-object v2, p1, Lcom/google/r/b/a/ams;->q:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/amu;->q:Ljava/lang/Object;

    .line 10585
    :cond_17
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_29

    move v2, v0

    :goto_11
    if-eqz v2, :cond_18

    .line 10586
    iget-object v2, p0, Lcom/google/r/b/a/amu;->r:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 10587
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/2addr v2, v6

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10589
    :cond_18
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_2a

    move v2, v0

    :goto_12
    if-eqz v2, :cond_19

    .line 10590
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/2addr v2, v7

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10591
    iget-object v2, p1, Lcom/google/r/b/a/ams;->s:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/amu;->s:Ljava/lang/Object;

    .line 10594
    :cond_19
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_2b

    move v2, v0

    :goto_13
    if-eqz v2, :cond_1a

    .line 10595
    iget-object v2, p0, Lcom/google/r/b/a/amu;->t:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ams;->t:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 10596
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10598
    :cond_1a
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_2c

    move v2, v0

    :goto_14
    if-eqz v2, :cond_1b

    .line 10599
    iget v2, p1, Lcom/google/r/b/a/ams;->u:I

    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    const/high16 v4, 0x80000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iput v2, p0, Lcom/google/r/b/a/amu;->u:I

    .line 10601
    :cond_1b
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_2d

    move v2, v0

    :goto_15
    if-eqz v2, :cond_2f

    .line 10602
    iget v2, p1, Lcom/google/r/b/a/ams;->v:I

    invoke-static {v2}, Lcom/google/r/b/a/amx;->a(I)Lcom/google/r/b/a/amx;

    move-result-object v2

    if-nez v2, :cond_1c

    sget-object v2, Lcom/google/r/b/a/amx;->a:Lcom/google/r/b/a/amx;

    :cond_1c
    if-nez v2, :cond_2e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1d
    move v2, v1

    .line 10532
    goto/16 :goto_6

    :cond_1e
    move v2, v1

    .line 10535
    goto/16 :goto_7

    :cond_1f
    move v2, v1

    .line 10538
    goto/16 :goto_8

    :cond_20
    move v2, v1

    .line 10541
    goto/16 :goto_9

    :cond_21
    move v2, v1

    .line 10545
    goto/16 :goto_a

    :cond_22
    move v2, v1

    .line 10550
    goto/16 :goto_b

    :cond_23
    move v2, v1

    .line 10555
    goto/16 :goto_c

    .line 10565
    :cond_24
    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-eq v2, v3, :cond_25

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/amu;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/r/b/a/amu;->a:I

    .line 10566
    :cond_25
    iget-object v2, p0, Lcom/google/r/b/a/amu;->n:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_d

    :cond_26
    move v2, v1

    .line 10570
    goto/16 :goto_e

    :cond_27
    move v2, v1

    .line 10575
    goto/16 :goto_f

    :cond_28
    move v2, v1

    .line 10580
    goto/16 :goto_10

    :cond_29
    move v2, v1

    .line 10585
    goto/16 :goto_11

    :cond_2a
    move v2, v1

    .line 10589
    goto/16 :goto_12

    :cond_2b
    move v2, v1

    .line 10594
    goto/16 :goto_13

    :cond_2c
    move v2, v1

    .line 10598
    goto :goto_14

    :cond_2d
    move v2, v1

    .line 10601
    goto :goto_15

    .line 10602
    :cond_2e
    iget v3, p0, Lcom/google/r/b/a/amu;->a:I

    const/high16 v4, 0x100000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/amu;->a:I

    iget v2, v2, Lcom/google/r/b/a/amx;->F:I

    iput v2, p0, Lcom/google/r/b/a/amu;->v:I

    .line 10604
    :cond_2f
    iget v2, p1, Lcom/google/r/b/a/ams;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_31

    :goto_16
    if-eqz v0, :cond_30

    .line 10605
    iget v0, p1, Lcom/google/r/b/a/ams;->w:I

    iget v1, p0, Lcom/google/r/b/a/amu;->a:I

    const/high16 v2, 0x200000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/r/b/a/amu;->a:I

    iput v0, p0, Lcom/google/r/b/a/amu;->w:I

    .line 10607
    :cond_30
    iget-object v0, p1, Lcom/google/r/b/a/ams;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_31
    move v0, v1

    .line 10604
    goto :goto_16
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/high16 v5, 0x100000

    const/high16 v4, 0x10000

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 10612
    iget v0, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 10634
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 10612
    goto :goto_0

    .line 10616
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-nez v0, :cond_3

    move v0, v1

    .line 10618
    goto :goto_1

    :cond_2
    move v0, v1

    .line 10616
    goto :goto_2

    .line 10620
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    move v0, v1

    .line 10622
    goto :goto_1

    :cond_4
    move v0, v1

    .line 10620
    goto :goto_3

    .line 10624
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_6

    move v0, v2

    :goto_4
    if-nez v0, :cond_7

    move v0, v1

    .line 10626
    goto :goto_1

    :cond_6
    move v0, v1

    .line 10624
    goto :goto_4

    .line 10628
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/amu;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_8

    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    .line 10629
    iget-object v0, p0, Lcom/google/r/b/a/amu;->r:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/e;->d()Lcom/google/maps/f/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/e;

    invoke-virtual {v0}, Lcom/google/maps/f/e;->b()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 10631
    goto :goto_1

    :cond_8
    move v0, v1

    .line 10628
    goto :goto_5

    :cond_9
    move v0, v2

    .line 10634
    goto :goto_1
.end method
