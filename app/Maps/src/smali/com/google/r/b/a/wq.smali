.class public final enum Lcom/google/r/b/a/wq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/wq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/wq;

.field public static final enum b:Lcom/google/r/b/a/wq;

.field public static final enum c:Lcom/google/r/b/a/wq;

.field public static final enum d:Lcom/google/r/b/a/wq;

.field private static final synthetic f:[Lcom/google/r/b/a/wq;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 132
    new-instance v0, Lcom/google/r/b/a/wq;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/wq;->a:Lcom/google/r/b/a/wq;

    .line 136
    new-instance v0, Lcom/google/r/b/a/wq;

    const-string v1, "HIGHLIGHT"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/wq;->b:Lcom/google/r/b/a/wq;

    .line 140
    new-instance v0, Lcom/google/r/b/a/wq;

    const-string v1, "DARK"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/wq;->c:Lcom/google/r/b/a/wq;

    .line 144
    new-instance v0, Lcom/google/r/b/a/wq;

    const-string v1, "THICK"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/wq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/wq;->d:Lcom/google/r/b/a/wq;

    .line 127
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/wq;

    sget-object v1, Lcom/google/r/b/a/wq;->a:Lcom/google/r/b/a/wq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/wq;->b:Lcom/google/r/b/a/wq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/wq;->c:Lcom/google/r/b/a/wq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/wq;->d:Lcom/google/r/b/a/wq;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/wq;->f:[Lcom/google/r/b/a/wq;

    .line 184
    new-instance v0, Lcom/google/r/b/a/wr;

    invoke-direct {v0}, Lcom/google/r/b/a/wr;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 194
    iput p3, p0, Lcom/google/r/b/a/wq;->e:I

    .line 195
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/wq;
    .locals 1

    .prologue
    .line 170
    packed-switch p0, :pswitch_data_0

    .line 175
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 171
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/wq;->a:Lcom/google/r/b/a/wq;

    goto :goto_0

    .line 172
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/wq;->b:Lcom/google/r/b/a/wq;

    goto :goto_0

    .line 173
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/wq;->c:Lcom/google/r/b/a/wq;

    goto :goto_0

    .line 174
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/wq;->d:Lcom/google/r/b/a/wq;

    goto :goto_0

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/wq;
    .locals 1

    .prologue
    .line 127
    const-class v0, Lcom/google/r/b/a/wq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/wq;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/wq;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/google/r/b/a/wq;->f:[Lcom/google/r/b/a/wq;

    invoke-virtual {v0}, [Lcom/google/r/b/a/wq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/wq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/r/b/a/wq;->e:I

    return v0
.end method
