.class public final Lcom/google/r/b/a/alm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aln;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/alh;",
        "Lcom/google/r/b/a/alm;",
        ">;",
        "Lcom/google/r/b/a/aln;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public e:Lcom/google/n/aq;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/n/ao;

.field private h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:I

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 13349
    sget-object v0, Lcom/google/r/b/a/alh;->o:Lcom/google/r/b/a/alh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 13512
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    .line 13585
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alm;->g:Lcom/google/n/ao;

    .line 13644
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/alm;->h:I

    .line 13681
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    .line 13754
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/alm;->j:I

    .line 13790
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alm;->k:Lcom/google/n/ao;

    .line 13849
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alm;->b:Lcom/google/n/ao;

    .line 13909
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    .line 14014
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    .line 13350
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 13514
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 13515
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    .line 13516
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13518
    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 13683
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 13684
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    .line 13685
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13687
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 13341
    new-instance v2, Lcom/google/r/b/a/alh;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/alh;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/alm;->a:I

    iget v4, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/alm;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/alm;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/alm;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v4, p0, Lcom/google/r/b/a/alm;->h:I

    iput v4, v2, Lcom/google/r/b/a/alh;->e:I

    iget v4, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/r/b/a/alm;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget v4, p0, Lcom/google/r/b/a/alm;->j:I

    iput v4, v2, Lcom/google/r/b/a/alh;->h:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/alm;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/alm;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/alm;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/alm;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/r/b/a/alm;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit8 v0, v0, 0x20

    :cond_7
    iget v1, p0, Lcom/google/r/b/a/alm;->d:I

    iput v1, v2, Lcom/google/r/b/a/alh;->m:I

    iget v1, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    iget-object v1, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/r/b/a/alm;->a:I

    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    iput v0, v2, Lcom/google/r/b/a/alh;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 13341
    check-cast p1, Lcom/google/r/b/a/alh;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/alm;->a(Lcom/google/r/b/a/alh;)Lcom/google/r/b/a/alm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/maps/g/gn;)Lcom/google/r/b/a/alm;
    .locals 2

    .prologue
    .line 13805
    if-nez p1, :cond_0

    .line 13806
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13808
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/alm;->k:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 13810
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13811
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/pz;)Lcom/google/r/b/a/alm;
    .locals 1

    .prologue
    .line 13772
    if-nez p1, :cond_0

    .line 13773
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13775
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13776
    iget v0, p1, Lcom/google/maps/g/pz;->f:I

    iput v0, p0, Lcom/google/r/b/a/alm;->j:I

    .line 13778
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/sp;)Lcom/google/r/b/a/alm;
    .locals 2

    .prologue
    .line 13600
    if-nez p1, :cond_0

    .line 13601
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13603
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/alm;->g:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 13605
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13606
    return-object p0
.end method

.method public final a(Lcom/google/r/b/a/alh;)Lcom/google/r/b/a/alm;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 13439
    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 13502
    :goto_0
    return-object p0

    .line 13440
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 13441
    iget-object v0, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 13442
    iget-object v0, p1, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    .line 13443
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13450
    :cond_1
    :goto_1
    iget v0, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 13451
    iget-object v0, p0, Lcom/google/r/b/a/alm;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13452
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13454
    :cond_2
    iget v0, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_8

    .line 13455
    iget v0, p1, Lcom/google/r/b/a/alh;->e:I

    invoke-static {v0}, Lcom/google/maps/g/pv;->a(I)Lcom/google/maps/g/pv;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/pv;->a:Lcom/google/maps/g/pv;

    :cond_3
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13445
    :cond_4
    invoke-direct {p0}, Lcom/google/r/b/a/alm;->i()V

    .line 13446
    iget-object v0, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v0, v2

    .line 13450
    goto :goto_2

    :cond_6
    move v0, v2

    .line 13454
    goto :goto_3

    .line 13455
    :cond_7
    iget v3, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/alm;->a:I

    iget v0, v0, Lcom/google/maps/g/pv;->e:I

    iput v0, p0, Lcom/google/r/b/a/alm;->h:I

    .line 13457
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 13458
    iget-object v0, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 13459
    iget-object v0, p1, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    .line 13460
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13467
    :cond_9
    :goto_4
    iget v0, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_12

    move v0, v1

    :goto_5
    if-eqz v0, :cond_b

    .line 13468
    iget v0, p1, Lcom/google/r/b/a/alh;->h:I

    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v0

    if-nez v0, :cond_a

    sget-object v0, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    :cond_a
    invoke-virtual {p0, v0}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/pz;)Lcom/google/r/b/a/alm;

    .line 13470
    :cond_b
    iget v0, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_13

    move v0, v1

    :goto_6
    if-eqz v0, :cond_c

    .line 13471
    iget-object v0, p0, Lcom/google/r/b/a/alm;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13472
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13474
    :cond_c
    iget v0, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_14

    move v0, v1

    :goto_7
    if-eqz v0, :cond_d

    .line 13475
    iget-object v0, p0, Lcom/google/r/b/a/alm;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13476
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13478
    :cond_d
    iget-object v0, p1, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 13479
    iget-object v0, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 13480
    iget-object v0, p1, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    .line 13481
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13488
    :cond_e
    :goto_8
    iget v0, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_16

    move v0, v1

    :goto_9
    if-eqz v0, :cond_f

    .line 13489
    iget v0, p1, Lcom/google/r/b/a/alh;->m:I

    iget v1, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/r/b/a/alm;->a:I

    iput v0, p0, Lcom/google/r/b/a/alm;->d:I

    .line 13491
    :cond_f
    iget-object v0, p1, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 13492
    iget-object v0, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 13493
    iget-object v0, p1, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    .line 13494
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13501
    :cond_10
    :goto_a
    iget-object v0, p1, Lcom/google/r/b/a/alh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 13462
    :cond_11
    invoke-direct {p0}, Lcom/google/r/b/a/alm;->j()V

    .line 13463
    iget-object v0, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_12
    move v0, v2

    .line 13467
    goto/16 :goto_5

    :cond_13
    move v0, v2

    .line 13470
    goto/16 :goto_6

    :cond_14
    move v0, v2

    .line 13474
    goto :goto_7

    .line 13483
    :cond_15
    invoke-virtual {p0}, Lcom/google/r/b/a/alm;->c()V

    .line 13484
    iget-object v0, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_16
    move v0, v2

    .line 13488
    goto :goto_9

    .line 13496
    :cond_17
    invoke-virtual {p0}, Lcom/google/r/b/a/alm;->d()V

    .line 13497
    iget-object v0, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_a
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/r/b/a/alm;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/maps/g/px;",
            ">;)",
            "Lcom/google/r/b/a/alm;"
        }
    .end annotation

    .prologue
    .line 13568
    invoke-direct {p0}, Lcom/google/r/b/a/alm;->i()V

    .line 13569
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/px;

    .line 13570
    iget-object v2, p0, Lcom/google/r/b/a/alm;->f:Ljava/util/List;

    iget v0, v0, Lcom/google/maps/g/px;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 13573
    :cond_0
    return-object p0
.end method

.method public final b(Ljava/lang/Iterable;)Lcom/google/r/b/a/alm;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/maps/g/pz;",
            ">;)",
            "Lcom/google/r/b/a/alm;"
        }
    .end annotation

    .prologue
    .line 13737
    invoke-direct {p0}, Lcom/google/r/b/a/alm;->j()V

    .line 13738
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pz;

    .line 13739
    iget-object v2, p0, Lcom/google/r/b/a/alm;->i:Ljava/util/List;

    iget v0, v0, Lcom/google/maps/g/pz;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 13742
    :cond_0
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 13506
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Ljava/lang/Iterable;)Lcom/google/r/b/a/alm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/r/b/a/alm;"
        }
    .end annotation

    .prologue
    .line 14078
    invoke-virtual {p0}, Lcom/google/r/b/a/alm;->d()V

    .line 14079
    iget-object v0, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    invoke-static {p1, v0}, Lcom/google/n/b;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 14082
    return-object p0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 13911
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 13912
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/alm;->c:Ljava/util/List;

    .line 13913
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 13915
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 14016
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    .line 14017
    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/r/b/a/alm;->e:Lcom/google/n/aq;

    .line 14018
    iget v0, p0, Lcom/google/r/b/a/alm;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/alm;->a:I

    .line 14020
    :cond_0
    return-void
.end method
