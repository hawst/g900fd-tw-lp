.class public final Lcom/google/r/b/a/a/h;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/a/f;",
        "Lcom/google/r/b/a/a/h;",
        ">;",
        "Lcom/google/r/b/a/a/i;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 265
    sget-object v0, Lcom/google/r/b/a/a/f;->b:Lcom/google/r/b/a/a/f;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 312
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    .line 266
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 257
    new-instance v0, Lcom/google/r/b/a/a/f;

    invoke-direct {v0, p0}, Lcom/google/r/b/a/a/f;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/r/b/a/a/h;->a:I

    iget v1, p0, Lcom/google/r/b/a/a/h;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/a/h;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/a/h;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 257
    check-cast p1, Lcom/google/r/b/a/a/f;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/a/h;->a(Lcom/google/r/b/a/a/f;)Lcom/google/r/b/a/a/h;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/a/f;)Lcom/google/r/b/a/a/h;
    .locals 2

    .prologue
    .line 290
    invoke-static {}, Lcom/google/r/b/a/a/f;->d()Lcom/google/r/b/a/a/f;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 302
    :goto_0
    return-object p0

    .line 291
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    iget-object v0, p1, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    .line 294
    iget v0, p0, Lcom/google/r/b/a/a/h;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/r/b/a/a/h;->a:I

    .line 301
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/r/b/a/a/f;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 296
    :cond_2
    invoke-virtual {p0}, Lcom/google/r/b/a/a/h;->c()V

    .line 297
    iget-object v0, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 314
    iget v0, p0, Lcom/google/r/b/a/a/h;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 315
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    .line 318
    iget v0, p0, Lcom/google/r/b/a/a/h;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/a/h;->a:I

    .line 320
    :cond_0
    return-void
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    .line 327
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 328
    iget-object v0, p0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 329
    invoke-static {}, Lcom/google/r/b/a/a/b;->d()Lcom/google/r/b/a/a/b;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 331
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
