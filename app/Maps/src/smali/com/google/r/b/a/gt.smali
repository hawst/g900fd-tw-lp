.class public final enum Lcom/google/r/b/a/gt;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/gt;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/gt;

.field public static final enum b:Lcom/google/r/b/a/gt;

.field public static final enum c:Lcom/google/r/b/a/gt;

.field private static final synthetic e:[Lcom/google/r/b/a/gt;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 719
    new-instance v0, Lcom/google/r/b/a/gt;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/r/b/a/gt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gt;->a:Lcom/google/r/b/a/gt;

    .line 723
    new-instance v0, Lcom/google/r/b/a/gt;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/gt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gt;->b:Lcom/google/r/b/a/gt;

    .line 727
    new-instance v0, Lcom/google/r/b/a/gt;

    const-string v1, "USER_NOT_ONBOARDED"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/r/b/a/gt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gt;->c:Lcom/google/r/b/a/gt;

    .line 714
    new-array v0, v5, [Lcom/google/r/b/a/gt;

    sget-object v1, Lcom/google/r/b/a/gt;->a:Lcom/google/r/b/a/gt;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/gt;->b:Lcom/google/r/b/a/gt;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/gt;->c:Lcom/google/r/b/a/gt;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/r/b/a/gt;->e:[Lcom/google/r/b/a/gt;

    .line 762
    new-instance v0, Lcom/google/r/b/a/gu;

    invoke-direct {v0}, Lcom/google/r/b/a/gu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 771
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 772
    iput p3, p0, Lcom/google/r/b/a/gt;->d:I

    .line 773
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/gt;
    .locals 1

    .prologue
    .line 749
    packed-switch p0, :pswitch_data_0

    .line 753
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 750
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/gt;->a:Lcom/google/r/b/a/gt;

    goto :goto_0

    .line 751
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/gt;->b:Lcom/google/r/b/a/gt;

    goto :goto_0

    .line 752
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/gt;->c:Lcom/google/r/b/a/gt;

    goto :goto_0

    .line 749
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/gt;
    .locals 1

    .prologue
    .line 714
    const-class v0, Lcom/google/r/b/a/gt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/gt;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/gt;
    .locals 1

    .prologue
    .line 714
    sget-object v0, Lcom/google/r/b/a/gt;->e:[Lcom/google/r/b/a/gt;

    invoke-virtual {v0}, [Lcom/google/r/b/a/gt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/gt;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 745
    iget v0, p0, Lcom/google/r/b/a/gt;->d:I

    return v0
.end method
