.class public final Lcom/google/r/b/a/dj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/dm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/dj;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/dj;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Z

.field c:Z

.field d:Z

.field e:Lcom/google/n/ao;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/r/b/a/dk;

    invoke-direct {v0}, Lcom/google/r/b/a/dk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/dj;->PARSER:Lcom/google/n/ax;

    .line 212
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/dj;->i:Lcom/google/n/aw;

    .line 521
    new-instance v0, Lcom/google/r/b/a/dj;

    invoke-direct {v0}, Lcom/google/r/b/a/dj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/dj;->f:Lcom/google/r/b/a/dj;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 140
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/dj;->e:Lcom/google/n/ao;

    .line 155
    iput-byte v3, p0, Lcom/google/r/b/a/dj;->g:B

    .line 183
    iput v3, p0, Lcom/google/r/b/a/dj;->h:I

    .line 18
    iput-boolean v1, p0, Lcom/google/r/b/a/dj;->b:Z

    .line 19
    iput-boolean v1, p0, Lcom/google/r/b/a/dj;->c:Z

    .line 20
    iput-boolean v1, p0, Lcom/google/r/b/a/dj;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/r/b/a/dj;->e:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/r/b/a/dj;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 34
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/dj;->a:I

    .line 49
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/dj;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/dj;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 49
    goto :goto_1

    .line 53
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/dj;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/dj;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 72
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 73
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v0, v2

    .line 54
    goto :goto_2

    .line 58
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/dj;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/r/b/a/dj;->d:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 63
    :sswitch_4
    iget-object v0, p0, Lcom/google/r/b/a/dj;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 64
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/dj;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 75
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dj;->au:Lcom/google/n/bn;

    .line 76
    return-void

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 140
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/dj;->e:Lcom/google/n/ao;

    .line 155
    iput-byte v1, p0, Lcom/google/r/b/a/dj;->g:B

    .line 183
    iput v1, p0, Lcom/google/r/b/a/dj;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/dj;
    .locals 1

    .prologue
    .line 524
    sget-object v0, Lcom/google/r/b/a/dj;->f:Lcom/google/r/b/a/dj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/dl;
    .locals 1

    .prologue
    .line 274
    new-instance v0, Lcom/google/r/b/a/dl;

    invoke-direct {v0}, Lcom/google/r/b/a/dl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/dj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/google/r/b/a/dj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 167
    invoke-virtual {p0}, Lcom/google/r/b/a/dj;->c()I

    .line 168
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 169
    iget-boolean v0, p0, Lcom/google/r/b/a/dj;->b:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IZ)V

    .line 171
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 172
    iget-boolean v0, p0, Lcom/google/r/b/a/dj;->c:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 174
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 175
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/dj;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 177
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 178
    iget-object v0, p0, Lcom/google/r/b/a/dj;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/dj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 181
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 157
    iget-byte v1, p0, Lcom/google/r/b/a/dj;->g:B

    .line 158
    if-ne v1, v0, :cond_0

    .line 162
    :goto_0
    return v0

    .line 159
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 161
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/dj;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 185
    iget v0, p0, Lcom/google/r/b/a/dj;->h:I

    .line 186
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 207
    :goto_0
    return v0

    .line 189
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 190
    iget-boolean v0, p0, Lcom/google/r/b/a/dj;->b:Z

    .line 191
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 193
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 194
    iget-boolean v2, p0, Lcom/google/r/b/a/dj;->c:Z

    .line 195
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 197
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 198
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/r/b/a/dj;->d:Z

    .line 199
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 201
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 202
    iget-object v2, p0, Lcom/google/r/b/a/dj;->e:Lcom/google/n/ao;

    .line 203
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 205
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/dj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    iput v0, p0, Lcom/google/r/b/a/dj;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/dj;->newBuilder()Lcom/google/r/b/a/dl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/dl;->a(Lcom/google/r/b/a/dj;)Lcom/google/r/b/a/dl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/dj;->newBuilder()Lcom/google/r/b/a/dl;

    move-result-object v0

    return-object v0
.end method
