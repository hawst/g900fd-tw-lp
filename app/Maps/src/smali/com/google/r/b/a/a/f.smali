.class public final Lcom/google/r/b/a/a/f;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/i;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/f;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/r/b/a/a/f;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/r/b/a/a/g;

    invoke-direct {v0}, Lcom/google/r/b/a/a/g;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/f;->PARSER:Lcom/google/n/ax;

    .line 185
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/f;->e:Lcom/google/n/aw;

    .line 452
    new-instance v0, Lcom/google/r/b/a/a/f;

    invoke-direct {v0}, Lcom/google/r/b/a/a/f;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/f;->b:Lcom/google/r/b/a/a/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 40
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 149
    iput-byte v0, p0, Lcom/google/r/b/a/a/f;->c:B

    .line 168
    iput v0, p0, Lcom/google/r/b/a/a/f;->d:I

    .line 41
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    .line 42
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 48
    invoke-direct {p0}, Lcom/google/r/b/a/a/f;-><init>()V

    .line 51
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 54
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 55
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 56
    sparse-switch v4, :sswitch_data_0

    .line 61
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 63
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 59
    goto :goto_0

    .line 68
    :sswitch_1
    and-int/lit8 v4, v0, 0x1

    if-eq v4, v2, :cond_1

    .line 69
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    .line 71
    or-int/lit8 v0, v0, 0x1

    .line 73
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 73
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 79
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 80
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 86
    iget-object v1, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    .line 88
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/f;->au:Lcom/google/n/bn;

    throw v0

    .line 85
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 86
    iget-object v0, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    .line 88
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/f;->au:Lcom/google/n/bn;

    .line 89
    return-void

    .line 81
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 82
    :try_start_2
    new-instance v4, Lcom/google/n/ak;

    .line 83
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 85
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 38
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 149
    iput-byte v0, p0, Lcom/google/r/b/a/a/f;->c:B

    .line 168
    iput v0, p0, Lcom/google/r/b/a/a/f;->d:I

    .line 39
    return-void
.end method

.method public static d()Lcom/google/r/b/a/a/f;
    .locals 1

    .prologue
    .line 455
    sget-object v0, Lcom/google/r/b/a/a/f;->b:Lcom/google/r/b/a/a/f;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/h;
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lcom/google/r/b/a/a/h;

    invoke-direct {v0}, Lcom/google/r/b/a/a/h;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/google/r/b/a/a/f;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/r/b/a/a/f;->c()I

    .line 162
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 163
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 162
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/a/f;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 166
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 151
    iget-byte v1, p0, Lcom/google/r/b/a/a/f;->c:B

    .line 152
    if-ne v1, v0, :cond_0

    .line 156
    :goto_0
    return v0

    .line 153
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 155
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/a/f;->c:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 170
    iget v0, p0, Lcom/google/r/b/a/a/f;->d:I

    .line 171
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 180
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 174
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 175
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/a/f;->a:Ljava/util/List;

    .line 176
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/a/f;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 179
    iput v0, p0, Lcom/google/r/b/a/a/f;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/google/r/b/a/a/f;->newBuilder()Lcom/google/r/b/a/a/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/h;->a(Lcom/google/r/b/a/a/f;)Lcom/google/r/b/a/a/h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/google/r/b/a/a/f;->newBuilder()Lcom/google/r/b/a/a/h;

    move-result-object v0

    return-object v0
.end method
