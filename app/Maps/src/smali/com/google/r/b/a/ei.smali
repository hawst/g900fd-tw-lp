.class public final Lcom/google/r/b/a/ei;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/en;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ei;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/r/b/a/ei;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/google/r/b/a/ej;

    invoke-direct {v0}, Lcom/google/r/b/a/ej;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ei;->PARSER:Lcom/google/n/ax;

    .line 1937
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ei;->d:Lcom/google/n/aw;

    .line 2048
    new-instance v0, Lcom/google/r/b/a/ei;

    invoke-direct {v0}, Lcom/google/r/b/a/ei;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ei;->a:Lcom/google/r/b/a/ei;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1908
    iput-byte v0, p0, Lcom/google/r/b/a/ei;->b:B

    .line 1924
    iput v0, p0, Lcom/google/r/b/a/ei;->c:I

    .line 18
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 24
    invoke-direct {p0}, Lcom/google/r/b/a/ei;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 28
    const/4 v0, 0x0

    .line 29
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 30
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 31
    packed-switch v3, :pswitch_data_0

    .line 36
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 38
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 34
    goto :goto_0

    .line 50
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ei;->au:Lcom/google/n/bn;

    .line 51
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 50
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ei;->au:Lcom/google/n/bn;

    throw v0

    .line 46
    :catch_1
    move-exception v0

    .line 47
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 48
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1908
    iput-byte v0, p0, Lcom/google/r/b/a/ei;->b:B

    .line 1924
    iput v0, p0, Lcom/google/r/b/a/ei;->c:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ei;
    .locals 1

    .prologue
    .line 2051
    sget-object v0, Lcom/google/r/b/a/ei;->a:Lcom/google/r/b/a/ei;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ek;
    .locals 1

    .prologue
    .line 1999
    new-instance v0, Lcom/google/r/b/a/ek;

    invoke-direct {v0}, Lcom/google/r/b/a/ek;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ei;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    sget-object v0, Lcom/google/r/b/a/ei;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 1920
    iget v0, p0, Lcom/google/r/b/a/ei;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1921
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ei;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1922
    return-void

    .line 1920
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/ei;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ei;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1910
    iget-byte v1, p0, Lcom/google/r/b/a/ei;->b:B

    .line 1911
    if-ne v1, v0, :cond_0

    .line 1915
    :goto_0
    return v0

    .line 1912
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1914
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ei;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1926
    iget v0, p0, Lcom/google/r/b/a/ei;->c:I

    .line 1927
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1932
    :goto_0
    return v0

    .line 1929
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/ei;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1931
    iput v0, p0, Lcom/google/r/b/a/ei;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ei;->newBuilder()Lcom/google/r/b/a/ek;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ek;->a(Lcom/google/r/b/a/ei;)Lcom/google/r/b/a/ek;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ei;->newBuilder()Lcom/google/r/b/a/ek;

    move-result-object v0

    return-object v0
.end method
