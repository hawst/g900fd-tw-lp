.class public final Lcom/google/r/b/a/f;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/d;",
        "Lcom/google/r/b/a/f;",
        ">;",
        "Lcom/google/r/b/a/i;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4439
    sget-object v0, Lcom/google/r/b/a/d;->d:Lcom/google/r/b/a/d;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4490
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/f;->b:I

    .line 4526
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/f;->c:Ljava/lang/Object;

    .line 4440
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4431
    new-instance v2, Lcom/google/r/b/a/d;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/d;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/f;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/f;->b:I

    iput v1, v2, Lcom/google/r/b/a/d;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/f;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/d;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4431
    check-cast p1, Lcom/google/r/b/a/d;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/f;->a(Lcom/google/r/b/a/d;)Lcom/google/r/b/a/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/d;)Lcom/google/r/b/a/f;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4471
    invoke-static {}, Lcom/google/r/b/a/d;->d()Lcom/google/r/b/a/d;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4481
    :goto_0
    return-object p0

    .line 4472
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/d;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 4473
    iget v2, p1, Lcom/google/r/b/a/d;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/g;->a(I)Lcom/google/r/b/a/g;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/g;->a:Lcom/google/r/b/a/g;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 4472
    goto :goto_1

    .line 4473
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/f;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/f;->a:I

    iget v2, v2, Lcom/google/r/b/a/g;->e:I

    iput v2, p0, Lcom/google/r/b/a/f;->b:I

    .line 4475
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/d;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 4476
    iget v0, p0, Lcom/google/r/b/a/f;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/f;->a:I

    .line 4477
    iget-object v0, p1, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/f;->c:Ljava/lang/Object;

    .line 4480
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/d;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 4475
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4485
    const/4 v0, 0x1

    return v0
.end method
