.class public final Lcom/google/r/b/a/ad;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ag;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ad;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/ad;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4706
    new-instance v0, Lcom/google/r/b/a/ae;

    invoke-direct {v0}, Lcom/google/r/b/a/ae;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ad;->PARSER:Lcom/google/n/ax;

    .line 4823
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ad;->g:Lcom/google/n/aw;

    .line 5096
    new-instance v0, Lcom/google/r/b/a/ad;

    invoke-direct {v0}, Lcom/google/r/b/a/ad;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ad;->d:Lcom/google/r/b/a/ad;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 4656
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4765
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ad;->c:Lcom/google/n/ao;

    .line 4780
    iput-byte v2, p0, Lcom/google/r/b/a/ad;->e:B

    .line 4802
    iput v2, p0, Lcom/google/r/b/a/ad;->f:I

    .line 4657
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ad;->b:Ljava/lang/Object;

    .line 4658
    iget-object v0, p0, Lcom/google/r/b/a/ad;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4659
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4665
    invoke-direct {p0}, Lcom/google/r/b/a/ad;-><init>()V

    .line 4666
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 4671
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 4672
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 4673
    sparse-switch v3, :sswitch_data_0

    .line 4678
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 4680
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4676
    goto :goto_0

    .line 4685
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 4686
    iget v4, p0, Lcom/google/r/b/a/ad;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ad;->a:I

    .line 4687
    iput-object v3, p0, Lcom/google/r/b/a/ad;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4697
    :catch_0
    move-exception v0

    .line 4698
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4703
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ad;->au:Lcom/google/n/bn;

    throw v0

    .line 4691
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/ad;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 4692
    iget v3, p0, Lcom/google/r/b/a/ad;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ad;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4699
    :catch_1
    move-exception v0

    .line 4700
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4701
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4703
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ad;->au:Lcom/google/n/bn;

    .line 4704
    return-void

    .line 4673
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 4654
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4765
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ad;->c:Lcom/google/n/ao;

    .line 4780
    iput-byte v1, p0, Lcom/google/r/b/a/ad;->e:B

    .line 4802
    iput v1, p0, Lcom/google/r/b/a/ad;->f:I

    .line 4655
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ad;
    .locals 1

    .prologue
    .line 5099
    sget-object v0, Lcom/google/r/b/a/ad;->d:Lcom/google/r/b/a/ad;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/af;
    .locals 1

    .prologue
    .line 4885
    new-instance v0, Lcom/google/r/b/a/af;

    invoke-direct {v0}, Lcom/google/r/b/a/af;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4718
    sget-object v0, Lcom/google/r/b/a/ad;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4792
    invoke-virtual {p0}, Lcom/google/r/b/a/ad;->c()I

    .line 4793
    iget v0, p0, Lcom/google/r/b/a/ad;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 4794
    iget-object v0, p0, Lcom/google/r/b/a/ad;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ad;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4796
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ad;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 4797
    iget-object v0, p0, Lcom/google/r/b/a/ad;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4799
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/ad;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4800
    return-void

    .line 4794
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4782
    iget-byte v1, p0, Lcom/google/r/b/a/ad;->e:B

    .line 4783
    if-ne v1, v0, :cond_0

    .line 4787
    :goto_0
    return v0

    .line 4784
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 4786
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ad;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 4804
    iget v0, p0, Lcom/google/r/b/a/ad;->f:I

    .line 4805
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4818
    :goto_0
    return v0

    .line 4808
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ad;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 4810
    iget-object v0, p0, Lcom/google/r/b/a/ad;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ad;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 4812
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/ad;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 4813
    iget-object v2, p0, Lcom/google/r/b/a/ad;->c:Lcom/google/n/ao;

    .line 4814
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 4816
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ad;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4817
    iput v0, p0, Lcom/google/r/b/a/ad;->f:I

    goto :goto_0

    .line 4810
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4648
    invoke-static {}, Lcom/google/r/b/a/ad;->newBuilder()Lcom/google/r/b/a/af;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/af;->a(Lcom/google/r/b/a/ad;)Lcom/google/r/b/a/af;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4648
    invoke-static {}, Lcom/google/r/b/a/ad;->newBuilder()Lcom/google/r/b/a/af;

    move-result-object v0

    return-object v0
.end method
