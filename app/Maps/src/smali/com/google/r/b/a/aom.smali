.class public final Lcom/google/r/b/a/aom;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aop;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aok;",
        "Lcom/google/r/b/a/aom;",
        ">;",
        "Lcom/google/r/b/a/aop;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 837
    sget-object v0, Lcom/google/r/b/a/aok;->d:Lcom/google/r/b/a/aok;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 900
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aom;->b:I

    .line 937
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    .line 838
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 829
    new-instance v2, Lcom/google/r/b/a/aok;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aok;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aom;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/aom;->b:I

    iput v1, v2, Lcom/google/r/b/a/aok;->b:I

    iget v1, p0, Lcom/google/r/b/a/aom;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/aom;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/aom;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/aok;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 829
    check-cast p1, Lcom/google/r/b/a/aok;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aom;->a(Lcom/google/r/b/a/aok;)Lcom/google/r/b/a/aom;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aok;)Lcom/google/r/b/a/aom;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 870
    invoke-static {}, Lcom/google/r/b/a/aok;->g()Lcom/google/r/b/a/aok;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 885
    :goto_0
    return-object p0

    .line 871
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/aok;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 872
    iget v0, p1, Lcom/google/r/b/a/aok;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/aon;->a(I)Lcom/google/r/b/a/aon;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/aon;->a:Lcom/google/r/b/a/aon;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 871
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 872
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/aom;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/aom;->a:I

    iget v0, v0, Lcom/google/r/b/a/aon;->d:I

    iput v0, p0, Lcom/google/r/b/a/aom;->b:I

    .line 874
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 875
    iget-object v0, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 876
    iget-object v0, p1, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    .line 877
    iget v0, p0, Lcom/google/r/b/a/aom;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/aom;->a:I

    .line 884
    :cond_5
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/aok;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 879
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/aom;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/aom;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/aom;->a:I

    .line 880
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 889
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 890
    iget-object v0, p0, Lcom/google/r/b/a/aom;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/anw;->i()Lcom/google/r/b/a/anw;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anw;

    invoke-virtual {v0}, Lcom/google/r/b/a/anw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 895
    :goto_1
    return v2

    .line 889
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 895
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
