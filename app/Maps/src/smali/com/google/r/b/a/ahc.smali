.class public final Lcom/google/r/b/a/ahc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ahh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aha;",
        "Lcom/google/r/b/a/ahc;",
        ">;",
        "Lcom/google/r/b/a/ahh;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/f;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/f;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/n/f;

.field public h:I

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public j:I

.field private k:I

.field private l:Lcom/google/n/ao;

.field private m:Lcom/google/n/ao;

.field private n:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 6215
    sget-object v0, Lcom/google/r/b/a/aha;->o:Lcom/google/r/b/a/aha;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6401
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ahc;->b:I

    .line 6437
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->c:Lcom/google/n/f;

    .line 6472
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    .line 6577
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    .line 6714
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    .line 6850
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->l:Lcom/google/n/ao;

    .line 6909
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->m:Lcom/google/n/ao;

    .line 6968
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->g:Lcom/google/n/f;

    .line 7068
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    .line 6216
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6207
    new-instance v2, Lcom/google/r/b/a/aha;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aha;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/ahc;->b:I

    iput v4, v2, Lcom/google/r/b/a/aha;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/ahc;->c:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/r/b/a/aha;->c:Lcom/google/n/f;

    iget v4, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/r/b/a/ahc;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/r/b/a/ahc;->k:I

    iput v4, v2, Lcom/google/r/b/a/aha;->e:I

    iget v4, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/r/b/a/ahc;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/r/b/a/ahc;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x8

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/aha;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ahc;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ahc;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x10

    :cond_6
    iget-object v4, v2, Lcom/google/r/b/a/aha;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ahc;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ahc;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit8 v0, v0, 0x20

    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/ahc;->g:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/aha;->j:Lcom/google/n/f;

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit8 v0, v0, 0x40

    :cond_8
    iget v1, p0, Lcom/google/r/b/a/ahc;->h:I

    iput v1, v2, Lcom/google/r/b/a/aha;->k:I

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x80

    :cond_9
    iget v1, p0, Lcom/google/r/b/a/ahc;->n:I

    iput v1, v2, Lcom/google/r/b/a/aha;->l:I

    iget v1, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    iget-object v1, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit16 v1, v1, -0x801

    iput v1, p0, Lcom/google/r/b/a/ahc;->a:I

    :cond_a
    iget-object v1, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x100

    :cond_b
    iget v1, p0, Lcom/google/r/b/a/ahc;->j:I

    iput v1, v2, Lcom/google/r/b/a/aha;->n:I

    iput v0, v2, Lcom/google/r/b/a/aha;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 6207
    check-cast p1, Lcom/google/r/b/a/aha;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ahc;->a(Lcom/google/r/b/a/aha;)Lcom/google/r/b/a/ahc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aha;)Lcom/google/r/b/a/ahc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6321
    invoke-static {}, Lcom/google/r/b/a/aha;->d()Lcom/google/r/b/a/aha;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6392
    :goto_0
    return-object p0

    .line 6322
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 6323
    iget v2, p1, Lcom/google/r/b/a/aha;->b:I

    invoke-static {v2}, Lcom/google/maps/g/wq;->a(I)Lcom/google/maps/g/wq;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/wq;->a:Lcom/google/maps/g/wq;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 6322
    goto :goto_1

    .line 6323
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ahc;->a:I

    iget v2, v2, Lcom/google/maps/g/wq;->h:I

    iput v2, p0, Lcom/google/r/b/a/ahc;->b:I

    .line 6325
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 6326
    iget-object v2, p1, Lcom/google/r/b/a/aha;->c:Lcom/google/n/f;

    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 6325
    goto :goto_2

    .line 6326
    :cond_6
    iget v3, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ahc;->a:I

    iput-object v2, p0, Lcom/google/r/b/a/ahc;->c:Lcom/google/n/f;

    .line 6328
    :cond_7
    iget-object v2, p1, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 6329
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 6330
    iget-object v2, p1, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    .line 6331
    iget v2, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6338
    :cond_8
    :goto_3
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    .line 6339
    iget v2, p1, Lcom/google/r/b/a/aha;->e:I

    iget v3, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/ahc;->a:I

    iput v2, p0, Lcom/google/r/b/a/ahc;->k:I

    .line 6341
    :cond_9
    iget-object v2, p1, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 6342
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 6343
    iget-object v2, p1, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    .line 6344
    iget v2, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6351
    :cond_a
    :goto_5
    iget-object v2, p1, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 6352
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 6353
    iget-object v2, p1, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    .line 6354
    iget v2, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6361
    :cond_b
    :goto_6
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_c

    .line 6362
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aha;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6363
    iget v2, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6365
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_8
    if-eqz v2, :cond_d

    .line 6366
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aha;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6367
    iget v2, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6369
    :cond_d
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_9
    if-eqz v2, :cond_16

    .line 6370
    iget-object v2, p1, Lcom/google/r/b/a/aha;->j:Lcom/google/n/f;

    if-nez v2, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6333
    :cond_e
    invoke-virtual {p0}, Lcom/google/r/b/a/ahc;->c()V

    .line 6334
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 6338
    goto/16 :goto_4

    .line 6346
    :cond_10
    invoke-virtual {p0}, Lcom/google/r/b/a/ahc;->d()V

    .line 6347
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    .line 6356
    :cond_11
    invoke-virtual {p0}, Lcom/google/r/b/a/ahc;->i()V

    .line 6357
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_12
    move v2, v1

    .line 6361
    goto :goto_7

    :cond_13
    move v2, v1

    .line 6365
    goto :goto_8

    :cond_14
    move v2, v1

    .line 6369
    goto :goto_9

    .line 6370
    :cond_15
    iget v3, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/ahc;->a:I

    iput-object v2, p0, Lcom/google/r/b/a/ahc;->g:Lcom/google/n/f;

    .line 6372
    :cond_16
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_a
    if-eqz v2, :cond_17

    .line 6373
    iget v2, p1, Lcom/google/r/b/a/aha;->k:I

    iget v3, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/r/b/a/ahc;->a:I

    iput v2, p0, Lcom/google/r/b/a/ahc;->h:I

    .line 6375
    :cond_17
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_b
    if-eqz v2, :cond_18

    .line 6376
    iget v2, p1, Lcom/google/r/b/a/aha;->l:I

    iget v3, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/ahc;->a:I

    iput v2, p0, Lcom/google/r/b/a/ahc;->n:I

    .line 6378
    :cond_18
    iget-object v2, p1, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_19

    .line 6379
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 6380
    iget-object v2, p1, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    .line 6381
    iget v2, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit16 v2, v2, -0x801

    iput v2, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6388
    :cond_19
    :goto_c
    iget v2, p1, Lcom/google/r/b/a/aha;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1e

    :goto_d
    if-eqz v0, :cond_1a

    .line 6389
    iget v0, p1, Lcom/google/r/b/a/aha;->n:I

    iget v1, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/r/b/a/ahc;->a:I

    iput v0, p0, Lcom/google/r/b/a/ahc;->j:I

    .line 6391
    :cond_1a
    iget-object v0, p1, Lcom/google/r/b/a/aha;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_1b
    move v2, v1

    .line 6372
    goto :goto_a

    :cond_1c
    move v2, v1

    .line 6375
    goto :goto_b

    .line 6383
    :cond_1d
    invoke-virtual {p0}, Lcom/google/r/b/a/ahc;->j()V

    .line 6384
    iget-object v2, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_c

    :cond_1e
    move v0, v1

    .line 6388
    goto :goto_d
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 6396
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 6474
    iget v0, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 6475
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->d:Ljava/util/List;

    .line 6476
    iget v0, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6478
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 6579
    iget v0, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 6580
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->e:Ljava/util/List;

    .line 6583
    iget v0, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6585
    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 6716
    iget v0, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 6717
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->f:Ljava/util/List;

    .line 6720
    iget v0, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 6722
    :cond_0
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 7070
    iget v0, p0, Lcom/google/r/b/a/ahc;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-eq v0, v1, :cond_0

    .line 7071
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ahc;->i:Ljava/util/List;

    .line 7074
    iget v0, p0, Lcom/google/r/b/a/ahc;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/ahc;->a:I

    .line 7076
    :cond_0
    return-void
.end method
