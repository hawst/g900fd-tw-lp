.class public final Lcom/google/r/b/a/rx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/sa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/rx;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/r/b/a/rx;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Ljava/lang/Object;

.field g:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/r/b/a/ry;

    invoke-direct {v0}, Lcom/google/r/b/a/ry;-><init>()V

    sput-object v0, Lcom/google/r/b/a/rx;->PARSER:Lcom/google/n/ax;

    .line 316
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/rx;->k:Lcom/google/n/aw;

    .line 831
    new-instance v0, Lcom/google/r/b/a/rx;

    invoke-direct {v0}, Lcom/google/r/b/a/rx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/rx;->h:Lcom/google/r/b/a/rx;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 108
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rx;->b:Lcom/google/n/ao;

    .line 139
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rx;->d:Lcom/google/n/ao;

    .line 155
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rx;->e:Lcom/google/n/ao;

    .line 227
    iput-byte v3, p0, Lcom/google/r/b/a/rx;->i:B

    .line 279
    iput v3, p0, Lcom/google/r/b/a/rx;->j:I

    .line 18
    iget-object v0, p0, Lcom/google/r/b/a/rx;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput v4, p0, Lcom/google/r/b/a/rx;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/r/b/a/rx;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/r/b/a/rx;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rx;->f:Ljava/lang/Object;

    .line 23
    iput v4, p0, Lcom/google/r/b/a/rx;->g:I

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/r/b/a/rx;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 38
    sparse-switch v3, :sswitch_data_0

    .line 43
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/rx;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 51
    iget v3, p0, Lcom/google/r/b/a/rx;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/rx;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/rx;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/rx;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 56
    iget v3, p0, Lcom/google/r/b/a/rx;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/rx;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 84
    :catch_1
    move-exception v0

    .line 85
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 86
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 61
    iget v4, p0, Lcom/google/r/b/a/rx;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/rx;->a:I

    .line 62
    iput-object v3, p0, Lcom/google/r/b/a/rx;->f:Ljava/lang/Object;

    goto :goto_0

    .line 66
    :sswitch_4
    iget v3, p0, Lcom/google/r/b/a/rx;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/rx;->a:I

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/rx;->g:I

    goto :goto_0

    .line 71
    :sswitch_5
    iget v3, p0, Lcom/google/r/b/a/rx;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/rx;->a:I

    .line 72
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/rx;->c:I

    goto :goto_0

    .line 76
    :sswitch_6
    iget-object v3, p0, Lcom/google/r/b/a/rx;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 77
    iget v3, p0, Lcom/google/r/b/a/rx;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/rx;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 88
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rx;->au:Lcom/google/n/bn;

    .line 89
    return-void

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 108
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rx;->b:Lcom/google/n/ao;

    .line 139
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rx;->d:Lcom/google/n/ao;

    .line 155
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rx;->e:Lcom/google/n/ao;

    .line 227
    iput-byte v1, p0, Lcom/google/r/b/a/rx;->i:B

    .line 279
    iput v1, p0, Lcom/google/r/b/a/rx;->j:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/rx;
    .locals 1

    .prologue
    .line 834
    sget-object v0, Lcom/google/r/b/a/rx;->h:Lcom/google/r/b/a/rx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/rz;
    .locals 1

    .prologue
    .line 378
    new-instance v0, Lcom/google/r/b/a/rz;

    invoke-direct {v0}, Lcom/google/r/b/a/rz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/rx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/google/r/b/a/rx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 257
    invoke-virtual {p0}, Lcom/google/r/b/a/rx;->c()I

    .line 258
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/r/b/a/rx;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 261
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 262
    iget-object v0, p0, Lcom/google/r/b/a/rx;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 264
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 265
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/rx;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rx;->f:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 267
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 268
    iget v0, p0, Lcom/google/r/b/a/rx;->g:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 270
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_4

    .line 271
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/rx;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 273
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 274
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/rx;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 276
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/rx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 277
    return-void

    .line 265
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 229
    iget-byte v0, p0, Lcom/google/r/b/a/rx;->i:B

    .line 230
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 252
    :goto_0
    return v0

    .line 231
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 233
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/google/r/b/a/rx;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 235
    iput-byte v2, p0, Lcom/google/r/b/a/rx;->i:B

    move v0, v2

    .line 236
    goto :goto_0

    :cond_2
    move v0, v2

    .line 233
    goto :goto_1

    .line 239
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 240
    iget-object v0, p0, Lcom/google/r/b/a/rx;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 241
    iput-byte v2, p0, Lcom/google/r/b/a/rx;->i:B

    move v0, v2

    .line 242
    goto :goto_0

    :cond_4
    move v0, v2

    .line 239
    goto :goto_2

    .line 245
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 246
    iget-object v0, p0, Lcom/google/r/b/a/rx;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 247
    iput-byte v2, p0, Lcom/google/r/b/a/rx;->i:B

    move v0, v2

    .line 248
    goto :goto_0

    :cond_6
    move v0, v2

    .line 245
    goto :goto_3

    .line 251
    :cond_7
    iput-byte v1, p0, Lcom/google/r/b/a/rx;->i:B

    move v0, v1

    .line 252
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v3, 0xa

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 281
    iget v0, p0, Lcom/google/r/b/a/rx;->j:I

    .line 282
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 311
    :goto_0
    return v0

    .line 285
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 286
    iget-object v0, p0, Lcom/google/r/b/a/rx;->b:Lcom/google/n/ao;

    .line 287
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 289
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_8

    .line 290
    iget-object v2, p0, Lcom/google/r/b/a/rx;->d:Lcom/google/n/ao;

    .line 291
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 293
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_1

    .line 294
    const/4 v4, 0x3

    .line 295
    iget-object v0, p0, Lcom/google/r/b/a/rx;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rx;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 297
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_2

    .line 298
    iget v0, p0, Lcom/google/r/b/a/rx;->g:I

    .line 299
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 301
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 302
    const/4 v0, 0x5

    iget v4, p0, Lcom/google/r/b/a/rx;->c:I

    .line 303
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_3

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_3
    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 305
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/rx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_5

    .line 306
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/r/b/a/rx;->e:Lcom/google/n/ao;

    .line 307
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 309
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/rx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 310
    iput v0, p0, Lcom/google/r/b/a/rx;->j:I

    goto/16 :goto_0

    .line 295
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_7
    move v0, v3

    .line 299
    goto :goto_4

    :cond_8
    move v2, v0

    goto/16 :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/rx;->newBuilder()Lcom/google/r/b/a/rz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/rz;->a(Lcom/google/r/b/a/rx;)Lcom/google/r/b/a/rz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/rx;->newBuilder()Lcom/google/r/b/a/rz;

    move-result-object v0

    return-object v0
.end method
