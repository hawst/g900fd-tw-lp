.class public final enum Lcom/google/r/b/a/bq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/bq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/bq;

.field public static final enum b:Lcom/google/r/b/a/bq;

.field public static final enum c:Lcom/google/r/b/a/bq;

.field public static final enum d:Lcom/google/r/b/a/bq;

.field public static final enum e:Lcom/google/r/b/a/bq;

.field public static final enum f:Lcom/google/r/b/a/bq;

.field public static final enum g:Lcom/google/r/b/a/bq;

.field public static final enum h:Lcom/google/r/b/a/bq;

.field public static final enum i:Lcom/google/r/b/a/bq;

.field public static final enum j:Lcom/google/r/b/a/bq;

.field public static final enum k:Lcom/google/r/b/a/bq;

.field public static final enum l:Lcom/google/r/b/a/bq;

.field public static final enum m:Lcom/google/r/b/a/bq;

.field public static final enum n:Lcom/google/r/b/a/bq;

.field public static final enum o:Lcom/google/r/b/a/bq;

.field public static final enum p:Lcom/google/r/b/a/bq;

.field public static final enum q:Lcom/google/r/b/a/bq;

.field public static final enum r:Lcom/google/r/b/a/bq;

.field public static final enum s:Lcom/google/r/b/a/bq;

.field public static final enum t:Lcom/google/r/b/a/bq;

.field public static final enum u:Lcom/google/r/b/a/bq;

.field public static final enum v:Lcom/google/r/b/a/bq;

.field public static final enum w:Lcom/google/r/b/a/bq;

.field public static final enum x:Lcom/google/r/b/a/bq;

.field private static final synthetic z:[Lcom/google/r/b/a/bq;


# instance fields
.field private final y:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x11

    const/16 v7, 0xc

    const/16 v6, 0xb

    const/4 v5, 0x7

    const/4 v4, 0x6

    .line 4198
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "WRITE_REVIEW"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->a:Lcom/google/r/b/a/bq;

    .line 4202
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "VIEW_PLACE_ON_MAP"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v5}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->b:Lcom/google/r/b/a/bq;

    .line 4206
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "OPEN_PUBLIC_PROFILE"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->c:Lcom/google/r/b/a/bq;

    .line 4210
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "SET_PLACE_ALIAS"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v7}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->d:Lcom/google/r/b/a/bq;

    .line 4214
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "LOAD_BUILDING_DIRECTORY"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v8}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->e:Lcom/google/r/b/a/bq;

    .line 4218
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "LOAD_MAPS_ACTIVITIES"

    const/4 v2, 0x5

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->f:Lcom/google/r/b/a/bq;

    .line 4222
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "DISMISS_CARD"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v4, v2}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->g:Lcom/google/r/b/a/bq;

    .line 4226
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "SHOW_MAP"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v5, v2}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->h:Lcom/google/r/b/a/bq;

    .line 4230
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "SEND_DISMISS_TODO"

    const/16 v2, 0x8

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->i:Lcom/google/r/b/a/bq;

    .line 4234
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "SHOW_OFFLINE_MAP_SELECTION"

    const/16 v2, 0x9

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->j:Lcom/google/r/b/a/bq;

    .line 4238
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "SHOW_PLACE"

    const/16 v2, 0xa

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->k:Lcom/google/r/b/a/bq;

    .line 4242
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "UPDATE_DIRECTIONS_STATE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v6, v2}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->l:Lcom/google/r/b/a/bq;

    .line 4246
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "UPDATE_DIRECTIONS_STATE_WITH_RENDERABLE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v7, v2}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->m:Lcom/google/r/b/a/bq;

    .line 4250
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "UPDATE_DIRECTIONS_STATE_WITH_LOCATION_DESCRIPTOR"

    const/16 v2, 0xd

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->n:Lcom/google/r/b/a/bq;

    .line 4254
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "UPDATE_DIRECTIONS_STATE_WITH_WAYPOINT_ON_MAP"

    const/16 v2, 0xe

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->o:Lcom/google/r/b/a/bq;

    .line 4258
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "INVOKE_EXTERNAL_APP"

    const/16 v2, 0xf

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->p:Lcom/google/r/b/a/bq;

    .line 4262
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "LOAD_USER_REVIEWS"

    const/16 v2, 0x10

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->q:Lcom/google/r/b/a/bq;

    .line 4266
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "LOAD_NEARBY_STATION"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v8, v2}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->r:Lcom/google/r/b/a/bq;

    .line 4270
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "START_FEEDBACK_FLOW"

    const/16 v2, 0x12

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->s:Lcom/google/r/b/a/bq;

    .line 4274
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "TACTILE_SEARCH"

    const/16 v2, 0x13

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->t:Lcom/google/r/b/a/bq;

    .line 4278
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "SHOW_PLACE_WITH_ALIAS_AND_QUERY"

    const/16 v2, 0x14

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->u:Lcom/google/r/b/a/bq;

    .line 4282
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "SELECT_MY_MAP"

    const/16 v2, 0x15

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->v:Lcom/google/r/b/a/bq;

    .line 4286
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "OPEN_MY_MAPS_LIST"

    const/16 v2, 0x16

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->w:Lcom/google/r/b/a/bq;

    .line 4290
    new-instance v0, Lcom/google/r/b/a/bq;

    const-string v1, "OPEN_CITY_EXPERT_DIALOG"

    const/16 v2, 0x17

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/bq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/bq;->x:Lcom/google/r/b/a/bq;

    .line 4193
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/google/r/b/a/bq;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/r/b/a/bq;->a:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/r/b/a/bq;->b:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/r/b/a/bq;->c:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/r/b/a/bq;->d:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/r/b/a/bq;->e:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/bq;->f:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/r/b/a/bq;->g:Lcom/google/r/b/a/bq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/bq;->h:Lcom/google/r/b/a/bq;

    aput-object v1, v0, v5

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/bq;->i:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/r/b/a/bq;->j:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/r/b/a/bq;->k:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/r/b/a/bq;->l:Lcom/google/r/b/a/bq;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/bq;->m:Lcom/google/r/b/a/bq;

    aput-object v1, v0, v7

    const/16 v1, 0xd

    sget-object v2, Lcom/google/r/b/a/bq;->n:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/r/b/a/bq;->o:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/r/b/a/bq;->p:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/r/b/a/bq;->q:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/r/b/a/bq;->r:Lcom/google/r/b/a/bq;

    aput-object v1, v0, v8

    const/16 v1, 0x12

    sget-object v2, Lcom/google/r/b/a/bq;->s:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/r/b/a/bq;->t:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/r/b/a/bq;->u:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/r/b/a/bq;->v:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/r/b/a/bq;->w:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/r/b/a/bq;->x:Lcom/google/r/b/a/bq;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/bq;->z:[Lcom/google/r/b/a/bq;

    .line 4430
    new-instance v0, Lcom/google/r/b/a/br;

    invoke-direct {v0}, Lcom/google/r/b/a/br;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 4439
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4440
    iput p3, p0, Lcom/google/r/b/a/bq;->y:I

    .line 4441
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/bq;
    .locals 1

    .prologue
    .line 4396
    packed-switch p0, :pswitch_data_0

    .line 4421
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4397
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/bq;->a:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4398
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/bq;->b:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4399
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/bq;->c:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4400
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/bq;->d:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4401
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/bq;->e:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4402
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/bq;->f:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4403
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/bq;->g:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4404
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/bq;->h:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4405
    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/bq;->i:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4406
    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/bq;->j:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4407
    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/bq;->k:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4408
    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/bq;->l:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4409
    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/bq;->m:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4410
    :pswitch_e
    sget-object v0, Lcom/google/r/b/a/bq;->n:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4411
    :pswitch_f
    sget-object v0, Lcom/google/r/b/a/bq;->o:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4412
    :pswitch_10
    sget-object v0, Lcom/google/r/b/a/bq;->p:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4413
    :pswitch_11
    sget-object v0, Lcom/google/r/b/a/bq;->q:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4414
    :pswitch_12
    sget-object v0, Lcom/google/r/b/a/bq;->r:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4415
    :pswitch_13
    sget-object v0, Lcom/google/r/b/a/bq;->s:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4416
    :pswitch_14
    sget-object v0, Lcom/google/r/b/a/bq;->t:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4417
    :pswitch_15
    sget-object v0, Lcom/google/r/b/a/bq;->u:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4418
    :pswitch_16
    sget-object v0, Lcom/google/r/b/a/bq;->v:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4419
    :pswitch_17
    sget-object v0, Lcom/google/r/b/a/bq;->w:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4420
    :pswitch_18
    sget-object v0, Lcom/google/r/b/a/bq;->x:Lcom/google/r/b/a/bq;

    goto :goto_0

    .line 4396
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_d
        :pswitch_13
        :pswitch_e
        :pswitch_f
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/bq;
    .locals 1

    .prologue
    .line 4193
    const-class v0, Lcom/google/r/b/a/bq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bq;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/bq;
    .locals 1

    .prologue
    .line 4193
    sget-object v0, Lcom/google/r/b/a/bq;->z:[Lcom/google/r/b/a/bq;

    invoke-virtual {v0}, [Lcom/google/r/b/a/bq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/bq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 4392
    iget v0, p0, Lcom/google/r/b/a/bq;->y:I

    return v0
.end method
