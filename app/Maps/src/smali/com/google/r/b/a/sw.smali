.class public final Lcom/google/r/b/a/sw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/sz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/sw;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/sw;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1226
    new-instance v0, Lcom/google/r/b/a/sx;

    invoke-direct {v0}, Lcom/google/r/b/a/sx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/sw;->PARSER:Lcom/google/n/ax;

    .line 1294
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/sw;->f:Lcom/google/n/aw;

    .line 1454
    new-instance v0, Lcom/google/r/b/a/sw;

    invoke-direct {v0}, Lcom/google/r/b/a/sw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/sw;->c:Lcom/google/r/b/a/sw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1177
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1258
    iput-byte v0, p0, Lcom/google/r/b/a/sw;->d:B

    .line 1277
    iput v0, p0, Lcom/google/r/b/a/sw;->e:I

    .line 1178
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/sw;->b:I

    .line 1179
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1185
    invoke-direct {p0}, Lcom/google/r/b/a/sw;-><init>()V

    .line 1186
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1190
    const/4 v0, 0x0

    .line 1191
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 1192
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1193
    sparse-switch v3, :sswitch_data_0

    .line 1198
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1200
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1196
    goto :goto_0

    .line 1205
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1206
    invoke-static {v3}, Lcom/google/r/b/a/ta;->a(I)Lcom/google/r/b/a/ta;

    move-result-object v4

    .line 1207
    if-nez v4, :cond_1

    .line 1208
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1217
    :catch_0
    move-exception v0

    .line 1218
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1223
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/sw;->au:Lcom/google/n/bn;

    throw v0

    .line 1210
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/sw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/sw;->a:I

    .line 1211
    iput v3, p0, Lcom/google/r/b/a/sw;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1219
    :catch_1
    move-exception v0

    .line 1220
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1221
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1223
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/sw;->au:Lcom/google/n/bn;

    .line 1224
    return-void

    .line 1193
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1175
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1258
    iput-byte v0, p0, Lcom/google/r/b/a/sw;->d:B

    .line 1277
    iput v0, p0, Lcom/google/r/b/a/sw;->e:I

    .line 1176
    return-void
.end method

.method public static d()Lcom/google/r/b/a/sw;
    .locals 1

    .prologue
    .line 1457
    sget-object v0, Lcom/google/r/b/a/sw;->c:Lcom/google/r/b/a/sw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/sy;
    .locals 1

    .prologue
    .line 1356
    new-instance v0, Lcom/google/r/b/a/sy;

    invoke-direct {v0}, Lcom/google/r/b/a/sy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/sw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1238
    sget-object v0, Lcom/google/r/b/a/sw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1270
    invoke-virtual {p0}, Lcom/google/r/b/a/sw;->c()I

    .line 1271
    iget v0, p0, Lcom/google/r/b/a/sw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1272
    iget v0, p0, Lcom/google/r/b/a/sw;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 1274
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/sw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1275
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1260
    iget-byte v1, p0, Lcom/google/r/b/a/sw;->d:B

    .line 1261
    if-ne v1, v0, :cond_0

    .line 1265
    :goto_0
    return v0

    .line 1262
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1264
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/sw;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1279
    iget v1, p0, Lcom/google/r/b/a/sw;->e:I

    .line 1280
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 1289
    :goto_0
    return v0

    .line 1283
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/sw;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 1284
    iget v1, p0, Lcom/google/r/b/a/sw;->b:I

    .line 1285
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_2

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1287
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/sw;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1288
    iput v0, p0, Lcom/google/r/b/a/sw;->e:I

    goto :goto_0

    .line 1285
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1169
    invoke-static {}, Lcom/google/r/b/a/sw;->newBuilder()Lcom/google/r/b/a/sy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/sy;->a(Lcom/google/r/b/a/sw;)Lcom/google/r/b/a/sy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1169
    invoke-static {}, Lcom/google/r/b/a/sw;->newBuilder()Lcom/google/r/b/a/sy;

    move-result-object v0

    return-object v0
.end method
