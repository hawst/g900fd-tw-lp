.class public final enum Lcom/google/r/b/a/zg;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/zg;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/zg;

.field public static final enum b:Lcom/google/r/b/a/zg;

.field public static final enum c:Lcom/google/r/b/a/zg;

.field private static final synthetic e:[Lcom/google/r/b/a/zg;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 440
    new-instance v0, Lcom/google/r/b/a/zg;

    const-string v1, "CLICKED_SUGGESTION"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/r/b/a/zg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/zg;->a:Lcom/google/r/b/a/zg;

    .line 444
    new-instance v0, Lcom/google/r/b/a/zg;

    const-string v1, "ENTER_KEY"

    invoke-direct {v0, v1, v3, v6}, Lcom/google/r/b/a/zg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/zg;->b:Lcom/google/r/b/a/zg;

    .line 448
    new-instance v0, Lcom/google/r/b/a/zg;

    const-string v1, "SPEECH_RECOGNITION"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v5, v2}, Lcom/google/r/b/a/zg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/zg;->c:Lcom/google/r/b/a/zg;

    .line 435
    new-array v0, v6, [Lcom/google/r/b/a/zg;

    sget-object v1, Lcom/google/r/b/a/zg;->a:Lcom/google/r/b/a/zg;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/zg;->b:Lcom/google/r/b/a/zg;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/zg;->c:Lcom/google/r/b/a/zg;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/zg;->e:[Lcom/google/r/b/a/zg;

    .line 483
    new-instance v0, Lcom/google/r/b/a/zh;

    invoke-direct {v0}, Lcom/google/r/b/a/zh;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 492
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 493
    iput p3, p0, Lcom/google/r/b/a/zg;->d:I

    .line 494
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/zg;
    .locals 1

    .prologue
    .line 470
    sparse-switch p0, :sswitch_data_0

    .line 474
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 471
    :sswitch_0
    sget-object v0, Lcom/google/r/b/a/zg;->a:Lcom/google/r/b/a/zg;

    goto :goto_0

    .line 472
    :sswitch_1
    sget-object v0, Lcom/google/r/b/a/zg;->b:Lcom/google/r/b/a/zg;

    goto :goto_0

    .line 473
    :sswitch_2
    sget-object v0, Lcom/google/r/b/a/zg;->c:Lcom/google/r/b/a/zg;

    goto :goto_0

    .line 470
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0xf -> :sswitch_2
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/zg;
    .locals 1

    .prologue
    .line 435
    const-class v0, Lcom/google/r/b/a/zg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/zg;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/zg;
    .locals 1

    .prologue
    .line 435
    sget-object v0, Lcom/google/r/b/a/zg;->e:[Lcom/google/r/b/a/zg;

    invoke-virtual {v0}, [Lcom/google/r/b/a/zg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/zg;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 466
    iget v0, p0, Lcom/google/r/b/a/zg;->d:I

    return v0
.end method
