.class public final Lcom/google/r/b/a/ik;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/il;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ii;",
        "Lcom/google/r/b/a/ik;",
        ">;",
        "Lcom/google/r/b/a/il;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 459
    sget-object v0, Lcom/google/r/b/a/ii;->j:Lcom/google/r/b/a/ii;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 634
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ik;->b:Lcom/google/n/ao;

    .line 693
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ik;->c:Lcom/google/n/ao;

    .line 752
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ik;->d:Lcom/google/n/ao;

    .line 811
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ik;->e:Lcom/google/n/ao;

    .line 870
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ik;->f:Lcom/google/n/ao;

    .line 929
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ik;->g:Lcom/google/n/ao;

    .line 988
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ik;->h:Lcom/google/n/ao;

    .line 1047
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ik;->i:Lcom/google/n/ao;

    .line 460
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 451
    new-instance v2, Lcom/google/r/b/a/ii;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ii;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/ii;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ik;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ik;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/ii;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ik;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ik;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/ii;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ik;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ik;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/ii;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ik;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ik;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/r/b/a/ii;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ik;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ik;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/ii;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ik;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ik;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/ii;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ik;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ik;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v3, v2, Lcom/google/r/b/a/ii;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/ik;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/ik;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/ii;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 451
    check-cast p1, Lcom/google/r/b/a/ii;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ik;->a(Lcom/google/r/b/a/ii;)Lcom/google/r/b/a/ik;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ii;)Lcom/google/r/b/a/ik;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 543
    invoke-static {}, Lcom/google/r/b/a/ii;->d()Lcom/google/r/b/a/ii;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 577
    :goto_0
    return-object p0

    .line 544
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ii;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 545
    iget-object v2, p0, Lcom/google/r/b/a/ik;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ii;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 546
    iget v2, p0, Lcom/google/r/b/a/ik;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ik;->a:I

    .line 548
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ii;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 549
    iget-object v2, p0, Lcom/google/r/b/a/ik;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ii;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 550
    iget v2, p0, Lcom/google/r/b/a/ik;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ik;->a:I

    .line 552
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ii;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 553
    iget-object v2, p0, Lcom/google/r/b/a/ik;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ii;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 554
    iget v2, p0, Lcom/google/r/b/a/ik;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/ik;->a:I

    .line 556
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ii;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 557
    iget-object v2, p0, Lcom/google/r/b/a/ik;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ii;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 558
    iget v2, p0, Lcom/google/r/b/a/ik;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/ik;->a:I

    .line 560
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/ii;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 561
    iget-object v2, p0, Lcom/google/r/b/a/ik;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ii;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 562
    iget v2, p0, Lcom/google/r/b/a/ik;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/ik;->a:I

    .line 564
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/ii;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 565
    iget-object v2, p0, Lcom/google/r/b/a/ik;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ii;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 566
    iget v2, p0, Lcom/google/r/b/a/ik;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/ik;->a:I

    .line 568
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/ii;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 569
    iget-object v2, p0, Lcom/google/r/b/a/ik;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ii;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 570
    iget v2, p0, Lcom/google/r/b/a/ik;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/ik;->a:I

    .line 572
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/ii;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 573
    iget-object v0, p0, Lcom/google/r/b/a/ik;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/ii;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 574
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/ik;->a:I

    .line 576
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/ii;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 544
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 548
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 552
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 556
    goto/16 :goto_4

    :cond_d
    move v2, v1

    .line 560
    goto :goto_5

    :cond_e
    move v2, v1

    .line 564
    goto :goto_6

    :cond_f
    move v2, v1

    .line 568
    goto :goto_7

    :cond_10
    move v0, v1

    .line 572
    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 581
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 582
    iget-object v0, p0, Lcom/google/r/b/a/ik;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hr;->d()Lcom/google/r/b/a/hr;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hr;

    invoke-virtual {v0}, Lcom/google/r/b/a/hr;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 629
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 581
    goto :goto_0

    .line 587
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 588
    iget-object v0, p0, Lcom/google/r/b/a/ik;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ann;->d()Lcom/google/r/b/a/ann;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ann;

    invoke-virtual {v0}, Lcom/google/r/b/a/ann;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 590
    goto :goto_1

    :cond_2
    move v0, v1

    .line 587
    goto :goto_2

    .line 593
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 594
    iget-object v0, p0, Lcom/google/r/b/a/ik;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/rx;->d()Lcom/google/r/b/a/rx;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/rx;

    invoke-virtual {v0}, Lcom/google/r/b/a/rx;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 596
    goto :goto_1

    :cond_4
    move v0, v1

    .line 593
    goto :goto_3

    .line 599
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    .line 600
    iget-object v0, p0, Lcom/google/r/b/a/ik;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/eo;->d()Lcom/google/r/b/a/eo;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/eo;

    invoke-virtual {v0}, Lcom/google/r/b/a/eo;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 602
    goto :goto_1

    :cond_6
    move v0, v1

    .line 599
    goto :goto_4

    .line 605
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_8

    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    .line 606
    iget-object v0, p0, Lcom/google/r/b/a/ik;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/pa;->d()Lcom/google/r/b/a/pa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/pa;

    invoke-virtual {v0}, Lcom/google/r/b/a/pa;->b()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 608
    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 605
    goto :goto_5

    .line 611
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_a

    move v0, v2

    :goto_6
    if-eqz v0, :cond_b

    .line 612
    iget-object v0, p0, Lcom/google/r/b/a/ik;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aoz;->d()Lcom/google/r/b/a/aoz;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aoz;

    invoke-virtual {v0}, Lcom/google/r/b/a/aoz;->b()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 614
    goto/16 :goto_1

    :cond_a
    move v0, v1

    .line 611
    goto :goto_6

    .line 617
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_c

    move v0, v2

    :goto_7
    if-eqz v0, :cond_d

    .line 618
    iget-object v0, p0, Lcom/google/r/b/a/ik;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ue;->d()Lcom/google/r/b/a/ue;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ue;

    invoke-virtual {v0}, Lcom/google/r/b/a/ue;->b()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    .line 620
    goto/16 :goto_1

    :cond_c
    move v0, v1

    .line 617
    goto :goto_7

    .line 623
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/ik;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_e

    move v0, v2

    :goto_8
    if-eqz v0, :cond_f

    .line 624
    iget-object v0, p0, Lcom/google/r/b/a/ik;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/tm;->d()Lcom/google/r/b/a/tm;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/tm;

    invoke-virtual {v0}, Lcom/google/r/b/a/tm;->b()Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    .line 626
    goto/16 :goto_1

    :cond_e
    move v0, v1

    .line 623
    goto :goto_8

    :cond_f
    move v0, v2

    .line 629
    goto/16 :goto_1
.end method
