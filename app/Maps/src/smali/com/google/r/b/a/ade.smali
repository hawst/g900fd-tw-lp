.class public final Lcom/google/r/b/a/ade;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/adh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ade;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/ade;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field public f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13097
    new-instance v0, Lcom/google/r/b/a/adf;

    invoke-direct {v0}, Lcom/google/r/b/a/adf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ade;->PARSER:Lcom/google/n/ax;

    .line 13282
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ade;->j:Lcom/google/n/aw;

    .line 13738
    new-instance v0, Lcom/google/r/b/a/ade;

    invoke-direct {v0}, Lcom/google/r/b/a/ade;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ade;->g:Lcom/google/r/b/a/ade;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 13029
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 13114
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    .line 13172
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ade;->d:Lcom/google/n/ao;

    .line 13188
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ade;->e:Lcom/google/n/ao;

    .line 13218
    iput-byte v3, p0, Lcom/google/r/b/a/ade;->h:B

    .line 13249
    iput v3, p0, Lcom/google/r/b/a/ade;->i:I

    .line 13030
    iget-object v0, p0, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 13031
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ade;->c:Ljava/lang/Object;

    .line 13032
    iget-object v0, p0, Lcom/google/r/b/a/ade;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 13033
    iget-object v0, p0, Lcom/google/r/b/a/ade;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 13034
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ade;->f:I

    .line 13035
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 13041
    invoke-direct {p0}, Lcom/google/r/b/a/ade;-><init>()V

    .line 13042
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 13047
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 13048
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 13049
    sparse-switch v3, :sswitch_data_0

    .line 13054
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 13056
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 13052
    goto :goto_0

    .line 13061
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 13062
    iget v3, p0, Lcom/google/r/b/a/ade;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ade;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 13088
    :catch_0
    move-exception v0

    .line 13089
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13094
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ade;->au:Lcom/google/n/bn;

    throw v0

    .line 13066
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 13067
    iget v4, p0, Lcom/google/r/b/a/ade;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/ade;->a:I

    .line 13068
    iput-object v3, p0, Lcom/google/r/b/a/ade;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 13090
    :catch_1
    move-exception v0

    .line 13091
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 13092
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 13072
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/ade;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 13073
    iget v3, p0, Lcom/google/r/b/a/ade;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/ade;->a:I

    goto :goto_0

    .line 13077
    :sswitch_4
    iget-object v3, p0, Lcom/google/r/b/a/ade;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 13078
    iget v3, p0, Lcom/google/r/b/a/ade;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/ade;->a:I

    goto :goto_0

    .line 13082
    :sswitch_5
    iget v3, p0, Lcom/google/r/b/a/ade;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/ade;->a:I

    .line 13083
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/ade;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 13094
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ade;->au:Lcom/google/n/bn;

    .line 13095
    return-void

    .line 13049
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 13027
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 13114
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    .line 13172
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ade;->d:Lcom/google/n/ao;

    .line 13188
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ade;->e:Lcom/google/n/ao;

    .line 13218
    iput-byte v1, p0, Lcom/google/r/b/a/ade;->h:B

    .line 13249
    iput v1, p0, Lcom/google/r/b/a/ade;->i:I

    .line 13028
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ade;
    .locals 1

    .prologue
    .line 13741
    sget-object v0, Lcom/google/r/b/a/ade;->g:Lcom/google/r/b/a/ade;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/adg;
    .locals 1

    .prologue
    .line 13344
    new-instance v0, Lcom/google/r/b/a/adg;

    invoke-direct {v0}, Lcom/google/r/b/a/adg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ade;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13109
    sget-object v0, Lcom/google/r/b/a/ade;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 13230
    invoke-virtual {p0}, Lcom/google/r/b/a/ade;->c()I

    .line 13231
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 13232
    iget-object v0, p0, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13234
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 13235
    iget-object v0, p0, Lcom/google/r/b/a/ade;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ade;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13237
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 13238
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/ade;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13240
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 13241
    iget-object v0, p0, Lcom/google/r/b/a/ade;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13243
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 13244
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/ade;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 13246
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/ade;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 13247
    return-void

    .line 13235
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 13220
    iget-byte v1, p0, Lcom/google/r/b/a/ade;->h:B

    .line 13221
    if-ne v1, v0, :cond_0

    .line 13225
    :goto_0
    return v0

    .line 13222
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 13224
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ade;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13251
    iget v0, p0, Lcom/google/r/b/a/ade;->i:I

    .line 13252
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13277
    :goto_0
    return v0

    .line 13255
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 13256
    iget-object v0, p0, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    .line 13257
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 13259
    :goto_1
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 13261
    iget-object v0, p0, Lcom/google/r/b/a/ade;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ade;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 13263
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 13264
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/ade;->d:Lcom/google/n/ao;

    .line 13265
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 13267
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 13268
    iget-object v0, p0, Lcom/google/r/b/a/ade;->e:Lcom/google/n/ao;

    .line 13269
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 13271
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 13272
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/r/b/a/ade;->f:I

    .line 13273
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 13275
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/ade;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 13276
    iput v0, p0, Lcom/google/r/b/a/ade;->i:I

    goto/16 :goto_0

    .line 13261
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 13273
    :cond_6
    const/16 v0, 0xa

    goto :goto_3

    :cond_7
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13021
    invoke-static {}, Lcom/google/r/b/a/ade;->newBuilder()Lcom/google/r/b/a/adg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/adg;->a(Lcom/google/r/b/a/ade;)Lcom/google/r/b/a/adg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13021
    invoke-static {}, Lcom/google/r/b/a/ade;->newBuilder()Lcom/google/r/b/a/adg;

    move-result-object v0

    return-object v0
.end method
