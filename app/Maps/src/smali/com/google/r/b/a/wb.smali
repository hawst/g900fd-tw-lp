.class public final Lcom/google/r/b/a/wb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/we;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/vz;",
        "Lcom/google/r/b/a/wb;",
        ">;",
        "Lcom/google/r/b/a/we;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1979
    sget-object v0, Lcom/google/r/b/a/vz;->d:Lcom/google/r/b/a/vz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2037
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/wb;->b:I

    .line 2073
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/wb;->c:Lcom/google/n/ao;

    .line 1980
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1971
    new-instance v2, Lcom/google/r/b/a/vz;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/vz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/wb;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/wb;->b:I

    iput v4, v2, Lcom/google/r/b/a/vz;->b:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v3, v2, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/wb;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/wb;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/vz;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1971
    check-cast p1, Lcom/google/r/b/a/vz;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/wb;->a(Lcom/google/r/b/a/vz;)Lcom/google/r/b/a/wb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/vz;)Lcom/google/r/b/a/wb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2013
    invoke-static {}, Lcom/google/r/b/a/vz;->d()Lcom/google/r/b/a/vz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2022
    :goto_0
    return-object p0

    .line 2014
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/vz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 2015
    iget v2, p1, Lcom/google/r/b/a/vz;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/wc;->a(I)Lcom/google/r/b/a/wc;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/wc;->a:Lcom/google/r/b/a/wc;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 2014
    goto :goto_1

    .line 2015
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/wb;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/wb;->a:I

    iget v2, v2, Lcom/google/r/b/a/wc;->e:I

    iput v2, p0, Lcom/google/r/b/a/wb;->b:I

    .line 2017
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/vz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 2018
    iget-object v0, p0, Lcom/google/r/b/a/wb;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2019
    iget v0, p0, Lcom/google/r/b/a/wb;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/wb;->a:I

    .line 2021
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/vz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 2017
    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2026
    iget v0, p0, Lcom/google/r/b/a/wb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 2027
    iget-object v0, p0, Lcom/google/r/b/a/wb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hh;->d()Lcom/google/r/b/a/hh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hh;

    invoke-virtual {v0}, Lcom/google/r/b/a/hh;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2032
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 2026
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2032
    goto :goto_1
.end method
