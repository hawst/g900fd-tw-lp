.class public final Lcom/google/r/b/a/uv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/uw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ut;",
        "Lcom/google/r/b/a/uv;",
        ">;",
        "Lcom/google/r/b/a/uw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 392
    sget-object v0, Lcom/google/r/b/a/ut;->g:Lcom/google/r/b/a/ut;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 490
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/uv;->b:I

    .line 554
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    .line 620
    const/16 v0, 0x16

    iput v0, p0, Lcom/google/r/b/a/uv;->e:I

    .line 653
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    .line 393
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 384
    new-instance v2, Lcom/google/r/b/a/ut;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ut;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/uv;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/uv;->b:I

    iput v1, v2, Lcom/google/r/b/a/ut;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/uv;->c:I

    iput v1, v2, Lcom/google/r/b/a/ut;->c:I

    iget v1, p0, Lcom/google/r/b/a/uv;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/uv;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/uv;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/uv;->e:I

    iput v1, v2, Lcom/google/r/b/a/ut;->e:I

    iget v1, p0, Lcom/google/r/b/a/uv;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/uv;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/r/b/a/uv;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/ut;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 384
    check-cast p1, Lcom/google/r/b/a/ut;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/uv;->a(Lcom/google/r/b/a/ut;)Lcom/google/r/b/a/uv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ut;)Lcom/google/r/b/a/uv;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 444
    invoke-static {}, Lcom/google/r/b/a/ut;->d()Lcom/google/r/b/a/ut;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 475
    :goto_0
    return-object p0

    .line 445
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 446
    iget v2, p1, Lcom/google/r/b/a/ut;->b:I

    iget v3, p0, Lcom/google/r/b/a/uv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/uv;->a:I

    iput v2, p0, Lcom/google/r/b/a/uv;->b:I

    .line 448
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 449
    iget v2, p1, Lcom/google/r/b/a/ut;->c:I

    iget v3, p0, Lcom/google/r/b/a/uv;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/uv;->a:I

    iput v2, p0, Lcom/google/r/b/a/uv;->c:I

    .line 451
    :cond_2
    iget-object v2, p1, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 452
    iget-object v2, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 453
    iget-object v2, p1, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    .line 454
    iget v2, p0, Lcom/google/r/b/a/uv;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/r/b/a/uv;->a:I

    .line 461
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_a

    :goto_4
    if-eqz v0, :cond_4

    .line 462
    iget v0, p1, Lcom/google/r/b/a/ut;->e:I

    iget v1, p0, Lcom/google/r/b/a/uv;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/uv;->a:I

    iput v0, p0, Lcom/google/r/b/a/uv;->e:I

    .line 464
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 465
    iget-object v0, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 466
    iget-object v0, p1, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    .line 467
    iget v0, p0, Lcom/google/r/b/a/uv;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/r/b/a/uv;->a:I

    .line 474
    :cond_5
    :goto_5
    iget-object v0, p1, Lcom/google/r/b/a/ut;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 445
    goto :goto_1

    :cond_7
    move v2, v1

    .line 448
    goto :goto_2

    .line 456
    :cond_8
    iget v2, p0, Lcom/google/r/b/a/uv;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/uv;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/uv;->a:I

    .line 457
    :cond_9
    iget-object v2, p0, Lcom/google/r/b/a/uv;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_a
    move v0, v1

    .line 461
    goto :goto_4

    .line 469
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/uv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_c

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/uv;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/uv;->a:I

    .line 470
    :cond_c
    iget-object v0, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 479
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/google/r/b/a/uv;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/oa;->d()Lcom/google/r/b/a/oa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/oa;

    invoke-virtual {v0}, Lcom/google/r/b/a/oa;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 485
    :goto_1
    return v2

    .line 479
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 485
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
