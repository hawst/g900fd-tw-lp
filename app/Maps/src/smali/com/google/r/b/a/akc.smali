.class public final Lcom/google/r/b/a/akc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/akd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aka;",
        "Lcom/google/r/b/a/akc;",
        ">;",
        "Lcom/google/r/b/a/akd;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 346
    sget-object v0, Lcom/google/r/b/a/aka;->f:Lcom/google/r/b/a/aka;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 422
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/akc;->b:Lcom/google/n/ao;

    .line 481
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/akc;->c:Lcom/google/n/ao;

    .line 540
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/akc;->d:Lcom/google/n/ao;

    .line 599
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/akc;->e:I

    .line 347
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 338
    new-instance v2, Lcom/google/r/b/a/aka;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aka;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/akc;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/aka;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/akc;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/akc;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/aka;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/akc;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/akc;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/aka;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/akc;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/akc;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/akc;->e:I

    iput v1, v2, Lcom/google/r/b/a/aka;->e:I

    iput v0, v2, Lcom/google/r/b/a/aka;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 338
    check-cast p1, Lcom/google/r/b/a/aka;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/akc;->a(Lcom/google/r/b/a/aka;)Lcom/google/r/b/a/akc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/maps/a/a;)Lcom/google/r/b/a/akc;
    .locals 2

    .prologue
    .line 496
    if-nez p1, :cond_0

    .line 497
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/akc;->c:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 501
    iget v0, p0, Lcom/google/r/b/a/akc;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/akc;->a:I

    .line 502
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/d/d;)Lcom/google/r/b/a/akc;
    .locals 1

    .prologue
    .line 617
    if-nez p1, :cond_0

    .line 618
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 620
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/akc;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/akc;->a:I

    .line 621
    iget v0, p1, Lcom/google/maps/g/d/d;->c:I

    iput v0, p0, Lcom/google/r/b/a/akc;->e:I

    .line 623
    return-object p0
.end method

.method public final a(Lcom/google/r/b/a/aka;)Lcom/google/r/b/a/akc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 396
    invoke-static {}, Lcom/google/r/b/a/aka;->d()Lcom/google/r/b/a/aka;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 413
    :goto_0
    return-object p0

    .line 397
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 398
    iget-object v2, p0, Lcom/google/r/b/a/akc;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aka;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 399
    iget v2, p0, Lcom/google/r/b/a/akc;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/akc;->a:I

    .line 401
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 402
    iget-object v2, p0, Lcom/google/r/b/a/akc;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aka;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 403
    iget v2, p0, Lcom/google/r/b/a/akc;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/akc;->a:I

    .line 405
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 406
    iget-object v2, p0, Lcom/google/r/b/a/akc;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aka;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 407
    iget v2, p0, Lcom/google/r/b/a/akc;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/akc;->a:I

    .line 409
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    :goto_4
    if-eqz v0, :cond_5

    .line 410
    iget v0, p1, Lcom/google/r/b/a/aka;->e:I

    invoke-static {v0}, Lcom/google/maps/g/d/d;->a(I)Lcom/google/maps/g/d/d;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/d/d;->a:Lcom/google/maps/g/d/d;

    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/r/b/a/akc;->a(Lcom/google/maps/g/d/d;)Lcom/google/r/b/a/akc;

    .line 412
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/aka;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 397
    goto :goto_1

    :cond_7
    move v2, v1

    .line 401
    goto :goto_2

    :cond_8
    move v2, v1

    .line 405
    goto :goto_3

    :cond_9
    move v0, v1

    .line 409
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x1

    return v0
.end method
