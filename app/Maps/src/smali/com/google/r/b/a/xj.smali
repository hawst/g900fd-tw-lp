.class public final Lcom/google/r/b/a/xj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/xm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/xh;",
        "Lcom/google/r/b/a/xj;",
        ">;",
        "Lcom/google/r/b/a/xm;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 351
    sget-object v0, Lcom/google/r/b/a/xh;->d:Lcom/google/r/b/a/xh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 406
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/xj;->b:I

    .line 442
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/xj;->c:Ljava/lang/Object;

    .line 352
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 343
    new-instance v2, Lcom/google/r/b/a/xh;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/xh;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/xj;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/xj;->b:I

    iput v1, v2, Lcom/google/r/b/a/xh;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/xj;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/xh;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 343
    check-cast p1, Lcom/google/r/b/a/xh;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/xj;->a(Lcom/google/r/b/a/xh;)Lcom/google/r/b/a/xj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/xh;)Lcom/google/r/b/a/xj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 383
    invoke-static {}, Lcom/google/r/b/a/xh;->g()Lcom/google/r/b/a/xh;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 393
    :goto_0
    return-object p0

    .line 384
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/xh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 385
    iget v2, p1, Lcom/google/r/b/a/xh;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/xk;->a(I)Lcom/google/r/b/a/xk;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/xk;->a:Lcom/google/r/b/a/xk;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 384
    goto :goto_1

    .line 385
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/xj;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/xj;->a:I

    iget v2, v2, Lcom/google/r/b/a/xk;->e:I

    iput v2, p0, Lcom/google/r/b/a/xj;->b:I

    .line 387
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/xh;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 388
    iget v0, p0, Lcom/google/r/b/a/xj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/xj;->a:I

    .line 389
    iget-object v0, p1, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/xj;->c:Ljava/lang/Object;

    .line 392
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/xh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 387
    goto :goto_2
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 397
    iget v2, p0, Lcom/google/r/b/a/xj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 401
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 397
    goto :goto_0

    :cond_1
    move v0, v1

    .line 401
    goto :goto_1
.end method
