.class public final Lcom/google/r/b/a/fd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/fg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/fd;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/r/b/a/fd;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 261
    new-instance v0, Lcom/google/r/b/a/fe;

    invoke-direct {v0}, Lcom/google/r/b/a/fe;-><init>()V

    sput-object v0, Lcom/google/r/b/a/fd;->PARSER:Lcom/google/n/ax;

    .line 600
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/fd;->k:Lcom/google/n/aw;

    .line 1237
    new-instance v0, Lcom/google/r/b/a/fd;

    invoke-direct {v0}, Lcom/google/r/b/a/fd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/fd;->h:Lcom/google/r/b/a/fd;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 182
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 529
    iput-byte v0, p0, Lcom/google/r/b/a/fd;->i:B

    .line 563
    iput v0, p0, Lcom/google/r/b/a/fd;->j:I

    .line 183
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/fd;->b:Ljava/lang/Object;

    .line 184
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/fd;->c:Ljava/lang/Object;

    .line 185
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    .line 186
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    .line 187
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    .line 188
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    .line 189
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 195
    invoke-direct {p0}, Lcom/google/r/b/a/fd;-><init>()V

    .line 196
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 200
    const/4 v0, 0x0

    .line 201
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 202
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 203
    sparse-switch v3, :sswitch_data_0

    .line 208
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 210
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 206
    goto :goto_0

    .line 215
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 216
    iget v4, p0, Lcom/google/r/b/a/fd;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/fd;->a:I

    .line 217
    iput-object v3, p0, Lcom/google/r/b/a/fd;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/fd;->au:Lcom/google/n/bn;

    throw v0

    .line 221
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 222
    iget v4, p0, Lcom/google/r/b/a/fd;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/fd;->a:I

    .line 223
    iput-object v3, p0, Lcom/google/r/b/a/fd;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 254
    :catch_1
    move-exception v0

    .line 255
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 256
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 227
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 228
    iget v4, p0, Lcom/google/r/b/a/fd;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/fd;->a:I

    .line 229
    iput-object v3, p0, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    goto :goto_0

    .line 233
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 234
    iget v4, p0, Lcom/google/r/b/a/fd;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/fd;->a:I

    .line 235
    iput-object v3, p0, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    goto :goto_0

    .line 239
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 240
    iget v4, p0, Lcom/google/r/b/a/fd;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/fd;->a:I

    .line 241
    iput-object v3, p0, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    goto :goto_0

    .line 245
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 246
    iget v4, p0, Lcom/google/r/b/a/fd;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/fd;->a:I

    .line 247
    iput-object v3, p0, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 258
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->au:Lcom/google/n/bn;

    .line 259
    return-void

    .line 203
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 180
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 529
    iput-byte v0, p0, Lcom/google/r/b/a/fd;->i:B

    .line 563
    iput v0, p0, Lcom/google/r/b/a/fd;->j:I

    .line 181
    return-void
.end method

.method public static j()Lcom/google/r/b/a/fd;
    .locals 1

    .prologue
    .line 1240
    sget-object v0, Lcom/google/r/b/a/fd;->h:Lcom/google/r/b/a/fd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ff;
    .locals 1

    .prologue
    .line 662
    new-instance v0, Lcom/google/r/b/a/ff;

    invoke-direct {v0}, Lcom/google/r/b/a/ff;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/fd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273
    sget-object v0, Lcom/google/r/b/a/fd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 541
    invoke-virtual {p0}, Lcom/google/r/b/a/fd;->c()I

    .line 542
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 543
    iget-object v0, p0, Lcom/google/r/b/a/fd;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 545
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 546
    iget-object v0, p0, Lcom/google/r/b/a/fd;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 548
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 549
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 551
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 552
    iget-object v0, p0, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 554
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 555
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 557
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 558
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 560
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/fd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 561
    return-void

    .line 543
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 546
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 549
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 552
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 555
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 558
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 531
    iget-byte v1, p0, Lcom/google/r/b/a/fd;->i:B

    .line 532
    if-ne v1, v0, :cond_0

    .line 536
    :goto_0
    return v0

    .line 533
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 535
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/fd;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 565
    iget v0, p0, Lcom/google/r/b/a/fd;->j:I

    .line 566
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 595
    :goto_0
    return v0

    .line 569
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_c

    .line 571
    iget-object v0, p0, Lcom/google/r/b/a/fd;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 573
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 575
    iget-object v0, p0, Lcom/google/r/b/a/fd;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 577
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 578
    const/4 v3, 0x3

    .line 579
    iget-object v0, p0, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 581
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 583
    iget-object v0, p0, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 585
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 586
    const/4 v3, 0x5

    .line 587
    iget-object v0, p0, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 589
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 590
    const/4 v3, 0x6

    .line 591
    iget-object v0, p0, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 593
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/fd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 594
    iput v0, p0, Lcom/google/r/b/a/fd;->j:I

    goto/16 :goto_0

    .line 571
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 575
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 579
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 583
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 587
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 591
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    :cond_c
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    .line 374
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 375
    check-cast v0, Ljava/lang/String;

    .line 383
    :goto_0
    return-object v0

    .line 377
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 379
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 380
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    iput-object v1, p0, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 383
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 174
    invoke-static {}, Lcom/google/r/b/a/fd;->newBuilder()Lcom/google/r/b/a/ff;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ff;->a(Lcom/google/r/b/a/fd;)Lcom/google/r/b/a/ff;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 174
    invoke-static {}, Lcom/google/r/b/a/fd;->newBuilder()Lcom/google/r/b/a/ff;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    .line 416
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 417
    check-cast v0, Ljava/lang/String;

    .line 425
    :goto_0
    return-object v0

    .line 419
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 421
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 422
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 423
    iput-object v1, p0, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 425
    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    .line 458
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 459
    check-cast v0, Ljava/lang/String;

    .line 467
    :goto_0
    return-object v0

    .line 461
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 463
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 464
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 465
    iput-object v1, p0, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 467
    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    .line 500
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 501
    check-cast v0, Ljava/lang/String;

    .line 509
    :goto_0
    return-object v0

    .line 503
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 505
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 506
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 507
    iput-object v1, p0, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 509
    goto :goto_0
.end method
