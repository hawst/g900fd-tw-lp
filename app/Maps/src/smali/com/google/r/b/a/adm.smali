.class public final Lcom/google/r/b/a/adm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/adn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/adi;",
        "Lcom/google/r/b/a/adm;",
        ">;",
        "Lcom/google/r/b/a/adn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/ao;

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 12623
    sget-object v0, Lcom/google/r/b/a/adi;->f:Lcom/google/r/b/a/adi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 12710
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    .line 12784
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    .line 12857
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/adm;->d:Lcom/google/n/ao;

    .line 12916
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/r/b/a/adm;->e:I

    .line 12624
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12615
    new-instance v2, Lcom/google/r/b/a/adi;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/adi;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/adm;->a:I

    iget v4, p0, Lcom/google/r/b/a/adm;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/adm;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/adm;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/adm;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/adm;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/adm;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/adi;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/adm;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/adm;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/adm;->e:I

    iput v1, v2, Lcom/google/r/b/a/adi;->e:I

    iput v0, v2, Lcom/google/r/b/a/adi;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 12615
    check-cast p1, Lcom/google/r/b/a/adi;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/adm;->a(Lcom/google/r/b/a/adi;)Lcom/google/r/b/a/adm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/adi;)Lcom/google/r/b/a/adm;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 12671
    invoke-static {}, Lcom/google/r/b/a/adi;->d()Lcom/google/r/b/a/adi;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 12700
    :goto_0
    return-object p0

    .line 12672
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 12673
    iget-object v2, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 12674
    iget-object v2, p1, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    .line 12675
    iget v2, p0, Lcom/google/r/b/a/adm;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/adm;->a:I

    .line 12682
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 12683
    iget-object v2, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 12684
    iget-object v2, p1, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    .line 12685
    iget v2, p0, Lcom/google/r/b/a/adm;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/r/b/a/adm;->a:I

    .line 12692
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/r/b/a/adi;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 12693
    iget-object v2, p0, Lcom/google/r/b/a/adm;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/adi;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 12694
    iget v2, p0, Lcom/google/r/b/a/adm;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/adm;->a:I

    .line 12696
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/adi;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_a

    :goto_4
    if-eqz v0, :cond_4

    .line 12697
    iget v0, p1, Lcom/google/r/b/a/adi;->e:I

    iget v1, p0, Lcom/google/r/b/a/adm;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/adm;->a:I

    iput v0, p0, Lcom/google/r/b/a/adm;->e:I

    .line 12699
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/adi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 12677
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/adm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/adm;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/adm;->a:I

    .line 12678
    :cond_6
    iget-object v2, p0, Lcom/google/r/b/a/adm;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 12687
    :cond_7
    iget v2, p0, Lcom/google/r/b/a/adm;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/adm;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/adm;->a:I

    .line 12688
    :cond_8
    iget-object v2, p0, Lcom/google/r/b/a/adm;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v2, v1

    .line 12692
    goto :goto_3

    :cond_a
    move v0, v1

    .line 12696
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 12704
    const/4 v0, 0x1

    return v0
.end method
