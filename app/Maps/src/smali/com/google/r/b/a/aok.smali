.class public final Lcom/google/r/b/a/aok;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aop;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aok;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/aok;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 566
    new-instance v0, Lcom/google/r/b/a/aol;

    invoke-direct {v0}, Lcom/google/r/b/a/aol;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aok;->PARSER:Lcom/google/n/ax;

    .line 757
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aok;->g:Lcom/google/n/aw;

    .line 1077
    new-instance v0, Lcom/google/r/b/a/aok;

    invoke-direct {v0}, Lcom/google/r/b/a/aok;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aok;->d:Lcom/google/r/b/a/aok;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 503
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 708
    iput-byte v0, p0, Lcom/google/r/b/a/aok;->e:B

    .line 736
    iput v0, p0, Lcom/google/r/b/a/aok;->f:I

    .line 504
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aok;->b:I

    .line 505
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    .line 506
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v7, 0x2

    .line 512
    invoke-direct {p0}, Lcom/google/r/b/a/aok;-><init>()V

    .line 515
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 518
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 519
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 520
    sparse-switch v4, :sswitch_data_0

    .line 525
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 527
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 523
    goto :goto_0

    .line 532
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 533
    invoke-static {v4}, Lcom/google/r/b/a/aon;->a(I)Lcom/google/r/b/a/aon;

    move-result-object v5

    .line 534
    if-nez v5, :cond_2

    .line 535
    const/4 v5, 0x3

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 554
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 555
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 560
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 561
    iget-object v1, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    .line 563
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aok;->au:Lcom/google/n/bn;

    throw v0

    .line 537
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/r/b/a/aok;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/aok;->a:I

    .line 538
    iput v4, p0, Lcom/google/r/b/a/aok;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 556
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 557
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 558
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 543
    :sswitch_2
    and-int/lit8 v4, v0, 0x2

    if-eq v4, v7, :cond_3

    .line 544
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    .line 546
    or-int/lit8 v0, v0, 0x2

    .line 548
    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 549
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 548
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 560
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    :cond_4
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_5

    .line 561
    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    .line 563
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aok;->au:Lcom/google/n/bn;

    .line 564
    return-void

    .line 520
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18 -> :sswitch_1
        0x22 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 501
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 708
    iput-byte v0, p0, Lcom/google/r/b/a/aok;->e:B

    .line 736
    iput v0, p0, Lcom/google/r/b/a/aok;->f:I

    .line 502
    return-void
.end method

.method public static g()Lcom/google/r/b/a/aok;
    .locals 1

    .prologue
    .line 1080
    sget-object v0, Lcom/google/r/b/a/aok;->d:Lcom/google/r/b/a/aok;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aom;
    .locals 1

    .prologue
    .line 819
    new-instance v0, Lcom/google/r/b/a/aom;

    invoke-direct {v0}, Lcom/google/r/b/a/aom;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aok;",
            ">;"
        }
    .end annotation

    .prologue
    .line 578
    sget-object v0, Lcom/google/r/b/a/aok;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 726
    invoke-virtual {p0}, Lcom/google/r/b/a/aok;->c()I

    .line 727
    iget v0, p0, Lcom/google/r/b/a/aok;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 728
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/aok;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 730
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 731
    const/4 v2, 0x4

    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 730
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 733
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aok;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 734
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 710
    iget-byte v0, p0, Lcom/google/r/b/a/aok;->e:B

    .line 711
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 721
    :cond_0
    :goto_0
    return v2

    .line 712
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 714
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 715
    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/anw;->i()Lcom/google/r/b/a/anw;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anw;

    invoke-virtual {v0}, Lcom/google/r/b/a/anw;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 716
    iput-byte v2, p0, Lcom/google/r/b/a/aok;->e:B

    goto :goto_0

    .line 714
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 720
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/aok;->e:B

    move v2, v3

    .line 721
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 738
    iget v0, p0, Lcom/google/r/b/a/aok;->f:I

    .line 739
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 752
    :goto_0
    return v0

    .line 742
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aok;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 743
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/r/b/a/aok;->b:I

    .line 744
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_1

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 746
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 747
    const/4 v4, 0x4

    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    .line 748
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 746
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 744
    :cond_1
    const/16 v0, 0xa

    goto :goto_1

    .line 750
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/aok;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 751
    iput v0, p0, Lcom/google/r/b/a/aok;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/anw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 672
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    .line 673
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 674
    iget-object v0, p0, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 675
    invoke-static {}, Lcom/google/r/b/a/anw;->i()Lcom/google/r/b/a/anw;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anw;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 677
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 495
    invoke-static {}, Lcom/google/r/b/a/aok;->newBuilder()Lcom/google/r/b/a/aom;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aom;->a(Lcom/google/r/b/a/aok;)Lcom/google/r/b/a/aom;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 495
    invoke-static {}, Lcom/google/r/b/a/aok;->newBuilder()Lcom/google/r/b/a/aom;

    move-result-object v0

    return-object v0
.end method
