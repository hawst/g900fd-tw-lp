.class public final Lcom/google/r/b/a/na;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/my;",
        "Lcom/google/r/b/a/na;",
        ">;",
        "Lcom/google/r/b/a/nb;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4711
    sget-object v0, Lcom/google/r/b/a/my;->f:Lcom/google/r/b/a/my;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4791
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/na;->c:Lcom/google/n/ao;

    .line 4850
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/na;->d:Ljava/lang/Object;

    .line 4927
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    .line 5063
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/na;->e:I

    .line 4712
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4703
    new-instance v2, Lcom/google/r/b/a/my;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/my;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/na;->b:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/my;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/na;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/na;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/na;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/my;->c:Ljava/lang/Object;

    iget v1, p0, Lcom/google/r/b/a/na;->b:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/na;->b:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/na;->b:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/na;->e:I

    iput v1, v2, Lcom/google/r/b/a/my;->e:I

    iput v0, v2, Lcom/google/r/b/a/my;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4703
    check-cast p1, Lcom/google/r/b/a/my;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/na;->a(Lcom/google/r/b/a/my;)Lcom/google/r/b/a/na;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/maps/g/fo;)Lcom/google/r/b/a/na;
    .locals 1

    .prologue
    .line 5081
    if-nez p1, :cond_0

    .line 5082
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5084
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/na;->b:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/na;->b:I

    .line 5085
    iget v0, p1, Lcom/google/maps/g/fo;->c:I

    iput v0, p0, Lcom/google/r/b/a/na;->e:I

    .line 5087
    return-object p0
.end method

.method public final a(Lcom/google/r/b/a/my;)Lcom/google/r/b/a/na;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4758
    invoke-static {}, Lcom/google/r/b/a/my;->d()Lcom/google/r/b/a/my;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4782
    :goto_0
    return-object p0

    .line 4759
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4760
    iget-object v2, p0, Lcom/google/r/b/a/na;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/my;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4761
    iget v2, p0, Lcom/google/r/b/a/na;->b:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/na;->b:I

    .line 4763
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 4764
    iget v2, p0, Lcom/google/r/b/a/na;->b:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/na;->b:I

    .line 4765
    iget-object v2, p1, Lcom/google/r/b/a/my;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/na;->d:Ljava/lang/Object;

    .line 4768
    :cond_2
    iget-object v2, p1, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 4769
    iget-object v2, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 4770
    iget-object v2, p1, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    .line 4771
    iget v2, p0, Lcom/google/r/b/a/na;->b:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/r/b/a/na;->b:I

    .line 4778
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    :goto_4
    if-eqz v0, :cond_5

    .line 4779
    iget v0, p1, Lcom/google/r/b/a/my;->e:I

    invoke-static {v0}, Lcom/google/maps/g/fo;->a(I)Lcom/google/maps/g/fo;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/fo;->a:Lcom/google/maps/g/fo;

    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/r/b/a/na;->a(Lcom/google/maps/g/fo;)Lcom/google/r/b/a/na;

    .line 4781
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/my;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 4759
    goto :goto_1

    :cond_7
    move v2, v1

    .line 4763
    goto :goto_2

    .line 4773
    :cond_8
    invoke-virtual {p0}, Lcom/google/r/b/a/na;->c()V

    .line 4774
    iget-object v2, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_9
    move v0, v1

    .line 4778
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4786
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 4929
    iget v0, p0, Lcom/google/r/b/a/na;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 4930
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/na;->a:Ljava/util/List;

    .line 4933
    iget v0, p0, Lcom/google/r/b/a/na;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/na;->b:I

    .line 4935
    :cond_0
    return-void
.end method
