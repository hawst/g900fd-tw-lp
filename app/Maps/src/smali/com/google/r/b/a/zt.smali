.class public final enum Lcom/google/r/b/a/zt;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/zt;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/zt;

.field public static final enum b:Lcom/google/r/b/a/zt;

.field public static final enum c:Lcom/google/r/b/a/zt;

.field private static final synthetic e:[Lcom/google/r/b/a/zt;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 333
    new-instance v0, Lcom/google/r/b/a/zt;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/zt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/zt;->a:Lcom/google/r/b/a/zt;

    .line 337
    new-instance v0, Lcom/google/r/b/a/zt;

    const-string v1, "BASE_MAP"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/zt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/zt;->b:Lcom/google/r/b/a/zt;

    .line 341
    new-instance v0, Lcom/google/r/b/a/zt;

    const-string v1, "SEARCH_MAP"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/zt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/zt;->c:Lcom/google/r/b/a/zt;

    .line 328
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/zt;

    sget-object v1, Lcom/google/r/b/a/zt;->a:Lcom/google/r/b/a/zt;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/zt;->b:Lcom/google/r/b/a/zt;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/zt;->c:Lcom/google/r/b/a/zt;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/zt;->e:[Lcom/google/r/b/a/zt;

    .line 376
    new-instance v0, Lcom/google/r/b/a/zu;

    invoke-direct {v0}, Lcom/google/r/b/a/zu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 385
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 386
    iput p3, p0, Lcom/google/r/b/a/zt;->d:I

    .line 387
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/zt;
    .locals 1

    .prologue
    .line 363
    packed-switch p0, :pswitch_data_0

    .line 367
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 364
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/zt;->a:Lcom/google/r/b/a/zt;

    goto :goto_0

    .line 365
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/zt;->b:Lcom/google/r/b/a/zt;

    goto :goto_0

    .line 366
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/zt;->c:Lcom/google/r/b/a/zt;

    goto :goto_0

    .line 363
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/zt;
    .locals 1

    .prologue
    .line 328
    const-class v0, Lcom/google/r/b/a/zt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/zt;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/zt;
    .locals 1

    .prologue
    .line 328
    sget-object v0, Lcom/google/r/b/a/zt;->e:[Lcom/google/r/b/a/zt;

    invoke-virtual {v0}, [Lcom/google/r/b/a/zt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/zt;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 359
    iget v0, p0, Lcom/google/r/b/a/zt;->d:I

    return v0
.end method
