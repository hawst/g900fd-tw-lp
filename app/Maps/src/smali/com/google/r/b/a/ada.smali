.class public final Lcom/google/r/b/a/ada;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/add;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/ada;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:I

.field d:Ljava/lang/Object;

.field e:J

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14418
    new-instance v0, Lcom/google/r/b/a/adb;

    invoke-direct {v0}, Lcom/google/r/b/a/adb;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ada;->PARSER:Lcom/google/n/ax;

    .line 14606
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ada;->i:Lcom/google/n/aw;

    .line 14981
    new-instance v0, Lcom/google/r/b/a/ada;

    invoke-direct {v0}, Lcom/google/r/b/a/ada;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ada;->f:Lcom/google/r/b/a/ada;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 14349
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 14549
    iput-byte v0, p0, Lcom/google/r/b/a/ada;->g:B

    .line 14577
    iput v0, p0, Lcom/google/r/b/a/ada;->h:I

    .line 14350
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ada;->b:Ljava/lang/Object;

    .line 14351
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ada;->c:I

    .line 14352
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ada;->d:Ljava/lang/Object;

    .line 14353
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/ada;->e:J

    .line 14354
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 14360
    invoke-direct {p0}, Lcom/google/r/b/a/ada;-><init>()V

    .line 14361
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 14365
    const/4 v0, 0x0

    .line 14366
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 14367
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 14368
    sparse-switch v3, :sswitch_data_0

    .line 14373
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 14375
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 14371
    goto :goto_0

    .line 14380
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 14381
    iget v4, p0, Lcom/google/r/b/a/ada;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ada;->a:I

    .line 14382
    iput-object v3, p0, Lcom/google/r/b/a/ada;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 14409
    :catch_0
    move-exception v0

    .line 14410
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 14415
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ada;->au:Lcom/google/n/bn;

    throw v0

    .line 14386
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 14387
    invoke-static {v3}, Lcom/google/r/b/a/acy;->a(I)Lcom/google/r/b/a/acy;

    move-result-object v4

    .line 14388
    if-nez v4, :cond_1

    .line 14389
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 14411
    :catch_1
    move-exception v0

    .line 14412
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 14413
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 14391
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/r/b/a/ada;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/ada;->a:I

    .line 14392
    iput v3, p0, Lcom/google/r/b/a/ada;->c:I

    goto :goto_0

    .line 14397
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 14398
    iget v4, p0, Lcom/google/r/b/a/ada;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/ada;->a:I

    .line 14399
    iput-object v3, p0, Lcom/google/r/b/a/ada;->d:Ljava/lang/Object;

    goto :goto_0

    .line 14403
    :sswitch_4
    iget v3, p0, Lcom/google/r/b/a/ada;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/ada;->a:I

    .line 14404
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/ada;->e:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 14415
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ada;->au:Lcom/google/n/bn;

    .line 14416
    return-void

    .line 14368
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x21 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 14347
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 14549
    iput-byte v0, p0, Lcom/google/r/b/a/ada;->g:B

    .line 14577
    iput v0, p0, Lcom/google/r/b/a/ada;->h:I

    .line 14348
    return-void
.end method

.method public static g()Lcom/google/r/b/a/ada;
    .locals 1

    .prologue
    .line 14984
    sget-object v0, Lcom/google/r/b/a/ada;->f:Lcom/google/r/b/a/ada;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/adc;
    .locals 1

    .prologue
    .line 14668
    new-instance v0, Lcom/google/r/b/a/adc;

    invoke-direct {v0}, Lcom/google/r/b/a/adc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14430
    sget-object v0, Lcom/google/r/b/a/ada;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 14561
    invoke-virtual {p0}, Lcom/google/r/b/a/ada;->c()I

    .line 14562
    iget v0, p0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 14563
    iget-object v0, p0, Lcom/google/r/b/a/ada;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ada;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14565
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 14566
    iget v0, p0, Lcom/google/r/b/a/ada;->c:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 14568
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 14569
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/ada;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ada;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14571
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 14572
    iget-wide v0, p0, Lcom/google/r/b/a/ada;->e:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/n/l;->c(IJ)V

    .line 14574
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/ada;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 14575
    return-void

    .line 14563
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 14569
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 14551
    iget-byte v1, p0, Lcom/google/r/b/a/ada;->g:B

    .line 14552
    if-ne v1, v0, :cond_0

    .line 14556
    :goto_0
    return v0

    .line 14553
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 14555
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ada;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 14579
    iget v0, p0, Lcom/google/r/b/a/ada;->h:I

    .line 14580
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 14601
    :goto_0
    return v0

    .line 14583
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 14585
    iget-object v0, p0, Lcom/google/r/b/a/ada;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ada;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 14587
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_6

    .line 14588
    iget v2, p0, Lcom/google/r/b/a/ada;->c:I

    .line 14589
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 14591
    :goto_4
    iget v0, p0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_1

    .line 14592
    const/4 v3, 0x3

    .line 14593
    iget-object v0, p0, Lcom/google/r/b/a/ada;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ada;->d:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 14595
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 14596
    iget-wide v4, p0, Lcom/google/r/b/a/ada;->e:J

    .line 14597
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v2, v0

    .line 14599
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ada;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 14600
    iput v0, p0, Lcom/google/r/b/a/ada;->h:I

    goto/16 :goto_0

    .line 14585
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 14589
    :cond_4
    const/16 v2, 0xa

    goto :goto_3

    .line 14593
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_6
    move v2, v0

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14504
    iget-object v0, p0, Lcom/google/r/b/a/ada;->d:Ljava/lang/Object;

    .line 14505
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14506
    check-cast v0, Ljava/lang/String;

    .line 14514
    :goto_0
    return-object v0

    .line 14508
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 14510
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 14511
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14512
    iput-object v1, p0, Lcom/google/r/b/a/ada;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14514
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 14341
    invoke-static {}, Lcom/google/r/b/a/ada;->newBuilder()Lcom/google/r/b/a/adc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/adc;->a(Lcom/google/r/b/a/ada;)Lcom/google/r/b/a/adc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 14341
    invoke-static {}, Lcom/google/r/b/a/ada;->newBuilder()Lcom/google/r/b/a/adc;

    move-result-object v0

    return-object v0
.end method
