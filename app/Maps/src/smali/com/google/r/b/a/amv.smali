.class public final enum Lcom/google/r/b/a/amv;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/amv;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/amv;

.field public static final enum b:Lcom/google/r/b/a/amv;

.field public static final enum c:Lcom/google/r/b/a/amv;

.field private static final synthetic e:[Lcom/google/r/b/a/amv;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 9107
    new-instance v0, Lcom/google/r/b/a/amv;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/r/b/a/amv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amv;->a:Lcom/google/r/b/a/amv;

    .line 9111
    new-instance v0, Lcom/google/r/b/a/amv;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/amv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amv;->b:Lcom/google/r/b/a/amv;

    .line 9115
    new-instance v0, Lcom/google/r/b/a/amv;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/r/b/a/amv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amv;->c:Lcom/google/r/b/a/amv;

    .line 9102
    new-array v0, v5, [Lcom/google/r/b/a/amv;

    sget-object v1, Lcom/google/r/b/a/amv;->a:Lcom/google/r/b/a/amv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/amv;->b:Lcom/google/r/b/a/amv;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/amv;->c:Lcom/google/r/b/a/amv;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/r/b/a/amv;->e:[Lcom/google/r/b/a/amv;

    .line 9150
    new-instance v0, Lcom/google/r/b/a/amw;

    invoke-direct {v0}, Lcom/google/r/b/a/amw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 9159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9160
    iput p3, p0, Lcom/google/r/b/a/amv;->d:I

    .line 9161
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/amv;
    .locals 1

    .prologue
    .line 9137
    packed-switch p0, :pswitch_data_0

    .line 9141
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 9138
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/amv;->a:Lcom/google/r/b/a/amv;

    goto :goto_0

    .line 9139
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/amv;->b:Lcom/google/r/b/a/amv;

    goto :goto_0

    .line 9140
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/amv;->c:Lcom/google/r/b/a/amv;

    goto :goto_0

    .line 9137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/amv;
    .locals 1

    .prologue
    .line 9102
    const-class v0, Lcom/google/r/b/a/amv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/amv;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/amv;
    .locals 1

    .prologue
    .line 9102
    sget-object v0, Lcom/google/r/b/a/amv;->e:[Lcom/google/r/b/a/amv;

    invoke-virtual {v0}, [Lcom/google/r/b/a/amv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/amv;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 9133
    iget v0, p0, Lcom/google/r/b/a/amv;->d:I

    return v0
.end method
