.class public final Lcom/google/r/b/a/ach;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ack;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ach;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/r/b/a/ach;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Lcom/google/maps/a/e;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/acu;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/r/b/a/aby;

.field public g:Lcom/google/maps/g/wb;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16002
    new-instance v0, Lcom/google/r/b/a/aci;

    invoke-direct {v0}, Lcom/google/r/b/a/aci;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ach;->PARSER:Lcom/google/n/ax;

    .line 16253
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ach;->k:Lcom/google/n/aw;

    .line 16894
    new-instance v0, Lcom/google/r/b/a/ach;

    invoke-direct {v0}, Lcom/google/r/b/a/ach;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ach;->h:Lcom/google/r/b/a/ach;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15900
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 16182
    iput-byte v0, p0, Lcom/google/r/b/a/ach;->i:B

    .line 16216
    iput v0, p0, Lcom/google/r/b/a/ach;->j:I

    .line 15901
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ach;->b:Ljava/lang/Object;

    .line 15902
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ach;->c:Ljava/lang/Object;

    .line 15903
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    .line 15904
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/16 v7, 0x8

    .line 15910
    invoke-direct {p0}, Lcom/google/r/b/a/ach;-><init>()V

    .line 15913
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v6

    move v4, v0

    move v1, v0

    .line 15916
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 15917
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 15918
    sparse-switch v0, :sswitch_data_0

    .line 15923
    invoke-virtual {v6, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v5

    .line 15925
    goto :goto_0

    :sswitch_0
    move v4, v5

    .line 15921
    goto :goto_0

    .line 15930
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 15931
    iget v2, p0, Lcom/google/r/b/a/ach;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ach;->a:I

    .line 15932
    iput-object v0, p0, Lcom/google/r/b/a/ach;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 15990
    :catch_0
    move-exception v0

    .line 15991
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 15996
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_1

    .line 15997
    iget-object v1, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    .line 15999
    :cond_1
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ach;->au:Lcom/google/n/bn;

    throw v0

    .line 15936
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 15937
    iget v2, p0, Lcom/google/r/b/a/ach;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ach;->a:I

    .line 15938
    iput-object v0, p0, Lcom/google/r/b/a/ach;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 15992
    :catch_1
    move-exception v0

    .line 15993
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 15994
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 15943
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_a

    .line 15944
    iget-object v0, p0, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/a/g;->a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;

    move-result-object v0

    move-object v2, v0

    .line 15946
    :goto_1
    sget-object v0, Lcom/google/maps/a/e;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iput-object v0, p0, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    .line 15947
    if-eqz v2, :cond_2

    .line 15948
    iget-object v0, p0, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    invoke-virtual {v2, v0}, Lcom/google/maps/a/g;->a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;

    .line 15949
    invoke-virtual {v2}, Lcom/google/maps/a/g;->c()Lcom/google/maps/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    .line 15951
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ach;->a:I

    goto/16 :goto_0

    .line 15955
    :sswitch_4
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v7, :cond_3

    .line 15956
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    .line 15957
    or-int/lit8 v1, v1, 0x8

    .line 15959
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/acu;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 15964
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_9

    .line 15965
    iget-object v0, p0, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    invoke-static {}, Lcom/google/r/b/a/aby;->newBuilder()Lcom/google/r/b/a/aca;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/aca;->a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;

    move-result-object v0

    move-object v2, v0

    .line 15967
    :goto_2
    sget-object v0, Lcom/google/r/b/a/aby;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aby;

    iput-object v0, p0, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    .line 15968
    if-eqz v2, :cond_4

    .line 15969
    iget-object v0, p0, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/aca;->a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;

    .line 15970
    invoke-virtual {v2}, Lcom/google/r/b/a/aca;->c()Lcom/google/r/b/a/aby;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    .line 15972
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/ach;->a:I

    goto/16 :goto_0

    .line 15977
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_8

    .line 15978
    iget-object v0, p0, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    invoke-static {}, Lcom/google/maps/g/wb;->newBuilder()Lcom/google/maps/g/wd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/wd;->a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;

    move-result-object v0

    move-object v2, v0

    .line 15980
    :goto_3
    sget-object v0, Lcom/google/maps/g/wb;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/wb;

    iput-object v0, p0, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    .line 15981
    if-eqz v2, :cond_5

    .line 15982
    iget-object v0, p0, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/wd;->a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;

    .line 15983
    invoke-virtual {v2}, Lcom/google/maps/g/wd;->c()Lcom/google/maps/g/wb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    .line 15985
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/ach;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 15996
    :cond_6
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_7

    .line 15997
    iget-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    .line 15999
    :cond_7
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->au:Lcom/google/n/bn;

    .line 16000
    return-void

    :cond_8
    move-object v2, v3

    goto :goto_3

    :cond_9
    move-object v2, v3

    goto :goto_2

    :cond_a
    move-object v2, v3

    goto/16 :goto_1

    .line 15918
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15898
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 16182
    iput-byte v0, p0, Lcom/google/r/b/a/ach;->i:B

    .line 16216
    iput v0, p0, Lcom/google/r/b/a/ach;->j:I

    .line 15899
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ach;
    .locals 1

    .prologue
    .line 16897
    sget-object v0, Lcom/google/r/b/a/ach;->h:Lcom/google/r/b/a/ach;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/acj;
    .locals 1

    .prologue
    .line 16315
    new-instance v0, Lcom/google/r/b/a/acj;

    invoke-direct {v0}, Lcom/google/r/b/a/acj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ach;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16014
    sget-object v0, Lcom/google/r/b/a/ach;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 16194
    invoke-virtual {p0}, Lcom/google/r/b/a/ach;->c()I

    .line 16195
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 16196
    iget-object v0, p0, Lcom/google/r/b/a/ach;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 16198
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 16199
    iget-object v0, p0, Lcom/google/r/b/a/ach;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 16201
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 16202
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 16204
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 16205
    iget-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 16204
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 16196
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 16199
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 16202
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    goto :goto_2

    .line 16207
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 16208
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/r/b/a/aby;->d()Lcom/google/r/b/a/aby;

    move-result-object v0

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 16210
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 16211
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/maps/g/wb;->j()Lcom/google/maps/g/wb;

    move-result-object v0

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 16213
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/ach;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 16214
    return-void

    .line 16208
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    goto :goto_4

    .line 16211
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    goto :goto_5
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 16184
    iget-byte v1, p0, Lcom/google/r/b/a/ach;->i:B

    .line 16185
    if-ne v1, v0, :cond_0

    .line 16189
    :goto_0
    return v0

    .line 16186
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 16188
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ach;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16218
    iget v0, p0, Lcom/google/r/b/a/ach;->j:I

    .line 16219
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 16248
    :goto_0
    return v0

    .line 16222
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 16224
    iget-object v0, p0, Lcom/google/r/b/a/ach;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 16226
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 16228
    iget-object v0, p0, Lcom/google/r/b/a/ach;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ach;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 16230
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 16231
    const/4 v3, 0x3

    .line 16232
    iget-object v0, p0, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v0

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_2
    move v3, v1

    move v1, v2

    .line 16234
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 16235
    iget-object v0, p0, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    .line 16236
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 16234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 16224
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 16228
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 16232
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    goto :goto_4

    .line 16238
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 16239
    const/4 v1, 0x5

    .line 16240
    iget-object v0, p0, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/r/b/a/aby;->d()Lcom/google/r/b/a/aby;

    move-result-object v0

    :goto_6
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 16242
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 16243
    const/4 v1, 0x6

    .line 16244
    iget-object v0, p0, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/maps/g/wb;->j()Lcom/google/maps/g/wb;

    move-result-object v0

    :goto_7
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 16246
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/ach;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 16247
    iput v0, p0, Lcom/google/r/b/a/ach;->j:I

    goto/16 :goto_0

    .line 16240
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    goto :goto_6

    .line 16244
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    goto :goto_7

    :cond_b
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 15892
    invoke-static {}, Lcom/google/r/b/a/ach;->newBuilder()Lcom/google/r/b/a/acj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/acj;->a(Lcom/google/r/b/a/ach;)Lcom/google/r/b/a/acj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 15892
    invoke-static {}, Lcom/google/r/b/a/ach;->newBuilder()Lcom/google/r/b/a/acj;

    move-result-object v0

    return-object v0
.end method
