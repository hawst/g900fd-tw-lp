.class public final Lcom/google/r/b/a/qh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/qf;",
        "Lcom/google/r/b/a/qh;",
        ">;",
        "Lcom/google/r/b/a/qi;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Z

.field public d:J

.field public e:J

.field public f:J

.field public g:Lcom/google/n/ao;

.field private h:Ljava/lang/Object;

.field private i:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 6833
    sget-object v0, Lcom/google/r/b/a/qf;->j:Lcom/google/r/b/a/qf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6950
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qh;->b:Lcom/google/n/ao;

    .line 7009
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/qh;->h:Ljava/lang/Object;

    .line 7213
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qh;->g:Lcom/google/n/ao;

    .line 6834
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6825
    new-instance v2, Lcom/google/r/b/a/qf;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/qf;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/qh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/qh;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/qh;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/qh;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/qf;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v4, p0, Lcom/google/r/b/a/qh;->c:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/qf;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/r/b/a/qh;->d:J

    iput-wide v4, v2, Lcom/google/r/b/a/qf;->e:J

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/r/b/a/qh;->e:J

    iput-wide v4, v2, Lcom/google/r/b/a/qf;->f:J

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-wide v4, p0, Lcom/google/r/b/a/qh;->f:J

    iput-wide v4, v2, Lcom/google/r/b/a/qf;->g:J

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/qf;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/qh;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/qh;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-wide v4, p0, Lcom/google/r/b/a/qh;->i:J

    iput-wide v4, v2, Lcom/google/r/b/a/qf;->i:J

    iput v0, v2, Lcom/google/r/b/a/qf;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 6825
    check-cast p1, Lcom/google/r/b/a/qf;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/qh;->a(Lcom/google/r/b/a/qf;)Lcom/google/r/b/a/qh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/qf;)Lcom/google/r/b/a/qh;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6905
    invoke-static {}, Lcom/google/r/b/a/qf;->d()Lcom/google/r/b/a/qf;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6935
    :goto_0
    return-object p0

    .line 6906
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 6907
    iget-object v2, p0, Lcom/google/r/b/a/qh;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6908
    iget v2, p0, Lcom/google/r/b/a/qh;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/qh;->a:I

    .line 6910
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 6911
    iget v2, p0, Lcom/google/r/b/a/qh;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/qh;->a:I

    .line 6912
    iget-object v2, p1, Lcom/google/r/b/a/qf;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/qh;->h:Ljava/lang/Object;

    .line 6915
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 6916
    iget-boolean v2, p1, Lcom/google/r/b/a/qf;->d:Z

    iget v3, p0, Lcom/google/r/b/a/qh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/qh;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/qh;->c:Z

    .line 6918
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 6919
    iget-wide v2, p1, Lcom/google/r/b/a/qf;->e:J

    iget v4, p0, Lcom/google/r/b/a/qh;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/qh;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/qh;->d:J

    .line 6921
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 6922
    iget-wide v2, p1, Lcom/google/r/b/a/qf;->f:J

    iget v4, p0, Lcom/google/r/b/a/qh;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/qh;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/qh;->e:J

    .line 6924
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 6925
    iget-wide v2, p1, Lcom/google/r/b/a/qf;->g:J

    iget v4, p0, Lcom/google/r/b/a/qh;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/qh;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/qh;->f:J

    .line 6927
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 6928
    iget-object v2, p0, Lcom/google/r/b/a/qh;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/qf;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6929
    iget v2, p0, Lcom/google/r/b/a/qh;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/qh;->a:I

    .line 6931
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/qf;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 6932
    iget-wide v0, p1, Lcom/google/r/b/a/qf;->i:J

    iget v2, p0, Lcom/google/r/b/a/qh;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/qh;->a:I

    iput-wide v0, p0, Lcom/google/r/b/a/qh;->i:J

    .line 6934
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/qf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 6906
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 6910
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 6915
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 6918
    goto :goto_4

    :cond_d
    move v2, v1

    .line 6921
    goto :goto_5

    :cond_e
    move v2, v1

    .line 6924
    goto :goto_6

    :cond_f
    move v2, v1

    .line 6927
    goto :goto_7

    :cond_10
    move v0, v1

    .line 6931
    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 6939
    iget v0, p0, Lcom/google/r/b/a/qh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 6940
    iget-object v0, p0, Lcom/google/r/b/a/qh;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/px;->d()Lcom/google/r/b/a/px;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/px;

    invoke-virtual {v0}, Lcom/google/r/b/a/px;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 6945
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 6939
    goto :goto_0

    :cond_1
    move v0, v2

    .line 6945
    goto :goto_1
.end method
