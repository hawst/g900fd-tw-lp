.class public final Lcom/google/r/b/a/aja;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ajd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aja;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/aja;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2939
    new-instance v0, Lcom/google/r/b/a/ajb;

    invoke-direct {v0}, Lcom/google/r/b/a/ajb;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aja;->PARSER:Lcom/google/n/ax;

    .line 3056
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aja;->g:Lcom/google/n/aw;

    .line 3329
    new-instance v0, Lcom/google/r/b/a/aja;

    invoke-direct {v0}, Lcom/google/r/b/a/aja;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aja;->d:Lcom/google/r/b/a/aja;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2889
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2998
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    .line 3013
    iput-byte v2, p0, Lcom/google/r/b/a/aja;->e:B

    .line 3035
    iput v2, p0, Lcom/google/r/b/a/aja;->f:I

    .line 2890
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    .line 2891
    iget-object v0, p0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 2892
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2898
    invoke-direct {p0}, Lcom/google/r/b/a/aja;-><init>()V

    .line 2899
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2904
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2905
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2906
    sparse-switch v3, :sswitch_data_0

    .line 2911
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2913
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2909
    goto :goto_0

    .line 2918
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2919
    iget v4, p0, Lcom/google/r/b/a/aja;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/aja;->a:I

    .line 2920
    iput-object v3, p0, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2930
    :catch_0
    move-exception v0

    .line 2931
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2936
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aja;->au:Lcom/google/n/bn;

    throw v0

    .line 2924
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 2925
    iget v3, p0, Lcom/google/r/b/a/aja;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/aja;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2932
    :catch_1
    move-exception v0

    .line 2933
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2934
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2936
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aja;->au:Lcom/google/n/bn;

    .line 2937
    return-void

    .line 2906
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2887
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2998
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    .line 3013
    iput-byte v1, p0, Lcom/google/r/b/a/aja;->e:B

    .line 3035
    iput v1, p0, Lcom/google/r/b/a/aja;->f:I

    .line 2888
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aja;
    .locals 1

    .prologue
    .line 3332
    sget-object v0, Lcom/google/r/b/a/aja;->d:Lcom/google/r/b/a/aja;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ajc;
    .locals 1

    .prologue
    .line 3118
    new-instance v0, Lcom/google/r/b/a/ajc;

    invoke-direct {v0}, Lcom/google/r/b/a/ajc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aja;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2951
    sget-object v0, Lcom/google/r/b/a/aja;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3025
    invoke-virtual {p0}, Lcom/google/r/b/a/aja;->c()I

    .line 3026
    iget v0, p0, Lcom/google/r/b/a/aja;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 3027
    iget-object v0, p0, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3029
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aja;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 3030
    iget-object v0, p0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3032
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aja;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3033
    return-void

    .line 3027
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3015
    iget-byte v1, p0, Lcom/google/r/b/a/aja;->e:B

    .line 3016
    if-ne v1, v0, :cond_0

    .line 3020
    :goto_0
    return v0

    .line 3017
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3019
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aja;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3037
    iget v0, p0, Lcom/google/r/b/a/aja;->f:I

    .line 3038
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3051
    :goto_0
    return v0

    .line 3041
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aja;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 3043
    iget-object v0, p0, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 3045
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/aja;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 3046
    iget-object v2, p0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    .line 3047
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 3049
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/aja;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3050
    iput v0, p0, Lcom/google/r/b/a/aja;->f:I

    goto :goto_0

    .line 3043
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2881
    invoke-static {}, Lcom/google/r/b/a/aja;->newBuilder()Lcom/google/r/b/a/ajc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ajc;->a(Lcom/google/r/b/a/aja;)Lcom/google/r/b/a/ajc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2881
    invoke-static {}, Lcom/google/r/b/a/aja;->newBuilder()Lcom/google/r/b/a/ajc;

    move-result-object v0

    return-object v0
.end method
