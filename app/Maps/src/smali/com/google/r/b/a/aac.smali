.class public final Lcom/google/r/b/a/aac;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aaf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aac;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/aac;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/r/b/a/aad;

    invoke-direct {v0}, Lcom/google/r/b/a/aad;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aac;->PARSER:Lcom/google/n/ax;

    .line 178
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aac;->f:Lcom/google/n/aw;

    .line 380
    new-instance v0, Lcom/google/r/b/a/aac;

    invoke-direct {v0}, Lcom/google/r/b/a/aac;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aac;->c:Lcom/google/r/b/a/aac;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 40
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 142
    iput-byte v0, p0, Lcom/google/r/b/a/aac;->d:B

    .line 161
    iput v0, p0, Lcom/google/r/b/a/aac;->e:I

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aac;->b:Ljava/lang/Object;

    .line 42
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 48
    invoke-direct {p0}, Lcom/google/r/b/a/aac;-><init>()V

    .line 49
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 53
    const/4 v0, 0x0

    .line 54
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 55
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 56
    sparse-switch v3, :sswitch_data_0

    .line 61
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 63
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 59
    goto :goto_0

    .line 68
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 69
    iget v4, p0, Lcom/google/r/b/a/aac;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/aac;->a:I

    .line 70
    iput-object v3, p0, Lcom/google/r/b/a/aac;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aac;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aac;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 56
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 38
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 142
    iput-byte v0, p0, Lcom/google/r/b/a/aac;->d:B

    .line 161
    iput v0, p0, Lcom/google/r/b/a/aac;->e:I

    .line 39
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aac;
    .locals 1

    .prologue
    .line 383
    sget-object v0, Lcom/google/r/b/a/aac;->c:Lcom/google/r/b/a/aac;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aae;
    .locals 1

    .prologue
    .line 240
    new-instance v0, Lcom/google/r/b/a/aae;

    invoke-direct {v0}, Lcom/google/r/b/a/aae;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/r/b/a/aac;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 154
    invoke-virtual {p0}, Lcom/google/r/b/a/aac;->c()I

    .line 155
    iget v0, p0, Lcom/google/r/b/a/aac;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/r/b/a/aac;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aac;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/aac;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 159
    return-void

    .line 156
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 144
    iget-byte v1, p0, Lcom/google/r/b/a/aac;->d:B

    .line 145
    if-ne v1, v0, :cond_0

    .line 149
    :goto_0
    return v0

    .line 146
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 148
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aac;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 163
    iget v0, p0, Lcom/google/r/b/a/aac;->e:I

    .line 164
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 173
    :goto_0
    return v0

    .line 167
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aac;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 169
    iget-object v0, p0, Lcom/google/r/b/a/aac;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aac;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 171
    :goto_2
    iget-object v1, p0, Lcom/google/r/b/a/aac;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    iput v0, p0, Lcom/google/r/b/a/aac;->e:I

    goto :goto_0

    .line 169
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/google/r/b/a/aac;->newBuilder()Lcom/google/r/b/a/aae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aae;->a(Lcom/google/r/b/a/aac;)Lcom/google/r/b/a/aae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/google/r/b/a/aac;->newBuilder()Lcom/google/r/b/a/aae;

    move-result-object v0

    return-object v0
.end method
