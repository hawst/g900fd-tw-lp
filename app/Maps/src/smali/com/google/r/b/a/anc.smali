.class public final Lcom/google/r/b/a/anc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/and;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ana;",
        "Lcom/google/r/b/a/anc;",
        ">;",
        "Lcom/google/r/b/a/and;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3734
    sget-object v0, Lcom/google/r/b/a/ana;->e:Lcom/google/r/b/a/ana;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3823
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anc;->b:Lcom/google/n/ao;

    .line 3883
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    .line 4020
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    .line 3735
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3726
    new-instance v2, Lcom/google/r/b/a/ana;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ana;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_2

    :goto_0
    iget-object v3, v2, Lcom/google/r/b/a/ana;->b:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/anc;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/anc;->b:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/anc;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/anc;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/ana;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3726
    check-cast p1, Lcom/google/r/b/a/ana;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/anc;->a(Lcom/google/r/b/a/ana;)Lcom/google/r/b/a/anc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ana;)Lcom/google/r/b/a/anc;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3776
    invoke-static {}, Lcom/google/r/b/a/ana;->d()Lcom/google/r/b/a/ana;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 3802
    :goto_0
    return-object p0

    .line 3777
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/ana;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_1
    if-eqz v0, :cond_1

    .line 3778
    iget-object v0, p0, Lcom/google/r/b/a/anc;->b:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/ana;->b:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3779
    iget v0, p0, Lcom/google/r/b/a/anc;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/anc;->a:I

    .line 3781
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3782
    iget-object v0, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3783
    iget-object v0, p1, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    .line 3784
    iget v0, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/anc;->a:I

    .line 3791
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3792
    iget-object v0, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3793
    iget-object v0, p1, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    .line 3794
    iget v0, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/r/b/a/anc;->a:I

    .line 3801
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/r/b/a/ana;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 3777
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 3786
    :cond_5
    invoke-virtual {p0}, Lcom/google/r/b/a/anc;->c()V

    .line 3787
    iget-object v0, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 3796
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/anc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/anc;->a:I

    .line 3797
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3806
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 3807
    iget-object v0, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ams;->d()Lcom/google/r/b/a/ams;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ams;

    invoke-virtual {v0}, Lcom/google/r/b/a/ams;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3818
    :cond_0
    :goto_1
    return v2

    .line 3806
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 3812
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3813
    iget-object v0, p0, Lcom/google/r/b/a/anc;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/amo;->d()Lcom/google/r/b/a/amo;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/amo;

    invoke-virtual {v0}, Lcom/google/r/b/a/amo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3812
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3818
    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 3885
    iget v0, p0, Lcom/google/r/b/a/anc;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 3886
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/anc;->c:Ljava/util/List;

    .line 3889
    iget v0, p0, Lcom/google/r/b/a/anc;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/anc;->a:I

    .line 3891
    :cond_0
    return-void
.end method
