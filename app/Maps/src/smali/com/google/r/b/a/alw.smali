.class public final Lcom/google/r/b/a/alw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aly;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/alu;",
        "Lcom/google/r/b/a/alw;",
        ">;",
        "Lcom/google/r/b/a/aly;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:I

.field private e:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1639
    sget-object v0, Lcom/google/r/b/a/alu;->g:Lcom/google/r/b/a/alu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1744
    iput v1, p0, Lcom/google/r/b/a/alw;->a:I

    .line 1904
    iput v1, p0, Lcom/google/r/b/a/alw;->d:I

    .line 1940
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alw;->e:Lcom/google/n/ao;

    .line 1999
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/alw;->f:Ljava/lang/Object;

    .line 1640
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 1631
    new-instance v4, Lcom/google/r/b/a/alu;

    invoke-direct {v4, p0}, Lcom/google/r/b/a/alu;-><init>(Lcom/google/n/v;)V

    iget v5, p0, Lcom/google/r/b/a/alw;->c:I

    iget v0, p0, Lcom/google/r/b/a/alw;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v1, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_0
    iget v0, p0, Lcom/google/r/b/a/alw;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v1, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_1
    and-int/lit8 v0, v5, 0x4

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/alw;->d:I

    iput v1, v4, Lcom/google/r/b/a/alu;->d:I

    and-int/lit8 v1, v5, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, v4, Lcom/google/r/b/a/alu;->e:Lcom/google/n/ao;

    iget-object v2, p0, Lcom/google/r/b/a/alw;->e:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    iget-object v6, p0, Lcom/google/r/b/a/alw;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v1, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v5, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/alw;->f:Ljava/lang/Object;

    iput-object v1, v4, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    iput v0, v4, Lcom/google/r/b/a/alu;->a:I

    iget v0, p0, Lcom/google/r/b/a/alw;->a:I

    iput v0, v4, Lcom/google/r/b/a/alu;->b:I

    return-object v4

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1631
    check-cast p1, Lcom/google/r/b/a/alu;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/alw;->a(Lcom/google/r/b/a/alu;)Lcom/google/r/b/a/alw;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/alu;)Lcom/google/r/b/a/alw;
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1694
    invoke-static {}, Lcom/google/r/b/a/alu;->d()Lcom/google/r/b/a/alu;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1731
    :goto_0
    return-object p0

    .line 1695
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    .line 1696
    iget v1, p1, Lcom/google/r/b/a/alu;->d:I

    invoke-static {v1}, Lcom/google/maps/g/ta;->a(I)Lcom/google/maps/g/ta;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/ta;->a:Lcom/google/maps/g/ta;

    :cond_1
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v1, v0

    .line 1695
    goto :goto_1

    .line 1696
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/alw;->c:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/alw;->c:I

    iget v1, v1, Lcom/google/maps/g/ta;->d:I

    iput v1, p0, Lcom/google/r/b/a/alw;->d:I

    .line 1698
    :cond_4
    iget v1, p1, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_8

    move v1, v2

    :goto_2
    if-eqz v1, :cond_5

    .line 1699
    iget-object v1, p0, Lcom/google/r/b/a/alw;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alu;->e:Lcom/google/n/ao;

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1700
    iget v1, p0, Lcom/google/r/b/a/alw;->c:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/alw;->c:I

    .line 1702
    :cond_5
    iget v1, p1, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_6

    move v0, v2

    :cond_6
    if-eqz v0, :cond_7

    .line 1703
    iget v0, p0, Lcom/google/r/b/a/alw;->c:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/alw;->c:I

    .line 1704
    iget-object v0, p1, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/alw;->f:Ljava/lang/Object;

    .line 1707
    :cond_7
    sget-object v0, Lcom/google/r/b/a/alt;->a:[I

    iget v1, p1, Lcom/google/r/b/a/alu;->b:I

    invoke-static {v1}, Lcom/google/r/b/a/alx;->a(I)Lcom/google/r/b/a/alx;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/r/b/a/alx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1724
    :goto_3
    iget-object v0, p1, Lcom/google/r/b/a/alu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_8
    move v1, v0

    .line 1698
    goto :goto_2

    .line 1709
    :pswitch_0
    iget v0, p0, Lcom/google/r/b/a/alw;->a:I

    if-eq v0, v2, :cond_9

    .line 1710
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    .line 1712
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1713
    iget-object v1, p1, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 1712
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1714
    iput v2, p0, Lcom/google/r/b/a/alw;->a:I

    goto :goto_3

    .line 1718
    :pswitch_1
    iget v0, p0, Lcom/google/r/b/a/alw;->a:I

    if-eq v0, v4, :cond_a

    .line 1719
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    .line 1721
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1722
    iget-object v1, p1, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 1721
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1723
    iput v4, p0, Lcom/google/r/b/a/alw;->a:I

    goto :goto_3

    .line 1707
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1735
    iget v0, p0, Lcom/google/r/b/a/alw;->a:I

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 1736
    iget v0, p0, Lcom/google/r/b/a/alw;->a:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/r/b/a/alw;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    :goto_1
    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1741
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 1735
    goto :goto_0

    .line 1736
    :cond_1
    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1741
    goto :goto_2
.end method
