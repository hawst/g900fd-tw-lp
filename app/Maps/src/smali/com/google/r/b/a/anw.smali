.class public final Lcom/google/r/b/a/anw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aob;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/anw;",
            ">;"
        }
    .end annotation
.end field

.field static final n:Lcom/google/r/b/a/anw;

.field private static volatile q:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:J

.field public c:I

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field g:I

.field h:I

.field i:Ljava/lang/Object;

.field public j:Ljava/lang/Object;

.field k:Ljava/lang/Object;

.field l:Ljava/lang/Object;

.field public m:Lcom/google/n/ao;

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1364
    new-instance v0, Lcom/google/r/b/a/anx;

    invoke-direct {v0}, Lcom/google/r/b/a/anx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/anw;->PARSER:Lcom/google/n/ax;

    .line 1916
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/anw;->q:Lcom/google/n/aw;

    .line 2932
    new-instance v0, Lcom/google/r/b/a/anw;

    invoke-direct {v0}, Lcom/google/r/b/a/anw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/anw;->n:Lcom/google/r/b/a/anw;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1237
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1540
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anw;->e:Lcom/google/n/ao;

    .line 1556
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    .line 1770
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anw;->m:Lcom/google/n/ao;

    .line 1785
    iput-byte v1, p0, Lcom/google/r/b/a/anw;->o:B

    .line 1855
    iput v1, p0, Lcom/google/r/b/a/anw;->p:I

    .line 1238
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/anw;->b:J

    .line 1239
    const/16 v0, 0x7f

    iput v0, p0, Lcom/google/r/b/a/anw;->c:I

    .line 1240
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    .line 1241
    iget-object v0, p0, Lcom/google/r/b/a/anw;->e:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1242
    iget-object v0, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1243
    iput v4, p0, Lcom/google/r/b/a/anw;->g:I

    .line 1244
    iput v4, p0, Lcom/google/r/b/a/anw;->h:I

    .line 1245
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/anw;->i:Ljava/lang/Object;

    .line 1246
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/anw;->j:Ljava/lang/Object;

    .line 1247
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/anw;->k:Ljava/lang/Object;

    .line 1248
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/anw;->l:Ljava/lang/Object;

    .line 1249
    iget-object v0, p0, Lcom/google/r/b/a/anw;->m:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1250
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x4

    const/4 v0, 0x0

    .line 1256
    invoke-direct {p0}, Lcom/google/r/b/a/anw;-><init>()V

    .line 1259
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 1262
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 1263
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 1264
    sparse-switch v4, :sswitch_data_0

    .line 1269
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 1271
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 1267
    goto :goto_0

    .line 1276
    :sswitch_1
    iget v4, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/anw;->a:I

    .line 1277
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/anw;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1352
    :catch_0
    move-exception v0

    .line 1353
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1358
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_1

    .line 1359
    iget-object v1, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    .line 1361
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/anw;->au:Lcom/google/n/bn;

    throw v0

    .line 1281
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 1282
    invoke-static {v4}, Lcom/google/r/b/a/anz;->a(I)Lcom/google/r/b/a/anz;

    move-result-object v5

    .line 1283
    if-nez v5, :cond_2

    .line 1284
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1354
    :catch_1
    move-exception v0

    .line 1355
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1356
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1286
    :cond_2
    :try_start_4
    iget v5, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/anw;->a:I

    .line 1287
    iput v4, p0, Lcom/google/r/b/a/anw;->c:I

    goto :goto_0

    .line 1292
    :sswitch_3
    and-int/lit8 v4, v1, 0x4

    if-eq v4, v7, :cond_3

    .line 1293
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    .line 1295
    or-int/lit8 v1, v1, 0x4

    .line 1297
    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 1298
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 1297
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1302
    :sswitch_4
    iget-object v4, p0, Lcom/google/r/b/a/anw;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 1303
    iget v4, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/anw;->a:I

    goto/16 :goto_0

    .line 1307
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 1308
    iget v5, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/r/b/a/anw;->a:I

    .line 1309
    iput-object v4, p0, Lcom/google/r/b/a/anw;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 1313
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 1314
    iget v5, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/r/b/a/anw;->a:I

    .line 1315
    iput-object v4, p0, Lcom/google/r/b/a/anw;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 1319
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 1320
    iget v5, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/r/b/a/anw;->a:I

    .line 1321
    iput-object v4, p0, Lcom/google/r/b/a/anw;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 1325
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 1326
    iget v5, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/r/b/a/anw;->a:I

    .line 1327
    iput-object v4, p0, Lcom/google/r/b/a/anw;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 1331
    :sswitch_9
    iget-object v4, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 1332
    iget v4, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/anw;->a:I

    goto/16 :goto_0

    .line 1336
    :sswitch_a
    iget-object v4, p0, Lcom/google/r/b/a/anw;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 1337
    iget v4, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/r/b/a/anw;->a:I

    goto/16 :goto_0

    .line 1341
    :sswitch_b
    iget v4, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/anw;->a:I

    .line 1342
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/anw;->g:I

    goto/16 :goto_0

    .line 1346
    :sswitch_c
    iget v4, p0, Lcom/google/r/b/a/anw;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/anw;->a:I

    .line 1347
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/anw;->h:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1358
    :cond_4
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v7, :cond_5

    .line 1359
    iget-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    .line 1361
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->au:Lcom/google/n/bn;

    .line 1362
    return-void

    .line 1264
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1235
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1540
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anw;->e:Lcom/google/n/ao;

    .line 1556
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    .line 1770
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anw;->m:Lcom/google/n/ao;

    .line 1785
    iput-byte v1, p0, Lcom/google/r/b/a/anw;->o:B

    .line 1855
    iput v1, p0, Lcom/google/r/b/a/anw;->p:I

    .line 1236
    return-void
.end method

.method public static i()Lcom/google/r/b/a/anw;
    .locals 1

    .prologue
    .line 2935
    sget-object v0, Lcom/google/r/b/a/anw;->n:Lcom/google/r/b/a/anw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/any;
    .locals 1

    .prologue
    .line 1978
    new-instance v0, Lcom/google/r/b/a/any;

    invoke-direct {v0}, Lcom/google/r/b/a/any;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/anw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1376
    sget-object v0, Lcom/google/r/b/a/anw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1815
    invoke-virtual {p0}, Lcom/google/r/b/a/anw;->c()I

    .line 1816
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1817
    iget-wide v0, p0, Lcom/google/r/b/a/anw;->b:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->c(IJ)V

    .line 1819
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1820
    iget v0, p0, Lcom/google/r/b/a/anw;->c:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 1822
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1823
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1822
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1825
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 1826
    iget-object v0, p0, Lcom/google/r/b/a/anw;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1828
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    .line 1829
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/anw;->i:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->i:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1831
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 1832
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/anw;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->j:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1834
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_6

    .line 1835
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/anw;->k:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->k:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1837
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_7

    .line 1838
    iget-object v0, p0, Lcom/google/r/b/a/anw;->l:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->l:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1840
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_8

    .line 1841
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1843
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_9

    .line 1844
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/r/b/a/anw;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1846
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    .line 1847
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/r/b/a/anw;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1849
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_b

    .line 1850
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/r/b/a/anw;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1852
    :cond_b
    iget-object v0, p0, Lcom/google/r/b/a/anw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1853
    return-void

    .line 1829
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1832
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 1835
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 1838
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1787
    iget-byte v0, p0, Lcom/google/r/b/a/anw;->o:B

    .line 1788
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1810
    :cond_0
    :goto_0
    return v2

    .line 1789
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1791
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1792
    iget-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aoc;->d()Lcom/google/r/b/a/aoc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aoc;

    invoke-virtual {v0}, Lcom/google/r/b/a/aoc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1793
    iput-byte v2, p0, Lcom/google/r/b/a/anw;->o:B

    goto :goto_0

    .line 1791
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1797
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 1798
    iget-object v0, p0, Lcom/google/r/b/a/anw;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ia;

    invoke-virtual {v0}, Lcom/google/r/b/a/ia;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1799
    iput-byte v2, p0, Lcom/google/r/b/a/anw;->o:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1797
    goto :goto_2

    .line 1803
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 1804
    iget-object v0, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ia;

    invoke-virtual {v0}, Lcom/google/r/b/a/ia;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1805
    iput-byte v2, p0, Lcom/google/r/b/a/anw;->o:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1803
    goto :goto_3

    .line 1809
    :cond_7
    iput-byte v3, p0, Lcom/google/r/b/a/anw;->o:B

    move v2, v3

    .line 1810
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1857
    iget v0, p0, Lcom/google/r/b/a/anw;->p:I

    .line 1858
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1911
    :goto_0
    return v0

    .line 1861
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_13

    .line 1862
    iget-wide v4, p0, Lcom/google/r/b/a/anw;->b:J

    .line 1863
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 1865
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v7, :cond_1

    .line 1866
    iget v2, p0, Lcom/google/r/b/a/anw;->c:I

    .line 1867
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v4, v0

    .line 1869
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1870
    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/anw;->d:Ljava/util/List;

    .line 1871
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1869
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_2
    move v2, v3

    .line 1867
    goto :goto_2

    .line 1873
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_4

    .line 1874
    iget-object v0, p0, Lcom/google/r/b/a/anw;->e:Lcom/google/n/ao;

    .line 1875
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1877
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_5

    .line 1878
    const/4 v2, 0x5

    .line 1879
    iget-object v0, p0, Lcom/google/r/b/a/anw;->i:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->i:Ljava/lang/Object;

    :goto_4
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1881
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_6

    .line 1882
    const/4 v2, 0x6

    .line 1883
    iget-object v0, p0, Lcom/google/r/b/a/anw;->j:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->j:Ljava/lang/Object;

    :goto_5
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1885
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_7

    .line 1886
    const/4 v2, 0x7

    .line 1887
    iget-object v0, p0, Lcom/google/r/b/a/anw;->k:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->k:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1889
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_8

    .line 1890
    const/16 v2, 0x8

    .line 1891
    iget-object v0, p0, Lcom/google/r/b/a/anw;->l:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anw;->l:Ljava/lang/Object;

    :goto_7
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1893
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_9

    .line 1894
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/r/b/a/anw;->f:Lcom/google/n/ao;

    .line 1895
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1897
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_a

    .line 1898
    iget-object v0, p0, Lcom/google/r/b/a/anw;->m:Lcom/google/n/ao;

    .line 1899
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 1901
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_b

    .line 1902
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/r/b/a/anw;->g:I

    .line 1903
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_12

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1905
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/anw;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_d

    .line 1906
    const/16 v0, 0xc

    iget v2, p0, Lcom/google/r/b/a/anw;->h:I

    .line 1907
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v2, :cond_c

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_c
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 1909
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/anw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1910
    iput v0, p0, Lcom/google/r/b/a/anw;->p:I

    goto/16 :goto_0

    .line 1879
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 1883
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 1887
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 1891
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    :cond_12
    move v0, v3

    .line 1903
    goto :goto_8

    :cond_13
    move v0, v1

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/google/r/b/a/anw;->i:Ljava/lang/Object;

    .line 1614
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1615
    check-cast v0, Ljava/lang/String;

    .line 1623
    :goto_0
    return-object v0

    .line 1617
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1619
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1620
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1621
    iput-object v1, p0, Lcom/google/r/b/a/anw;->i:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1623
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1229
    invoke-static {}, Lcom/google/r/b/a/anw;->newBuilder()Lcom/google/r/b/a/any;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/any;->a(Lcom/google/r/b/a/anw;)Lcom/google/r/b/a/any;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1229
    invoke-static {}, Lcom/google/r/b/a/anw;->newBuilder()Lcom/google/r/b/a/any;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1697
    iget-object v0, p0, Lcom/google/r/b/a/anw;->k:Ljava/lang/Object;

    .line 1698
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1699
    check-cast v0, Ljava/lang/String;

    .line 1707
    :goto_0
    return-object v0

    .line 1701
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1703
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1704
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1705
    iput-object v1, p0, Lcom/google/r/b/a/anw;->k:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1707
    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1739
    iget-object v0, p0, Lcom/google/r/b/a/anw;->l:Ljava/lang/Object;

    .line 1740
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1741
    check-cast v0, Ljava/lang/String;

    .line 1749
    :goto_0
    return-object v0

    .line 1743
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1745
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1746
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1747
    iput-object v1, p0, Lcom/google/r/b/a/anw;->l:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1749
    goto :goto_0
.end method
