.class public final Lcom/google/r/b/a/ed;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/eh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ed;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/r/b/a/amx;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/ed;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/google/r/b/a/ee;

    invoke-direct {v0}, Lcom/google/r/b/a/ee;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ed;->PARSER:Lcom/google/n/ax;

    .line 131
    new-instance v0, Lcom/google/r/b/a/ef;

    invoke-direct {v0}, Lcom/google/r/b/a/ef;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ed;->d:Lcom/google/n/aj;

    .line 207
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ed;->h:Lcom/google/n/aw;

    .line 454
    new-instance v0, Lcom/google/r/b/a/ed;

    invoke-direct {v0}, Lcom/google/r/b/a/ed;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ed;->e:Lcom/google/r/b/a/ed;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 159
    iput-byte v0, p0, Lcom/google/r/b/a/ed;->f:B

    .line 181
    iput v0, p0, Lcom/google/r/b/a/ed;->g:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ed;->b:I

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v7, 0x2

    .line 26
    invoke-direct {p0}, Lcom/google/r/b/a/ed;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 32
    :cond_0
    :goto_0
    if-nez v2, :cond_6

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 34
    sparse-switch v1, :sswitch_data_0

    .line 39
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 41
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    iget v1, p0, Lcom/google/r/b/a/ed;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/ed;->a:I

    .line 47
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/ed;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 85
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 86
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 92
    iget-object v1, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    .line 94
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ed;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    .line 52
    invoke-static {v5}, Lcom/google/r/b/a/amx;->a(I)Lcom/google/r/b/a/amx;

    move-result-object v1

    .line 53
    if-nez v1, :cond_2

    .line 54
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v5}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 87
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 88
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 89
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 56
    :cond_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_9

    .line 57
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 58
    or-int/lit8 v1, v0, 0x2

    .line 60
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 62
    goto :goto_0

    .line 65
    :sswitch_3
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 66
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 67
    :goto_5
    iget v1, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v1, v6, :cond_3

    const/4 v1, -0x1

    :goto_6
    if-lez v1, :cond_5

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v6

    .line 69
    invoke-static {v6}, Lcom/google/r/b/a/amx;->a(I)Lcom/google/r/b/a/amx;

    move-result-object v1

    .line 70
    if-nez v1, :cond_4

    .line 71
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 91
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_2

    .line 67
    :cond_3
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v1, v6, v1

    goto :goto_6

    .line 73
    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_8

    .line 74
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 75
    or-int/lit8 v1, v0, 0x2

    .line 77
    :goto_7
    :try_start_7
    iget-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 79
    goto :goto_5

    .line 80
    :cond_5
    :try_start_8
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 91
    :cond_6
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_7

    .line 92
    iget-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    .line 94
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ed;->au:Lcom/google/n/bn;

    .line 95
    return-void

    .line 87
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 85
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto :goto_7

    :cond_9
    move v1, v0

    goto :goto_4

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 159
    iput-byte v0, p0, Lcom/google/r/b/a/ed;->f:B

    .line 181
    iput v0, p0, Lcom/google/r/b/a/ed;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ed;
    .locals 1

    .prologue
    .line 457
    sget-object v0, Lcom/google/r/b/a/ed;->e:Lcom/google/r/b/a/ed;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/eg;
    .locals 1

    .prologue
    .line 269
    new-instance v0, Lcom/google/r/b/a/eg;

    invoke-direct {v0}, Lcom/google/r/b/a/eg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ed;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    sget-object v0, Lcom/google/r/b/a/ed;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 171
    invoke-virtual {p0}, Lcom/google/r/b/a/ed;->c()I

    .line 172
    iget v0, p0, Lcom/google/r/b/a/ed;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 173
    iget v0, p0, Lcom/google/r/b/a/ed;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 175
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 176
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/ed;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 179
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 161
    iget-byte v1, p0, Lcom/google/r/b/a/ed;->f:B

    .line 162
    if-ne v1, v0, :cond_0

    .line 166
    :goto_0
    return v0

    .line 163
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 165
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ed;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/16 v1, 0xa

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 183
    iget v0, p0, Lcom/google/r/b/a/ed;->g:I

    .line 184
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 202
    :goto_0
    return v0

    .line 187
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ed;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 188
    iget v0, p0, Lcom/google/r/b/a/ed;->b:I

    .line 189
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    :goto_2
    move v4, v3

    .line 193
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 194
    iget-object v0, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    .line 195
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v4

    .line 193
    add-int/lit8 v3, v3, 0x1

    move v4, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 189
    goto :goto_1

    :cond_2
    move v0, v1

    .line 195
    goto :goto_4

    .line 197
    :cond_3
    add-int v0, v2, v4

    .line 198
    iget-object v1, p0, Lcom/google/r/b/a/ed;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 200
    iget-object v1, p0, Lcom/google/r/b/a/ed;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    iput v0, p0, Lcom/google/r/b/a/ed;->g:I

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ed;->newBuilder()Lcom/google/r/b/a/eg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/eg;->a(Lcom/google/r/b/a/ed;)Lcom/google/r/b/a/eg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ed;->newBuilder()Lcom/google/r/b/a/eg;

    move-result-object v0

    return-object v0
.end method
