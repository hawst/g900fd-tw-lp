.class public final Lcom/google/r/b/a/amf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/amd;",
        "Lcom/google/r/b/a/amf;",
        ">;",
        "Lcom/google/r/b/a/amg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:F

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:Z

.field private h:Z

.field private i:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2652
    sget-object v0, Lcom/google/r/b/a/amd;->j:Lcom/google/r/b/a/amd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2784
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    .line 2952
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amf;->d:Lcom/google/n/ao;

    .line 3011
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amf;->e:Lcom/google/n/ao;

    .line 3070
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amf;->f:Ljava/lang/Object;

    .line 3210
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/amf;->i:Lcom/google/n/f;

    .line 2653
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2644
    new-instance v2, Lcom/google/r/b/a/amd;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/amd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/amf;->a:I

    iget v4, p0, Lcom/google/r/b/a/amf;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/amf;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/amf;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/amf;->c:F

    iput v4, v2, Lcom/google/r/b/a/amd;->c:F

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/amd;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amf;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amf;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amf;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amf;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/amf;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/amd;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-boolean v1, p0, Lcom/google/r/b/a/amf;->g:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/amd;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-boolean v1, p0, Lcom/google/r/b/a/amf;->h:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/amd;->h:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/amf;->i:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/amd;->i:Lcom/google/n/f;

    iput v0, v2, Lcom/google/r/b/a/amd;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2644
    check-cast p1, Lcom/google/r/b/a/amd;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/amf;->a(Lcom/google/r/b/a/amd;)Lcom/google/r/b/a/amf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/amd;)Lcom/google/r/b/a/amf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2725
    invoke-static {}, Lcom/google/r/b/a/amd;->d()Lcom/google/r/b/a/amd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2762
    :goto_0
    return-object p0

    .line 2726
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2727
    iget-object v2, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2728
    iget-object v2, p1, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    .line 2729
    iget v2, p0, Lcom/google/r/b/a/amf;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/amf;->a:I

    .line 2736
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2737
    iget v2, p1, Lcom/google/r/b/a/amd;->c:F

    iget v3, p0, Lcom/google/r/b/a/amf;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/amf;->a:I

    iput v2, p0, Lcom/google/r/b/a/amf;->c:F

    .line 2739
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 2740
    iget-object v2, p0, Lcom/google/r/b/a/amf;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/amd;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2741
    iget v2, p0, Lcom/google/r/b/a/amf;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/amf;->a:I

    .line 2743
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 2744
    iget-object v2, p0, Lcom/google/r/b/a/amf;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2745
    iget v2, p0, Lcom/google/r/b/a/amf;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/amf;->a:I

    .line 2747
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 2748
    iget v2, p0, Lcom/google/r/b/a/amf;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/amf;->a:I

    .line 2749
    iget-object v2, p1, Lcom/google/r/b/a/amd;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/amf;->f:Ljava/lang/Object;

    .line 2752
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 2753
    iget-boolean v2, p1, Lcom/google/r/b/a/amd;->g:Z

    iget v3, p0, Lcom/google/r/b/a/amf;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/amf;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/amf;->g:Z

    .line 2755
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 2756
    iget-boolean v2, p1, Lcom/google/r/b/a/amd;->h:Z

    iget v3, p0, Lcom/google/r/b/a/amf;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/amf;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/amf;->h:Z

    .line 2758
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_12

    .line 2759
    iget-object v0, p1, Lcom/google/r/b/a/amd;->i:Lcom/google/n/f;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2731
    :cond_8
    iget v2, p0, Lcom/google/r/b/a/amf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/amf;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/amf;->a:I

    .line 2732
    :cond_9
    iget-object v2, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 2736
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 2739
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 2743
    goto/16 :goto_4

    :cond_d
    move v2, v1

    .line 2747
    goto :goto_5

    :cond_e
    move v2, v1

    .line 2752
    goto :goto_6

    :cond_f
    move v2, v1

    .line 2755
    goto :goto_7

    :cond_10
    move v0, v1

    .line 2758
    goto :goto_8

    .line 2759
    :cond_11
    iget v1, p0, Lcom/google/r/b/a/amf;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/amf;->a:I

    iput-object v0, p0, Lcom/google/r/b/a/amf;->i:Lcom/google/n/f;

    .line 2761
    :cond_12
    iget-object v0, p1, Lcom/google/r/b/a/amd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2766
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2767
    iget-object v0, p0, Lcom/google/r/b/a/amf;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/alu;->d()Lcom/google/r/b/a/alu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alu;

    invoke-virtual {v0}, Lcom/google/r/b/a/alu;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2778
    :cond_0
    :goto_1
    return v2

    .line 2766
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2772
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/amf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 2773
    iget-object v0, p0, Lcom/google/r/b/a/amf;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v3

    .line 2778
    goto :goto_1

    :cond_4
    move v0, v2

    .line 2772
    goto :goto_2
.end method
