.class public final Lcom/google/r/b/a/om;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ot;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/om;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/om;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Z

.field public d:Z

.field public e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/r/b/a/on;

    invoke-direct {v0}, Lcom/google/r/b/a/on;-><init>()V

    sput-object v0, Lcom/google/r/b/a/om;->PARSER:Lcom/google/n/ax;

    .line 395
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/om;->i:Lcom/google/n/aw;

    .line 682
    new-instance v0, Lcom/google/r/b/a/om;

    invoke-direct {v0}, Lcom/google/r/b/a/om;-><init>()V

    sput-object v0, Lcom/google/r/b/a/om;->f:Lcom/google/r/b/a/om;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 338
    iput-byte v1, p0, Lcom/google/r/b/a/om;->g:B

    .line 366
    iput v1, p0, Lcom/google/r/b/a/om;->h:I

    .line 18
    iput v0, p0, Lcom/google/r/b/a/om;->b:I

    .line 19
    iput-boolean v0, p0, Lcom/google/r/b/a/om;->c:Z

    .line 20
    iput-boolean v0, p0, Lcom/google/r/b/a/om;->d:Z

    .line 21
    iput v0, p0, Lcom/google/r/b/a/om;->e:I

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/r/b/a/om;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 34
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 49
    invoke-static {v0}, Lcom/google/r/b/a/op;->a(I)Lcom/google/r/b/a/op;

    move-result-object v5

    .line 50
    if-nez v5, :cond_1

    .line 51
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/om;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/r/b/a/om;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/om;->a:I

    .line 54
    iput v0, p0, Lcom/google/r/b/a/om;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_2
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/om;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/om;->a:I

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/om;->c:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 64
    :sswitch_3
    iget v0, p0, Lcom/google/r/b/a/om;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/om;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/om;->d:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 69
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 70
    invoke-static {v0}, Lcom/google/r/b/a/or;->a(I)Lcom/google/r/b/a/or;

    move-result-object v5

    .line 71
    if-nez v5, :cond_4

    .line 72
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 74
    :cond_4
    iget v5, p0, Lcom/google/r/b/a/om;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/r/b/a/om;->a:I

    .line 75
    iput v0, p0, Lcom/google/r/b/a/om;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 87
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/om;->au:Lcom/google/n/bn;

    .line 88
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 338
    iput-byte v0, p0, Lcom/google/r/b/a/om;->g:B

    .line 366
    iput v0, p0, Lcom/google/r/b/a/om;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/om;
    .locals 1

    .prologue
    .line 685
    sget-object v0, Lcom/google/r/b/a/om;->f:Lcom/google/r/b/a/om;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/oo;
    .locals 1

    .prologue
    .line 457
    new-instance v0, Lcom/google/r/b/a/oo;

    invoke-direct {v0}, Lcom/google/r/b/a/oo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/om;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lcom/google/r/b/a/om;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 350
    invoke-virtual {p0}, Lcom/google/r/b/a/om;->c()I

    .line 351
    iget v0, p0, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 352
    iget v0, p0, Lcom/google/r/b/a/om;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 354
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 355
    iget-boolean v0, p0, Lcom/google/r/b/a/om;->c:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 357
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 358
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/om;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 360
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 361
    iget v0, p0, Lcom/google/r/b/a/om;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 363
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/om;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 364
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 340
    iget-byte v1, p0, Lcom/google/r/b/a/om;->g:B

    .line 341
    if-ne v1, v0, :cond_0

    .line 345
    :goto_0
    return v0

    .line 342
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 344
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/om;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v1, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 368
    iget v0, p0, Lcom/google/r/b/a/om;->h:I

    .line 369
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 390
    :goto_0
    return v0

    .line 372
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 373
    iget v0, p0, Lcom/google/r/b/a/om;->b:I

    .line 374
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 376
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 377
    iget-boolean v3, p0, Lcom/google/r/b/a/om;->c:Z

    .line 378
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 380
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 381
    const/4 v3, 0x3

    iget-boolean v4, p0, Lcom/google/r/b/a/om;->d:Z

    .line 382
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 384
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_4

    .line 385
    iget v3, p0, Lcom/google/r/b/a/om;->e:I

    .line 386
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 388
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/om;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 389
    iput v0, p0, Lcom/google/r/b/a/om;->h:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 374
    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/om;->newBuilder()Lcom/google/r/b/a/oo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/oo;->a(Lcom/google/r/b/a/om;)Lcom/google/r/b/a/oo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/om;->newBuilder()Lcom/google/r/b/a/oo;

    move-result-object v0

    return-object v0
.end method
