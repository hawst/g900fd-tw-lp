.class public final Lcom/google/r/b/a/ly;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/lz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/lw;",
        "Lcom/google/r/b/a/ly;",
        ">;",
        "Lcom/google/r/b/a/lz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/r/b/a/ay;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/mi;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/n/aq;

.field private f:Lcom/google/r/b/a/ma;

.field private g:Lcom/google/maps/g/ea;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3819
    sget-object v0, Lcom/google/r/b/a/lw;->h:Lcom/google/r/b/a/lw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3934
    iput-object v1, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    .line 3996
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    .line 4120
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ly;->d:Ljava/lang/Object;

    .line 4196
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    .line 4289
    iput-object v1, p0, Lcom/google/r/b/a/ly;->f:Lcom/google/r/b/a/ma;

    .line 4350
    iput-object v1, p0, Lcom/google/r/b/a/ly;->g:Lcom/google/maps/g/ea;

    .line 3820
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3811
    new-instance v2, Lcom/google/r/b/a/lw;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/lw;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ly;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    iput-object v1, v2, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    iget v1, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/ly;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ly;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/lw;->d:Ljava/lang/Object;

    iget v1, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/r/b/a/ly;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/ly;->f:Lcom/google/r/b/a/ma;

    iput-object v1, v2, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/ly;->g:Lcom/google/maps/g/ea;

    iput-object v1, v2, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    iput v0, v2, Lcom/google/r/b/a/lw;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3811
    check-cast p1, Lcom/google/r/b/a/lw;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ly;->a(Lcom/google/r/b/a/lw;)Lcom/google/r/b/a/ly;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/lw;)Lcom/google/r/b/a/ly;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3877
    invoke-static {}, Lcom/google/r/b/a/lw;->d()Lcom/google/r/b/a/lw;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 3913
    :goto_0
    return-object p0

    .line 3878
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 3879
    iget-object v0, p1, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_9

    iget-object v3, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v4

    if-eq v3, v4, :cond_9

    iget-object v3, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    invoke-static {v3}, Lcom/google/r/b/a/ay;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/ba;->c()Lcom/google/r/b/a/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    :goto_3
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ly;->a:I

    .line 3881
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3882
    iget-object v0, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 3883
    iget-object v0, p1, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    .line 3884
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/ly;->a:I

    .line 3891
    :cond_2
    :goto_4
    iget v0, p1, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_c

    move v0, v1

    :goto_5
    if-eqz v0, :cond_3

    .line 3892
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ly;->a:I

    .line 3893
    iget-object v0, p1, Lcom/google/r/b/a/lw;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/ly;->d:Ljava/lang/Object;

    .line 3896
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3897
    iget-object v0, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 3898
    iget-object v0, p1, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    .line 3899
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/r/b/a/ly;->a:I

    .line 3906
    :cond_4
    :goto_6
    iget v0, p1, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_f

    move v0, v1

    :goto_7
    if-eqz v0, :cond_5

    .line 3907
    iget-object v0, p1, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/r/b/a/ma;->d()Lcom/google/r/b/a/ma;

    move-result-object v0

    :goto_8
    iget v3, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_11

    iget-object v3, p0, Lcom/google/r/b/a/ly;->f:Lcom/google/r/b/a/ma;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lcom/google/r/b/a/ly;->f:Lcom/google/r/b/a/ma;

    invoke-static {}, Lcom/google/r/b/a/ma;->d()Lcom/google/r/b/a/ma;

    move-result-object v4

    if-eq v3, v4, :cond_11

    iget-object v3, p0, Lcom/google/r/b/a/ly;->f:Lcom/google/r/b/a/ma;

    invoke-static {v3}, Lcom/google/r/b/a/ma;->a(Lcom/google/r/b/a/ma;)Lcom/google/r/b/a/mc;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/mc;->a(Lcom/google/r/b/a/ma;)Lcom/google/r/b/a/mc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/mc;->c()Lcom/google/r/b/a/ma;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ly;->f:Lcom/google/r/b/a/ma;

    :goto_9
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/ly;->a:I

    .line 3909
    :cond_5
    iget v0, p1, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_12

    move v0, v1

    :goto_a
    if-eqz v0, :cond_6

    .line 3910
    iget-object v0, p1, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    if-nez v0, :cond_13

    invoke-static {}, Lcom/google/maps/g/ea;->d()Lcom/google/maps/g/ea;

    move-result-object v0

    :goto_b
    iget v1, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_14

    iget-object v1, p0, Lcom/google/r/b/a/ly;->g:Lcom/google/maps/g/ea;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/google/r/b/a/ly;->g:Lcom/google/maps/g/ea;

    invoke-static {}, Lcom/google/maps/g/ea;->d()Lcom/google/maps/g/ea;

    move-result-object v2

    if-eq v1, v2, :cond_14

    iget-object v1, p0, Lcom/google/r/b/a/ly;->g:Lcom/google/maps/g/ea;

    invoke-static {v1}, Lcom/google/maps/g/ea;->a(Lcom/google/maps/g/ea;)Lcom/google/maps/g/ec;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/ec;->a(Lcom/google/maps/g/ea;)Lcom/google/maps/g/ec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/ec;->c()Lcom/google/maps/g/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ly;->g:Lcom/google/maps/g/ea;

    :goto_c
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/ly;->a:I

    .line 3912
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/lw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 3878
    goto/16 :goto_1

    .line 3879
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    goto/16 :goto_2

    :cond_9
    iput-object v0, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    goto/16 :goto_3

    .line 3886
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eq v0, v5, :cond_b

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ly;->a:I

    .line 3887
    :cond_b
    iget-object v0, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_c
    move v0, v2

    .line 3891
    goto/16 :goto_5

    .line 3901
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v0, v0, 0x8

    if-eq v0, v6, :cond_e

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/ly;->a:I

    .line 3902
    :cond_e
    iget-object v0, p0, Lcom/google/r/b/a/ly;->e:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_f
    move v0, v2

    .line 3906
    goto/16 :goto_7

    .line 3907
    :cond_10
    iget-object v0, p1, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    goto/16 :goto_8

    :cond_11
    iput-object v0, p0, Lcom/google/r/b/a/ly;->f:Lcom/google/r/b/a/ma;

    goto/16 :goto_9

    :cond_12
    move v0, v2

    .line 3909
    goto/16 :goto_a

    .line 3910
    :cond_13
    iget-object v0, p1, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    goto/16 :goto_b

    :cond_14
    iput-object v0, p0, Lcom/google/r/b/a/ly;->g:Lcom/google/maps/g/ea;

    goto :goto_c
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3917
    iget v0, p0, Lcom/google/r/b/a/ly;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 3918
    iget-object v0, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/r/b/a/ay;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3929
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    .line 3917
    goto :goto_0

    .line 3918
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ly;->b:Lcom/google/r/b/a/ay;

    goto :goto_1

    :cond_3
    move v1, v2

    .line 3923
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 3924
    iget-object v0, p0, Lcom/google/r/b/a/ly;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/mi;

    invoke-virtual {v0}, Lcom/google/r/b/a/mi;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3923
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v2, v3

    .line 3929
    goto :goto_2
.end method
