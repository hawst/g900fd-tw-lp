.class public final Lcom/google/r/b/a/alu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aly;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/alu;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/alu;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:I

.field e:Lcom/google/n/ao;

.field public f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1330
    new-instance v0, Lcom/google/r/b/a/alv;

    invoke-direct {v0}, Lcom/google/r/b/a/alv;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alu;->PARSER:Lcom/google/n/ax;

    .line 1559
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/alu;->j:Lcom/google/n/aw;

    .line 2079
    new-instance v0, Lcom/google/r/b/a/alu;

    invoke-direct {v0}, Lcom/google/r/b/a/alu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alu;->g:Lcom/google/r/b/a/alu;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1250
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1346
    iput v3, p0, Lcom/google/r/b/a/alu;->b:I

    .line 1430
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alu;->e:Lcom/google/n/ao;

    .line 1487
    iput-byte v2, p0, Lcom/google/r/b/a/alu;->h:B

    .line 1526
    iput v2, p0, Lcom/google/r/b/a/alu;->i:I

    .line 1251
    iput v3, p0, Lcom/google/r/b/a/alu;->d:I

    .line 1252
    iget-object v0, p0, Lcom/google/r/b/a/alu;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1253
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    .line 1254
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1260
    invoke-direct {p0}, Lcom/google/r/b/a/alu;-><init>()V

    .line 1261
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 1266
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 1267
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1268
    sparse-switch v0, :sswitch_data_0

    .line 1273
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 1275
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 1271
    goto :goto_0

    .line 1280
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/alu;->b:I

    if-eq v0, v2, :cond_1

    .line 1281
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    .line 1283
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1284
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 1283
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 1285
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/alu;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1321
    :catch_0
    move-exception v0

    .line 1322
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1327
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/alu;->au:Lcom/google/n/bn;

    throw v0

    .line 1289
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 1290
    invoke-static {v0}, Lcom/google/maps/g/ta;->a(I)Lcom/google/maps/g/ta;

    move-result-object v4

    .line 1291
    if-nez v4, :cond_2

    .line 1292
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1323
    :catch_1
    move-exception v0

    .line 1324
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1325
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1294
    :cond_2
    :try_start_4
    iget v4, p0, Lcom/google/r/b/a/alu;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/alu;->a:I

    .line 1295
    iput v0, p0, Lcom/google/r/b/a/alu;->d:I

    goto :goto_0

    .line 1300
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 1301
    iget v4, p0, Lcom/google/r/b/a/alu;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/alu;->a:I

    .line 1302
    iput-object v0, p0, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    goto :goto_0

    .line 1306
    :sswitch_4
    iget-object v0, p0, Lcom/google/r/b/a/alu;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 1307
    iget v0, p0, Lcom/google/r/b/a/alu;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/alu;->a:I

    goto/16 :goto_0

    .line 1311
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/alu;->b:I

    if-eq v0, v5, :cond_3

    .line 1312
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    .line 1314
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1315
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 1314
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 1316
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/r/b/a/alu;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1327
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alu;->au:Lcom/google/n/bn;

    .line 1328
    return-void

    .line 1268
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1248
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1346
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/alu;->b:I

    .line 1430
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alu;->e:Lcom/google/n/ao;

    .line 1487
    iput-byte v1, p0, Lcom/google/r/b/a/alu;->h:B

    .line 1526
    iput v1, p0, Lcom/google/r/b/a/alu;->i:I

    .line 1249
    return-void
.end method

.method public static d()Lcom/google/r/b/a/alu;
    .locals 1

    .prologue
    .line 2082
    sget-object v0, Lcom/google/r/b/a/alu;->g:Lcom/google/r/b/a/alu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/alw;
    .locals 1

    .prologue
    .line 1621
    new-instance v0, Lcom/google/r/b/a/alw;

    invoke-direct {v0}, Lcom/google/r/b/a/alw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/alu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342
    sget-object v0, Lcom/google/r/b/a/alu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v1, 0x1

    .line 1505
    invoke-virtual {p0}, Lcom/google/r/b/a/alu;->c()I

    .line 1506
    iget v0, p0, Lcom/google/r/b/a/alu;->b:I

    if-ne v0, v1, :cond_0

    .line 1507
    iget-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1508
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 1507
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1510
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_1

    .line 1511
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/r/b/a/alu;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 1513
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 1514
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1516
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1517
    iget-object v0, p0, Lcom/google/r/b/a/alu;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1519
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/alu;->b:I

    if-ne v0, v4, :cond_4

    .line 1520
    iget-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1521
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 1520
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1523
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/alu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1524
    return-void

    .line 1514
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1489
    iget-byte v0, p0, Lcom/google/r/b/a/alu;->h:B

    .line 1490
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1500
    :goto_0
    return v0

    .line 1491
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1493
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/alu;->b:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 1494
    iget v0, p0, Lcom/google/r/b/a/alu;->b:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    :goto_2
    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1495
    iput-byte v2, p0, Lcom/google/r/b/a/alu;->h:B

    move v0, v2

    .line 1496
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1493
    goto :goto_1

    .line 1494
    :cond_3
    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v0

    goto :goto_2

    .line 1499
    :cond_4
    iput-byte v1, p0, Lcom/google/r/b/a/alu;->h:B

    move v0, v1

    .line 1500
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1528
    iget v0, p0, Lcom/google/r/b/a/alu;->i:I

    .line 1529
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1554
    :goto_0
    return v0

    .line 1532
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/alu;->b:I

    if-ne v0, v3, :cond_7

    .line 1533
    iget-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1534
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1536
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_6

    .line 1537
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/r/b/a/alu;->d:I

    .line 1538
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 1540
    :goto_3
    iget v0, p0, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    .line 1541
    const/4 v3, 0x3

    .line 1542
    iget-object v0, p0, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1544
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/alu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 1545
    iget-object v0, p0, Lcom/google/r/b/a/alu;->e:Lcom/google/n/ao;

    .line 1546
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1548
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/alu;->b:I

    if-ne v0, v6, :cond_3

    .line 1549
    iget-object v0, p0, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 1550
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1552
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/alu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1553
    iput v0, p0, Lcom/google/r/b/a/alu;->i:I

    goto/16 :goto_0

    .line 1538
    :cond_4
    const/16 v2, 0xa

    goto :goto_2

    .line 1542
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v2, v0

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1242
    invoke-static {}, Lcom/google/r/b/a/alu;->newBuilder()Lcom/google/r/b/a/alw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/alw;->a(Lcom/google/r/b/a/alu;)Lcom/google/r/b/a/alw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1242
    invoke-static {}, Lcom/google/r/b/a/alu;->newBuilder()Lcom/google/r/b/a/alw;

    move-result-object v0

    return-object v0
.end method
