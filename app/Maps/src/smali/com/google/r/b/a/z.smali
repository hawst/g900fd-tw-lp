.class public final Lcom/google/r/b/a/z;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ac;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/z;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/z;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2127
    new-instance v0, Lcom/google/r/b/a/aa;

    invoke-direct {v0}, Lcom/google/r/b/a/aa;-><init>()V

    sput-object v0, Lcom/google/r/b/a/z;->PARSER:Lcom/google/n/ax;

    .line 2271
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/z;->f:Lcom/google/n/aw;

    .line 2692
    new-instance v0, Lcom/google/r/b/a/z;

    invoke-direct {v0}, Lcom/google/r/b/a/z;-><init>()V

    sput-object v0, Lcom/google/r/b/a/z;->c:Lcom/google/r/b/a/z;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2062
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2228
    iput-byte v0, p0, Lcom/google/r/b/a/z;->d:B

    .line 2250
    iput v0, p0, Lcom/google/r/b/a/z;->e:I

    .line 2063
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    .line 2064
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    .line 2065
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    .line 2071
    invoke-direct {p0}, Lcom/google/r/b/a/z;-><init>()V

    .line 2074
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 2077
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 2078
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 2079
    sparse-switch v1, :sswitch_data_0

    .line 2084
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 2086
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 2082
    goto :goto_0

    .line 2091
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_7

    .line 2092
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2094
    or-int/lit8 v1, v0, 0x1

    .line 2096
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 2097
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 2096
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 2098
    goto :goto_0

    .line 2101
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_1

    .line 2102
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    .line 2104
    or-int/lit8 v0, v0, 0x2

    .line 2106
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 2107
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 2106
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2112
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 2113
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2118
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_2

    .line 2119
    iget-object v2, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    .line 2121
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_3

    .line 2122
    iget-object v1, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    .line 2124
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/z;->au:Lcom/google/n/bn;

    throw v0

    .line 2118
    :cond_4
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_5

    .line 2119
    iget-object v1, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    .line 2121
    :cond_5
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_6

    .line 2122
    iget-object v0, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    .line 2124
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/z;->au:Lcom/google/n/bn;

    .line 2125
    return-void

    .line 2114
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 2115
    :goto_4
    :try_start_4
    new-instance v2, Lcom/google/n/ak;

    .line 2116
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2118
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_3

    .line 2114
    :catch_2
    move-exception v0

    goto :goto_4

    .line 2112
    :catch_3
    move-exception v0

    goto :goto_2

    :cond_7
    move v1, v0

    goto/16 :goto_1

    .line 2079
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2060
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2228
    iput-byte v0, p0, Lcom/google/r/b/a/z;->d:B

    .line 2250
    iput v0, p0, Lcom/google/r/b/a/z;->e:I

    .line 2061
    return-void
.end method

.method public static g()Lcom/google/r/b/a/z;
    .locals 1

    .prologue
    .line 2695
    sget-object v0, Lcom/google/r/b/a/z;->c:Lcom/google/r/b/a/z;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ab;
    .locals 1

    .prologue
    .line 2333
    new-instance v0, Lcom/google/r/b/a/ab;

    invoke-direct {v0}, Lcom/google/r/b/a/ab;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2139
    sget-object v0, Lcom/google/r/b/a/z;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2240
    invoke-virtual {p0}, Lcom/google/r/b/a/z;->c()I

    move v1, v2

    .line 2241
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2242
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2244
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 2245
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2244
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2247
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/z;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2248
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2230
    iget-byte v1, p0, Lcom/google/r/b/a/z;->d:B

    .line 2231
    if-ne v1, v0, :cond_0

    .line 2235
    :goto_0
    return v0

    .line 2232
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2234
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/z;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2252
    iget v0, p0, Lcom/google/r/b/a/z;->e:I

    .line 2253
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2266
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 2256
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2257
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    .line 2258
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2256
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 2260
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2261
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    .line 2262
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2260
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2264
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/z;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2265
    iput v0, p0, Lcom/google/r/b/a/z;->e:I

    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2149
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    .line 2150
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2151
    iget-object v0, p0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 2152
    invoke-static {}, Lcom/google/r/b/a/j;->g()Lcom/google/r/b/a/j;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/j;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2154
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2054
    invoke-static {}, Lcom/google/r/b/a/z;->newBuilder()Lcom/google/r/b/a/ab;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ab;->a(Lcom/google/r/b/a/z;)Lcom/google/r/b/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2054
    invoke-static {}, Lcom/google/r/b/a/z;->newBuilder()Lcom/google/r/b/a/ab;

    move-result-object v0

    return-object v0
.end method
