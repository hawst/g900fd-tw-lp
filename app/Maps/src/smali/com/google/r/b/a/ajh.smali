.class public final enum Lcom/google/r/b/a/ajh;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ajh;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ajh;

.field public static final enum b:Lcom/google/r/b/a/ajh;

.field private static final synthetic d:[Lcom/google/r/b/a/ajh;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1614
    new-instance v0, Lcom/google/r/b/a/ajh;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/r/b/a/ajh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    .line 1618
    new-instance v0, Lcom/google/r/b/a/ajh;

    const-string v1, "FIFE"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/r/b/a/ajh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ajh;->b:Lcom/google/r/b/a/ajh;

    .line 1609
    new-array v0, v4, [Lcom/google/r/b/a/ajh;

    sget-object v1, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/ajh;->b:Lcom/google/r/b/a/ajh;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/r/b/a/ajh;->d:[Lcom/google/r/b/a/ajh;

    .line 1648
    new-instance v0, Lcom/google/r/b/a/aji;

    invoke-direct {v0}, Lcom/google/r/b/a/aji;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1657
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1658
    iput p3, p0, Lcom/google/r/b/a/ajh;->c:I

    .line 1659
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ajh;
    .locals 1

    .prologue
    .line 1636
    packed-switch p0, :pswitch_data_0

    .line 1639
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1637
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    goto :goto_0

    .line 1638
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ajh;->b:Lcom/google/r/b/a/ajh;

    goto :goto_0

    .line 1636
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ajh;
    .locals 1

    .prologue
    .line 1609
    const-class v0, Lcom/google/r/b/a/ajh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ajh;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ajh;
    .locals 1

    .prologue
    .line 1609
    sget-object v0, Lcom/google/r/b/a/ajh;->d:[Lcom/google/r/b/a/ajh;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ajh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ajh;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1632
    iget v0, p0, Lcom/google/r/b/a/ajh;->c:I

    return v0
.end method
