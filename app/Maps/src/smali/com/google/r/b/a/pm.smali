.class public final Lcom/google/r/b/a/pm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/pn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/pk;",
        "Lcom/google/r/b/a/pm;",
        ">;",
        "Lcom/google/r/b/a/pn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x7d0

    .line 319
    sget-object v0, Lcom/google/r/b/a/pk;->g:Lcom/google/r/b/a/pk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 395
    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/r/b/a/pm;->b:I

    .line 427
    iput v1, p0, Lcom/google/r/b/a/pm;->c:I

    .line 459
    iput v1, p0, Lcom/google/r/b/a/pm;->d:I

    .line 491
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/r/b/a/pm;->e:I

    .line 320
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 311
    new-instance v2, Lcom/google/r/b/a/pk;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/pk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/pm;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/pm;->b:I

    iput v1, v2, Lcom/google/r/b/a/pk;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/pm;->c:I

    iput v1, v2, Lcom/google/r/b/a/pk;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/pm;->d:I

    iput v1, v2, Lcom/google/r/b/a/pk;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/pm;->e:I

    iput v1, v2, Lcom/google/r/b/a/pk;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/r/b/a/pm;->f:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/pk;->f:Z

    iput v0, v2, Lcom/google/r/b/a/pk;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 311
    check-cast p1, Lcom/google/r/b/a/pk;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/pm;->a(Lcom/google/r/b/a/pk;)Lcom/google/r/b/a/pm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/pk;)Lcom/google/r/b/a/pm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 369
    invoke-static {}, Lcom/google/r/b/a/pk;->d()Lcom/google/r/b/a/pk;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 386
    :goto_0
    return-object p0

    .line 370
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 371
    iget v2, p1, Lcom/google/r/b/a/pk;->b:I

    iget v3, p0, Lcom/google/r/b/a/pm;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/pm;->a:I

    iput v2, p0, Lcom/google/r/b/a/pm;->b:I

    .line 373
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 374
    iget v2, p1, Lcom/google/r/b/a/pk;->c:I

    iget v3, p0, Lcom/google/r/b/a/pm;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/pm;->a:I

    iput v2, p0, Lcom/google/r/b/a/pm;->c:I

    .line 376
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 377
    iget v2, p1, Lcom/google/r/b/a/pk;->d:I

    iget v3, p0, Lcom/google/r/b/a/pm;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/pm;->a:I

    iput v2, p0, Lcom/google/r/b/a/pm;->d:I

    .line 379
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 380
    iget v2, p1, Lcom/google/r/b/a/pk;->e:I

    iget v3, p0, Lcom/google/r/b/a/pm;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/pm;->a:I

    iput v2, p0, Lcom/google/r/b/a/pm;->e:I

    .line 382
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 383
    iget-boolean v0, p1, Lcom/google/r/b/a/pk;->f:Z

    iget v1, p0, Lcom/google/r/b/a/pm;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/r/b/a/pm;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/pm;->f:Z

    .line 385
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/pk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 370
    goto :goto_1

    :cond_7
    move v2, v1

    .line 373
    goto :goto_2

    :cond_8
    move v2, v1

    .line 376
    goto :goto_3

    :cond_9
    move v2, v1

    .line 379
    goto :goto_4

    :cond_a
    move v0, v1

    .line 382
    goto :goto_5
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x1

    return v0
.end method
