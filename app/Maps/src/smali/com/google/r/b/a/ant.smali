.class public final Lcom/google/r/b/a/ant;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/anu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/anr;",
        "Lcom/google/r/b/a/ant;",
        ">;",
        "Lcom/google/r/b/a/anu;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/aq;

.field private e:Lcom/google/n/aq;

.field private f:Lcom/google/n/ao;

.field private g:Ljava/lang/Object;

.field private h:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 464
    sget-object v0, Lcom/google/r/b/a/anr;->i:Lcom/google/r/b/a/anr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 591
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ant;->b:Lcom/google/n/ao;

    .line 650
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ant;->c:Lcom/google/n/ao;

    .line 709
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    .line 802
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    .line 895
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ant;->f:Lcom/google/n/ao;

    .line 954
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ant;->g:Ljava/lang/Object;

    .line 465
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 456
    new-instance v2, Lcom/google/r/b/a/anr;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/anr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/anr;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ant;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ant;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/anr;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ant;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ant;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/r/b/a/ant;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/r/b/a/ant;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v4, v2, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ant;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ant;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/ant;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/anr;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget v1, p0, Lcom/google/r/b/a/ant;->h:I

    iput v1, v2, Lcom/google/r/b/a/anr;->h:I

    iput v0, v2, Lcom/google/r/b/a/anr;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 456
    check-cast p1, Lcom/google/r/b/a/anr;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ant;->a(Lcom/google/r/b/a/anr;)Lcom/google/r/b/a/ant;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/anr;)Lcom/google/r/b/a/ant;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 534
    invoke-static {}, Lcom/google/r/b/a/anr;->d()Lcom/google/r/b/a/anr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 576
    :goto_0
    return-object p0

    .line 535
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 536
    iget-object v2, p0, Lcom/google/r/b/a/ant;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/anr;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 537
    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ant;->a:I

    .line 539
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 540
    iget-object v2, p0, Lcom/google/r/b/a/ant;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/anr;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 541
    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ant;->a:I

    .line 543
    :cond_2
    iget-object v2, p1, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 544
    iget-object v2, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 545
    iget-object v2, p1, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    .line 546
    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/r/b/a/ant;->a:I

    .line 553
    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 554
    iget-object v2, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 555
    iget-object v2, p1, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    .line 556
    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/r/b/a/ant;->a:I

    .line 563
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 564
    iget-object v2, p0, Lcom/google/r/b/a/ant;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 565
    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/ant;->a:I

    .line 567
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 568
    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/ant;->a:I

    .line 569
    iget-object v2, p1, Lcom/google/r/b/a/anr;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ant;->g:Ljava/lang/Object;

    .line 572
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    :goto_7
    if-eqz v0, :cond_7

    .line 573
    iget v0, p1, Lcom/google/r/b/a/anr;->h:I

    iget v1, p0, Lcom/google/r/b/a/ant;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/r/b/a/ant;->a:I

    iput v0, p0, Lcom/google/r/b/a/ant;->h:I

    .line 575
    :cond_7
    iget-object v0, p1, Lcom/google/r/b/a/anr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_8
    move v2, v1

    .line 535
    goto/16 :goto_1

    :cond_9
    move v2, v1

    .line 539
    goto/16 :goto_2

    .line 548
    :cond_a
    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_b

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/ant;->a:I

    .line 549
    :cond_b
    iget-object v2, p0, Lcom/google/r/b/a/ant;->d:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 558
    :cond_c
    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v5, :cond_d

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/r/b/a/ant;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/ant;->a:I

    .line 559
    :cond_d
    iget-object v2, p0, Lcom/google/r/b/a/ant;->e:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 563
    goto/16 :goto_5

    :cond_f
    move v2, v1

    .line 567
    goto :goto_6

    :cond_10
    move v0, v1

    .line 572
    goto :goto_7
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 580
    iget v0, p0, Lcom/google/r/b/a/ant;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 581
    iget-object v0, p0, Lcom/google/r/b/a/ant;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 586
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 580
    goto :goto_0

    :cond_1
    move v0, v2

    .line 586
    goto :goto_1
.end method
