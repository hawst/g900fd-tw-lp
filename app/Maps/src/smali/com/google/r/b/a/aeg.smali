.class public final Lcom/google/r/b/a/aeg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aej;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aeg;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/aeg;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19684
    new-instance v0, Lcom/google/r/b/a/aeh;

    invoke-direct {v0}, Lcom/google/r/b/a/aeh;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aeg;->PARSER:Lcom/google/n/ax;

    .line 19926
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aeg;->i:Lcom/google/n/aw;

    .line 20456
    new-instance v0, Lcom/google/r/b/a/aeg;

    invoke-direct {v0}, Lcom/google/r/b/a/aeg;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aeg;->f:Lcom/google/r/b/a/aeg;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19612
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 19869
    iput-byte v0, p0, Lcom/google/r/b/a/aeg;->g:B

    .line 19897
    iput v0, p0, Lcom/google/r/b/a/aeg;->h:I

    .line 19613
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    .line 19614
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->c:Ljava/lang/Object;

    .line 19615
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->d:Ljava/lang/Object;

    .line 19616
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->e:Ljava/lang/Object;

    .line 19617
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 19623
    invoke-direct {p0}, Lcom/google/r/b/a/aeg;-><init>()V

    .line 19626
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 19629
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 19630
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 19631
    sparse-switch v4, :sswitch_data_0

    .line 19636
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 19638
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 19634
    goto :goto_0

    .line 19643
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 19644
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    .line 19646
    or-int/lit8 v1, v1, 0x1

    .line 19648
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 19649
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 19648
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 19672
    :catch_0
    move-exception v0

    .line 19673
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 19678
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 19679
    iget-object v1, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    .line 19681
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aeg;->au:Lcom/google/n/bn;

    throw v0

    .line 19653
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 19654
    iget v5, p0, Lcom/google/r/b/a/aeg;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/aeg;->a:I

    .line 19655
    iput-object v4, p0, Lcom/google/r/b/a/aeg;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 19674
    :catch_1
    move-exception v0

    .line 19675
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 19676
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 19659
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 19660
    iget v5, p0, Lcom/google/r/b/a/aeg;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/aeg;->a:I

    .line 19661
    iput-object v4, p0, Lcom/google/r/b/a/aeg;->d:Ljava/lang/Object;

    goto :goto_0

    .line 19665
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 19666
    iget v5, p0, Lcom/google/r/b/a/aeg;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/aeg;->a:I

    .line 19667
    iput-object v4, p0, Lcom/google/r/b/a/aeg;->e:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 19678
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 19679
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    .line 19681
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->au:Lcom/google/n/bn;

    .line 19682
    return-void

    .line 19631
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19610
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 19869
    iput-byte v0, p0, Lcom/google/r/b/a/aeg;->g:B

    .line 19897
    iput v0, p0, Lcom/google/r/b/a/aeg;->h:I

    .line 19611
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aeg;
    .locals 1

    .prologue
    .line 20459
    sget-object v0, Lcom/google/r/b/a/aeg;->f:Lcom/google/r/b/a/aeg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aei;
    .locals 1

    .prologue
    .line 19988
    new-instance v0, Lcom/google/r/b/a/aei;

    invoke-direct {v0}, Lcom/google/r/b/a/aei;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aeg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19696
    sget-object v0, Lcom/google/r/b/a/aeg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 19881
    invoke-virtual {p0}, Lcom/google/r/b/a/aeg;->c()I

    .line 19882
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 19883
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 19882
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 19885
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 19886
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 19888
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 19889
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/aeg;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 19891
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 19892
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->e:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 19894
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 19895
    return-void

    .line 19886
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 19889
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 19892
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 19871
    iget-byte v1, p0, Lcom/google/r/b/a/aeg;->g:B

    .line 19872
    if-ne v1, v0, :cond_0

    .line 19876
    :goto_0
    return v0

    .line 19873
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 19875
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aeg;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 19899
    iget v0, p0, Lcom/google/r/b/a/aeg;->h:I

    .line 19900
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 19921
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 19903
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 19904
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    .line 19905
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 19903
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 19907
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 19909
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 19911
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 19912
    const/4 v1, 0x3

    .line 19913
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 19915
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 19917
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeg;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 19919
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/aeg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 19920
    iput v0, p0, Lcom/google/r/b/a/aeg;->h:I

    goto/16 :goto_0

    .line 19909
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 19913
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 19917
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 19604
    invoke-static {}, Lcom/google/r/b/a/aeg;->newBuilder()Lcom/google/r/b/a/aei;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aei;->a(Lcom/google/r/b/a/aeg;)Lcom/google/r/b/a/aei;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 19604
    invoke-static {}, Lcom/google/r/b/a/aeg;->newBuilder()Lcom/google/r/b/a/aei;

    move-result-object v0

    return-object v0
.end method
