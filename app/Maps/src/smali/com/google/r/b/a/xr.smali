.class public final Lcom/google/r/b/a/xr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/xs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/xo;",
        "Lcom/google/r/b/a/xr;",
        ">;",
        "Lcom/google/r/b/a/xs;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 597
    sget-object v0, Lcom/google/r/b/a/xo;->k:Lcom/google/r/b/a/xo;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 753
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->b:Lcom/google/n/ao;

    .line 812
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->h:Lcom/google/n/ao;

    .line 871
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->c:Lcom/google/n/ao;

    .line 930
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->d:Lcom/google/n/ao;

    .line 989
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->e:Lcom/google/n/ao;

    .line 1048
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->i:Lcom/google/n/ao;

    .line 1107
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->f:Lcom/google/n/ao;

    .line 1167
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    .line 1240
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->g:Lcom/google/n/ao;

    .line 598
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1169
    iget v0, p0, Lcom/google/r/b/a/xr;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 1170
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    .line 1171
    iget v0, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/xr;->a:I

    .line 1173
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 589
    new-instance v2, Lcom/google/r/b/a/xo;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/xo;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/xr;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/xr;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/xr;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/xo;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/xr;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/xr;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/xo;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/xr;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/xr;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/xo;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/xr;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/xr;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/r/b/a/xo;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/xr;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/xr;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/xo;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/xr;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/xr;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/xo;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/xr;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/xr;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/xr;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/xr;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/r/b/a/xr;->a:I

    :cond_6
    iget-object v4, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v3, v2, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/xr;->g:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/xr;->g:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/xo;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 589
    check-cast p1, Lcom/google/r/b/a/xo;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/xr;->a(Lcom/google/r/b/a/xo;)Lcom/google/r/b/a/xr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/xo;)Lcom/google/r/b/a/xr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 688
    invoke-static {}, Lcom/google/r/b/a/xo;->d()Lcom/google/r/b/a/xo;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 732
    :goto_0
    return-object p0

    .line 689
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 690
    iget-object v2, p0, Lcom/google/r/b/a/xr;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 691
    iget v2, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/xr;->a:I

    .line 693
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 694
    iget-object v2, p0, Lcom/google/r/b/a/xr;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/xo;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 695
    iget v2, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/xr;->a:I

    .line 697
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 698
    iget-object v2, p0, Lcom/google/r/b/a/xr;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/xo;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 699
    iget v2, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/xr;->a:I

    .line 701
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 702
    iget-object v2, p0, Lcom/google/r/b/a/xr;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/xo;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 703
    iget v2, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/xr;->a:I

    .line 705
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 706
    iget-object v2, p0, Lcom/google/r/b/a/xr;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/xo;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 707
    iget v2, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/xr;->a:I

    .line 709
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 710
    iget-object v2, p0, Lcom/google/r/b/a/xr;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/xo;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 711
    iget v2, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/xr;->a:I

    .line 713
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 714
    iget-object v2, p0, Lcom/google/r/b/a/xr;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/xo;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 715
    iget v2, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/xr;->a:I

    .line 717
    :cond_7
    iget-object v2, p1, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 718
    iget-object v2, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 719
    iget-object v2, p1, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    .line 720
    iget v2, p0, Lcom/google/r/b/a/xr;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/r/b/a/xr;->a:I

    .line 727
    :cond_8
    :goto_8
    iget v2, p1, Lcom/google/r/b/a/xo;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_9
    if-eqz v0, :cond_9

    .line 728
    iget-object v0, p0, Lcom/google/r/b/a/xr;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 729
    iget v0, p0, Lcom/google/r/b/a/xr;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/xr;->a:I

    .line 731
    :cond_9
    iget-object v0, p1, Lcom/google/r/b/a/xo;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 689
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 693
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 697
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 701
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 705
    goto/16 :goto_5

    :cond_f
    move v2, v1

    .line 709
    goto :goto_6

    :cond_10
    move v2, v1

    .line 713
    goto :goto_7

    .line 722
    :cond_11
    invoke-direct {p0}, Lcom/google/r/b/a/xr;->c()V

    .line 723
    iget-object v2, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_12
    move v0, v1

    .line 727
    goto :goto_9
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/r/b/a/xr;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/r/b/a/acy;",
            ">;)",
            "Lcom/google/r/b/a/xr;"
        }
    .end annotation

    .prologue
    .line 1223
    invoke-direct {p0}, Lcom/google/r/b/a/xr;->c()V

    .line 1224
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acy;

    .line 1225
    iget-object v2, p0, Lcom/google/r/b/a/xr;->j:Ljava/util/List;

    iget v0, v0, Lcom/google/r/b/a/acy;->t:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1228
    :cond_0
    return-object p0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 736
    iget v0, p0, Lcom/google/r/b/a/xr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 737
    iget-object v0, p0, Lcom/google/r/b/a/xr;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mf;->d()Lcom/google/o/h/a/mf;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mf;

    invoke-virtual {v0}, Lcom/google/o/h/a/mf;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 748
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 736
    goto :goto_0

    .line 742
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/xr;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 743
    iget-object v0, p0, Lcom/google/r/b/a/xr;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 745
    goto :goto_1

    :cond_2
    move v0, v1

    .line 742
    goto :goto_2

    :cond_3
    move v0, v2

    .line 748
    goto :goto_1
.end method
