.class public final Lcom/google/r/b/a/px;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/px;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/px;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/f;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1214
    new-instance v0, Lcom/google/r/b/a/py;

    invoke-direct {v0}, Lcom/google/r/b/a/py;-><init>()V

    sput-object v0, Lcom/google/r/b/a/px;->PARSER:Lcom/google/n/ax;

    .line 1333
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/px;->h:Lcom/google/n/aw;

    .line 1640
    new-instance v0, Lcom/google/r/b/a/px;

    invoke-direct {v0}, Lcom/google/r/b/a/px;-><init>()V

    sput-object v0, Lcom/google/r/b/a/px;->e:Lcom/google/r/b/a/px;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1159
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1246
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/px;->c:Lcom/google/n/ao;

    .line 1262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    .line 1277
    iput-byte v2, p0, Lcom/google/r/b/a/px;->f:B

    .line 1308
    iput v2, p0, Lcom/google/r/b/a/px;->g:I

    .line 1160
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/px;->b:Lcom/google/n/f;

    .line 1161
    iget-object v0, p0, Lcom/google/r/b/a/px;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1162
    iget-object v0, p0, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1163
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1169
    invoke-direct {p0}, Lcom/google/r/b/a/px;-><init>()V

    .line 1170
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1175
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1176
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1177
    sparse-switch v3, :sswitch_data_0

    .line 1182
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1184
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1180
    goto :goto_0

    .line 1189
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/px;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/px;->a:I

    .line 1190
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/r/b/a/px;->b:Lcom/google/n/f;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1205
    :catch_0
    move-exception v0

    .line 1206
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1211
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/px;->au:Lcom/google/n/bn;

    throw v0

    .line 1194
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/px;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1195
    iget v3, p0, Lcom/google/r/b/a/px;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/px;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1207
    :catch_1
    move-exception v0

    .line 1208
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1209
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1199
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1200
    iget v3, p0, Lcom/google/r/b/a/px;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/px;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1211
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/px;->au:Lcom/google/n/bn;

    .line 1212
    return-void

    .line 1177
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1157
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1246
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/px;->c:Lcom/google/n/ao;

    .line 1262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    .line 1277
    iput-byte v1, p0, Lcom/google/r/b/a/px;->f:B

    .line 1308
    iput v1, p0, Lcom/google/r/b/a/px;->g:I

    .line 1158
    return-void
.end method

.method public static d()Lcom/google/r/b/a/px;
    .locals 1

    .prologue
    .line 1643
    sget-object v0, Lcom/google/r/b/a/px;->e:Lcom/google/r/b/a/px;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/pz;
    .locals 1

    .prologue
    .line 1395
    new-instance v0, Lcom/google/r/b/a/pz;

    invoke-direct {v0}, Lcom/google/r/b/a/pz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/px;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1226
    sget-object v0, Lcom/google/r/b/a/px;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1295
    invoke-virtual {p0}, Lcom/google/r/b/a/px;->c()I

    .line 1296
    iget v0, p0, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1297
    iget-object v0, p0, Lcom/google/r/b/a/px;->b:Lcom/google/n/f;

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1299
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1300
    iget-object v0, p0, Lcom/google/r/b/a/px;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1302
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1303
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1305
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/px;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1306
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1279
    iget-byte v0, p0, Lcom/google/r/b/a/px;->f:B

    .line 1280
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1290
    :goto_0
    return v0

    .line 1281
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1283
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1284
    iget-object v0, p0, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/qb;->d()Lcom/google/r/b/a/qb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/qb;

    invoke-virtual {v0}, Lcom/google/r/b/a/qb;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1285
    iput-byte v2, p0, Lcom/google/r/b/a/px;->f:B

    move v0, v2

    .line 1286
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1283
    goto :goto_1

    .line 1289
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/px;->f:B

    move v0, v1

    .line 1290
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1310
    iget v0, p0, Lcom/google/r/b/a/px;->g:I

    .line 1311
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1328
    :goto_0
    return v0

    .line 1314
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 1315
    iget-object v0, p0, Lcom/google/r/b/a/px;->b:Lcom/google/n/f;

    .line 1316
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1318
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1319
    iget-object v2, p0, Lcom/google/r/b/a/px;->c:Lcom/google/n/ao;

    .line 1320
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1322
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 1323
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    .line 1324
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1326
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/px;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1327
    iput v0, p0, Lcom/google/r/b/a/px;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1151
    invoke-static {}, Lcom/google/r/b/a/px;->newBuilder()Lcom/google/r/b/a/pz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/pz;->a(Lcom/google/r/b/a/px;)Lcom/google/r/b/a/pz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1151
    invoke-static {}, Lcom/google/r/b/a/px;->newBuilder()Lcom/google/r/b/a/pz;

    move-result-object v0

    return-object v0
.end method
