.class public final Lcom/google/r/b/a/kd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/kg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/kd;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/kd;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/google/r/b/a/ke;

    invoke-direct {v0}, Lcom/google/r/b/a/ke;-><init>()V

    sput-object v0, Lcom/google/r/b/a/kd;->PARSER:Lcom/google/n/ax;

    .line 196
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/kd;->g:Lcom/google/n/aw;

    .line 401
    new-instance v0, Lcom/google/r/b/a/kd;

    invoke-direct {v0}, Lcom/google/r/b/a/kd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/kd;->d:Lcom/google/r/b/a/kd;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 44
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 153
    iput-byte v0, p0, Lcom/google/r/b/a/kd;->e:B

    .line 175
    iput v0, p0, Lcom/google/r/b/a/kd;->f:I

    .line 45
    iput v1, p0, Lcom/google/r/b/a/kd;->b:I

    .line 46
    iput v1, p0, Lcom/google/r/b/a/kd;->c:I

    .line 47
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 53
    invoke-direct {p0}, Lcom/google/r/b/a/kd;-><init>()V

    .line 54
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 58
    const/4 v0, 0x0

    .line 59
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 60
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 61
    sparse-switch v3, :sswitch_data_0

    .line 66
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 68
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 64
    goto :goto_0

    .line 73
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 74
    invoke-static {v3}, Lcom/google/maps/g/if;->a(I)Lcom/google/maps/g/if;

    move-result-object v4

    .line 75
    if-nez v4, :cond_1

    .line 76
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/kd;->au:Lcom/google/n/bn;

    throw v0

    .line 78
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/kd;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/kd;->a:I

    .line 79
    iput v3, p0, Lcom/google/r/b/a/kd;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 98
    :catch_1
    move-exception v0

    .line 99
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 100
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 84
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 85
    invoke-static {v3}, Lcom/google/maps/g/if;->a(I)Lcom/google/maps/g/if;

    move-result-object v4

    .line 86
    if-nez v4, :cond_2

    .line 87
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 89
    :cond_2
    iget v4, p0, Lcom/google/r/b/a/kd;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/kd;->a:I

    .line 90
    iput v3, p0, Lcom/google/r/b/a/kd;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 102
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/kd;->au:Lcom/google/n/bn;

    .line 103
    return-void

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 42
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 153
    iput-byte v0, p0, Lcom/google/r/b/a/kd;->e:B

    .line 175
    iput v0, p0, Lcom/google/r/b/a/kd;->f:I

    .line 43
    return-void
.end method

.method public static d()Lcom/google/r/b/a/kd;
    .locals 1

    .prologue
    .line 404
    sget-object v0, Lcom/google/r/b/a/kd;->d:Lcom/google/r/b/a/kd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/kf;
    .locals 1

    .prologue
    .line 258
    new-instance v0, Lcom/google/r/b/a/kf;

    invoke-direct {v0}, Lcom/google/r/b/a/kf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/kd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    sget-object v0, Lcom/google/r/b/a/kd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 165
    invoke-virtual {p0}, Lcom/google/r/b/a/kd;->c()I

    .line 166
    iget v0, p0, Lcom/google/r/b/a/kd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 167
    iget v0, p0, Lcom/google/r/b/a/kd;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 169
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/kd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 170
    iget v0, p0, Lcom/google/r/b/a/kd;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/kd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 173
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 155
    iget-byte v1, p0, Lcom/google/r/b/a/kd;->e:B

    .line 156
    if-ne v1, v0, :cond_0

    .line 160
    :goto_0
    return v0

    .line 157
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 159
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/kd;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 177
    iget v0, p0, Lcom/google/r/b/a/kd;->f:I

    .line 178
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 191
    :goto_0
    return v0

    .line 181
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/kd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 182
    iget v0, p0, Lcom/google/r/b/a/kd;->b:I

    .line 183
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 185
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/kd;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 186
    iget v3, p0, Lcom/google/r/b/a/kd;->c:I

    .line 187
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 189
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/kd;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    iput v0, p0, Lcom/google/r/b/a/kd;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 183
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/r/b/a/kd;->newBuilder()Lcom/google/r/b/a/kf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/kf;->a(Lcom/google/r/b/a/kd;)Lcom/google/r/b/a/kf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/r/b/a/kd;->newBuilder()Lcom/google/r/b/a/kf;

    move-result-object v0

    return-object v0
.end method
