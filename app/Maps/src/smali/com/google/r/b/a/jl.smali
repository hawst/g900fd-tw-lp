.class public final enum Lcom/google/r/b/a/jl;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/jl;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/jl;

.field public static final enum b:Lcom/google/r/b/a/jl;

.field public static final enum c:Lcom/google/r/b/a/jl;

.field public static final enum d:Lcom/google/r/b/a/jl;

.field public static final enum e:Lcom/google/r/b/a/jl;

.field private static final synthetic g:[Lcom/google/r/b/a/jl;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/r/b/a/jl;

    const-string v1, "LOCATION_PROVIDER_STATE_UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/jl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/jl;->a:Lcom/google/r/b/a/jl;

    .line 18
    new-instance v0, Lcom/google/r/b/a/jl;

    const-string v1, "HARDWARE_MISSING"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/jl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/jl;->b:Lcom/google/r/b/a/jl;

    .line 22
    new-instance v0, Lcom/google/r/b/a/jl;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/jl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/jl;->c:Lcom/google/r/b/a/jl;

    .line 26
    new-instance v0, Lcom/google/r/b/a/jl;

    const-string v1, "DISABLED_BY_DEVICE_SETTING"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/jl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/jl;->d:Lcom/google/r/b/a/jl;

    .line 30
    new-instance v0, Lcom/google/r/b/a/jl;

    const-string v1, "DISABLED_BY_PERMISSION_SETTING"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/jl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/jl;->e:Lcom/google/r/b/a/jl;

    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/r/b/a/jl;

    sget-object v1, Lcom/google/r/b/a/jl;->a:Lcom/google/r/b/a/jl;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/jl;->b:Lcom/google/r/b/a/jl;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/jl;->c:Lcom/google/r/b/a/jl;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/jl;->d:Lcom/google/r/b/a/jl;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/jl;->e:Lcom/google/r/b/a/jl;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/r/b/a/jl;->g:[Lcom/google/r/b/a/jl;

    .line 75
    new-instance v0, Lcom/google/r/b/a/jm;

    invoke-direct {v0}, Lcom/google/r/b/a/jm;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 85
    iput p3, p0, Lcom/google/r/b/a/jl;->f:I

    .line 86
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/jl;
    .locals 1

    .prologue
    .line 60
    packed-switch p0, :pswitch_data_0

    .line 66
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 61
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/jl;->a:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 62
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/jl;->b:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 63
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/jl;->c:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 64
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/jl;->d:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 65
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/jl;->e:Lcom/google/r/b/a/jl;

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/jl;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/r/b/a/jl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/jl;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/jl;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/r/b/a/jl;->g:[Lcom/google/r/b/a/jl;

    invoke-virtual {v0}, [Lcom/google/r/b/a/jl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/jl;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/google/r/b/a/jl;->f:I

    return v0
.end method
