.class public final Lcom/google/r/b/a/jz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/kb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/jx;",
        "Lcom/google/r/b/a/jz;",
        ">;",
        "Lcom/google/r/b/a/kb;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:I

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 340
    sget-object v0, Lcom/google/r/b/a/jx;->e:Lcom/google/r/b/a/jx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 408
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/jz;->a:I

    .line 341
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 332
    new-instance v4, Lcom/google/r/b/a/jx;

    invoke-direct {v4, p0}, Lcom/google/r/b/a/jx;-><init>(Lcom/google/n/v;)V

    iget v5, p0, Lcom/google/r/b/a/jz;->c:I

    iget v0, p0, Lcom/google/r/b/a/jz;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/r/b/a/jz;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v6

    iget-object v1, p0, Lcom/google/r/b/a/jz;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_0
    iget v0, p0, Lcom/google/r/b/a/jz;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/r/b/a/jz;->b:Ljava/lang/Object;

    iput-object v0, v4, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    :cond_1
    and-int/lit8 v0, v5, 0x4

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/jz;->d:Z

    iput-boolean v1, v4, Lcom/google/r/b/a/jx;->d:Z

    iput v0, v4, Lcom/google/r/b/a/jx;->a:I

    iget v0, p0, Lcom/google/r/b/a/jz;->a:I

    iput v0, v4, Lcom/google/r/b/a/jx;->b:I

    return-object v4

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 332
    check-cast p1, Lcom/google/r/b/a/jx;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/jz;->a(Lcom/google/r/b/a/jx;)Lcom/google/r/b/a/jz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/jx;)Lcom/google/r/b/a/jz;
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x3

    const/4 v0, 0x0

    .line 378
    invoke-static {}, Lcom/google/r/b/a/jx;->d()Lcom/google/r/b/a/jx;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 401
    :goto_0
    return-object p0

    .line 379
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/jx;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1

    .line 380
    iget-boolean v1, p1, Lcom/google/r/b/a/jx;->d:Z

    iget v2, p0, Lcom/google/r/b/a/jz;->c:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/jz;->c:I

    iput-boolean v1, p0, Lcom/google/r/b/a/jz;->d:Z

    .line 382
    :cond_1
    sget-object v1, Lcom/google/r/b/a/jw;->a:[I

    iget v2, p1, Lcom/google/r/b/a/jx;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/ka;->a(I)Lcom/google/r/b/a/ka;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/r/b/a/ka;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 394
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/jx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_2
    move v1, v0

    .line 379
    goto :goto_1

    .line 384
    :pswitch_0
    iget v0, p0, Lcom/google/r/b/a/jz;->a:I

    if-eq v0, v3, :cond_3

    .line 385
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/jz;->b:Ljava/lang/Object;

    .line 387
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/jz;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 388
    iget-object v1, p1, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 387
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 389
    iput v3, p0, Lcom/google/r/b/a/jz;->a:I

    goto :goto_2

    .line 393
    :pswitch_1
    iget v1, p1, Lcom/google/r/b/a/jx;->b:I

    if-ne v1, v4, :cond_4

    iget-object v0, p1, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :cond_4
    iput v4, p0, Lcom/google/r/b/a/jz;->a:I

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/jz;->b:Ljava/lang/Object;

    goto :goto_2

    .line 382
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x1

    return v0
.end method
