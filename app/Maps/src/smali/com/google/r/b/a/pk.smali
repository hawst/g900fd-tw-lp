.class public final Lcom/google/r/b/a/pk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/pn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/pk;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/pk;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field f:Z

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/r/b/a/pl;

    invoke-direct {v0}, Lcom/google/r/b/a/pl;-><init>()V

    sput-object v0, Lcom/google/r/b/a/pk;->PARSER:Lcom/google/n/ax;

    .line 239
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/pk;->j:Lcom/google/n/aw;

    .line 559
    new-instance v0, Lcom/google/r/b/a/pk;

    invoke-direct {v0}, Lcom/google/r/b/a/pk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/pk;->g:Lcom/google/r/b/a/pk;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x7d0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 175
    iput-byte v0, p0, Lcom/google/r/b/a/pk;->h:B

    .line 206
    iput v0, p0, Lcom/google/r/b/a/pk;->i:I

    .line 18
    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/r/b/a/pk;->b:I

    .line 19
    iput v1, p0, Lcom/google/r/b/a/pk;->c:I

    .line 20
    iput v1, p0, Lcom/google/r/b/a/pk;->d:I

    .line 21
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/r/b/a/pk;->e:I

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/pk;->f:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 29
    invoke-direct {p0}, Lcom/google/r/b/a/pk;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 35
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 37
    sparse-switch v0, :sswitch_data_0

    .line 42
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/pk;->a:I

    .line 50
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/pk;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/pk;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/pk;->a:I

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/pk;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/pk;->a:I

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/pk;->d:I

    goto :goto_0

    .line 64
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/pk;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/pk;->e:I

    goto :goto_0

    .line 69
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/pk;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/pk;->f:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 81
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/pk;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 175
    iput-byte v0, p0, Lcom/google/r/b/a/pk;->h:B

    .line 206
    iput v0, p0, Lcom/google/r/b/a/pk;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/pk;
    .locals 1

    .prologue
    .line 562
    sget-object v0, Lcom/google/r/b/a/pk;->g:Lcom/google/r/b/a/pk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/pm;
    .locals 1

    .prologue
    .line 301
    new-instance v0, Lcom/google/r/b/a/pm;

    invoke-direct {v0}, Lcom/google/r/b/a/pm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/pk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/r/b/a/pk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 187
    invoke-virtual {p0}, Lcom/google/r/b/a/pk;->c()I

    .line 188
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 189
    iget v0, p0, Lcom/google/r/b/a/pk;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 191
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 192
    iget v0, p0, Lcom/google/r/b/a/pk;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 194
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 195
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/pk;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 197
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 198
    iget v0, p0, Lcom/google/r/b/a/pk;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 200
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 201
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/pk;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 203
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/pk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 204
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 177
    iget-byte v1, p0, Lcom/google/r/b/a/pk;->h:B

    .line 178
    if-ne v1, v0, :cond_0

    .line 182
    :goto_0
    return v0

    .line 179
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 181
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/pk;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 208
    iget v0, p0, Lcom/google/r/b/a/pk;->i:I

    .line 209
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 234
    :goto_0
    return v0

    .line 212
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 213
    iget v0, p0, Lcom/google/r/b/a/pk;->b:I

    .line 214
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 216
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 217
    iget v3, p0, Lcom/google/r/b/a/pk;->c:I

    .line 218
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 220
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 221
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/pk;->d:I

    .line 222
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 224
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_4

    .line 225
    iget v3, p0, Lcom/google/r/b/a/pk;->e:I

    .line 226
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 228
    :cond_4
    iget v1, p0, Lcom/google/r/b/a/pk;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_5

    .line 229
    const/4 v1, 0x5

    iget-boolean v3, p0, Lcom/google/r/b/a/pk;->f:Z

    .line 230
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 232
    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/pk;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    iput v0, p0, Lcom/google/r/b/a/pk;->i:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 214
    goto :goto_1

    :cond_7
    move v3, v1

    .line 218
    goto :goto_3

    :cond_8
    move v3, v1

    .line 222
    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/pk;->newBuilder()Lcom/google/r/b/a/pm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/pm;->a(Lcom/google/r/b/a/pk;)Lcom/google/r/b/a/pm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/pk;->newBuilder()Lcom/google/r/b/a/pm;

    move-result-object v0

    return-object v0
.end method
