.class public final Lcom/google/r/b/a/zq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/zq;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/zq;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field public g:Ljava/lang/Object;

.field public h:Ljava/lang/Object;

.field public i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 310
    new-instance v0, Lcom/google/r/b/a/zr;

    invoke-direct {v0}, Lcom/google/r/b/a/zr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/zq;->PARSER:Lcom/google/n/ax;

    .line 788
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/zq;->m:Lcom/google/n/aw;

    .line 1557
    new-instance v0, Lcom/google/r/b/a/zq;

    invoke-direct {v0}, Lcom/google/r/b/a/zq;-><init>()V

    sput-object v0, Lcom/google/r/b/a/zq;->j:Lcom/google/r/b/a/zq;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 212
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 703
    iput-byte v0, p0, Lcom/google/r/b/a/zq;->k:B

    .line 743
    iput v0, p0, Lcom/google/r/b/a/zq;->l:I

    .line 213
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    .line 214
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zq;->c:Ljava/lang/Object;

    .line 215
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zq;->d:Ljava/lang/Object;

    .line 216
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zq;->e:Ljava/lang/Object;

    .line 217
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    .line 218
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    .line 219
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    .line 220
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/zq;->i:I

    .line 221
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 227
    invoke-direct {p0}, Lcom/google/r/b/a/zq;-><init>()V

    .line 228
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 232
    const/4 v0, 0x0

    .line 233
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 234
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 235
    sparse-switch v3, :sswitch_data_0

    .line 240
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 242
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 238
    goto :goto_0

    .line 247
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 248
    iget v4, p0, Lcom/google/r/b/a/zq;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/zq;->a:I

    .line 249
    iput-object v3, p0, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 301
    :catch_0
    move-exception v0

    .line 302
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 307
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/zq;->au:Lcom/google/n/bn;

    throw v0

    .line 253
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 254
    iget v4, p0, Lcom/google/r/b/a/zq;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/zq;->a:I

    .line 255
    iput-object v3, p0, Lcom/google/r/b/a/zq;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 303
    :catch_1
    move-exception v0

    .line 304
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 305
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 259
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 260
    iget v4, p0, Lcom/google/r/b/a/zq;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/zq;->a:I

    .line 261
    iput-object v3, p0, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    goto :goto_0

    .line 265
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 266
    invoke-static {v3}, Lcom/google/r/b/a/zt;->a(I)Lcom/google/r/b/a/zt;

    move-result-object v4

    .line 267
    if-nez v4, :cond_1

    .line 268
    const/4 v4, 0x4

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 270
    :cond_1
    iget v4, p0, Lcom/google/r/b/a/zq;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/r/b/a/zq;->a:I

    .line 271
    iput v3, p0, Lcom/google/r/b/a/zq;->i:I

    goto :goto_0

    .line 276
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 277
    iget v4, p0, Lcom/google/r/b/a/zq;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/zq;->a:I

    .line 278
    iput-object v3, p0, Lcom/google/r/b/a/zq;->d:Ljava/lang/Object;

    goto :goto_0

    .line 282
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 283
    iget v4, p0, Lcom/google/r/b/a/zq;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/zq;->a:I

    .line 284
    iput-object v3, p0, Lcom/google/r/b/a/zq;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 288
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 289
    iget v4, p0, Lcom/google/r/b/a/zq;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/zq;->a:I

    .line 290
    iput-object v3, p0, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 294
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 295
    iget v4, p0, Lcom/google/r/b/a/zq;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/zq;->a:I

    .line 296
    iput-object v3, p0, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 307
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->au:Lcom/google/n/bn;

    .line 308
    return-void

    .line 235
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 210
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 703
    iput-byte v0, p0, Lcom/google/r/b/a/zq;->k:B

    .line 743
    iput v0, p0, Lcom/google/r/b/a/zq;->l:I

    .line 211
    return-void
.end method

.method public static d()Lcom/google/r/b/a/zq;
    .locals 1

    .prologue
    .line 1560
    sget-object v0, Lcom/google/r/b/a/zq;->j:Lcom/google/r/b/a/zq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/zs;
    .locals 1

    .prologue
    .line 850
    new-instance v0, Lcom/google/r/b/a/zs;

    invoke-direct {v0}, Lcom/google/r/b/a/zs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/zq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    sget-object v0, Lcom/google/r/b/a/zq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 715
    invoke-virtual {p0}, Lcom/google/r/b/a/zq;->c()I

    .line 716
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 717
    iget-object v0, p0, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 719
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 720
    iget-object v0, p0, Lcom/google/r/b/a/zq;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 722
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 723
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 725
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_3

    .line 726
    iget v0, p0, Lcom/google/r/b/a/zq;->i:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(II)V

    .line 728
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_4

    .line 729
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/zq;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->d:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 731
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_5

    .line 732
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/zq;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->e:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 734
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 735
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 737
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 738
    iget-object v0, p0, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    :goto_6
    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 740
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/zq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 741
    return-void

    .line 717
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 720
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 723
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 729
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 732
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 735
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 738
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_6
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 705
    iget-byte v1, p0, Lcom/google/r/b/a/zq;->k:B

    .line 706
    if-ne v1, v0, :cond_0

    .line 710
    :goto_0
    return v0

    .line 707
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 709
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/zq;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 745
    iget v0, p0, Lcom/google/r/b/a/zq;->l:I

    .line 746
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 783
    :goto_0
    return v0

    .line 749
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_10

    .line 751
    iget-object v0, p0, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 753
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 755
    iget-object v0, p0, Lcom/google/r/b/a/zq;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 757
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    .line 758
    const/4 v3, 0x3

    .line 759
    iget-object v0, p0, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 761
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_3

    .line 762
    iget v0, p0, Lcom/google/r/b/a/zq;->i:I

    .line 763
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_b

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 765
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 766
    const/4 v3, 0x5

    .line 767
    iget-object v0, p0, Lcom/google/r/b/a/zq;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->d:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 769
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_5

    .line 770
    const/4 v3, 0x6

    .line 771
    iget-object v0, p0, Lcom/google/r/b/a/zq;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->e:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 773
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_6

    .line 774
    const/4 v3, 0x7

    .line 775
    iget-object v0, p0, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 777
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_7

    .line 779
    iget-object v0, p0, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    :goto_9
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 781
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/zq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 782
    iput v0, p0, Lcom/google/r/b/a/zq;->l:I

    goto/16 :goto_0

    .line 751
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 755
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 759
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 763
    :cond_b
    const/16 v0, 0xa

    goto/16 :goto_5

    .line 767
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 771
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 775
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 779
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    :cond_10
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 204
    invoke-static {}, Lcom/google/r/b/a/zq;->newBuilder()Lcom/google/r/b/a/zs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/zs;->a(Lcom/google/r/b/a/zq;)Lcom/google/r/b/a/zs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 204
    invoke-static {}, Lcom/google/r/b/a/zq;->newBuilder()Lcom/google/r/b/a/zs;

    move-result-object v0

    return-object v0
.end method
