.class public final Lcom/google/r/b/a/v;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/y;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/v;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/v;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/f;

.field public c:I

.field public d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1582
    new-instance v0, Lcom/google/r/b/a/w;

    invoke-direct {v0}, Lcom/google/r/b/a/w;-><init>()V

    sput-object v0, Lcom/google/r/b/a/v;->PARSER:Lcom/google/n/ax;

    .line 1720
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/v;->h:Lcom/google/n/aw;

    .line 2007
    new-instance v0, Lcom/google/r/b/a/v;

    invoke-direct {v0}, Lcom/google/r/b/a/v;-><init>()V

    sput-object v0, Lcom/google/r/b/a/v;->e:Lcom/google/r/b/a/v;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1526
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1670
    iput-byte v0, p0, Lcom/google/r/b/a/v;->f:B

    .line 1695
    iput v0, p0, Lcom/google/r/b/a/v;->g:I

    .line 1527
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/v;->b:Lcom/google/n/f;

    .line 1528
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/v;->c:I

    .line 1529
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    .line 1530
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1536
    invoke-direct {p0}, Lcom/google/r/b/a/v;-><init>()V

    .line 1537
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1541
    const/4 v0, 0x0

    .line 1542
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1543
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1544
    sparse-switch v3, :sswitch_data_0

    .line 1549
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1551
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1547
    goto :goto_0

    .line 1556
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/v;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/v;->a:I

    .line 1557
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/r/b/a/v;->b:Lcom/google/n/f;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1573
    :catch_0
    move-exception v0

    .line 1574
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1579
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/v;->au:Lcom/google/n/bn;

    throw v0

    .line 1561
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/v;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/v;->a:I

    .line 1562
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/v;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1575
    :catch_1
    move-exception v0

    .line 1576
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1577
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1566
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1567
    iget v4, p0, Lcom/google/r/b/a/v;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/v;->a:I

    .line 1568
    iput-object v3, p0, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1579
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/v;->au:Lcom/google/n/bn;

    .line 1580
    return-void

    .line 1544
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1524
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1670
    iput-byte v0, p0, Lcom/google/r/b/a/v;->f:B

    .line 1695
    iput v0, p0, Lcom/google/r/b/a/v;->g:I

    .line 1525
    return-void
.end method

.method public static d()Lcom/google/r/b/a/v;
    .locals 1

    .prologue
    .line 2010
    sget-object v0, Lcom/google/r/b/a/v;->e:Lcom/google/r/b/a/v;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/x;
    .locals 1

    .prologue
    .line 1782
    new-instance v0, Lcom/google/r/b/a/x;

    invoke-direct {v0}, Lcom/google/r/b/a/x;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/v;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1594
    sget-object v0, Lcom/google/r/b/a/v;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1682
    invoke-virtual {p0}, Lcom/google/r/b/a/v;->c()I

    .line 1683
    iget v0, p0, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1684
    iget-object v0, p0, Lcom/google/r/b/a/v;->b:Lcom/google/n/f;

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1686
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1687
    iget v0, p0, Lcom/google/r/b/a/v;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 1689
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1690
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1692
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/v;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1693
    return-void

    .line 1690
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1672
    iget-byte v1, p0, Lcom/google/r/b/a/v;->f:B

    .line 1673
    if-ne v1, v0, :cond_0

    .line 1677
    :goto_0
    return v0

    .line 1674
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1676
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/v;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1697
    iget v0, p0, Lcom/google/r/b/a/v;->g:I

    .line 1698
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1715
    :goto_0
    return v0

    .line 1701
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 1702
    iget-object v0, p0, Lcom/google/r/b/a/v;->b:Lcom/google/n/f;

    .line 1703
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1705
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_4

    .line 1706
    iget v2, p0, Lcom/google/r/b/a/v;->c:I

    .line 1707
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 1709
    :goto_3
    iget v0, p0, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 1710
    const/4 v3, 0x3

    .line 1711
    iget-object v0, p0, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1713
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/v;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1714
    iput v0, p0, Lcom/google/r/b/a/v;->g:I

    goto :goto_0

    .line 1707
    :cond_2
    const/16 v2, 0xa

    goto :goto_2

    .line 1711
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_4
    move v2, v0

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1518
    invoke-static {}, Lcom/google/r/b/a/v;->newBuilder()Lcom/google/r/b/a/x;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/x;->a(Lcom/google/r/b/a/v;)Lcom/google/r/b/a/x;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1518
    invoke-static {}, Lcom/google/r/b/a/v;->newBuilder()Lcom/google/r/b/a/x;

    move-result-object v0

    return-object v0
.end method
