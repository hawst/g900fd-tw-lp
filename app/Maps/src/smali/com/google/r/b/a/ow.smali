.class public final Lcom/google/r/b/a/ow;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/oz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ou;",
        "Lcom/google/r/b/a/ow;",
        ">;",
        "Lcom/google/r/b/a/oz;"
    }
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:Z

.field private H:I

.field private J:I

.field private K:I

.field private L:Lcom/google/n/ao;

.field private M:Z

.field private N:I

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:I

.field private S:I

.field private T:I

.field private U:I

.field private V:I

.field private W:I

.field private X:I

.field private Y:I

.field private Z:I

.field private a:I

.field private aa:I

.field private ab:I

.field private ac:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:F

.field private m:F

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x46

    const/16 v4, 0x32

    const/4 v3, 0x5

    const/4 v2, 0x3

    const/16 v1, 0xa

    .line 1748
    sget-object v0, Lcom/google/r/b/a/ou;->ac:Lcom/google/r/b/a/ou;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2262
    iput v5, p0, Lcom/google/r/b/a/ow;->c:I

    .line 2294
    const v0, 0x9eb10

    iput v0, p0, Lcom/google/r/b/a/ow;->d:I

    .line 2326
    iput v1, p0, Lcom/google/r/b/a/ow;->e:I

    .line 2358
    const/16 v0, 0x78

    iput v0, p0, Lcom/google/r/b/a/ow;->f:I

    .line 2390
    const/16 v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/ow;->g:I

    .line 2422
    const/16 v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/ow;->h:I

    .line 2454
    iput v3, p0, Lcom/google/r/b/a/ow;->i:I

    .line 2486
    iput v1, p0, Lcom/google/r/b/a/ow;->j:I

    .line 2518
    const v0, 0xf4240

    iput v0, p0, Lcom/google/r/b/a/ow;->k:I

    .line 2550
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/google/r/b/a/ow;->l:F

    .line 2582
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/google/r/b/a/ow;->m:F

    .line 2614
    const v0, 0xf4240

    iput v0, p0, Lcom/google/r/b/a/ow;->n:I

    .line 2646
    const/16 v0, 0xc8

    iput v0, p0, Lcom/google/r/b/a/ow;->o:I

    .line 2678
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/r/b/a/ow;->p:I

    .line 2710
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ow;->q:I

    .line 2746
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/ow;->r:I

    .line 2778
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/r/b/a/ow;->s:I

    .line 2810
    iput v3, p0, Lcom/google/r/b/a/ow;->t:I

    .line 2842
    const/16 v0, 0x60

    iput v0, p0, Lcom/google/r/b/a/ow;->u:I

    .line 2874
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/r/b/a/ow;->v:I

    .line 2906
    const v0, 0x1e8480

    iput v0, p0, Lcom/google/r/b/a/ow;->w:I

    .line 2938
    const/16 v0, 0x12c

    iput v0, p0, Lcom/google/r/b/a/ow;->x:I

    .line 2970
    const/16 v0, 0x384

    iput v0, p0, Lcom/google/r/b/a/ow;->y:I

    .line 3002
    iput v4, p0, Lcom/google/r/b/a/ow;->z:I

    .line 3034
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/ow;->A:I

    .line 3066
    const/16 v0, 0x190

    iput v0, p0, Lcom/google/r/b/a/ow;->B:I

    .line 3098
    iput v2, p0, Lcom/google/r/b/a/ow;->C:I

    .line 3130
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/r/b/a/ow;->D:I

    .line 3162
    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/r/b/a/ow;->E:I

    .line 3194
    const/16 v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/ow;->F:I

    .line 3226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/ow;->G:Z

    .line 3258
    iput v2, p0, Lcom/google/r/b/a/ow;->H:I

    .line 3322
    iput v4, p0, Lcom/google/r/b/a/ow;->K:I

    .line 3354
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ow;->L:Lcom/google/n/ao;

    .line 3445
    const/16 v0, 0x23

    iput v0, p0, Lcom/google/r/b/a/ow;->N:I

    .line 3573
    const/16 v0, 0x3c

    iput v0, p0, Lcom/google/r/b/a/ow;->R:I

    .line 3605
    const/16 v0, 0x78

    iput v0, p0, Lcom/google/r/b/a/ow;->S:I

    .line 3637
    const/16 v0, 0x258

    iput v0, p0, Lcom/google/r/b/a/ow;->T:I

    .line 3669
    const/16 v0, 0xf0

    iput v0, p0, Lcom/google/r/b/a/ow;->U:I

    .line 3701
    iput v1, p0, Lcom/google/r/b/a/ow;->V:I

    .line 3733
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/r/b/a/ow;->W:I

    .line 3765
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/r/b/a/ow;->X:I

    .line 3797
    const/16 v0, 0x3840

    iput v0, p0, Lcom/google/r/b/a/ow;->Y:I

    .line 3829
    iput v5, p0, Lcom/google/r/b/a/ow;->Z:I

    .line 3861
    const/16 v0, 0x384

    iput v0, p0, Lcom/google/r/b/a/ow;->aa:I

    .line 3893
    const/16 v0, 0xe10

    iput v0, p0, Lcom/google/r/b/a/ow;->ab:I

    .line 3926
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    .line 1749
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 12

    .prologue
    const/high16 v11, 0x80000

    const/high16 v10, 0x40000

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    .line 1740
    new-instance v2, Lcom/google/r/b/a/ou;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ou;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    iget v4, p0, Lcom/google/r/b/a/ow;->b:I

    const/4 v0, 0x0

    const/4 v1, 0x0

    and-int/lit8 v5, v3, 0x1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget v5, p0, Lcom/google/r/b/a/ow;->c:I

    iput v5, v2, Lcom/google/r/b/a/ou;->c:I

    and-int/lit8 v5, v3, 0x2

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v5, p0, Lcom/google/r/b/a/ow;->d:I

    iput v5, v2, Lcom/google/r/b/a/ou;->d:I

    and-int/lit8 v5, v3, 0x4

    const/4 v6, 0x4

    if-ne v5, v6, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v5, p0, Lcom/google/r/b/a/ow;->e:I

    iput v5, v2, Lcom/google/r/b/a/ou;->e:I

    and-int/lit8 v5, v3, 0x8

    const/16 v6, 0x8

    if-ne v5, v6, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget v5, p0, Lcom/google/r/b/a/ow;->f:I

    iput v5, v2, Lcom/google/r/b/a/ou;->f:I

    and-int/lit8 v5, v3, 0x10

    const/16 v6, 0x10

    if-ne v5, v6, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget v5, p0, Lcom/google/r/b/a/ow;->g:I

    iput v5, v2, Lcom/google/r/b/a/ou;->g:I

    and-int/lit8 v5, v3, 0x20

    const/16 v6, 0x20

    if-ne v5, v6, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget v5, p0, Lcom/google/r/b/a/ow;->h:I

    iput v5, v2, Lcom/google/r/b/a/ou;->h:I

    and-int/lit8 v5, v3, 0x40

    const/16 v6, 0x40

    if-ne v5, v6, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget v5, p0, Lcom/google/r/b/a/ow;->i:I

    iput v5, v2, Lcom/google/r/b/a/ou;->i:I

    and-int/lit16 v5, v3, 0x80

    const/16 v6, 0x80

    if-ne v5, v6, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget v5, p0, Lcom/google/r/b/a/ow;->j:I

    iput v5, v2, Lcom/google/r/b/a/ou;->j:I

    and-int/lit16 v5, v3, 0x100

    const/16 v6, 0x100

    if-ne v5, v6, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget v5, p0, Lcom/google/r/b/a/ow;->k:I

    iput v5, v2, Lcom/google/r/b/a/ou;->k:I

    and-int/lit16 v5, v3, 0x200

    const/16 v6, 0x200

    if-ne v5, v6, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget v5, p0, Lcom/google/r/b/a/ow;->l:F

    iput v5, v2, Lcom/google/r/b/a/ou;->l:F

    and-int/lit16 v5, v3, 0x400

    const/16 v6, 0x400

    if-ne v5, v6, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget v5, p0, Lcom/google/r/b/a/ow;->m:F

    iput v5, v2, Lcom/google/r/b/a/ou;->m:F

    and-int/lit16 v5, v3, 0x800

    const/16 v6, 0x800

    if-ne v5, v6, :cond_b

    or-int/lit16 v0, v0, 0x800

    :cond_b
    iget v5, p0, Lcom/google/r/b/a/ow;->n:I

    iput v5, v2, Lcom/google/r/b/a/ou;->n:I

    and-int/lit16 v5, v3, 0x1000

    const/16 v6, 0x1000

    if-ne v5, v6, :cond_c

    or-int/lit16 v0, v0, 0x1000

    :cond_c
    iget v5, p0, Lcom/google/r/b/a/ow;->o:I

    iput v5, v2, Lcom/google/r/b/a/ou;->o:I

    and-int/lit16 v5, v3, 0x2000

    const/16 v6, 0x2000

    if-ne v5, v6, :cond_d

    or-int/lit16 v0, v0, 0x2000

    :cond_d
    iget v5, p0, Lcom/google/r/b/a/ow;->p:I

    iput v5, v2, Lcom/google/r/b/a/ou;->p:I

    and-int/lit16 v5, v3, 0x4000

    const/16 v6, 0x4000

    if-ne v5, v6, :cond_e

    or-int/lit16 v0, v0, 0x4000

    :cond_e
    iget v5, p0, Lcom/google/r/b/a/ow;->q:I

    iput v5, v2, Lcom/google/r/b/a/ou;->q:I

    and-int v5, v3, v7

    if-ne v5, v7, :cond_f

    or-int/2addr v0, v7

    :cond_f
    iget v5, p0, Lcom/google/r/b/a/ow;->r:I

    iput v5, v2, Lcom/google/r/b/a/ou;->r:I

    and-int v5, v3, v8

    if-ne v5, v8, :cond_10

    or-int/2addr v0, v8

    :cond_10
    iget v5, p0, Lcom/google/r/b/a/ow;->s:I

    iput v5, v2, Lcom/google/r/b/a/ou;->s:I

    and-int v5, v3, v9

    if-ne v5, v9, :cond_11

    or-int/2addr v0, v9

    :cond_11
    iget v5, p0, Lcom/google/r/b/a/ow;->t:I

    iput v5, v2, Lcom/google/r/b/a/ou;->t:I

    and-int v5, v3, v10

    if-ne v5, v10, :cond_12

    or-int/2addr v0, v10

    :cond_12
    iget v5, p0, Lcom/google/r/b/a/ow;->u:I

    iput v5, v2, Lcom/google/r/b/a/ou;->u:I

    and-int v5, v3, v11

    if-ne v5, v11, :cond_13

    or-int/2addr v0, v11

    :cond_13
    iget v5, p0, Lcom/google/r/b/a/ow;->v:I

    iput v5, v2, Lcom/google/r/b/a/ou;->v:I

    const/high16 v5, 0x100000

    and-int/2addr v5, v3

    const/high16 v6, 0x100000

    if-ne v5, v6, :cond_14

    const/high16 v5, 0x100000

    or-int/2addr v0, v5

    :cond_14
    iget v5, p0, Lcom/google/r/b/a/ow;->w:I

    iput v5, v2, Lcom/google/r/b/a/ou;->w:I

    const/high16 v5, 0x200000

    and-int/2addr v5, v3

    const/high16 v6, 0x200000

    if-ne v5, v6, :cond_15

    const/high16 v5, 0x200000

    or-int/2addr v0, v5

    :cond_15
    iget v5, p0, Lcom/google/r/b/a/ow;->x:I

    iput v5, v2, Lcom/google/r/b/a/ou;->x:I

    const/high16 v5, 0x400000

    and-int/2addr v5, v3

    const/high16 v6, 0x400000

    if-ne v5, v6, :cond_16

    const/high16 v5, 0x400000

    or-int/2addr v0, v5

    :cond_16
    iget v5, p0, Lcom/google/r/b/a/ow;->y:I

    iput v5, v2, Lcom/google/r/b/a/ou;->y:I

    const/high16 v5, 0x800000

    and-int/2addr v5, v3

    const/high16 v6, 0x800000

    if-ne v5, v6, :cond_17

    const/high16 v5, 0x800000

    or-int/2addr v0, v5

    :cond_17
    iget v5, p0, Lcom/google/r/b/a/ow;->z:I

    iput v5, v2, Lcom/google/r/b/a/ou;->z:I

    const/high16 v5, 0x1000000

    and-int/2addr v5, v3

    const/high16 v6, 0x1000000

    if-ne v5, v6, :cond_18

    const/high16 v5, 0x1000000

    or-int/2addr v0, v5

    :cond_18
    iget v5, p0, Lcom/google/r/b/a/ow;->A:I

    iput v5, v2, Lcom/google/r/b/a/ou;->A:I

    const/high16 v5, 0x2000000

    and-int/2addr v5, v3

    const/high16 v6, 0x2000000

    if-ne v5, v6, :cond_19

    const/high16 v5, 0x2000000

    or-int/2addr v0, v5

    :cond_19
    iget v5, p0, Lcom/google/r/b/a/ow;->B:I

    iput v5, v2, Lcom/google/r/b/a/ou;->B:I

    const/high16 v5, 0x4000000

    and-int/2addr v5, v3

    const/high16 v6, 0x4000000

    if-ne v5, v6, :cond_1a

    const/high16 v5, 0x4000000

    or-int/2addr v0, v5

    :cond_1a
    iget v5, p0, Lcom/google/r/b/a/ow;->C:I

    iput v5, v2, Lcom/google/r/b/a/ou;->C:I

    const/high16 v5, 0x8000000

    and-int/2addr v5, v3

    const/high16 v6, 0x8000000

    if-ne v5, v6, :cond_1b

    const/high16 v5, 0x8000000

    or-int/2addr v0, v5

    :cond_1b
    iget v5, p0, Lcom/google/r/b/a/ow;->D:I

    iput v5, v2, Lcom/google/r/b/a/ou;->D:I

    const/high16 v5, 0x10000000

    and-int/2addr v5, v3

    const/high16 v6, 0x10000000

    if-ne v5, v6, :cond_1c

    const/high16 v5, 0x10000000

    or-int/2addr v0, v5

    :cond_1c
    iget v5, p0, Lcom/google/r/b/a/ow;->E:I

    iput v5, v2, Lcom/google/r/b/a/ou;->E:I

    const/high16 v5, 0x20000000

    and-int/2addr v5, v3

    const/high16 v6, 0x20000000

    if-ne v5, v6, :cond_1d

    const/high16 v5, 0x20000000

    or-int/2addr v0, v5

    :cond_1d
    iget v5, p0, Lcom/google/r/b/a/ow;->F:I

    iput v5, v2, Lcom/google/r/b/a/ou;->F:I

    const/high16 v5, 0x40000000    # 2.0f

    and-int/2addr v5, v3

    const/high16 v6, 0x40000000    # 2.0f

    if-ne v5, v6, :cond_1e

    const/high16 v5, 0x40000000    # 2.0f

    or-int/2addr v0, v5

    :cond_1e
    iget-boolean v5, p0, Lcom/google/r/b/a/ow;->G:Z

    iput-boolean v5, v2, Lcom/google/r/b/a/ou;->G:Z

    const/high16 v5, -0x80000000

    and-int/2addr v3, v5

    const/high16 v5, -0x80000000

    if-ne v3, v5, :cond_1f

    const/high16 v3, -0x80000000

    or-int/2addr v0, v3

    :cond_1f
    iget v3, p0, Lcom/google/r/b/a/ow;->H:I

    iput v3, v2, Lcom/google/r/b/a/ou;->H:I

    and-int/lit8 v3, v4, 0x1

    const/4 v5, 0x1

    if-ne v3, v5, :cond_20

    const/4 v1, 0x1

    :cond_20
    iget v3, p0, Lcom/google/r/b/a/ow;->J:I

    iput v3, v2, Lcom/google/r/b/a/ou;->I:I

    and-int/lit8 v3, v4, 0x2

    const/4 v5, 0x2

    if-ne v3, v5, :cond_21

    or-int/lit8 v1, v1, 0x2

    :cond_21
    iget v3, p0, Lcom/google/r/b/a/ow;->K:I

    iput v3, v2, Lcom/google/r/b/a/ou;->J:I

    and-int/lit8 v3, v4, 0x4

    const/4 v5, 0x4

    if-ne v3, v5, :cond_22

    or-int/lit8 v1, v1, 0x4

    :cond_22
    iget-object v3, v2, Lcom/google/r/b/a/ou;->K:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ow;->L:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ow;->L:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v3, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v4, 0x8

    const/16 v5, 0x8

    if-ne v3, v5, :cond_23

    or-int/lit8 v1, v1, 0x8

    :cond_23
    iget-boolean v3, p0, Lcom/google/r/b/a/ow;->M:Z

    iput-boolean v3, v2, Lcom/google/r/b/a/ou;->L:Z

    and-int/lit8 v3, v4, 0x10

    const/16 v5, 0x10

    if-ne v3, v5, :cond_24

    or-int/lit8 v1, v1, 0x10

    :cond_24
    iget v3, p0, Lcom/google/r/b/a/ow;->N:I

    iput v3, v2, Lcom/google/r/b/a/ou;->M:I

    and-int/lit8 v3, v4, 0x20

    const/16 v5, 0x20

    if-ne v3, v5, :cond_25

    or-int/lit8 v1, v1, 0x20

    :cond_25
    iget-boolean v3, p0, Lcom/google/r/b/a/ow;->O:Z

    iput-boolean v3, v2, Lcom/google/r/b/a/ou;->N:Z

    and-int/lit8 v3, v4, 0x40

    const/16 v5, 0x40

    if-ne v3, v5, :cond_26

    or-int/lit8 v1, v1, 0x40

    :cond_26
    iget-boolean v3, p0, Lcom/google/r/b/a/ow;->P:Z

    iput-boolean v3, v2, Lcom/google/r/b/a/ou;->O:Z

    and-int/lit16 v3, v4, 0x80

    const/16 v5, 0x80

    if-ne v3, v5, :cond_27

    or-int/lit16 v1, v1, 0x80

    :cond_27
    iget-boolean v3, p0, Lcom/google/r/b/a/ow;->Q:Z

    iput-boolean v3, v2, Lcom/google/r/b/a/ou;->P:Z

    and-int/lit16 v3, v4, 0x100

    const/16 v5, 0x100

    if-ne v3, v5, :cond_28

    or-int/lit16 v1, v1, 0x100

    :cond_28
    iget v3, p0, Lcom/google/r/b/a/ow;->R:I

    iput v3, v2, Lcom/google/r/b/a/ou;->Q:I

    and-int/lit16 v3, v4, 0x200

    const/16 v5, 0x200

    if-ne v3, v5, :cond_29

    or-int/lit16 v1, v1, 0x200

    :cond_29
    iget v3, p0, Lcom/google/r/b/a/ow;->S:I

    iput v3, v2, Lcom/google/r/b/a/ou;->R:I

    and-int/lit16 v3, v4, 0x400

    const/16 v5, 0x400

    if-ne v3, v5, :cond_2a

    or-int/lit16 v1, v1, 0x400

    :cond_2a
    iget v3, p0, Lcom/google/r/b/a/ow;->T:I

    iput v3, v2, Lcom/google/r/b/a/ou;->S:I

    and-int/lit16 v3, v4, 0x800

    const/16 v5, 0x800

    if-ne v3, v5, :cond_2b

    or-int/lit16 v1, v1, 0x800

    :cond_2b
    iget v3, p0, Lcom/google/r/b/a/ow;->U:I

    iput v3, v2, Lcom/google/r/b/a/ou;->T:I

    and-int/lit16 v3, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v3, v5, :cond_2c

    or-int/lit16 v1, v1, 0x1000

    :cond_2c
    iget v3, p0, Lcom/google/r/b/a/ow;->V:I

    iput v3, v2, Lcom/google/r/b/a/ou;->U:I

    and-int/lit16 v3, v4, 0x2000

    const/16 v5, 0x2000

    if-ne v3, v5, :cond_2d

    or-int/lit16 v1, v1, 0x2000

    :cond_2d
    iget v3, p0, Lcom/google/r/b/a/ow;->W:I

    iput v3, v2, Lcom/google/r/b/a/ou;->V:I

    and-int/lit16 v3, v4, 0x4000

    const/16 v5, 0x4000

    if-ne v3, v5, :cond_2e

    or-int/lit16 v1, v1, 0x4000

    :cond_2e
    iget v3, p0, Lcom/google/r/b/a/ow;->X:I

    iput v3, v2, Lcom/google/r/b/a/ou;->W:I

    and-int v3, v4, v7

    if-ne v3, v7, :cond_2f

    or-int/2addr v1, v7

    :cond_2f
    iget v3, p0, Lcom/google/r/b/a/ow;->Y:I

    iput v3, v2, Lcom/google/r/b/a/ou;->X:I

    and-int v3, v4, v8

    if-ne v3, v8, :cond_30

    or-int/2addr v1, v8

    :cond_30
    iget v3, p0, Lcom/google/r/b/a/ow;->Z:I

    iput v3, v2, Lcom/google/r/b/a/ou;->Y:I

    and-int v3, v4, v9

    if-ne v3, v9, :cond_31

    or-int/2addr v1, v9

    :cond_31
    iget v3, p0, Lcom/google/r/b/a/ow;->aa:I

    iput v3, v2, Lcom/google/r/b/a/ou;->Z:I

    and-int v3, v4, v10

    if-ne v3, v10, :cond_32

    or-int/2addr v1, v10

    :cond_32
    iget v3, p0, Lcom/google/r/b/a/ow;->ab:I

    iput v3, v2, Lcom/google/r/b/a/ou;->aa:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    and-int/2addr v3, v11

    if-ne v3, v11, :cond_33

    iget-object v3, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    const v4, -0x80001

    and-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    :cond_33
    iget-object v3, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    iput-object v3, v2, Lcom/google/r/b/a/ou;->ab:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/ou;->a:I

    iput v1, v2, Lcom/google/r/b/a/ou;->b:I

    return-object v2
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1740
    check-cast p1, Lcom/google/r/b/a/ou;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ow;->a(Lcom/google/r/b/a/ou;)Lcom/google/r/b/a/ow;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ou;)Lcom/google/r/b/a/ow;
    .locals 8

    .prologue
    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2086
    invoke-static {}, Lcom/google/r/b/a/ou;->g()Lcom/google/r/b/a/ou;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2252
    :goto_0
    return-object p0

    .line 2087
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_10

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2088
    iget v2, p1, Lcom/google/r/b/a/ou;->c:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->c:I

    .line 2090
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2091
    iget v2, p1, Lcom/google/r/b/a/ou;->d:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->d:I

    .line 2093
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 2094
    iget v2, p1, Lcom/google/r/b/a/ou;->e:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->e:I

    .line 2096
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 2097
    iget v2, p1, Lcom/google/r/b/a/ou;->f:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->f:I

    .line 2099
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 2100
    iget v2, p1, Lcom/google/r/b/a/ou;->g:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->g:I

    .line 2102
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 2103
    iget v2, p1, Lcom/google/r/b/a/ou;->h:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->h:I

    .line 2105
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 2106
    iget v2, p1, Lcom/google/r/b/a/ou;->i:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->i:I

    .line 2108
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 2109
    iget v2, p1, Lcom/google/r/b/a/ou;->j:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->j:I

    .line 2111
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 2112
    iget v2, p1, Lcom/google/r/b/a/ou;->k:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->k:I

    .line 2114
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 2115
    iget v2, p1, Lcom/google/r/b/a/ou;->l:F

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->l:F

    .line 2117
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 2118
    iget v2, p1, Lcom/google/r/b/a/ou;->m:F

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->m:F

    .line 2120
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 2121
    iget v2, p1, Lcom/google/r/b/a/ou;->n:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->n:I

    .line 2123
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 2124
    iget v2, p1, Lcom/google/r/b/a/ou;->o:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->o:I

    .line 2126
    :cond_d
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_e
    if-eqz v2, :cond_e

    .line 2127
    iget v2, p1, Lcom/google/r/b/a/ou;->p:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->p:I

    .line 2129
    :cond_e
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_f
    if-eqz v2, :cond_20

    .line 2130
    iget v2, p1, Lcom/google/r/b/a/ou;->q:I

    invoke-static {v2}, Lcom/google/r/b/a/ox;->a(I)Lcom/google/r/b/a/ox;

    move-result-object v2

    if-nez v2, :cond_f

    sget-object v2, Lcom/google/r/b/a/ox;->a:Lcom/google/r/b/a/ox;

    :cond_f
    if-nez v2, :cond_1f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    move v2, v1

    .line 2087
    goto/16 :goto_1

    :cond_11
    move v2, v1

    .line 2090
    goto/16 :goto_2

    :cond_12
    move v2, v1

    .line 2093
    goto/16 :goto_3

    :cond_13
    move v2, v1

    .line 2096
    goto/16 :goto_4

    :cond_14
    move v2, v1

    .line 2099
    goto/16 :goto_5

    :cond_15
    move v2, v1

    .line 2102
    goto/16 :goto_6

    :cond_16
    move v2, v1

    .line 2105
    goto/16 :goto_7

    :cond_17
    move v2, v1

    .line 2108
    goto/16 :goto_8

    :cond_18
    move v2, v1

    .line 2111
    goto/16 :goto_9

    :cond_19
    move v2, v1

    .line 2114
    goto/16 :goto_a

    :cond_1a
    move v2, v1

    .line 2117
    goto/16 :goto_b

    :cond_1b
    move v2, v1

    .line 2120
    goto :goto_c

    :cond_1c
    move v2, v1

    .line 2123
    goto :goto_d

    :cond_1d
    move v2, v1

    .line 2126
    goto :goto_e

    :cond_1e
    move v2, v1

    .line 2129
    goto :goto_f

    .line 2130
    :cond_1f
    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iget v2, v2, Lcom/google/r/b/a/ox;->d:I

    iput v2, p0, Lcom/google/r/b/a/ow;->q:I

    .line 2132
    :cond_20
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_46

    move v2, v0

    :goto_10
    if-eqz v2, :cond_21

    .line 2133
    iget v2, p1, Lcom/google/r/b/a/ou;->r:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->r:I

    .line 2135
    :cond_21
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_47

    move v2, v0

    :goto_11
    if-eqz v2, :cond_22

    .line 2136
    iget v2, p1, Lcom/google/r/b/a/ou;->s:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->s:I

    .line 2138
    :cond_22
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_48

    move v2, v0

    :goto_12
    if-eqz v2, :cond_23

    .line 2139
    iget v2, p1, Lcom/google/r/b/a/ou;->t:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    or-int/2addr v3, v7

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->t:I

    .line 2141
    :cond_23
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_49

    move v2, v0

    :goto_13
    if-eqz v2, :cond_24

    .line 2142
    iget v2, p1, Lcom/google/r/b/a/ou;->u:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x40000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->u:I

    .line 2144
    :cond_24
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_4a

    move v2, v0

    :goto_14
    if-eqz v2, :cond_25

    .line 2145
    iget v2, p1, Lcom/google/r/b/a/ou;->v:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x80000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->v:I

    .line 2147
    :cond_25
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_4b

    move v2, v0

    :goto_15
    if-eqz v2, :cond_26

    .line 2148
    iget v2, p1, Lcom/google/r/b/a/ou;->w:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x100000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->w:I

    .line 2150
    :cond_26
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_4c

    move v2, v0

    :goto_16
    if-eqz v2, :cond_27

    .line 2151
    iget v2, p1, Lcom/google/r/b/a/ou;->x:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x200000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->x:I

    .line 2153
    :cond_27
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    const/high16 v3, 0x400000

    if-ne v2, v3, :cond_4d

    move v2, v0

    :goto_17
    if-eqz v2, :cond_28

    .line 2154
    iget v2, p1, Lcom/google/r/b/a/ou;->y:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x400000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->y:I

    .line 2156
    :cond_28
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    const/high16 v3, 0x800000

    if-ne v2, v3, :cond_4e

    move v2, v0

    :goto_18
    if-eqz v2, :cond_29

    .line 2157
    iget v2, p1, Lcom/google/r/b/a/ou;->z:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x800000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->z:I

    .line 2159
    :cond_29
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    const/high16 v3, 0x1000000

    if-ne v2, v3, :cond_4f

    move v2, v0

    :goto_19
    if-eqz v2, :cond_2a

    .line 2160
    iget v2, p1, Lcom/google/r/b/a/ou;->A:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x1000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->A:I

    .line 2162
    :cond_2a
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    const/high16 v3, 0x2000000

    if-ne v2, v3, :cond_50

    move v2, v0

    :goto_1a
    if-eqz v2, :cond_2b

    .line 2163
    iget v2, p1, Lcom/google/r/b/a/ou;->B:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x2000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->B:I

    .line 2165
    :cond_2b
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v2, v3

    const/high16 v3, 0x4000000

    if-ne v2, v3, :cond_51

    move v2, v0

    :goto_1b
    if-eqz v2, :cond_2c

    .line 2166
    iget v2, p1, Lcom/google/r/b/a/ou;->C:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x4000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->C:I

    .line 2168
    :cond_2c
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v2, v3

    const/high16 v3, 0x8000000

    if-ne v2, v3, :cond_52

    move v2, v0

    :goto_1c
    if-eqz v2, :cond_2d

    .line 2169
    iget v2, p1, Lcom/google/r/b/a/ou;->D:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x8000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->D:I

    .line 2171
    :cond_2d
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000000

    if-ne v2, v3, :cond_53

    move v2, v0

    :goto_1d
    if-eqz v2, :cond_2e

    .line 2172
    iget v2, p1, Lcom/google/r/b/a/ou;->E:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x10000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->E:I

    .line 2174
    :cond_2e
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000000

    if-ne v2, v3, :cond_54

    move v2, v0

    :goto_1e
    if-eqz v2, :cond_2f

    .line 2175
    iget v2, p1, Lcom/google/r/b/a/ou;->F:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x20000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->F:I

    .line 2177
    :cond_2f
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v2, v3, :cond_55

    move v2, v0

    :goto_1f
    if-eqz v2, :cond_30

    .line 2178
    iget-boolean v2, p1, Lcom/google/r/b/a/ou;->G:Z

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/ow;->G:Z

    .line 2180
    :cond_30
    iget v2, p1, Lcom/google/r/b/a/ou;->a:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_56

    move v2, v0

    :goto_20
    if-eqz v2, :cond_31

    .line 2181
    iget v2, p1, Lcom/google/r/b/a/ou;->H:I

    iget v3, p0, Lcom/google/r/b/a/ow;->a:I

    const/high16 v4, -0x80000000

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/ow;->a:I

    iput v2, p0, Lcom/google/r/b/a/ow;->H:I

    .line 2183
    :cond_31
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_57

    move v2, v0

    :goto_21
    if-eqz v2, :cond_32

    .line 2184
    iget v2, p1, Lcom/google/r/b/a/ou;->I:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->J:I

    .line 2186
    :cond_32
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_58

    move v2, v0

    :goto_22
    if-eqz v2, :cond_33

    .line 2187
    iget v2, p1, Lcom/google/r/b/a/ou;->J:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->K:I

    .line 2189
    :cond_33
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_59

    move v2, v0

    :goto_23
    if-eqz v2, :cond_34

    .line 2190
    iget-object v2, p0, Lcom/google/r/b/a/ow;->L:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ou;->K:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2191
    iget v2, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/ow;->b:I

    .line 2193
    :cond_34
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_5a

    move v2, v0

    :goto_24
    if-eqz v2, :cond_35

    .line 2194
    iget-boolean v2, p1, Lcom/google/r/b/a/ou;->L:Z

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput-boolean v2, p0, Lcom/google/r/b/a/ow;->M:Z

    .line 2196
    :cond_35
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5b

    move v2, v0

    :goto_25
    if-eqz v2, :cond_36

    .line 2197
    iget v2, p1, Lcom/google/r/b/a/ou;->M:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->N:I

    .line 2199
    :cond_36
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5c

    move v2, v0

    :goto_26
    if-eqz v2, :cond_37

    .line 2200
    iget-boolean v2, p1, Lcom/google/r/b/a/ou;->N:Z

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput-boolean v2, p0, Lcom/google/r/b/a/ow;->O:Z

    .line 2202
    :cond_37
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_5d

    move v2, v0

    :goto_27
    if-eqz v2, :cond_38

    .line 2203
    iget-boolean v2, p1, Lcom/google/r/b/a/ou;->O:Z

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput-boolean v2, p0, Lcom/google/r/b/a/ow;->P:Z

    .line 2205
    :cond_38
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_5e

    move v2, v0

    :goto_28
    if-eqz v2, :cond_39

    .line 2206
    iget-boolean v2, p1, Lcom/google/r/b/a/ou;->P:Z

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput-boolean v2, p0, Lcom/google/r/b/a/ow;->Q:Z

    .line 2208
    :cond_39
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_5f

    move v2, v0

    :goto_29
    if-eqz v2, :cond_3a

    .line 2209
    iget v2, p1, Lcom/google/r/b/a/ou;->Q:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->R:I

    .line 2211
    :cond_3a
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_60

    move v2, v0

    :goto_2a
    if-eqz v2, :cond_3b

    .line 2212
    iget v2, p1, Lcom/google/r/b/a/ou;->R:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->S:I

    .line 2214
    :cond_3b
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_61

    move v2, v0

    :goto_2b
    if-eqz v2, :cond_3c

    .line 2215
    iget v2, p1, Lcom/google/r/b/a/ou;->S:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->T:I

    .line 2217
    :cond_3c
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_62

    move v2, v0

    :goto_2c
    if-eqz v2, :cond_3d

    .line 2218
    iget v2, p1, Lcom/google/r/b/a/ou;->T:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->U:I

    .line 2220
    :cond_3d
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_63

    move v2, v0

    :goto_2d
    if-eqz v2, :cond_3e

    .line 2221
    iget v2, p1, Lcom/google/r/b/a/ou;->U:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->V:I

    .line 2223
    :cond_3e
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_64

    move v2, v0

    :goto_2e
    if-eqz v2, :cond_3f

    .line 2224
    iget v2, p1, Lcom/google/r/b/a/ou;->V:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->W:I

    .line 2226
    :cond_3f
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_65

    move v2, v0

    :goto_2f
    if-eqz v2, :cond_40

    .line 2227
    iget v2, p1, Lcom/google/r/b/a/ou;->W:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->X:I

    .line 2229
    :cond_40
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_66

    move v2, v0

    :goto_30
    if-eqz v2, :cond_41

    .line 2230
    iget v2, p1, Lcom/google/r/b/a/ou;->X:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->Y:I

    .line 2232
    :cond_41
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_67

    move v2, v0

    :goto_31
    if-eqz v2, :cond_42

    .line 2233
    iget v2, p1, Lcom/google/r/b/a/ou;->Y:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->Z:I

    .line 2235
    :cond_42
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_68

    move v2, v0

    :goto_32
    if-eqz v2, :cond_43

    .line 2236
    iget v2, p1, Lcom/google/r/b/a/ou;->Z:I

    iget v3, p0, Lcom/google/r/b/a/ow;->b:I

    or-int/2addr v3, v7

    iput v3, p0, Lcom/google/r/b/a/ow;->b:I

    iput v2, p0, Lcom/google/r/b/a/ow;->aa:I

    .line 2238
    :cond_43
    iget v2, p1, Lcom/google/r/b/a/ou;->b:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_69

    :goto_33
    if-eqz v0, :cond_44

    .line 2239
    iget v0, p1, Lcom/google/r/b/a/ou;->aa:I

    iget v1, p0, Lcom/google/r/b/a/ow;->b:I

    const/high16 v2, 0x40000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/r/b/a/ow;->b:I

    iput v0, p0, Lcom/google/r/b/a/ow;->ab:I

    .line 2241
    :cond_44
    iget-object v0, p1, Lcom/google/r/b/a/ou;->ab:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_45

    .line 2242
    iget-object v0, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 2243
    iget-object v0, p1, Lcom/google/r/b/a/ou;->ab:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    .line 2244
    iget v0, p0, Lcom/google/r/b/a/ow;->b:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/r/b/a/ow;->b:I

    .line 2251
    :cond_45
    :goto_34
    iget-object v0, p1, Lcom/google/r/b/a/ou;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_46
    move v2, v1

    .line 2132
    goto/16 :goto_10

    :cond_47
    move v2, v1

    .line 2135
    goto/16 :goto_11

    :cond_48
    move v2, v1

    .line 2138
    goto/16 :goto_12

    :cond_49
    move v2, v1

    .line 2141
    goto/16 :goto_13

    :cond_4a
    move v2, v1

    .line 2144
    goto/16 :goto_14

    :cond_4b
    move v2, v1

    .line 2147
    goto/16 :goto_15

    :cond_4c
    move v2, v1

    .line 2150
    goto/16 :goto_16

    :cond_4d
    move v2, v1

    .line 2153
    goto/16 :goto_17

    :cond_4e
    move v2, v1

    .line 2156
    goto/16 :goto_18

    :cond_4f
    move v2, v1

    .line 2159
    goto/16 :goto_19

    :cond_50
    move v2, v1

    .line 2162
    goto/16 :goto_1a

    :cond_51
    move v2, v1

    .line 2165
    goto/16 :goto_1b

    :cond_52
    move v2, v1

    .line 2168
    goto/16 :goto_1c

    :cond_53
    move v2, v1

    .line 2171
    goto/16 :goto_1d

    :cond_54
    move v2, v1

    .line 2174
    goto/16 :goto_1e

    :cond_55
    move v2, v1

    .line 2177
    goto/16 :goto_1f

    :cond_56
    move v2, v1

    .line 2180
    goto/16 :goto_20

    :cond_57
    move v2, v1

    .line 2183
    goto/16 :goto_21

    :cond_58
    move v2, v1

    .line 2186
    goto/16 :goto_22

    :cond_59
    move v2, v1

    .line 2189
    goto/16 :goto_23

    :cond_5a
    move v2, v1

    .line 2193
    goto/16 :goto_24

    :cond_5b
    move v2, v1

    .line 2196
    goto/16 :goto_25

    :cond_5c
    move v2, v1

    .line 2199
    goto/16 :goto_26

    :cond_5d
    move v2, v1

    .line 2202
    goto/16 :goto_27

    :cond_5e
    move v2, v1

    .line 2205
    goto/16 :goto_28

    :cond_5f
    move v2, v1

    .line 2208
    goto/16 :goto_29

    :cond_60
    move v2, v1

    .line 2211
    goto/16 :goto_2a

    :cond_61
    move v2, v1

    .line 2214
    goto/16 :goto_2b

    :cond_62
    move v2, v1

    .line 2217
    goto/16 :goto_2c

    :cond_63
    move v2, v1

    .line 2220
    goto/16 :goto_2d

    :cond_64
    move v2, v1

    .line 2223
    goto/16 :goto_2e

    :cond_65
    move v2, v1

    .line 2226
    goto/16 :goto_2f

    :cond_66
    move v2, v1

    .line 2229
    goto/16 :goto_30

    :cond_67
    move v2, v1

    .line 2232
    goto/16 :goto_31

    :cond_68
    move v2, v1

    .line 2235
    goto/16 :goto_32

    :cond_69
    move v0, v1

    .line 2238
    goto/16 :goto_33

    .line 2246
    :cond_6a
    iget v0, p0, Lcom/google/r/b/a/ow;->b:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-eq v0, v1, :cond_6b

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/ow;->b:I

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/r/b/a/ow;->b:I

    .line 2247
    :cond_6b
    iget-object v0, p0, Lcom/google/r/b/a/ow;->ac:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/ou;->ab:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_34
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2256
    const/4 v0, 0x1

    return v0
.end method
