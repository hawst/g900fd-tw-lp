.class public final Lcom/google/r/b/a/aka;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/akd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aka;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/aka;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/google/r/b/a/akb;

    invoke-direct {v0}, Lcom/google/r/b/a/akb;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aka;->PARSER:Lcom/google/n/ax;

    .line 266
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aka;->i:Lcom/google/n/aw;

    .line 639
    new-instance v0, Lcom/google/r/b/a/aka;

    invoke-direct {v0}, Lcom/google/r/b/a/aka;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aka;->f:Lcom/google/r/b/a/aka;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 146
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aka;->b:Lcom/google/n/ao;

    .line 162
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aka;->c:Lcom/google/n/ao;

    .line 178
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aka;->d:Lcom/google/n/ao;

    .line 209
    iput-byte v3, p0, Lcom/google/r/b/a/aka;->g:B

    .line 237
    iput v3, p0, Lcom/google/r/b/a/aka;->h:I

    .line 63
    iget-object v0, p0, Lcom/google/r/b/a/aka;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 64
    iget-object v0, p0, Lcom/google/r/b/a/aka;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 65
    iget-object v0, p0, Lcom/google/r/b/a/aka;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aka;->e:I

    .line 67
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Lcom/google/r/b/a/aka;-><init>()V

    .line 74
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 79
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 80
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 81
    sparse-switch v3, :sswitch_data_0

    .line 86
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 88
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 84
    goto :goto_0

    .line 93
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/aka;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 94
    iget v3, p0, Lcom/google/r/b/a/aka;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aka;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aka;->au:Lcom/google/n/bn;

    throw v0

    .line 98
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/aka;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 99
    iget v3, p0, Lcom/google/r/b/a/aka;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/aka;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 122
    :catch_1
    move-exception v0

    .line 123
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 124
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/aka;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 104
    iget v3, p0, Lcom/google/r/b/a/aka;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/aka;->a:I

    goto :goto_0

    .line 108
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 109
    invoke-static {v3}, Lcom/google/maps/g/d/d;->a(I)Lcom/google/maps/g/d/d;

    move-result-object v4

    .line 110
    if-nez v4, :cond_1

    .line 111
    const/4 v4, 0x4

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 113
    :cond_1
    iget v4, p0, Lcom/google/r/b/a/aka;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/aka;->a:I

    .line 114
    iput v3, p0, Lcom/google/r/b/a/aka;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aka;->au:Lcom/google/n/bn;

    .line 127
    return-void

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 60
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 146
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aka;->b:Lcom/google/n/ao;

    .line 162
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aka;->c:Lcom/google/n/ao;

    .line 178
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aka;->d:Lcom/google/n/ao;

    .line 209
    iput-byte v1, p0, Lcom/google/r/b/a/aka;->g:B

    .line 237
    iput v1, p0, Lcom/google/r/b/a/aka;->h:I

    .line 61
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aka;
    .locals 1

    .prologue
    .line 642
    sget-object v0, Lcom/google/r/b/a/aka;->f:Lcom/google/r/b/a/aka;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/akc;
    .locals 1

    .prologue
    .line 328
    new-instance v0, Lcom/google/r/b/a/akc;

    invoke-direct {v0}, Lcom/google/r/b/a/akc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aka;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    sget-object v0, Lcom/google/r/b/a/aka;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 221
    invoke-virtual {p0}, Lcom/google/r/b/a/aka;->c()I

    .line 222
    iget v0, p0, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/r/b/a/aka;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 225
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 226
    iget-object v0, p0, Lcom/google/r/b/a/aka;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 228
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 229
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/aka;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 231
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 232
    iget v0, p0, Lcom/google/r/b/a/aka;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 234
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/aka;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 235
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 211
    iget-byte v1, p0, Lcom/google/r/b/a/aka;->g:B

    .line 212
    if-ne v1, v0, :cond_0

    .line 216
    :goto_0
    return v0

    .line 213
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 215
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aka;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 239
    iget v0, p0, Lcom/google/r/b/a/aka;->h:I

    .line 240
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 261
    :goto_0
    return v0

    .line 243
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 244
    iget-object v0, p0, Lcom/google/r/b/a/aka;->b:Lcom/google/n/ao;

    .line 245
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 247
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 248
    iget-object v2, p0, Lcom/google/r/b/a/aka;->c:Lcom/google/n/ao;

    .line 249
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 251
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 252
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/aka;->d:Lcom/google/n/ao;

    .line 253
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 255
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/aka;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 256
    iget v2, p0, Lcom/google/r/b/a/aka;->e:I

    .line 257
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 259
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/aka;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    iput v0, p0, Lcom/google/r/b/a/aka;->h:I

    goto :goto_0

    .line 257
    :cond_4
    const/16 v1, 0xa

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/r/b/a/aka;->newBuilder()Lcom/google/r/b/a/akc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/akc;->a(Lcom/google/r/b/a/aka;)Lcom/google/r/b/a/akc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/r/b/a/aka;->newBuilder()Lcom/google/r/b/a/akc;

    move-result-object v0

    return-object v0
.end method
