.class public final Lcom/google/r/b/a/jd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/jg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/jd;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/jd;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/r/b/a/je;

    invoke-direct {v0}, Lcom/google/r/b/a/je;-><init>()V

    sput-object v0, Lcom/google/r/b/a/jd;->PARSER:Lcom/google/n/ax;

    .line 190
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/jd;->g:Lcom/google/n/aw;

    .line 469
    new-instance v0, Lcom/google/r/b/a/jd;

    invoke-direct {v0}, Lcom/google/r/b/a/jd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/jd;->d:Lcom/google/r/b/a/jd;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 84
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/jd;->b:Lcom/google/n/ao;

    .line 141
    iput-byte v2, p0, Lcom/google/r/b/a/jd;->e:B

    .line 169
    iput v2, p0, Lcom/google/r/b/a/jd;->f:I

    .line 18
    iget-object v0, p0, Lcom/google/r/b/a/jd;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/jd;->c:Ljava/lang/Object;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/r/b/a/jd;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 34
    sparse-switch v3, :sswitch_data_0

    .line 39
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/jd;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 47
    iget v3, p0, Lcom/google/r/b/a/jd;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/jd;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/jd;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 52
    iget v4, p0, Lcom/google/r/b/a/jd;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/jd;->a:I

    .line 53
    iput-object v3, p0, Lcom/google/r/b/a/jd;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 60
    :catch_1
    move-exception v0

    .line 61
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 62
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/jd;->au:Lcom/google/n/bn;

    .line 65
    return-void

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 84
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/jd;->b:Lcom/google/n/ao;

    .line 141
    iput-byte v1, p0, Lcom/google/r/b/a/jd;->e:B

    .line 169
    iput v1, p0, Lcom/google/r/b/a/jd;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/jd;
    .locals 1

    .prologue
    .line 472
    sget-object v0, Lcom/google/r/b/a/jd;->d:Lcom/google/r/b/a/jd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/jf;
    .locals 1

    .prologue
    .line 252
    new-instance v0, Lcom/google/r/b/a/jf;

    invoke-direct {v0}, Lcom/google/r/b/a/jf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/jd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/google/r/b/a/jd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 159
    invoke-virtual {p0}, Lcom/google/r/b/a/jd;->c()I

    .line 160
    iget v0, p0, Lcom/google/r/b/a/jd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/r/b/a/jd;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 163
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/jd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 164
    iget-object v0, p0, Lcom/google/r/b/a/jd;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/jd;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/jd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 167
    return-void

    .line 164
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 143
    iget-byte v0, p0, Lcom/google/r/b/a/jd;->e:B

    .line 144
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 154
    :goto_0
    return v0

    .line 145
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 147
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/jd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/google/r/b/a/jd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ael;->g()Lcom/google/r/b/a/ael;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ael;

    invoke-virtual {v0}, Lcom/google/r/b/a/ael;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 149
    iput-byte v2, p0, Lcom/google/r/b/a/jd;->e:B

    move v0, v2

    .line 150
    goto :goto_0

    :cond_2
    move v0, v2

    .line 147
    goto :goto_1

    .line 153
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/jd;->e:B

    move v0, v1

    .line 154
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 171
    iget v0, p0, Lcom/google/r/b/a/jd;->f:I

    .line 172
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 185
    :goto_0
    return v0

    .line 175
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/jd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 176
    iget-object v0, p0, Lcom/google/r/b/a/jd;->b:Lcom/google/n/ao;

    .line 177
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 179
    :goto_1
    iget v0, p0, Lcom/google/r/b/a/jd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/r/b/a/jd;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/jd;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/jd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 184
    iput v0, p0, Lcom/google/r/b/a/jd;->f:I

    goto :goto_0

    .line 181
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/jd;->newBuilder()Lcom/google/r/b/a/jf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/jf;->a(Lcom/google/r/b/a/jd;)Lcom/google/r/b/a/jf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/jd;->newBuilder()Lcom/google/r/b/a/jf;

    move-result-object v0

    return-object v0
.end method
