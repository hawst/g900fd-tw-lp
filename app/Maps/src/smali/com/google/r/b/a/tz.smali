.class public final Lcom/google/r/b/a/tz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/uc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/tz;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/r/b/a/tz;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/be;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 205
    new-instance v0, Lcom/google/r/b/a/ua;

    invoke-direct {v0}, Lcom/google/r/b/a/ua;-><init>()V

    sput-object v0, Lcom/google/r/b/a/tz;->PARSER:Lcom/google/n/ax;

    .line 297
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/tz;->e:Lcom/google/n/aw;

    .line 558
    new-instance v0, Lcom/google/r/b/a/tz;

    invoke-direct {v0}, Lcom/google/r/b/a/tz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/tz;->b:Lcom/google/r/b/a/tz;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 156
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 255
    iput-byte v0, p0, Lcom/google/r/b/a/tz;->c:B

    .line 280
    iput v0, p0, Lcom/google/r/b/a/tz;->d:I

    .line 157
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    .line 158
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 164
    invoke-direct {p0}, Lcom/google/r/b/a/tz;-><init>()V

    .line 167
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 170
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 171
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 172
    sparse-switch v4, :sswitch_data_0

    .line 177
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 179
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 175
    goto :goto_0

    .line 184
    :sswitch_1
    and-int/lit8 v4, v0, 0x1

    if-eq v4, v2, :cond_1

    .line 185
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    .line 186
    or-int/lit8 v0, v0, 0x1

    .line 188
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    sget-object v5, Lcom/google/r/b/a/be;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 193
    :catch_0
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 194
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 200
    iget-object v1, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    .line 202
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/tz;->au:Lcom/google/n/bn;

    throw v0

    .line 199
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 200
    iget-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    .line 202
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tz;->au:Lcom/google/n/bn;

    .line 203
    return-void

    .line 195
    :catch_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 196
    :try_start_2
    new-instance v4, Lcom/google/n/ak;

    .line 197
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 199
    :catchall_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_1

    .line 172
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 154
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 255
    iput-byte v0, p0, Lcom/google/r/b/a/tz;->c:B

    .line 280
    iput v0, p0, Lcom/google/r/b/a/tz;->d:I

    .line 155
    return-void
.end method

.method public static d()Lcom/google/r/b/a/tz;
    .locals 1

    .prologue
    .line 561
    sget-object v0, Lcom/google/r/b/a/tz;->b:Lcom/google/r/b/a/tz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ub;
    .locals 1

    .prologue
    .line 359
    new-instance v0, Lcom/google/r/b/a/ub;

    invoke-direct {v0}, Lcom/google/r/b/a/ub;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/tz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    sget-object v0, Lcom/google/r/b/a/tz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/google/r/b/a/tz;->c()I

    .line 274
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 275
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 274
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/tz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 278
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 257
    iget-byte v0, p0, Lcom/google/r/b/a/tz;->c:B

    .line 258
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 268
    :cond_0
    :goto_0
    return v2

    .line 259
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 261
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 262
    iget-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 263
    iput-byte v2, p0, Lcom/google/r/b/a/tz;->c:B

    goto :goto_0

    .line 261
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 267
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/tz;->c:B

    move v2, v3

    .line 268
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 282
    iget v0, p0, Lcom/google/r/b/a/tz;->d:I

    .line 283
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 292
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 286
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 287
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    .line 288
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 286
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/tz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 291
    iput v0, p0, Lcom/google/r/b/a/tz;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 148
    invoke-static {}, Lcom/google/r/b/a/tz;->newBuilder()Lcom/google/r/b/a/ub;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ub;->a(Lcom/google/r/b/a/tz;)Lcom/google/r/b/a/ub;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 148
    invoke-static {}, Lcom/google/r/b/a/tz;->newBuilder()Lcom/google/r/b/a/ub;

    move-result-object v0

    return-object v0
.end method
