.class public final Lcom/google/r/b/a/yi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/xy;",
        "Lcom/google/r/b/a/yi;",
        ">;",
        "Lcom/google/r/b/a/zb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/r/b/a/yj;

.field private c:Lcom/google/r/b/a/yt;

.field private d:Lcom/google/r/b/a/ya;

.field private e:Lcom/google/r/b/a/yp;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5338
    sget-object v0, Lcom/google/r/b/a/xy;->f:Lcom/google/r/b/a/xy;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5425
    iput-object v1, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    .line 5486
    iput-object v1, p0, Lcom/google/r/b/a/yi;->c:Lcom/google/r/b/a/yt;

    .line 5547
    iput-object v1, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    .line 5608
    iput-object v1, p0, Lcom/google/r/b/a/yi;->e:Lcom/google/r/b/a/yp;

    .line 5339
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5330
    new-instance v2, Lcom/google/r/b/a/xy;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/xy;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/yi;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    iput-object v1, v2, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/yi;->c:Lcom/google/r/b/a/yt;

    iput-object v1, v2, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    iput-object v1, v2, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/yi;->e:Lcom/google/r/b/a/yp;

    iput-object v1, v2, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    iput v0, v2, Lcom/google/r/b/a/xy;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5330
    check-cast p1, Lcom/google/r/b/a/xy;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/yi;->a(Lcom/google/r/b/a/xy;)Lcom/google/r/b/a/yi;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/xy;)Lcom/google/r/b/a/yi;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 5382
    invoke-static {}, Lcom/google/r/b/a/xy;->d()Lcom/google/r/b/a/xy;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 5396
    :goto_0
    return-object p0

    .line 5383
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 5384
    iget-object v0, p1, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/r/b/a/yj;->d()Lcom/google/r/b/a/yj;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/r/b/a/yi;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_7

    iget-object v3, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    invoke-static {}, Lcom/google/r/b/a/yj;->d()Lcom/google/r/b/a/yj;

    move-result-object v4

    if-eq v3, v4, :cond_7

    iget-object v3, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    invoke-static {v3}, Lcom/google/r/b/a/yj;->a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/yl;->a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/yl;->c()Lcom/google/r/b/a/yj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    :goto_3
    iget v0, p0, Lcom/google/r/b/a/yi;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/yi;->a:I

    .line 5386
    :cond_1
    iget v0, p1, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 5387
    iget-object v0, p1, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/r/b/a/yt;->d()Lcom/google/r/b/a/yt;

    move-result-object v0

    :goto_5
    iget v3, p0, Lcom/google/r/b/a/yi;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_a

    iget-object v3, p0, Lcom/google/r/b/a/yi;->c:Lcom/google/r/b/a/yt;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/r/b/a/yi;->c:Lcom/google/r/b/a/yt;

    invoke-static {}, Lcom/google/r/b/a/yt;->d()Lcom/google/r/b/a/yt;

    move-result-object v4

    if-eq v3, v4, :cond_a

    iget-object v3, p0, Lcom/google/r/b/a/yi;->c:Lcom/google/r/b/a/yt;

    invoke-static {v3}, Lcom/google/r/b/a/yt;->a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/yv;->a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/yv;->c()Lcom/google/r/b/a/yt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yi;->c:Lcom/google/r/b/a/yt;

    :goto_6
    iget v0, p0, Lcom/google/r/b/a/yi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/yi;->a:I

    .line 5389
    :cond_2
    iget v0, p1, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_b

    move v0, v1

    :goto_7
    if-eqz v0, :cond_3

    .line 5390
    iget-object v0, p1, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/r/b/a/ya;->d()Lcom/google/r/b/a/ya;

    move-result-object v0

    :goto_8
    iget v3, p0, Lcom/google/r/b/a/yi;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_d

    iget-object v3, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    invoke-static {}, Lcom/google/r/b/a/ya;->d()Lcom/google/r/b/a/ya;

    move-result-object v4

    if-eq v3, v4, :cond_d

    iget-object v3, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    invoke-static {v3}, Lcom/google/r/b/a/ya;->a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/yc;->a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/yc;->c()Lcom/google/r/b/a/ya;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    :goto_9
    iget v0, p0, Lcom/google/r/b/a/yi;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/yi;->a:I

    .line 5392
    :cond_3
    iget v0, p1, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_e

    move v0, v1

    :goto_a
    if-eqz v0, :cond_4

    .line 5393
    iget-object v0, p1, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    if-nez v0, :cond_f

    invoke-static {}, Lcom/google/r/b/a/yp;->d()Lcom/google/r/b/a/yp;

    move-result-object v0

    :goto_b
    iget v1, p0, Lcom/google/r/b/a/yi;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_10

    iget-object v1, p0, Lcom/google/r/b/a/yi;->e:Lcom/google/r/b/a/yp;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/google/r/b/a/yi;->e:Lcom/google/r/b/a/yp;

    invoke-static {}, Lcom/google/r/b/a/yp;->d()Lcom/google/r/b/a/yp;

    move-result-object v2

    if-eq v1, v2, :cond_10

    iget-object v1, p0, Lcom/google/r/b/a/yi;->e:Lcom/google/r/b/a/yp;

    invoke-static {v1}, Lcom/google/r/b/a/yp;->a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/yr;->a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/yr;->c()Lcom/google/r/b/a/yp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yi;->e:Lcom/google/r/b/a/yp;

    :goto_c
    iget v0, p0, Lcom/google/r/b/a/yi;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/yi;->a:I

    .line 5395
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/xy;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 5383
    goto/16 :goto_1

    .line 5384
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    goto/16 :goto_2

    :cond_7
    iput-object v0, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    goto/16 :goto_3

    :cond_8
    move v0, v2

    .line 5386
    goto/16 :goto_4

    .line 5387
    :cond_9
    iget-object v0, p1, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    goto/16 :goto_5

    :cond_a
    iput-object v0, p0, Lcom/google/r/b/a/yi;->c:Lcom/google/r/b/a/yt;

    goto/16 :goto_6

    :cond_b
    move v0, v2

    .line 5389
    goto/16 :goto_7

    .line 5390
    :cond_c
    iget-object v0, p1, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    goto/16 :goto_8

    :cond_d
    iput-object v0, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    goto :goto_9

    :cond_e
    move v0, v2

    .line 5392
    goto :goto_a

    .line 5393
    :cond_f
    iget-object v0, p1, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    goto :goto_b

    :cond_10
    iput-object v0, p0, Lcom/google/r/b/a/yi;->e:Lcom/google/r/b/a/yp;

    goto :goto_c
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5400
    iget v0, p0, Lcom/google/r/b/a/yi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 5420
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 5400
    goto :goto_0

    .line 5404
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/yi;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-nez v0, :cond_3

    move v0, v1

    .line 5406
    goto :goto_1

    :cond_2
    move v0, v1

    .line 5404
    goto :goto_2

    .line 5408
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/yi;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    move v0, v1

    .line 5410
    goto :goto_1

    :cond_4
    move v0, v1

    .line 5408
    goto :goto_3

    .line 5412
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/r/b/a/yj;->d()Lcom/google/r/b/a/yj;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/r/b/a/yj;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 5414
    goto :goto_1

    .line 5412
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/yi;->b:Lcom/google/r/b/a/yj;

    goto :goto_4

    .line 5416
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/r/b/a/ya;->d()Lcom/google/r/b/a/ya;

    move-result-object v0

    :goto_5
    invoke-virtual {v0}, Lcom/google/r/b/a/ya;->b()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 5418
    goto :goto_1

    .line 5416
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/yi;->d:Lcom/google/r/b/a/ya;

    goto :goto_5

    :cond_9
    move v0, v2

    .line 5420
    goto :goto_1
.end method
