.class public final Lcom/google/r/b/a/qr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qu;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qr;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/qr;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3814
    new-instance v0, Lcom/google/r/b/a/qs;

    invoke-direct {v0}, Lcom/google/r/b/a/qs;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qr;->PARSER:Lcom/google/n/ax;

    .line 3911
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/qr;->g:Lcom/google/n/aw;

    .line 4174
    new-instance v0, Lcom/google/r/b/a/qr;

    invoke-direct {v0}, Lcom/google/r/b/a/qr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qr;->d:Lcom/google/r/b/a/qr;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 3765
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3831
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qr;->b:Lcom/google/n/ao;

    .line 3847
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    .line 3862
    iput-byte v2, p0, Lcom/google/r/b/a/qr;->e:B

    .line 3890
    iput v2, p0, Lcom/google/r/b/a/qr;->f:I

    .line 3766
    iget-object v0, p0, Lcom/google/r/b/a/qr;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 3767
    iget-object v0, p0, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 3768
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3774
    invoke-direct {p0}, Lcom/google/r/b/a/qr;-><init>()V

    .line 3775
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 3780
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 3781
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 3782
    sparse-switch v3, :sswitch_data_0

    .line 3787
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 3789
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 3785
    goto :goto_0

    .line 3794
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/qr;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3795
    iget v3, p0, Lcom/google/r/b/a/qr;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/qr;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3805
    :catch_0
    move-exception v0

    .line 3806
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3811
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/qr;->au:Lcom/google/n/bn;

    throw v0

    .line 3799
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3800
    iget v3, p0, Lcom/google/r/b/a/qr;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/qr;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3807
    :catch_1
    move-exception v0

    .line 3808
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3809
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3811
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qr;->au:Lcom/google/n/bn;

    .line 3812
    return-void

    .line 3782
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 3763
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3831
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qr;->b:Lcom/google/n/ao;

    .line 3847
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    .line 3862
    iput-byte v1, p0, Lcom/google/r/b/a/qr;->e:B

    .line 3890
    iput v1, p0, Lcom/google/r/b/a/qr;->f:I

    .line 3764
    return-void
.end method

.method public static d()Lcom/google/r/b/a/qr;
    .locals 1

    .prologue
    .line 4177
    sget-object v0, Lcom/google/r/b/a/qr;->d:Lcom/google/r/b/a/qr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/qt;
    .locals 1

    .prologue
    .line 3973
    new-instance v0, Lcom/google/r/b/a/qt;

    invoke-direct {v0}, Lcom/google/r/b/a/qt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3826
    sget-object v0, Lcom/google/r/b/a/qr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3880
    invoke-virtual {p0}, Lcom/google/r/b/a/qr;->c()I

    .line 3881
    iget v0, p0, Lcom/google/r/b/a/qr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3882
    iget-object v0, p0, Lcom/google/r/b/a/qr;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3884
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3885
    iget-object v0, p0, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3887
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/qr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3888
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3864
    iget-byte v0, p0, Lcom/google/r/b/a/qr;->e:B

    .line 3865
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 3875
    :goto_0
    return v0

    .line 3866
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 3868
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/qr;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 3869
    iget-object v0, p0, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ana;->d()Lcom/google/r/b/a/ana;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ana;

    invoke-virtual {v0}, Lcom/google/r/b/a/ana;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3870
    iput-byte v2, p0, Lcom/google/r/b/a/qr;->e:B

    move v0, v2

    .line 3871
    goto :goto_0

    :cond_2
    move v0, v2

    .line 3868
    goto :goto_1

    .line 3874
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/qr;->e:B

    move v0, v1

    .line 3875
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3892
    iget v0, p0, Lcom/google/r/b/a/qr;->f:I

    .line 3893
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3906
    :goto_0
    return v0

    .line 3896
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 3897
    iget-object v0, p0, Lcom/google/r/b/a/qr;->b:Lcom/google/n/ao;

    .line 3898
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 3900
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/qr;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 3901
    iget-object v2, p0, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    .line 3902
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 3904
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/qr;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3905
    iput v0, p0, Lcom/google/r/b/a/qr;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3757
    invoke-static {}, Lcom/google/r/b/a/qr;->newBuilder()Lcom/google/r/b/a/qt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/qt;->a(Lcom/google/r/b/a/qr;)Lcom/google/r/b/a/qt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3757
    invoke-static {}, Lcom/google/r/b/a/qr;->newBuilder()Lcom/google/r/b/a/qt;

    move-result-object v0

    return-object v0
.end method
