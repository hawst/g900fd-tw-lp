.class public final Lcom/google/r/b/a/ki;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/kl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ki;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/ki;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field d:Z

.field e:Lcom/google/n/ao;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcom/google/r/b/a/kj;

    invoke-direct {v0}, Lcom/google/r/b/a/kj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ki;->PARSER:Lcom/google/n/ax;

    .line 298
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ki;->i:Lcom/google/n/aw;

    .line 720
    new-instance v0, Lcom/google/r/b/a/ki;

    invoke-direct {v0}, Lcom/google/r/b/a/ki;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ki;->f:Lcom/google/r/b/a/ki;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 226
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ki;->e:Lcom/google/n/ao;

    .line 241
    iput-byte v2, p0, Lcom/google/r/b/a/ki;->g:B

    .line 269
    iput v2, p0, Lcom/google/r/b/a/ki;->h:I

    .line 68
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    .line 69
    iput-boolean v3, p0, Lcom/google/r/b/a/ki;->c:Z

    .line 70
    iput-boolean v3, p0, Lcom/google/r/b/a/ki;->d:Z

    .line 71
    iget-object v0, p0, Lcom/google/r/b/a/ki;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 72
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 78
    invoke-direct {p0}, Lcom/google/r/b/a/ki;-><init>()V

    .line 81
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 84
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 85
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 86
    sparse-switch v4, :sswitch_data_0

    .line 91
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 93
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 89
    goto :goto_0

    .line 98
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 99
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    .line 101
    or-int/lit8 v1, v1, 0x1

    .line 103
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 104
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 103
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 131
    iget-object v1, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    .line 133
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ki;->au:Lcom/google/n/bn;

    throw v0

    .line 108
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/ki;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ki;->a:I

    .line 109
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/ki;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 126
    :catch_1
    move-exception v0

    .line 127
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 128
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 113
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/r/b/a/ki;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/ki;->a:I

    .line 114
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/ki;->d:Z

    goto :goto_0

    .line 118
    :sswitch_4
    iget-object v4, p0, Lcom/google/r/b/a/ki;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 119
    iget v4, p0, Lcom/google/r/b/a/ki;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/ki;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 130
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 131
    iget-object v0, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    .line 133
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ki;->au:Lcom/google/n/bn;

    .line 134
    return-void

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 65
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 226
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ki;->e:Lcom/google/n/ao;

    .line 241
    iput-byte v1, p0, Lcom/google/r/b/a/ki;->g:B

    .line 269
    iput v1, p0, Lcom/google/r/b/a/ki;->h:I

    .line 66
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ki;
    .locals 1

    .prologue
    .line 723
    sget-object v0, Lcom/google/r/b/a/ki;->f:Lcom/google/r/b/a/ki;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/kk;
    .locals 1

    .prologue
    .line 360
    new-instance v0, Lcom/google/r/b/a/kk;

    invoke-direct {v0}, Lcom/google/r/b/a/kk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ki;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    sget-object v0, Lcom/google/r/b/a/ki;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 253
    invoke-virtual {p0}, Lcom/google/r/b/a/ki;->c()I

    .line 254
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 254
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 257
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 258
    iget-boolean v0, p0, Lcom/google/r/b/a/ki;->c:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 260
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 261
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/ki;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 263
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 264
    iget-object v0, p0, Lcom/google/r/b/a/ki;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 266
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/ki;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 267
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 243
    iget-byte v1, p0, Lcom/google/r/b/a/ki;->g:B

    .line 244
    if-ne v1, v0, :cond_0

    .line 248
    :goto_0
    return v0

    .line 245
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ki;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 271
    iget v0, p0, Lcom/google/r/b/a/ki;->h:I

    .line 272
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 293
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 275
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    .line 277
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 275
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 279
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 280
    iget-boolean v0, p0, Lcom/google/r/b/a/ki;->c:Z

    .line 281
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 283
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 284
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/ki;->d:Z

    .line 285
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 287
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 288
    iget-object v0, p0, Lcom/google/r/b/a/ki;->e:Lcom/google/n/ao;

    .line 289
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 291
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/ki;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 292
    iput v0, p0, Lcom/google/r/b/a/ki;->h:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcom/google/r/b/a/ki;->newBuilder()Lcom/google/r/b/a/kk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/kk;->a(Lcom/google/r/b/a/ki;)Lcom/google/r/b/a/kk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcom/google/r/b/a/ki;->newBuilder()Lcom/google/r/b/a/kk;

    move-result-object v0

    return-object v0
.end method
