.class public final enum Lcom/google/r/b/a/vn;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/vn;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/vn;

.field public static final enum b:Lcom/google/r/b/a/vn;

.field private static final synthetic d:[Lcom/google/r/b/a/vn;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 644
    new-instance v0, Lcom/google/r/b/a/vn;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/vn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/vn;->a:Lcom/google/r/b/a/vn;

    .line 648
    new-instance v0, Lcom/google/r/b/a/vn;

    const-string v1, "BACKEND_FAILURE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/vn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/vn;->b:Lcom/google/r/b/a/vn;

    .line 639
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/r/b/a/vn;

    sget-object v1, Lcom/google/r/b/a/vn;->a:Lcom/google/r/b/a/vn;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/vn;->b:Lcom/google/r/b/a/vn;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/r/b/a/vn;->d:[Lcom/google/r/b/a/vn;

    .line 678
    new-instance v0, Lcom/google/r/b/a/vo;

    invoke-direct {v0}, Lcom/google/r/b/a/vo;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 687
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 688
    iput p3, p0, Lcom/google/r/b/a/vn;->c:I

    .line 689
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/vn;
    .locals 1

    .prologue
    .line 666
    packed-switch p0, :pswitch_data_0

    .line 669
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 667
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/vn;->a:Lcom/google/r/b/a/vn;

    goto :goto_0

    .line 668
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/vn;->b:Lcom/google/r/b/a/vn;

    goto :goto_0

    .line 666
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/vn;
    .locals 1

    .prologue
    .line 639
    const-class v0, Lcom/google/r/b/a/vn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/vn;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/vn;
    .locals 1

    .prologue
    .line 639
    sget-object v0, Lcom/google/r/b/a/vn;->d:[Lcom/google/r/b/a/vn;

    invoke-virtual {v0}, [Lcom/google/r/b/a/vn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/vn;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 662
    iget v0, p0, Lcom/google/r/b/a/vn;->c:I

    return v0
.end method
