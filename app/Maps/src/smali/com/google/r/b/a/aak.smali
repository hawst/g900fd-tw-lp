.class public final Lcom/google/r/b/a/aak;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aan;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aak;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/aak;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;

.field public f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1788
    new-instance v0, Lcom/google/r/b/a/aal;

    invoke-direct {v0}, Lcom/google/r/b/a/aal;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aak;->PARSER:Lcom/google/n/ax;

    .line 2032
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aak;->j:Lcom/google/n/aw;

    .line 2556
    new-instance v0, Lcom/google/r/b/a/aak;

    invoke-direct {v0}, Lcom/google/r/b/a/aak;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aak;->g:Lcom/google/r/b/a/aak;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1718
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1805
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aak;->b:Lcom/google/n/ao;

    .line 1947
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aak;->f:Lcom/google/n/ao;

    .line 1962
    iput-byte v2, p0, Lcom/google/r/b/a/aak;->h:B

    .line 1999
    iput v2, p0, Lcom/google/r/b/a/aak;->i:I

    .line 1719
    iget-object v0, p0, Lcom/google/r/b/a/aak;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1720
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aak;->c:Ljava/lang/Object;

    .line 1721
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aak;->d:Ljava/lang/Object;

    .line 1722
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aak;->e:Ljava/lang/Object;

    .line 1723
    iget-object v0, p0, Lcom/google/r/b/a/aak;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1724
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1730
    invoke-direct {p0}, Lcom/google/r/b/a/aak;-><init>()V

    .line 1731
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1736
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1737
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1738
    sparse-switch v3, :sswitch_data_0

    .line 1743
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1745
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1741
    goto :goto_0

    .line 1750
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/aak;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1751
    iget v3, p0, Lcom/google/r/b/a/aak;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aak;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1779
    :catch_0
    move-exception v0

    .line 1780
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1785
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aak;->au:Lcom/google/n/bn;

    throw v0

    .line 1755
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1756
    iget v4, p0, Lcom/google/r/b/a/aak;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/aak;->a:I

    .line 1757
    iput-object v3, p0, Lcom/google/r/b/a/aak;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1781
    :catch_1
    move-exception v0

    .line 1782
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1783
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1761
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1762
    iget v4, p0, Lcom/google/r/b/a/aak;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/aak;->a:I

    .line 1763
    iput-object v3, p0, Lcom/google/r/b/a/aak;->d:Ljava/lang/Object;

    goto :goto_0

    .line 1767
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1768
    iget v4, p0, Lcom/google/r/b/a/aak;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/aak;->a:I

    .line 1769
    iput-object v3, p0, Lcom/google/r/b/a/aak;->e:Ljava/lang/Object;

    goto :goto_0

    .line 1773
    :sswitch_5
    iget-object v3, p0, Lcom/google/r/b/a/aak;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1774
    iget v3, p0, Lcom/google/r/b/a/aak;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/aak;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1785
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aak;->au:Lcom/google/n/bn;

    .line 1786
    return-void

    .line 1738
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1716
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1805
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aak;->b:Lcom/google/n/ao;

    .line 1947
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aak;->f:Lcom/google/n/ao;

    .line 1962
    iput-byte v1, p0, Lcom/google/r/b/a/aak;->h:B

    .line 1999
    iput v1, p0, Lcom/google/r/b/a/aak;->i:I

    .line 1717
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aak;
    .locals 1

    .prologue
    .line 2559
    sget-object v0, Lcom/google/r/b/a/aak;->g:Lcom/google/r/b/a/aak;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aam;
    .locals 1

    .prologue
    .line 2094
    new-instance v0, Lcom/google/r/b/a/aam;

    invoke-direct {v0}, Lcom/google/r/b/a/aam;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1800
    sget-object v0, Lcom/google/r/b/a/aak;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1980
    invoke-virtual {p0}, Lcom/google/r/b/a/aak;->c()I

    .line 1981
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1982
    iget-object v0, p0, Lcom/google/r/b/a/aak;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1984
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1985
    iget-object v0, p0, Lcom/google/r/b/a/aak;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aak;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1987
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 1988
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/aak;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aak;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1990
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1991
    iget-object v0, p0, Lcom/google/r/b/a/aak;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aak;->e:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1993
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1994
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/r/b/a/aak;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1996
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/aak;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1997
    return-void

    .line 1985
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1988
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1991
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1964
    iget-byte v0, p0, Lcom/google/r/b/a/aak;->h:B

    .line 1965
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1975
    :goto_0
    return v0

    .line 1966
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1968
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1969
    iget-object v0, p0, Lcom/google/r/b/a/aak;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aba;->d()Lcom/google/r/b/a/aba;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aba;

    invoke-virtual {v0}, Lcom/google/r/b/a/aba;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1970
    iput-byte v2, p0, Lcom/google/r/b/a/aak;->h:B

    move v0, v2

    .line 1971
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1968
    goto :goto_1

    .line 1974
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/aak;->h:B

    move v0, v1

    .line 1975
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2001
    iget v0, p0, Lcom/google/r/b/a/aak;->i:I

    .line 2002
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2027
    :goto_0
    return v0

    .line 2005
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 2006
    iget-object v0, p0, Lcom/google/r/b/a/aak;->b:Lcom/google/n/ao;

    .line 2007
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 2009
    :goto_1
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 2011
    iget-object v0, p0, Lcom/google/r/b/a/aak;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aak;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 2013
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 2014
    const/4 v3, 0x3

    .line 2015
    iget-object v0, p0, Lcom/google/r/b/a/aak;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aak;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 2017
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 2019
    iget-object v0, p0, Lcom/google/r/b/a/aak;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aak;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 2021
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aak;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 2022
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/r/b/a/aak;->f:Lcom/google/n/ao;

    .line 2023
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 2025
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/aak;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 2026
    iput v0, p0, Lcom/google/r/b/a/aak;->i:I

    goto/16 :goto_0

    .line 2011
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 2015
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 2019
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_8
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1710
    invoke-static {}, Lcom/google/r/b/a/aak;->newBuilder()Lcom/google/r/b/a/aam;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aam;->a(Lcom/google/r/b/a/aak;)Lcom/google/r/b/a/aam;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1710
    invoke-static {}, Lcom/google/r/b/a/aak;->newBuilder()Lcom/google/r/b/a/aam;

    move-result-object v0

    return-object v0
.end method
