.class public final enum Lcom/google/r/b/a/hk;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/hk;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/hk;

.field public static final enum b:Lcom/google/r/b/a/hk;

.field public static final enum c:Lcom/google/r/b/a/hk;

.field public static final enum d:Lcom/google/r/b/a/hk;

.field public static final enum e:Lcom/google/r/b/a/hk;

.field public static final enum f:Lcom/google/r/b/a/hk;

.field public static final enum g:Lcom/google/r/b/a/hk;

.field public static final enum h:Lcom/google/r/b/a/hk;

.field public static final enum i:Lcom/google/r/b/a/hk;

.field private static final synthetic k:[Lcom/google/r/b/a/hk;


# instance fields
.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 107
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->a:Lcom/google/r/b/a/hk;

    .line 111
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "GAIA_ACCOUNT_DISABLE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->b:Lcom/google/r/b/a/hk;

    .line 115
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "GAIA_ACCOUNT_DELETED"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->c:Lcom/google/r/b/a/hk;

    .line 119
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "GAIA_INVALID_COOKIE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->d:Lcom/google/r/b/a/hk;

    .line 123
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "GAIA_SESSION_EXPIRE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->e:Lcom/google/r/b/a/hk;

    .line 127
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "IGNORE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->f:Lcom/google/r/b/a/hk;

    .line 131
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "SCOPE_NOT_PERMITTED"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->g:Lcom/google/r/b/a/hk;

    .line 135
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "SERVER_ERROR"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->h:Lcom/google/r/b/a/hk;

    .line 139
    new-instance v0, Lcom/google/r/b/a/hk;

    const-string v1, "DASHER_ADMIN_DISABLED"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/hk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hk;->i:Lcom/google/r/b/a/hk;

    .line 102
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/r/b/a/hk;

    sget-object v1, Lcom/google/r/b/a/hk;->a:Lcom/google/r/b/a/hk;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/hk;->b:Lcom/google/r/b/a/hk;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/hk;->c:Lcom/google/r/b/a/hk;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/hk;->d:Lcom/google/r/b/a/hk;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/hk;->e:Lcom/google/r/b/a/hk;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/hk;->f:Lcom/google/r/b/a/hk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/hk;->g:Lcom/google/r/b/a/hk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/r/b/a/hk;->h:Lcom/google/r/b/a/hk;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/hk;->i:Lcom/google/r/b/a/hk;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/hk;->k:[Lcom/google/r/b/a/hk;

    .line 204
    new-instance v0, Lcom/google/r/b/a/hl;

    invoke-direct {v0}, Lcom/google/r/b/a/hl;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 213
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 214
    iput p3, p0, Lcom/google/r/b/a/hk;->j:I

    .line 215
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/hk;
    .locals 1

    .prologue
    .line 185
    packed-switch p0, :pswitch_data_0

    .line 195
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 186
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/hk;->a:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 187
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/hk;->b:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 188
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/hk;->c:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 189
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/hk;->d:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 190
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/hk;->e:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 191
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/hk;->f:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 192
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/hk;->g:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 193
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/hk;->h:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 194
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/hk;->i:Lcom/google/r/b/a/hk;

    goto :goto_0

    .line 185
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/hk;
    .locals 1

    .prologue
    .line 102
    const-class v0, Lcom/google/r/b/a/hk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hk;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/hk;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/r/b/a/hk;->k:[Lcom/google/r/b/a/hk;

    invoke-virtual {v0}, [Lcom/google/r/b/a/hk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/hk;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/google/r/b/a/hk;->j:I

    return v0
.end method
