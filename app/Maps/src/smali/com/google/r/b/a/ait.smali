.class public final Lcom/google/r/b/a/ait;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aiw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ait;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/ait;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2032
    new-instance v0, Lcom/google/r/b/a/aiu;

    invoke-direct {v0}, Lcom/google/r/b/a/aiu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ait;->PARSER:Lcom/google/n/ax;

    .line 2198
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ait;->h:Lcom/google/n/aw;

    .line 2532
    new-instance v0, Lcom/google/r/b/a/ait;

    invoke-direct {v0}, Lcom/google/r/b/a/ait;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ait;->e:Lcom/google/r/b/a/ait;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1969
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2148
    iput-byte v0, p0, Lcom/google/r/b/a/ait;->f:B

    .line 2173
    iput v0, p0, Lcom/google/r/b/a/ait;->g:I

    .line 1970
    iput v0, p0, Lcom/google/r/b/a/ait;->b:I

    .line 1971
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ait;->c:Ljava/lang/Object;

    .line 1972
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ait;->d:Ljava/lang/Object;

    .line 1973
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1979
    invoke-direct {p0}, Lcom/google/r/b/a/ait;-><init>()V

    .line 1980
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1984
    const/4 v0, 0x0

    .line 1985
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 1986
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1987
    sparse-switch v3, :sswitch_data_0

    .line 1992
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1994
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1990
    goto :goto_0

    .line 1999
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 2000
    invoke-static {v3}, Lcom/google/maps/g/c/d;->a(I)Lcom/google/maps/g/c/d;

    move-result-object v4

    .line 2001
    if-nez v4, :cond_1

    .line 2002
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2023
    :catch_0
    move-exception v0

    .line 2024
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2029
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ait;->au:Lcom/google/n/bn;

    throw v0

    .line 2004
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/ait;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ait;->a:I

    .line 2005
    iput v3, p0, Lcom/google/r/b/a/ait;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2025
    :catch_1
    move-exception v0

    .line 2026
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2027
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2010
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2011
    iget v4, p0, Lcom/google/r/b/a/ait;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/ait;->a:I

    .line 2012
    iput-object v3, p0, Lcom/google/r/b/a/ait;->c:Ljava/lang/Object;

    goto :goto_0

    .line 2016
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2017
    iget v4, p0, Lcom/google/r/b/a/ait;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/ait;->a:I

    .line 2018
    iput-object v3, p0, Lcom/google/r/b/a/ait;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2029
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ait;->au:Lcom/google/n/bn;

    .line 2030
    return-void

    .line 1987
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1967
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2148
    iput-byte v0, p0, Lcom/google/r/b/a/ait;->f:B

    .line 2173
    iput v0, p0, Lcom/google/r/b/a/ait;->g:I

    .line 1968
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ait;
    .locals 1

    .prologue
    .line 2535
    sget-object v0, Lcom/google/r/b/a/ait;->e:Lcom/google/r/b/a/ait;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aiv;
    .locals 1

    .prologue
    .line 2260
    new-instance v0, Lcom/google/r/b/a/aiv;

    invoke-direct {v0}, Lcom/google/r/b/a/aiv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ait;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2044
    sget-object v0, Lcom/google/r/b/a/ait;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2160
    invoke-virtual {p0}, Lcom/google/r/b/a/ait;->c()I

    .line 2161
    iget v0, p0, Lcom/google/r/b/a/ait;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2162
    iget v0, p0, Lcom/google/r/b/a/ait;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 2164
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ait;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2165
    iget-object v0, p0, Lcom/google/r/b/a/ait;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ait;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2167
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ait;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2168
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/ait;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ait;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2170
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ait;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2171
    return-void

    .line 2165
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 2168
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2150
    iget-byte v1, p0, Lcom/google/r/b/a/ait;->f:B

    .line 2151
    if-ne v1, v0, :cond_0

    .line 2155
    :goto_0
    return v0

    .line 2152
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2154
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ait;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2175
    iget v0, p0, Lcom/google/r/b/a/ait;->g:I

    .line 2176
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2193
    :goto_0
    return v0

    .line 2179
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ait;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 2180
    iget v0, p0, Lcom/google/r/b/a/ait;->b:I

    .line 2181
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 2183
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/ait;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 2185
    iget-object v0, p0, Lcom/google/r/b/a/ait;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ait;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 2187
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ait;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 2188
    const/4 v3, 0x3

    .line 2189
    iget-object v0, p0, Lcom/google/r/b/a/ait;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ait;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 2191
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ait;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 2192
    iput v0, p0, Lcom/google/r/b/a/ait;->g:I

    goto :goto_0

    .line 2181
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    .line 2185
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 2189
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1961
    invoke-static {}, Lcom/google/r/b/a/ait;->newBuilder()Lcom/google/r/b/a/aiv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aiv;->a(Lcom/google/r/b/a/ait;)Lcom/google/r/b/a/aiv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1961
    invoke-static {}, Lcom/google/r/b/a/ait;->newBuilder()Lcom/google/r/b/a/aiv;

    move-result-object v0

    return-object v0
.end method
