.class public final Lcom/google/r/b/a/jx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/kb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/jx;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/jx;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/google/r/b/a/jy;

    invoke-direct {v0}, Lcom/google/r/b/a/jy;-><init>()V

    sput-object v0, Lcom/google/r/b/a/jx;->PARSER:Lcom/google/n/ax;

    .line 260
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/jx;->h:Lcom/google/n/aw;

    .line 568
    new-instance v0, Lcom/google/r/b/a/jx;

    invoke-direct {v0}, Lcom/google/r/b/a/jx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/jx;->e:Lcom/google/r/b/a/jx;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 53
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 126
    iput v1, p0, Lcom/google/r/b/a/jx;->b:I

    .line 207
    iput-byte v0, p0, Lcom/google/r/b/a/jx;->f:B

    .line 234
    iput v0, p0, Lcom/google/r/b/a/jx;->g:I

    .line 54
    iput-boolean v1, p0, Lcom/google/r/b/a/jx;->d:Z

    .line 55
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    invoke-direct {p0}, Lcom/google/r/b/a/jx;-><init>()V

    .line 62
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 67
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 68
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 69
    sparse-switch v0, :sswitch_data_0

    .line 74
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 76
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 72
    goto :goto_0

    .line 81
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/jx;->b:I

    if-eq v0, v8, :cond_1

    .line 82
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 85
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    .line 84
    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 86
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/r/b/a/jx;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/jx;->au:Lcom/google/n/bn;

    throw v0

    .line 90
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/jx;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/jx;->a:I

    .line 91
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/jx;->d:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 103
    :catch_1
    move-exception v0

    .line 104
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 105
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v0, v2

    .line 91
    goto :goto_1

    .line 95
    :sswitch_3
    const/4 v0, 0x5

    :try_start_4
    iput v0, p0, Lcom/google/r/b/a/jx;->b:I

    .line 96
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 107
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/jx;->au:Lcom/google/n/bn;

    .line 108
    return-void

    .line 69
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x20 -> :sswitch_2
        0x28 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 51
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/jx;->b:I

    .line 207
    iput-byte v1, p0, Lcom/google/r/b/a/jx;->f:B

    .line 234
    iput v1, p0, Lcom/google/r/b/a/jx;->g:I

    .line 52
    return-void
.end method

.method public static d()Lcom/google/r/b/a/jx;
    .locals 1

    .prologue
    .line 571
    sget-object v0, Lcom/google/r/b/a/jx;->e:Lcom/google/r/b/a/jx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/jz;
    .locals 1

    .prologue
    .line 322
    new-instance v0, Lcom/google/r/b/a/jz;

    invoke-direct {v0}, Lcom/google/r/b/a/jz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/jx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    sget-object v0, Lcom/google/r/b/a/jx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x4

    const/4 v1, 0x3

    .line 219
    invoke-virtual {p0}, Lcom/google/r/b/a/jx;->c()I

    .line 220
    iget v0, p0, Lcom/google/r/b/a/jx;->b:I

    if-ne v0, v1, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 222
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 221
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 224
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/jx;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v2, :cond_1

    .line 225
    iget-boolean v0, p0, Lcom/google/r/b/a/jx;->d:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 227
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/jx;->b:I

    if-ne v0, v3, :cond_2

    .line 228
    iget-object v0, p0, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    .line 229
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 228
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 231
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/jx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 232
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 209
    iget-byte v1, p0, Lcom/google/r/b/a/jx;->f:B

    .line 210
    if-ne v1, v0, :cond_0

    .line 214
    :goto_0
    return v0

    .line 211
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/jx;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 236
    iget v0, p0, Lcom/google/r/b/a/jx;->g:I

    .line 237
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 255
    :goto_0
    return v0

    .line 240
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/jx;->b:I

    if-ne v0, v3, :cond_3

    .line 241
    iget-object v0, p0, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 242
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 244
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/jx;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_2

    .line 245
    iget-boolean v2, p0, Lcom/google/r/b/a/jx;->d:Z

    .line 246
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v0

    .line 248
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/jx;->b:I

    if-ne v0, v5, :cond_1

    .line 249
    iget-object v0, p0, Lcom/google/r/b/a/jx;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    .line 251
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 250
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/jx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 254
    iput v0, p0, Lcom/google/r/b/a/jx;->g:I

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/r/b/a/jx;->newBuilder()Lcom/google/r/b/a/jz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/jz;->a(Lcom/google/r/b/a/jx;)Lcom/google/r/b/a/jz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/r/b/a/jx;->newBuilder()Lcom/google/r/b/a/jz;

    move-result-object v0

    return-object v0
.end method
