.class public final Lcom/google/r/b/a/agh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/agk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/agd;",
        "Lcom/google/r/b/a/agh;",
        ">;",
        "Lcom/google/r/b/a/agk;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Z

.field public e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 680
    sget-object v0, Lcom/google/r/b/a/agd;->j:Lcom/google/r/b/a/agd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 811
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agh;->b:Lcom/google/n/ao;

    .line 871
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    .line 977
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    .line 1050
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agh;->c:Lcom/google/n/ao;

    .line 681
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 873
    iget v0, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 874
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    .line 875
    iget v0, p0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/agh;->a:I

    .line 877
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 979
    iget v0, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 980
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    .line 981
    iget v0, p0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/agh;->a:I

    .line 983
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 672
    new-instance v2, Lcom/google/r/b/a/agd;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/agd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/agh;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/agh;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/agh;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-boolean v4, p0, Lcom/google/r/b/a/agh;->g:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/agd;->d:Z

    iget v4, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/r/b/a/agh;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v4, v2, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/agh;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/agh;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-boolean v1, p0, Lcom/google/r/b/a/agh;->d:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/agd;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-boolean v1, p0, Lcom/google/r/b/a/agh;->i:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/agd;->h:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-boolean v1, p0, Lcom/google/r/b/a/agh;->e:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/agd;->i:Z

    iput v0, v2, Lcom/google/r/b/a/agd;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 672
    check-cast p1, Lcom/google/r/b/a/agd;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/agh;->a(Lcom/google/r/b/a/agd;)Lcom/google/r/b/a/agh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/agd;)Lcom/google/r/b/a/agh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 754
    invoke-static {}, Lcom/google/r/b/a/agd;->d()Lcom/google/r/b/a/agd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 796
    :goto_0
    return-object p0

    .line 755
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 756
    iget-object v2, p0, Lcom/google/r/b/a/agh;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 757
    iget v2, p0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/agh;->a:I

    .line 759
    :cond_1
    iget-object v2, p1, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 760
    iget-object v2, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 761
    iget-object v2, p1, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    .line 762
    iget v2, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/r/b/a/agh;->a:I

    .line 769
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 770
    iget-boolean v2, p1, Lcom/google/r/b/a/agd;->d:Z

    iget v3, p0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/agh;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agh;->g:Z

    .line 772
    :cond_3
    iget-object v2, p1, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 773
    iget-object v2, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 774
    iget-object v2, p1, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    .line 775
    iget v2, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/r/b/a/agh;->a:I

    .line 782
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 783
    iget-object v2, p0, Lcom/google/r/b/a/agh;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 784
    iget v2, p0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/agh;->a:I

    .line 786
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 787
    iget-boolean v2, p1, Lcom/google/r/b/a/agd;->g:Z

    iget v3, p0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/agh;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agh;->d:Z

    .line 789
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 790
    iget-boolean v2, p1, Lcom/google/r/b/a/agd;->h:Z

    iget v3, p0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/agh;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agh;->i:Z

    .line 792
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 793
    iget-boolean v0, p1, Lcom/google/r/b/a/agd;->i:Z

    iget v1, p0, Lcom/google/r/b/a/agh;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/agh;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/agh;->e:Z

    .line 795
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/agd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 755
    goto/16 :goto_1

    .line 764
    :cond_a
    invoke-direct {p0}, Lcom/google/r/b/a/agh;->c()V

    .line 765
    iget-object v2, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 769
    goto/16 :goto_3

    .line 777
    :cond_c
    invoke-direct {p0}, Lcom/google/r/b/a/agh;->d()V

    .line 778
    iget-object v2, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_d
    move v2, v1

    .line 782
    goto :goto_5

    :cond_e
    move v2, v1

    .line 786
    goto :goto_6

    :cond_f
    move v2, v1

    .line 789
    goto :goto_7

    :cond_10
    move v0, v1

    .line 792
    goto :goto_8
.end method

.method public final a(Lcom/google/r/b/a/agi;)Lcom/google/r/b/a/agh;
    .locals 2

    .prologue
    .line 1020
    if-nez p1, :cond_0

    .line 1021
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1023
    :cond_0
    invoke-direct {p0}, Lcom/google/r/b/a/agh;->d()V

    .line 1024
    iget-object v0, p0, Lcom/google/r/b/a/agh;->h:Ljava/util/List;

    iget v1, p1, Lcom/google/r/b/a/agi;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1026
    return-object p0
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/r/b/a/agh;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/r/b/a/acy;",
            ">;)",
            "Lcom/google/r/b/a/agh;"
        }
    .end annotation

    .prologue
    .line 927
    invoke-direct {p0}, Lcom/google/r/b/a/agh;->c()V

    .line 928
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acy;

    .line 929
    iget-object v2, p0, Lcom/google/r/b/a/agh;->f:Ljava/util/List;

    iget v0, v0, Lcom/google/r/b/a/acy;->t:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 932
    :cond_0
    return-object p0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 800
    iget v0, p0, Lcom/google/r/b/a/agh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 801
    iget-object v0, p0, Lcom/google/r/b/a/agh;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afj;->g()Lcom/google/r/b/a/afj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afj;

    invoke-virtual {v0}, Lcom/google/r/b/a/afj;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 806
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 800
    goto :goto_0

    :cond_1
    move v0, v2

    .line 806
    goto :goto_1
.end method
