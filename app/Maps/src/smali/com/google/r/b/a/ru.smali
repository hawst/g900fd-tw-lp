.class public final enum Lcom/google/r/b/a/ru;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ru;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ru;

.field public static final enum b:Lcom/google/r/b/a/ru;

.field public static final enum c:Lcom/google/r/b/a/ru;

.field public static final enum d:Lcom/google/r/b/a/ru;

.field private static final synthetic f:[Lcom/google/r/b/a/ru;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 137
    new-instance v0, Lcom/google/r/b/a/ru;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/ru;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ru;->a:Lcom/google/r/b/a/ru;

    .line 141
    new-instance v0, Lcom/google/r/b/a/ru;

    const-string v1, "CALL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/ru;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ru;->b:Lcom/google/r/b/a/ru;

    .line 145
    new-instance v0, Lcom/google/r/b/a/ru;

    const-string v1, "SHARE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/ru;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ru;->c:Lcom/google/r/b/a/ru;

    .line 149
    new-instance v0, Lcom/google/r/b/a/ru;

    const-string v1, "PLACE_PAGE_EXPANSION"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/ru;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ru;->d:Lcom/google/r/b/a/ru;

    .line 132
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/ru;

    sget-object v1, Lcom/google/r/b/a/ru;->a:Lcom/google/r/b/a/ru;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/ru;->b:Lcom/google/r/b/a/ru;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/ru;->c:Lcom/google/r/b/a/ru;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/ru;->d:Lcom/google/r/b/a/ru;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/ru;->f:[Lcom/google/r/b/a/ru;

    .line 189
    new-instance v0, Lcom/google/r/b/a/rv;

    invoke-direct {v0}, Lcom/google/r/b/a/rv;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 199
    iput p3, p0, Lcom/google/r/b/a/ru;->e:I

    .line 200
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ru;
    .locals 1

    .prologue
    .line 175
    packed-switch p0, :pswitch_data_0

    .line 180
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 176
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/ru;->a:Lcom/google/r/b/a/ru;

    goto :goto_0

    .line 177
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ru;->b:Lcom/google/r/b/a/ru;

    goto :goto_0

    .line 178
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/ru;->c:Lcom/google/r/b/a/ru;

    goto :goto_0

    .line 179
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/ru;->d:Lcom/google/r/b/a/ru;

    goto :goto_0

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ru;
    .locals 1

    .prologue
    .line 132
    const-class v0, Lcom/google/r/b/a/ru;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ru;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ru;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/google/r/b/a/ru;->f:[Lcom/google/r/b/a/ru;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ru;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ru;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/google/r/b/a/ru;->e:I

    return v0
.end method
