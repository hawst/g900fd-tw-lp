.class public final Lcom/google/r/b/a/ahn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ahq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ahn;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/ahn;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/aq;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/r/b/a/aho;

    invoke-direct {v0}, Lcom/google/r/b/a/aho;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ahn;->PARSER:Lcom/google/n/ax;

    .line 251
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ahn;->g:Lcom/google/n/aw;

    .line 563
    new-instance v0, Lcom/google/r/b/a/ahn;

    invoke-direct {v0}, Lcom/google/r/b/a/ahn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ahn;->d:Lcom/google/r/b/a/ahn;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 59
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 203
    iput-byte v0, p0, Lcom/google/r/b/a/ahn;->e:B

    .line 225
    iput v0, p0, Lcom/google/r/b/a/ahn;->f:I

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ahn;->b:Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    .line 62
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x2

    .line 68
    invoke-direct {p0}, Lcom/google/r/b/a/ahn;-><init>()V

    .line 71
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 74
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 75
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 76
    sparse-switch v4, :sswitch_data_0

    .line 81
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 83
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 79
    goto :goto_0

    .line 88
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 89
    iget v5, p0, Lcom/google/r/b/a/ahn;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/ahn;->a:I

    .line 90
    iput-object v4, p0, Lcom/google/r/b/a/ahn;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 104
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 105
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 111
    iget-object v1, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    .line 113
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ahn;->au:Lcom/google/n/bn;

    throw v0

    .line 94
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 95
    and-int/lit8 v5, v0, 0x2

    if-eq v5, v6, :cond_2

    .line 96
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    .line 97
    or-int/lit8 v0, v0, 0x2

    .line 99
    :cond_2
    iget-object v5, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 106
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 107
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 108
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 110
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 111
    iget-object v0, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    .line 113
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahn;->au:Lcom/google/n/bn;

    .line 114
    return-void

    .line 110
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 76
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 57
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 203
    iput-byte v0, p0, Lcom/google/r/b/a/ahn;->e:B

    .line 225
    iput v0, p0, Lcom/google/r/b/a/ahn;->f:I

    .line 58
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ahn;
    .locals 1

    .prologue
    .line 566
    sget-object v0, Lcom/google/r/b/a/ahn;->d:Lcom/google/r/b/a/ahn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ahp;
    .locals 1

    .prologue
    .line 313
    new-instance v0, Lcom/google/r/b/a/ahp;

    invoke-direct {v0}, Lcom/google/r/b/a/ahp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ahn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    sget-object v0, Lcom/google/r/b/a/ahn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 215
    invoke-virtual {p0}, Lcom/google/r/b/a/ahn;->c()I

    .line 216
    iget v0, p0, Lcom/google/r/b/a/ahn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/r/b/a/ahn;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahn;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 219
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 220
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 217
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ahn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 223
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 205
    iget-byte v1, p0, Lcom/google/r/b/a/ahn;->e:B

    .line 206
    if-ne v1, v0, :cond_0

    .line 210
    :goto_0
    return v0

    .line 207
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 209
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ahn;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 227
    iget v0, p0, Lcom/google/r/b/a/ahn;->f:I

    .line 228
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 246
    :goto_0
    return v0

    .line 231
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ahn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 233
    iget-object v0, p0, Lcom/google/r/b/a/ahn;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahn;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    .line 237
    :goto_3
    iget-object v3, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 238
    iget-object v3, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    .line 239
    invoke-interface {v3, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 237
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 233
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 241
    :cond_2
    add-int/2addr v0, v2

    .line 242
    iget-object v1, p0, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 244
    iget-object v1, p0, Lcom/google/r/b/a/ahn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    iput v0, p0, Lcom/google/r/b/a/ahn;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/google/r/b/a/ahn;->newBuilder()Lcom/google/r/b/a/ahp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ahp;->a(Lcom/google/r/b/a/ahn;)Lcom/google/r/b/a/ahp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/google/r/b/a/ahn;->newBuilder()Lcom/google/r/b/a/ahp;

    move-result-object v0

    return-object v0
.end method
