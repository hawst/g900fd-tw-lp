.class public final Lcom/google/r/b/a/agl;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ags;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/agl;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/agl;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/r/b/a/afn;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/ago;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1347
    new-instance v0, Lcom/google/r/b/a/agm;

    invoke-direct {v0}, Lcom/google/r/b/a/agm;-><init>()V

    sput-object v0, Lcom/google/r/b/a/agl;->PARSER:Lcom/google/n/ax;

    .line 1722
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/agl;->h:Lcom/google/n/aw;

    .line 2197
    new-instance v0, Lcom/google/r/b/a/agl;

    invoke-direct {v0}, Lcom/google/r/b/a/agl;-><init>()V

    sput-object v0, Lcom/google/r/b/a/agl;->e:Lcom/google/r/b/a/agl;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1273
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1666
    iput-byte v0, p0, Lcom/google/r/b/a/agl;->f:B

    .line 1697
    iput v0, p0, Lcom/google/r/b/a/agl;->g:I

    .line 1274
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    .line 1275
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    .line 1276
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    .line 1282
    invoke-direct {p0}, Lcom/google/r/b/a/agl;-><init>()V

    .line 1285
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 1288
    :goto_0
    if-nez v3, :cond_4

    .line 1289
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1290
    sparse-switch v0, :sswitch_data_0

    .line 1295
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_9

    move v3, v4

    .line 1297
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 1293
    goto :goto_0

    .line 1302
    :sswitch_1
    const/4 v0, 0x0

    .line 1303
    iget v2, p0, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_8

    .line 1304
    iget-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    invoke-static {}, Lcom/google/r/b/a/afn;->newBuilder()Lcom/google/r/b/a/afp;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/afp;->a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;

    move-result-object v0

    move-object v2, v0

    .line 1306
    :goto_1
    sget-object v0, Lcom/google/r/b/a/afn;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afn;

    iput-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    .line 1307
    if-eqz v2, :cond_0

    .line 1308
    iget-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/afp;->a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;

    .line 1309
    invoke-virtual {v2}, Lcom/google/r/b/a/afp;->c()Lcom/google/r/b/a/afn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    .line 1311
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/agl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/agl;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1332
    :catch_0
    move-exception v0

    .line 1333
    :goto_2
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v6, :cond_1

    .line 1339
    iget-object v2, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    .line 1341
    :cond_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_2

    .line 1342
    iget-object v1, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    .line 1344
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agl;->au:Lcom/google/n/bn;

    throw v0

    .line 1315
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v6, :cond_3

    .line 1316
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    .line 1317
    or-int/lit8 v1, v1, 0x2

    .line 1319
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/ago;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1334
    :catch_1
    move-exception v0

    .line 1335
    :goto_4
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1336
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1323
    :sswitch_3
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v7, :cond_7

    .line 1324
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1325
    or-int/lit8 v0, v1, 0x4

    .line 1327
    :goto_5
    :try_start_5
    iget-object v1, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/ada;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_6
    move v1, v0

    .line 1331
    goto/16 :goto_0

    .line 1338
    :cond_4
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_5

    .line 1339
    iget-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    .line 1341
    :cond_5
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v7, :cond_6

    .line 1342
    iget-object v0, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    .line 1344
    :cond_6
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agl;->au:Lcom/google/n/bn;

    .line 1345
    return-void

    .line 1338
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_3

    .line 1334
    :catch_2
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_4

    .line 1332
    :catch_3
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_2

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    move-object v2, v0

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto :goto_6

    .line 1290
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1271
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1666
    iput-byte v0, p0, Lcom/google/r/b/a/agl;->f:B

    .line 1697
    iput v0, p0, Lcom/google/r/b/a/agl;->g:I

    .line 1272
    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/google/r/b/a/agl;
    .locals 1

    .prologue
    .line 1764
    sget-object v0, Lcom/google/r/b/a/agl;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, p0}, Lcom/google/n/ax;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/agl;
    .locals 1

    .prologue
    .line 2200
    sget-object v0, Lcom/google/r/b/a/agl;->e:Lcom/google/r/b/a/agl;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/agn;
    .locals 1

    .prologue
    .line 1784
    new-instance v0, Lcom/google/r/b/a/agn;

    invoke-direct {v0}, Lcom/google/r/b/a/agn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/agl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1359
    sget-object v0, Lcom/google/r/b/a/agl;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1684
    invoke-virtual {p0}, Lcom/google/r/b/a/agl;->c()I

    .line 1685
    iget v0, p0, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1686
    iget-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    :cond_0
    move v1, v2

    .line 1688
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1689
    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1688
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1686
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_0

    .line 1691
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1692
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1691
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1694
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/agl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1695
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1668
    iget-byte v0, p0, Lcom/google/r/b/a/agl;->f:B

    .line 1669
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1679
    :goto_0
    return v0

    .line 1670
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1672
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 1673
    iget-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/r/b/a/afn;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1674
    iput-byte v2, p0, Lcom/google/r/b/a/agl;->f:B

    move v0, v2

    .line 1675
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1672
    goto :goto_1

    .line 1673
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_2

    .line 1678
    :cond_4
    iput-byte v1, p0, Lcom/google/r/b/a/agl;->f:B

    move v0, v1

    .line 1679
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1699
    iget v0, p0, Lcom/google/r/b/a/agl;->g:I

    .line 1700
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1717
    :goto_0
    return v0

    .line 1703
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 1705
    iget-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 1707
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1708
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/agl;->c:Ljava/util/List;

    .line 1709
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1707
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1705
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_1

    :cond_2
    move v2, v1

    .line 1711
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1712
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    .line 1713
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1711
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1715
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/agl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1716
    iput v0, p0, Lcom/google/r/b/a/agl;->g:I

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1265
    invoke-static {}, Lcom/google/r/b/a/agl;->newBuilder()Lcom/google/r/b/a/agn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/agn;->a(Lcom/google/r/b/a/agl;)Lcom/google/r/b/a/agn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1265
    invoke-static {}, Lcom/google/r/b/a/agl;->newBuilder()Lcom/google/r/b/a/agn;

    move-result-object v0

    return-object v0
.end method
