.class public final Lcom/google/r/b/a/be;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/bh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/be;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/be;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/google/r/b/a/bf;

    invoke-direct {v0}, Lcom/google/r/b/a/bf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/be;->PARSER:Lcom/google/n/ax;

    .line 298
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/be;->i:Lcom/google/n/aw;

    .line 746
    new-instance v0, Lcom/google/r/b/a/be;

    invoke-direct {v0}, Lcom/google/r/b/a/be;-><init>()V

    sput-object v0, Lcom/google/r/b/a/be;->f:Lcom/google/r/b/a/be;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 230
    iput-byte v0, p0, Lcom/google/r/b/a/be;->g:B

    .line 264
    iput v0, p0, Lcom/google/r/b/a/be;->h:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    .line 19
    iput v1, p0, Lcom/google/r/b/a/be;->c:I

    .line 20
    iput v1, p0, Lcom/google/r/b/a/be;->d:I

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const v9, 0x7fffffff

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/16 v8, 0x8

    const/4 v4, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/r/b/a/be;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 34
    :cond_0
    :goto_0
    if-nez v3, :cond_b

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 43
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v4, :cond_1

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    .line 51
    or-int/lit8 v1, v1, 0x1

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 53
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_2

    .line 109
    iget-object v2, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    .line 111
    :cond_2
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v8, :cond_3

    .line 112
    iget-object v1, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    .line 114
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/be;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 59
    invoke-static {v0}, Lcom/google/r/b/a/ca;->a(I)Lcom/google/r/b/a/ca;

    move-result-object v6

    .line 60
    if-nez v6, :cond_4

    .line 61
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 104
    :catch_1
    move-exception v0

    .line 105
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 106
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :cond_4
    :try_start_4
    iget v6, p0, Lcom/google/r/b/a/be;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/r/b/a/be;->a:I

    .line 64
    iput v0, p0, Lcom/google/r/b/a/be;->c:I

    goto :goto_0

    .line 69
    :sswitch_3
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v8, :cond_5

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    .line 71
    or-int/lit8 v1, v1, 0x8

    .line 73
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 77
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 78
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 79
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v8, :cond_6

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_7

    move v0, v2

    :goto_1
    if-lez v0, :cond_6

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    .line 81
    or-int/lit8 v1, v1, 0x8

    .line 83
    :cond_6
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_8

    move v0, v2

    :goto_3
    if-lez v0, :cond_9

    .line 84
    iget-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 79
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 83
    :cond_8
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 86
    :cond_9
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 90
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 91
    invoke-static {v0}, Lcom/google/r/b/a/df;->a(I)Lcom/google/r/b/a/df;

    move-result-object v6

    .line 92
    if-nez v6, :cond_a

    .line 93
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 95
    :cond_a
    iget v6, p0, Lcom/google/r/b/a/be;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/r/b/a/be;->a:I

    .line 96
    iput v0, p0, Lcom/google/r/b/a/be;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 108
    :cond_b
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v4, :cond_c

    .line 109
    iget-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    .line 111
    :cond_c
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v8, :cond_d

    .line 112
    iget-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    .line 114
    :cond_d
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/be;->au:Lcom/google/n/bn;

    .line 115
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 230
    iput-byte v0, p0, Lcom/google/r/b/a/be;->g:B

    .line 264
    iput v0, p0, Lcom/google/r/b/a/be;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/be;
    .locals 1

    .prologue
    .line 749
    sget-object v0, Lcom/google/r/b/a/be;->f:Lcom/google/r/b/a/be;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/bg;
    .locals 1

    .prologue
    .line 360
    new-instance v0, Lcom/google/r/b/a/bg;

    invoke-direct {v0}, Lcom/google/r/b/a/bg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/be;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    sget-object v0, Lcom/google/r/b/a/be;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 248
    invoke-virtual {p0}, Lcom/google/r/b/a/be;->c()I

    move v1, v2

    .line 249
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 249
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 252
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/be;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 253
    iget v0, p0, Lcom/google/r/b/a/be;->c:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(II)V

    .line 255
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 256
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 255
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 258
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/be;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 259
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/r/b/a/be;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/be;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 262
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 232
    iget-byte v0, p0, Lcom/google/r/b/a/be;->g:B

    .line 233
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 243
    :cond_0
    :goto_0
    return v2

    .line 234
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 236
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 237
    iget-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/im;->d()Lcom/google/r/b/a/im;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/im;

    invoke-virtual {v0}, Lcom/google/r/b/a/im;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 238
    iput-byte v2, p0, Lcom/google/r/b/a/be;->g:B

    goto :goto_0

    .line 236
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 242
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/be;->g:B

    move v2, v3

    .line 243
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 266
    iget v0, p0, Lcom/google/r/b/a/be;->h:I

    .line 267
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 293
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 270
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    .line 272
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 270
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 274
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/be;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_2

    .line 275
    iget v0, p0, Lcom/google/r/b/a/be;->c:I

    .line 276
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_2
    move v1, v2

    move v5, v2

    .line 280
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 281
    iget-object v0, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    .line 282
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v5, v0

    .line 280
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v4

    .line 276
    goto :goto_2

    :cond_4
    move v0, v4

    .line 282
    goto :goto_4

    .line 284
    :cond_5
    add-int v0, v3, v5

    .line 285
    iget-object v1, p0, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 287
    iget v1, p0, Lcom/google/r/b/a/be;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_7

    .line 288
    const/4 v1, 0x4

    iget v3, p0, Lcom/google/r/b/a/be;->d:I

    .line 289
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_6
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 291
    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/be;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 292
    iput v0, p0, Lcom/google/r/b/a/be;->h:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/be;->newBuilder()Lcom/google/r/b/a/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/bg;->a(Lcom/google/r/b/a/be;)Lcom/google/r/b/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/be;->newBuilder()Lcom/google/r/b/a/bg;

    move-result-object v0

    return-object v0
.end method
