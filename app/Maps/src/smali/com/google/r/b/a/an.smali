.class public final Lcom/google/r/b/a/an;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/al;",
        "Lcom/google/r/b/a/an;",
        ">;",
        "Lcom/google/r/b/a/aq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:I

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 337
    sget-object v0, Lcom/google/r/b/a/al;->e:Lcom/google/r/b/a/al;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 427
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/an;->c:I

    .line 338
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 329
    new-instance v2, Lcom/google/r/b/a/al;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/al;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/an;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/an;->b:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/al;->b:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/an;->c:I

    iput v1, v2, Lcom/google/r/b/a/al;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/r/b/a/an;->d:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/al;->d:Z

    iput v0, v2, Lcom/google/r/b/a/al;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 329
    check-cast p1, Lcom/google/r/b/a/al;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/an;->a(Lcom/google/r/b/a/al;)Lcom/google/r/b/a/an;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/al;)Lcom/google/r/b/a/an;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 375
    invoke-static {}, Lcom/google/r/b/a/al;->d()Lcom/google/r/b/a/al;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 386
    :goto_0
    return-object p0

    .line 376
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 377
    iget-boolean v2, p1, Lcom/google/r/b/a/al;->b:Z

    iget v3, p0, Lcom/google/r/b/a/an;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/an;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/an;->b:Z

    .line 379
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 380
    iget v2, p1, Lcom/google/r/b/a/al;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/ao;->a(I)Lcom/google/r/b/a/ao;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/r/b/a/ao;->a:Lcom/google/r/b/a/ao;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 376
    goto :goto_1

    :cond_4
    move v2, v1

    .line 379
    goto :goto_2

    .line 380
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/an;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/an;->a:I

    iget v2, v2, Lcom/google/r/b/a/ao;->d:I

    iput v2, p0, Lcom/google/r/b/a/an;->c:I

    .line 382
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_7

    .line 383
    iget-boolean v0, p1, Lcom/google/r/b/a/al;->d:Z

    iget v1, p0, Lcom/google/r/b/a/an;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/an;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/an;->d:Z

    .line 385
    :cond_7
    iget-object v0, p1, Lcom/google/r/b/a/al;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_8
    move v0, v1

    .line 382
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x1

    return v0
.end method
