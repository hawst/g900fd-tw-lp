.class public final Lcom/google/r/b/a/lh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/lk;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/lh;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/lh;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5763
    new-instance v0, Lcom/google/r/b/a/li;

    invoke-direct {v0}, Lcom/google/r/b/a/li;-><init>()V

    sput-object v0, Lcom/google/r/b/a/lh;->PARSER:Lcom/google/n/ax;

    .line 5889
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/lh;->h:Lcom/google/n/aw;

    .line 6229
    new-instance v0, Lcom/google/r/b/a/lh;

    invoke-direct {v0}, Lcom/google/r/b/a/lh;-><init>()V

    sput-object v0, Lcom/google/r/b/a/lh;->e:Lcom/google/r/b/a/lh;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5708
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5780
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    .line 5796
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    .line 5812
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    .line 5827
    iput-byte v3, p0, Lcom/google/r/b/a/lh;->f:B

    .line 5864
    iput v3, p0, Lcom/google/r/b/a/lh;->g:I

    .line 5709
    iget-object v0, p0, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 5710
    iget-object v0, p0, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 5711
    iget-object v0, p0, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 5712
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5718
    invoke-direct {p0}, Lcom/google/r/b/a/lh;-><init>()V

    .line 5719
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 5724
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 5725
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 5726
    sparse-switch v3, :sswitch_data_0

    .line 5731
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 5733
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 5729
    goto :goto_0

    .line 5738
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 5739
    iget v3, p0, Lcom/google/r/b/a/lh;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/lh;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 5754
    :catch_0
    move-exception v0

    .line 5755
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5760
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/lh;->au:Lcom/google/n/bn;

    throw v0

    .line 5743
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 5744
    iget v3, p0, Lcom/google/r/b/a/lh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/lh;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 5756
    :catch_1
    move-exception v0

    .line 5757
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 5758
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5748
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 5749
    iget v3, p0, Lcom/google/r/b/a/lh;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/lh;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 5760
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lh;->au:Lcom/google/n/bn;

    .line 5761
    return-void

    .line 5726
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 5706
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5780
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    .line 5796
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    .line 5812
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    .line 5827
    iput-byte v1, p0, Lcom/google/r/b/a/lh;->f:B

    .line 5864
    iput v1, p0, Lcom/google/r/b/a/lh;->g:I

    .line 5707
    return-void
.end method

.method public static d()Lcom/google/r/b/a/lh;
    .locals 1

    .prologue
    .line 6232
    sget-object v0, Lcom/google/r/b/a/lh;->e:Lcom/google/r/b/a/lh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/lj;
    .locals 1

    .prologue
    .line 5951
    new-instance v0, Lcom/google/r/b/a/lj;

    invoke-direct {v0}, Lcom/google/r/b/a/lj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/lh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5775
    sget-object v0, Lcom/google/r/b/a/lh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5851
    invoke-virtual {p0}, Lcom/google/r/b/a/lh;->c()I

    .line 5852
    iget v0, p0, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_0

    .line 5853
    iget-object v0, p0, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5855
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 5856
    iget-object v0, p0, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5858
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    .line 5859
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5861
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/lh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5862
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5829
    iget-byte v0, p0, Lcom/google/r/b/a/lh;->f:B

    .line 5830
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 5846
    :goto_0
    return v0

    .line 5831
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 5833
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 5834
    iget-object v0, p0, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/be;->d()Lcom/google/r/b/a/be;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 5835
    iput-byte v2, p0, Lcom/google/r/b/a/lh;->f:B

    move v0, v2

    .line 5836
    goto :goto_0

    :cond_2
    move v0, v2

    .line 5833
    goto :goto_1

    .line 5839
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 5840
    iget-object v0, p0, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/lt;->d()Lcom/google/r/b/a/lt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/lt;

    invoke-virtual {v0}, Lcom/google/r/b/a/lt;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5841
    iput-byte v2, p0, Lcom/google/r/b/a/lh;->f:B

    move v0, v2

    .line 5842
    goto :goto_0

    :cond_4
    move v0, v2

    .line 5839
    goto :goto_2

    .line 5845
    :cond_5
    iput-byte v1, p0, Lcom/google/r/b/a/lh;->f:B

    move v0, v1

    .line 5846
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 5866
    iget v0, p0, Lcom/google/r/b/a/lh;->g:I

    .line 5867
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5884
    :goto_0
    return v0

    .line 5870
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_3

    .line 5871
    iget-object v0, p0, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    .line 5872
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 5874
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 5875
    iget-object v2, p0, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    .line 5876
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 5878
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_2

    .line 5879
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    .line 5880
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 5882
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/lh;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 5883
    iput v0, p0, Lcom/google/r/b/a/lh;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5700
    invoke-static {}, Lcom/google/r/b/a/lh;->newBuilder()Lcom/google/r/b/a/lj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/lj;->a(Lcom/google/r/b/a/lh;)Lcom/google/r/b/a/lj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5700
    invoke-static {}, Lcom/google/r/b/a/lh;->newBuilder()Lcom/google/r/b/a/lj;

    move-result-object v0

    return-object v0
.end method
