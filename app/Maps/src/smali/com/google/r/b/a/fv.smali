.class public final Lcom/google/r/b/a/fv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/fw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/fr;",
        "Lcom/google/r/b/a/fv;",
        ">;",
        "Lcom/google/r/b/a/fw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5063
    sget-object v0, Lcom/google/r/b/a/fr;->d:Lcom/google/r/b/a/fr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5114
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/fv;->b:I

    .line 5150
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/fv;->c:Ljava/lang/Object;

    .line 5064
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5055
    new-instance v2, Lcom/google/r/b/a/fr;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/fr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/fv;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/fv;->b:I

    iput v1, v2, Lcom/google/r/b/a/fr;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/fv;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/fr;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/fr;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5055
    check-cast p1, Lcom/google/r/b/a/fr;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/fv;->a(Lcom/google/r/b/a/fr;)Lcom/google/r/b/a/fv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/fr;)Lcom/google/r/b/a/fv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 5095
    invoke-static {}, Lcom/google/r/b/a/fr;->d()Lcom/google/r/b/a/fr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 5105
    :goto_0
    return-object p0

    .line 5096
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/fr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 5097
    iget v2, p1, Lcom/google/r/b/a/fr;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/ft;->a(I)Lcom/google/r/b/a/ft;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 5096
    goto :goto_1

    .line 5097
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/fv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/fv;->a:I

    iget v2, v2, Lcom/google/r/b/a/ft;->o:I

    iput v2, p0, Lcom/google/r/b/a/fv;->b:I

    .line 5099
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/fr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 5100
    iget v0, p0, Lcom/google/r/b/a/fv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/fv;->a:I

    .line 5101
    iget-object v0, p1, Lcom/google/r/b/a/fr;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/fv;->c:Ljava/lang/Object;

    .line 5104
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/fr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 5099
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 5109
    const/4 v0, 0x1

    return v0
.end method
