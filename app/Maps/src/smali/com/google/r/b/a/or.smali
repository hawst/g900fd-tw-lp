.class public final enum Lcom/google/r/b/a/or;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/or;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/or;

.field public static final enum b:Lcom/google/r/b/a/or;

.field public static final enum c:Lcom/google/r/b/a/or;

.field public static final enum d:Lcom/google/r/b/a/or;

.field public static final enum e:Lcom/google/r/b/a/or;

.field public static final enum f:Lcom/google/r/b/a/or;

.field private static final synthetic h:[Lcom/google/r/b/a/or;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 189
    new-instance v0, Lcom/google/r/b/a/or;

    const-string v1, "UNKNOWN_VIEW_MODE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/or;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/or;->a:Lcom/google/r/b/a/or;

    .line 193
    new-instance v0, Lcom/google/r/b/a/or;

    const-string v1, "FAR"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/or;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/or;->b:Lcom/google/r/b/a/or;

    .line 197
    new-instance v0, Lcom/google/r/b/a/or;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/or;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    .line 201
    new-instance v0, Lcom/google/r/b/a/or;

    const-string v1, "APPROACH"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/r/b/a/or;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/or;->d:Lcom/google/r/b/a/or;

    .line 205
    new-instance v0, Lcom/google/r/b/a/or;

    const-string v1, "INSPECT_STEP"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/r/b/a/or;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/or;->e:Lcom/google/r/b/a/or;

    .line 209
    new-instance v0, Lcom/google/r/b/a/or;

    const-string v1, "INSPECT_ROUTE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/or;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/or;->f:Lcom/google/r/b/a/or;

    .line 184
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/r/b/a/or;

    sget-object v1, Lcom/google/r/b/a/or;->a:Lcom/google/r/b/a/or;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/or;->b:Lcom/google/r/b/a/or;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/or;->d:Lcom/google/r/b/a/or;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/or;->e:Lcom/google/r/b/a/or;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/or;->f:Lcom/google/r/b/a/or;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/or;->h:[Lcom/google/r/b/a/or;

    .line 259
    new-instance v0, Lcom/google/r/b/a/os;

    invoke-direct {v0}, Lcom/google/r/b/a/os;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 268
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 269
    iput p3, p0, Lcom/google/r/b/a/or;->g:I

    .line 270
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/or;
    .locals 1

    .prologue
    .line 243
    packed-switch p0, :pswitch_data_0

    .line 250
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 244
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/or;->a:Lcom/google/r/b/a/or;

    goto :goto_0

    .line 245
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/or;->b:Lcom/google/r/b/a/or;

    goto :goto_0

    .line 246
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/or;->c:Lcom/google/r/b/a/or;

    goto :goto_0

    .line 247
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/or;->d:Lcom/google/r/b/a/or;

    goto :goto_0

    .line 248
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/or;->e:Lcom/google/r/b/a/or;

    goto :goto_0

    .line 249
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/or;->f:Lcom/google/r/b/a/or;

    goto :goto_0

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/or;
    .locals 1

    .prologue
    .line 184
    const-class v0, Lcom/google/r/b/a/or;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/or;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/or;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/google/r/b/a/or;->h:[Lcom/google/r/b/a/or;

    invoke-virtual {v0}, [Lcom/google/r/b/a/or;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/or;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/google/r/b/a/or;->g:I

    return v0
.end method
