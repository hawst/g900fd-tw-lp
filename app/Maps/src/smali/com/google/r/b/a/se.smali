.class public final enum Lcom/google/r/b/a/se;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/se;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/se;

.field public static final enum b:Lcom/google/r/b/a/se;

.field public static final enum c:Lcom/google/r/b/a/se;

.field public static final enum d:Lcom/google/r/b/a/se;

.field public static final enum e:Lcom/google/r/b/a/se;

.field private static final synthetic g:[Lcom/google/r/b/a/se;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 101
    new-instance v0, Lcom/google/r/b/a/se;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/se;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/se;->a:Lcom/google/r/b/a/se;

    .line 105
    new-instance v0, Lcom/google/r/b/a/se;

    const-string v1, "GRIPPER_1"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/se;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/se;->b:Lcom/google/r/b/a/se;

    .line 109
    new-instance v0, Lcom/google/r/b/a/se;

    const-string v1, "GRIPPER_2"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/se;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/se;->c:Lcom/google/r/b/a/se;

    .line 113
    new-instance v0, Lcom/google/r/b/a/se;

    const-string v1, "GRIPPER_3"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/se;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/se;->d:Lcom/google/r/b/a/se;

    .line 117
    new-instance v0, Lcom/google/r/b/a/se;

    const-string v1, "GRIPPER_4"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/se;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/se;->e:Lcom/google/r/b/a/se;

    .line 96
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/r/b/a/se;

    sget-object v1, Lcom/google/r/b/a/se;->a:Lcom/google/r/b/a/se;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/se;->b:Lcom/google/r/b/a/se;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/se;->c:Lcom/google/r/b/a/se;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/se;->d:Lcom/google/r/b/a/se;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/se;->e:Lcom/google/r/b/a/se;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/r/b/a/se;->g:[Lcom/google/r/b/a/se;

    .line 162
    new-instance v0, Lcom/google/r/b/a/sf;

    invoke-direct {v0}, Lcom/google/r/b/a/sf;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 171
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 172
    iput p3, p0, Lcom/google/r/b/a/se;->f:I

    .line 173
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/se;
    .locals 1

    .prologue
    .line 147
    packed-switch p0, :pswitch_data_0

    .line 153
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 148
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/se;->a:Lcom/google/r/b/a/se;

    goto :goto_0

    .line 149
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/se;->b:Lcom/google/r/b/a/se;

    goto :goto_0

    .line 150
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/se;->c:Lcom/google/r/b/a/se;

    goto :goto_0

    .line 151
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/se;->d:Lcom/google/r/b/a/se;

    goto :goto_0

    .line 152
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/se;->e:Lcom/google/r/b/a/se;

    goto :goto_0

    .line 147
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/se;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/google/r/b/a/se;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/se;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/se;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/google/r/b/a/se;->g:[Lcom/google/r/b/a/se;

    invoke-virtual {v0}, [Lcom/google/r/b/a/se;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/se;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/google/r/b/a/se;->f:I

    return v0
.end method
