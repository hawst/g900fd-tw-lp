.class public final Lcom/google/r/b/a/aba;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/abd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aba;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/r/b/a/aba;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field public g:Ljava/lang/Object;

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/google/n/ao;

.field j:Z

.field public k:Ljava/lang/Object;

.field public l:Lcom/google/n/ao;

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3411
    new-instance v0, Lcom/google/r/b/a/abb;

    invoke-direct {v0}, Lcom/google/r/b/a/abb;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aba;->PARSER:Lcom/google/n/ax;

    .line 4115
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aba;->p:Lcom/google/n/aw;

    .line 5166
    new-instance v0, Lcom/google/r/b/a/aba;

    invoke-direct {v0}, Lcom/google/r/b/a/aba;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aba;->m:Lcom/google/r/b/a/aba;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3294
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3646
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aba;->b:Lcom/google/n/ao;

    .line 3915
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aba;->i:Lcom/google/n/ao;

    .line 3988
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aba;->l:Lcom/google/n/ao;

    .line 4003
    iput-byte v3, p0, Lcom/google/r/b/a/aba;->n:B

    .line 4058
    iput v3, p0, Lcom/google/r/b/a/aba;->o:I

    .line 3295
    iget-object v0, p0, Lcom/google/r/b/a/aba;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3296
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aba;->c:Ljava/lang/Object;

    .line 3297
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aba;->d:Ljava/lang/Object;

    .line 3298
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aba;->e:Ljava/lang/Object;

    .line 3299
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aba;->f:Ljava/lang/Object;

    .line 3300
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aba;->g:Ljava/lang/Object;

    .line 3301
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    .line 3302
    iget-object v0, p0, Lcom/google/r/b/a/aba;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3303
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/aba;->j:Z

    .line 3304
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aba;->k:Ljava/lang/Object;

    .line 3305
    iget-object v0, p0, Lcom/google/r/b/a/aba;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3306
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x40

    const/4 v0, 0x0

    .line 3312
    invoke-direct {p0}, Lcom/google/r/b/a/aba;-><init>()V

    .line 3315
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 3318
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 3319
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 3320
    sparse-switch v4, :sswitch_data_0

    .line 3325
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 3327
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 3323
    goto :goto_0

    .line 3332
    :sswitch_1
    iget-object v4, p0, Lcom/google/r/b/a/aba;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 3333
    iget v4, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/aba;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3399
    :catch_0
    move-exception v0

    .line 3400
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3405
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x40

    if-ne v1, v7, :cond_1

    .line 3406
    iget-object v1, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    .line 3408
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aba;->au:Lcom/google/n/bn;

    throw v0

    .line 3337
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 3338
    iget v5, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/aba;->a:I

    .line 3339
    iput-object v4, p0, Lcom/google/r/b/a/aba;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3401
    :catch_1
    move-exception v0

    .line 3402
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 3403
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3343
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 3344
    iget v5, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/aba;->a:I

    .line 3345
    iput-object v4, p0, Lcom/google/r/b/a/aba;->d:Ljava/lang/Object;

    goto :goto_0

    .line 3349
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 3350
    iget v5, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/r/b/a/aba;->a:I

    .line 3351
    iput-object v4, p0, Lcom/google/r/b/a/aba;->e:Ljava/lang/Object;

    goto :goto_0

    .line 3355
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 3356
    iget v5, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/r/b/a/aba;->a:I

    .line 3357
    iput-object v4, p0, Lcom/google/r/b/a/aba;->f:Ljava/lang/Object;

    goto :goto_0

    .line 3361
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 3362
    iget v5, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/r/b/a/aba;->a:I

    .line 3363
    iput-object v4, p0, Lcom/google/r/b/a/aba;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 3367
    :sswitch_7
    and-int/lit8 v4, v1, 0x40

    if-eq v4, v7, :cond_2

    .line 3368
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    .line 3370
    or-int/lit8 v1, v1, 0x40

    .line 3372
    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 3373
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 3372
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 3377
    :sswitch_8
    iget-object v4, p0, Lcom/google/r/b/a/aba;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 3378
    iget v4, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/aba;->a:I

    goto/16 :goto_0

    .line 3382
    :sswitch_9
    iget v4, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/r/b/a/aba;->a:I

    .line 3383
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/aba;->j:Z

    goto/16 :goto_0

    .line 3387
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 3388
    iget v5, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/r/b/a/aba;->a:I

    .line 3389
    iput-object v4, p0, Lcom/google/r/b/a/aba;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 3393
    :sswitch_b
    iget-object v4, p0, Lcom/google/r/b/a/aba;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 3394
    iget v4, p0, Lcom/google/r/b/a/aba;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/r/b/a/aba;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 3405
    :cond_3
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v7, :cond_4

    .line 3406
    iget-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    .line 3408
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->au:Lcom/google/n/bn;

    .line 3409
    return-void

    .line 3320
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 3292
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3646
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aba;->b:Lcom/google/n/ao;

    .line 3915
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aba;->i:Lcom/google/n/ao;

    .line 3988
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aba;->l:Lcom/google/n/ao;

    .line 4003
    iput-byte v1, p0, Lcom/google/r/b/a/aba;->n:B

    .line 4058
    iput v1, p0, Lcom/google/r/b/a/aba;->o:I

    .line 3293
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aba;
    .locals 1

    .prologue
    .line 5169
    sget-object v0, Lcom/google/r/b/a/aba;->m:Lcom/google/r/b/a/aba;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/abc;
    .locals 1

    .prologue
    .line 4177
    new-instance v0, Lcom/google/r/b/a/abc;

    invoke-direct {v0}, Lcom/google/r/b/a/abc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aba;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3423
    sget-object v0, Lcom/google/r/b/a/aba;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4021
    invoke-virtual {p0}, Lcom/google/r/b/a/aba;->c()I

    .line 4022
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4023
    iget-object v0, p0, Lcom/google/r/b/a/aba;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4025
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4026
    iget-object v0, p0, Lcom/google/r/b/a/aba;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4028
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 4029
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/aba;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4031
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 4032
    iget-object v0, p0, Lcom/google/r/b/a/aba;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->e:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4034
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 4035
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/aba;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->f:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4037
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 4038
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/aba;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->g:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4040
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 4041
    const/4 v2, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4040
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 4026
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 4029
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 4032
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 4035
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 4038
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 4043
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_c

    .line 4044
    iget-object v0, p0, Lcom/google/r/b/a/aba;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4046
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_d

    .line 4047
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/aba;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 4049
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_e

    .line 4050
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/aba;->k:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->k:Ljava/lang/Object;

    :goto_6
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4052
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_f

    .line 4053
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/r/b/a/aba;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4055
    :cond_f
    iget-object v0, p0, Lcom/google/r/b/a/aba;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4056
    return-void

    .line 4050
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_6
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4005
    iget-byte v0, p0, Lcom/google/r/b/a/aba;->n:B

    .line 4006
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 4016
    :cond_0
    :goto_0
    return v2

    .line 4007
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 4009
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 4010
    iget-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/zy;->d()Lcom/google/r/b/a/zy;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/zy;

    invoke-virtual {v0}, Lcom/google/r/b/a/zy;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4011
    iput-byte v2, p0, Lcom/google/r/b/a/aba;->n:B

    goto :goto_0

    .line 4009
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 4015
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/aba;->n:B

    move v2, v3

    .line 4016
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4060
    iget v0, p0, Lcom/google/r/b/a/aba;->o:I

    .line 4061
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4110
    :goto_0
    return v0

    .line 4064
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_11

    .line 4065
    iget-object v0, p0, Lcom/google/r/b/a/aba;->b:Lcom/google/n/ao;

    .line 4066
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 4068
    :goto_1
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 4070
    iget-object v0, p0, Lcom/google/r/b/a/aba;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 4072
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 4073
    const/4 v3, 0x3

    .line 4074
    iget-object v0, p0, Lcom/google/r/b/a/aba;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 4076
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 4078
    iget-object v0, p0, Lcom/google/r/b/a/aba;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 4080
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 4081
    const/4 v3, 0x5

    .line 4082
    iget-object v0, p0, Lcom/google/r/b/a/aba;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 4084
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 4085
    const/4 v3, 0x6

    .line 4086
    iget-object v0, p0, Lcom/google/r/b/a/aba;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_5
    move v3, v1

    move v1, v2

    .line 4088
    :goto_7
    iget-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 4089
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    .line 4090
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 4088
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 4070
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 4074
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 4078
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 4082
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 4086
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 4092
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_c

    .line 4093
    iget-object v0, p0, Lcom/google/r/b/a/aba;->i:Lcom/google/n/ao;

    .line 4094
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 4096
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_d

    .line 4097
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/aba;->j:Z

    .line 4098
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 4100
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_e

    .line 4101
    const/16 v1, 0xa

    .line 4102
    iget-object v0, p0, Lcom/google/r/b/a/aba;->k:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aba;->k:Ljava/lang/Object;

    :goto_8
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 4104
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_f

    .line 4105
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/r/b/a/aba;->l:Lcom/google/n/ao;

    .line 4106
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 4108
    :cond_f
    iget-object v0, p0, Lcom/google/r/b/a/aba;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 4109
    iput v0, p0, Lcom/google/r/b/a/aba;->o:I

    goto/16 :goto_0

    .line 4102
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_11
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3286
    invoke-static {}, Lcom/google/r/b/a/aba;->newBuilder()Lcom/google/r/b/a/abc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/abc;->a(Lcom/google/r/b/a/aba;)Lcom/google/r/b/a/abc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3286
    invoke-static {}, Lcom/google/r/b/a/aba;->newBuilder()Lcom/google/r/b/a/abc;

    move-result-object v0

    return-object v0
.end method
