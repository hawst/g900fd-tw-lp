.class public final enum Lcom/google/r/b/a/cy;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/cy;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/cy;

.field public static final enum b:Lcom/google/r/b/a/cy;

.field public static final enum c:Lcom/google/r/b/a/cy;

.field public static final enum d:Lcom/google/r/b/a/cy;

.field private static final synthetic f:[Lcom/google/r/b/a/cy;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5700
    new-instance v0, Lcom/google/r/b/a/cy;

    const-string v1, "LIST_VIEW_FULL_PAGE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/cy;->a:Lcom/google/r/b/a/cy;

    .line 5704
    new-instance v0, Lcom/google/r/b/a/cy;

    const-string v1, "LIST_VIEW_COLLAPSED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/cy;->b:Lcom/google/r/b/a/cy;

    .line 5708
    new-instance v0, Lcom/google/r/b/a/cy;

    const-string v1, "LIST_VIEW_PARTIALLY_EXPANDED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/cy;->c:Lcom/google/r/b/a/cy;

    .line 5712
    new-instance v0, Lcom/google/r/b/a/cy;

    const-string v1, "LIST_VIEW_FULLY_EXPANDED"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/cy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/cy;->d:Lcom/google/r/b/a/cy;

    .line 5695
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/cy;

    sget-object v1, Lcom/google/r/b/a/cy;->a:Lcom/google/r/b/a/cy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/cy;->b:Lcom/google/r/b/a/cy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/cy;->c:Lcom/google/r/b/a/cy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/cy;->d:Lcom/google/r/b/a/cy;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/cy;->f:[Lcom/google/r/b/a/cy;

    .line 5752
    new-instance v0, Lcom/google/r/b/a/cz;

    invoke-direct {v0}, Lcom/google/r/b/a/cz;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 5761
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5762
    iput p3, p0, Lcom/google/r/b/a/cy;->e:I

    .line 5763
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/cy;
    .locals 1

    .prologue
    .line 5738
    packed-switch p0, :pswitch_data_0

    .line 5743
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 5739
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/cy;->a:Lcom/google/r/b/a/cy;

    goto :goto_0

    .line 5740
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/cy;->b:Lcom/google/r/b/a/cy;

    goto :goto_0

    .line 5741
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/cy;->c:Lcom/google/r/b/a/cy;

    goto :goto_0

    .line 5742
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/cy;->d:Lcom/google/r/b/a/cy;

    goto :goto_0

    .line 5738
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/cy;
    .locals 1

    .prologue
    .line 5695
    const-class v0, Lcom/google/r/b/a/cy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/cy;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/cy;
    .locals 1

    .prologue
    .line 5695
    sget-object v0, Lcom/google/r/b/a/cy;->f:[Lcom/google/r/b/a/cy;

    invoke-virtual {v0}, [Lcom/google/r/b/a/cy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/cy;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 5734
    iget v0, p0, Lcom/google/r/b/a/cy;->e:I

    return v0
.end method
