.class public final Lcom/google/r/b/a/po;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/pr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/po;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/r/b/a/po;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:J

.field f:Ljava/lang/Object;

.field g:J

.field h:Ljava/lang/Object;

.field i:Z

.field j:Z

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/google/r/b/a/pp;

    invoke-direct {v0}, Lcom/google/r/b/a/pp;-><init>()V

    sput-object v0, Lcom/google/r/b/a/po;->PARSER:Lcom/google/n/ax;

    .line 407
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/po;->n:Lcom/google/n/aw;

    .line 983
    new-instance v0, Lcom/google/r/b/a/po;

    invoke-direct {v0}, Lcom/google/r/b/a/po;-><init>()V

    sput-object v0, Lcom/google/r/b/a/po;->k:Lcom/google/r/b/a/po;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 315
    iput-byte v0, p0, Lcom/google/r/b/a/po;->l:B

    .line 358
    iput v0, p0, Lcom/google/r/b/a/po;->m:I

    .line 18
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/r/b/a/po;->b:I

    .line 19
    const v0, 0xdbba00

    iput v0, p0, Lcom/google/r/b/a/po;->c:I

    .line 20
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/r/b/a/po;->d:I

    .line 21
    const-wide/32 v0, 0x9d21

    iput-wide v0, p0, Lcom/google/r/b/a/po;->e:J

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/po;->f:Ljava/lang/Object;

    .line 23
    const-wide/16 v0, 0x3eda

    iput-wide v0, p0, Lcom/google/r/b/a/po;->g:J

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/po;->h:Ljava/lang/Object;

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/po;->i:Z

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/po;->j:Z

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 33
    invoke-direct {p0}, Lcom/google/r/b/a/po;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 39
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 46
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 48
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/po;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/po;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/po;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/po;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/po;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 103
    :catch_1
    move-exception v0

    .line 104
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 105
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/po;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/po;->d:I

    goto :goto_0

    .line 68
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/po;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/po;->e:J

    goto :goto_0

    .line 73
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 74
    iget v5, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/r/b/a/po;->a:I

    .line 75
    iput-object v0, p0, Lcom/google/r/b/a/po;->f:Ljava/lang/Object;

    goto :goto_0

    .line 79
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/po;->a:I

    .line 80
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/po;->g:J

    goto :goto_0

    .line 84
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 85
    iget v5, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/r/b/a/po;->a:I

    .line 86
    iput-object v0, p0, Lcom/google/r/b/a/po;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 90
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/po;->a:I

    .line 91
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/po;->i:Z

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 95
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/po;->a:I

    .line 96
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/po;->j:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 107
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/po;->au:Lcom/google/n/bn;

    .line 108
    return-void

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 315
    iput-byte v0, p0, Lcom/google/r/b/a/po;->l:B

    .line 358
    iput v0, p0, Lcom/google/r/b/a/po;->m:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/po;
    .locals 1

    .prologue
    .line 986
    sget-object v0, Lcom/google/r/b/a/po;->k:Lcom/google/r/b/a/po;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/pq;
    .locals 1

    .prologue
    .line 469
    new-instance v0, Lcom/google/r/b/a/pq;

    invoke-direct {v0}, Lcom/google/r/b/a/pq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/po;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    sget-object v0, Lcom/google/r/b/a/po;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 327
    invoke-virtual {p0}, Lcom/google/r/b/a/po;->c()I

    .line 328
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 329
    iget v0, p0, Lcom/google/r/b/a/po;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 331
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 332
    iget v0, p0, Lcom/google/r/b/a/po;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 334
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 335
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/po;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 337
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 338
    iget-wide v0, p0, Lcom/google/r/b/a/po;->e:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 340
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 341
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/po;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/po;->f:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 343
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 344
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/r/b/a/po;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 346
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 347
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/po;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/po;->h:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 349
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 350
    iget-boolean v0, p0, Lcom/google/r/b/a/po;->i:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 352
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 353
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/po;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 355
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/po;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 356
    return-void

    .line 341
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 347
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 317
    iget-byte v1, p0, Lcom/google/r/b/a/po;->l:B

    .line 318
    if-ne v1, v0, :cond_0

    .line 322
    :goto_0
    return v0

    .line 319
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 321
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/po;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 360
    iget v0, p0, Lcom/google/r/b/a/po;->m:I

    .line 361
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 402
    :goto_0
    return v0

    .line 364
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_e

    .line 365
    iget v0, p0, Lcom/google/r/b/a/po;->b:I

    .line 366
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 368
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 369
    iget v3, p0, Lcom/google/r/b/a/po;->c:I

    .line 370
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 372
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_3

    .line 373
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/po;->d:I

    .line 374
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 376
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_d

    .line 377
    iget-wide v4, p0, Lcom/google/r/b/a/po;->e:J

    .line 378
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 380
    :goto_4
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 381
    const/4 v3, 0x5

    .line 382
    iget-object v0, p0, Lcom/google/r/b/a/po;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/po;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 384
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 385
    const/4 v0, 0x6

    iget-wide v4, p0, Lcom/google/r/b/a/po;->g:J

    .line 386
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 388
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 389
    const/4 v3, 0x7

    .line 390
    iget-object v0, p0, Lcom/google/r/b/a/po;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/po;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 392
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 393
    const/16 v0, 0x8

    iget-boolean v3, p0, Lcom/google/r/b/a/po;->i:Z

    .line 394
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 396
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/po;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    .line 397
    const/16 v0, 0x9

    iget-boolean v3, p0, Lcom/google/r/b/a/po;->j:Z

    .line 398
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 400
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/po;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 401
    iput v0, p0, Lcom/google/r/b/a/po;->m:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 366
    goto/16 :goto_1

    :cond_a
    move v3, v1

    .line 370
    goto/16 :goto_3

    .line 382
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 390
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_d
    move v1, v0

    goto/16 :goto_4

    :cond_e
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/po;->newBuilder()Lcom/google/r/b/a/pq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/pq;->a(Lcom/google/r/b/a/po;)Lcom/google/r/b/a/pq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/po;->newBuilder()Lcom/google/r/b/a/pq;

    move-result-object v0

    return-object v0
.end method
