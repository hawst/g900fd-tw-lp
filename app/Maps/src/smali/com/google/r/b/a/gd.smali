.class public final enum Lcom/google/r/b/a/gd;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/gd;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/gd;

.field public static final enum b:Lcom/google/r/b/a/gd;

.field public static final enum c:Lcom/google/r/b/a/gd;

.field public static final enum d:Lcom/google/r/b/a/gd;

.field private static final synthetic f:[Lcom/google/r/b/a/gd;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/r/b/a/gd;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/gd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gd;->a:Lcom/google/r/b/a/gd;

    .line 18
    new-instance v0, Lcom/google/r/b/a/gd;

    const-string v1, "AC"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/gd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gd;->b:Lcom/google/r/b/a/gd;

    .line 22
    new-instance v0, Lcom/google/r/b/a/gd;

    const-string v1, "USB"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/gd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gd;->c:Lcom/google/r/b/a/gd;

    .line 26
    new-instance v0, Lcom/google/r/b/a/gd;

    const-string v1, "WIRELESS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/gd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gd;->d:Lcom/google/r/b/a/gd;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/gd;

    sget-object v1, Lcom/google/r/b/a/gd;->a:Lcom/google/r/b/a/gd;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/gd;->b:Lcom/google/r/b/a/gd;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/gd;->c:Lcom/google/r/b/a/gd;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/gd;->d:Lcom/google/r/b/a/gd;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/gd;->f:[Lcom/google/r/b/a/gd;

    .line 66
    new-instance v0, Lcom/google/r/b/a/ge;

    invoke-direct {v0}, Lcom/google/r/b/a/ge;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput p3, p0, Lcom/google/r/b/a/gd;->e:I

    .line 77
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/gd;
    .locals 1

    .prologue
    .line 52
    packed-switch p0, :pswitch_data_0

    .line 57
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 53
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/gd;->a:Lcom/google/r/b/a/gd;

    goto :goto_0

    .line 54
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/gd;->b:Lcom/google/r/b/a/gd;

    goto :goto_0

    .line 55
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/gd;->c:Lcom/google/r/b/a/gd;

    goto :goto_0

    .line 56
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/gd;->d:Lcom/google/r/b/a/gd;

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/gd;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/r/b/a/gd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/gd;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/gd;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/r/b/a/gd;->f:[Lcom/google/r/b/a/gd;

    invoke-virtual {v0}, [Lcom/google/r/b/a/gd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/gd;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/r/b/a/gd;->e:I

    return v0
.end method
