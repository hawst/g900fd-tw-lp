.class public final Lcom/google/r/b/a/vz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/we;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/vz;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/vz;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1726
    new-instance v0, Lcom/google/r/b/a/wa;

    invoke-direct {v0}, Lcom/google/r/b/a/wa;-><init>()V

    sput-object v0, Lcom/google/r/b/a/vz;->PARSER:Lcom/google/n/ax;

    .line 1899
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/vz;->g:Lcom/google/n/aw;

    .line 2136
    new-instance v0, Lcom/google/r/b/a/vz;

    invoke-direct {v0}, Lcom/google/r/b/a/vz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/vz;->d:Lcom/google/r/b/a/vz;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1671
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1835
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    .line 1850
    iput-byte v2, p0, Lcom/google/r/b/a/vz;->e:B

    .line 1878
    iput v2, p0, Lcom/google/r/b/a/vz;->f:I

    .line 1672
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/vz;->b:I

    .line 1673
    iget-object v0, p0, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1674
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1680
    invoke-direct {p0}, Lcom/google/r/b/a/vz;-><init>()V

    .line 1681
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1686
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 1687
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1688
    sparse-switch v3, :sswitch_data_0

    .line 1693
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1695
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1691
    goto :goto_0

    .line 1700
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1701
    invoke-static {v3}, Lcom/google/r/b/a/wc;->a(I)Lcom/google/r/b/a/wc;

    move-result-object v4

    .line 1702
    if-nez v4, :cond_1

    .line 1703
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1717
    :catch_0
    move-exception v0

    .line 1718
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1723
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/vz;->au:Lcom/google/n/bn;

    throw v0

    .line 1705
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/vz;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/vz;->a:I

    .line 1706
    iput v3, p0, Lcom/google/r/b/a/vz;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1719
    :catch_1
    move-exception v0

    .line 1720
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1721
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1711
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1712
    iget v3, p0, Lcom/google/r/b/a/vz;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/vz;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1723
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vz;->au:Lcom/google/n/bn;

    .line 1724
    return-void

    .line 1688
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1669
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1835
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    .line 1850
    iput-byte v1, p0, Lcom/google/r/b/a/vz;->e:B

    .line 1878
    iput v1, p0, Lcom/google/r/b/a/vz;->f:I

    .line 1670
    return-void
.end method

.method public static d()Lcom/google/r/b/a/vz;
    .locals 1

    .prologue
    .line 2139
    sget-object v0, Lcom/google/r/b/a/vz;->d:Lcom/google/r/b/a/vz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/wb;
    .locals 1

    .prologue
    .line 1961
    new-instance v0, Lcom/google/r/b/a/wb;

    invoke-direct {v0}, Lcom/google/r/b/a/wb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/vz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1738
    sget-object v0, Lcom/google/r/b/a/vz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1868
    invoke-virtual {p0}, Lcom/google/r/b/a/vz;->c()I

    .line 1869
    iget v0, p0, Lcom/google/r/b/a/vz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1870
    iget v0, p0, Lcom/google/r/b/a/vz;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 1872
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/vz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1873
    iget-object v0, p0, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1875
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/vz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1876
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1852
    iget-byte v0, p0, Lcom/google/r/b/a/vz;->e:B

    .line 1853
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1863
    :goto_0
    return v0

    .line 1854
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1856
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/vz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1857
    iget-object v0, p0, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hh;->d()Lcom/google/r/b/a/hh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hh;

    invoke-virtual {v0}, Lcom/google/r/b/a/hh;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1858
    iput-byte v2, p0, Lcom/google/r/b/a/vz;->e:B

    move v0, v2

    .line 1859
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1856
    goto :goto_1

    .line 1862
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/vz;->e:B

    move v0, v1

    .line 1863
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1880
    iget v0, p0, Lcom/google/r/b/a/vz;->f:I

    .line 1881
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1894
    :goto_0
    return v0

    .line 1884
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/vz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 1885
    iget v0, p0, Lcom/google/r/b/a/vz;->b:I

    .line 1886
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1888
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/vz;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1889
    iget-object v2, p0, Lcom/google/r/b/a/vz;->c:Lcom/google/n/ao;

    .line 1890
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1892
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/vz;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1893
    iput v0, p0, Lcom/google/r/b/a/vz;->f:I

    goto :goto_0

    .line 1886
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1663
    invoke-static {}, Lcom/google/r/b/a/vz;->newBuilder()Lcom/google/r/b/a/wb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/wb;->a(Lcom/google/r/b/a/vz;)Lcom/google/r/b/a/wb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1663
    invoke-static {}, Lcom/google/r/b/a/vz;->newBuilder()Lcom/google/r/b/a/wb;

    move-result-object v0

    return-object v0
.end method
