.class public final Lcom/google/r/b/a/oa;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/od;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/oa;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/r/b/a/oa;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field public d:I

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lcom/google/r/b/a/ob;

    invoke-direct {v0}, Lcom/google/r/b/a/ob;-><init>()V

    sput-object v0, Lcom/google/r/b/a/oa;->PARSER:Lcom/google/n/ax;

    .line 388
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/oa;->k:Lcom/google/n/aw;

    .line 1035
    new-instance v0, Lcom/google/r/b/a/oa;

    invoke-direct {v0}, Lcom/google/r/b/a/oa;-><init>()V

    sput-object v0, Lcom/google/r/b/a/oa;->h:Lcom/google/r/b/a/oa;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 294
    iput-byte v1, p0, Lcom/google/r/b/a/oa;->i:B

    .line 346
    iput v1, p0, Lcom/google/r/b/a/oa;->j:I

    .line 18
    iput v0, p0, Lcom/google/r/b/a/oa;->b:I

    .line 19
    iput v0, p0, Lcom/google/r/b/a/oa;->c:I

    .line 20
    iput v0, p0, Lcom/google/r/b/a/oa;->d:I

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/16 v9, 0x20

    const/16 v8, 0x8

    const/16 v7, 0x10

    .line 30
    invoke-direct {p0}, Lcom/google/r/b/a/oa;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v0

    .line 36
    :cond_0
    :goto_0
    if-nez v3, :cond_8

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 38
    sparse-switch v1, :sswitch_data_0

    .line 43
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    const/4 v1, 0x1

    move v3, v1

    goto :goto_0

    .line 40
    :sswitch_0
    const/4 v1, 0x1

    move v3, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    and-int/lit8 v1, v0, 0x10

    if-eq v1, v7, :cond_e

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 52
    or-int/lit8 v1, v0, 0x10

    .line 54
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 55
    goto :goto_0

    .line 58
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 59
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 60
    and-int/lit8 v1, v0, 0x10

    if-eq v1, v7, :cond_d

    iget v1, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v1, v6, :cond_4

    move v1, v2

    :goto_2
    if-lez v1, :cond_d

    .line 61
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62
    or-int/lit8 v1, v0, 0x10

    .line 64
    :goto_3
    :try_start_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_5

    move v0, v2

    :goto_4
    if-lez v0, :cond_6

    .line 65
    iget-object v0, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 107
    :catch_0
    move-exception v0

    .line 108
    :goto_5
    :try_start_4
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 113
    :catchall_0
    move-exception v0

    :goto_6
    and-int/lit8 v2, v1, 0x10

    if-ne v2, v7, :cond_1

    .line 114
    iget-object v2, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    .line 116
    :cond_1
    and-int/lit8 v2, v1, 0x20

    if-ne v2, v9, :cond_2

    .line 117
    iget-object v2, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    .line 119
    :cond_2
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v8, :cond_3

    .line 120
    iget-object v1, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    .line 122
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/oa;->au:Lcom/google/n/bn;

    throw v0

    .line 60
    :cond_4
    :try_start_5
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v6

    iget v6, p1, Lcom/google/n/j;->f:I
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    sub-int v1, v6, v1

    goto :goto_2

    .line 64
    :cond_5
    :try_start_6
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_4

    .line 67
    :cond_6
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v0, v1

    .line 68
    goto/16 :goto_0

    .line 71
    :sswitch_3
    :try_start_7
    iget v1, p0, Lcom/google/r/b/a/oa;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/oa;->a:I

    .line 72
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/oa;->b:I

    goto/16 :goto_0

    .line 107
    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_5

    .line 76
    :sswitch_4
    iget v1, p0, Lcom/google/r/b/a/oa;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/oa;->a:I

    .line 77
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/oa;->c:I
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_0

    .line 109
    :catch_2
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 110
    :goto_7
    :try_start_8
    new-instance v2, Lcom/google/n/ak;

    .line 111
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 81
    :sswitch_5
    :try_start_9
    iget v1, p0, Lcom/google/r/b/a/oa;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/oa;->a:I

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/oa;->d:I

    goto/16 :goto_0

    .line 113
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_6

    .line 86
    :sswitch_6
    and-int/lit8 v1, v0, 0x20

    if-eq v1, v9, :cond_c

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 89
    or-int/lit8 v1, v0, 0x20

    .line 91
    :goto_8
    :try_start_a
    iget-object v0, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 92
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 91
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move v0, v1

    .line 93
    goto/16 :goto_0

    .line 96
    :sswitch_7
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v8, :cond_7

    .line 97
    :try_start_b
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    .line 99
    or-int/lit8 v0, v0, 0x8

    .line 101
    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 101
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_0

    .line 113
    :cond_8
    and-int/lit8 v1, v0, 0x10

    if-ne v1, v7, :cond_9

    .line 114
    iget-object v1, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    .line 116
    :cond_9
    and-int/lit8 v1, v0, 0x20

    if-ne v1, v9, :cond_a

    .line 117
    iget-object v1, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    .line 119
    :cond_a
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_b

    .line 120
    iget-object v0, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    .line 122
    :cond_b
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/oa;->au:Lcom/google/n/bn;

    .line 123
    return-void

    .line 109
    :catch_3
    move-exception v0

    goto/16 :goto_7

    :cond_c
    move v1, v0

    goto :goto_8

    :cond_d
    move v1, v0

    goto/16 :goto_3

    :cond_e
    move v1, v0

    goto/16 :goto_1

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
        0x20 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 294
    iput-byte v0, p0, Lcom/google/r/b/a/oa;->i:B

    .line 346
    iput v0, p0, Lcom/google/r/b/a/oa;->j:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/oa;
    .locals 1

    .prologue
    .line 1038
    sget-object v0, Lcom/google/r/b/a/oa;->h:Lcom/google/r/b/a/oa;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/oc;
    .locals 1

    .prologue
    .line 450
    new-instance v0, Lcom/google/r/b/a/oc;

    invoke-direct {v0}, Lcom/google/r/b/a/oc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/oa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    sget-object v0, Lcom/google/r/b/a/oa;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 324
    invoke-virtual {p0}, Lcom/google/r/b/a/oa;->c()I

    move v1, v2

    .line 325
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 325
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 328
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 329
    iget v0, p0, Lcom/google/r/b/a/oa;->b:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 331
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 332
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/oa;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 334
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 335
    iget v0, p0, Lcom/google/r/b/a/oa;->d:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(II)V

    :cond_3
    move v1, v2

    .line 337
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 338
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 337
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 340
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 341
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 340
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 343
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/oa;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 344
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 296
    iget-byte v0, p0, Lcom/google/r/b/a/oa;->i:B

    .line 297
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 319
    :cond_0
    :goto_0
    return v2

    .line 298
    :cond_1
    if-eqz v0, :cond_0

    .line 300
    iget v0, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    .line 301
    iput-byte v2, p0, Lcom/google/r/b/a/oa;->i:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 300
    goto :goto_1

    .line 304
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-nez v0, :cond_5

    .line 305
    iput-byte v2, p0, Lcom/google/r/b/a/oa;->i:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 304
    goto :goto_2

    .line 308
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-nez v0, :cond_7

    .line 309
    iput-byte v2, p0, Lcom/google/r/b/a/oa;->i:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 308
    goto :goto_3

    :cond_7
    move v1, v2

    .line 312
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 313
    iget-object v0, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/nw;->d()Lcom/google/r/b/a/nw;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/nw;

    invoke-virtual {v0}, Lcom/google/r/b/a/nw;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 314
    iput-byte v2, p0, Lcom/google/r/b/a/oa;->i:B

    goto :goto_0

    .line 312
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 318
    :cond_9
    iput-byte v3, p0, Lcom/google/r/b/a/oa;->i:B

    move v2, v3

    .line 319
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 348
    iget v0, p0, Lcom/google/r/b/a/oa;->j:I

    .line 349
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 383
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 354
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 355
    iget-object v0, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    .line 356
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    .line 354
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v4

    .line 356
    goto :goto_2

    .line 358
    :cond_2
    add-int/lit8 v0, v3, 0x0

    .line 359
    iget-object v1, p0, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 361
    iget v0, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_a

    .line 362
    iget v0, p0, Lcom/google/r/b/a/oa;->b:I

    .line 363
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 365
    :goto_4
    iget v1, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v5, :cond_3

    .line 366
    const/4 v1, 0x3

    iget v3, p0, Lcom/google/r/b/a/oa;->c:I

    .line 367
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_5
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    .line 369
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_5

    .line 370
    iget v1, p0, Lcom/google/r/b/a/oa;->d:I

    .line 371
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v1, :cond_4

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_4
    add-int v1, v3, v4

    add-int/2addr v0, v1

    :cond_5
    move v1, v2

    move v3, v0

    .line 373
    :goto_6
    iget-object v0, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 374
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    .line 375
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 373
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_6
    move v0, v4

    .line 363
    goto :goto_3

    :cond_7
    move v1, v4

    .line 367
    goto :goto_5

    :cond_8
    move v1, v2

    .line 377
    :goto_7
    iget-object v0, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 378
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    .line 379
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 377
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 381
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/oa;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 382
    iput v0, p0, Lcom/google/r/b/a/oa;->j:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_4
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/oa;->newBuilder()Lcom/google/r/b/a/oc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/oc;->a(Lcom/google/r/b/a/oa;)Lcom/google/r/b/a/oc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/oa;->newBuilder()Lcom/google/r/b/a/oc;

    move-result-object v0

    return-object v0
.end method
