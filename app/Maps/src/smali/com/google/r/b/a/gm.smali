.class public final Lcom/google/r/b/a/gm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/gp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/gk;",
        "Lcom/google/r/b/a/gm;",
        ">;",
        "Lcom/google/r/b/a/gp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 388
    sget-object v0, Lcom/google/r/b/a/gk;->e:Lcom/google/r/b/a/gk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 448
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/gm;->b:Ljava/lang/Object;

    .line 524
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/gm;->c:I

    .line 389
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 380
    new-instance v2, Lcom/google/r/b/a/gk;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/gk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/gm;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/gm;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/gk;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/gm;->c:I

    iput v1, v2, Lcom/google/r/b/a/gk;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/r/b/a/gm;->d:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/gk;->d:Z

    iput v0, v2, Lcom/google/r/b/a/gk;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 380
    check-cast p1, Lcom/google/r/b/a/gk;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/gm;->a(Lcom/google/r/b/a/gk;)Lcom/google/r/b/a/gm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/gk;)Lcom/google/r/b/a/gm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 426
    invoke-static {}, Lcom/google/r/b/a/gk;->d()Lcom/google/r/b/a/gk;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 439
    :goto_0
    return-object p0

    .line 427
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 428
    iget v2, p0, Lcom/google/r/b/a/gm;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/gm;->a:I

    .line 429
    iget-object v2, p1, Lcom/google/r/b/a/gk;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/gm;->b:Ljava/lang/Object;

    .line 432
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 433
    iget v2, p1, Lcom/google/r/b/a/gk;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/gn;->a(I)Lcom/google/r/b/a/gn;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/r/b/a/gn;->a:Lcom/google/r/b/a/gn;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 427
    goto :goto_1

    :cond_4
    move v2, v1

    .line 432
    goto :goto_2

    .line 433
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/gm;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/gm;->a:I

    iget v2, v2, Lcom/google/r/b/a/gn;->b:I

    iput v2, p0, Lcom/google/r/b/a/gm;->c:I

    .line 435
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_7

    .line 436
    iget-boolean v0, p1, Lcom/google/r/b/a/gk;->d:Z

    iget v1, p0, Lcom/google/r/b/a/gm;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/gm;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/gm;->d:Z

    .line 438
    :cond_7
    iget-object v0, p1, Lcom/google/r/b/a/gk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_8
    move v0, v1

    .line 435
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x1

    return v0
.end method
