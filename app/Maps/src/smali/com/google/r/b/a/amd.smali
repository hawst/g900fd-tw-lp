.class public final Lcom/google/r/b/a/amd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/amd;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/amd;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:F

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Ljava/lang/Object;

.field g:Z

.field public h:Z

.field public i:Lcom/google/n/f;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2282
    new-instance v0, Lcom/google/r/b/a/ame;

    invoke-direct {v0}, Lcom/google/r/b/a/ame;-><init>()V

    sput-object v0, Lcom/google/r/b/a/amd;->PARSER:Lcom/google/n/ax;

    .line 2572
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/amd;->m:Lcom/google/n/aw;

    .line 3249
    new-instance v0, Lcom/google/r/b/a/amd;

    invoke-direct {v0}, Lcom/google/r/b/a/amd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/amd;->j:Lcom/google/r/b/a/amd;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2188
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2357
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amd;->d:Lcom/google/n/ao;

    .line 2373
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    .line 2475
    iput-byte v2, p0, Lcom/google/r/b/a/amd;->k:B

    .line 2527
    iput v2, p0, Lcom/google/r/b/a/amd;->l:I

    .line 2189
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    .line 2190
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/amd;->c:F

    .line 2191
    iget-object v0, p0, Lcom/google/r/b/a/amd;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 2192
    iget-object v0, p0, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 2193
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amd;->f:Ljava/lang/Object;

    .line 2194
    iput-boolean v3, p0, Lcom/google/r/b/a/amd;->g:Z

    .line 2195
    iput-boolean v3, p0, Lcom/google/r/b/a/amd;->h:Z

    .line 2196
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/amd;->i:Lcom/google/n/f;

    .line 2197
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2203
    invoke-direct {p0}, Lcom/google/r/b/a/amd;-><init>()V

    .line 2206
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 2209
    :cond_0
    :goto_0
    if-nez v4, :cond_4

    .line 2210
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 2211
    sparse-switch v0, :sswitch_data_0

    .line 2216
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 2218
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 2214
    goto :goto_0

    .line 2223
    :sswitch_1
    iget-object v0, p0, Lcom/google/r/b/a/amd;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 2224
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/amd;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2270
    :catch_0
    move-exception v0

    .line 2271
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2277
    iget-object v1, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    .line 2279
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/amd;->au:Lcom/google/n/bn;

    throw v0

    .line 2228
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 2229
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/amd;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2272
    :catch_1
    move-exception v0

    .line 2273
    :try_start_3
    new-instance v3, Lcom/google/n/ak;

    .line 2274
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v3, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2233
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 2234
    iget v6, p0, Lcom/google/r/b/a/amd;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/r/b/a/amd;->a:I

    .line 2235
    iput-object v0, p0, Lcom/google/r/b/a/amd;->f:Ljava/lang/Object;

    goto :goto_0

    .line 2239
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/amd;->a:I

    .line 2240
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/amd;->g:Z

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 2244
    :sswitch_5
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v2, :cond_3

    .line 2245
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    .line 2247
    or-int/lit8 v1, v1, 0x1

    .line 2249
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 2250
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 2249
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2254
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/amd;->a:I

    .line 2255
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/amd;->c:F

    goto/16 :goto_0

    .line 2259
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/amd;->a:I

    .line 2260
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/amd;->h:Z

    goto/16 :goto_0

    .line 2264
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/amd;->a:I

    .line 2265
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amd;->i:Lcom/google/n/f;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2276
    :cond_4
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_5

    .line 2277
    iget-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    .line 2279
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amd;->au:Lcom/google/n/bn;

    .line 2280
    return-void

    .line 2211
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3d -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2186
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2357
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amd;->d:Lcom/google/n/ao;

    .line 2373
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    .line 2475
    iput-byte v1, p0, Lcom/google/r/b/a/amd;->k:B

    .line 2527
    iput v1, p0, Lcom/google/r/b/a/amd;->l:I

    .line 2187
    return-void
.end method

.method public static d()Lcom/google/r/b/a/amd;
    .locals 1

    .prologue
    .line 3252
    sget-object v0, Lcom/google/r/b/a/amd;->j:Lcom/google/r/b/a/amd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/amf;
    .locals 1

    .prologue
    .line 2634
    new-instance v0, Lcom/google/r/b/a/amf;

    invoke-direct {v0}, Lcom/google/r/b/a/amf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/amd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2294
    sget-object v0, Lcom/google/r/b/a/amd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x2

    .line 2499
    invoke-virtual {p0}, Lcom/google/r/b/a/amd;->c()I

    .line 2500
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_0

    .line 2501
    iget-object v0, p0, Lcom/google/r/b/a/amd;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2503
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v2, :cond_1

    .line 2504
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2506
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v3, :cond_2

    .line 2507
    iget-object v0, p0, Lcom/google/r/b/a/amd;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amd;->f:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2509
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 2510
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/amd;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 2512
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 2513
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2512
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2507
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 2515
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 2516
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/amd;->c:F

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IF)V

    .line 2518
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 2519
    iget-boolean v0, p0, Lcom/google/r/b/a/amd;->h:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 2521
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 2522
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/r/b/a/amd;->i:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2524
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/amd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2525
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2477
    iget-byte v0, p0, Lcom/google/r/b/a/amd;->k:B

    .line 2478
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 2494
    :cond_0
    :goto_0
    return v2

    .line 2479
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 2481
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2482
    iget-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/alu;->d()Lcom/google/r/b/a/alu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alu;

    invoke-virtual {v0}, Lcom/google/r/b/a/alu;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2483
    iput-byte v2, p0, Lcom/google/r/b/a/amd;->k:B

    goto :goto_0

    .line 2481
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2487
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 2488
    iget-object v0, p0, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2489
    iput-byte v2, p0, Lcom/google/r/b/a/amd;->k:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 2487
    goto :goto_2

    .line 2493
    :cond_5
    iput-byte v3, p0, Lcom/google/r/b/a/amd;->k:B

    move v2, v3

    .line 2494
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 2529
    iget v0, p0, Lcom/google/r/b/a/amd;->l:I

    .line 2530
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2567
    :goto_0
    return v0

    .line 2533
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_9

    .line 2534
    iget-object v0, p0, Lcom/google/r/b/a/amd;->d:Lcom/google/n/ao;

    .line 2535
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 2537
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_8

    .line 2538
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/amd;->e:Lcom/google/n/ao;

    .line 2539
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 2541
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_1

    .line 2543
    iget-object v0, p0, Lcom/google/r/b/a/amd;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amd;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 2545
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    .line 2546
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/r/b/a/amd;->g:Z

    .line 2547
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    :cond_2
    move v3, v2

    move v2, v1

    .line 2549
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2550
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    .line 2551
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2549
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2543
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 2553
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    .line 2554
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/r/b/a/amd;->c:F

    .line 2555
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 2557
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_6

    .line 2558
    iget-boolean v0, p0, Lcom/google/r/b/a/amd;->h:Z

    .line 2559
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 2561
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/amd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_7

    .line 2562
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/r/b/a/amd;->i:Lcom/google/n/f;

    .line 2563
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 2565
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/amd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2566
    iput v0, p0, Lcom/google/r/b/a/amd;->l:I

    goto/16 :goto_0

    :cond_8
    move v2, v0

    goto/16 :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2180
    invoke-static {}, Lcom/google/r/b/a/amd;->newBuilder()Lcom/google/r/b/a/amf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/amf;->a(Lcom/google/r/b/a/amd;)Lcom/google/r/b/a/amf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2180
    invoke-static {}, Lcom/google/r/b/a/amd;->newBuilder()Lcom/google/r/b/a/amf;

    move-result-object v0

    return-object v0
.end method
