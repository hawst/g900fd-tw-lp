.class public final enum Lcom/google/r/b/a/um;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/um;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/um;

.field public static final enum b:Lcom/google/r/b/a/um;

.field public static final enum c:Lcom/google/r/b/a/um;

.field public static final enum d:Lcom/google/r/b/a/um;

.field public static final enum e:Lcom/google/r/b/a/um;

.field public static final enum f:Lcom/google/r/b/a/um;

.field public static final enum g:Lcom/google/r/b/a/um;

.field public static final enum h:Lcom/google/r/b/a/um;

.field public static final enum i:Lcom/google/r/b/a/um;

.field public static final enum j:Lcom/google/r/b/a/um;

.field public static final enum k:Lcom/google/r/b/a/um;

.field public static final enum l:Lcom/google/r/b/a/um;

.field public static final enum m:Lcom/google/r/b/a/um;

.field public static final enum n:Lcom/google/r/b/a/um;

.field public static final enum o:Lcom/google/r/b/a/um;

.field private static final synthetic q:[Lcom/google/r/b/a/um;


# instance fields
.field final p:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 85
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_USER_STRING"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->a:Lcom/google/r/b/a/um;

    .line 89
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_MAP_POINT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->b:Lcom/google/r/b/a/um;

    .line 93
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_MY_LOCATION"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->c:Lcom/google/r/b/a/um;

    .line 97
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_SERVER_RESPONSE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->d:Lcom/google/r/b/a/um;

    .line 101
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_MY_PLACES"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->e:Lcom/google/r/b/a/um;

    .line 105
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_EXTERNAL_APP"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->f:Lcom/google/r/b/a/um;

    .line 109
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_FRIEND"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->g:Lcom/google/r/b/a/um;

    .line 113
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_EPHEMERAL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->h:Lcom/google/r/b/a/um;

    .line 117
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_HOME"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->i:Lcom/google/r/b/a/um;

    .line 121
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_WORK"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->j:Lcom/google/r/b/a/um;

    .line 125
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_SUGGEST"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->k:Lcom/google/r/b/a/um;

    .line 129
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_ODELAY"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->l:Lcom/google/r/b/a/um;

    .line 133
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_ODELAY_STRONG"

    const/16 v2, 0xc

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->m:Lcom/google/r/b/a/um;

    .line 137
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_SMART_MAPS"

    const/16 v2, 0xd

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->n:Lcom/google/r/b/a/um;

    .line 141
    new-instance v0, Lcom/google/r/b/a/um;

    const-string v1, "SOURCE_LONG_MAP_PRESS"

    const/16 v2, 0xe

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/um;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/um;->o:Lcom/google/r/b/a/um;

    .line 80
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/google/r/b/a/um;

    sget-object v1, Lcom/google/r/b/a/um;->a:Lcom/google/r/b/a/um;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/um;->b:Lcom/google/r/b/a/um;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/um;->c:Lcom/google/r/b/a/um;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/um;->d:Lcom/google/r/b/a/um;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/um;->e:Lcom/google/r/b/a/um;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/um;->f:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/um;->g:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/r/b/a/um;->h:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/um;->i:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/r/b/a/um;->j:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/r/b/a/um;->k:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/r/b/a/um;->l:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/r/b/a/um;->m:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/r/b/a/um;->n:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/r/b/a/um;->o:Lcom/google/r/b/a/um;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/um;->q:[Lcom/google/r/b/a/um;

    .line 236
    new-instance v0, Lcom/google/r/b/a/un;

    invoke-direct {v0}, Lcom/google/r/b/a/un;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 245
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 246
    iput p3, p0, Lcom/google/r/b/a/um;->p:I

    .line 247
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/um;
    .locals 1

    .prologue
    .line 211
    packed-switch p0, :pswitch_data_0

    .line 227
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 212
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/um;->a:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 213
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/um;->b:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 214
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/um;->c:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 215
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/um;->d:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 216
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/um;->e:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 217
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/um;->f:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 218
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/um;->g:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 219
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/um;->h:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 220
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/um;->i:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 221
    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/um;->j:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 222
    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/um;->k:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 223
    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/um;->l:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 224
    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/um;->m:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 225
    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/um;->n:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 226
    :pswitch_e
    sget-object v0, Lcom/google/r/b/a/um;->o:Lcom/google/r/b/a/um;

    goto :goto_0

    .line 211
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_d
        :pswitch_e
        :pswitch_c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/um;
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcom/google/r/b/a/um;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/um;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/um;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/google/r/b/a/um;->q:[Lcom/google/r/b/a/um;

    invoke-virtual {v0}, [Lcom/google/r/b/a/um;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/um;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/google/r/b/a/um;->p:I

    return v0
.end method
