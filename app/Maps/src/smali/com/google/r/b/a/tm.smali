.class public final Lcom/google/r/b/a/tm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/tp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/tm;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/tm;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:F

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/ao;

.field h:Ljava/lang/Object;

.field i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/google/r/b/a/tn;

    invoke-direct {v0}, Lcom/google/r/b/a/tn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/tm;->PARSER:Lcom/google/n/ax;

    .line 443
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/tm;->m:Lcom/google/n/aw;

    .line 1136
    new-instance v0, Lcom/google/r/b/a/tm;

    invoke-direct {v0}, Lcom/google/r/b/a/tm;-><init>()V

    sput-object v0, Lcom/google/r/b/a/tm;->j:Lcom/google/r/b/a/tm;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/tm;->b:Lcom/google/n/ao;

    .line 280
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    .line 352
    iput-byte v2, p0, Lcom/google/r/b/a/tm;->k:B

    .line 398
    iput v2, p0, Lcom/google/r/b/a/tm;->l:I

    .line 18
    iget-object v0, p0, Lcom/google/r/b/a/tm;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/tm;->c:Ljava/lang/Object;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/tm;->d:F

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/tm;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/tm;->f:Ljava/lang/Object;

    .line 23
    iget-object v0, p0, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/tm;->h:Ljava/lang/Object;

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/tm;->i:I

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/r/b/a/tm;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 40
    sparse-switch v3, :sswitch_data_0

    .line 45
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/tm;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 53
    iget v3, p0, Lcom/google/r/b/a/tm;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/tm;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/tm;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 58
    iget v4, p0, Lcom/google/r/b/a/tm;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/tm;->a:I

    .line 59
    iput-object v3, p0, Lcom/google/r/b/a/tm;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 99
    :catch_1
    move-exception v0

    .line 100
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 101
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/tm;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/tm;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/tm;->d:F

    goto :goto_0

    .line 68
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 69
    iget v4, p0, Lcom/google/r/b/a/tm;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/tm;->a:I

    .line 70
    iput-object v3, p0, Lcom/google/r/b/a/tm;->e:Ljava/lang/Object;

    goto :goto_0

    .line 74
    :sswitch_5
    iget-object v3, p0, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 75
    iget v3, p0, Lcom/google/r/b/a/tm;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/tm;->a:I

    goto :goto_0

    .line 79
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 80
    iget v4, p0, Lcom/google/r/b/a/tm;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/tm;->a:I

    .line 81
    iput-object v3, p0, Lcom/google/r/b/a/tm;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 85
    :sswitch_7
    iget v3, p0, Lcom/google/r/b/a/tm;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/tm;->a:I

    .line 86
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/tm;->i:I

    goto/16 :goto_0

    .line 90
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 91
    iget v4, p0, Lcom/google/r/b/a/tm;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/tm;->a:I

    .line 92
    iput-object v3, p0, Lcom/google/r/b/a/tm;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 103
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->au:Lcom/google/n/bn;

    .line 104
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/tm;->b:Lcom/google/n/ao;

    .line 280
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    .line 352
    iput-byte v1, p0, Lcom/google/r/b/a/tm;->k:B

    .line 398
    iput v1, p0, Lcom/google/r/b/a/tm;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/tm;
    .locals 1

    .prologue
    .line 1139
    sget-object v0, Lcom/google/r/b/a/tm;->j:Lcom/google/r/b/a/tm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/to;
    .locals 1

    .prologue
    .line 505
    new-instance v0, Lcom/google/r/b/a/to;

    invoke-direct {v0}, Lcom/google/r/b/a/to;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/tm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    sget-object v0, Lcom/google/r/b/a/tm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 370
    invoke-virtual {p0}, Lcom/google/r/b/a/tm;->c()I

    .line 371
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/r/b/a/tm;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 374
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 375
    iget-object v0, p0, Lcom/google/r/b/a/tm;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 377
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 378
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/tm;->d:F

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IF)V

    .line 380
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 381
    iget-object v0, p0, Lcom/google/r/b/a/tm;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 383
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 384
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 386
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 387
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/tm;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->h:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 389
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 390
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/tm;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 392
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 393
    iget-object v0, p0, Lcom/google/r/b/a/tm;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->f:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 395
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/tm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 396
    return-void

    .line 375
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 381
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 387
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 393
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 354
    iget-byte v0, p0, Lcom/google/r/b/a/tm;->k:B

    .line 355
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 365
    :goto_0
    return v0

    .line 356
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 358
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 359
    iget-object v0, p0, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 360
    iput-byte v2, p0, Lcom/google/r/b/a/tm;->k:B

    move v0, v2

    .line 361
    goto :goto_0

    :cond_2
    move v0, v2

    .line 358
    goto :goto_1

    .line 364
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/tm;->k:B

    move v0, v1

    .line 365
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 400
    iget v0, p0, Lcom/google/r/b/a/tm;->l:I

    .line 401
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 438
    :goto_0
    return v0

    .line 404
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_d

    .line 405
    iget-object v0, p0, Lcom/google/r/b/a/tm;->b:Lcom/google/n/ao;

    .line 406
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 408
    :goto_1
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 410
    iget-object v0, p0, Lcom/google/r/b/a/tm;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 412
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 413
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/r/b/a/tm;->d:F

    .line 414
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v1, v0

    .line 416
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 418
    iget-object v0, p0, Lcom/google/r/b/a/tm;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 420
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 421
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    .line 422
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 424
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 425
    const/4 v3, 0x6

    .line 426
    iget-object v0, p0, Lcom/google/r/b/a/tm;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->h:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 428
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 429
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/r/b/a/tm;->i:I

    .line 430
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_b

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 432
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_7

    .line 434
    iget-object v0, p0, Lcom/google/r/b/a/tm;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tm;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 436
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/tm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 437
    iput v0, p0, Lcom/google/r/b/a/tm;->l:I

    goto/16 :goto_0

    .line 410
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 418
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 426
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 430
    :cond_b
    const/16 v0, 0xa

    goto :goto_5

    .line 434
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_d
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/tm;->newBuilder()Lcom/google/r/b/a/to;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/to;->a(Lcom/google/r/b/a/tm;)Lcom/google/r/b/a/to;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/tm;->newBuilder()Lcom/google/r/b/a/to;

    move-result-object v0

    return-object v0
.end method
