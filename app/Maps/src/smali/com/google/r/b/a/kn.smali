.class public final Lcom/google/r/b/a/kn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/kq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/kn;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/kn;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/google/r/b/a/ko;

    invoke-direct {v0}, Lcom/google/r/b/a/ko;-><init>()V

    sput-object v0, Lcom/google/r/b/a/kn;->PARSER:Lcom/google/n/ax;

    .line 268
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/kn;->h:Lcom/google/n/aw;

    .line 653
    new-instance v0, Lcom/google/r/b/a/kn;

    invoke-direct {v0}, Lcom/google/r/b/a/kn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/kn;->e:Lcom/google/r/b/a/kn;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 203
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/kn;->d:Lcom/google/n/ao;

    .line 218
    iput-byte v2, p0, Lcom/google/r/b/a/kn;->f:B

    .line 243
    iput v2, p0, Lcom/google/r/b/a/kn;->g:I

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/kn;->b:I

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    .line 61
    iget-object v0, p0, Lcom/google/r/b/a/kn;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 62
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Lcom/google/r/b/a/kn;-><init>()V

    .line 71
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 74
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 75
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 76
    sparse-switch v4, :sswitch_data_0

    .line 81
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 83
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 79
    goto :goto_0

    .line 88
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 89
    invoke-static {v4}, Lcom/google/maps/g/il;->a(I)Lcom/google/maps/g/il;

    move-result-object v5

    .line 90
    if-nez v5, :cond_2

    .line 91
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 122
    iget-object v1, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    .line 124
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/kn;->au:Lcom/google/n/bn;

    throw v0

    .line 93
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/r/b/a/kn;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/kn;->a:I

    .line 94
    iput v4, p0, Lcom/google/r/b/a/kn;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 117
    :catch_1
    move-exception v0

    .line 118
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 119
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 99
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_3

    .line 100
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    .line 102
    or-int/lit8 v1, v1, 0x2

    .line 104
    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 105
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 104
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    :sswitch_3
    iget-object v4, p0, Lcom/google/r/b/a/kn;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 110
    iget v4, p0, Lcom/google/r/b/a/kn;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/kn;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 121
    :cond_4
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_5

    .line 122
    iget-object v0, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    .line 124
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/kn;->au:Lcom/google/n/bn;

    .line 125
    return-void

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 56
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 203
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/kn;->d:Lcom/google/n/ao;

    .line 218
    iput-byte v1, p0, Lcom/google/r/b/a/kn;->f:B

    .line 243
    iput v1, p0, Lcom/google/r/b/a/kn;->g:I

    .line 57
    return-void
.end method

.method public static d()Lcom/google/r/b/a/kn;
    .locals 1

    .prologue
    .line 656
    sget-object v0, Lcom/google/r/b/a/kn;->e:Lcom/google/r/b/a/kn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/kp;
    .locals 1

    .prologue
    .line 330
    new-instance v0, Lcom/google/r/b/a/kp;

    invoke-direct {v0}, Lcom/google/r/b/a/kp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/kn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    sget-object v0, Lcom/google/r/b/a/kn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 230
    invoke-virtual {p0}, Lcom/google/r/b/a/kn;->c()I

    .line 231
    iget v0, p0, Lcom/google/r/b/a/kn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 232
    iget v0, p0, Lcom/google/r/b/a/kn;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 234
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 237
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/kn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 238
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/kn;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 240
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/kn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 241
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 220
    iget-byte v1, p0, Lcom/google/r/b/a/kn;->f:B

    .line 221
    if-ne v1, v0, :cond_0

    .line 225
    :goto_0
    return v0

    .line 222
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 224
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/kn;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 245
    iget v0, p0, Lcom/google/r/b/a/kn;->g:I

    .line 246
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 263
    :goto_0
    return v0

    .line 249
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/kn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 250
    iget v0, p0, Lcom/google/r/b/a/kn;->b:I

    .line 251
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 253
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 254
    iget-object v0, p0, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    .line 255
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 253
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 251
    :cond_1
    const/16 v0, 0xa

    goto :goto_1

    .line 257
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/kn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_3

    .line 258
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/r/b/a/kn;->d:Lcom/google/n/ao;

    .line 259
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/kn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 262
    iput v0, p0, Lcom/google/r/b/a/kn;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/r/b/a/kn;->newBuilder()Lcom/google/r/b/a/kp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/kp;->a(Lcom/google/r/b/a/kn;)Lcom/google/r/b/a/kp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/r/b/a/kn;->newBuilder()Lcom/google/r/b/a/kp;

    move-result-object v0

    return-object v0
.end method
