.class public final Lcom/google/r/b/a/uy;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/vb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/uy;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/r/b/a/uy;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 970
    new-instance v0, Lcom/google/r/b/a/uz;

    invoke-direct {v0}, Lcom/google/r/b/a/uz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/uy;->PARSER:Lcom/google/n/ax;

    .line 1014
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/uy;->d:Lcom/google/n/aw;

    .line 1125
    new-instance v0, Lcom/google/r/b/a/uy;

    invoke-direct {v0}, Lcom/google/r/b/a/uy;-><init>()V

    sput-object v0, Lcom/google/r/b/a/uy;->a:Lcom/google/r/b/a/uy;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 934
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 985
    iput-byte v0, p0, Lcom/google/r/b/a/uy;->b:B

    .line 1001
    iput v0, p0, Lcom/google/r/b/a/uy;->c:I

    .line 935
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 941
    invoke-direct {p0}, Lcom/google/r/b/a/uy;-><init>()V

    .line 943
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 945
    const/4 v0, 0x0

    .line 946
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 947
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 948
    packed-switch v3, :pswitch_data_0

    .line 953
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 955
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 951
    goto :goto_0

    .line 967
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/uy;->au:Lcom/google/n/bn;

    .line 968
    return-void

    .line 961
    :catch_0
    move-exception v0

    .line 962
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 967
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/uy;->au:Lcom/google/n/bn;

    throw v0

    .line 963
    :catch_1
    move-exception v0

    .line 964
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 965
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 948
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 932
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 985
    iput-byte v0, p0, Lcom/google/r/b/a/uy;->b:B

    .line 1001
    iput v0, p0, Lcom/google/r/b/a/uy;->c:I

    .line 933
    return-void
.end method

.method public static d()Lcom/google/r/b/a/uy;
    .locals 1

    .prologue
    .line 1128
    sget-object v0, Lcom/google/r/b/a/uy;->a:Lcom/google/r/b/a/uy;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/va;
    .locals 1

    .prologue
    .line 1076
    new-instance v0, Lcom/google/r/b/a/va;

    invoke-direct {v0}, Lcom/google/r/b/a/va;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/uy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982
    sget-object v0, Lcom/google/r/b/a/uy;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 997
    iget v0, p0, Lcom/google/r/b/a/uy;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 998
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/uy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 999
    return-void

    .line 997
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/uy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/uy;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 987
    iget-byte v1, p0, Lcom/google/r/b/a/uy;->b:B

    .line 988
    if-ne v1, v0, :cond_0

    .line 992
    :goto_0
    return v0

    .line 989
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 991
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/uy;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1003
    iget v0, p0, Lcom/google/r/b/a/uy;->c:I

    .line 1004
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1009
    :goto_0
    return v0

    .line 1006
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/uy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1008
    iput v0, p0, Lcom/google/r/b/a/uy;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 926
    invoke-static {}, Lcom/google/r/b/a/uy;->newBuilder()Lcom/google/r/b/a/va;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/va;->a(Lcom/google/r/b/a/uy;)Lcom/google/r/b/a/va;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 926
    invoke-static {}, Lcom/google/r/b/a/uy;->newBuilder()Lcom/google/r/b/a/va;

    move-result-object v0

    return-object v0
.end method
