.class public final enum Lcom/google/r/b/a/hd;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/hd;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/hd;

.field public static final enum b:Lcom/google/r/b/a/hd;

.field private static final synthetic d:[Lcom/google/r/b/a/hd;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1757
    new-instance v0, Lcom/google/r/b/a/hd;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/r/b/a/hd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hd;->a:Lcom/google/r/b/a/hd;

    .line 1761
    new-instance v0, Lcom/google/r/b/a/hd;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/r/b/a/hd;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/hd;->b:Lcom/google/r/b/a/hd;

    .line 1752
    new-array v0, v4, [Lcom/google/r/b/a/hd;

    sget-object v1, Lcom/google/r/b/a/hd;->a:Lcom/google/r/b/a/hd;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/hd;->b:Lcom/google/r/b/a/hd;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/r/b/a/hd;->d:[Lcom/google/r/b/a/hd;

    .line 1791
    new-instance v0, Lcom/google/r/b/a/he;

    invoke-direct {v0}, Lcom/google/r/b/a/he;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1800
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1801
    iput p3, p0, Lcom/google/r/b/a/hd;->c:I

    .line 1802
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/hd;
    .locals 1

    .prologue
    .line 1779
    packed-switch p0, :pswitch_data_0

    .line 1782
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1780
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/hd;->a:Lcom/google/r/b/a/hd;

    goto :goto_0

    .line 1781
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/hd;->b:Lcom/google/r/b/a/hd;

    goto :goto_0

    .line 1779
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/hd;
    .locals 1

    .prologue
    .line 1752
    const-class v0, Lcom/google/r/b/a/hd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hd;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/hd;
    .locals 1

    .prologue
    .line 1752
    sget-object v0, Lcom/google/r/b/a/hd;->d:[Lcom/google/r/b/a/hd;

    invoke-virtual {v0}, [Lcom/google/r/b/a/hd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/hd;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1775
    iget v0, p0, Lcom/google/r/b/a/hd;->c:I

    return v0
.end method
