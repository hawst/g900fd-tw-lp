.class public final enum Lcom/google/r/b/a/ao;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ao;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ao;

.field public static final enum b:Lcom/google/r/b/a/ao;

.field public static final enum c:Lcom/google/r/b/a/ao;

.field private static final synthetic e:[Lcom/google/r/b/a/ao;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 101
    new-instance v0, Lcom/google/r/b/a/ao;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/ao;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ao;->a:Lcom/google/r/b/a/ao;

    .line 105
    new-instance v0, Lcom/google/r/b/a/ao;

    const-string v1, "YELLOW_STRIPE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/ao;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ao;->b:Lcom/google/r/b/a/ao;

    .line 109
    new-instance v0, Lcom/google/r/b/a/ao;

    const-string v1, "YELLOW_HUE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/ao;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ao;->c:Lcom/google/r/b/a/ao;

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/ao;

    sget-object v1, Lcom/google/r/b/a/ao;->a:Lcom/google/r/b/a/ao;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/ao;->b:Lcom/google/r/b/a/ao;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/ao;->c:Lcom/google/r/b/a/ao;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/ao;->e:[Lcom/google/r/b/a/ao;

    .line 144
    new-instance v0, Lcom/google/r/b/a/ap;

    invoke-direct {v0}, Lcom/google/r/b/a/ap;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 154
    iput p3, p0, Lcom/google/r/b/a/ao;->d:I

    .line 155
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ao;
    .locals 1

    .prologue
    .line 131
    packed-switch p0, :pswitch_data_0

    .line 135
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 132
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/ao;->a:Lcom/google/r/b/a/ao;

    goto :goto_0

    .line 133
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ao;->b:Lcom/google/r/b/a/ao;

    goto :goto_0

    .line 134
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/ao;->c:Lcom/google/r/b/a/ao;

    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ao;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/google/r/b/a/ao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ao;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ao;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/google/r/b/a/ao;->e:[Lcom/google/r/b/a/ao;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ao;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ao;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/google/r/b/a/ao;->d:I

    return v0
.end method
