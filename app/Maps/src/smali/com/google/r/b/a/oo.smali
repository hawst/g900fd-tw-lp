.class public final Lcom/google/r/b/a/oo;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/om;",
        "Lcom/google/r/b/a/oo;",
        ">;",
        "Lcom/google/r/b/a/ot;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public d:Z

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 475
    sget-object v0, Lcom/google/r/b/a/om;->f:Lcom/google/r/b/a/om;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 542
    iput v1, p0, Lcom/google/r/b/a/oo;->b:I

    .line 642
    iput v1, p0, Lcom/google/r/b/a/oo;->e:I

    .line 476
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 467
    new-instance v2, Lcom/google/r/b/a/om;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/om;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/oo;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/oo;->b:I

    iput v1, v2, Lcom/google/r/b/a/om;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/r/b/a/oo;->c:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/om;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/r/b/a/oo;->d:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/om;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/oo;->e:I

    iput v1, v2, Lcom/google/r/b/a/om;->e:I

    iput v0, v2, Lcom/google/r/b/a/om;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 467
    check-cast p1, Lcom/google/r/b/a/om;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/oo;->a(Lcom/google/r/b/a/om;)Lcom/google/r/b/a/oo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/om;)Lcom/google/r/b/a/oo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 519
    invoke-static {}, Lcom/google/r/b/a/om;->d()Lcom/google/r/b/a/om;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 533
    :goto_0
    return-object p0

    .line 520
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 521
    iget v2, p1, Lcom/google/r/b/a/om;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/op;->a(I)Lcom/google/r/b/a/op;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/op;->a:Lcom/google/r/b/a/op;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 520
    goto :goto_1

    .line 521
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/oo;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/oo;->a:I

    iget v2, v2, Lcom/google/r/b/a/op;->e:I

    iput v2, p0, Lcom/google/r/b/a/oo;->b:I

    .line 523
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 524
    iget-boolean v2, p1, Lcom/google/r/b/a/om;->c:Z

    iget v3, p0, Lcom/google/r/b/a/oo;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/oo;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/oo;->c:Z

    .line 526
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 527
    iget-boolean v2, p1, Lcom/google/r/b/a/om;->d:Z

    iget v3, p0, Lcom/google/r/b/a/oo;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/oo;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/oo;->d:Z

    .line 529
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/om;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_4
    if-eqz v0, :cond_8

    .line 530
    iget v0, p1, Lcom/google/r/b/a/om;->e:I

    invoke-static {v0}, Lcom/google/r/b/a/or;->a(I)Lcom/google/r/b/a/or;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/r/b/a/or;->a:Lcom/google/r/b/a/or;

    :cond_7
    invoke-virtual {p0, v0}, Lcom/google/r/b/a/oo;->a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oo;

    .line 532
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/om;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_9
    move v2, v1

    .line 523
    goto :goto_2

    :cond_a
    move v2, v1

    .line 526
    goto :goto_3

    :cond_b
    move v0, v1

    .line 529
    goto :goto_4
.end method

.method public final a(Lcom/google/r/b/a/or;)Lcom/google/r/b/a/oo;
    .locals 1

    .prologue
    .line 660
    if-nez p1, :cond_0

    .line 661
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 663
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/oo;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/oo;->a:I

    .line 664
    iget v0, p1, Lcom/google/r/b/a/or;->g:I

    iput v0, p0, Lcom/google/r/b/a/oo;->e:I

    .line 666
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 537
    const/4 v0, 0x1

    return v0
.end method
