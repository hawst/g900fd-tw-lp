.class public final Lcom/google/r/b/a/lw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/lz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/lw;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/r/b/a/lw;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/r/b/a/ay;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/mi;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/lang/Object;

.field public e:Lcom/google/n/aq;

.field public f:Lcom/google/r/b/a/ma;

.field public g:Lcom/google/maps/g/ea;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3484
    new-instance v0, Lcom/google/r/b/a/lx;

    invoke-direct {v0}, Lcom/google/r/b/a/lx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/lw;->PARSER:Lcom/google/n/ax;

    .line 3739
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/lw;->k:Lcom/google/n/aw;

    .line 4415
    new-instance v0, Lcom/google/r/b/a/lw;

    invoke-direct {v0}, Lcom/google/r/b/a/lw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/lw;->h:Lcom/google/r/b/a/lw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3376
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3651
    iput-byte v0, p0, Lcom/google/r/b/a/lw;->i:B

    .line 3697
    iput v0, p0, Lcom/google/r/b/a/lw;->j:I

    .line 3377
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    .line 3378
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/lw;->d:Ljava/lang/Object;

    .line 3379
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    .line 3380
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/16 v7, 0x8

    .line 3386
    invoke-direct {p0}, Lcom/google/r/b/a/lw;-><init>()V

    .line 3389
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v6

    move v4, v0

    move v1, v0

    .line 3392
    :cond_0
    :goto_0
    if-nez v4, :cond_8

    .line 3393
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 3394
    sparse-switch v0, :sswitch_data_0

    .line 3399
    invoke-virtual {v6, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v5

    .line 3401
    goto :goto_0

    :sswitch_0
    move v4, v5

    .line 3397
    goto :goto_0

    .line 3407
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_d

    .line 3408
    iget-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    invoke-static {}, Lcom/google/r/b/a/ay;->newBuilder()Lcom/google/r/b/a/ba;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v0

    move-object v2, v0

    .line 3410
    :goto_1
    sget-object v0, Lcom/google/r/b/a/ay;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ay;

    iput-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    .line 3411
    if-eqz v2, :cond_1

    .line 3412
    iget-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    .line 3413
    invoke-virtual {v2}, Lcom/google/r/b/a/ba;->c()Lcom/google/r/b/a/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    .line 3415
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/lw;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3469
    :catch_0
    move-exception v0

    .line 3470
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3475
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v8, :cond_2

    .line 3476
    iget-object v2, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    .line 3478
    :cond_2
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_3

    .line 3479
    iget-object v1, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    .line 3481
    :cond_3
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/lw;->au:Lcom/google/n/bn;

    throw v0

    .line 3419
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v8, :cond_4

    .line 3420
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    .line 3421
    or-int/lit8 v1, v1, 0x2

    .line 3423
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/mi;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3471
    :catch_1
    move-exception v0

    .line 3472
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 3473
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3427
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 3428
    iget v2, p0, Lcom/google/r/b/a/lw;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/lw;->a:I

    .line 3429
    iput-object v0, p0, Lcom/google/r/b/a/lw;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 3433
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 3434
    and-int/lit8 v2, v1, 0x8

    if-eq v2, v7, :cond_5

    .line 3435
    new-instance v2, Lcom/google/n/ap;

    invoke-direct {v2}, Lcom/google/n/ap;-><init>()V

    iput-object v2, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    .line 3436
    or-int/lit8 v1, v1, 0x8

    .line 3438
    :cond_5
    iget-object v2, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 3443
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_c

    .line 3444
    iget-object v0, p0, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    invoke-static {v0}, Lcom/google/r/b/a/ma;->a(Lcom/google/r/b/a/ma;)Lcom/google/r/b/a/mc;

    move-result-object v0

    move-object v2, v0

    .line 3446
    :goto_2
    sget-object v0, Lcom/google/r/b/a/ma;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ma;

    iput-object v0, p0, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    .line 3447
    if-eqz v2, :cond_6

    .line 3448
    iget-object v0, p0, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/mc;->a(Lcom/google/r/b/a/ma;)Lcom/google/r/b/a/mc;

    .line 3449
    invoke-virtual {v2}, Lcom/google/r/b/a/mc;->c()Lcom/google/r/b/a/ma;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    .line 3451
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/lw;->a:I

    goto/16 :goto_0

    .line 3456
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_b

    .line 3457
    iget-object v0, p0, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    invoke-static {}, Lcom/google/maps/g/ea;->newBuilder()Lcom/google/maps/g/ec;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/ec;->a(Lcom/google/maps/g/ea;)Lcom/google/maps/g/ec;

    move-result-object v0

    move-object v2, v0

    .line 3459
    :goto_3
    sget-object v0, Lcom/google/maps/g/ea;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ea;

    iput-object v0, p0, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    .line 3460
    if-eqz v2, :cond_7

    .line 3461
    iget-object v0, p0, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/ec;->a(Lcom/google/maps/g/ea;)Lcom/google/maps/g/ec;

    .line 3462
    invoke-virtual {v2}, Lcom/google/maps/g/ec;->c()Lcom/google/maps/g/ea;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    .line 3464
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/lw;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 3475
    :cond_8
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v8, :cond_9

    .line 3476
    iget-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    .line 3478
    :cond_9
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_a

    .line 3479
    iget-object v0, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    .line 3481
    :cond_a
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->au:Lcom/google/n/bn;

    .line 3482
    return-void

    :cond_b
    move-object v2, v3

    goto :goto_3

    :cond_c
    move-object v2, v3

    goto :goto_2

    :cond_d
    move-object v2, v3

    goto/16 :goto_1

    .line 3394
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3374
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3651
    iput-byte v0, p0, Lcom/google/r/b/a/lw;->i:B

    .line 3697
    iput v0, p0, Lcom/google/r/b/a/lw;->j:I

    .line 3375
    return-void
.end method

.method public static d()Lcom/google/r/b/a/lw;
    .locals 1

    .prologue
    .line 4418
    sget-object v0, Lcom/google/r/b/a/lw;->h:Lcom/google/r/b/a/lw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ly;
    .locals 1

    .prologue
    .line 3801
    new-instance v0, Lcom/google/r/b/a/ly;

    invoke-direct {v0}, Lcom/google/r/b/a/ly;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/lw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3496
    sget-object v0, Lcom/google/r/b/a/lw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3675
    invoke-virtual {p0}, Lcom/google/r/b/a/lw;->c()I

    .line 3676
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3677
    iget-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    :cond_0
    move v1, v2

    .line 3679
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 3680
    iget-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 3679
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 3677
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    goto :goto_0

    .line 3682
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_3

    .line 3683
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/lw;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3685
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 3686
    iget-object v0, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v0, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3685
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 3683
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 3688
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_6

    .line 3689
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/r/b/a/ma;->d()Lcom/google/r/b/a/ma;

    move-result-object v0

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 3691
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 3692
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/maps/g/ea;->d()Lcom/google/maps/g/ea;

    move-result-object v0

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 3694
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/lw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3695
    return-void

    .line 3689
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    goto :goto_4

    .line 3692
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3653
    iget-byte v0, p0, Lcom/google/r/b/a/lw;->i:B

    .line 3654
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 3670
    :cond_0
    :goto_0
    return v2

    .line 3655
    :cond_1
    if-eqz v0, :cond_0

    .line 3657
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_4

    .line 3658
    iget-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/r/b/a/ay;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3659
    iput-byte v2, p0, Lcom/google/r/b/a/lw;->i:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 3657
    goto :goto_1

    .line 3658
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    goto :goto_2

    :cond_4
    move v1, v2

    .line 3663
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 3664
    iget-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/mi;

    invoke-virtual {v0}, Lcom/google/r/b/a/mi;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 3665
    iput-byte v2, p0, Lcom/google/r/b/a/lw;->i:B

    goto :goto_0

    .line 3663
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 3669
    :cond_6
    iput-byte v3, p0, Lcom/google/r/b/a/lw;->i:B

    move v2, v3

    .line 3670
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3699
    iget v0, p0, Lcom/google/r/b/a/lw;->j:I

    .line 3700
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3734
    :goto_0
    return v0

    .line 3703
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 3705
    iget-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 3707
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 3708
    iget-object v0, p0, Lcom/google/r/b/a/lw;->c:Ljava/util/List;

    .line 3709
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3707
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 3705
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/lw;->b:Lcom/google/r/b/a/ay;

    goto :goto_1

    .line 3711
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 3712
    const/4 v2, 0x3

    .line 3713
    iget-object v0, p0, Lcom/google/r/b/a/lw;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lw;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_3
    move v0, v1

    move v2, v1

    .line 3717
    :goto_5
    iget-object v4, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 3718
    iget-object v4, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    .line 3719
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 3717
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 3713
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 3721
    :cond_5
    add-int v0, v3, v2

    .line 3722
    iget-object v2, p0, Lcom/google/r/b/a/lw;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 3724
    iget v0, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_9

    .line 3725
    const/4 v3, 0x5

    .line 3726
    iget-object v0, p0, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/r/b/a/ma;->d()Lcom/google/r/b/a/ma;

    move-result-object v0

    :goto_6
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 3728
    :goto_7
    iget v2, p0, Lcom/google/r/b/a/lw;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_6

    .line 3729
    const/4 v3, 0x6

    .line 3730
    iget-object v2, p0, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    if-nez v2, :cond_8

    invoke-static {}, Lcom/google/maps/g/ea;->d()Lcom/google/maps/g/ea;

    move-result-object v2

    :goto_8
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 3732
    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/lw;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3733
    iput v0, p0, Lcom/google/r/b/a/lw;->j:I

    goto/16 :goto_0

    .line 3726
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/lw;->f:Lcom/google/r/b/a/ma;

    goto :goto_6

    .line 3730
    :cond_8
    iget-object v2, p0, Lcom/google/r/b/a/lw;->g:Lcom/google/maps/g/ea;

    goto :goto_8

    :cond_9
    move v0, v2

    goto :goto_7

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3368
    invoke-static {}, Lcom/google/r/b/a/lw;->newBuilder()Lcom/google/r/b/a/ly;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ly;->a(Lcom/google/r/b/a/lw;)Lcom/google/r/b/a/ly;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3368
    invoke-static {}, Lcom/google/r/b/a/lw;->newBuilder()Lcom/google/r/b/a/ly;

    move-result-object v0

    return-object v0
.end method
