.class public final Lcom/google/r/b/a/al;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/al;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/al;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Z

.field c:I

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/r/b/a/am;

    invoke-direct {v0}, Lcom/google/r/b/a/am;-><init>()V

    sput-object v0, Lcom/google/r/b/a/al;->PARSER:Lcom/google/n/ax;

    .line 257
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/al;->h:Lcom/google/n/aw;

    .line 499
    new-instance v0, Lcom/google/r/b/a/al;

    invoke-direct {v0}, Lcom/google/r/b/a/al;-><init>()V

    sput-object v0, Lcom/google/r/b/a/al;->e:Lcom/google/r/b/a/al;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 207
    iput-byte v1, p0, Lcom/google/r/b/a/al;->f:B

    .line 232
    iput v1, p0, Lcom/google/r/b/a/al;->g:I

    .line 18
    iput-boolean v0, p0, Lcom/google/r/b/a/al;->b:Z

    .line 19
    iput v0, p0, Lcom/google/r/b/a/al;->c:I

    .line 20
    iput-boolean v0, p0, Lcom/google/r/b/a/al;->d:Z

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/r/b/a/al;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 33
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 35
    sparse-switch v0, :sswitch_data_0

    .line 40
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/al;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/al;->a:I

    .line 48
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/al;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/al;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 48
    goto :goto_1

    .line 52
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 53
    invoke-static {v0}, Lcom/google/r/b/a/ao;->a(I)Lcom/google/r/b/a/ao;

    move-result-object v5

    .line 54
    if-nez v5, :cond_2

    .line 55
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 72
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 73
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :cond_2
    :try_start_4
    iget v5, p0, Lcom/google/r/b/a/al;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/al;->a:I

    .line 58
    iput v0, p0, Lcom/google/r/b/a/al;->c:I

    goto :goto_0

    .line 63
    :sswitch_3
    iget v0, p0, Lcom/google/r/b/a/al;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/al;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/al;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 75
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/al;->au:Lcom/google/n/bn;

    .line 76
    return-void

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 207
    iput-byte v0, p0, Lcom/google/r/b/a/al;->f:B

    .line 232
    iput v0, p0, Lcom/google/r/b/a/al;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/al;
    .locals 1

    .prologue
    .line 502
    sget-object v0, Lcom/google/r/b/a/al;->e:Lcom/google/r/b/a/al;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/an;
    .locals 1

    .prologue
    .line 319
    new-instance v0, Lcom/google/r/b/a/an;

    invoke-direct {v0}, Lcom/google/r/b/a/an;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/al;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/google/r/b/a/al;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 219
    invoke-virtual {p0}, Lcom/google/r/b/a/al;->c()I

    .line 220
    iget v0, p0, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 221
    iget-boolean v0, p0, Lcom/google/r/b/a/al;->b:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IZ)V

    .line 223
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 224
    iget v0, p0, Lcom/google/r/b/a/al;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 226
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 227
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/al;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 229
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/al;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 230
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 209
    iget-byte v1, p0, Lcom/google/r/b/a/al;->f:B

    .line 210
    if-ne v1, v0, :cond_0

    .line 214
    :goto_0
    return v0

    .line 211
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/al;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 234
    iget v0, p0, Lcom/google/r/b/a/al;->g:I

    .line 235
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 252
    :goto_0
    return v0

    .line 238
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 239
    iget-boolean v0, p0, Lcom/google/r/b/a/al;->b:Z

    .line 240
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 242
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 243
    iget v2, p0, Lcom/google/r/b/a/al;->c:I

    .line 244
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 246
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/al;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 247
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/r/b/a/al;->d:Z

    .line 248
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 250
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/al;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    iput v0, p0, Lcom/google/r/b/a/al;->g:I

    goto :goto_0

    .line 244
    :cond_3
    const/16 v2, 0xa

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/al;->newBuilder()Lcom/google/r/b/a/an;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/an;->a(Lcom/google/r/b/a/al;)Lcom/google/r/b/a/an;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/al;->newBuilder()Lcom/google/r/b/a/an;

    move-result-object v0

    return-object v0
.end method
