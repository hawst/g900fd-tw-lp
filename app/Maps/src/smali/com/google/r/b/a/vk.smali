.class public final Lcom/google/r/b/a/vk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/vp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/vk;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/vk;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 621
    new-instance v0, Lcom/google/r/b/a/vl;

    invoke-direct {v0}, Lcom/google/r/b/a/vl;-><init>()V

    sput-object v0, Lcom/google/r/b/a/vk;->PARSER:Lcom/google/n/ax;

    .line 747
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/vk;->f:Lcom/google/n/aw;

    .line 907
    new-instance v0, Lcom/google/r/b/a/vk;

    invoke-direct {v0}, Lcom/google/r/b/a/vk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/vk;->c:Lcom/google/r/b/a/vk;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 572
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 711
    iput-byte v0, p0, Lcom/google/r/b/a/vk;->d:B

    .line 730
    iput v0, p0, Lcom/google/r/b/a/vk;->e:I

    .line 573
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/vk;->b:I

    .line 574
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 580
    invoke-direct {p0}, Lcom/google/r/b/a/vk;-><init>()V

    .line 581
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 585
    const/4 v0, 0x0

    .line 586
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 587
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 588
    sparse-switch v3, :sswitch_data_0

    .line 593
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 595
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 591
    goto :goto_0

    .line 600
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 601
    invoke-static {v3}, Lcom/google/r/b/a/vn;->a(I)Lcom/google/r/b/a/vn;

    move-result-object v4

    .line 602
    if-nez v4, :cond_1

    .line 603
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 612
    :catch_0
    move-exception v0

    .line 613
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/vk;->au:Lcom/google/n/bn;

    throw v0

    .line 605
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/vk;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/vk;->a:I

    .line 606
    iput v3, p0, Lcom/google/r/b/a/vk;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 614
    :catch_1
    move-exception v0

    .line 615
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 616
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 618
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vk;->au:Lcom/google/n/bn;

    .line 619
    return-void

    .line 588
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 570
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 711
    iput-byte v0, p0, Lcom/google/r/b/a/vk;->d:B

    .line 730
    iput v0, p0, Lcom/google/r/b/a/vk;->e:I

    .line 571
    return-void
.end method

.method public static d()Lcom/google/r/b/a/vk;
    .locals 1

    .prologue
    .line 910
    sget-object v0, Lcom/google/r/b/a/vk;->c:Lcom/google/r/b/a/vk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/vm;
    .locals 1

    .prologue
    .line 809
    new-instance v0, Lcom/google/r/b/a/vm;

    invoke-direct {v0}, Lcom/google/r/b/a/vm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/vk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633
    sget-object v0, Lcom/google/r/b/a/vk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 723
    invoke-virtual {p0}, Lcom/google/r/b/a/vk;->c()I

    .line 724
    iget v0, p0, Lcom/google/r/b/a/vk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 725
    iget v0, p0, Lcom/google/r/b/a/vk;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 727
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/vk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 728
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 713
    iget-byte v1, p0, Lcom/google/r/b/a/vk;->d:B

    .line 714
    if-ne v1, v0, :cond_0

    .line 718
    :goto_0
    return v0

    .line 715
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 717
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/vk;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 732
    iget v1, p0, Lcom/google/r/b/a/vk;->e:I

    .line 733
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 742
    :goto_0
    return v0

    .line 736
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/vk;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 737
    iget v1, p0, Lcom/google/r/b/a/vk;->b:I

    .line 738
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_2

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 740
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/vk;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 741
    iput v0, p0, Lcom/google/r/b/a/vk;->e:I

    goto :goto_0

    .line 738
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 564
    invoke-static {}, Lcom/google/r/b/a/vk;->newBuilder()Lcom/google/r/b/a/vm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/vm;->a(Lcom/google/r/b/a/vk;)Lcom/google/r/b/a/vm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 564
    invoke-static {}, Lcom/google/r/b/a/vk;->newBuilder()Lcom/google/r/b/a/vm;

    move-result-object v0

    return-object v0
.end method
