.class public final Lcom/google/r/b/a/zk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/zi;",
        "Lcom/google/r/b/a/zk;",
        ">;",
        "Lcom/google/r/b/a/zl;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 859
    sget-object v0, Lcom/google/r/b/a/zi;->f:Lcom/google/r/b/a/zi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 934
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/r/b/a/zk;->b:I

    .line 998
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    .line 860
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 851
    new-instance v2, Lcom/google/r/b/a/zi;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/zi;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/zk;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/zk;->b:I

    iput v1, v2, Lcom/google/r/b/a/zi;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/zk;->d:I

    iput v1, v2, Lcom/google/r/b/a/zi;->c:I

    iget v1, p0, Lcom/google/r/b/a/zk;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/zk;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/zk;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/zk;->c:I

    iput v1, v2, Lcom/google/r/b/a/zi;->e:I

    iput v0, v2, Lcom/google/r/b/a/zi;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 851
    check-cast p1, Lcom/google/r/b/a/zi;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/zk;->a(Lcom/google/r/b/a/zi;)Lcom/google/r/b/a/zk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/zi;)Lcom/google/r/b/a/zk;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 904
    invoke-static {}, Lcom/google/r/b/a/zi;->d()Lcom/google/r/b/a/zi;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 925
    :goto_0
    return-object p0

    .line 905
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 906
    iget v2, p1, Lcom/google/r/b/a/zi;->b:I

    iget v3, p0, Lcom/google/r/b/a/zk;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/zk;->a:I

    iput v2, p0, Lcom/google/r/b/a/zk;->b:I

    .line 908
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 909
    iget v2, p1, Lcom/google/r/b/a/zi;->c:I

    iget v3, p0, Lcom/google/r/b/a/zk;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/zk;->a:I

    iput v2, p0, Lcom/google/r/b/a/zk;->d:I

    .line 911
    :cond_2
    iget-object v2, p1, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 912
    iget-object v2, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 913
    iget-object v2, p1, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    .line 914
    iget v2, p0, Lcom/google/r/b/a/zk;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/r/b/a/zk;->a:I

    .line 921
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_9

    :goto_4
    if-eqz v0, :cond_4

    .line 922
    iget v0, p1, Lcom/google/r/b/a/zi;->e:I

    iget v1, p0, Lcom/google/r/b/a/zk;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/zk;->a:I

    iput v0, p0, Lcom/google/r/b/a/zk;->c:I

    .line 924
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/zi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 905
    goto :goto_1

    :cond_6
    move v2, v1

    .line 908
    goto :goto_2

    .line 916
    :cond_7
    iget v2, p0, Lcom/google/r/b/a/zk;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/zk;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/zk;->a:I

    .line 917
    :cond_8
    iget-object v2, p0, Lcom/google/r/b/a/zk;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_9
    move v0, v1

    .line 921
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 929
    const/4 v0, 0x1

    return v0
.end method
