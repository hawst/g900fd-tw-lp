.class public final Lcom/google/r/b/a/mi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/mv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/mi;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/r/b/a/mi;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/r/b/a/ay;

.field public c:Lcom/google/r/b/a/ay;

.field d:Ljava/lang/Object;

.field e:Lcom/google/n/aq;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation
.end field

.field g:I

.field public h:Lcom/google/r/b/a/ml;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 324
    new-instance v0, Lcom/google/r/b/a/mj;

    invoke-direct {v0}, Lcom/google/r/b/a/mj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/mi;->PARSER:Lcom/google/n/ax;

    .line 1779
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/mi;->l:Lcom/google/n/aw;

    .line 2506
    new-instance v0, Lcom/google/r/b/a/mi;

    invoke-direct {v0}, Lcom/google/r/b/a/mi;-><init>()V

    sput-object v0, Lcom/google/r/b/a/mi;->i:Lcom/google/r/b/a/mi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 204
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1678
    iput-byte v0, p0, Lcom/google/r/b/a/mi;->j:B

    .line 1733
    iput v0, p0, Lcom/google/r/b/a/mi;->k:I

    .line 205
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/mi;->d:Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    .line 207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    .line 208
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/mi;->g:I

    .line 209
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/16 v8, 0x8

    const/4 v5, 0x1

    const/16 v7, 0x10

    .line 215
    invoke-direct {p0}, Lcom/google/r/b/a/mi;-><init>()V

    .line 218
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v6

    move v4, v0

    move v1, v0

    .line 221
    :cond_0
    :goto_0
    if-nez v4, :cond_9

    .line 222
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 223
    sparse-switch v0, :sswitch_data_0

    .line 228
    invoke-virtual {v6, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v5

    .line 230
    goto :goto_0

    :sswitch_0
    move v4, v5

    .line 226
    goto :goto_0

    .line 236
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_e

    .line 237
    iget-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    invoke-static {}, Lcom/google/r/b/a/ay;->newBuilder()Lcom/google/r/b/a/ba;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v0

    move-object v2, v0

    .line 239
    :goto_1
    sget-object v0, Lcom/google/r/b/a/ay;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ay;

    iput-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    .line 240
    if-eqz v2, :cond_1

    .line 241
    iget-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    .line 242
    invoke-virtual {v2}, Lcom/google/r/b/a/ba;->c()Lcom/google/r/b/a/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    .line 244
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/mi;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 310
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x8

    if-ne v2, v8, :cond_2

    .line 316
    iget-object v2, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    .line 318
    :cond_2
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_3

    .line 319
    iget-object v1, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    .line 321
    :cond_3
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/mi;->au:Lcom/google/n/bn;

    throw v0

    .line 249
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_d

    .line 250
    iget-object v0, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    invoke-static {}, Lcom/google/r/b/a/ay;->newBuilder()Lcom/google/r/b/a/ba;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v0

    move-object v2, v0

    .line 252
    :goto_2
    sget-object v0, Lcom/google/r/b/a/ay;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ay;

    iput-object v0, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    .line 253
    if-eqz v2, :cond_4

    .line 254
    iget-object v0, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    .line 255
    invoke-virtual {v2}, Lcom/google/r/b/a/ba;->c()Lcom/google/r/b/a/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    .line 257
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/mi;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 311
    :catch_1
    move-exception v0

    .line 312
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 313
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 261
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 262
    invoke-static {v0}, Lcom/google/r/b/a/mt;->a(I)Lcom/google/r/b/a/mt;

    move-result-object v2

    .line 263
    if-nez v2, :cond_5

    .line 264
    const/4 v2, 0x3

    invoke-virtual {v6, v2, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 266
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/mi;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/mi;->a:I

    .line 267
    iput v0, p0, Lcom/google/r/b/a/mi;->g:I

    goto/16 :goto_0

    .line 272
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 273
    iget v2, p0, Lcom/google/r/b/a/mi;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/mi;->a:I

    .line 274
    iput-object v0, p0, Lcom/google/r/b/a/mi;->d:Ljava/lang/Object;

    goto/16 :goto_0

    .line 278
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 279
    and-int/lit8 v2, v1, 0x8

    if-eq v2, v8, :cond_6

    .line 280
    new-instance v2, Lcom/google/n/ap;

    invoke-direct {v2}, Lcom/google/n/ap;-><init>()V

    iput-object v2, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    .line 281
    or-int/lit8 v1, v1, 0x8

    .line 283
    :cond_6
    iget-object v2, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 287
    :sswitch_6
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v7, :cond_7

    .line 288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    .line 289
    or-int/lit8 v1, v1, 0x10

    .line 291
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/aje;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 296
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_c

    .line 297
    iget-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    invoke-static {v0}, Lcom/google/r/b/a/ml;->a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;

    move-result-object v0

    move-object v2, v0

    .line 299
    :goto_3
    sget-object v0, Lcom/google/r/b/a/ml;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ml;

    iput-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    .line 300
    if-eqz v2, :cond_8

    .line 301
    iget-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/mn;->a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;

    .line 302
    invoke-virtual {v2}, Lcom/google/r/b/a/mn;->c()Lcom/google/r/b/a/ml;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    .line 304
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/mi;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 315
    :cond_9
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v8, :cond_a

    .line 316
    iget-object v0, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    .line 318
    :cond_a
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v7, :cond_b

    .line 319
    iget-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    .line 321
    :cond_b
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->au:Lcom/google/n/bn;

    .line 322
    return-void

    :cond_c
    move-object v2, v3

    goto :goto_3

    :cond_d
    move-object v2, v3

    goto/16 :goto_2

    :cond_e
    move-object v2, v3

    goto/16 :goto_1

    .line 223
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 202
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1678
    iput-byte v0, p0, Lcom/google/r/b/a/mi;->j:B

    .line 1733
    iput v0, p0, Lcom/google/r/b/a/mi;->k:I

    .line 203
    return-void
.end method

.method public static d()Lcom/google/r/b/a/mi;
    .locals 1

    .prologue
    .line 2509
    sget-object v0, Lcom/google/r/b/a/mi;->i:Lcom/google/r/b/a/mi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/mk;
    .locals 1

    .prologue
    .line 1841
    new-instance v0, Lcom/google/r/b/a/mk;

    invoke-direct {v0}, Lcom/google/r/b/a/mk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/mi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336
    sget-object v0, Lcom/google/r/b/a/mi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1708
    invoke-virtual {p0}, Lcom/google/r/b/a/mi;->c()I

    .line 1709
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1710
    iget-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1712
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1713
    iget-object v0, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1715
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    .line 1716
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/r/b/a/mi;->g:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->b(II)V

    .line 1718
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 1719
    iget-object v0, p0, Lcom/google/r/b/a/mi;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_3
    move v0, v1

    .line 1721
    :goto_3
    iget-object v2, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 1722
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1721
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1710
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    goto :goto_0

    .line 1713
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    goto :goto_1

    .line 1719
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 1724
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1725
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1724
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1727
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_9

    .line 1728
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/r/b/a/ml;->d()Lcom/google/r/b/a/ml;

    move-result-object v0

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1730
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/mi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1731
    return-void

    .line 1728
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1680
    iget-byte v0, p0, Lcom/google/r/b/a/mi;->j:B

    .line 1681
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1703
    :goto_0
    return v0

    .line 1682
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1684
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 1685
    iget-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/r/b/a/ay;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1686
    iput-byte v2, p0, Lcom/google/r/b/a/mi;->j:B

    move v0, v2

    .line 1687
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1684
    goto :goto_1

    .line 1685
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    goto :goto_2

    .line 1690
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 1691
    iget-object v0, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/r/b/a/ay;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1692
    iput-byte v2, p0, Lcom/google/r/b/a/mi;->j:B

    move v0, v2

    .line 1693
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1690
    goto :goto_3

    .line 1691
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    goto :goto_4

    .line 1696
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    .line 1697
    iget-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/r/b/a/ml;->d()Lcom/google/r/b/a/ml;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, Lcom/google/r/b/a/ml;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1698
    iput-byte v2, p0, Lcom/google/r/b/a/mi;->j:B

    move v0, v2

    .line 1699
    goto :goto_0

    :cond_8
    move v0, v2

    .line 1696
    goto :goto_5

    .line 1697
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    goto :goto_6

    .line 1702
    :cond_a
    iput-byte v1, p0, Lcom/google/r/b/a/mi;->j:B

    move v0, v1

    .line 1703
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1735
    iget v0, p0, Lcom/google/r/b/a/mi;->k:I

    .line 1736
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1774
    :goto_0
    return v0

    .line 1739
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_c

    .line 1741
    iget-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1743
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1745
    iget-object v2, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1747
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    .line 1748
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/r/b/a/mi;->g:I

    .line 1749
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_5

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 1751
    :goto_5
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 1753
    iget-object v0, p0, Lcom/google/r/b/a/mi;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mi;->d:Ljava/lang/Object;

    :goto_6
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    :cond_2
    move v0, v1

    move v3, v1

    .line 1757
    :goto_7
    iget-object v4, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_7

    .line 1758
    iget-object v4, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    .line 1759
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 1757
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1741
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    goto/16 :goto_1

    .line 1745
    :cond_4
    iget-object v2, p0, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    goto/16 :goto_3

    .line 1749
    :cond_5
    const/16 v2, 0xa

    goto :goto_4

    .line 1753
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 1761
    :cond_7
    add-int v0, v2, v3

    .line 1762
    iget-object v2, p0, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v1

    move v3, v0

    .line 1764
    :goto_8
    iget-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 1765
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    .line 1766
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1764
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 1768
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_9

    .line 1769
    const/4 v2, 0x7

    .line 1770
    iget-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/r/b/a/ml;->d()Lcom/google/r/b/a/ml;

    move-result-object v0

    :goto_9
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1772
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/mi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1773
    iput v0, p0, Lcom/google/r/b/a/mi;->k:I

    goto/16 :goto_0

    .line 1770
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    goto :goto_9

    :cond_b
    move v2, v0

    goto/16 :goto_5

    :cond_c
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 196
    invoke-static {}, Lcom/google/r/b/a/mi;->newBuilder()Lcom/google/r/b/a/mk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/mk;->a(Lcom/google/r/b/a/mi;)Lcom/google/r/b/a/mk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 196
    invoke-static {}, Lcom/google/r/b/a/mi;->newBuilder()Lcom/google/r/b/a/mk;

    move-result-object v0

    return-object v0
.end method
