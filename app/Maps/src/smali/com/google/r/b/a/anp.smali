.class public final Lcom/google/r/b/a/anp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/anq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ann;",
        "Lcom/google/r/b/a/anp;",
        ">;",
        "Lcom/google/r/b/a/anq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 439
    sget-object v0, Lcom/google/r/b/a/ann;->g:Lcom/google/r/b/a/ann;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 557
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/anp;->b:Ljava/lang/Object;

    .line 633
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anp;->c:Lcom/google/n/ao;

    .line 693
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    .line 830
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    .line 966
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anp;->f:Lcom/google/n/ao;

    .line 440
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 431
    new-instance v2, Lcom/google/r/b/a/ann;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ann;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, p0, Lcom/google/r/b/a/anp;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ann;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/ann;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/anp;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/anp;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/r/b/a/anp;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/r/b/a/anp;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v3, v2, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/anp;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/anp;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/ann;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 431
    check-cast p1, Lcom/google/r/b/a/ann;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/anp;->a(Lcom/google/r/b/a/ann;)Lcom/google/r/b/a/anp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ann;)Lcom/google/r/b/a/anp;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 495
    invoke-static {}, Lcom/google/r/b/a/ann;->d()Lcom/google/r/b/a/ann;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 530
    :goto_0
    return-object p0

    .line 496
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 497
    iget v2, p0, Lcom/google/r/b/a/anp;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/anp;->a:I

    .line 498
    iget-object v2, p1, Lcom/google/r/b/a/ann;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/anp;->b:Ljava/lang/Object;

    .line 501
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 502
    iget-object v2, p0, Lcom/google/r/b/a/anp;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ann;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 503
    iget v2, p0, Lcom/google/r/b/a/anp;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/anp;->a:I

    .line 505
    :cond_2
    iget-object v2, p1, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 506
    iget-object v2, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 507
    iget-object v2, p1, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    .line 508
    iget v2, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/r/b/a/anp;->a:I

    .line 515
    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 516
    iget-object v2, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 517
    iget-object v2, p1, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    .line 518
    iget v2, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/r/b/a/anp;->a:I

    .line 525
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_c

    :goto_5
    if-eqz v0, :cond_5

    .line 526
    iget-object v0, p0, Lcom/google/r/b/a/anp;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 527
    iget v0, p0, Lcom/google/r/b/a/anp;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/anp;->a:I

    .line 529
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/ann;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 496
    goto :goto_1

    :cond_7
    move v2, v1

    .line 501
    goto :goto_2

    .line 510
    :cond_8
    iget v2, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/anp;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/anp;->a:I

    .line 511
    :cond_9
    iget-object v2, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 520
    :cond_a
    iget v2, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-eq v2, v3, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/anp;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/anp;->a:I

    .line 521
    :cond_b
    iget-object v2, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_c
    move v0, v1

    .line 525
    goto :goto_5
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 534
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 535
    iget-object v0, p0, Lcom/google/r/b/a/anp;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/anr;->d()Lcom/google/r/b/a/anr;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anr;

    invoke-virtual {v0}, Lcom/google/r/b/a/anr;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 552
    :cond_0
    :goto_1
    return v2

    .line 534
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 540
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 541
    iget-object v0, p0, Lcom/google/r/b/a/anp;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/be;->d()Lcom/google/r/b/a/be;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 546
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/anp;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 547
    iget-object v0, p0, Lcom/google/r/b/a/anp;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/anr;->d()Lcom/google/r/b/a/anr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anr;

    invoke-virtual {v0}, Lcom/google/r/b/a/anr;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 552
    goto :goto_1

    :cond_5
    move v0, v2

    .line 546
    goto :goto_3
.end method
