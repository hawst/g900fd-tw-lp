.class public final Lcom/google/r/b/a/lo;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/lr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/lo;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/lo;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/maps/g/co;

.field c:Z

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4981
    new-instance v0, Lcom/google/r/b/a/lp;

    invoke-direct {v0}, Lcom/google/r/b/a/lp;-><init>()V

    sput-object v0, Lcom/google/r/b/a/lo;->PARSER:Lcom/google/n/ax;

    .line 5070
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/lo;->g:Lcom/google/n/aw;

    .line 5296
    new-instance v0, Lcom/google/r/b/a/lo;

    invoke-direct {v0}, Lcom/google/r/b/a/lo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/lo;->d:Lcom/google/r/b/a/lo;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4925
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5027
    iput-byte v0, p0, Lcom/google/r/b/a/lo;->e:B

    .line 5049
    iput v0, p0, Lcom/google/r/b/a/lo;->f:I

    .line 4926
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/lo;->c:Z

    .line 4927
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 4933
    invoke-direct {p0}, Lcom/google/r/b/a/lo;-><init>()V

    .line 4934
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v2

    .line 4939
    :cond_0
    :goto_0
    if-nez v4, :cond_3

    .line 4940
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 4941
    sparse-switch v0, :sswitch_data_0

    .line 4946
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v1

    .line 4948
    goto :goto_0

    :sswitch_0
    move v4, v1

    .line 4944
    goto :goto_0

    .line 4953
    :sswitch_1
    const/4 v0, 0x0

    .line 4954
    iget v3, p0, Lcom/google/r/b/a/lo;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_4

    .line 4955
    iget-object v0, p0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    invoke-static {}, Lcom/google/maps/g/co;->newBuilder()Lcom/google/maps/g/cq;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/cq;->a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;

    move-result-object v0

    move-object v3, v0

    .line 4957
    :goto_1
    sget-object v0, Lcom/google/maps/g/co;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/co;

    iput-object v0, p0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    .line 4958
    if-eqz v3, :cond_1

    .line 4959
    iget-object v0, p0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    invoke-virtual {v3, v0}, Lcom/google/maps/g/cq;->a(Lcom/google/maps/g/co;)Lcom/google/maps/g/cq;

    .line 4960
    invoke-virtual {v3}, Lcom/google/maps/g/cq;->c()Lcom/google/maps/g/co;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    .line 4962
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/lo;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/lo;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4972
    :catch_0
    move-exception v0

    .line 4973
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4978
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/lo;->au:Lcom/google/n/bn;

    throw v0

    .line 4966
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/lo;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/lo;->a:I

    .line 4967
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/lo;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4974
    :catch_1
    move-exception v0

    .line 4975
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4976
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v0, v2

    .line 4967
    goto :goto_2

    .line 4978
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/lo;->au:Lcom/google/n/bn;

    .line 4979
    return-void

    :cond_4
    move-object v3, v0

    goto :goto_1

    .line 4941
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4923
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5027
    iput-byte v0, p0, Lcom/google/r/b/a/lo;->e:B

    .line 5049
    iput v0, p0, Lcom/google/r/b/a/lo;->f:I

    .line 4924
    return-void
.end method

.method public static d()Lcom/google/r/b/a/lo;
    .locals 1

    .prologue
    .line 5299
    sget-object v0, Lcom/google/r/b/a/lo;->d:Lcom/google/r/b/a/lo;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/lq;
    .locals 1

    .prologue
    .line 5132
    new-instance v0, Lcom/google/r/b/a/lq;

    invoke-direct {v0}, Lcom/google/r/b/a/lq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/lo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4993
    sget-object v0, Lcom/google/r/b/a/lo;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5039
    invoke-virtual {p0}, Lcom/google/r/b/a/lo;->c()I

    .line 5040
    iget v0, p0, Lcom/google/r/b/a/lo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5041
    iget-object v0, p0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/maps/g/co;->d()Lcom/google/maps/g/co;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 5043
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/lo;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 5044
    iget-boolean v0, p0, Lcom/google/r/b/a/lo;->c:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 5046
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/lo;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5047
    return-void

    .line 5041
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5029
    iget-byte v1, p0, Lcom/google/r/b/a/lo;->e:B

    .line 5030
    if-ne v1, v0, :cond_0

    .line 5034
    :goto_0
    return v0

    .line 5031
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 5033
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/lo;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 5051
    iget v0, p0, Lcom/google/r/b/a/lo;->f:I

    .line 5052
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5065
    :goto_0
    return v0

    .line 5055
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/lo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 5057
    iget-object v0, p0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/maps/g/co;->d()Lcom/google/maps/g/co;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 5059
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/lo;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 5060
    iget-boolean v2, p0, Lcom/google/r/b/a/lo;->c:Z

    .line 5061
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5063
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/lo;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 5064
    iput v0, p0, Lcom/google/r/b/a/lo;->f:I

    goto :goto_0

    .line 5057
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/lo;->b:Lcom/google/maps/g/co;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4917
    invoke-static {}, Lcom/google/r/b/a/lo;->newBuilder()Lcom/google/r/b/a/lq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/lq;->a(Lcom/google/r/b/a/lo;)Lcom/google/r/b/a/lq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4917
    invoke-static {}, Lcom/google/r/b/a/lo;->newBuilder()Lcom/google/r/b/a/lq;

    move-result-object v0

    return-object v0
.end method
