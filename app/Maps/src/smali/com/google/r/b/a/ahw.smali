.class public final Lcom/google/r/b/a/ahw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ahz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ahw;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/ahw;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/google/r/b/a/ahx;

    invoke-direct {v0}, Lcom/google/r/b/a/ahx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ahw;->PARSER:Lcom/google/n/ax;

    .line 216
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ahw;->g:Lcom/google/n/aw;

    .line 489
    new-instance v0, Lcom/google/r/b/a/ahw;

    invoke-direct {v0}, Lcom/google/r/b/a/ahw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ahw;->d:Lcom/google/r/b/a/ahw;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 158
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ahw;->c:Lcom/google/n/ao;

    .line 173
    iput-byte v2, p0, Lcom/google/r/b/a/ahw;->e:B

    .line 195
    iput v2, p0, Lcom/google/r/b/a/ahw;->f:I

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ahw;->b:Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lcom/google/r/b/a/ahw;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 52
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Lcom/google/r/b/a/ahw;-><init>()V

    .line 59
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 64
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 65
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 66
    sparse-switch v3, :sswitch_data_0

    .line 71
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 73
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 69
    goto :goto_0

    .line 78
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 79
    iget v4, p0, Lcom/google/r/b/a/ahw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ahw;->a:I

    .line 80
    iput-object v3, p0, Lcom/google/r/b/a/ahw;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ahw;->au:Lcom/google/n/bn;

    throw v0

    .line 84
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/ahw;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 85
    iget v3, p0, Lcom/google/r/b/a/ahw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ahw;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 92
    :catch_1
    move-exception v0

    .line 93
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 94
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 96
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahw;->au:Lcom/google/n/bn;

    .line 97
    return-void

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 47
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 158
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ahw;->c:Lcom/google/n/ao;

    .line 173
    iput-byte v1, p0, Lcom/google/r/b/a/ahw;->e:B

    .line 195
    iput v1, p0, Lcom/google/r/b/a/ahw;->f:I

    .line 48
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ahw;
    .locals 1

    .prologue
    .line 492
    sget-object v0, Lcom/google/r/b/a/ahw;->d:Lcom/google/r/b/a/ahw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ahy;
    .locals 1

    .prologue
    .line 278
    new-instance v0, Lcom/google/r/b/a/ahy;

    invoke-direct {v0}, Lcom/google/r/b/a/ahy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ahw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lcom/google/r/b/a/ahw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 185
    invoke-virtual {p0}, Lcom/google/r/b/a/ahw;->c()I

    .line 186
    iget v0, p0, Lcom/google/r/b/a/ahw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/r/b/a/ahw;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahw;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 189
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ahw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 190
    iget-object v0, p0, Lcom/google/r/b/a/ahw;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/ahw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 193
    return-void

    .line 187
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 175
    iget-byte v1, p0, Lcom/google/r/b/a/ahw;->e:B

    .line 176
    if-ne v1, v0, :cond_0

    .line 180
    :goto_0
    return v0

    .line 177
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 179
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ahw;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 197
    iget v0, p0, Lcom/google/r/b/a/ahw;->f:I

    .line 198
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 211
    :goto_0
    return v0

    .line 201
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ahw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 203
    iget-object v0, p0, Lcom/google/r/b/a/ahw;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahw;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 205
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/ahw;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 206
    iget-object v2, p0, Lcom/google/r/b/a/ahw;->c:Lcom/google/n/ao;

    .line 207
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 209
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ahw;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    iput v0, p0, Lcom/google/r/b/a/ahw;->f:I

    goto :goto_0

    .line 203
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/r/b/a/ahw;->newBuilder()Lcom/google/r/b/a/ahy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ahy;->a(Lcom/google/r/b/a/ahw;)Lcom/google/r/b/a/ahy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/r/b/a/ahw;->newBuilder()Lcom/google/r/b/a/ahy;

    move-result-object v0

    return-object v0
.end method
