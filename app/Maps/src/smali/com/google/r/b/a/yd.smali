.class public final Lcom/google/r/b/a/yd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/yg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/yd;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/r/b/a/yd;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:I

.field public e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3473
    new-instance v0, Lcom/google/r/b/a/ye;

    invoke-direct {v0}, Lcom/google/r/b/a/ye;-><init>()V

    sput-object v0, Lcom/google/r/b/a/yd;->PARSER:Lcom/google/n/ax;

    .line 3736
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/yd;->k:Lcom/google/n/aw;

    .line 4243
    new-instance v0, Lcom/google/r/b/a/yd;

    invoke-direct {v0}, Lcom/google/r/b/a/yd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/yd;->h:Lcom/google/r/b/a/yd;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 3391
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3661
    iput-byte v0, p0, Lcom/google/r/b/a/yd;->i:B

    .line 3699
    iput v0, p0, Lcom/google/r/b/a/yd;->j:I

    .line 3392
    iput v1, p0, Lcom/google/r/b/a/yd;->b:I

    .line 3393
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    .line 3394
    const v0, -0x7f02078e

    iput v0, p0, Lcom/google/r/b/a/yd;->d:I

    .line 3395
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    .line 3396
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yd;->f:Ljava/lang/Object;

    .line 3397
    iput v1, p0, Lcom/google/r/b/a/yd;->g:I

    .line 3398
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 3404
    invoke-direct {p0}, Lcom/google/r/b/a/yd;-><init>()V

    .line 3405
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 3409
    const/4 v0, 0x0

    .line 3410
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 3411
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 3412
    sparse-switch v3, :sswitch_data_0

    .line 3417
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 3419
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 3415
    goto :goto_0

    .line 3424
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/yd;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/yd;->a:I

    .line 3425
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yd;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3464
    :catch_0
    move-exception v0

    .line 3465
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3470
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/yd;->au:Lcom/google/n/bn;

    throw v0

    .line 3429
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3430
    iget v4, p0, Lcom/google/r/b/a/yd;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/yd;->a:I

    .line 3431
    iput-object v3, p0, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3466
    :catch_1
    move-exception v0

    .line 3467
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3468
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3435
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/yd;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/yd;->a:I

    .line 3436
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yd;->d:I

    goto :goto_0

    .line 3440
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3441
    iget v4, p0, Lcom/google/r/b/a/yd;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/yd;->a:I

    .line 3442
    iput-object v3, p0, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    goto :goto_0

    .line 3446
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3447
    iget v4, p0, Lcom/google/r/b/a/yd;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/yd;->a:I

    .line 3448
    iput-object v3, p0, Lcom/google/r/b/a/yd;->f:Ljava/lang/Object;

    goto :goto_0

    .line 3452
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 3453
    invoke-static {v3}, Lcom/google/r/b/a/yz;->a(I)Lcom/google/r/b/a/yz;

    move-result-object v4

    .line 3454
    if-nez v4, :cond_1

    .line 3455
    const/16 v4, 0x3e

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 3457
    :cond_1
    iget v4, p0, Lcom/google/r/b/a/yd;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/yd;->a:I

    .line 3458
    iput v3, p0, Lcom/google/r/b/a/yd;->g:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 3470
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yd;->au:Lcom/google/n/bn;

    .line 3471
    return-void

    .line 3412
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a8 -> :sswitch_1
        0x1b2 -> :sswitch_2
        0x1b8 -> :sswitch_3
        0x1c2 -> :sswitch_4
        0x1ca -> :sswitch_5
        0x1f0 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3389
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3661
    iput-byte v0, p0, Lcom/google/r/b/a/yd;->i:B

    .line 3699
    iput v0, p0, Lcom/google/r/b/a/yd;->j:I

    .line 3390
    return-void
.end method

.method public static d()Lcom/google/r/b/a/yd;
    .locals 1

    .prologue
    .line 4246
    sget-object v0, Lcom/google/r/b/a/yd;->h:Lcom/google/r/b/a/yd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/yf;
    .locals 1

    .prologue
    .line 3798
    new-instance v0, Lcom/google/r/b/a/yf;

    invoke-direct {v0}, Lcom/google/r/b/a/yf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/yd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3485
    sget-object v0, Lcom/google/r/b/a/yd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 3677
    invoke-virtual {p0}, Lcom/google/r/b/a/yd;->c()I

    .line 3678
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 3679
    const/16 v0, 0x35

    iget v1, p0, Lcom/google/r/b/a/yd;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 3681
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 3682
    const/16 v1, 0x36

    iget-object v0, p0, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3684
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 3685
    const/16 v0, 0x37

    iget v1, p0, Lcom/google/r/b/a/yd;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 3687
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 3688
    const/16 v1, 0x38

    iget-object v0, p0, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3690
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 3691
    const/16 v1, 0x39

    iget-object v0, p0, Lcom/google/r/b/a/yd;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yd;->f:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3693
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 3694
    const/16 v0, 0x3e

    iget v1, p0, Lcom/google/r/b/a/yd;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 3696
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/yd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3697
    return-void

    .line 3682
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 3688
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 3691
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3663
    iget-byte v2, p0, Lcom/google/r/b/a/yd;->i:B

    .line 3664
    if-ne v2, v0, :cond_0

    .line 3672
    :goto_0
    return v0

    .line 3665
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 3667
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 3668
    iput-byte v1, p0, Lcom/google/r/b/a/yd;->i:B

    move v0, v1

    .line 3669
    goto :goto_0

    :cond_2
    move v2, v1

    .line 3667
    goto :goto_1

    .line 3671
    :cond_3
    iput-byte v0, p0, Lcom/google/r/b/a/yd;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 3701
    iget v0, p0, Lcom/google/r/b/a/yd;->j:I

    .line 3702
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3731
    :goto_0
    return v0

    .line 3705
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_c

    .line 3706
    const/16 v0, 0x35

    iget v2, p0, Lcom/google/r/b/a/yd;->b:I

    .line 3707
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_7

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 3709
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    .line 3710
    const/16 v4, 0x36

    .line 3711
    iget-object v0, p0, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yd;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 3713
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_2

    .line 3714
    const/16 v0, 0x37

    iget v4, p0, Lcom/google/r/b/a/yd;->d:I

    .line 3715
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 3717
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_3

    .line 3718
    const/16 v4, 0x38

    .line 3719
    iget-object v0, p0, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yd;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 3721
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_4

    .line 3722
    const/16 v4, 0x39

    .line 3723
    iget-object v0, p0, Lcom/google/r/b/a/yd;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yd;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 3725
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/yd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_6

    .line 3726
    const/16 v0, 0x3e

    iget v4, p0, Lcom/google/r/b/a/yd;->g:I

    .line 3727
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 3729
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/yd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 3730
    iput v0, p0, Lcom/google/r/b/a/yd;->j:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 3707
    goto/16 :goto_1

    .line 3711
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    :cond_9
    move v0, v1

    .line 3715
    goto/16 :goto_4

    .line 3719
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 3723
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_c
    move v2, v3

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3383
    invoke-static {}, Lcom/google/r/b/a/yd;->newBuilder()Lcom/google/r/b/a/yf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yf;->a(Lcom/google/r/b/a/yd;)Lcom/google/r/b/a/yf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3383
    invoke-static {}, Lcom/google/r/b/a/yd;->newBuilder()Lcom/google/r/b/a/yf;

    move-result-object v0

    return-object v0
.end method
