.class public final enum Lcom/google/r/b/a/sg;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/sg;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/sg;

.field public static final enum b:Lcom/google/r/b/a/sg;

.field public static final enum c:Lcom/google/r/b/a/sg;

.field private static final synthetic e:[Lcom/google/r/b/a/sg;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 186
    new-instance v0, Lcom/google/r/b/a/sg;

    const-string v1, "MORE_INFO_LINK"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/sg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/sg;->a:Lcom/google/r/b/a/sg;

    .line 190
    new-instance v0, Lcom/google/r/b/a/sg;

    const-string v1, "BUTTON"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/sg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/sg;->b:Lcom/google/r/b/a/sg;

    .line 194
    new-instance v0, Lcom/google/r/b/a/sg;

    const-string v1, "BUTTON_ON_CARD"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/sg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/sg;->c:Lcom/google/r/b/a/sg;

    .line 181
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/sg;

    sget-object v1, Lcom/google/r/b/a/sg;->a:Lcom/google/r/b/a/sg;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/sg;->b:Lcom/google/r/b/a/sg;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/sg;->c:Lcom/google/r/b/a/sg;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/sg;->e:[Lcom/google/r/b/a/sg;

    .line 229
    new-instance v0, Lcom/google/r/b/a/sh;

    invoke-direct {v0}, Lcom/google/r/b/a/sh;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 238
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 239
    iput p3, p0, Lcom/google/r/b/a/sg;->d:I

    .line 240
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/sg;
    .locals 1

    .prologue
    .line 216
    packed-switch p0, :pswitch_data_0

    .line 220
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 217
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/sg;->a:Lcom/google/r/b/a/sg;

    goto :goto_0

    .line 218
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/sg;->b:Lcom/google/r/b/a/sg;

    goto :goto_0

    .line 219
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/sg;->c:Lcom/google/r/b/a/sg;

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/sg;
    .locals 1

    .prologue
    .line 181
    const-class v0, Lcom/google/r/b/a/sg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/sg;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/sg;
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/google/r/b/a/sg;->e:[Lcom/google/r/b/a/sg;

    invoke-virtual {v0}, [Lcom/google/r/b/a/sg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/sg;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/google/r/b/a/sg;->d:I

    return v0
.end method
