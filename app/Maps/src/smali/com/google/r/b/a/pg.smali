.class public final Lcom/google/r/b/a/pg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ph;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/pe;",
        "Lcom/google/r/b/a/pg;",
        ">;",
        "Lcom/google/r/b/a/ph;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x7530

    .line 291
    sget-object v0, Lcom/google/r/b/a/pe;->f:Lcom/google/r/b/a/pe;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 358
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/pg;->b:I

    .line 390
    iput v1, p0, Lcom/google/r/b/a/pg;->c:I

    .line 422
    iput v1, p0, Lcom/google/r/b/a/pg;->d:I

    .line 292
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 283
    new-instance v2, Lcom/google/r/b/a/pe;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/pe;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/pg;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/pg;->b:I

    iput v1, v2, Lcom/google/r/b/a/pe;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/pg;->c:I

    iput v1, v2, Lcom/google/r/b/a/pe;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/pg;->d:I

    iput v1, v2, Lcom/google/r/b/a/pe;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/r/b/a/pg;->e:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/pe;->e:Z

    iput v0, v2, Lcom/google/r/b/a/pe;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 283
    check-cast p1, Lcom/google/r/b/a/pe;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/pg;->a(Lcom/google/r/b/a/pe;)Lcom/google/r/b/a/pg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/pe;)Lcom/google/r/b/a/pg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 335
    invoke-static {}, Lcom/google/r/b/a/pe;->d()Lcom/google/r/b/a/pe;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 349
    :goto_0
    return-object p0

    .line 336
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/pe;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 337
    iget v2, p1, Lcom/google/r/b/a/pe;->b:I

    iget v3, p0, Lcom/google/r/b/a/pg;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/pg;->a:I

    iput v2, p0, Lcom/google/r/b/a/pg;->b:I

    .line 339
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/pe;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 340
    iget v2, p1, Lcom/google/r/b/a/pe;->c:I

    iget v3, p0, Lcom/google/r/b/a/pg;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/pg;->a:I

    iput v2, p0, Lcom/google/r/b/a/pg;->c:I

    .line 342
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/pe;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 343
    iget v2, p1, Lcom/google/r/b/a/pe;->d:I

    iget v3, p0, Lcom/google/r/b/a/pg;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/pg;->a:I

    iput v2, p0, Lcom/google/r/b/a/pg;->d:I

    .line 345
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/pe;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 346
    iget-boolean v0, p1, Lcom/google/r/b/a/pe;->e:Z

    iget v1, p0, Lcom/google/r/b/a/pg;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/pg;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/pg;->e:Z

    .line 348
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/pe;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 336
    goto :goto_1

    :cond_6
    move v2, v1

    .line 339
    goto :goto_2

    :cond_7
    move v2, v1

    .line 342
    goto :goto_3

    :cond_8
    move v0, v1

    .line 345
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x1

    return v0
.end method
