.class public final Lcom/google/r/b/a/it;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/iu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ir;",
        "Lcom/google/r/b/a/it;",
        ">;",
        "Lcom/google/r/b/a/iu;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:I

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 435
    sget-object v0, Lcom/google/r/b/a/ir;->f:Lcom/google/r/b/a/ir;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 508
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/it;->b:Ljava/lang/Object;

    .line 616
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/it;->d:Ljava/lang/Object;

    .line 692
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/it;->e:Ljava/lang/Object;

    .line 436
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 427
    new-instance v2, Lcom/google/r/b/a/ir;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ir;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/it;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/it;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ir;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/it;->c:I

    iput v1, v2, Lcom/google/r/b/a/ir;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/it;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ir;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/it;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ir;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/ir;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 427
    check-cast p1, Lcom/google/r/b/a/ir;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/it;->a(Lcom/google/r/b/a/ir;)Lcom/google/r/b/a/it;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ir;)Lcom/google/r/b/a/it;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 479
    invoke-static {}, Lcom/google/r/b/a/ir;->d()Lcom/google/r/b/a/ir;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 499
    :goto_0
    return-object p0

    .line 480
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ir;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 481
    iget v2, p0, Lcom/google/r/b/a/it;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/it;->a:I

    .line 482
    iget-object v2, p1, Lcom/google/r/b/a/ir;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/it;->b:Ljava/lang/Object;

    .line 485
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ir;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 486
    iget v2, p1, Lcom/google/r/b/a/ir;->c:I

    iget v3, p0, Lcom/google/r/b/a/it;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/it;->a:I

    iput v2, p0, Lcom/google/r/b/a/it;->c:I

    .line 488
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ir;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 489
    iget v2, p0, Lcom/google/r/b/a/it;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/it;->a:I

    .line 490
    iget-object v2, p1, Lcom/google/r/b/a/ir;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/it;->d:Ljava/lang/Object;

    .line 493
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ir;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 494
    iget v0, p0, Lcom/google/r/b/a/it;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/it;->a:I

    .line 495
    iget-object v0, p1, Lcom/google/r/b/a/ir;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/it;->e:Ljava/lang/Object;

    .line 498
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/ir;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 480
    goto :goto_1

    :cond_6
    move v2, v1

    .line 485
    goto :goto_2

    :cond_7
    move v2, v1

    .line 488
    goto :goto_3

    :cond_8
    move v0, v1

    .line 493
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 503
    const/4 v0, 0x1

    return v0
.end method
