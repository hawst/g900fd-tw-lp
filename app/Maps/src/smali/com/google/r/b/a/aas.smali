.class public final Lcom/google/r/b/a/aas;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aav;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aas;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/aas;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7963
    new-instance v0, Lcom/google/r/b/a/aat;

    invoke-direct {v0}, Lcom/google/r/b/a/aat;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aas;->PARSER:Lcom/google/n/ax;

    .line 8037
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aas;->f:Lcom/google/n/aw;

    .line 8229
    new-instance v0, Lcom/google/r/b/a/aas;

    invoke-direct {v0}, Lcom/google/r/b/a/aas;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aas;->c:Lcom/google/r/b/a/aas;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 7920
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 7980
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aas;->b:Lcom/google/n/ao;

    .line 7995
    iput-byte v2, p0, Lcom/google/r/b/a/aas;->d:B

    .line 8020
    iput v2, p0, Lcom/google/r/b/a/aas;->e:I

    .line 7921
    iget-object v0, p0, Lcom/google/r/b/a/aas;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 7922
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 7928
    invoke-direct {p0}, Lcom/google/r/b/a/aas;-><init>()V

    .line 7929
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 7934
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 7935
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 7936
    sparse-switch v3, :sswitch_data_0

    .line 7941
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 7943
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 7939
    goto :goto_0

    .line 7948
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/aas;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 7949
    iget v3, p0, Lcom/google/r/b/a/aas;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aas;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 7954
    :catch_0
    move-exception v0

    .line 7955
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7960
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aas;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aas;->au:Lcom/google/n/bn;

    .line 7961
    return-void

    .line 7956
    :catch_1
    move-exception v0

    .line 7957
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 7958
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7936
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 7918
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 7980
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aas;->b:Lcom/google/n/ao;

    .line 7995
    iput-byte v1, p0, Lcom/google/r/b/a/aas;->d:B

    .line 8020
    iput v1, p0, Lcom/google/r/b/a/aas;->e:I

    .line 7919
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aas;
    .locals 1

    .prologue
    .line 8232
    sget-object v0, Lcom/google/r/b/a/aas;->c:Lcom/google/r/b/a/aas;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aau;
    .locals 1

    .prologue
    .line 8099
    new-instance v0, Lcom/google/r/b/a/aau;

    invoke-direct {v0}, Lcom/google/r/b/a/aau;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aas;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7975
    sget-object v0, Lcom/google/r/b/a/aas;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 8013
    invoke-virtual {p0}, Lcom/google/r/b/a/aas;->c()I

    .line 8014
    iget v0, p0, Lcom/google/r/b/a/aas;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 8015
    iget-object v0, p0, Lcom/google/r/b/a/aas;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 8017
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/aas;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 8018
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 7997
    iget-byte v0, p0, Lcom/google/r/b/a/aas;->d:B

    .line 7998
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 8008
    :goto_0
    return v0

    .line 7999
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 8001
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aas;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 8002
    iget-object v0, p0, Lcom/google/r/b/a/aas;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 8003
    iput-byte v2, p0, Lcom/google/r/b/a/aas;->d:B

    move v0, v2

    .line 8004
    goto :goto_0

    :cond_2
    move v0, v2

    .line 8001
    goto :goto_1

    .line 8007
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/aas;->d:B

    move v0, v1

    .line 8008
    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 8022
    iget v1, p0, Lcom/google/r/b/a/aas;->e:I

    .line 8023
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 8032
    :goto_0
    return v0

    .line 8026
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/aas;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 8027
    iget-object v1, p0, Lcom/google/r/b/a/aas;->b:Lcom/google/n/ao;

    .line 8028
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 8030
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/aas;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 8031
    iput v0, p0, Lcom/google/r/b/a/aas;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7912
    invoke-static {}, Lcom/google/r/b/a/aas;->newBuilder()Lcom/google/r/b/a/aau;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aau;->a(Lcom/google/r/b/a/aas;)Lcom/google/r/b/a/aau;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7912
    invoke-static {}, Lcom/google/r/b/a/aas;->newBuilder()Lcom/google/r/b/a/aau;

    move-result-object v0

    return-object v0
.end method
