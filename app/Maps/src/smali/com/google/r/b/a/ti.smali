.class public final Lcom/google/r/b/a/ti;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/tl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ti;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/r/b/a/ti;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:J

.field public j:Z

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/r/b/a/tj;

    invoke-direct {v0}, Lcom/google/r/b/a/tj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ti;->PARSER:Lcom/google/n/ax;

    .line 351
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ti;->n:Lcom/google/n/aw;

    .line 835
    new-instance v0, Lcom/google/r/b/a/ti;

    invoke-direct {v0}, Lcom/google/r/b/a/ti;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ti;->k:Lcom/google/r/b/a/ti;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x7d0

    const/16 v1, 0xf

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 259
    iput-byte v0, p0, Lcom/google/r/b/a/ti;->l:B

    .line 302
    iput v0, p0, Lcom/google/r/b/a/ti;->m:I

    .line 18
    iput v2, p0, Lcom/google/r/b/a/ti;->b:I

    .line 19
    iput v2, p0, Lcom/google/r/b/a/ti;->c:I

    .line 20
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/google/r/b/a/ti;->d:I

    .line 21
    iput v1, p0, Lcom/google/r/b/a/ti;->e:I

    .line 22
    iput v1, p0, Lcom/google/r/b/a/ti;->f:I

    .line 23
    const/16 v0, 0xe

    iput v0, p0, Lcom/google/r/b/a/ti;->g:I

    .line 24
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/ti;->h:I

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/ti;->i:J

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/ti;->j:Z

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 33
    invoke-direct {p0}, Lcom/google/r/b/a/ti;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 39
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 46
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 48
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ti;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ti;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ti;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ti;->d:I

    goto :goto_0

    .line 68
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ti;->e:I

    goto :goto_0

    .line 73
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ti;->f:I

    goto :goto_0

    .line 78
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 79
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ti;->g:I

    goto :goto_0

    .line 83
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 84
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ti;->h:I

    goto/16 :goto_0

    .line 88
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/ti;->i:J

    goto/16 :goto_0

    .line 93
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/ti;->a:I

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/ti;->j:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 105
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ti;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 259
    iput-byte v0, p0, Lcom/google/r/b/a/ti;->l:B

    .line 302
    iput v0, p0, Lcom/google/r/b/a/ti;->m:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ti;
    .locals 1

    .prologue
    .line 838
    sget-object v0, Lcom/google/r/b/a/ti;->k:Lcom/google/r/b/a/ti;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/tk;
    .locals 1

    .prologue
    .line 413
    new-instance v0, Lcom/google/r/b/a/tk;

    invoke-direct {v0}, Lcom/google/r/b/a/tk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ti;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/r/b/a/ti;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 271
    invoke-virtual {p0}, Lcom/google/r/b/a/ti;->c()I

    .line 272
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 273
    iget v0, p0, Lcom/google/r/b/a/ti;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 275
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 276
    iget v0, p0, Lcom/google/r/b/a/ti;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 278
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 279
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/ti;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 281
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 282
    iget v0, p0, Lcom/google/r/b/a/ti;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 284
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 285
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/ti;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 287
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 288
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/ti;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 290
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 291
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/ti;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 293
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 294
    iget-wide v0, p0, Lcom/google/r/b/a/ti;->i:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 296
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 297
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/ti;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 299
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/ti;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 300
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 261
    iget-byte v1, p0, Lcom/google/r/b/a/ti;->l:B

    .line 262
    if-ne v1, v0, :cond_0

    .line 266
    :goto_0
    return v0

    .line 263
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 265
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ti;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 304
    iget v0, p0, Lcom/google/r/b/a/ti;->m:I

    .line 305
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 346
    :goto_0
    return v0

    .line 308
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_10

    .line 309
    iget v0, p0, Lcom/google/r/b/a/ti;->b:I

    .line 310
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 312
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 313
    iget v3, p0, Lcom/google/r/b/a/ti;->c:I

    .line 314
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_b

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 316
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 317
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/ti;->d:I

    .line 318
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 320
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 321
    iget v3, p0, Lcom/google/r/b/a/ti;->e:I

    .line 322
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_d

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 324
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 325
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/r/b/a/ti;->f:I

    .line 326
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 328
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 329
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/r/b/a/ti;->g:I

    .line 330
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 332
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_7

    .line 333
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/r/b/a/ti;->h:I

    .line 334
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_6
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 336
    :cond_7
    iget v1, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_8

    .line 337
    const/16 v1, 0x8

    iget-wide v4, p0, Lcom/google/r/b/a/ti;->i:J

    .line 338
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 340
    :cond_8
    iget v1, p0, Lcom/google/r/b/a/ti;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_9

    .line 341
    const/16 v1, 0x9

    iget-boolean v3, p0, Lcom/google/r/b/a/ti;->j:Z

    .line 342
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 344
    :cond_9
    iget-object v1, p0, Lcom/google/r/b/a/ti;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 345
    iput v0, p0, Lcom/google/r/b/a/ti;->m:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 310
    goto/16 :goto_1

    :cond_b
    move v3, v1

    .line 314
    goto/16 :goto_3

    :cond_c
    move v3, v1

    .line 318
    goto/16 :goto_4

    :cond_d
    move v3, v1

    .line 322
    goto/16 :goto_5

    :cond_e
    move v3, v1

    .line 326
    goto/16 :goto_6

    :cond_f
    move v3, v1

    .line 330
    goto :goto_7

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ti;->newBuilder()Lcom/google/r/b/a/tk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/tk;->a(Lcom/google/r/b/a/ti;)Lcom/google/r/b/a/tk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ti;->newBuilder()Lcom/google/r/b/a/tk;

    move-result-object v0

    return-object v0
.end method
