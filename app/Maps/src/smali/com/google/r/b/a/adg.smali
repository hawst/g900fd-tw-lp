.class public final Lcom/google/r/b/a/adg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/adh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ade;",
        "Lcom/google/r/b/a/adg;",
        ">;",
        "Lcom/google/r/b/a/adh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 13362
    sget-object v0, Lcom/google/r/b/a/ade;->g:Lcom/google/r/b/a/ade;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 13449
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/adg;->b:Lcom/google/n/ao;

    .line 13508
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/adg;->c:Ljava/lang/Object;

    .line 13584
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/adg;->d:Lcom/google/n/ao;

    .line 13643
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/adg;->e:Lcom/google/n/ao;

    .line 13363
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 13354
    new-instance v2, Lcom/google/r/b/a/ade;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ade;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/adg;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/adg;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/adg;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/adg;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ade;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/ade;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/adg;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/adg;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/ade;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/adg;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/adg;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/r/b/a/adg;->f:I

    iput v1, v2, Lcom/google/r/b/a/ade;->f:I

    iput v0, v2, Lcom/google/r/b/a/ade;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 13354
    check-cast p1, Lcom/google/r/b/a/ade;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/adg;->a(Lcom/google/r/b/a/ade;)Lcom/google/r/b/a/adg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ade;)Lcom/google/r/b/a/adg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 13418
    invoke-static {}, Lcom/google/r/b/a/ade;->d()Lcom/google/r/b/a/ade;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 13440
    :goto_0
    return-object p0

    .line 13419
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 13420
    iget-object v2, p0, Lcom/google/r/b/a/adg;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ade;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13421
    iget v2, p0, Lcom/google/r/b/a/adg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/adg;->a:I

    .line 13423
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 13424
    iget v2, p0, Lcom/google/r/b/a/adg;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/adg;->a:I

    .line 13425
    iget-object v2, p1, Lcom/google/r/b/a/ade;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/adg;->c:Ljava/lang/Object;

    .line 13428
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 13429
    iget-object v2, p0, Lcom/google/r/b/a/adg;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ade;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13430
    iget v2, p0, Lcom/google/r/b/a/adg;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/adg;->a:I

    .line 13432
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 13433
    iget-object v2, p0, Lcom/google/r/b/a/adg;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ade;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13434
    iget v2, p0, Lcom/google/r/b/a/adg;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/adg;->a:I

    .line 13436
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/ade;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 13437
    iget v0, p1, Lcom/google/r/b/a/ade;->f:I

    iget v1, p0, Lcom/google/r/b/a/adg;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/r/b/a/adg;->a:I

    iput v0, p0, Lcom/google/r/b/a/adg;->f:I

    .line 13439
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/ade;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 13419
    goto :goto_1

    :cond_7
    move v2, v1

    .line 13423
    goto :goto_2

    :cond_8
    move v2, v1

    .line 13428
    goto :goto_3

    :cond_9
    move v2, v1

    .line 13432
    goto :goto_4

    :cond_a
    move v0, v1

    .line 13436
    goto :goto_5
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 13444
    const/4 v0, 0x1

    return v0
.end method
