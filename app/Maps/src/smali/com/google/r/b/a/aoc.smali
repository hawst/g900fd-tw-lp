.class public final Lcom/google/r/b/a/aoc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aof;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aoc;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/aoc;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/f;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3026
    new-instance v0, Lcom/google/r/b/a/aod;

    invoke-direct {v0}, Lcom/google/r/b/a/aod;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aoc;->PARSER:Lcom/google/n/ax;

    .line 3122
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aoc;->g:Lcom/google/n/aw;

    .line 3358
    new-instance v0, Lcom/google/r/b/a/aoc;

    invoke-direct {v0}, Lcom/google/r/b/a/aoc;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aoc;->d:Lcom/google/r/b/a/aoc;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2977
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3043
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aoc;->b:Lcom/google/n/ao;

    .line 3073
    iput-byte v2, p0, Lcom/google/r/b/a/aoc;->e:B

    .line 3101
    iput v2, p0, Lcom/google/r/b/a/aoc;->f:I

    .line 2978
    iget-object v0, p0, Lcom/google/r/b/a/aoc;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 2979
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/aoc;->c:Lcom/google/n/f;

    .line 2980
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2986
    invoke-direct {p0}, Lcom/google/r/b/a/aoc;-><init>()V

    .line 2987
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2992
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2993
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2994
    sparse-switch v3, :sswitch_data_0

    .line 2999
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 3001
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2997
    goto :goto_0

    .line 3006
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/aoc;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 3007
    iget v3, p0, Lcom/google/r/b/a/aoc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aoc;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3017
    :catch_0
    move-exception v0

    .line 3018
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3023
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aoc;->au:Lcom/google/n/bn;

    throw v0

    .line 3011
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/aoc;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/aoc;->a:I

    .line 3012
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/r/b/a/aoc;->c:Lcom/google/n/f;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3019
    :catch_1
    move-exception v0

    .line 3020
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3021
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3023
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aoc;->au:Lcom/google/n/bn;

    .line 3024
    return-void

    .line 2994
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2975
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3043
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aoc;->b:Lcom/google/n/ao;

    .line 3073
    iput-byte v1, p0, Lcom/google/r/b/a/aoc;->e:B

    .line 3101
    iput v1, p0, Lcom/google/r/b/a/aoc;->f:I

    .line 2976
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aoc;
    .locals 1

    .prologue
    .line 3361
    sget-object v0, Lcom/google/r/b/a/aoc;->d:Lcom/google/r/b/a/aoc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aoe;
    .locals 1

    .prologue
    .line 3184
    new-instance v0, Lcom/google/r/b/a/aoe;

    invoke-direct {v0}, Lcom/google/r/b/a/aoe;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aoc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3038
    sget-object v0, Lcom/google/r/b/a/aoc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3091
    invoke-virtual {p0}, Lcom/google/r/b/a/aoc;->c()I

    .line 3092
    iget v0, p0, Lcom/google/r/b/a/aoc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3093
    iget-object v0, p0, Lcom/google/r/b/a/aoc;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3095
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aoc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3096
    iget-object v0, p0, Lcom/google/r/b/a/aoc;->c:Lcom/google/n/f;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3098
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aoc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3099
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3075
    iget-byte v0, p0, Lcom/google/r/b/a/aoc;->e:B

    .line 3076
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 3086
    :goto_0
    return v0

    .line 3077
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 3079
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aoc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 3080
    iget-object v0, p0, Lcom/google/r/b/a/aoc;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ia;

    invoke-virtual {v0}, Lcom/google/r/b/a/ia;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3081
    iput-byte v2, p0, Lcom/google/r/b/a/aoc;->e:B

    move v0, v2

    .line 3082
    goto :goto_0

    :cond_2
    move v0, v2

    .line 3079
    goto :goto_1

    .line 3085
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/aoc;->e:B

    move v0, v1

    .line 3086
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3103
    iget v0, p0, Lcom/google/r/b/a/aoc;->f:I

    .line 3104
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3117
    :goto_0
    return v0

    .line 3107
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aoc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 3108
    iget-object v0, p0, Lcom/google/r/b/a/aoc;->b:Lcom/google/n/ao;

    .line 3109
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 3111
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/aoc;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 3112
    iget-object v2, p0, Lcom/google/r/b/a/aoc;->c:Lcom/google/n/f;

    .line 3113
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 3115
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/aoc;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3116
    iput v0, p0, Lcom/google/r/b/a/aoc;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2969
    invoke-static {}, Lcom/google/r/b/a/aoc;->newBuilder()Lcom/google/r/b/a/aoe;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aoe;->a(Lcom/google/r/b/a/aoc;)Lcom/google/r/b/a/aoe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2969
    invoke-static {}, Lcom/google/r/b/a/aoc;->newBuilder()Lcom/google/r/b/a/aoe;

    move-result-object v0

    return-object v0
.end method
