.class public final Lcom/google/r/b/a/dp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/dq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/dn;",
        "Lcom/google/r/b/a/dp;",
        ">;",
        "Lcom/google/r/b/a/dq;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/google/r/b/a/dn;->d:Lcom/google/r/b/a/dn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 345
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    .line 481
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/dp;->c:Lcom/google/n/ao;

    .line 279
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270
    new-instance v2, Lcom/google/r/b/a/dn;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/dn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/dp;->a:I

    iget v4, p0, Lcom/google/r/b/a/dp;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/dp;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/dp;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/dn;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v3, v2, Lcom/google/r/b/a/dn;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/dp;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/dp;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/dn;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 270
    check-cast p1, Lcom/google/r/b/a/dn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/dp;->a(Lcom/google/r/b/a/dn;)Lcom/google/r/b/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/dn;)Lcom/google/r/b/a/dp;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 313
    invoke-static {}, Lcom/google/r/b/a/dn;->d()Lcom/google/r/b/a/dn;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 329
    :goto_0
    return-object p0

    .line 314
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/dn;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 315
    iget-object v1, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 316
    iget-object v1, p1, Lcom/google/r/b/a/dn;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    .line 317
    iget v1, p0, Lcom/google/r/b/a/dp;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/dp;->a:I

    .line 324
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/r/b/a/dn;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/google/r/b/a/dp;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/dn;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 326
    iget v0, p0, Lcom/google/r/b/a/dp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/dp;->a:I

    .line 328
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/dn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 319
    :cond_3
    invoke-virtual {p0}, Lcom/google/r/b/a/dp;->c()V

    .line 320
    iget-object v1, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/dn;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 324
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 333
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/rd;->d()Lcom/google/r/b/a/rd;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/rd;

    invoke-virtual {v0}, Lcom/google/r/b/a/rd;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    :goto_1
    return v2

    .line 333
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 339
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 347
    iget v0, p0, Lcom/google/r/b/a/dp;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 348
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/dp;->b:Ljava/util/List;

    .line 351
    iget v0, p0, Lcom/google/r/b/a/dp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/dp;->a:I

    .line 353
    :cond_0
    return-void
.end method
