.class public final Lcom/google/r/b/a/ba;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/bb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ay;",
        "Lcom/google/r/b/a/ba;",
        ">;",
        "Lcom/google/r/b/a/bb;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:J

.field public c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 266
    sget-object v0, Lcom/google/r/b/a/ay;->d:Lcom/google/r/b/a/ay;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 267
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/google/r/b/a/ba;->c()Lcom/google/r/b/a/ay;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 258
    check-cast p1, Lcom/google/r/b/a/ay;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 298
    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 306
    :goto_0
    return-object p0

    .line 299
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ay;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 300
    iget-wide v2, p1, Lcom/google/r/b/a/ay;->b:J

    iget v4, p0, Lcom/google/r/b/a/ba;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ba;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/ba;->b:J

    .line 302
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ay;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 303
    iget v0, p1, Lcom/google/r/b/a/ay;->c:I

    iget v1, p0, Lcom/google/r/b/a/ba;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/ba;->a:I

    iput v0, p0, Lcom/google/r/b/a/ba;->c:I

    .line 305
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/ay;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 299
    goto :goto_1

    :cond_4
    move v0, v1

    .line 302
    goto :goto_2
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 310
    iget v2, p0, Lcom/google/r/b/a/ba;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 314
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 310
    goto :goto_0

    :cond_1
    move v0, v1

    .line 314
    goto :goto_1
.end method

.method public final c()Lcom/google/r/b/a/ay;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 282
    new-instance v2, Lcom/google/r/b/a/ay;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ay;-><init>(Lcom/google/n/v;)V

    .line 283
    iget v3, p0, Lcom/google/r/b/a/ba;->a:I

    .line 284
    const/4 v1, 0x0

    .line 285
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 288
    :goto_0
    iget-wide v4, p0, Lcom/google/r/b/a/ba;->b:J

    iput-wide v4, v2, Lcom/google/r/b/a/ay;->b:J

    .line 289
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 290
    or-int/lit8 v0, v0, 0x2

    .line 292
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/ba;->c:I

    iput v1, v2, Lcom/google/r/b/a/ay;->c:I

    .line 293
    iput v0, v2, Lcom/google/r/b/a/ay;->a:I

    .line 294
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
