.class public final Lcom/google/r/b/a/ahf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ahg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ahd;",
        "Lcom/google/r/b/a/ahf;",
        ">;",
        "Lcom/google/r/b/a/ahg;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/f;

.field public c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5588
    sget-object v0, Lcom/google/r/b/a/ahd;->d:Lcom/google/r/b/a/ahd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5637
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/ahf;->b:Lcom/google/n/f;

    .line 5589
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5580
    new-instance v2, Lcom/google/r/b/a/ahd;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ahd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ahf;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/ahf;->b:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/ahd;->b:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/ahf;->c:I

    iput v1, v2, Lcom/google/r/b/a/ahd;->c:I

    iput v0, v2, Lcom/google/r/b/a/ahd;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5580
    check-cast p1, Lcom/google/r/b/a/ahd;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ahf;->a(Lcom/google/r/b/a/ahd;)Lcom/google/r/b/a/ahf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ahd;)Lcom/google/r/b/a/ahf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 5620
    invoke-static {}, Lcom/google/r/b/a/ahd;->d()Lcom/google/r/b/a/ahd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 5628
    :goto_0
    return-object p0

    .line 5621
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ahd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 5622
    iget-object v2, p1, Lcom/google/r/b/a/ahd;->b:Lcom/google/n/f;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v2, v1

    .line 5621
    goto :goto_1

    .line 5622
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/ahf;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ahf;->a:I

    iput-object v2, p0, Lcom/google/r/b/a/ahf;->b:Lcom/google/n/f;

    .line 5624
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ahd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    :goto_2
    if-eqz v0, :cond_4

    .line 5625
    iget v0, p1, Lcom/google/r/b/a/ahd;->c:I

    iget v1, p0, Lcom/google/r/b/a/ahf;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/ahf;->a:I

    iput v0, p0, Lcom/google/r/b/a/ahf;->c:I

    .line 5627
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/ahd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v0, v1

    .line 5624
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 5632
    const/4 v0, 0x1

    return v0
.end method
