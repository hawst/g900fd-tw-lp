.class public final Lcom/google/r/b/a/fi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/fl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/fi;",
            ">;"
        }
    .end annotation
.end field

.field static final S:Lcom/google/r/b/a/fi;

.field private static volatile V:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public A:Z

.field B:Z

.field C:Z

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field H:Z

.field I:Z

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:Z

.field public O:Z

.field public P:Z

.field public Q:Z

.field public R:Z

.field private T:B

.field private U:I

.field a:I

.field b:I

.field public c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field j:Z

.field public k:Z

.field public l:Z

.field m:Z

.field n:Z

.field o:Z

.field p:Z

.field q:Z

.field r:Z

.field s:Z

.field t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field x:Z

.field y:Z

.field public z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 307
    new-instance v0, Lcom/google/r/b/a/fj;

    invoke-direct {v0}, Lcom/google/r/b/a/fj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/fi;->PARSER:Lcom/google/n/ax;

    .line 1277
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/fi;->V:Lcom/google/n/aw;

    .line 3118
    new-instance v0, Lcom/google/r/b/a/fi;

    invoke-direct {v0}, Lcom/google/r/b/a/fi;-><init>()V

    sput-object v0, Lcom/google/r/b/a/fi;->S:Lcom/google/r/b/a/fi;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 954
    iput-byte v2, p0, Lcom/google/r/b/a/fi;->T:B

    .line 1096
    iput v2, p0, Lcom/google/r/b/a/fi;->U:I

    .line 18
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->c:Z

    .line 19
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->d:Z

    .line 20
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->e:Z

    .line 21
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->f:Z

    .line 22
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->g:Z

    .line 23
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->h:Z

    .line 24
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->i:Z

    .line 25
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->j:Z

    .line 26
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->k:Z

    .line 27
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->l:Z

    .line 28
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->m:Z

    .line 29
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->n:Z

    .line 30
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->o:Z

    .line 31
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->p:Z

    .line 32
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->q:Z

    .line 33
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->r:Z

    .line 34
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->s:Z

    .line 35
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->t:Z

    .line 36
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->u:Z

    .line 37
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->v:Z

    .line 38
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->w:Z

    .line 39
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->x:Z

    .line 40
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->y:Z

    .line 41
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->z:Z

    .line 42
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->A:Z

    .line 43
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->B:Z

    .line 44
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->C:Z

    .line 45
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->D:Z

    .line 46
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->E:Z

    .line 47
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->F:Z

    .line 48
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->G:Z

    .line 49
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->H:Z

    .line 50
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->I:Z

    .line 51
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->J:Z

    .line 52
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->K:Z

    .line 53
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->L:Z

    .line 54
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->M:Z

    .line 55
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->N:Z

    .line 56
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->O:Z

    .line 57
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->P:Z

    .line 58
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->Q:Z

    .line 59
    iput-boolean v1, p0, Lcom/google/r/b/a/fi;->R:Z

    .line 60
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 66
    invoke-direct {p0}, Lcom/google/r/b/a/fi;-><init>()V

    .line 67
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 73
    :cond_0
    :goto_0
    if-nez v3, :cond_2b

    .line 74
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 75
    sparse-switch v0, :sswitch_data_0

    .line 80
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 82
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 78
    goto :goto_0

    .line 87
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->c:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 298
    :catch_0
    move-exception v0

    .line 299
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 304
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/fi;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 88
    goto :goto_1

    .line 92
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 93
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->d:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 300
    :catch_1
    move-exception v0

    .line 301
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 302
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v0, v2

    .line 93
    goto :goto_2

    .line 97
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 98
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->e:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 102
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 103
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->f:Z

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_4

    .line 107
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 108
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->g:Z

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_5

    .line 112
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 113
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->h:Z

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_6

    .line 117
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 118
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->i:Z

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_7

    .line 122
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 123
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->j:Z

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto :goto_8

    .line 127
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 128
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->k:Z

    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto :goto_9

    .line 132
    :sswitch_a
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 133
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->m:Z

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_a

    .line 137
    :sswitch_b
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 138
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->n:Z

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto :goto_b

    .line 142
    :sswitch_c
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 143
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->o:Z

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_c

    .line 147
    :sswitch_d
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 148
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->p:Z

    goto/16 :goto_0

    :cond_d
    move v0, v2

    goto :goto_d

    .line 152
    :sswitch_e
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 153
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->q:Z

    goto/16 :goto_0

    :cond_e
    move v0, v2

    goto :goto_e

    .line 157
    :sswitch_f
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const v5, 0x8000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 158
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_f

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->r:Z

    goto/16 :goto_0

    :cond_f
    move v0, v2

    goto :goto_f

    .line 162
    :sswitch_10
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x10000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 163
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_10

    move v0, v1

    :goto_10
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->s:Z

    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto :goto_10

    .line 167
    :sswitch_11
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x20000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 168
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_11

    move v0, v1

    :goto_11
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->t:Z

    goto/16 :goto_0

    :cond_11
    move v0, v2

    goto :goto_11

    .line 172
    :sswitch_12
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x40000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 173
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_12

    move v0, v1

    :goto_12
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->u:Z

    goto/16 :goto_0

    :cond_12
    move v0, v2

    goto :goto_12

    .line 177
    :sswitch_13
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x100000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 178
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_13

    move v0, v1

    :goto_13
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->w:Z

    goto/16 :goto_0

    :cond_13
    move v0, v2

    goto :goto_13

    .line 182
    :sswitch_14
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x200000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 183
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_14

    move v0, v1

    :goto_14
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->x:Z

    goto/16 :goto_0

    :cond_14
    move v0, v2

    goto :goto_14

    .line 187
    :sswitch_15
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x400000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 188
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_15

    move v0, v1

    :goto_15
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->y:Z

    goto/16 :goto_0

    :cond_15
    move v0, v2

    goto :goto_15

    .line 192
    :sswitch_16
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x800000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 193
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_16

    move v0, v1

    :goto_16
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->z:Z

    goto/16 :goto_0

    :cond_16
    move v0, v2

    goto :goto_16

    .line 197
    :sswitch_17
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x80000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 198
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_17

    move v0, v1

    :goto_17
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->v:Z

    goto/16 :goto_0

    :cond_17
    move v0, v2

    goto :goto_17

    .line 202
    :sswitch_18
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x1000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 203
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_18

    move v0, v1

    :goto_18
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->A:Z

    goto/16 :goto_0

    :cond_18
    move v0, v2

    goto :goto_18

    .line 207
    :sswitch_19
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 208
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_19

    move v0, v1

    :goto_19
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->l:Z

    goto/16 :goto_0

    :cond_19
    move v0, v2

    goto :goto_19

    .line 212
    :sswitch_1a
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x2000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 213
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1a

    move v0, v1

    :goto_1a
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->B:Z

    goto/16 :goto_0

    :cond_1a
    move v0, v2

    goto :goto_1a

    .line 217
    :sswitch_1b
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x4000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 218
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1b

    move v0, v1

    :goto_1b
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->C:Z

    goto/16 :goto_0

    :cond_1b
    move v0, v2

    goto :goto_1b

    .line 222
    :sswitch_1c
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x8000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 223
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1c

    move v0, v1

    :goto_1c
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->D:Z

    goto/16 :goto_0

    :cond_1c
    move v0, v2

    goto :goto_1c

    .line 227
    :sswitch_1d
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x10000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 228
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1d

    move v0, v1

    :goto_1d
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->E:Z

    goto/16 :goto_0

    :cond_1d
    move v0, v2

    goto :goto_1d

    .line 232
    :sswitch_1e
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x20000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 233
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1e

    move v0, v1

    :goto_1e
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->F:Z

    goto/16 :goto_0

    :cond_1e
    move v0, v2

    goto :goto_1e

    .line 237
    :sswitch_1f
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, 0x40000000    # 2.0f

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 238
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1f

    move v0, v1

    :goto_1f
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->G:Z

    goto/16 :goto_0

    :cond_1f
    move v0, v2

    goto :goto_1f

    .line 242
    :sswitch_20
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v5, -0x80000000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/fi;->a:I

    .line 243
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_20

    move v0, v1

    :goto_20
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->H:Z

    goto/16 :goto_0

    :cond_20
    move v0, v2

    goto :goto_20

    .line 247
    :sswitch_21
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 248
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_21

    move v0, v1

    :goto_21
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->I:Z

    goto/16 :goto_0

    :cond_21
    move v0, v2

    goto :goto_21

    .line 252
    :sswitch_22
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 253
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_22

    move v0, v1

    :goto_22
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->J:Z

    goto/16 :goto_0

    :cond_22
    move v0, v2

    goto :goto_22

    .line 257
    :sswitch_23
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 258
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_23

    move v0, v1

    :goto_23
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->L:Z

    goto/16 :goto_0

    :cond_23
    move v0, v2

    goto :goto_23

    .line 262
    :sswitch_24
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 263
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_24

    move v0, v1

    :goto_24
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->M:Z

    goto/16 :goto_0

    :cond_24
    move v0, v2

    goto :goto_24

    .line 267
    :sswitch_25
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 268
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_25

    move v0, v1

    :goto_25
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->N:Z

    goto/16 :goto_0

    :cond_25
    move v0, v2

    goto :goto_25

    .line 272
    :sswitch_26
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 273
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_26

    move v0, v1

    :goto_26
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->O:Z

    goto/16 :goto_0

    :cond_26
    move v0, v2

    goto :goto_26

    .line 277
    :sswitch_27
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 278
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_27

    move v0, v1

    :goto_27
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->P:Z

    goto/16 :goto_0

    :cond_27
    move v0, v2

    goto :goto_27

    .line 282
    :sswitch_28
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 283
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_28

    move v0, v1

    :goto_28
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->Q:Z

    goto/16 :goto_0

    :cond_28
    move v0, v2

    goto :goto_28

    .line 287
    :sswitch_29
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 288
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_29

    move v0, v1

    :goto_29
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->K:Z

    goto/16 :goto_0

    :cond_29
    move v0, v2

    goto :goto_29

    .line 292
    :sswitch_2a
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/fi;->b:I

    .line 293
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2a

    move v0, v1

    :goto_2a
    iput-boolean v0, p0, Lcom/google/r/b/a/fi;->R:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_2a
    move v0, v2

    goto :goto_2a

    .line 304
    :cond_2b
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fi;->au:Lcom/google/n/bn;

    .line 305
    return-void

    .line 75
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe0 -> :sswitch_1c
        0xe8 -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0x100 -> :sswitch_1f
        0x108 -> :sswitch_20
        0x110 -> :sswitch_21
        0x118 -> :sswitch_22
        0x120 -> :sswitch_23
        0x128 -> :sswitch_24
        0x130 -> :sswitch_25
        0x138 -> :sswitch_26
        0x140 -> :sswitch_27
        0x148 -> :sswitch_28
        0x150 -> :sswitch_29
        0x158 -> :sswitch_2a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 954
    iput-byte v0, p0, Lcom/google/r/b/a/fi;->T:B

    .line 1096
    iput v0, p0, Lcom/google/r/b/a/fi;->U:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/fi;
    .locals 1

    .prologue
    .line 3121
    sget-object v0, Lcom/google/r/b/a/fi;->S:Lcom/google/r/b/a/fi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/fk;
    .locals 1

    .prologue
    .line 1339
    new-instance v0, Lcom/google/r/b/a/fk;

    invoke-direct {v0}, Lcom/google/r/b/a/fk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/fi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    sget-object v0, Lcom/google/r/b/a/fi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 966
    invoke-virtual {p0}, Lcom/google/r/b/a/fi;->c()I

    .line 967
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 968
    iget-boolean v0, p0, Lcom/google/r/b/a/fi;->c:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 970
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 971
    iget-boolean v0, p0, Lcom/google/r/b/a/fi;->d:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 973
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 974
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 976
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 977
    iget-boolean v0, p0, Lcom/google/r/b/a/fi;->f:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 979
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_4

    .line 980
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 982
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 983
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 985
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 986
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 988
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 989
    iget-boolean v0, p0, Lcom/google/r/b/a/fi;->j:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(IZ)V

    .line 991
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 992
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 994
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_9

    .line 995
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 997
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_a

    .line 998
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->n:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1000
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_b

    .line 1001
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1003
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_c

    .line 1004
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1006
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_d

    .line 1007
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->q:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1009
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_e

    .line 1010
    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->r:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1012
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_f

    .line 1013
    iget-boolean v0, p0, Lcom/google/r/b/a/fi;->s:Z

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(IZ)V

    .line 1015
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_10

    .line 1016
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->t:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1018
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_11

    .line 1019
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->u:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1021
    :cond_11
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_12

    .line 1022
    const/16 v0, 0x13

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->w:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1024
    :cond_12
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_13

    .line 1025
    const/16 v0, 0x14

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->x:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1027
    :cond_13
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_14

    .line 1028
    const/16 v0, 0x15

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->y:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1030
    :cond_14
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    const/high16 v1, 0x800000

    if-ne v0, v1, :cond_15

    .line 1031
    const/16 v0, 0x16

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->z:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1033
    :cond_15
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_16

    .line 1034
    const/16 v0, 0x17

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->v:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1036
    :cond_16
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_17

    .line 1037
    const/16 v0, 0x18

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->A:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1039
    :cond_17
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_18

    .line 1040
    const/16 v0, 0x19

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1042
    :cond_18
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000000

    if-ne v0, v1, :cond_19

    .line 1043
    const/16 v0, 0x1a

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->B:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1045
    :cond_19
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    const/high16 v1, 0x4000000

    if-ne v0, v1, :cond_1a

    .line 1046
    const/16 v0, 0x1b

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->C:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1048
    :cond_1a
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    const/high16 v1, 0x8000000

    if-ne v0, v1, :cond_1b

    .line 1049
    const/16 v0, 0x1c

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->D:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1051
    :cond_1b
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_1c

    .line 1052
    const/16 v0, 0x1d

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->E:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1054
    :cond_1c
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000000

    if-ne v0, v1, :cond_1d

    .line 1055
    const/16 v0, 0x1e

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->F:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1057
    :cond_1d
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_1e

    .line 1058
    const/16 v0, 0x20

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->G:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1060
    :cond_1e
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v1, -0x80000000

    and-int/2addr v0, v1

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_1f

    .line 1061
    const/16 v0, 0x21

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->H:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1063
    :cond_1f
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_20

    .line 1064
    const/16 v0, 0x22

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->I:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1066
    :cond_20
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_21

    .line 1067
    const/16 v0, 0x23

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->J:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1069
    :cond_21
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_22

    .line 1070
    const/16 v0, 0x24

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->L:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1072
    :cond_22
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_23

    .line 1073
    const/16 v0, 0x25

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->M:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1075
    :cond_23
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_24

    .line 1076
    const/16 v0, 0x26

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->N:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1078
    :cond_24
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_25

    .line 1079
    const/16 v0, 0x27

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->O:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1081
    :cond_25
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_26

    .line 1082
    const/16 v0, 0x28

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->P:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1084
    :cond_26
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_27

    .line 1085
    const/16 v0, 0x29

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->Q:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1087
    :cond_27
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_28

    .line 1088
    const/16 v0, 0x2a

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->K:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1090
    :cond_28
    iget v0, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_29

    .line 1091
    const/16 v0, 0x2b

    iget-boolean v1, p0, Lcom/google/r/b/a/fi;->R:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1093
    :cond_29
    iget-object v0, p0, Lcom/google/r/b/a/fi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1094
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 956
    iget-byte v1, p0, Lcom/google/r/b/a/fi;->T:B

    .line 957
    if-ne v1, v0, :cond_0

    .line 961
    :goto_0
    return v0

    .line 958
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 960
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/fi;->T:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1098
    iget v0, p0, Lcom/google/r/b/a/fi;->U:I

    .line 1099
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1272
    :goto_0
    return v0

    .line 1102
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_2a

    .line 1103
    iget-boolean v0, p0, Lcom/google/r/b/a/fi;->c:Z

    .line 1104
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1106
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 1107
    iget-boolean v2, p0, Lcom/google/r/b/a/fi;->d:Z

    .line 1108
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1110
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 1111
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->e:Z

    .line 1112
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1114
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v7, :cond_3

    .line 1115
    iget-boolean v2, p0, Lcom/google/r/b/a/fi;->f:Z

    .line 1116
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1118
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 1119
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->g:Z

    .line 1120
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1122
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 1123
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->h:Z

    .line 1124
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1126
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 1127
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->i:Z

    .line 1128
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1130
    :cond_6
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_7

    .line 1131
    iget-boolean v2, p0, Lcom/google/r/b/a/fi;->j:Z

    .line 1132
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1134
    :cond_7
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_8

    .line 1135
    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->k:Z

    .line 1136
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1138
    :cond_8
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_9

    .line 1139
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->m:Z

    .line 1140
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1142
    :cond_9
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_a

    .line 1143
    const/16 v2, 0xb

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->n:Z

    .line 1144
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1146
    :cond_a
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_b

    .line 1147
    const/16 v2, 0xc

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->o:Z

    .line 1148
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1150
    :cond_b
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_c

    .line 1151
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->p:Z

    .line 1152
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1154
    :cond_c
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_d

    .line 1155
    const/16 v2, 0xe

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->q:Z

    .line 1156
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1158
    :cond_d
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const v3, 0x8000

    and-int/2addr v2, v3

    const v3, 0x8000

    if-ne v2, v3, :cond_e

    .line 1159
    const/16 v2, 0xf

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->r:Z

    .line 1160
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1162
    :cond_e
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000

    if-ne v2, v3, :cond_f

    .line 1163
    const/16 v2, 0x10

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->s:Z

    .line 1164
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1166
    :cond_f
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_10

    .line 1167
    const/16 v2, 0x11

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->t:Z

    .line 1168
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1170
    :cond_10
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_11

    .line 1171
    const/16 v2, 0x12

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->u:Z

    .line 1172
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1174
    :cond_11
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_12

    .line 1175
    const/16 v2, 0x13

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->w:Z

    .line 1176
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1178
    :cond_12
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_13

    .line 1179
    const/16 v2, 0x14

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->x:Z

    .line 1180
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1182
    :cond_13
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    const/high16 v3, 0x400000

    if-ne v2, v3, :cond_14

    .line 1183
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->y:Z

    .line 1184
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1186
    :cond_14
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    const/high16 v3, 0x800000

    if-ne v2, v3, :cond_15

    .line 1187
    const/16 v2, 0x16

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->z:Z

    .line 1188
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1190
    :cond_15
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_16

    .line 1191
    const/16 v2, 0x17

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->v:Z

    .line 1192
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1194
    :cond_16
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    const/high16 v3, 0x1000000

    if-ne v2, v3, :cond_17

    .line 1195
    const/16 v2, 0x18

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->A:Z

    .line 1196
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1198
    :cond_17
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_18

    .line 1199
    const/16 v2, 0x19

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->l:Z

    .line 1200
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1202
    :cond_18
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    const/high16 v3, 0x2000000

    if-ne v2, v3, :cond_19

    .line 1203
    const/16 v2, 0x1a

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->B:Z

    .line 1204
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1206
    :cond_19
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x4000000

    and-int/2addr v2, v3

    const/high16 v3, 0x4000000

    if-ne v2, v3, :cond_1a

    .line 1207
    const/16 v2, 0x1b

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->C:Z

    .line 1208
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1210
    :cond_1a
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x8000000

    and-int/2addr v2, v3

    const/high16 v3, 0x8000000

    if-ne v2, v3, :cond_1b

    .line 1211
    const/16 v2, 0x1c

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->D:Z

    .line 1212
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1214
    :cond_1b
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x10000000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000000

    if-ne v2, v3, :cond_1c

    .line 1215
    const/16 v2, 0x1d

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->E:Z

    .line 1216
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1218
    :cond_1c
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x20000000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000000

    if-ne v2, v3, :cond_1d

    .line 1219
    const/16 v2, 0x1e

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->F:Z

    .line 1220
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1222
    :cond_1d
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, 0x40000000    # 2.0f

    and-int/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v2, v3, :cond_1e

    .line 1223
    const/16 v2, 0x20

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->G:Z

    .line 1224
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1226
    :cond_1e
    iget v2, p0, Lcom/google/r/b/a/fi;->a:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1f

    .line 1227
    const/16 v2, 0x21

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->H:Z

    .line 1228
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1230
    :cond_1f
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_20

    .line 1231
    const/16 v2, 0x22

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->I:Z

    .line 1232
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1234
    :cond_20
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_21

    .line 1235
    const/16 v2, 0x23

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->J:Z

    .line 1236
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1238
    :cond_21
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v7, :cond_22

    .line 1239
    const/16 v2, 0x24

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->L:Z

    .line 1240
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1242
    :cond_22
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_23

    .line 1243
    const/16 v2, 0x25

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->M:Z

    .line 1244
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1246
    :cond_23
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_24

    .line 1247
    const/16 v2, 0x26

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->N:Z

    .line 1248
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1250
    :cond_24
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_25

    .line 1251
    const/16 v2, 0x27

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->O:Z

    .line 1252
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1254
    :cond_25
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_26

    .line 1255
    const/16 v2, 0x28

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->P:Z

    .line 1256
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1258
    :cond_26
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_27

    .line 1259
    const/16 v2, 0x29

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->Q:Z

    .line 1260
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1262
    :cond_27
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_28

    .line 1263
    const/16 v2, 0x2a

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->K:Z

    .line 1264
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1266
    :cond_28
    iget v2, p0, Lcom/google/r/b/a/fi;->b:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_29

    .line 1267
    const/16 v2, 0x2b

    iget-boolean v3, p0, Lcom/google/r/b/a/fi;->R:Z

    .line 1268
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1270
    :cond_29
    iget-object v1, p0, Lcom/google/r/b/a/fi;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1271
    iput v0, p0, Lcom/google/r/b/a/fi;->U:I

    goto/16 :goto_0

    :cond_2a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/fi;->newBuilder()Lcom/google/r/b/a/fk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/fk;->a(Lcom/google/r/b/a/fi;)Lcom/google/r/b/a/fk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/fi;->newBuilder()Lcom/google/r/b/a/fk;

    move-result-object v0

    return-object v0
.end method
