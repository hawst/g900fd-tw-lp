.class public final Lcom/google/r/b/a/tk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/tl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ti;",
        "Lcom/google/r/b/a/tk;",
        ">;",
        "Lcom/google/r/b/a/tl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:J

.field private j:Z


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x7d0

    const/16 v1, 0xf

    .line 431
    sget-object v0, Lcom/google/r/b/a/ti;->k:Lcom/google/r/b/a/ti;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 543
    iput v2, p0, Lcom/google/r/b/a/tk;->b:I

    .line 575
    iput v2, p0, Lcom/google/r/b/a/tk;->c:I

    .line 607
    const/16 v0, 0x2d0

    iput v0, p0, Lcom/google/r/b/a/tk;->d:I

    .line 639
    iput v1, p0, Lcom/google/r/b/a/tk;->e:I

    .line 671
    iput v1, p0, Lcom/google/r/b/a/tk;->f:I

    .line 703
    const/16 v0, 0xe

    iput v0, p0, Lcom/google/r/b/a/tk;->g:I

    .line 735
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/tk;->h:I

    .line 799
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/tk;->j:Z

    .line 432
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 423
    new-instance v2, Lcom/google/r/b/a/ti;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ti;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/tk;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/tk;->b:I

    iput v1, v2, Lcom/google/r/b/a/ti;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/tk;->c:I

    iput v1, v2, Lcom/google/r/b/a/ti;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/tk;->d:I

    iput v1, v2, Lcom/google/r/b/a/ti;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/tk;->e:I

    iput v1, v2, Lcom/google/r/b/a/ti;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/r/b/a/tk;->f:I

    iput v1, v2, Lcom/google/r/b/a/ti;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/r/b/a/tk;->g:I

    iput v1, v2, Lcom/google/r/b/a/ti;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/r/b/a/tk;->h:I

    iput v1, v2, Lcom/google/r/b/a/ti;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-wide v4, p0, Lcom/google/r/b/a/tk;->i:J

    iput-wide v4, v2, Lcom/google/r/b/a/ti;->i:J

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v1, p0, Lcom/google/r/b/a/tk;->j:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/ti;->j:Z

    iput v0, v2, Lcom/google/r/b/a/ti;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 423
    check-cast p1, Lcom/google/r/b/a/ti;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/tk;->a(Lcom/google/r/b/a/ti;)Lcom/google/r/b/a/tk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ti;)Lcom/google/r/b/a/tk;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 505
    invoke-static {}, Lcom/google/r/b/a/ti;->d()Lcom/google/r/b/a/ti;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 534
    :goto_0
    return-object p0

    .line 506
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 507
    iget v2, p1, Lcom/google/r/b/a/ti;->b:I

    iget v3, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/tk;->a:I

    iput v2, p0, Lcom/google/r/b/a/tk;->b:I

    .line 509
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 510
    iget v2, p1, Lcom/google/r/b/a/ti;->c:I

    iget v3, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/tk;->a:I

    iput v2, p0, Lcom/google/r/b/a/tk;->c:I

    .line 512
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 513
    iget v2, p1, Lcom/google/r/b/a/ti;->d:I

    iget v3, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/tk;->a:I

    iput v2, p0, Lcom/google/r/b/a/tk;->d:I

    .line 515
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 516
    iget v2, p1, Lcom/google/r/b/a/ti;->e:I

    iget v3, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/tk;->a:I

    iput v2, p0, Lcom/google/r/b/a/tk;->e:I

    .line 518
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 519
    iget v2, p1, Lcom/google/r/b/a/ti;->f:I

    iget v3, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/tk;->a:I

    iput v2, p0, Lcom/google/r/b/a/tk;->f:I

    .line 521
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 522
    iget v2, p1, Lcom/google/r/b/a/ti;->g:I

    iget v3, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/tk;->a:I

    iput v2, p0, Lcom/google/r/b/a/tk;->g:I

    .line 524
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 525
    iget v2, p1, Lcom/google/r/b/a/ti;->h:I

    iget v3, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/tk;->a:I

    iput v2, p0, Lcom/google/r/b/a/tk;->h:I

    .line 527
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 528
    iget-wide v2, p1, Lcom/google/r/b/a/ti;->i:J

    iget v4, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/r/b/a/tk;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/tk;->i:J

    .line 530
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/ti;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_12

    :goto_9
    if-eqz v0, :cond_9

    .line 531
    iget-boolean v0, p1, Lcom/google/r/b/a/ti;->j:Z

    iget v1, p0, Lcom/google/r/b/a/tk;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/r/b/a/tk;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/tk;->j:Z

    .line 533
    :cond_9
    iget-object v0, p1, Lcom/google/r/b/a/ti;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 506
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 509
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 512
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 515
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 518
    goto :goto_5

    :cond_f
    move v2, v1

    .line 521
    goto :goto_6

    :cond_10
    move v2, v1

    .line 524
    goto :goto_7

    :cond_11
    move v2, v1

    .line 527
    goto :goto_8

    :cond_12
    move v0, v1

    .line 530
    goto :goto_9
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x1

    return v0
.end method
