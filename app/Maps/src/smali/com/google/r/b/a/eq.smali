.class public final Lcom/google/r/b/a/eq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/er;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/eo;",
        "Lcom/google/r/b/a/eq;",
        ">;",
        "Lcom/google/r/b/a/er;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 305
    sget-object v0, Lcom/google/r/b/a/eo;->f:Lcom/google/r/b/a/eo;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 390
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/eq;->b:Lcom/google/n/ao;

    .line 449
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/eq;->c:Lcom/google/n/ao;

    .line 306
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 297
    new-instance v2, Lcom/google/r/b/a/eo;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/eo;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/eq;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/eo;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/eq;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/eq;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/eo;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/eq;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/eq;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/eq;->d:I

    iput v1, v2, Lcom/google/r/b/a/eo;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/eq;->e:I

    iput v1, v2, Lcom/google/r/b/a/eo;->e:I

    iput v0, v2, Lcom/google/r/b/a/eo;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 297
    check-cast p1, Lcom/google/r/b/a/eo;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/eq;->a(Lcom/google/r/b/a/eo;)Lcom/google/r/b/a/eq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/eo;)Lcom/google/r/b/a/eq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 353
    invoke-static {}, Lcom/google/r/b/a/eo;->d()Lcom/google/r/b/a/eo;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 369
    :goto_0
    return-object p0

    .line 354
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/eo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 355
    iget-object v2, p0, Lcom/google/r/b/a/eq;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/eo;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 356
    iget v2, p0, Lcom/google/r/b/a/eq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/eq;->a:I

    .line 358
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/eo;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 359
    iget-object v2, p0, Lcom/google/r/b/a/eq;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/eo;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 360
    iget v2, p0, Lcom/google/r/b/a/eq;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/eq;->a:I

    .line 362
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/eo;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 363
    iget v2, p1, Lcom/google/r/b/a/eo;->d:I

    iget v3, p0, Lcom/google/r/b/a/eq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/eq;->a:I

    iput v2, p0, Lcom/google/r/b/a/eq;->d:I

    .line 365
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/eo;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 366
    iget v0, p1, Lcom/google/r/b/a/eo;->e:I

    iget v1, p0, Lcom/google/r/b/a/eq;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/eq;->a:I

    iput v0, p0, Lcom/google/r/b/a/eq;->e:I

    .line 368
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/eo;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 354
    goto :goto_1

    :cond_6
    move v2, v1

    .line 358
    goto :goto_2

    :cond_7
    move v2, v1

    .line 362
    goto :goto_3

    :cond_8
    move v0, v1

    .line 365
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 373
    iget v0, p0, Lcom/google/r/b/a/eq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/google/r/b/a/eq;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    invoke-virtual {v0}, Lcom/google/r/b/a/agl;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 385
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 373
    goto :goto_0

    .line 379
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/eq;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 380
    iget-object v0, p0, Lcom/google/r/b/a/eq;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 382
    goto :goto_1

    :cond_2
    move v0, v1

    .line 379
    goto :goto_2

    :cond_3
    move v0, v2

    .line 385
    goto :goto_1
.end method
