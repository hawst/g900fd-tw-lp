.class public final Lcom/google/r/b/a/yr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ys;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/yp;",
        "Lcom/google/r/b/a/yr;",
        ">;",
        "Lcom/google/r/b/a/ys;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/f;

.field private c:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4985
    sget-object v0, Lcom/google/r/b/a/yp;->d:Lcom/google/r/b/a/yp;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5034
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/yr;->b:Lcom/google/n/f;

    .line 5069
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/yr;->c:Lcom/google/n/f;

    .line 4986
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 4977
    invoke-virtual {p0}, Lcom/google/r/b/a/yr;->c()Lcom/google/r/b/a/yp;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4977
    check-cast p1, Lcom/google/r/b/a/yp;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/yr;->a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 5017
    invoke-static {}, Lcom/google/r/b/a/yp;->d()Lcom/google/r/b/a/yp;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 5025
    :goto_0
    return-object p0

    .line 5018
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/yp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 5019
    iget-object v2, p1, Lcom/google/r/b/a/yp;->b:Lcom/google/n/f;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v2, v1

    .line 5018
    goto :goto_1

    .line 5019
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/yr;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/yr;->a:I

    iput-object v2, p0, Lcom/google/r/b/a/yr;->b:Lcom/google/n/f;

    .line 5021
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/yp;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_6

    .line 5022
    iget-object v0, p1, Lcom/google/r/b/a/yp;->c:Lcom/google/n/f;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v0, v1

    .line 5021
    goto :goto_2

    .line 5022
    :cond_5
    iget v1, p0, Lcom/google/r/b/a/yr;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/yr;->a:I

    iput-object v0, p0, Lcom/google/r/b/a/yr;->c:Lcom/google/n/f;

    .line 5024
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/yp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 5029
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/r/b/a/yp;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5001
    new-instance v2, Lcom/google/r/b/a/yp;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/yp;-><init>(Lcom/google/n/v;)V

    .line 5002
    iget v3, p0, Lcom/google/r/b/a/yr;->a:I

    .line 5003
    const/4 v1, 0x0

    .line 5004
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 5007
    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/yr;->b:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/yp;->b:Lcom/google/n/f;

    .line 5008
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 5009
    or-int/lit8 v0, v0, 0x2

    .line 5011
    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/yr;->c:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/yp;->c:Lcom/google/n/f;

    .line 5012
    iput v0, v2, Lcom/google/r/b/a/yp;->a:I

    .line 5013
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
