.class public final Lcom/google/r/b/a/aex;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aex;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/aex;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field private g:I

.field private h:I

.field private i:I

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9383
    new-instance v0, Lcom/google/r/b/a/aey;

    invoke-direct {v0}, Lcom/google/r/b/a/aey;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aex;->PARSER:Lcom/google/n/ax;

    .line 9582
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aex;->l:Lcom/google/n/aw;

    .line 9987
    new-instance v0, Lcom/google/r/b/a/aex;

    invoke-direct {v0}, Lcom/google/r/b/a/aex;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aex;->f:Lcom/google/r/b/a/aex;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 9265
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 9420
    iput v0, p0, Lcom/google/r/b/a/aex;->g:I

    .line 9443
    iput v0, p0, Lcom/google/r/b/a/aex;->h:I

    .line 9466
    iput v0, p0, Lcom/google/r/b/a/aex;->i:I

    .line 9483
    iput-byte v0, p0, Lcom/google/r/b/a/aex;->j:B

    .line 9523
    iput v0, p0, Lcom/google/r/b/a/aex;->k:I

    .line 9266
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    .line 9267
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    .line 9268
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    .line 9269
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aex;->e:I

    .line 9270
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const v8, 0x7fffffff

    const/4 v4, 0x1

    const/4 v2, -0x1

    .line 9276
    invoke-direct {p0}, Lcom/google/r/b/a/aex;-><init>()V

    .line 9277
    const/4 v1, 0x0

    .line 9279
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 9281
    const/4 v0, 0x0

    move v3, v0

    .line 9282
    :cond_0
    :goto_0
    if-nez v3, :cond_13

    .line 9283
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 9284
    sparse-switch v0, :sswitch_data_0

    .line 9289
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 9291
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 9287
    goto :goto_0

    .line 9296
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v4, :cond_1

    .line 9297
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    .line 9298
    or-int/lit8 v1, v1, 0x1

    .line 9300
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->j()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 9365
    :catch_0
    move-exception v0

    .line 9366
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 9371
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_2

    .line 9372
    iget-object v2, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    .line 9374
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v9, :cond_3

    .line 9375
    iget-object v2, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    .line 9377
    :cond_3
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v10, :cond_4

    .line 9378
    iget-object v1, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    .line 9380
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aex;->au:Lcom/google/n/bn;

    throw v0

    .line 9304
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 9305
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 9306
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v4, :cond_5

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v8, :cond_6

    move v0, v2

    :goto_1
    if-lez v0, :cond_5

    .line 9307
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    .line 9308
    or-int/lit8 v1, v1, 0x1

    .line 9310
    :cond_5
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v8, :cond_7

    move v0, v2

    :goto_3
    if-lez v0, :cond_8

    .line 9311
    iget-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->j()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 9367
    :catch_1
    move-exception v0

    .line 9368
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 9369
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 9306
    :cond_6
    :try_start_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 9310
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 9313
    :cond_8
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 9317
    :sswitch_3
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v9, :cond_9

    .line 9318
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    .line 9319
    or-int/lit8 v1, v1, 0x2

    .line 9321
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->j()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 9325
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 9326
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 9327
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v9, :cond_a

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v8, :cond_b

    move v0, v2

    :goto_4
    if-lez v0, :cond_a

    .line 9328
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    .line 9329
    or-int/lit8 v1, v1, 0x2

    .line 9331
    :cond_a
    :goto_5
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v8, :cond_c

    move v0, v2

    :goto_6
    if-lez v0, :cond_d

    .line 9332
    iget-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->j()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 9327
    :cond_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 9331
    :cond_c
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_6

    .line 9334
    :cond_d
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 9338
    :sswitch_5
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v10, :cond_e

    .line 9339
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    .line 9340
    or-int/lit8 v1, v1, 0x4

    .line 9342
    :cond_e
    iget-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->j()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 9346
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 9347
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 9348
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v10, :cond_f

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v8, :cond_10

    move v0, v2

    :goto_7
    if-lez v0, :cond_f

    .line 9349
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    .line 9350
    or-int/lit8 v1, v1, 0x4

    .line 9352
    :cond_f
    :goto_8
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v8, :cond_11

    move v0, v2

    :goto_9
    if-lez v0, :cond_12

    .line 9353
    iget-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->j()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 9348
    :cond_10
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_7

    .line 9352
    :cond_11
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_9

    .line 9355
    :cond_12
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 9359
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/aex;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/aex;->a:I

    .line 9360
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aex;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 9371
    :cond_13
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v4, :cond_14

    .line 9372
    iget-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    .line 9374
    :cond_14
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v9, :cond_15

    .line 9375
    iget-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    .line 9377
    :cond_15
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v10, :cond_16

    .line 9378
    iget-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    .line 9380
    :cond_16
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aex;->au:Lcom/google/n/bn;

    .line 9381
    return-void

    .line 9284
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x12 -> :sswitch_4
        0x18 -> :sswitch_5
        0x1a -> :sswitch_6
        0x20 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 9263
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 9420
    iput v0, p0, Lcom/google/r/b/a/aex;->g:I

    .line 9443
    iput v0, p0, Lcom/google/r/b/a/aex;->h:I

    .line 9466
    iput v0, p0, Lcom/google/r/b/a/aex;->i:I

    .line 9483
    iput-byte v0, p0, Lcom/google/r/b/a/aex;->j:B

    .line 9523
    iput v0, p0, Lcom/google/r/b/a/aex;->k:I

    .line 9264
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aex;
    .locals 1

    .prologue
    .line 9990
    sget-object v0, Lcom/google/r/b/a/aex;->f:Lcom/google/r/b/a/aex;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aez;
    .locals 1

    .prologue
    .line 9644
    new-instance v0, Lcom/google/r/b/a/aez;

    invoke-direct {v0}, Lcom/google/r/b/a/aez;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aex;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9395
    sget-object v0, Lcom/google/r/b/a/aex;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 9495
    invoke-virtual {p0}, Lcom/google/r/b/a/aex;->c()I

    .line 9496
    iget-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 9497
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 9498
    iget v0, p0, Lcom/google/r/b/a/aex;->g:I

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_0
    move v1, v2

    .line 9500
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 9501
    iget-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v3, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 9500
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 9503
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 9504
    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 9505
    iget v0, p0, Lcom/google/r/b/a/aex;->h:I

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_2
    move v1, v2

    .line 9507
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 9508
    iget-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v3, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 9507
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 9510
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 9511
    const/16 v0, 0x1a

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 9512
    iget v0, p0, Lcom/google/r/b/a/aex;->i:I

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 9514
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 9515
    iget-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v1, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 9514
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 9517
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aex;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 9518
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/r/b/a/aex;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 9520
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/aex;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 9521
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 9485
    iget-byte v1, p0, Lcom/google/r/b/a/aex;->j:B

    .line 9486
    if-ne v1, v0, :cond_0

    .line 9490
    :goto_0
    return v0

    .line 9487
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 9489
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aex;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 9525
    iget v0, p0, Lcom/google/r/b/a/aex;->k:I

    .line 9526
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 9577
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 9531
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 9532
    iget-object v0, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    .line 9533
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v5, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v3, v0

    .line 9531
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 9535
    :cond_1
    add-int/lit8 v0, v3, 0x0

    .line 9536
    iget-object v1, p0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 9537
    add-int/lit8 v1, v0, 0x1

    .line 9539
    if-ltz v3, :cond_2

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    move v1, v0

    .line 9541
    :goto_3
    iput v3, p0, Lcom/google/r/b/a/aex;->g:I

    move v3, v2

    move v5, v2

    .line 9545
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 9546
    iget-object v0, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    .line 9547
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v6, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v5, v0

    .line 9545
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_2
    move v0, v4

    .line 9539
    goto :goto_2

    .line 9549
    :cond_3
    add-int v0, v1, v5

    .line 9550
    iget-object v1, p0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 9551
    add-int/lit8 v1, v0, 0x1

    .line 9553
    if-ltz v5, :cond_4

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v1

    move v1, v0

    .line 9555
    :goto_6
    iput v5, p0, Lcom/google/r/b/a/aex;->h:I

    move v3, v2

    move v5, v2

    .line 9559
    :goto_7
    iget-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 9560
    iget-object v0, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    .line 9561
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int/lit8 v6, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v5, v0

    .line 9559
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    :cond_4
    move v0, v4

    .line 9553
    goto :goto_5

    .line 9563
    :cond_5
    add-int v0, v1, v5

    .line 9564
    iget-object v1, p0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 9565
    add-int/lit8 v1, v0, 0x1

    .line 9567
    if-ltz v5, :cond_9

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v1

    .line 9569
    :cond_6
    iput v5, p0, Lcom/google/r/b/a/aex;->i:I

    .line 9571
    iget v1, p0, Lcom/google/r/b/a/aex;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_8

    .line 9572
    const/4 v1, 0x4

    iget v3, p0, Lcom/google/r/b/a/aex;->e:I

    .line 9573
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_7
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 9575
    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/aex;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 9576
    iput v0, p0, Lcom/google/r/b/a/aex;->k:I

    goto/16 :goto_0

    :cond_9
    move v0, v4

    .line 9567
    goto :goto_8

    :cond_a
    move v1, v0

    goto :goto_6

    :cond_b
    move v1, v0

    goto/16 :goto_3
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9257
    invoke-static {}, Lcom/google/r/b/a/aex;->newBuilder()Lcom/google/r/b/a/aez;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aez;->a(Lcom/google/r/b/a/aex;)Lcom/google/r/b/a/aez;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9257
    invoke-static {}, Lcom/google/r/b/a/aex;->newBuilder()Lcom/google/r/b/a/aez;

    move-result-object v0

    return-object v0
.end method
