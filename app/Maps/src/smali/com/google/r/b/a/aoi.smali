.class public final Lcom/google/r/b/a/aoi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aoj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aog;",
        "Lcom/google/r/b/a/aoi;",
        ">;",
        "Lcom/google/r/b/a/aoj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lcom/google/r/b/a/aog;->d:Lcom/google/r/b/a/aog;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 351
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    .line 295
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 286
    new-instance v2, Lcom/google/r/b/a/aog;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aog;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aoi;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/r/b/a/aoi;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aoi;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/aoi;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/aoi;->c:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aog;->c:Z

    iput v0, v2, Lcom/google/r/b/a/aog;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 286
    check-cast p1, Lcom/google/r/b/a/aog;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aoi;->a(Lcom/google/r/b/a/aog;)Lcom/google/r/b/a/aoi;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aog;)Lcom/google/r/b/a/aoi;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 327
    invoke-static {}, Lcom/google/r/b/a/aog;->d()Lcom/google/r/b/a/aog;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 342
    :goto_0
    return-object p0

    .line 328
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 329
    iget-object v1, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 330
    iget-object v1, p1, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    .line 331
    iget v1, p0, Lcom/google/r/b/a/aoi;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/aoi;->a:I

    .line 338
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/r/b/a/aog;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 339
    iget-boolean v0, p1, Lcom/google/r/b/a/aog;->c:Z

    iget v1, p0, Lcom/google/r/b/a/aoi;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/aoi;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/aoi;->c:Z

    .line 341
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/aog;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 333
    :cond_3
    invoke-virtual {p0}, Lcom/google/r/b/a/aoi;->c()V

    .line 334
    iget-object v1, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 338
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 353
    iget v0, p0, Lcom/google/r/b/a/aoi;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 354
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    .line 355
    iget v0, p0, Lcom/google/r/b/a/aoi;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/aoi;->a:I

    .line 357
    :cond_0
    return-void
.end method
