.class public final enum Lcom/google/r/b/a/iy;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/iy;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/iy;

.field public static final enum b:Lcom/google/r/b/a/iy;

.field public static final enum c:Lcom/google/r/b/a/iy;

.field private static final synthetic e:[Lcom/google/r/b/a/iy;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 120
    new-instance v0, Lcom/google/r/b/a/iy;

    const-string v1, "NEW_PAGE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/iy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/iy;->a:Lcom/google/r/b/a/iy;

    .line 124
    new-instance v0, Lcom/google/r/b/a/iy;

    const-string v1, "CURRENT_PAGE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/iy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/iy;->b:Lcom/google/r/b/a/iy;

    .line 128
    new-instance v0, Lcom/google/r/b/a/iy;

    const-string v1, "MAP_VIEW"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/iy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/iy;->c:Lcom/google/r/b/a/iy;

    .line 115
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/iy;

    sget-object v1, Lcom/google/r/b/a/iy;->a:Lcom/google/r/b/a/iy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/iy;->b:Lcom/google/r/b/a/iy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/iy;->c:Lcom/google/r/b/a/iy;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/iy;->e:[Lcom/google/r/b/a/iy;

    .line 163
    new-instance v0, Lcom/google/r/b/a/iz;

    invoke-direct {v0}, Lcom/google/r/b/a/iz;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 172
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 173
    iput p3, p0, Lcom/google/r/b/a/iy;->d:I

    .line 174
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/iy;
    .locals 1

    .prologue
    .line 150
    packed-switch p0, :pswitch_data_0

    .line 154
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 151
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/iy;->a:Lcom/google/r/b/a/iy;

    goto :goto_0

    .line 152
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/iy;->b:Lcom/google/r/b/a/iy;

    goto :goto_0

    .line 153
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/iy;->c:Lcom/google/r/b/a/iy;

    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/iy;
    .locals 1

    .prologue
    .line 115
    const-class v0, Lcom/google/r/b/a/iy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/iy;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/iy;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/google/r/b/a/iy;->e:[Lcom/google/r/b/a/iy;

    invoke-virtual {v0}, [Lcom/google/r/b/a/iy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/iy;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/google/r/b/a/iy;->d:I

    return v0
.end method
