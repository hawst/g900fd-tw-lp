.class public final Lcom/google/r/b/a/bg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/bh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/be;",
        "Lcom/google/r/b/a/bg;",
        ">;",
        "Lcom/google/r/b/a/bh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 378
    sget-object v0, Lcom/google/r/b/a/be;->f:Lcom/google/r/b/a/be;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 468
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    .line 604
    iput v1, p0, Lcom/google/r/b/a/bg;->c:I

    .line 640
    iput v1, p0, Lcom/google/r/b/a/bg;->d:I

    .line 676
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    .line 379
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 370
    new-instance v2, Lcom/google/r/b/a/be;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/be;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/bg;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/r/b/a/bg;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/bg;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/bg;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/bg;->c:I

    iput v1, v2, Lcom/google/r/b/a/be;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/bg;->d:I

    iput v1, v2, Lcom/google/r/b/a/be;->d:I

    iget v1, p0, Lcom/google/r/b/a/bg;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/bg;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/r/b/a/bg;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/be;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 370
    check-cast p1, Lcom/google/r/b/a/be;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/bg;->a(Lcom/google/r/b/a/be;)Lcom/google/r/b/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/be;)Lcom/google/r/b/a/bg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 424
    invoke-static {}, Lcom/google/r/b/a/be;->d()Lcom/google/r/b/a/be;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 452
    :goto_0
    return-object p0

    .line 425
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 426
    iget-object v2, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 427
    iget-object v2, p1, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    .line 428
    iget v2, p0, Lcom/google/r/b/a/bg;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/bg;->a:I

    .line 435
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/r/b/a/be;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 436
    iget v2, p1, Lcom/google/r/b/a/be;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/ca;->a(I)Lcom/google/r/b/a/ca;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/r/b/a/ca;->a:Lcom/google/r/b/a/ca;

    :cond_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 430
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/bg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/bg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/bg;->a:I

    .line 431
    :cond_4
    iget-object v2, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/be;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 435
    goto :goto_2

    .line 436
    :cond_6
    iget v3, p0, Lcom/google/r/b/a/bg;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/bg;->a:I

    iget v2, v2, Lcom/google/r/b/a/ca;->f:I

    iput v2, p0, Lcom/google/r/b/a/bg;->c:I

    .line 438
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/be;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    :goto_3
    if-eqz v0, :cond_b

    .line 439
    iget v0, p1, Lcom/google/r/b/a/be;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/df;->a(I)Lcom/google/r/b/a/df;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/r/b/a/df;->a:Lcom/google/r/b/a/df;

    :cond_8
    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    move v0, v1

    .line 438
    goto :goto_3

    .line 439
    :cond_a
    iget v1, p0, Lcom/google/r/b/a/bg;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/bg;->a:I

    iget v0, v0, Lcom/google/r/b/a/df;->e:I

    iput v0, p0, Lcom/google/r/b/a/bg;->d:I

    .line 441
    :cond_b
    iget-object v0, p1, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 442
    iget-object v0, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 443
    iget-object v0, p1, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    .line 444
    iget v0, p0, Lcom/google/r/b/a/bg;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/r/b/a/bg;->a:I

    .line 451
    :cond_c
    :goto_4
    iget-object v0, p1, Lcom/google/r/b/a/be;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 446
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/bg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_e

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/bg;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/bg;->a:I

    .line 447
    :cond_e
    iget-object v0, p0, Lcom/google/r/b/a/bg;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/be;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 456
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/google/r/b/a/bg;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/im;->d()Lcom/google/r/b/a/im;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/im;

    invoke-virtual {v0}, Lcom/google/r/b/a/im;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 462
    :goto_1
    return v2

    .line 456
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 462
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
