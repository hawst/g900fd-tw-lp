.class public final enum Lcom/google/r/b/a/acy;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/acy;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/acy;

.field public static final enum b:Lcom/google/r/b/a/acy;

.field public static final enum c:Lcom/google/r/b/a/acy;

.field public static final enum d:Lcom/google/r/b/a/acy;

.field public static final enum e:Lcom/google/r/b/a/acy;

.field public static final enum f:Lcom/google/r/b/a/acy;

.field public static final enum g:Lcom/google/r/b/a/acy;

.field public static final enum h:Lcom/google/r/b/a/acy;

.field public static final enum i:Lcom/google/r/b/a/acy;

.field public static final enum j:Lcom/google/r/b/a/acy;

.field public static final enum k:Lcom/google/r/b/a/acy;

.field public static final enum l:Lcom/google/r/b/a/acy;

.field public static final enum m:Lcom/google/r/b/a/acy;

.field public static final enum n:Lcom/google/r/b/a/acy;

.field public static final enum o:Lcom/google/r/b/a/acy;

.field public static final enum p:Lcom/google/r/b/a/acy;

.field public static final enum q:Lcom/google/r/b/a/acy;

.field public static final enum r:Lcom/google/r/b/a/acy;

.field public static final enum s:Lcom/google/r/b/a/acy;

.field private static final synthetic u:[Lcom/google/r/b/a/acy;


# instance fields
.field public final t:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 86
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PIXEL_15"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->a:Lcom/google/r/b/a/acy;

    .line 90
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PIXEL_22"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->b:Lcom/google/r/b/a/acy;

    .line 94
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PIXEL_30"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->c:Lcom/google/r/b/a/acy;

    .line 98
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PIXEL_44"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->d:Lcom/google/r/b/a/acy;

    .line 102
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_19_BG_LIGHT"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->e:Lcom/google/r/b/a/acy;

    .line 106
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_30_BG_LIGHT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->f:Lcom/google/r/b/a/acy;

    .line 110
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_38_BG_LIGHT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->g:Lcom/google/r/b/a/acy;

    .line 114
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_60_BG_LIGHT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->h:Lcom/google/r/b/a/acy;

    .line 118
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_38_BG_DARK"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->i:Lcom/google/r/b/a/acy;

    .line 122
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_60_BG_DARK"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->j:Lcom/google/r/b/a/acy;

    .line 126
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_76_BG_DARK"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->k:Lcom/google/r/b/a/acy;

    .line 130
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_120_BG_DARK"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->l:Lcom/google/r/b/a/acy;

    .line 134
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "SVG_LIGHT"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    .line 138
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "SVG_DARK"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->n:Lcom/google/r/b/a/acy;

    .line 142
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_22_BG_LIGHT"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->o:Lcom/google/r/b/a/acy;

    .line 146
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_33_BG_LIGHT"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->p:Lcom/google/r/b/a/acy;

    .line 150
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_44_BG_LIGHT"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->q:Lcom/google/r/b/a/acy;

    .line 154
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "PX_66_BG_LIGHT"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->r:Lcom/google/r/b/a/acy;

    .line 158
    new-instance v0, Lcom/google/r/b/a/acy;

    const-string v1, "SVG_INCIDENT_LIGHT"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/acy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    .line 81
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/google/r/b/a/acy;

    sget-object v1, Lcom/google/r/b/a/acy;->a:Lcom/google/r/b/a/acy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/acy;->b:Lcom/google/r/b/a/acy;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/acy;->c:Lcom/google/r/b/a/acy;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/acy;->d:Lcom/google/r/b/a/acy;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/acy;->e:Lcom/google/r/b/a/acy;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/acy;->f:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/acy;->g:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/r/b/a/acy;->h:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/acy;->i:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/r/b/a/acy;->j:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/r/b/a/acy;->k:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/r/b/a/acy;->l:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/r/b/a/acy;->n:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/r/b/a/acy;->o:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/r/b/a/acy;->p:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/r/b/a/acy;->q:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/r/b/a/acy;->r:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/acy;->u:[Lcom/google/r/b/a/acy;

    .line 273
    new-instance v0, Lcom/google/r/b/a/acz;

    invoke-direct {v0}, Lcom/google/r/b/a/acz;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 282
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 283
    iput p3, p0, Lcom/google/r/b/a/acy;->t:I

    .line 284
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/acy;
    .locals 1

    .prologue
    .line 244
    packed-switch p0, :pswitch_data_0

    .line 264
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 245
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/acy;->a:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 246
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/acy;->b:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 247
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/acy;->c:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 248
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/acy;->d:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 249
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/acy;->e:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 250
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/acy;->f:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 251
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/acy;->g:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 252
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/acy;->h:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 253
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/acy;->i:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 254
    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/acy;->j:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 255
    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/acy;->k:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 256
    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/acy;->l:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 257
    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 258
    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/acy;->n:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 259
    :pswitch_e
    sget-object v0, Lcom/google/r/b/a/acy;->o:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 260
    :pswitch_f
    sget-object v0, Lcom/google/r/b/a/acy;->p:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 261
    :pswitch_10
    sget-object v0, Lcom/google/r/b/a/acy;->q:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 262
    :pswitch_11
    sget-object v0, Lcom/google/r/b/a/acy;->r:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 263
    :pswitch_12
    sget-object v0, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    goto :goto_0

    .line 244
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/acy;
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/google/r/b/a/acy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acy;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/acy;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/google/r/b/a/acy;->u:[Lcom/google/r/b/a/acy;

    invoke-virtual {v0}, [Lcom/google/r/b/a/acy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/acy;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lcom/google/r/b/a/acy;->t:I

    return v0
.end method
