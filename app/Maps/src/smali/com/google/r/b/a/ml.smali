.class public final Lcom/google/r/b/a/ml;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ms;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ml;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/ml;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/mo;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 540
    new-instance v0, Lcom/google/r/b/a/mm;

    invoke-direct {v0}, Lcom/google/r/b/a/mm;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ml;->PARSER:Lcom/google/n/ax;

    .line 1048
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ml;->h:Lcom/google/n/aw;

    .line 1498
    new-instance v0, Lcom/google/r/b/a/ml;

    invoke-direct {v0}, Lcom/google/r/b/a/ml;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ml;->e:Lcom/google/r/b/a/ml;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 467
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 992
    iput-byte v0, p0, Lcom/google/r/b/a/ml;->f:B

    .line 1023
    iput v0, p0, Lcom/google/r/b/a/ml;->g:I

    .line 468
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    .line 469
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    .line 470
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ml;->d:I

    .line 471
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v2, 0x1

    .line 477
    invoke-direct {p0}, Lcom/google/r/b/a/ml;-><init>()V

    .line 480
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 483
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 484
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 485
    sparse-switch v4, :sswitch_data_0

    .line 490
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 492
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 488
    goto :goto_0

    .line 497
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 498
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    .line 499
    or-int/lit8 v1, v1, 0x1

    .line 501
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    sget-object v5, Lcom/google/r/b/a/mo;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 525
    :catch_0
    move-exception v0

    .line 526
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 531
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_2

    .line 532
    iget-object v2, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    .line 534
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_3

    .line 535
    iget-object v1, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    .line 537
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ml;->au:Lcom/google/n/bn;

    throw v0

    .line 505
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v6, :cond_4

    .line 506
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    .line 507
    or-int/lit8 v1, v1, 0x2

    .line 509
    :cond_4
    iget-object v4, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    sget-object v5, Lcom/google/r/b/a/aje;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 527
    :catch_1
    move-exception v0

    .line 528
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 529
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 513
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 514
    invoke-static {v4}, Lcom/google/maps/g/mc;->a(I)Lcom/google/maps/g/mc;

    move-result-object v5

    .line 515
    if-nez v5, :cond_5

    .line 516
    const/4 v5, 0x3

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 518
    :cond_5
    iget v5, p0, Lcom/google/r/b/a/ml;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/ml;->a:I

    .line 519
    iput v4, p0, Lcom/google/r/b/a/ml;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 531
    :cond_6
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_7

    .line 532
    iget-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    .line 534
    :cond_7
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_8

    .line 535
    iget-object v0, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    .line 537
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ml;->au:Lcom/google/n/bn;

    .line 538
    return-void

    .line 485
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 465
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 992
    iput-byte v0, p0, Lcom/google/r/b/a/ml;->f:B

    .line 1023
    iput v0, p0, Lcom/google/r/b/a/ml;->g:I

    .line 466
    return-void
.end method

.method public static a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;
    .locals 1

    .prologue
    .line 1113
    invoke-static {}, Lcom/google/r/b/a/ml;->newBuilder()Lcom/google/r/b/a/mn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/mn;->a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/ml;
    .locals 1

    .prologue
    .line 1501
    sget-object v0, Lcom/google/r/b/a/ml;->e:Lcom/google/r/b/a/ml;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/mn;
    .locals 1

    .prologue
    .line 1110
    new-instance v0, Lcom/google/r/b/a/mn;

    invoke-direct {v0}, Lcom/google/r/b/a/mn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ml;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552
    sget-object v0, Lcom/google/r/b/a/ml;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1010
    invoke-virtual {p0}, Lcom/google/r/b/a/ml;->c()I

    move v1, v2

    .line 1011
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1012
    iget-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1011
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1014
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1015
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1014
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1017
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ml;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 1018
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/ml;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 1020
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ml;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1021
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 994
    iget-byte v0, p0, Lcom/google/r/b/a/ml;->f:B

    .line 995
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1005
    :cond_0
    :goto_0
    return v2

    .line 996
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 998
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 999
    iget-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/mo;

    invoke-virtual {v0}, Lcom/google/r/b/a/mo;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1000
    iput-byte v2, p0, Lcom/google/r/b/a/ml;->f:B

    goto :goto_0

    .line 998
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1004
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/ml;->f:B

    move v2, v3

    .line 1005
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1025
    iget v0, p0, Lcom/google/r/b/a/ml;->g:I

    .line 1026
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1043
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1029
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1030
    iget-object v0, p0, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    .line 1031
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1029
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 1033
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1034
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    .line 1035
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1033
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1037
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ml;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_3

    .line 1038
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/ml;->d:I

    .line 1039
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_4

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1041
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/ml;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1042
    iput v0, p0, Lcom/google/r/b/a/ml;->g:I

    goto :goto_0

    .line 1039
    :cond_4
    const/16 v0, 0xa

    goto :goto_3
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 459
    invoke-static {}, Lcom/google/r/b/a/ml;->newBuilder()Lcom/google/r/b/a/mn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/mn;->a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 459
    invoke-static {}, Lcom/google/r/b/a/ml;->newBuilder()Lcom/google/r/b/a/mn;

    move-result-object v0

    return-object v0
.end method
