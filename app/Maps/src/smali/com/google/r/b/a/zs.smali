.class public final Lcom/google/r/b/a/zs;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/zq;",
        "Lcom/google/r/b/a/zs;",
        ">;",
        "Lcom/google/r/b/a/zv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 868
    sget-object v0, Lcom/google/r/b/a/zq;->j:Lcom/google/r/b/a/zq;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 985
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zs;->b:Ljava/lang/Object;

    .line 1061
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zs;->c:Ljava/lang/Object;

    .line 1137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zs;->d:Ljava/lang/Object;

    .line 1213
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zs;->e:Ljava/lang/Object;

    .line 1289
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zs;->f:Ljava/lang/Object;

    .line 1365
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zs;->g:Ljava/lang/Object;

    .line 1441
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zs;->h:Ljava/lang/Object;

    .line 1517
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/zs;->i:I

    .line 869
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 860
    new-instance v2, Lcom/google/r/b/a/zq;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/zq;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/zs;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/zs;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/zs;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zq;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/zs;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zq;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/zs;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zq;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/zs;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/zs;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/zs;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/r/b/a/zs;->i:I

    iput v1, v2, Lcom/google/r/b/a/zq;->i:I

    iput v0, v2, Lcom/google/r/b/a/zq;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 860
    check-cast p1, Lcom/google/r/b/a/zq;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/zs;->a(Lcom/google/r/b/a/zq;)Lcom/google/r/b/a/zs;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/zq;)Lcom/google/r/b/a/zs;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 936
    invoke-static {}, Lcom/google/r/b/a/zq;->d()Lcom/google/r/b/a/zq;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 976
    :goto_0
    return-object p0

    .line 937
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 938
    iget v2, p0, Lcom/google/r/b/a/zs;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/zs;->a:I

    .line 939
    iget-object v2, p1, Lcom/google/r/b/a/zq;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zs;->b:Ljava/lang/Object;

    .line 942
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 943
    iget v2, p0, Lcom/google/r/b/a/zs;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/zs;->a:I

    .line 944
    iget-object v2, p1, Lcom/google/r/b/a/zq;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zs;->c:Ljava/lang/Object;

    .line 947
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 948
    iget v2, p0, Lcom/google/r/b/a/zs;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/zs;->a:I

    .line 949
    iget-object v2, p1, Lcom/google/r/b/a/zq;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zs;->d:Ljava/lang/Object;

    .line 952
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 953
    iget v2, p0, Lcom/google/r/b/a/zs;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/zs;->a:I

    .line 954
    iget-object v2, p1, Lcom/google/r/b/a/zq;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zs;->e:Ljava/lang/Object;

    .line 957
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 958
    iget v2, p0, Lcom/google/r/b/a/zs;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/zs;->a:I

    .line 959
    iget-object v2, p1, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zs;->f:Ljava/lang/Object;

    .line 962
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 963
    iget v2, p0, Lcom/google/r/b/a/zs;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/zs;->a:I

    .line 964
    iget-object v2, p1, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zs;->g:Ljava/lang/Object;

    .line 967
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/zq;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 968
    iget v2, p0, Lcom/google/r/b/a/zs;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/zs;->a:I

    .line 969
    iget-object v2, p1, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zs;->h:Ljava/lang/Object;

    .line 972
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/zq;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_12

    .line 973
    iget v0, p1, Lcom/google/r/b/a/zq;->i:I

    invoke-static {v0}, Lcom/google/r/b/a/zt;->a(I)Lcom/google/r/b/a/zt;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/r/b/a/zt;->a:Lcom/google/r/b/a/zt;

    :cond_8
    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    move v2, v1

    .line 937
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 942
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 947
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 952
    goto :goto_4

    :cond_d
    move v2, v1

    .line 957
    goto :goto_5

    :cond_e
    move v2, v1

    .line 962
    goto :goto_6

    :cond_f
    move v2, v1

    .line 967
    goto :goto_7

    :cond_10
    move v0, v1

    .line 972
    goto :goto_8

    .line 973
    :cond_11
    iget v1, p0, Lcom/google/r/b/a/zs;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/zs;->a:I

    iget v0, v0, Lcom/google/r/b/a/zt;->d:I

    iput v0, p0, Lcom/google/r/b/a/zs;->i:I

    .line 975
    :cond_12
    iget-object v0, p1, Lcom/google/r/b/a/zq;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 980
    const/4 v0, 0x1

    return v0
.end method
