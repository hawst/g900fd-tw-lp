.class public final enum Lcom/google/r/b/a/g;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/g;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/g;

.field public static final enum b:Lcom/google/r/b/a/g;

.field public static final enum c:Lcom/google/r/b/a/g;

.field public static final enum d:Lcom/google/r/b/a/g;

.field private static final synthetic f:[Lcom/google/r/b/a/g;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4189
    new-instance v0, Lcom/google/r/b/a/g;

    const-string v1, "FIRST_NAME"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/r/b/a/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/g;->a:Lcom/google/r/b/a/g;

    .line 4193
    new-instance v0, Lcom/google/r/b/a/g;

    const-string v1, "LAST_NAME"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/g;->b:Lcom/google/r/b/a/g;

    .line 4197
    new-instance v0, Lcom/google/r/b/a/g;

    const-string v1, "EMAIL_ADDRESS"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/r/b/a/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/g;->c:Lcom/google/r/b/a/g;

    .line 4201
    new-instance v0, Lcom/google/r/b/a/g;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/r/b/a/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/g;->d:Lcom/google/r/b/a/g;

    .line 4184
    new-array v0, v6, [Lcom/google/r/b/a/g;

    sget-object v1, Lcom/google/r/b/a/g;->a:Lcom/google/r/b/a/g;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/g;->b:Lcom/google/r/b/a/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/g;->c:Lcom/google/r/b/a/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/g;->d:Lcom/google/r/b/a/g;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/g;->f:[Lcom/google/r/b/a/g;

    .line 4241
    new-instance v0, Lcom/google/r/b/a/h;

    invoke-direct {v0}, Lcom/google/r/b/a/h;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 4250
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4251
    iput p3, p0, Lcom/google/r/b/a/g;->e:I

    .line 4252
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/g;
    .locals 1

    .prologue
    .line 4227
    packed-switch p0, :pswitch_data_0

    .line 4232
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4228
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/g;->a:Lcom/google/r/b/a/g;

    goto :goto_0

    .line 4229
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/g;->b:Lcom/google/r/b/a/g;

    goto :goto_0

    .line 4230
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/g;->c:Lcom/google/r/b/a/g;

    goto :goto_0

    .line 4231
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/g;->d:Lcom/google/r/b/a/g;

    goto :goto_0

    .line 4227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/g;
    .locals 1

    .prologue
    .line 4184
    const-class v0, Lcom/google/r/b/a/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/g;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/g;
    .locals 1

    .prologue
    .line 4184
    sget-object v0, Lcom/google/r/b/a/g;->f:[Lcom/google/r/b/a/g;

    invoke-virtual {v0}, [Lcom/google/r/b/a/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/g;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 4223
    iget v0, p0, Lcom/google/r/b/a/g;->e:I

    return v0
.end method
