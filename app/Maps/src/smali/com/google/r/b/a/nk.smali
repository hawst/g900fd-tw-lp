.class public final Lcom/google/r/b/a/nk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ni;",
        "Lcom/google/r/b/a/nk;",
        ">;",
        "Lcom/google/r/b/a/nl;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 339
    sget-object v0, Lcom/google/r/b/a/ni;->f:Lcom/google/r/b/a/ni;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 415
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/nk;->b:Lcom/google/n/ao;

    .line 474
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/nk;->c:Lcom/google/n/ao;

    .line 533
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/nk;->d:Lcom/google/n/ao;

    .line 592
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/nk;->e:Z

    .line 340
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 331
    new-instance v2, Lcom/google/r/b/a/ni;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ni;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/nk;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/ni;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/nk;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/nk;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/ni;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/nk;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/nk;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/ni;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/nk;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/nk;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/r/b/a/nk;->e:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/ni;->e:Z

    iput v0, v2, Lcom/google/r/b/a/ni;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 331
    check-cast p1, Lcom/google/r/b/a/ni;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/nk;->a(Lcom/google/r/b/a/ni;)Lcom/google/r/b/a/nk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ni;)Lcom/google/r/b/a/nk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 389
    invoke-static {}, Lcom/google/r/b/a/ni;->d()Lcom/google/r/b/a/ni;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 406
    :goto_0
    return-object p0

    .line 390
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 391
    iget-object v2, p0, Lcom/google/r/b/a/nk;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ni;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 392
    iget v2, p0, Lcom/google/r/b/a/nk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/nk;->a:I

    .line 394
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 395
    iget-object v2, p0, Lcom/google/r/b/a/nk;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ni;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 396
    iget v2, p0, Lcom/google/r/b/a/nk;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/nk;->a:I

    .line 398
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 399
    iget-object v2, p0, Lcom/google/r/b/a/nk;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ni;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 400
    iget v2, p0, Lcom/google/r/b/a/nk;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/nk;->a:I

    .line 402
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 403
    iget-boolean v0, p1, Lcom/google/r/b/a/ni;->e:Z

    iget v1, p0, Lcom/google/r/b/a/nk;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/nk;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/nk;->e:Z

    .line 405
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/ni;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 390
    goto :goto_1

    :cond_6
    move v2, v1

    .line 394
    goto :goto_2

    :cond_7
    move v2, v1

    .line 398
    goto :goto_3

    :cond_8
    move v0, v1

    .line 402
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 410
    const/4 v0, 0x1

    return v0
.end method
