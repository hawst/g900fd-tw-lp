.class public final enum Lcom/google/r/b/a/yz;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/yz;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/yz;

.field public static final enum b:Lcom/google/r/b/a/yz;

.field private static final synthetic d:[Lcom/google/r/b/a/yz;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 294
    new-instance v0, Lcom/google/r/b/a/yz;

    const-string v1, "OUTDOOR"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/yz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/yz;->a:Lcom/google/r/b/a/yz;

    .line 298
    new-instance v0, Lcom/google/r/b/a/yz;

    const-string v1, "INDOOR"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/yz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/yz;->b:Lcom/google/r/b/a/yz;

    .line 289
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/r/b/a/yz;

    sget-object v1, Lcom/google/r/b/a/yz;->a:Lcom/google/r/b/a/yz;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/yz;->b:Lcom/google/r/b/a/yz;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/r/b/a/yz;->d:[Lcom/google/r/b/a/yz;

    .line 328
    new-instance v0, Lcom/google/r/b/a/za;

    invoke-direct {v0}, Lcom/google/r/b/a/za;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 337
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 338
    iput p3, p0, Lcom/google/r/b/a/yz;->c:I

    .line 339
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/yz;
    .locals 1

    .prologue
    .line 316
    packed-switch p0, :pswitch_data_0

    .line 319
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 317
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/yz;->a:Lcom/google/r/b/a/yz;

    goto :goto_0

    .line 318
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/yz;->b:Lcom/google/r/b/a/yz;

    goto :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/yz;
    .locals 1

    .prologue
    .line 289
    const-class v0, Lcom/google/r/b/a/yz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yz;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/yz;
    .locals 1

    .prologue
    .line 289
    sget-object v0, Lcom/google/r/b/a/yz;->d:[Lcom/google/r/b/a/yz;

    invoke-virtual {v0}, [Lcom/google/r/b/a/yz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/yz;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lcom/google/r/b/a/yz;->c:I

    return v0
.end method
