.class public final Lcom/google/r/b/a/hw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/hz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/hw;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/hw;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 489
    new-instance v0, Lcom/google/r/b/a/hx;

    invoke-direct {v0}, Lcom/google/r/b/a/hx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/hw;->PARSER:Lcom/google/n/ax;

    .line 586
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/hw;->g:Lcom/google/n/aw;

    .line 791
    new-instance v0, Lcom/google/r/b/a/hw;

    invoke-direct {v0}, Lcom/google/r/b/a/hw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/hw;->d:Lcom/google/r/b/a/hw;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 440
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 535
    iput-byte v0, p0, Lcom/google/r/b/a/hw;->e:B

    .line 565
    iput v0, p0, Lcom/google/r/b/a/hw;->f:I

    .line 441
    iput v1, p0, Lcom/google/r/b/a/hw;->b:I

    .line 442
    iput v1, p0, Lcom/google/r/b/a/hw;->c:I

    .line 443
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 449
    invoke-direct {p0}, Lcom/google/r/b/a/hw;-><init>()V

    .line 450
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 454
    const/4 v0, 0x0

    .line 455
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 456
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 457
    sparse-switch v3, :sswitch_data_0

    .line 462
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 464
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 460
    goto :goto_0

    .line 469
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/hw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/hw;->a:I

    .line 470
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    ushr-int/lit8 v4, v3, 0x1

    and-int/lit8 v3, v3, 0x1

    neg-int v3, v3

    xor-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/hw;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 480
    :catch_0
    move-exception v0

    .line 481
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/hw;->au:Lcom/google/n/bn;

    throw v0

    .line 474
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/hw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/hw;->a:I

    .line 475
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    ushr-int/lit8 v4, v3, 0x1

    and-int/lit8 v3, v3, 0x1

    neg-int v3, v3

    xor-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/hw;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 482
    :catch_1
    move-exception v0

    .line 483
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 484
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 486
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hw;->au:Lcom/google/n/bn;

    .line 487
    return-void

    .line 457
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 438
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 535
    iput-byte v0, p0, Lcom/google/r/b/a/hw;->e:B

    .line 565
    iput v0, p0, Lcom/google/r/b/a/hw;->f:I

    .line 439
    return-void
.end method

.method public static d()Lcom/google/r/b/a/hw;
    .locals 1

    .prologue
    .line 794
    sget-object v0, Lcom/google/r/b/a/hw;->d:Lcom/google/r/b/a/hw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/hy;
    .locals 1

    .prologue
    .line 648
    new-instance v0, Lcom/google/r/b/a/hy;

    invoke-direct {v0}, Lcom/google/r/b/a/hy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/hw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 501
    sget-object v0, Lcom/google/r/b/a/hw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 555
    invoke-virtual {p0}, Lcom/google/r/b/a/hw;->c()I

    .line 556
    iget v0, p0, Lcom/google/r/b/a/hw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 557
    iget v0, p0, Lcom/google/r/b/a/hw;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->c(II)V

    .line 559
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/hw;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 560
    iget v0, p0, Lcom/google/r/b/a/hw;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->c(II)V

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/hw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 563
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 537
    iget-byte v2, p0, Lcom/google/r/b/a/hw;->e:B

    .line 538
    if-ne v2, v0, :cond_0

    .line 550
    :goto_0
    return v0

    .line 539
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 541
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/hw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 542
    iput-byte v1, p0, Lcom/google/r/b/a/hw;->e:B

    move v0, v1

    .line 543
    goto :goto_0

    :cond_2
    move v2, v1

    .line 541
    goto :goto_1

    .line 545
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/hw;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 546
    iput-byte v1, p0, Lcom/google/r/b/a/hw;->e:B

    move v0, v1

    .line 547
    goto :goto_0

    :cond_4
    move v2, v1

    .line 545
    goto :goto_2

    .line 549
    :cond_5
    iput-byte v0, p0, Lcom/google/r/b/a/hw;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 567
    iget v0, p0, Lcom/google/r/b/a/hw;->f:I

    .line 568
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 581
    :goto_0
    return v0

    .line 571
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/hw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 572
    iget v0, p0, Lcom/google/r/b/a/hw;->b:I

    .line 573
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    shl-int/lit8 v3, v0, 0x1

    shr-int/lit8 v0, v0, 0x1f

    xor-int/2addr v0, v3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 575
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/hw;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 576
    iget v2, p0, Lcom/google/r/b/a/hw;->c:I

    .line 577
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    shl-int/lit8 v3, v2, 0x1

    shr-int/lit8 v2, v2, 0x1f

    xor-int/2addr v2, v3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 579
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/hw;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 580
    iput v0, p0, Lcom/google/r/b/a/hw;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 432
    invoke-static {}, Lcom/google/r/b/a/hw;->newBuilder()Lcom/google/r/b/a/hy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/hy;->a(Lcom/google/r/b/a/hw;)Lcom/google/r/b/a/hy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 432
    invoke-static {}, Lcom/google/r/b/a/hw;->newBuilder()Lcom/google/r/b/a/hy;

    move-result-object v0

    return-object v0
.end method
