.class public final Lcom/google/r/b/a/ci;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ce;",
        "Lcom/google/r/b/a/ci;",
        ">;",
        "Lcom/google/r/b/a/cj;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1947
    sget-object v0, Lcom/google/r/b/a/ce;->c:Lcom/google/r/b/a/ce;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2011
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    .line 2085
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    .line 1948
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 1939
    new-instance v0, Lcom/google/r/b/a/ce;

    invoke-direct {v0, p0}, Lcom/google/r/b/a/ce;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/r/b/a/ci;->c:I

    iget v1, p0, Lcom/google/r/b/a/ci;->c:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ci;->c:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/ci;->c:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    iput-object v1, v0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ci;->c:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ci;->c:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/ci;->c:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1939
    check-cast p1, Lcom/google/r/b/a/ce;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ci;->a(Lcom/google/r/b/a/ce;)Lcom/google/r/b/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ce;)Lcom/google/r/b/a/ci;
    .locals 2

    .prologue
    .line 1979
    invoke-static {}, Lcom/google/r/b/a/ce;->d()Lcom/google/r/b/a/ce;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2001
    :goto_0
    return-object p0

    .line 1980
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1981
    iget-object v0, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1982
    iget-object v0, p1, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    .line 1983
    iget v0, p0, Lcom/google/r/b/a/ci;->c:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/r/b/a/ci;->c:I

    .line 1990
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1991
    iget-object v0, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1992
    iget-object v0, p1, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    .line 1993
    iget v0, p0, Lcom/google/r/b/a/ci;->c:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/ci;->c:I

    .line 2000
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/ce;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1985
    :cond_3
    invoke-virtual {p0}, Lcom/google/r/b/a/ci;->c()V

    .line 1986
    iget-object v0, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1995
    :cond_4
    invoke-virtual {p0}, Lcom/google/r/b/a/ci;->d()V

    .line 1996
    iget-object v0, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2005
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2013
    iget v0, p0, Lcom/google/r/b/a/ci;->c:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 2014
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ci;->a:Ljava/util/List;

    .line 2015
    iget v0, p0, Lcom/google/r/b/a/ci;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ci;->c:I

    .line 2017
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 2087
    iget v0, p0, Lcom/google/r/b/a/ci;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2088
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ci;->b:Ljava/util/List;

    .line 2089
    iget v0, p0, Lcom/google/r/b/a/ci;->c:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ci;->c:I

    .line 2091
    :cond_0
    return-void
.end method
