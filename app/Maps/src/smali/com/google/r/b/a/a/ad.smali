.class public final Lcom/google/r/b/a/a/ad;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/ae;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/a/ab;",
        "Lcom/google/r/b/a/a/ad;",
        ">;",
        "Lcom/google/r/b/a/a/ae;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:J

.field private d:I

.field private e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1839
    sget-object v0, Lcom/google/r/b/a/a/ab;->f:Lcom/google/r/b/a/a/ab;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1924
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    .line 2092
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/ad;->e:Lcom/google/n/ao;

    .line 1840
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1831
    new-instance v2, Lcom/google/r/b/a/a/ab;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/a/ab;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/a/ad;->a:I

    iget v4, p0, Lcom/google/r/b/a/a/ad;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/a/ad;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/a/ad;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/a/ad;->d:I

    iput v4, v2, Lcom/google/r/b/a/a/ab;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/a/ab;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/a/ad;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/a/ad;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-wide v4, p0, Lcom/google/r/b/a/a/ad;->c:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/ab;->e:J

    iput v0, v2, Lcom/google/r/b/a/a/ab;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1831
    check-cast p1, Lcom/google/r/b/a/a/ab;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/a/ad;->a(Lcom/google/r/b/a/a/ab;)Lcom/google/r/b/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/a/ab;)Lcom/google/r/b/a/a/ad;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1886
    invoke-static {}, Lcom/google/r/b/a/a/ab;->g()Lcom/google/r/b/a/a/ab;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1908
    :goto_0
    return-object p0

    .line 1887
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1888
    iget-object v2, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1889
    iget-object v2, p1, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    .line 1890
    iget v2, p0, Lcom/google/r/b/a/a/ad;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/a/ad;->a:I

    .line 1897
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1898
    iget v2, p1, Lcom/google/r/b/a/a/ab;->c:I

    iget v3, p0, Lcom/google/r/b/a/a/ad;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/a/ad;->a:I

    iput v2, p0, Lcom/google/r/b/a/a/ad;->d:I

    .line 1900
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1901
    iget-object v2, p0, Lcom/google/r/b/a/a/ad;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/a/ab;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1902
    iget v2, p0, Lcom/google/r/b/a/a/ad;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/a/ad;->a:I

    .line 1904
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 1905
    iget-wide v0, p1, Lcom/google/r/b/a/a/ab;->e:J

    iget v2, p0, Lcom/google/r/b/a/a/ad;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/a/ad;->a:I

    iput-wide v0, p0, Lcom/google/r/b/a/a/ad;->c:J

    .line 1907
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/a/ab;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1892
    :cond_5
    invoke-virtual {p0}, Lcom/google/r/b/a/a/ad;->c()V

    .line 1893
    iget-object v2, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 1897
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1900
    goto :goto_3

    :cond_8
    move v0, v1

    .line 1904
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1912
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1913
    iget-object v0, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/af;->g()Lcom/google/r/b/a/a/af;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    invoke-virtual {v0}, Lcom/google/r/b/a/a/af;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1918
    :goto_1
    return v2

    .line 1912
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1918
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1926
    iget v0, p0, Lcom/google/r/b/a/a/ad;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1927
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/a/ad;->b:Ljava/util/List;

    .line 1930
    iget v0, p0, Lcom/google/r/b/a/a/ad;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/a/ad;->a:I

    .line 1932
    :cond_0
    return-void
.end method
