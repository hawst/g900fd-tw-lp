.class public final Lcom/google/r/b/a/jt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ju;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/jr;",
        "Lcom/google/r/b/a/jt;",
        ">;",
        "Lcom/google/r/b/a/ju;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/r/b/a/jr;->e:Lcom/google/r/b/a/jr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 321
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/google/r/b/a/jt;->b:I

    .line 353
    const/16 v0, 0x32

    iput v0, p0, Lcom/google/r/b/a/jt;->c:I

    .line 385
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/jt;->d:I

    .line 264
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 255
    new-instance v2, Lcom/google/r/b/a/jr;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/jr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/jt;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/jt;->b:I

    iput v1, v2, Lcom/google/r/b/a/jr;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/jt;->c:I

    iput v1, v2, Lcom/google/r/b/a/jr;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/jt;->d:I

    iput v1, v2, Lcom/google/r/b/a/jr;->d:I

    iput v0, v2, Lcom/google/r/b/a/jr;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 255
    check-cast p1, Lcom/google/r/b/a/jr;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/jt;->a(Lcom/google/r/b/a/jr;)Lcom/google/r/b/a/jt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/jr;)Lcom/google/r/b/a/jt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 301
    invoke-static {}, Lcom/google/r/b/a/jr;->d()Lcom/google/r/b/a/jr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 312
    :goto_0
    return-object p0

    .line 302
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/jr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 303
    iget v2, p1, Lcom/google/r/b/a/jr;->b:I

    iget v3, p0, Lcom/google/r/b/a/jt;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/jt;->a:I

    iput v2, p0, Lcom/google/r/b/a/jt;->b:I

    .line 305
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/jr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 306
    iget v2, p1, Lcom/google/r/b/a/jr;->c:I

    iget v3, p0, Lcom/google/r/b/a/jt;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/jt;->a:I

    iput v2, p0, Lcom/google/r/b/a/jt;->c:I

    .line 308
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/jr;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 309
    iget v0, p1, Lcom/google/r/b/a/jr;->d:I

    iget v1, p0, Lcom/google/r/b/a/jt;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/jt;->a:I

    iput v0, p0, Lcom/google/r/b/a/jt;->d:I

    .line 311
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/jr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 302
    goto :goto_1

    :cond_5
    move v2, v1

    .line 305
    goto :goto_2

    :cond_6
    move v0, v1

    .line 308
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x1

    return v0
.end method
