.class public final enum Lcom/google/r/b/a/alx;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/alx;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/alx;

.field public static final enum b:Lcom/google/r/b/a/alx;

.field public static final enum c:Lcom/google/r/b/a/alx;

.field private static final synthetic e:[Lcom/google/r/b/a/alx;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1350
    new-instance v0, Lcom/google/r/b/a/alx;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/r/b/a/alx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/alx;->a:Lcom/google/r/b/a/alx;

    .line 1351
    new-instance v0, Lcom/google/r/b/a/alx;

    const-string v1, "TRANSIT_TRIP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, Lcom/google/r/b/a/alx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/alx;->b:Lcom/google/r/b/a/alx;

    .line 1352
    new-instance v0, Lcom/google/r/b/a/alx;

    const-string v1, "RESULT_NOT_SET"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/r/b/a/alx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/alx;->c:Lcom/google/r/b/a/alx;

    .line 1348
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/alx;

    sget-object v1, Lcom/google/r/b/a/alx;->a:Lcom/google/r/b/a/alx;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/alx;->b:Lcom/google/r/b/a/alx;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/alx;->c:Lcom/google/r/b/a/alx;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/alx;->e:[Lcom/google/r/b/a/alx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1354
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1353
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/alx;->d:I

    .line 1355
    iput p3, p0, Lcom/google/r/b/a/alx;->d:I

    .line 1356
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/alx;
    .locals 2

    .prologue
    .line 1358
    packed-switch p0, :pswitch_data_0

    .line 1362
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value is undefined for this oneof enum."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1359
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/alx;->a:Lcom/google/r/b/a/alx;

    .line 1361
    :goto_0
    return-object v0

    .line 1360
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/alx;->b:Lcom/google/r/b/a/alx;

    goto :goto_0

    .line 1361
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/alx;->c:Lcom/google/r/b/a/alx;

    goto :goto_0

    .line 1358
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/alx;
    .locals 1

    .prologue
    .line 1348
    const-class v0, Lcom/google/r/b/a/alx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alx;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/alx;
    .locals 1

    .prologue
    .line 1348
    sget-object v0, Lcom/google/r/b/a/alx;->e:[Lcom/google/r/b/a/alx;

    invoke-virtual {v0}, [Lcom/google/r/b/a/alx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/alx;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1367
    iget v0, p0, Lcom/google/r/b/a/alx;->d:I

    return v0
.end method
