.class public final Lcom/google/r/b/a/r;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/u;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/r;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/r;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3356
    new-instance v0, Lcom/google/r/b/a/s;

    invoke-direct {v0}, Lcom/google/r/b/a/s;-><init>()V

    sput-object v0, Lcom/google/r/b/a/r;->PARSER:Lcom/google/n/ax;

    .line 3550
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/r;->h:Lcom/google/n/aw;

    .line 4060
    new-instance v0, Lcom/google/r/b/a/r;

    invoke-direct {v0}, Lcom/google/r/b/a/r;-><init>()V

    sput-object v0, Lcom/google/r/b/a/r;->e:Lcom/google/r/b/a/r;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3284
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3500
    iput-byte v0, p0, Lcom/google/r/b/a/r;->f:B

    .line 3525
    iput v0, p0, Lcom/google/r/b/a/r;->g:I

    .line 3285
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/r;->b:Ljava/lang/Object;

    .line 3286
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    .line 3287
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    .line 3288
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x2

    .line 3294
    invoke-direct {p0}, Lcom/google/r/b/a/r;-><init>()V

    .line 3297
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 3300
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 3301
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 3302
    sparse-switch v1, :sswitch_data_0

    .line 3307
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 3309
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 3305
    goto :goto_0

    .line 3314
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 3315
    iget v5, p0, Lcom/google/r/b/a/r;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/r;->a:I

    .line 3316
    iput-object v1, p0, Lcom/google/r/b/a/r;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 3341
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 3342
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3347
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v8, :cond_1

    .line 3348
    iget-object v2, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    .line 3350
    :cond_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_2

    .line 3351
    iget-object v1, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    .line 3353
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/r;->au:Lcom/google/n/bn;

    throw v0

    .line 3320
    :sswitch_2
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_7

    .line 3321
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3323
    or-int/lit8 v1, v0, 0x4

    .line 3325
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 3326
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 3325
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 3327
    goto :goto_0

    .line 3330
    :sswitch_3
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_3

    .line 3331
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    .line 3333
    or-int/lit8 v0, v0, 0x2

    .line 3335
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 3336
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 3335
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 3343
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 3344
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 3345
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3347
    :cond_4
    and-int/lit8 v1, v0, 0x4

    if-ne v1, v8, :cond_5

    .line 3348
    iget-object v1, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    .line 3350
    :cond_5
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_6

    .line 3351
    iget-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    .line 3353
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/r;->au:Lcom/google/n/bn;

    .line 3354
    return-void

    .line 3347
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto/16 :goto_2

    .line 3343
    :catch_2
    move-exception v0

    goto :goto_4

    .line 3341
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_7
    move v1, v0

    goto :goto_3

    .line 3302
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3282
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3500
    iput-byte v0, p0, Lcom/google/r/b/a/r;->f:B

    .line 3525
    iput v0, p0, Lcom/google/r/b/a/r;->g:I

    .line 3283
    return-void
.end method

.method public static g()Lcom/google/r/b/a/r;
    .locals 1

    .prologue
    .line 4063
    sget-object v0, Lcom/google/r/b/a/r;->e:Lcom/google/r/b/a/r;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/t;
    .locals 1

    .prologue
    .line 3612
    new-instance v0, Lcom/google/r/b/a/t;

    invoke-direct {v0}, Lcom/google/r/b/a/t;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3368
    sget-object v0, Lcom/google/r/b/a/r;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3512
    invoke-virtual {p0}, Lcom/google/r/b/a/r;->c()I

    .line 3513
    iget v0, p0, Lcom/google/r/b/a/r;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 3514
    iget-object v0, p0, Lcom/google/r/b/a/r;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/r;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 3516
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 3517
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3516
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 3514
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 3519
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 3520
    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3519
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 3522
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/r;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3523
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3502
    iget-byte v1, p0, Lcom/google/r/b/a/r;->f:B

    .line 3503
    if-ne v1, v0, :cond_0

    .line 3507
    :goto_0
    return v0

    .line 3504
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3506
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/r;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3527
    iget v0, p0, Lcom/google/r/b/a/r;->g:I

    .line 3528
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3545
    :goto_0
    return v0

    .line 3531
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/r;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 3533
    iget-object v0, p0, Lcom/google/r/b/a/r;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/r;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 3535
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 3536
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    .line 3537
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3535
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 3533
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v2, v1

    .line 3539
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 3540
    const/4 v4, 0x4

    iget-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    .line 3541
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3539
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 3543
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/r;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 3544
    iput v0, p0, Lcom/google/r/b/a/r;->g:I

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3421
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    .line 3422
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 3423
    iget-object v0, p0, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 3424
    invoke-static {}, Lcom/google/r/b/a/ad;->d()Lcom/google/r/b/a/ad;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ad;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3426
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3276
    invoke-static {}, Lcom/google/r/b/a/r;->newBuilder()Lcom/google/r/b/a/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/t;->a(Lcom/google/r/b/a/r;)Lcom/google/r/b/a/t;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3276
    invoke-static {}, Lcom/google/r/b/a/r;->newBuilder()Lcom/google/r/b/a/t;

    move-result-object v0

    return-object v0
.end method
