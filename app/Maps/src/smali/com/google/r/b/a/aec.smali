.class public final Lcom/google/r/b/a/aec;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aef;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aec;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/r/b/a/aec;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:F

.field d:Z

.field public e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field public g:Lcom/google/n/ao;

.field public h:I

.field i:I

.field public j:Ljava/lang/Object;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18411
    new-instance v0, Lcom/google/r/b/a/aed;

    invoke-direct {v0}, Lcom/google/r/b/a/aed;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aec;->PARSER:Lcom/google/n/ax;

    .line 18764
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aec;->n:Lcom/google/n/aw;

    .line 19529
    new-instance v0, Lcom/google/r/b/a/aec;

    invoke-direct {v0}, Lcom/google/r/b/a/aec;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aec;->k:Lcom/google/r/b/a/aec;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 18309
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 18585
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    .line 18672
    iput-byte v3, p0, Lcom/google/r/b/a/aec;->l:B

    .line 18715
    iput v3, p0, Lcom/google/r/b/a/aec;->m:I

    .line 18310
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    .line 18311
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aec;->c:F

    .line 18312
    iput-boolean v2, p0, Lcom/google/r/b/a/aec;->d:Z

    .line 18313
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    .line 18314
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aec;->f:Ljava/lang/Object;

    .line 18315
    iget-object v0, p0, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 18316
    iput v2, p0, Lcom/google/r/b/a/aec;->h:I

    .line 18317
    iput v2, p0, Lcom/google/r/b/a/aec;->i:I

    .line 18318
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    .line 18319
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 18325
    invoke-direct {p0}, Lcom/google/r/b/a/aec;-><init>()V

    .line 18328
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 18331
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 18332
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 18333
    sparse-switch v4, :sswitch_data_0

    .line 18338
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 18340
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 18336
    goto :goto_0

    .line 18345
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 18346
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    .line 18348
    or-int/lit8 v1, v1, 0x1

    .line 18350
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 18351
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 18350
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 18399
    :catch_0
    move-exception v0

    .line 18400
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18405
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 18406
    iget-object v1, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    .line 18408
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aec;->au:Lcom/google/n/bn;

    throw v0

    .line 18355
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/aec;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/aec;->a:I

    .line 18356
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/aec;->d:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 18401
    :catch_1
    move-exception v0

    .line 18402
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 18403
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 18360
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 18361
    iget v5, p0, Lcom/google/r/b/a/aec;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/aec;->a:I

    .line 18362
    iput-object v4, p0, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    goto :goto_0

    .line 18366
    :sswitch_4
    iget-object v4, p0, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 18367
    iget v4, p0, Lcom/google/r/b/a/aec;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/aec;->a:I

    goto/16 :goto_0

    .line 18371
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 18372
    iget v5, p0, Lcom/google/r/b/a/aec;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/r/b/a/aec;->a:I

    .line 18373
    iput-object v4, p0, Lcom/google/r/b/a/aec;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 18377
    :sswitch_6
    iget v4, p0, Lcom/google/r/b/a/aec;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/aec;->a:I

    .line 18378
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/aec;->i:I

    goto/16 :goto_0

    .line 18382
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 18383
    iget v5, p0, Lcom/google/r/b/a/aec;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/r/b/a/aec;->a:I

    .line 18384
    iput-object v4, p0, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 18388
    :sswitch_8
    iget v4, p0, Lcom/google/r/b/a/aec;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/aec;->a:I

    .line 18389
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/aec;->c:F

    goto/16 :goto_0

    .line 18393
    :sswitch_9
    iget v4, p0, Lcom/google/r/b/a/aec;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/aec;->a:I

    .line 18394
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/aec;->h:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 18405
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 18406
    iget-object v0, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    .line 18408
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->au:Lcom/google/n/bn;

    .line 18409
    return-void

    .line 18333
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x45 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 18307
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 18585
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    .line 18672
    iput-byte v1, p0, Lcom/google/r/b/a/aec;->l:B

    .line 18715
    iput v1, p0, Lcom/google/r/b/a/aec;->m:I

    .line 18308
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aec;
    .locals 1

    .prologue
    .line 19532
    sget-object v0, Lcom/google/r/b/a/aec;->k:Lcom/google/r/b/a/aec;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aee;
    .locals 1

    .prologue
    .line 18826
    new-instance v0, Lcom/google/r/b/a/aee;

    invoke-direct {v0}, Lcom/google/r/b/a/aee;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18423
    sget-object v0, Lcom/google/r/b/a/aec;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 18684
    invoke-virtual {p0}, Lcom/google/r/b/a/aec;->c()I

    .line 18685
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 18686
    iget-object v0, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18685
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 18688
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 18689
    iget-boolean v0, p0, Lcom/google/r/b/a/aec;->d:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 18691
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 18692
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18694
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 18695
    iget-object v0, p0, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18697
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_4

    .line 18698
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/aec;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->f:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18700
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 18701
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/aec;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 18703
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 18704
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18706
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 18707
    iget v0, p0, Lcom/google/r/b/a/aec;->c:F

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(IF)V

    .line 18709
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 18710
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/r/b/a/aec;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 18712
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/aec;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 18713
    return-void

    .line 18692
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 18698
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 18704
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 18674
    iget-byte v1, p0, Lcom/google/r/b/a/aec;->l:B

    .line 18675
    if-ne v1, v0, :cond_0

    .line 18679
    :goto_0
    return v0

    .line 18676
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 18678
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aec;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 18717
    iget v0, p0, Lcom/google/r/b/a/aec;->m:I

    .line 18718
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 18759
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 18721
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 18722
    iget-object v0, p0, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    .line 18723
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 18721
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 18725
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    .line 18726
    iget-boolean v0, p0, Lcom/google/r/b/a/aec;->d:Z

    .line 18727
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 18729
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_3

    .line 18730
    const/4 v1, 0x3

    .line 18731
    iget-object v0, p0, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 18733
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 18734
    iget-object v0, p0, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    .line 18735
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 18737
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_5

    .line 18738
    const/4 v1, 0x5

    .line 18739
    iget-object v0, p0, Lcom/google/r/b/a/aec;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 18741
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 18742
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/aec;->i:I

    .line 18743
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v1, :cond_c

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 18745
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 18746
    const/4 v1, 0x7

    .line 18747
    iget-object v0, p0, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 18749
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_8

    .line 18750
    iget v0, p0, Lcom/google/r/b/a/aec;->c:F

    .line 18751
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 18753
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 18754
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/r/b/a/aec;->h:I

    .line 18755
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_e

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 18757
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/aec;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 18758
    iput v0, p0, Lcom/google/r/b/a/aec;->m:I

    goto/16 :goto_0

    .line 18731
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 18739
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 18743
    :cond_c
    const/16 v0, 0xa

    goto :goto_4

    .line 18747
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 18755
    :cond_e
    const/16 v0, 0xa

    goto :goto_6
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 18301
    invoke-static {}, Lcom/google/r/b/a/aec;->newBuilder()Lcom/google/r/b/a/aee;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aee;->a(Lcom/google/r/b/a/aec;)Lcom/google/r/b/a/aee;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 18301
    invoke-static {}, Lcom/google/r/b/a/aec;->newBuilder()Lcom/google/r/b/a/aee;

    move-result-object v0

    return-object v0
.end method
