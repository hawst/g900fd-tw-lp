.class public final Lcom/google/r/b/a/j;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/m;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/j;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/j;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 972
    new-instance v0, Lcom/google/r/b/a/k;

    invoke-direct {v0}, Lcom/google/r/b/a/k;-><init>()V

    sput-object v0, Lcom/google/r/b/a/j;->PARSER:Lcom/google/n/ax;

    .line 1137
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/j;->h:Lcom/google/n/aw;

    .line 1467
    new-instance v0, Lcom/google/r/b/a/j;

    invoke-direct {v0}, Lcom/google/r/b/a/j;-><init>()V

    sput-object v0, Lcom/google/r/b/a/j;->e:Lcom/google/r/b/a/j;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 915
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1087
    iput-byte v0, p0, Lcom/google/r/b/a/j;->f:B

    .line 1112
    iput v0, p0, Lcom/google/r/b/a/j;->g:I

    .line 916
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/j;->b:I

    .line 917
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    .line 918
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/j;->d:Ljava/lang/Object;

    .line 919
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 925
    invoke-direct {p0}, Lcom/google/r/b/a/j;-><init>()V

    .line 926
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 930
    const/4 v0, 0x0

    .line 931
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 932
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 933
    sparse-switch v3, :sswitch_data_0

    .line 938
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 940
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 936
    goto :goto_0

    .line 945
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/j;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/j;->a:I

    .line 946
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/j;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 963
    :catch_0
    move-exception v0

    .line 964
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 969
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/j;->au:Lcom/google/n/bn;

    throw v0

    .line 950
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 951
    iget v4, p0, Lcom/google/r/b/a/j;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/j;->a:I

    .line 952
    iput-object v3, p0, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 965
    :catch_1
    move-exception v0

    .line 966
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 967
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 956
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 957
    iget v4, p0, Lcom/google/r/b/a/j;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/j;->a:I

    .line 958
    iput-object v3, p0, Lcom/google/r/b/a/j;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 969
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/j;->au:Lcom/google/n/bn;

    .line 970
    return-void

    .line 933
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 913
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1087
    iput-byte v0, p0, Lcom/google/r/b/a/j;->f:B

    .line 1112
    iput v0, p0, Lcom/google/r/b/a/j;->g:I

    .line 914
    return-void
.end method

.method public static g()Lcom/google/r/b/a/j;
    .locals 1

    .prologue
    .line 1470
    sget-object v0, Lcom/google/r/b/a/j;->e:Lcom/google/r/b/a/j;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/l;
    .locals 1

    .prologue
    .line 1199
    new-instance v0, Lcom/google/r/b/a/l;

    invoke-direct {v0}, Lcom/google/r/b/a/l;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 984
    sget-object v0, Lcom/google/r/b/a/j;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1099
    invoke-virtual {p0}, Lcom/google/r/b/a/j;->c()I

    .line 1100
    iget v0, p0, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1101
    iget v0, p0, Lcom/google/r/b/a/j;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 1103
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1104
    iget-object v0, p0, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1106
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1107
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/j;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/j;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1109
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/j;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1110
    return-void

    .line 1104
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1107
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1089
    iget-byte v1, p0, Lcom/google/r/b/a/j;->f:B

    .line 1090
    if-ne v1, v0, :cond_0

    .line 1094
    :goto_0
    return v0

    .line 1091
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1093
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/j;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1114
    iget v0, p0, Lcom/google/r/b/a/j;->g:I

    .line 1115
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1132
    :goto_0
    return v0

    .line 1118
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 1119
    iget v0, p0, Lcom/google/r/b/a/j;->b:I

    .line 1120
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 1122
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 1124
    iget-object v0, p0, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1126
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 1127
    const/4 v3, 0x3

    .line 1128
    iget-object v0, p0, Lcom/google/r/b/a/j;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/j;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1130
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/j;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1131
    iput v0, p0, Lcom/google/r/b/a/j;->g:I

    goto :goto_0

    .line 1120
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    .line 1124
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 1128
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    .line 1016
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1017
    check-cast v0, Ljava/lang/String;

    .line 1025
    :goto_0
    return-object v0

    .line 1019
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1021
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1022
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1023
    iput-object v1, p0, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1025
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 907
    invoke-static {}, Lcom/google/r/b/a/j;->newBuilder()Lcom/google/r/b/a/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/l;->a(Lcom/google/r/b/a/j;)Lcom/google/r/b/a/l;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 907
    invoke-static {}, Lcom/google/r/b/a/j;->newBuilder()Lcom/google/r/b/a/l;

    move-result-object v0

    return-object v0
.end method
