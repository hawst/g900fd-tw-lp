.class public final Lcom/google/r/b/a/kk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/kl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ki;",
        "Lcom/google/r/b/a/kk;",
        ">;",
        "Lcom/google/r/b/a/kl;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 378
    sget-object v0, Lcom/google/r/b/a/ki;->f:Lcom/google/r/b/a/ki;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 457
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    .line 657
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/kk;->e:Lcom/google/n/ao;

    .line 379
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 370
    new-instance v2, Lcom/google/r/b/a/ki;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ki;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/kk;->a:I

    iget v4, p0, Lcom/google/r/b/a/kk;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/kk;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/kk;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    :goto_0
    iget-boolean v4, p0, Lcom/google/r/b/a/kk;->c:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/ki;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-boolean v4, p0, Lcom/google/r/b/a/kk;->d:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/ki;->d:Z

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v3, v2, Lcom/google/r/b/a/ki;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/kk;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/kk;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/ki;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 370
    check-cast p1, Lcom/google/r/b/a/ki;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/kk;->a(Lcom/google/r/b/a/ki;)Lcom/google/r/b/a/kk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ki;)Lcom/google/r/b/a/kk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 425
    invoke-static {}, Lcom/google/r/b/a/ki;->d()Lcom/google/r/b/a/ki;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 447
    :goto_0
    return-object p0

    .line 426
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 427
    iget-object v2, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 428
    iget-object v2, p1, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    .line 429
    iget v2, p0, Lcom/google/r/b/a/kk;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/kk;->a:I

    .line 436
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 437
    iget-boolean v2, p1, Lcom/google/r/b/a/ki;->c:Z

    iget v3, p0, Lcom/google/r/b/a/kk;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/kk;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/kk;->c:Z

    .line 439
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 440
    iget-boolean v2, p1, Lcom/google/r/b/a/ki;->d:Z

    iget v3, p0, Lcom/google/r/b/a/kk;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/kk;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/kk;->d:Z

    .line 442
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ki;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 443
    iget-object v0, p0, Lcom/google/r/b/a/kk;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/ki;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 444
    iget v0, p0, Lcom/google/r/b/a/kk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/kk;->a:I

    .line 446
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/ki;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 431
    :cond_5
    invoke-virtual {p0}, Lcom/google/r/b/a/kk;->c()V

    .line 432
    iget-object v2, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/ki;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 436
    goto :goto_2

    :cond_7
    move v2, v1

    .line 439
    goto :goto_3

    :cond_8
    move v0, v1

    .line 442
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 459
    iget v0, p0, Lcom/google/r/b/a/kk;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 460
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/kk;->b:Ljava/util/List;

    .line 463
    iget v0, p0, Lcom/google/r/b/a/kk;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/kk;->a:I

    .line 465
    :cond_0
    return-void
.end method
