.class public final Lcom/google/r/b/a/ain;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aio;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ail;",
        "Lcom/google/r/b/a/ain;",
        ">;",
        "Lcom/google/r/b/a/aio;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 971
    sget-object v0, Lcom/google/r/b/a/ail;->e:Lcom/google/r/b/a/ail;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1033
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/r/b/a/ain;->b:I

    .line 1069
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ain;->c:Ljava/lang/Object;

    .line 1145
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ain;->d:Ljava/lang/Object;

    .line 972
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 963
    new-instance v2, Lcom/google/r/b/a/ail;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ail;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ain;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/ain;->b:I

    iput v1, v2, Lcom/google/r/b/a/ail;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ain;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ail;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ain;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ail;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/ail;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 963
    check-cast p1, Lcom/google/r/b/a/ail;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ain;->a(Lcom/google/r/b/a/ail;)Lcom/google/r/b/a/ain;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ail;)Lcom/google/r/b/a/ain;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1009
    invoke-static {}, Lcom/google/r/b/a/ail;->d()Lcom/google/r/b/a/ail;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1024
    :goto_0
    return-object p0

    .line 1010
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1011
    iget v2, p1, Lcom/google/r/b/a/ail;->b:I

    invoke-static {v2}, Lcom/google/maps/g/c/d;->a(I)Lcom/google/maps/g/c/d;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/c/d;->a:Lcom/google/maps/g/c/d;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1010
    goto :goto_1

    .line 1011
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/ain;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ain;->a:I

    iget v2, v2, Lcom/google/maps/g/c/d;->i:I

    iput v2, p0, Lcom/google/r/b/a/ain;->b:I

    .line 1013
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1014
    iget v2, p0, Lcom/google/r/b/a/ain;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ain;->a:I

    .line 1015
    iget-object v2, p1, Lcom/google/r/b/a/ail;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ain;->c:Ljava/lang/Object;

    .line 1018
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_6

    .line 1019
    iget v0, p0, Lcom/google/r/b/a/ain;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ain;->a:I

    .line 1020
    iget-object v0, p1, Lcom/google/r/b/a/ail;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/ain;->d:Ljava/lang/Object;

    .line 1023
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/ail;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_7
    move v2, v1

    .line 1013
    goto :goto_2

    :cond_8
    move v0, v1

    .line 1018
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1028
    const/4 v0, 0x1

    return v0
.end method
