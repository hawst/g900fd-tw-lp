.class public final Lcom/google/r/b/a/aai;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aaj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aag;",
        "Lcom/google/r/b/a/aai;",
        ">;",
        "Lcom/google/r/b/a/aaj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 782
    sget-object v0, Lcom/google/r/b/a/aag;->g:Lcom/google/r/b/a/aag;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 926
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aai;->d:Lcom/google/n/ao;

    .line 985
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aai;->b:Lcom/google/n/ao;

    .line 1044
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aai;->e:Lcom/google/n/ao;

    .line 1103
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aai;->f:Lcom/google/n/ao;

    .line 783
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 774
    invoke-virtual {p0}, Lcom/google/r/b/a/aai;->c()Lcom/google/r/b/a/aag;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 774
    check-cast p1, Lcom/google/r/b/a/aag;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aai;->a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 840
    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 861
    :goto_0
    return-object p0

    .line 841
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 842
    iget v2, p1, Lcom/google/r/b/a/aag;->b:I

    iget v3, p0, Lcom/google/r/b/a/aai;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aai;->a:I

    iput v2, p0, Lcom/google/r/b/a/aai;->c:I

    .line 844
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 845
    iget-object v2, p0, Lcom/google/r/b/a/aai;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 846
    iget v2, p0, Lcom/google/r/b/a/aai;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/aai;->a:I

    .line 848
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 849
    iget-object v2, p0, Lcom/google/r/b/a/aai;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 850
    iget v2, p0, Lcom/google/r/b/a/aai;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/aai;->a:I

    .line 852
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 853
    iget-object v2, p0, Lcom/google/r/b/a/aai;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 854
    iget v2, p0, Lcom/google/r/b/a/aai;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/aai;->a:I

    .line 856
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 857
    iget-object v0, p0, Lcom/google/r/b/a/aai;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 858
    iget v0, p0, Lcom/google/r/b/a/aai;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/aai;->a:I

    .line 860
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/aag;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 841
    goto :goto_1

    :cond_7
    move v2, v1

    .line 844
    goto :goto_2

    :cond_8
    move v2, v1

    .line 848
    goto :goto_3

    :cond_9
    move v2, v1

    .line 852
    goto :goto_4

    :cond_a
    move v0, v1

    .line 856
    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 865
    iget v0, p0, Lcom/google/r/b/a/aai;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 866
    iget-object v0, p0, Lcom/google/r/b/a/aai;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/abe;->d()Lcom/google/r/b/a/abe;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abe;

    invoke-virtual {v0}, Lcom/google/r/b/a/abe;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 889
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 865
    goto :goto_0

    .line 871
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aai;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 872
    iget-object v0, p0, Lcom/google/r/b/a/aai;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/abe;->d()Lcom/google/r/b/a/abe;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abe;

    invoke-virtual {v0}, Lcom/google/r/b/a/abe;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 874
    goto :goto_1

    :cond_2
    move v0, v1

    .line 871
    goto :goto_2

    .line 877
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aai;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 878
    iget-object v0, p0, Lcom/google/r/b/a/aai;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aao;->d()Lcom/google/r/b/a/aao;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aao;

    invoke-virtual {v0}, Lcom/google/r/b/a/aao;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 880
    goto :goto_1

    :cond_4
    move v0, v1

    .line 877
    goto :goto_3

    .line 883
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aai;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    .line 884
    iget-object v0, p0, Lcom/google/r/b/a/aai;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aaw;->d()Lcom/google/r/b/a/aaw;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aaw;

    invoke-virtual {v0}, Lcom/google/r/b/a/aaw;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 886
    goto :goto_1

    :cond_6
    move v0, v1

    .line 883
    goto :goto_4

    :cond_7
    move v0, v2

    .line 889
    goto :goto_1
.end method

.method public final c()Lcom/google/r/b/a/aag;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 804
    new-instance v2, Lcom/google/r/b/a/aag;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aag;-><init>(Lcom/google/n/v;)V

    .line 805
    iget v3, p0, Lcom/google/r/b/a/aai;->a:I

    .line 807
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 810
    :goto_0
    iget v4, p0, Lcom/google/r/b/a/aai;->c:I

    iput v4, v2, Lcom/google/r/b/a/aag;->b:I

    .line 811
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 812
    or-int/lit8 v0, v0, 0x2

    .line 814
    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aai;->d:Lcom/google/n/ao;

    .line 815
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aai;->d:Lcom/google/n/ao;

    .line 816
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 814
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 817
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 818
    or-int/lit8 v0, v0, 0x4

    .line 820
    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aai;->b:Lcom/google/n/ao;

    .line 821
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aai;->b:Lcom/google/n/ao;

    .line 822
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 820
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 823
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 824
    or-int/lit8 v0, v0, 0x8

    .line 826
    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aai;->e:Lcom/google/n/ao;

    .line 827
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aai;->e:Lcom/google/n/ao;

    .line 828
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 826
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 829
    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    .line 830
    or-int/lit8 v0, v0, 0x10

    .line 832
    :cond_3
    iget-object v3, v2, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/aai;->f:Lcom/google/n/ao;

    .line 833
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/aai;->f:Lcom/google/n/ao;

    .line 834
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 832
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 835
    iput v0, v2, Lcom/google/r/b/a/aag;->a:I

    .line 836
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method
