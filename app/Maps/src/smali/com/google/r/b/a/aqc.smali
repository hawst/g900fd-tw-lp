.class public final Lcom/google/r/b/a/aqc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aqf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aqc;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/aqc;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/google/r/b/a/aqd;

    invoke-direct {v0}, Lcom/google/r/b/a/aqd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aqc;->PARSER:Lcom/google/n/ax;

    .line 236
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aqc;->g:Lcom/google/n/aw;

    .line 588
    new-instance v0, Lcom/google/r/b/a/aqc;

    invoke-direct {v0}, Lcom/google/r/b/a/aqc;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aqc;->d:Lcom/google/r/b/a/aqc;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    .line 181
    iput-byte v2, p0, Lcom/google/r/b/a/aqc;->e:B

    .line 215
    iput v2, p0, Lcom/google/r/b/a/aqc;->f:I

    .line 50
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    .line 52
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Lcom/google/r/b/a/aqc;-><init>()V

    .line 61
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 64
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 65
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 66
    sparse-switch v4, :sswitch_data_0

    .line 71
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 73
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 69
    goto :goto_0

    .line 78
    :sswitch_1
    iget-object v4, p0, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 79
    iget v4, p0, Lcom/google/r/b/a/aqc;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/aqc;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 94
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 95
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 101
    iget-object v1, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    .line 103
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aqc;->au:Lcom/google/n/bn;

    throw v0

    .line 83
    :sswitch_2
    and-int/lit8 v4, v0, 0x2

    if-eq v4, v7, :cond_2

    .line 84
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    .line 86
    or-int/lit8 v0, v0, 0x2

    .line 88
    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 88
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 96
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 97
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 98
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 100
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_4

    .line 101
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    .line 103
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqc;->au:Lcom/google/n/bn;

    .line 104
    return-void

    .line 100
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 47
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    .line 181
    iput-byte v1, p0, Lcom/google/r/b/a/aqc;->e:B

    .line 215
    iput v1, p0, Lcom/google/r/b/a/aqc;->f:I

    .line 48
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aqc;
    .locals 1

    .prologue
    .line 591
    sget-object v0, Lcom/google/r/b/a/aqc;->d:Lcom/google/r/b/a/aqc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aqe;
    .locals 1

    .prologue
    .line 298
    new-instance v0, Lcom/google/r/b/a/aqe;

    invoke-direct {v0}, Lcom/google/r/b/a/aqe;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aqc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    sget-object v0, Lcom/google/r/b/a/aqc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 205
    invoke-virtual {p0}, Lcom/google/r/b/a/aqc;->c()I

    .line 206
    iget v0, p0, Lcom/google/r/b/a/aqc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 209
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 210
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 209
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 213
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 183
    iget-byte v0, p0, Lcom/google/r/b/a/aqc;->e:B

    .line 184
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 200
    :cond_0
    :goto_0
    return v2

    .line 185
    :cond_1
    if-eqz v0, :cond_0

    .line 187
    iget v0, p0, Lcom/google/r/b/a/aqc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 188
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hn;->g()Lcom/google/r/b/a/hn;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hn;

    invoke-virtual {v0}, Lcom/google/r/b/a/hn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 189
    iput-byte v2, p0, Lcom/google/r/b/a/aqc;->e:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 187
    goto :goto_1

    :cond_3
    move v1, v2

    .line 193
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 194
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/be;->d()Lcom/google/r/b/a/be;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 195
    iput-byte v2, p0, Lcom/google/r/b/a/aqc;->e:B

    goto :goto_0

    .line 193
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 199
    :cond_5
    iput-byte v3, p0, Lcom/google/r/b/a/aqc;->e:B

    move v2, v3

    .line 200
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 217
    iget v0, p0, Lcom/google/r/b/a/aqc;->f:I

    .line 218
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 231
    :goto_0
    return v0

    .line 221
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aqc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 222
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    .line 223
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 225
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 226
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    .line 227
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 225
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aqc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 230
    iput v0, p0, Lcom/google/r/b/a/aqc;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/r/b/a/aqc;->newBuilder()Lcom/google/r/b/a/aqe;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aqe;->a(Lcom/google/r/b/a/aqc;)Lcom/google/r/b/a/aqe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/google/r/b/a/aqc;->newBuilder()Lcom/google/r/b/a/aqe;

    move-result-object v0

    return-object v0
.end method
