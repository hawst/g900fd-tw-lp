.class public final Lcom/google/r/b/a/ce;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/cj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ce;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/ce;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1737
    new-instance v0, Lcom/google/r/b/a/cf;

    invoke-direct {v0}, Lcom/google/r/b/a/cf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ce;->PARSER:Lcom/google/n/ax;

    .line 1755
    new-instance v0, Lcom/google/r/b/a/cg;

    invoke-direct {v0}, Lcom/google/r/b/a/cg;-><init>()V

    .line 1786
    new-instance v0, Lcom/google/r/b/a/ch;

    invoke-direct {v0}, Lcom/google/r/b/a/ch;-><init>()V

    .line 1867
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ce;->f:Lcom/google/n/aw;

    .line 2162
    new-instance v0, Lcom/google/r/b/a/ce;

    invoke-direct {v0}, Lcom/google/r/b/a/ce;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ce;->c:Lcom/google/r/b/a/ce;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1626
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1814
    iput-byte v0, p0, Lcom/google/r/b/a/ce;->d:B

    .line 1836
    iput v0, p0, Lcom/google/r/b/a/ce;->e:I

    .line 1627
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    .line 1628
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    .line 1629
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const v9, 0x7fffffff

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v8, 0x2

    const/4 v4, 0x1

    .line 1635
    invoke-direct {p0}, Lcom/google/r/b/a/ce;-><init>()V

    .line 1638
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 1641
    :cond_0
    :goto_0
    if-nez v3, :cond_c

    .line 1642
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 1643
    sparse-switch v1, :sswitch_data_0

    .line 1648
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 1650
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 1646
    goto :goto_0

    .line 1655
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    .line 1656
    invoke-static {v6}, Lcom/google/r/b/a/cc;->a(I)Lcom/google/r/b/a/cc;

    move-result-object v1

    .line 1657
    if-nez v1, :cond_3

    .line 1658
    const/4 v1, 0x1

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 1722
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 1723
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1728
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_1

    .line 1729
    iget-object v2, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    .line 1731
    :cond_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v8, :cond_2

    .line 1732
    iget-object v1, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    .line 1734
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ce;->au:Lcom/google/n/bn;

    throw v0

    .line 1660
    :cond_3
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v4, :cond_11

    .line 1661
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1662
    or-int/lit8 v1, v0, 0x1

    .line 1664
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 1666
    goto :goto_0

    .line 1669
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 1670
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v6

    move v1, v0

    .line 1671
    :goto_4
    :try_start_5
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_4

    move v0, v2

    :goto_5
    if-lez v0, :cond_7

    .line 1672
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 1673
    invoke-static {v0}, Lcom/google/r/b/a/cc;->a(I)Lcom/google/r/b/a/cc;

    move-result-object v7

    .line 1674
    if-nez v7, :cond_5

    .line 1675
    const/4 v7, 0x1

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_4

    .line 1722
    :catch_1
    move-exception v0

    goto :goto_1

    .line 1671
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_5

    .line 1677
    :cond_5
    and-int/lit8 v7, v1, 0x1

    if-eq v7, v4, :cond_6

    .line 1678
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    .line 1679
    or-int/lit8 v1, v1, 0x1

    .line 1681
    :cond_6
    iget-object v7, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 1724
    :catch_2
    move-exception v0

    .line 1725
    :goto_6
    :try_start_6
    new-instance v2, Lcom/google/n/ak;

    .line 1726
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1684
    :cond_7
    :try_start_7
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 1685
    goto/16 :goto_0

    .line 1688
    :sswitch_3
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v6

    .line 1689
    invoke-static {v6}, Lcom/google/r/b/a/dd;->a(I)Lcom/google/r/b/a/dd;

    move-result-object v1

    .line 1690
    if-nez v1, :cond_8

    .line 1691
    const/4 v1, 0x2

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 1724
    :catch_3
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_6

    .line 1693
    :cond_8
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v8, :cond_10

    .line 1694
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1695
    or-int/lit8 v1, v0, 0x2

    .line 1697
    :goto_7
    :try_start_9
    iget-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 1699
    goto/16 :goto_0

    .line 1702
    :sswitch_4
    :try_start_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 1703
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 1704
    :goto_8
    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_9

    move v1, v2

    :goto_9
    if-lez v1, :cond_b

    .line 1705
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v7

    .line 1706
    invoke-static {v7}, Lcom/google/r/b/a/dd;->a(I)Lcom/google/r/b/a/dd;

    move-result-object v1

    .line 1707
    if-nez v1, :cond_a

    .line 1708
    const/4 v1, 0x2

    invoke-virtual {v5, v1, v7}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_8

    .line 1728
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_2

    .line 1704
    :cond_9
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_9

    .line 1710
    :cond_a
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v8, :cond_f

    .line 1711
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1712
    or-int/lit8 v1, v0, 0x2

    .line 1714
    :goto_a
    :try_start_b
    iget-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v1

    .line 1716
    goto :goto_8

    .line 1717
    :cond_b
    :try_start_c
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 1728
    :cond_c
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v4, :cond_d

    .line 1729
    iget-object v1, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    .line 1731
    :cond_d
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_e

    .line 1732
    iget-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    .line 1734
    :cond_e
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ce;->au:Lcom/google/n/bn;

    .line 1735
    return-void

    :cond_f
    move v1, v0

    goto :goto_a

    :cond_10
    move v1, v0

    goto :goto_7

    :cond_11
    move v1, v0

    goto/16 :goto_3

    .line 1643
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x12 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1624
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1814
    iput-byte v0, p0, Lcom/google/r/b/a/ce;->d:B

    .line 1836
    iput v0, p0, Lcom/google/r/b/a/ce;->e:I

    .line 1625
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ce;
    .locals 1

    .prologue
    .line 2165
    sget-object v0, Lcom/google/r/b/a/ce;->c:Lcom/google/r/b/a/ce;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ci;
    .locals 1

    .prologue
    .line 1929
    new-instance v0, Lcom/google/r/b/a/ci;

    invoke-direct {v0}, Lcom/google/r/b/a/ci;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ce;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1749
    sget-object v0, Lcom/google/r/b/a/ce;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1826
    invoke-virtual {p0}, Lcom/google/r/b/a/ce;->c()I

    move v1, v2

    .line 1827
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1828
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 1827
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1830
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1831
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 1830
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1833
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/ce;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1834
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1816
    iget-byte v1, p0, Lcom/google/r/b/a/ce;->d:B

    .line 1817
    if-ne v1, v0, :cond_0

    .line 1821
    :goto_0
    return v0

    .line 1818
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1820
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ce;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 1838
    iget v0, p0, Lcom/google/r/b/a/ce;->e:I

    .line 1839
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1862
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1844
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1845
    iget-object v0, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    .line 1846
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    .line 1844
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v4

    .line 1846
    goto :goto_2

    .line 1848
    :cond_2
    add-int/lit8 v0, v3, 0x0

    .line 1849
    iget-object v1, p0, Lcom/google/r/b/a/ce;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int v3, v0, v1

    move v1, v2

    .line 1853
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1854
    iget-object v0, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    .line 1855
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    .line 1853
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v4

    .line 1855
    goto :goto_4

    .line 1857
    :cond_4
    add-int v0, v3, v1

    .line 1858
    iget-object v1, p0, Lcom/google/r/b/a/ce;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1860
    iget-object v1, p0, Lcom/google/r/b/a/ce;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1861
    iput v0, p0, Lcom/google/r/b/a/ce;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1618
    invoke-static {}, Lcom/google/r/b/a/ce;->newBuilder()Lcom/google/r/b/a/ci;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ci;->a(Lcom/google/r/b/a/ce;)Lcom/google/r/b/a/ci;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1618
    invoke-static {}, Lcom/google/r/b/a/ce;->newBuilder()Lcom/google/r/b/a/ci;

    move-result-object v0

    return-object v0
.end method
