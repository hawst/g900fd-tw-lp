.class public final Lcom/google/r/b/a/aco;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/acp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/acm;",
        "Lcom/google/r/b/a/aco;",
        ">;",
        "Lcom/google/r/b/a/acp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 14067
    sget-object v0, Lcom/google/r/b/a/acm;->d:Lcom/google/r/b/a/acm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 14120
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aco;->b:Ljava/lang/Object;

    .line 14196
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aco;->c:Ljava/lang/Object;

    .line 14068
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 14059
    new-instance v2, Lcom/google/r/b/a/acm;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/acm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aco;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/aco;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/aco;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/acm;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 14059
    check-cast p1, Lcom/google/r/b/a/acm;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aco;->a(Lcom/google/r/b/a/acm;)Lcom/google/r/b/a/aco;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/acm;)Lcom/google/r/b/a/aco;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 14099
    invoke-static {}, Lcom/google/r/b/a/acm;->h()Lcom/google/r/b/a/acm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 14111
    :goto_0
    return-object p0

    .line 14100
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 14101
    iget v2, p0, Lcom/google/r/b/a/aco;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/aco;->a:I

    .line 14102
    iget-object v2, p1, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aco;->b:Ljava/lang/Object;

    .line 14105
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 14106
    iget v0, p0, Lcom/google/r/b/a/aco;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/aco;->a:I

    .line 14107
    iget-object v0, p1, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/aco;->c:Ljava/lang/Object;

    .line 14110
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/acm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 14100
    goto :goto_1

    :cond_4
    move v0, v1

    .line 14105
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 14115
    const/4 v0, 0x1

    return v0
.end method
