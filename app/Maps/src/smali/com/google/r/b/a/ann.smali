.class public final Lcom/google/r/b/a/ann;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/anq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ann;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/ann;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/ao;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/google/r/b/a/ano;

    invoke-direct {v0}, Lcom/google/r/b/a/ano;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ann;->PARSER:Lcom/google/n/ax;

    .line 359
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ann;->j:Lcom/google/n/aw;

    .line 1029
    new-instance v0, Lcom/google/r/b/a/ann;

    invoke-direct {v0}, Lcom/google/r/b/a/ann;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ann;->g:Lcom/google/r/b/a/ann;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 160
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ann;->c:Lcom/google/n/ao;

    .line 262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    .line 277
    iput-byte v2, p0, Lcom/google/r/b/a/ann;->h:B

    .line 326
    iput v2, p0, Lcom/google/r/b/a/ann;->i:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ann;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/r/b/a/ann;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    .line 22
    iget-object v0, p0, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/r/b/a/ann;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 37
    sparse-switch v1, :sswitch_data_0

    .line 42
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 44
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v7, :cond_7

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 52
    or-int/lit8 v1, v0, 0x4

    .line 54
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 54
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 56
    goto :goto_0

    .line 59
    :sswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 60
    iget v1, p0, Lcom/google/r/b/a/ann;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/ann;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 87
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v7, :cond_1

    .line 93
    iget-object v2, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    .line 95
    :cond_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v8, :cond_2

    .line 96
    iget-object v1, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    .line 98
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ann;->au:Lcom/google/n/bn;

    throw v0

    .line 64
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 65
    iget v5, p0, Lcom/google/r/b/a/ann;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/ann;->a:I

    .line 66
    iput-object v1, p0, Lcom/google/r/b/a/ann;->b:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 88
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 89
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 90
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 70
    :sswitch_4
    :try_start_6
    iget-object v1, p0, Lcom/google/r/b/a/ann;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 71
    iget v1, p0, Lcom/google/r/b/a/ann;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/ann;->a:I

    goto/16 :goto_0

    .line 92
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_3

    .line 75
    :sswitch_5
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v8, :cond_3

    .line 76
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    .line 78
    or-int/lit8 v0, v0, 0x8

    .line 80
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 81
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 80
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 92
    :cond_4
    and-int/lit8 v1, v0, 0x4

    if-ne v1, v7, :cond_5

    .line 93
    iget-object v1, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    .line 95
    :cond_5
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_6

    .line 96
    iget-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    .line 98
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ann;->au:Lcom/google/n/bn;

    .line 99
    return-void

    .line 88
    :catch_2
    move-exception v0

    goto :goto_4

    .line 86
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_7
    move v1, v0

    goto/16 :goto_1

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x7a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 160
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ann;->c:Lcom/google/n/ao;

    .line 262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    .line 277
    iput-byte v1, p0, Lcom/google/r/b/a/ann;->h:B

    .line 326
    iput v1, p0, Lcom/google/r/b/a/ann;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ann;
    .locals 1

    .prologue
    .line 1032
    sget-object v0, Lcom/google/r/b/a/ann;->g:Lcom/google/r/b/a/ann;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/anp;
    .locals 1

    .prologue
    .line 421
    new-instance v0, Lcom/google/r/b/a/anp;

    invoke-direct {v0}, Lcom/google/r/b/a/anp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ann;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lcom/google/r/b/a/ann;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 307
    invoke-virtual {p0}, Lcom/google/r/b/a/ann;->c()I

    move v1, v2

    .line 308
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 308
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 311
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 312
    iget-object v0, p0, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 314
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 315
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/ann;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ann;->b:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 317
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 318
    iget-object v0, p0, Lcom/google/r/b/a/ann;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 320
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 321
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 320
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 315
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 323
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/ann;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 324
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 279
    iget-byte v0, p0, Lcom/google/r/b/a/ann;->h:B

    .line 280
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 302
    :cond_0
    :goto_0
    return v2

    .line 281
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 283
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 284
    iget-object v0, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/anr;->d()Lcom/google/r/b/a/anr;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anr;

    invoke-virtual {v0}, Lcom/google/r/b/a/anr;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 285
    iput-byte v2, p0, Lcom/google/r/b/a/ann;->h:B

    goto :goto_0

    .line 283
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 289
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 290
    iget-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/be;->d()Lcom/google/r/b/a/be;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 291
    iput-byte v2, p0, Lcom/google/r/b/a/ann;->h:B

    goto :goto_0

    .line 289
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 295
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 296
    iget-object v0, p0, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/anr;->d()Lcom/google/r/b/a/anr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anr;

    invoke-virtual {v0}, Lcom/google/r/b/a/anr;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 297
    iput-byte v2, p0, Lcom/google/r/b/a/ann;->h:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 295
    goto :goto_3

    .line 301
    :cond_7
    iput-byte v3, p0, Lcom/google/r/b/a/ann;->h:B

    move v2, v3

    .line 302
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 328
    iget v0, p0, Lcom/google/r/b/a/ann;->i:I

    .line 329
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 354
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 332
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/google/r/b/a/ann;->d:Ljava/util/List;

    .line 334
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 332
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 336
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 337
    iget-object v0, p0, Lcom/google/r/b/a/ann;->f:Lcom/google/n/ao;

    .line 338
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 340
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 341
    const/4 v1, 0x3

    .line 342
    iget-object v0, p0, Lcom/google/r/b/a/ann;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ann;->b:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 344
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ann;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 345
    iget-object v0, p0, Lcom/google/r/b/a/ann;->c:Lcom/google/n/ao;

    .line 346
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_4
    move v1, v2

    .line 348
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 349
    const/16 v4, 0xf

    iget-object v0, p0, Lcom/google/r/b/a/ann;->e:Ljava/util/List;

    .line 350
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 348
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 342
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 352
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/ann;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 353
    iput v0, p0, Lcom/google/r/b/a/ann;->i:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ann;->newBuilder()Lcom/google/r/b/a/anp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/anp;->a(Lcom/google/r/b/a/ann;)Lcom/google/r/b/a/anp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ann;->newBuilder()Lcom/google/r/b/a/anp;

    move-result-object v0

    return-object v0
.end method
