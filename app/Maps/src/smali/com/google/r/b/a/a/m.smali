.class public final Lcom/google/r/b/a/a/m;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/a/k;",
        "Lcom/google/r/b/a/a/m;",
        ">;",
        "Lcom/google/r/b/a/a/n;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1306
    sget-object v0, Lcom/google/r/b/a/a/k;->e:Lcom/google/r/b/a/a/k;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1364
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/a/m;->b:I

    .line 1400
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/a/m;->c:I

    .line 1307
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1298
    new-instance v2, Lcom/google/r/b/a/a/k;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/a/k;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/a/m;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/a/m;->b:I

    iput v1, v2, Lcom/google/r/b/a/a/k;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/a/m;->c:I

    iput v1, v2, Lcom/google/r/b/a/a/k;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/r/b/a/a/m;->d:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/k;->d:J

    iput v0, v2, Lcom/google/r/b/a/a/k;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1298
    check-cast p1, Lcom/google/r/b/a/a/k;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/a/m;->a(Lcom/google/r/b/a/a/k;)Lcom/google/r/b/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/a/k;)Lcom/google/r/b/a/a/m;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1344
    invoke-static {}, Lcom/google/r/b/a/a/k;->d()Lcom/google/r/b/a/a/k;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1355
    :goto_0
    return-object p0

    .line 1345
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1346
    iget v2, p1, Lcom/google/r/b/a/a/k;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/fr;->a(I)Lcom/google/maps/g/a/fr;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/fr;->a:Lcom/google/maps/g/a/fr;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1345
    goto :goto_1

    .line 1346
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/a/m;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/a/m;->a:I

    iget v2, v2, Lcom/google/maps/g/a/fr;->c:I

    iput v2, p0, Lcom/google/r/b/a/a/m;->b:I

    .line 1348
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 1349
    iget v2, p1, Lcom/google/r/b/a/a/k;->c:I

    invoke-static {v2}, Lcom/google/maps/g/a/fu;->a(I)Lcom/google/maps/g/a/fu;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/a/fu;->a:Lcom/google/maps/g/a/fu;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 1348
    goto :goto_2

    .line 1349
    :cond_7
    iget v3, p0, Lcom/google/r/b/a/a/m;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/a/m;->a:I

    iget v2, v2, Lcom/google/maps/g/a/fu;->c:I

    iput v2, p0, Lcom/google/r/b/a/a/m;->c:I

    .line 1351
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    :goto_3
    if-eqz v0, :cond_9

    .line 1352
    iget-wide v0, p1, Lcom/google/r/b/a/a/k;->d:J

    iget v2, p0, Lcom/google/r/b/a/a/m;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/a/m;->a:I

    iput-wide v0, p0, Lcom/google/r/b/a/a/m;->d:J

    .line 1354
    :cond_9
    iget-object v0, p1, Lcom/google/r/b/a/a/k;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_a
    move v0, v1

    .line 1351
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1359
    const/4 v0, 0x1

    return v0
.end method
