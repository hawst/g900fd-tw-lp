.class public final Lcom/google/r/b/a/acj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ack;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ach;",
        "Lcom/google/r/b/a/acj;",
        ">;",
        "Lcom/google/r/b/a/ack;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/maps/a/e;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/acu;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/r/b/a/aby;

.field private g:Lcom/google/maps/g/wb;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16333
    sget-object v0, Lcom/google/r/b/a/ach;->h:Lcom/google/r/b/a/ach;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 16430
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/acj;->b:Ljava/lang/Object;

    .line 16506
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/acj;->c:Ljava/lang/Object;

    .line 16582
    iput-object v1, p0, Lcom/google/r/b/a/acj;->d:Lcom/google/maps/a/e;

    .line 16644
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    .line 16768
    iput-object v1, p0, Lcom/google/r/b/a/acj;->f:Lcom/google/r/b/a/aby;

    .line 16829
    iput-object v1, p0, Lcom/google/r/b/a/acj;->g:Lcom/google/maps/g/wb;

    .line 16334
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 16325
    new-instance v2, Lcom/google/r/b/a/ach;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ach;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/acj;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/acj;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ach;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/acj;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ach;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/acj;->d:Lcom/google/maps/a/e;

    iput-object v1, v2, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    iget v1, p0, Lcom/google/r/b/a/acj;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/acj;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/r/b/a/acj;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/acj;->f:Lcom/google/r/b/a/aby;

    iput-object v1, v2, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/acj;->g:Lcom/google/maps/g/wb;

    iput-object v1, v2, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    iput v0, v2, Lcom/google/r/b/a/ach;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 16325
    check-cast p1, Lcom/google/r/b/a/ach;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/acj;->a(Lcom/google/r/b/a/ach;)Lcom/google/r/b/a/acj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ach;)Lcom/google/r/b/a/acj;
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 16390
    invoke-static {}, Lcom/google/r/b/a/ach;->d()Lcom/google/r/b/a/ach;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 16421
    :goto_0
    return-object p0

    .line 16391
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 16392
    iget v0, p0, Lcom/google/r/b/a/acj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/acj;->a:I

    .line 16393
    iget-object v0, p1, Lcom/google/r/b/a/ach;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/acj;->b:Ljava/lang/Object;

    .line 16396
    :cond_1
    iget v0, p1, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 16397
    iget v0, p0, Lcom/google/r/b/a/acj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/acj;->a:I

    .line 16398
    iget-object v0, p1, Lcom/google/r/b/a/ach;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/acj;->c:Ljava/lang/Object;

    .line 16401
    :cond_2
    iget v0, p1, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_9

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 16402
    iget-object v0, p1, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v0

    :goto_4
    iget v3, p0, Lcom/google/r/b/a/acj;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v4, :cond_b

    iget-object v3, p0, Lcom/google/r/b/a/acj;->d:Lcom/google/maps/a/e;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/r/b/a/acj;->d:Lcom/google/maps/a/e;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v4

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/google/r/b/a/acj;->d:Lcom/google/maps/a/e;

    invoke-static {v3}, Lcom/google/maps/a/e;->a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/a/g;->a(Lcom/google/maps/a/e;)Lcom/google/maps/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/a/g;->c()Lcom/google/maps/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acj;->d:Lcom/google/maps/a/e;

    :goto_5
    iget v0, p0, Lcom/google/r/b/a/acj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/acj;->a:I

    .line 16404
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 16405
    iget-object v0, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 16406
    iget-object v0, p1, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    .line 16407
    iget v0, p0, Lcom/google/r/b/a/acj;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/r/b/a/acj;->a:I

    .line 16414
    :cond_4
    :goto_6
    iget v0, p1, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_e

    move v0, v1

    :goto_7
    if-eqz v0, :cond_5

    .line 16415
    iget-object v0, p1, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    if-nez v0, :cond_f

    invoke-static {}, Lcom/google/r/b/a/aby;->d()Lcom/google/r/b/a/aby;

    move-result-object v0

    :goto_8
    iget v3, p0, Lcom/google/r/b/a/acj;->a:I

    and-int/lit8 v3, v3, 0x10

    if-ne v3, v6, :cond_10

    iget-object v3, p0, Lcom/google/r/b/a/acj;->f:Lcom/google/r/b/a/aby;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/r/b/a/acj;->f:Lcom/google/r/b/a/aby;

    invoke-static {}, Lcom/google/r/b/a/aby;->d()Lcom/google/r/b/a/aby;

    move-result-object v4

    if-eq v3, v4, :cond_10

    iget-object v3, p0, Lcom/google/r/b/a/acj;->f:Lcom/google/r/b/a/aby;

    invoke-static {v3}, Lcom/google/r/b/a/aby;->a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/aca;->a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/aca;->c()Lcom/google/r/b/a/aby;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acj;->f:Lcom/google/r/b/a/aby;

    :goto_9
    iget v0, p0, Lcom/google/r/b/a/acj;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/acj;->a:I

    .line 16417
    :cond_5
    iget v0, p1, Lcom/google/r/b/a/ach;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_11

    move v0, v1

    :goto_a
    if-eqz v0, :cond_6

    .line 16418
    iget-object v0, p1, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    if-nez v0, :cond_12

    invoke-static {}, Lcom/google/maps/g/wb;->j()Lcom/google/maps/g/wb;

    move-result-object v0

    :goto_b
    iget v1, p0, Lcom/google/r/b/a/acj;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_13

    iget-object v1, p0, Lcom/google/r/b/a/acj;->g:Lcom/google/maps/g/wb;

    if-eqz v1, :cond_13

    iget-object v1, p0, Lcom/google/r/b/a/acj;->g:Lcom/google/maps/g/wb;

    invoke-static {}, Lcom/google/maps/g/wb;->j()Lcom/google/maps/g/wb;

    move-result-object v2

    if-eq v1, v2, :cond_13

    iget-object v1, p0, Lcom/google/r/b/a/acj;->g:Lcom/google/maps/g/wb;

    invoke-static {v1}, Lcom/google/maps/g/wb;->a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/wd;->a(Lcom/google/maps/g/wb;)Lcom/google/maps/g/wd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/wd;->c()Lcom/google/maps/g/wb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acj;->g:Lcom/google/maps/g/wb;

    :goto_c
    iget v0, p0, Lcom/google/r/b/a/acj;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/acj;->a:I

    .line 16420
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/ach;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 16391
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 16396
    goto/16 :goto_2

    :cond_9
    move v0, v2

    .line 16401
    goto/16 :goto_3

    .line 16402
    :cond_a
    iget-object v0, p1, Lcom/google/r/b/a/ach;->d:Lcom/google/maps/a/e;

    goto/16 :goto_4

    :cond_b
    iput-object v0, p0, Lcom/google/r/b/a/acj;->d:Lcom/google/maps/a/e;

    goto/16 :goto_5

    .line 16409
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/acj;->a:I

    and-int/lit8 v0, v0, 0x8

    if-eq v0, v5, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/acj;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/acj;->a:I

    .line 16410
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/acj;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/ach;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_e
    move v0, v2

    .line 16414
    goto/16 :goto_7

    .line 16415
    :cond_f
    iget-object v0, p1, Lcom/google/r/b/a/ach;->f:Lcom/google/r/b/a/aby;

    goto/16 :goto_8

    :cond_10
    iput-object v0, p0, Lcom/google/r/b/a/acj;->f:Lcom/google/r/b/a/aby;

    goto/16 :goto_9

    :cond_11
    move v0, v2

    .line 16417
    goto :goto_a

    .line 16418
    :cond_12
    iget-object v0, p1, Lcom/google/r/b/a/ach;->g:Lcom/google/maps/g/wb;

    goto :goto_b

    :cond_13
    iput-object v0, p0, Lcom/google/r/b/a/acj;->g:Lcom/google/maps/g/wb;

    goto :goto_c
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 16425
    const/4 v0, 0x1

    return v0
.end method
