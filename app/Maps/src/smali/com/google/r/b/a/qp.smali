.class public final Lcom/google/r/b/a/qp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/qn;",
        "Lcom/google/r/b/a/qp;",
        ">;",
        "Lcom/google/r/b/a/qq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 8581
    sget-object v0, Lcom/google/r/b/a/qn;->e:Lcom/google/r/b/a/qn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 8662
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    .line 8799
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    .line 8582
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 8573
    new-instance v2, Lcom/google/r/b/a/qn;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/qn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/qp;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/r/b/a/qp;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/qp;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/qp;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/qp;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/qp;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/qp;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    :goto_0
    iget-wide v4, p0, Lcom/google/r/b/a/qp;->d:J

    iput-wide v4, v2, Lcom/google/r/b/a/qn;->d:J

    iput v0, v2, Lcom/google/r/b/a/qn;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 8573
    check-cast p1, Lcom/google/r/b/a/qn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/qp;->a(Lcom/google/r/b/a/qn;)Lcom/google/r/b/a/qp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/qn;)Lcom/google/r/b/a/qp;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 8621
    invoke-static {}, Lcom/google/r/b/a/qn;->d()Lcom/google/r/b/a/qn;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 8646
    :goto_0
    return-object p0

    .line 8622
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 8623
    iget-object v1, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 8624
    iget-object v1, p1, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    .line 8625
    iget v1, p0, Lcom/google/r/b/a/qp;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/qp;->a:I

    .line 8632
    :cond_1
    :goto_1
    iget-object v1, p1, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 8633
    iget-object v1, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 8634
    iget-object v1, p1, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    .line 8635
    iget v1, p0, Lcom/google/r/b/a/qp;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/qp;->a:I

    .line 8642
    :cond_2
    :goto_2
    iget v1, p1, Lcom/google/r/b/a/qn;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_3
    if-eqz v0, :cond_3

    .line 8643
    iget-wide v0, p1, Lcom/google/r/b/a/qn;->d:J

    iget v2, p0, Lcom/google/r/b/a/qp;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/qp;->a:I

    iput-wide v0, p0, Lcom/google/r/b/a/qp;->d:J

    .line 8645
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/qn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 8627
    :cond_4
    iget v1, p0, Lcom/google/r/b/a/qp;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/qp;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/qp;->a:I

    .line 8628
    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 8637
    :cond_6
    iget v1, p0, Lcom/google/r/b/a/qp;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/qp;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/qp;->a:I

    .line 8638
    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/qp;->c:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 8642
    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 8650
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 8651
    iget-object v0, p0, Lcom/google/r/b/a/qp;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/qf;->d()Lcom/google/r/b/a/qf;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/qf;

    invoke-virtual {v0}, Lcom/google/r/b/a/qf;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 8656
    :goto_1
    return v2

    .line 8650
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 8656
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
