.class public final Lcom/google/r/b/a/yv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/yw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/yt;",
        "Lcom/google/r/b/a/yv;",
        ">;",
        "Lcom/google/r/b/a/yw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2804
    sget-object v0, Lcom/google/r/b/a/yt;->j:Lcom/google/r/b/a/yt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3035
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/r/b/a/yv;->f:I

    .line 3071
    const v0, 0x4c4b400

    iput v0, p0, Lcom/google/r/b/a/yv;->g:I

    .line 3103
    const v0, -0x55d4a80

    iput v0, p0, Lcom/google/r/b/a/yv;->h:I

    .line 3135
    const v0, 0x55d4a80

    iput v0, p0, Lcom/google/r/b/a/yv;->i:I

    .line 2805
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 2796
    invoke-virtual {p0}, Lcom/google/r/b/a/yv;->c()Lcom/google/r/b/a/yt;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2796
    check-cast p1, Lcom/google/r/b/a/yt;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/yv;->a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2872
    invoke-static {}, Lcom/google/r/b/a/yt;->d()Lcom/google/r/b/a/yt;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2898
    :goto_0
    return-object p0

    .line 2873
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2874
    iget v2, p1, Lcom/google/r/b/a/yt;->b:I

    iget v3, p0, Lcom/google/r/b/a/yv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/yv;->a:I

    iput v2, p0, Lcom/google/r/b/a/yv;->b:I

    .line 2876
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2877
    iget v2, p1, Lcom/google/r/b/a/yt;->c:I

    iget v3, p0, Lcom/google/r/b/a/yv;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/yv;->a:I

    iput v2, p0, Lcom/google/r/b/a/yv;->c:I

    .line 2879
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 2880
    iget v2, p1, Lcom/google/r/b/a/yt;->d:I

    iget v3, p0, Lcom/google/r/b/a/yv;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/yv;->a:I

    iput v2, p0, Lcom/google/r/b/a/yv;->d:I

    .line 2882
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 2883
    iget v2, p1, Lcom/google/r/b/a/yt;->e:I

    iget v3, p0, Lcom/google/r/b/a/yv;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/yv;->a:I

    iput v2, p0, Lcom/google/r/b/a/yv;->e:I

    .line 2885
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 2886
    iget v2, p1, Lcom/google/r/b/a/yt;->f:I

    invoke-static {v2}, Lcom/google/r/b/a/yx;->a(I)Lcom/google/r/b/a/yx;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/r/b/a/yx;->a:Lcom/google/r/b/a/yx;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 2873
    goto :goto_1

    :cond_7
    move v2, v1

    .line 2876
    goto :goto_2

    :cond_8
    move v2, v1

    .line 2879
    goto :goto_3

    :cond_9
    move v2, v1

    .line 2882
    goto :goto_4

    :cond_a
    move v2, v1

    .line 2885
    goto :goto_5

    .line 2886
    :cond_b
    iget v3, p0, Lcom/google/r/b/a/yv;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/yv;->a:I

    iget v2, v2, Lcom/google/r/b/a/yx;->c:I

    iput v2, p0, Lcom/google/r/b/a/yv;->f:I

    .line 2888
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 2889
    iget v2, p1, Lcom/google/r/b/a/yt;->g:I

    iget v3, p0, Lcom/google/r/b/a/yv;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/yv;->a:I

    iput v2, p0, Lcom/google/r/b/a/yv;->g:I

    .line 2891
    :cond_d
    iget v2, p1, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_e

    .line 2892
    iget v2, p1, Lcom/google/r/b/a/yt;->h:I

    iget v3, p0, Lcom/google/r/b/a/yv;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/yv;->a:I

    iput v2, p0, Lcom/google/r/b/a/yv;->h:I

    .line 2894
    :cond_e
    iget v2, p1, Lcom/google/r/b/a/yt;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_f

    .line 2895
    iget v0, p1, Lcom/google/r/b/a/yt;->i:I

    iget v1, p0, Lcom/google/r/b/a/yv;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/yv;->a:I

    iput v0, p0, Lcom/google/r/b/a/yv;->i:I

    .line 2897
    :cond_f
    iget-object v0, p1, Lcom/google/r/b/a/yt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_10
    move v2, v1

    .line 2888
    goto :goto_6

    :cond_11
    move v2, v1

    .line 2891
    goto :goto_7

    :cond_12
    move v0, v1

    .line 2894
    goto :goto_8
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2902
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/r/b/a/yt;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2832
    new-instance v2, Lcom/google/r/b/a/yt;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/yt;-><init>(Lcom/google/n/v;)V

    .line 2833
    iget v3, p0, Lcom/google/r/b/a/yv;->a:I

    .line 2834
    const/4 v1, 0x0

    .line 2835
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 2838
    :goto_0
    iget v1, p0, Lcom/google/r/b/a/yv;->b:I

    iput v1, v2, Lcom/google/r/b/a/yt;->b:I

    .line 2839
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2840
    or-int/lit8 v0, v0, 0x2

    .line 2842
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/yv;->c:I

    iput v1, v2, Lcom/google/r/b/a/yt;->c:I

    .line 2843
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2844
    or-int/lit8 v0, v0, 0x4

    .line 2846
    :cond_1
    iget v1, p0, Lcom/google/r/b/a/yv;->d:I

    iput v1, v2, Lcom/google/r/b/a/yt;->d:I

    .line 2847
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2848
    or-int/lit8 v0, v0, 0x8

    .line 2850
    :cond_2
    iget v1, p0, Lcom/google/r/b/a/yv;->e:I

    iput v1, v2, Lcom/google/r/b/a/yt;->e:I

    .line 2851
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2852
    or-int/lit8 v0, v0, 0x10

    .line 2854
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/yv;->f:I

    iput v1, v2, Lcom/google/r/b/a/yt;->f:I

    .line 2855
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 2856
    or-int/lit8 v0, v0, 0x20

    .line 2858
    :cond_4
    iget v1, p0, Lcom/google/r/b/a/yv;->g:I

    iput v1, v2, Lcom/google/r/b/a/yt;->g:I

    .line 2859
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 2860
    or-int/lit8 v0, v0, 0x40

    .line 2862
    :cond_5
    iget v1, p0, Lcom/google/r/b/a/yv;->h:I

    iput v1, v2, Lcom/google/r/b/a/yt;->h:I

    .line 2863
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 2864
    or-int/lit16 v0, v0, 0x80

    .line 2866
    :cond_6
    iget v1, p0, Lcom/google/r/b/a/yv;->i:I

    iput v1, v2, Lcom/google/r/b/a/yt;->i:I

    .line 2867
    iput v0, v2, Lcom/google/r/b/a/yt;->a:I

    .line 2868
    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method
