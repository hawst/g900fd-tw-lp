.class public final Lcom/google/r/b/a/yc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/yh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ya;",
        "Lcom/google/r/b/a/yc;",
        ">;",
        "Lcom/google/r/b/a/yh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/yd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4457
    sget-object v0, Lcom/google/r/b/a/ya;->e:Lcom/google/r/b/a/ya;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4529
    const v0, 0x6ddd0

    iput v0, p0, Lcom/google/r/b/a/yc;->b:I

    .line 4561
    const v0, 0x7a120

    iput v0, p0, Lcom/google/r/b/a/yc;->c:I

    .line 4594
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    .line 4458
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 4449
    invoke-virtual {p0}, Lcom/google/r/b/a/yc;->c()Lcom/google/r/b/a/ya;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4449
    check-cast p1, Lcom/google/r/b/a/ya;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/yc;->a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4496
    invoke-static {}, Lcom/google/r/b/a/ya;->d()Lcom/google/r/b/a/ya;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4514
    :goto_0
    return-object p0

    .line 4497
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ya;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4498
    iget v2, p1, Lcom/google/r/b/a/ya;->b:I

    iget v3, p0, Lcom/google/r/b/a/yc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/yc;->a:I

    iput v2, p0, Lcom/google/r/b/a/yc;->b:I

    .line 4500
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ya;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    :goto_2
    if-eqz v0, :cond_2

    .line 4501
    iget v0, p1, Lcom/google/r/b/a/ya;->c:I

    iget v1, p0, Lcom/google/r/b/a/yc;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/yc;->a:I

    iput v0, p0, Lcom/google/r/b/a/yc;->c:I

    .line 4503
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4504
    iget-object v0, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4505
    iget-object v0, p1, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    .line 4506
    iget v0, p0, Lcom/google/r/b/a/yc;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/r/b/a/yc;->a:I

    .line 4513
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/r/b/a/ya;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 4497
    goto :goto_1

    :cond_5
    move v0, v1

    .line 4500
    goto :goto_2

    .line 4508
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/yc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/yc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/yc;->a:I

    .line 4509
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4518
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 4519
    iget-object v0, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yd;

    invoke-virtual {v0}, Lcom/google/r/b/a/yd;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4524
    :goto_1
    return v2

    .line 4518
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4524
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final c()Lcom/google/r/b/a/ya;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4475
    new-instance v2, Lcom/google/r/b/a/ya;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ya;-><init>(Lcom/google/n/v;)V

    .line 4476
    iget v3, p0, Lcom/google/r/b/a/yc;->a:I

    .line 4477
    const/4 v1, 0x0

    .line 4478
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 4481
    :goto_0
    iget v1, p0, Lcom/google/r/b/a/yc;->b:I

    iput v1, v2, Lcom/google/r/b/a/ya;->b:I

    .line 4482
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 4483
    or-int/lit8 v0, v0, 0x2

    .line 4485
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/yc;->c:I

    iput v1, v2, Lcom/google/r/b/a/ya;->c:I

    .line 4486
    iget v1, p0, Lcom/google/r/b/a/yc;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 4487
    iget-object v1, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    .line 4488
    iget v1, p0, Lcom/google/r/b/a/yc;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/yc;->a:I

    .line 4490
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/yc;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    .line 4491
    iput v0, v2, Lcom/google/r/b/a/ya;->a:I

    .line 4492
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
