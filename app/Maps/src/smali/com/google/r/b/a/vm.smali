.class public final Lcom/google/r/b/a/vm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/vp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/vk;",
        "Lcom/google/r/b/a/vm;",
        ">;",
        "Lcom/google/r/b/a/vp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 827
    sget-object v0, Lcom/google/r/b/a/vk;->c:Lcom/google/r/b/a/vk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 867
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/vm;->b:I

    .line 828
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 819
    new-instance v2, Lcom/google/r/b/a/vk;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/vk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/vm;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/vm;->b:I

    iput v1, v2, Lcom/google/r/b/a/vk;->b:I

    iput v0, v2, Lcom/google/r/b/a/vk;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 819
    check-cast p1, Lcom/google/r/b/a/vk;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/vm;->a(Lcom/google/r/b/a/vk;)Lcom/google/r/b/a/vm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/vk;)Lcom/google/r/b/a/vm;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 853
    invoke-static {}, Lcom/google/r/b/a/vk;->d()Lcom/google/r/b/a/vk;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 858
    :goto_0
    return-object p0

    .line 854
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/vk;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 855
    iget v0, p1, Lcom/google/r/b/a/vk;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/vn;->a(I)Lcom/google/r/b/a/vn;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/vn;->a:Lcom/google/r/b/a/vn;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 854
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 855
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/vm;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/vm;->a:I

    iget v0, v0, Lcom/google/r/b/a/vn;->c:I

    iput v0, p0, Lcom/google/r/b/a/vm;->b:I

    .line 857
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/vk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 862
    const/4 v0, 0x1

    return v0
.end method
