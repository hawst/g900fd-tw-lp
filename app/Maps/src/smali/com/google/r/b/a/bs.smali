.class public final Lcom/google/r/b/a/bs;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/bt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/bn;",
        "Lcom/google/r/b/a/bs;",
        ">;",
        "Lcom/google/r/b/a/bt;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4598
    sget-object v0, Lcom/google/r/b/a/bn;->b:Lcom/google/r/b/a/bn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4645
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    .line 4599
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 4590
    new-instance v0, Lcom/google/r/b/a/bn;

    invoke-direct {v0, p0}, Lcom/google/r/b/a/bn;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/r/b/a/bs;->a:I

    iget v1, p0, Lcom/google/r/b/a/bs;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/bs;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/bs;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/r/b/a/bn;->a:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4590
    check-cast p1, Lcom/google/r/b/a/bn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/bs;->a(Lcom/google/r/b/a/bn;)Lcom/google/r/b/a/bs;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/bn;)Lcom/google/r/b/a/bs;
    .locals 2

    .prologue
    .line 4623
    invoke-static {}, Lcom/google/r/b/a/bn;->d()Lcom/google/r/b/a/bn;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 4635
    :goto_0
    return-object p0

    .line 4624
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/bn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4625
    iget-object v0, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4626
    iget-object v0, p1, Lcom/google/r/b/a/bn;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    .line 4627
    iget v0, p0, Lcom/google/r/b/a/bs;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/r/b/a/bs;->a:I

    .line 4634
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/r/b/a/bn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 4629
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/bs;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/bs;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/bs;->a:I

    .line 4630
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/bs;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/bn;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4639
    const/4 v0, 0x1

    return v0
.end method
