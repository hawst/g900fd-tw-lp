.class public final Lcom/google/r/b/a/aaa;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aab;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/zy;",
        "Lcom/google/r/b/a/aaa;",
        ">;",
        "Lcom/google/r/b/a/aab;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/n/aq;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/n/ao;

.field private h:Ljava/lang/Object;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 6482
    sget-object v0, Lcom/google/r/b/a/zy;->l:Lcom/google/r/b/a/zy;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6645
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->b:Lcom/google/n/ao;

    .line 6704
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->c:Ljava/lang/Object;

    .line 6780
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->d:Ljava/lang/Object;

    .line 6856
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    .line 6949
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->f:Ljava/lang/Object;

    .line 7025
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->g:Lcom/google/n/ao;

    .line 7084
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->h:Ljava/lang/Object;

    .line 7161
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    .line 7297
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->j:Lcom/google/n/ao;

    .line 7356
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aaa;->k:Lcom/google/n/ao;

    .line 6483
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6474
    new-instance v2, Lcom/google/r/b/a/zy;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/zy;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/zy;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aaa;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aaa;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/aaa;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/zy;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/aaa;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/zy;->d:Ljava/lang/Object;

    iget v4, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/r/b/a/aaa;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/r/b/a/zy;->e:Lcom/google/n/aq;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/aaa;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/zy;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/zy;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aaa;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aaa;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, p0, Lcom/google/r/b/a/aaa;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/zy;->h:Ljava/lang/Object;

    iget v4, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/r/b/a/aaa;->a:I

    :cond_6
    iget-object v4, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/zy;->i:Ljava/util/List;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget-object v4, v2, Lcom/google/r/b/a/zy;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aaa;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aaa;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-object v3, v2, Lcom/google/r/b/a/zy;->k:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/aaa;->k:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/aaa;->k:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/zy;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 6474
    check-cast p1, Lcom/google/r/b/a/zy;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aaa;->a(Lcom/google/r/b/a/zy;)Lcom/google/r/b/a/aaa;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/zy;)Lcom/google/r/b/a/aaa;
    .locals 6

    .prologue
    const/16 v5, 0x80

    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6572
    invoke-static {}, Lcom/google/r/b/a/zy;->d()Lcom/google/r/b/a/zy;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6630
    :goto_0
    return-object p0

    .line 6573
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/zy;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 6574
    iget-object v2, p0, Lcom/google/r/b/a/aaa;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/zy;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6575
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6577
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/zy;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 6578
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6579
    iget-object v2, p1, Lcom/google/r/b/a/zy;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aaa;->c:Ljava/lang/Object;

    .line 6582
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/zy;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 6583
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6584
    iget-object v2, p1, Lcom/google/r/b/a/zy;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aaa;->d:Ljava/lang/Object;

    .line 6587
    :cond_3
    iget-object v2, p1, Lcom/google/r/b/a/zy;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 6588
    iget-object v2, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 6589
    iget-object v2, p1, Lcom/google/r/b/a/zy;->e:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    .line 6590
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6597
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/r/b/a/zy;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 6598
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6599
    iget-object v2, p1, Lcom/google/r/b/a/zy;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aaa;->f:Ljava/lang/Object;

    .line 6602
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/zy;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 6603
    iget-object v2, p0, Lcom/google/r/b/a/aaa;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/zy;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6604
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6606
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/zy;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 6607
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6608
    iget-object v2, p1, Lcom/google/r/b/a/zy;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aaa;->h:Ljava/lang/Object;

    .line 6611
    :cond_7
    iget-object v2, p1, Lcom/google/r/b/a/zy;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 6612
    iget-object v2, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 6613
    iget-object v2, p1, Lcom/google/r/b/a/zy;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    .line 6614
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6621
    :cond_8
    :goto_8
    iget v2, p1, Lcom/google/r/b/a/zy;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 6622
    iget-object v2, p0, Lcom/google/r/b/a/aaa;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/zy;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6623
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6625
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/zy;->a:I

    and-int/lit16 v2, v2, 0x80

    if-ne v2, v5, :cond_16

    :goto_a
    if-eqz v0, :cond_a

    .line 6626
    iget-object v0, p0, Lcom/google/r/b/a/aaa;->k:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/zy;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6627
    iget v0, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6629
    :cond_a
    iget-object v0, p1, Lcom/google/r/b/a/zy;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 6573
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 6577
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 6582
    goto/16 :goto_3

    .line 6592
    :cond_e
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_f

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6593
    :cond_f
    iget-object v2, p0, Lcom/google/r/b/a/aaa;->e:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/r/b/a/zy;->e:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 6597
    goto/16 :goto_5

    :cond_11
    move v2, v1

    .line 6602
    goto/16 :goto_6

    :cond_12
    move v2, v1

    .line 6606
    goto/16 :goto_7

    .line 6616
    :cond_13
    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eq v2, v5, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/aaa;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/aaa;->a:I

    .line 6617
    :cond_14
    iget-object v2, p0, Lcom/google/r/b/a/aaa;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/zy;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_15
    move v2, v1

    .line 6621
    goto :goto_9

    :cond_16
    move v0, v1

    .line 6625
    goto :goto_a
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 6634
    iget v0, p0, Lcom/google/r/b/a/aaa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 6635
    iget-object v0, p0, Lcom/google/r/b/a/aaa;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 6640
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 6634
    goto :goto_0

    :cond_1
    move v0, v2

    .line 6640
    goto :goto_1
.end method
