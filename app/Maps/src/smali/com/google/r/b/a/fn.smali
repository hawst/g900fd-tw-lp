.class public final Lcom/google/r/b/a/fn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/fq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/fn;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/fn;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5330
    new-instance v0, Lcom/google/r/b/a/fo;

    invoke-direct {v0}, Lcom/google/r/b/a/fo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/fn;->PARSER:Lcom/google/n/ax;

    .line 5421
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/fn;->g:Lcom/google/n/aw;

    .line 5652
    new-instance v0, Lcom/google/r/b/a/fn;

    invoke-direct {v0}, Lcom/google/r/b/a/fn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/fn;->d:Lcom/google/r/b/a/fn;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 5275
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5363
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/fn;->c:Lcom/google/n/ao;

    .line 5378
    iput-byte v2, p0, Lcom/google/r/b/a/fn;->e:B

    .line 5400
    iput v2, p0, Lcom/google/r/b/a/fn;->f:I

    .line 5276
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/fn;->b:I

    .line 5277
    iget-object v0, p0, Lcom/google/r/b/a/fn;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 5278
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 5284
    invoke-direct {p0}, Lcom/google/r/b/a/fn;-><init>()V

    .line 5285
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 5290
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 5291
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 5292
    sparse-switch v3, :sswitch_data_0

    .line 5297
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 5299
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 5295
    goto :goto_0

    .line 5304
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 5305
    invoke-static {v3}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v4

    .line 5306
    if-nez v4, :cond_1

    .line 5307
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 5321
    :catch_0
    move-exception v0

    .line 5322
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5327
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/fn;->au:Lcom/google/n/bn;

    throw v0

    .line 5309
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/fn;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/fn;->a:I

    .line 5310
    iput v3, p0, Lcom/google/r/b/a/fn;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 5323
    :catch_1
    move-exception v0

    .line 5324
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 5325
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5315
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/fn;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 5316
    iget v3, p0, Lcom/google/r/b/a/fn;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/fn;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 5327
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fn;->au:Lcom/google/n/bn;

    .line 5328
    return-void

    .line 5292
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 5273
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5363
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/fn;->c:Lcom/google/n/ao;

    .line 5378
    iput-byte v1, p0, Lcom/google/r/b/a/fn;->e:B

    .line 5400
    iput v1, p0, Lcom/google/r/b/a/fn;->f:I

    .line 5274
    return-void
.end method

.method public static d()Lcom/google/r/b/a/fn;
    .locals 1

    .prologue
    .line 5655
    sget-object v0, Lcom/google/r/b/a/fn;->d:Lcom/google/r/b/a/fn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/fp;
    .locals 1

    .prologue
    .line 5483
    new-instance v0, Lcom/google/r/b/a/fp;

    invoke-direct {v0}, Lcom/google/r/b/a/fp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/fn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5342
    sget-object v0, Lcom/google/r/b/a/fn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5390
    invoke-virtual {p0}, Lcom/google/r/b/a/fn;->c()I

    .line 5391
    iget v0, p0, Lcom/google/r/b/a/fn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5392
    iget v0, p0, Lcom/google/r/b/a/fn;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 5394
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/fn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 5395
    iget-object v0, p0, Lcom/google/r/b/a/fn;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5397
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/fn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5398
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5380
    iget-byte v1, p0, Lcom/google/r/b/a/fn;->e:B

    .line 5381
    if-ne v1, v0, :cond_0

    .line 5385
    :goto_0
    return v0

    .line 5382
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 5384
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/fn;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 5402
    iget v0, p0, Lcom/google/r/b/a/fn;->f:I

    .line 5403
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5416
    :goto_0
    return v0

    .line 5406
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/fn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 5407
    iget v0, p0, Lcom/google/r/b/a/fn;->b:I

    .line 5408
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 5410
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/fn;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 5411
    iget-object v2, p0, Lcom/google/r/b/a/fn;->c:Lcom/google/n/ao;

    .line 5412
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 5414
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/fn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 5415
    iput v0, p0, Lcom/google/r/b/a/fn;->f:I

    goto :goto_0

    .line 5408
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5267
    invoke-static {}, Lcom/google/r/b/a/fn;->newBuilder()Lcom/google/r/b/a/fp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/fp;->a(Lcom/google/r/b/a/fn;)Lcom/google/r/b/a/fp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5267
    invoke-static {}, Lcom/google/r/b/a/fn;->newBuilder()Lcom/google/r/b/a/fp;

    move-result-object v0

    return-object v0
.end method
