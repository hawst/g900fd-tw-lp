.class public final Lcom/google/r/b/a/aqg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aql;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aqg;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final t:Lcom/google/r/b/a/aqg;

.field private static volatile w:Lcom/google/n/aw;


# instance fields
.field a:I

.field public b:I

.field public c:Z

.field public d:I

.field public e:I

.field f:Z

.field g:I

.field public h:I

.field public i:Z

.field j:Z

.field k:Z

.field l:I

.field public m:I

.field public n:I

.field public o:Z

.field public p:Z

.field public q:Ljava/lang/Object;

.field r:Ljava/lang/Object;

.field public s:Z

.field private u:B

.field private v:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/google/r/b/a/aqh;

    invoke-direct {v0}, Lcom/google/r/b/a/aqh;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aqg;->PARSER:Lcom/google/n/ax;

    .line 724
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aqg;->w:Lcom/google/n/aw;

    .line 1673
    new-instance v0, Lcom/google/r/b/a/aqg;

    invoke-direct {v0}, Lcom/google/r/b/a/aqg;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aqg;->t:Lcom/google/r/b/a/aqg;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x2760

    const/4 v0, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 569
    iput-byte v0, p0, Lcom/google/r/b/a/aqg;->u:B

    .line 639
    iput v0, p0, Lcom/google/r/b/a/aqg;->v:I

    .line 18
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/r/b/a/aqg;->b:I

    .line 19
    iput-boolean v2, p0, Lcom/google/r/b/a/aqg;->c:Z

    .line 20
    const/16 v0, 0xe

    iput v0, p0, Lcom/google/r/b/a/aqg;->d:I

    .line 21
    iput v3, p0, Lcom/google/r/b/a/aqg;->e:I

    .line 22
    iput-boolean v1, p0, Lcom/google/r/b/a/aqg;->f:Z

    .line 23
    iput v1, p0, Lcom/google/r/b/a/aqg;->g:I

    .line 24
    iput v1, p0, Lcom/google/r/b/a/aqg;->h:I

    .line 25
    iput-boolean v2, p0, Lcom/google/r/b/a/aqg;->i:Z

    .line 26
    iput-boolean v1, p0, Lcom/google/r/b/a/aqg;->j:Z

    .line 27
    iput-boolean v1, p0, Lcom/google/r/b/a/aqg;->k:Z

    .line 28
    const/16 v0, 0x78

    iput v0, p0, Lcom/google/r/b/a/aqg;->l:I

    .line 29
    iput v3, p0, Lcom/google/r/b/a/aqg;->m:I

    .line 30
    iput v1, p0, Lcom/google/r/b/a/aqg;->n:I

    .line 31
    iput-boolean v1, p0, Lcom/google/r/b/a/aqg;->o:Z

    .line 32
    iput-boolean v1, p0, Lcom/google/r/b/a/aqg;->p:Z

    .line 33
    const-string v0, "https://www.google.com/maps/vt/"

    iput-object v0, p0, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    .line 34
    const-string v0, "https://www.google.com/maps/vt/"

    iput-object v0, p0, Lcom/google/r/b/a/aqg;->r:Ljava/lang/Object;

    .line 35
    iput-boolean v2, p0, Lcom/google/r/b/a/aqg;->s:Z

    .line 36
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 42
    invoke-direct {p0}, Lcom/google/r/b/a/aqg;-><init>()V

    .line 43
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 48
    :cond_0
    :goto_0
    if-nez v3, :cond_a

    .line 49
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 50
    sparse-switch v0, :sswitch_data_0

    .line 55
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 57
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 53
    goto :goto_0

    .line 62
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aqg;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 161
    :catch_0
    move-exception v0

    .line 162
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aqg;->au:Lcom/google/n/bn;

    throw v0

    .line 67
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/aqg;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 163
    :catch_1
    move-exception v0

    .line 164
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 165
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 68
    goto :goto_1

    .line 72
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aqg;->d:I

    goto :goto_0

    .line 77
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 78
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aqg;->e:I

    goto :goto_0

    .line 82
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/aqg;->f:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 87
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aqg;->g:I

    goto/16 :goto_0

    .line 92
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 93
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aqg;->h:I

    goto/16 :goto_0

    .line 97
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 98
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/r/b/a/aqg;->i:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 102
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 103
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/r/b/a/aqg;->j:Z

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_4

    .line 107
    :sswitch_a
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 108
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/r/b/a/aqg;->k:Z

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_5

    .line 112
    :sswitch_b
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 113
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aqg;->l:I

    goto/16 :goto_0

    .line 117
    :sswitch_c
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 118
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/aqg;->m:I

    goto/16 :goto_0

    .line 122
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 123
    invoke-static {v0}, Lcom/google/r/b/a/aqj;->a(I)Lcom/google/r/b/a/aqj;

    move-result-object v5

    .line 124
    if-nez v5, :cond_6

    .line 125
    const/16 v5, 0xd

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 127
    :cond_6
    iget v5, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit16 v5, v5, 0x1000

    iput v5, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 128
    iput v0, p0, Lcom/google/r/b/a/aqg;->n:I

    goto/16 :goto_0

    .line 133
    :sswitch_e
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 134
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_7

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/r/b/a/aqg;->o:Z

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_6

    .line 138
    :sswitch_f
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 139
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/r/b/a/aqg;->p:Z

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto :goto_7

    .line 143
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 144
    iget v5, p0, Lcom/google/r/b/a/aqg;->a:I

    const v6, 0x8000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 145
    iput-object v0, p0, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    goto/16 :goto_0

    .line 149
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 150
    iget v5, p0, Lcom/google/r/b/a/aqg;->a:I

    const/high16 v6, 0x10000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 151
    iput-object v0, p0, Lcom/google/r/b/a/aqg;->r:Ljava/lang/Object;

    goto/16 :goto_0

    .line 155
    :sswitch_12
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    const/high16 v5, 0x20000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/aqg;->a:I

    .line 156
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_9

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/google/r/b/a/aqg;->s:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto :goto_8

    .line 167
    :cond_a
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqg;->au:Lcom/google/n/bn;

    .line 168
    return-void

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 569
    iput-byte v0, p0, Lcom/google/r/b/a/aqg;->u:B

    .line 639
    iput v0, p0, Lcom/google/r/b/a/aqg;->v:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aqg;
    .locals 1

    .prologue
    .line 1676
    sget-object v0, Lcom/google/r/b/a/aqg;->t:Lcom/google/r/b/a/aqg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aqi;
    .locals 1

    .prologue
    .line 786
    new-instance v0, Lcom/google/r/b/a/aqi;

    invoke-direct {v0}, Lcom/google/r/b/a/aqi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aqg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    sget-object v0, Lcom/google/r/b/a/aqg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 581
    invoke-virtual {p0}, Lcom/google/r/b/a/aqg;->c()I

    .line 582
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 583
    iget v0, p0, Lcom/google/r/b/a/aqg;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 585
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 586
    iget-boolean v0, p0, Lcom/google/r/b/a/aqg;->c:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 588
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 589
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/aqg;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 591
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 592
    iget v0, p0, Lcom/google/r/b/a/aqg;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 594
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_4

    .line 595
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/aqg;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 597
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 598
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/aqg;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 600
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 601
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/aqg;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 603
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 604
    iget-boolean v0, p0, Lcom/google/r/b/a/aqg;->i:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 606
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 607
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/aqg;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 609
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 610
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/r/b/a/aqg;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 612
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 613
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/r/b/a/aqg;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 615
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 616
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/r/b/a/aqg;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 618
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 619
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/r/b/a/aqg;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 621
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 622
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/r/b/a/aqg;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 624
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_e

    .line 625
    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/google/r/b/a/aqg;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 627
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_f

    .line 628
    iget-object v0, p0, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 630
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_10

    .line 631
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/r/b/a/aqg;->r:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqg;->r:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 633
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_11

    .line 634
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/r/b/a/aqg;->s:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 636
    :cond_11
    iget-object v0, p0, Lcom/google/r/b/a/aqg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 637
    return-void

    .line 628
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 631
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 571
    iget-byte v1, p0, Lcom/google/r/b/a/aqg;->u:B

    .line 572
    if-ne v1, v0, :cond_0

    .line 576
    :goto_0
    return v0

    .line 573
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 575
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aqg;->u:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 641
    iget v0, p0, Lcom/google/r/b/a/aqg;->v:I

    .line 642
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 719
    :goto_0
    return v0

    .line 645
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1c

    .line 646
    iget v0, p0, Lcom/google/r/b/a/aqg;->b:I

    .line 647
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_12

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 649
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 650
    iget-boolean v3, p0, Lcom/google/r/b/a/aqg;->c:Z

    .line 651
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 653
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 654
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/aqg;->d:I

    .line 655
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_13

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 657
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 658
    iget v3, p0, Lcom/google/r/b/a/aqg;->e:I

    .line 659
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_14

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 661
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 662
    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/r/b/a/aqg;->f:Z

    .line 663
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 665
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 666
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/r/b/a/aqg;->g:I

    .line 667
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_15

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 669
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 670
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/r/b/a/aqg;->h:I

    .line 671
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_16

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 673
    :cond_6
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 674
    const/16 v3, 0x8

    iget-boolean v4, p0, Lcom/google/r/b/a/aqg;->i:Z

    .line 675
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 677
    :cond_7
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 678
    const/16 v3, 0x9

    iget-boolean v4, p0, Lcom/google/r/b/a/aqg;->j:Z

    .line 679
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 681
    :cond_8
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 682
    iget-boolean v3, p0, Lcom/google/r/b/a/aqg;->k:Z

    .line 683
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 685
    :cond_9
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 686
    const/16 v3, 0xb

    iget v4, p0, Lcom/google/r/b/a/aqg;->l:I

    .line 687
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_17

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 689
    :cond_a
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 690
    const/16 v3, 0xc

    iget v4, p0, Lcom/google/r/b/a/aqg;->m:I

    .line 691
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_18

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 693
    :cond_b
    iget v3, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_d

    .line 694
    const/16 v3, 0xd

    iget v4, p0, Lcom/google/r/b/a/aqg;->n:I

    .line 695
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_c
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 697
    :cond_d
    iget v1, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_e

    .line 698
    const/16 v1, 0xe

    iget-boolean v3, p0, Lcom/google/r/b/a/aqg;->o:Z

    .line 699
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 701
    :cond_e
    iget v1, p0, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_1b

    .line 702
    const/16 v1, 0xf

    iget-boolean v3, p0, Lcom/google/r/b/a/aqg;->p:Z

    .line 703
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v0

    .line 705
    :goto_9
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    const v3, 0x8000

    and-int/2addr v0, v3

    const v3, 0x8000

    if-ne v0, v3, :cond_f

    .line 706
    const/16 v3, 0x10

    .line 707
    iget-object v0, p0, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    :goto_a
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 709
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000

    if-ne v0, v3, :cond_10

    .line 710
    const/16 v3, 0x11

    .line 711
    iget-object v0, p0, Lcom/google/r/b/a/aqg;->r:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aqg;->r:Ljava/lang/Object;

    :goto_b
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 713
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/aqg;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v0, v3

    const/high16 v3, 0x20000

    if-ne v0, v3, :cond_11

    .line 714
    const/16 v0, 0x12

    iget-boolean v3, p0, Lcom/google/r/b/a/aqg;->s:Z

    .line 715
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 717
    :cond_11
    iget-object v0, p0, Lcom/google/r/b/a/aqg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 718
    iput v0, p0, Lcom/google/r/b/a/aqg;->v:I

    goto/16 :goto_0

    :cond_12
    move v0, v1

    .line 647
    goto/16 :goto_1

    :cond_13
    move v3, v1

    .line 655
    goto/16 :goto_3

    :cond_14
    move v3, v1

    .line 659
    goto/16 :goto_4

    :cond_15
    move v3, v1

    .line 667
    goto/16 :goto_5

    :cond_16
    move v3, v1

    .line 671
    goto/16 :goto_6

    :cond_17
    move v3, v1

    .line 687
    goto/16 :goto_7

    :cond_18
    move v3, v1

    .line 691
    goto/16 :goto_8

    .line 707
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    .line 711
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto :goto_b

    :cond_1b
    move v1, v0

    goto/16 :goto_9

    :cond_1c
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/aqg;->newBuilder()Lcom/google/r/b/a/aqi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aqi;->a(Lcom/google/r/b/a/aqg;)Lcom/google/r/b/a/aqi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/aqg;->newBuilder()Lcom/google/r/b/a/aqi;

    move-result-object v0

    return-object v0
.end method
