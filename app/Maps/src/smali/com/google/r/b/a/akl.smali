.class public final enum Lcom/google/r/b/a/akl;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/akl;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/akl;

.field public static final enum b:Lcom/google/r/b/a/akl;

.field public static final enum c:Lcom/google/r/b/a/akl;

.field private static final synthetic e:[Lcom/google/r/b/a/akl;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/google/r/b/a/akl;

    const-string v1, "DIRECTIONS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/akl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/akl;->a:Lcom/google/r/b/a/akl;

    .line 23
    new-instance v0, Lcom/google/r/b/a/akl;

    const-string v1, "INTENT_MAP"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/akl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/akl;->b:Lcom/google/r/b/a/akl;

    .line 27
    new-instance v0, Lcom/google/r/b/a/akl;

    const-string v1, "TRANSIT_FEATURE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/akl;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/akl;->c:Lcom/google/r/b/a/akl;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/akl;

    sget-object v1, Lcom/google/r/b/a/akl;->a:Lcom/google/r/b/a/akl;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/akl;->b:Lcom/google/r/b/a/akl;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/akl;->c:Lcom/google/r/b/a/akl;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/akl;->e:[Lcom/google/r/b/a/akl;

    .line 62
    new-instance v0, Lcom/google/r/b/a/akm;

    invoke-direct {v0}, Lcom/google/r/b/a/akm;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput p3, p0, Lcom/google/r/b/a/akl;->d:I

    .line 73
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/akl;
    .locals 1

    .prologue
    .line 49
    packed-switch p0, :pswitch_data_0

    .line 53
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 50
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/akl;->a:Lcom/google/r/b/a/akl;

    goto :goto_0

    .line 51
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/akl;->b:Lcom/google/r/b/a/akl;

    goto :goto_0

    .line 52
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/akl;->c:Lcom/google/r/b/a/akl;

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/akl;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/r/b/a/akl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/akl;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/akl;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/r/b/a/akl;->e:[Lcom/google/r/b/a/akl;

    invoke-virtual {v0}, [Lcom/google/r/b/a/akl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/akl;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/r/b/a/akl;->d:I

    return v0
.end method
