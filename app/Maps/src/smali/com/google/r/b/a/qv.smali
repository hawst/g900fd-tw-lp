.class public final Lcom/google/r/b/a/qv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qy;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qv;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/qv;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field f:I

.field public g:I

.field public h:I

.field public i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/google/r/b/a/qw;

    invoke-direct {v0}, Lcom/google/r/b/a/qw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qv;->PARSER:Lcom/google/n/ax;

    .line 323
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/qv;->m:Lcom/google/n/aw;

    .line 766
    new-instance v0, Lcom/google/r/b/a/qv;

    invoke-direct {v0}, Lcom/google/r/b/a/qv;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qv;->j:Lcom/google/r/b/a/qv;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 238
    iput-byte v0, p0, Lcom/google/r/b/a/qv;->k:B

    .line 278
    iput v0, p0, Lcom/google/r/b/a/qv;->l:I

    .line 18
    const v0, 0x2a300

    iput v0, p0, Lcom/google/r/b/a/qv;->b:I

    .line 19
    const v0, 0x15180

    iput v0, p0, Lcom/google/r/b/a/qv;->c:I

    .line 20
    const/16 v0, 0x5460

    iput v0, p0, Lcom/google/r/b/a/qv;->d:I

    .line 21
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/r/b/a/qv;->e:I

    .line 22
    const v0, 0x69780

    iput v0, p0, Lcom/google/r/b/a/qv;->f:I

    .line 23
    const/16 v0, 0x19

    iput v0, p0, Lcom/google/r/b/a/qv;->g:I

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/qv;->h:I

    .line 25
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/qv;->i:I

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Lcom/google/r/b/a/qv;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 37
    const/4 v0, 0x0

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 40
    sparse-switch v3, :sswitch_data_0

    .line 45
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/qv;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qv;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/qv;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/qv;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qv;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 95
    :catch_1
    move-exception v0

    .line 96
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 97
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/qv;->a:I

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qv;->e:I

    goto :goto_0

    .line 67
    :sswitch_4
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/qv;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qv;->f:I

    goto :goto_0

    .line 72
    :sswitch_5
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/qv;->a:I

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qv;->g:I

    goto :goto_0

    .line 77
    :sswitch_6
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/qv;->a:I

    .line 78
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qv;->h:I

    goto :goto_0

    .line 82
    :sswitch_7
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/qv;->a:I

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qv;->i:I

    goto/16 :goto_0

    .line 87
    :sswitch_8
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/qv;->a:I

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qv;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 99
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qv;->au:Lcom/google/n/bn;

    .line 100
    return-void

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 238
    iput-byte v0, p0, Lcom/google/r/b/a/qv;->k:B

    .line 278
    iput v0, p0, Lcom/google/r/b/a/qv;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/qv;
    .locals 1

    .prologue
    .line 769
    sget-object v0, Lcom/google/r/b/a/qv;->j:Lcom/google/r/b/a/qv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/qx;
    .locals 1

    .prologue
    .line 385
    new-instance v0, Lcom/google/r/b/a/qx;

    invoke-direct {v0}, Lcom/google/r/b/a/qx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    sget-object v0, Lcom/google/r/b/a/qv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 250
    invoke-virtual {p0}, Lcom/google/r/b/a/qv;->c()I

    .line 251
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 252
    iget v0, p0, Lcom/google/r/b/a/qv;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 254
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 255
    iget v0, p0, Lcom/google/r/b/a/qv;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 257
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_2

    .line 258
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/qv;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 260
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 261
    iget v0, p0, Lcom/google/r/b/a/qv;->f:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 263
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 264
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/qv;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 266
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 267
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/qv;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 269
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 270
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/qv;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 272
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_7

    .line 273
    iget v0, p0, Lcom/google/r/b/a/qv;->d:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 275
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/qv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 276
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 240
    iget-byte v1, p0, Lcom/google/r/b/a/qv;->k:B

    .line 241
    if-ne v1, v0, :cond_0

    .line 245
    :goto_0
    return v0

    .line 242
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 244
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/qv;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 280
    iget v0, p0, Lcom/google/r/b/a/qv;->l:I

    .line 281
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 318
    :goto_0
    return v0

    .line 284
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_10

    .line 285
    iget v0, p0, Lcom/google/r/b/a/qv;->b:I

    .line 286
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 288
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 289
    iget v3, p0, Lcom/google/r/b/a/qv;->c:I

    .line 290
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 292
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 293
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/qv;->e:I

    .line 294
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 296
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    .line 297
    iget v3, p0, Lcom/google/r/b/a/qv;->f:I

    .line 298
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_c

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 300
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    .line 301
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/r/b/a/qv;->g:I

    .line 302
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 304
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    .line 305
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/r/b/a/qv;->h:I

    .line 306
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 308
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    .line 309
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/r/b/a/qv;->i:I

    .line 310
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 312
    :cond_6
    iget v3, p0, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_8

    .line 313
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/r/b/a/qv;->d:I

    .line 314
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_7
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 316
    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/qv;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    iput v0, p0, Lcom/google/r/b/a/qv;->l:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 286
    goto/16 :goto_1

    :cond_a
    move v3, v1

    .line 290
    goto/16 :goto_3

    :cond_b
    move v3, v1

    .line 294
    goto/16 :goto_4

    :cond_c
    move v3, v1

    .line 298
    goto/16 :goto_5

    :cond_d
    move v3, v1

    .line 302
    goto :goto_6

    :cond_e
    move v3, v1

    .line 306
    goto :goto_7

    :cond_f
    move v3, v1

    .line 310
    goto :goto_8

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/qv;->newBuilder()Lcom/google/r/b/a/qx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/qx;->a(Lcom/google/r/b/a/qv;)Lcom/google/r/b/a/qx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/qv;->newBuilder()Lcom/google/r/b/a/qx;

    move-result-object v0

    return-object v0
.end method
