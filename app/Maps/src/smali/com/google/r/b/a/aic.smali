.class public final Lcom/google/r/b/a/aic;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aif;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aia;",
        "Lcom/google/r/b/a/aic;",
        ">;",
        "Lcom/google/r/b/a/aif;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/aq;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 909
    sget-object v0, Lcom/google/r/b/a/aia;->e:Lcom/google/r/b/a/aia;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 978
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aic;->b:I

    .line 1014
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aic;->c:Lcom/google/n/ao;

    .line 1073
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    .line 910
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 901
    new-instance v2, Lcom/google/r/b/a/aia;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aia;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aic;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/aic;->b:I

    iput v4, v2, Lcom/google/r/b/a/aia;->b:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v3, v2, Lcom/google/r/b/a/aia;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/aic;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/aic;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/r/b/a/aic;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/r/b/a/aic;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/aic;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    iput v0, v2, Lcom/google/r/b/a/aia;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 901
    check-cast p1, Lcom/google/r/b/a/aia;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aic;->a(Lcom/google/r/b/a/aia;)Lcom/google/r/b/a/aic;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aia;)Lcom/google/r/b/a/aic;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 950
    invoke-static {}, Lcom/google/r/b/a/aia;->d()Lcom/google/r/b/a/aia;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 969
    :goto_0
    return-object p0

    .line 951
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aia;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 952
    iget v2, p1, Lcom/google/r/b/a/aia;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/aid;->a(I)Lcom/google/r/b/a/aid;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/aid;->a:Lcom/google/r/b/a/aid;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 951
    goto :goto_1

    .line 952
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/aic;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aic;->a:I

    iget v2, v2, Lcom/google/r/b/a/aid;->e:I

    iput v2, p0, Lcom/google/r/b/a/aic;->b:I

    .line 954
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/aia;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_2
    if-eqz v0, :cond_5

    .line 955
    iget-object v0, p0, Lcom/google/r/b/a/aic;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/aia;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 956
    iget v0, p0, Lcom/google/r/b/a/aic;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/aic;->a:I

    .line 958
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 959
    iget-object v0, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 960
    iget-object v0, p1, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    .line 961
    iget v0, p0, Lcom/google/r/b/a/aic;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/r/b/a/aic;->a:I

    .line 968
    :cond_6
    :goto_3
    iget-object v0, p1, Lcom/google/r/b/a/aia;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_7
    move v0, v1

    .line 954
    goto :goto_2

    .line 963
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/aic;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_9

    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/r/b/a/aic;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/aic;->a:I

    .line 964
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/aic;->d:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 973
    const/4 v0, 0x1

    return v0
.end method
