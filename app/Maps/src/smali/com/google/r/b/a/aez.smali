.class public final Lcom/google/r/b/a/aez;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aex;",
        "Lcom/google/r/b/a/aez;",
        ">;",
        "Lcom/google/r/b/a/afa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 9662
    sget-object v0, Lcom/google/r/b/a/aex;->f:Lcom/google/r/b/a/aex;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 9753
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    .line 9819
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    .line 9885
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    .line 9663
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 9654
    new-instance v2, Lcom/google/r/b/a/aex;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aex;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aez;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/aez;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/aez;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/r/b/a/aez;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/aez;->e:I

    iput v1, v2, Lcom/google/r/b/a/aex;->e:I

    iput v0, v2, Lcom/google/r/b/a/aex;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 9654
    check-cast p1, Lcom/google/r/b/a/aex;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aez;->a(Lcom/google/r/b/a/aex;)Lcom/google/r/b/a/aez;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aex;)Lcom/google/r/b/a/aez;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 9709
    invoke-static {}, Lcom/google/r/b/a/aex;->d()Lcom/google/r/b/a/aex;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 9744
    :goto_0
    return-object p0

    .line 9710
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 9711
    iget-object v1, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 9712
    iget-object v1, p1, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    .line 9713
    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/aez;->a:I

    .line 9720
    :cond_1
    :goto_1
    iget-object v1, p1, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 9721
    iget-object v1, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 9722
    iget-object v1, p1, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    .line 9723
    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/aez;->a:I

    .line 9730
    :cond_2
    :goto_2
    iget-object v1, p1, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 9731
    iget-object v1, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 9732
    iget-object v1, p1, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    .line 9733
    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/aez;->a:I

    .line 9740
    :cond_3
    :goto_3
    iget v1, p1, Lcom/google/r/b/a/aex;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_b

    :goto_4
    if-eqz v0, :cond_4

    .line 9741
    iget v0, p1, Lcom/google/r/b/a/aex;->e:I

    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/aez;->a:I

    iput v0, p0, Lcom/google/r/b/a/aez;->e:I

    .line 9743
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/aex;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 9715
    :cond_5
    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_6

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/aez;->a:I

    .line 9716
    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/aez;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 9725
    :cond_7
    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_8

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/aez;->a:I

    .line 9726
    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/aez;->c:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 9735
    :cond_9
    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-eq v1, v2, :cond_a

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/aez;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/aez;->a:I

    .line 9736
    :cond_a
    iget-object v1, p0, Lcom/google/r/b/a/aez;->d:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 9740
    :cond_b
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 9748
    const/4 v0, 0x1

    return v0
.end method
