.class public final Lcom/google/r/b/a/gw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/gz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/gw;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/gw;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/aq;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1202
    new-instance v0, Lcom/google/r/b/a/gx;

    invoke-direct {v0}, Lcom/google/r/b/a/gx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/gw;->PARSER:Lcom/google/n/ax;

    .line 1337
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/gw;->g:Lcom/google/n/aw;

    .line 1649
    new-instance v0, Lcom/google/r/b/a/gw;

    invoke-direct {v0}, Lcom/google/r/b/a/gw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/gw;->d:Lcom/google/r/b/a/gw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1145
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1289
    iput-byte v0, p0, Lcom/google/r/b/a/gw;->e:B

    .line 1311
    iput v0, p0, Lcom/google/r/b/a/gw;->f:I

    .line 1146
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/gw;->b:Ljava/lang/Object;

    .line 1147
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    .line 1148
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x2

    .line 1154
    invoke-direct {p0}, Lcom/google/r/b/a/gw;-><init>()V

    .line 1157
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 1160
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 1161
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 1162
    sparse-switch v4, :sswitch_data_0

    .line 1167
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 1169
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 1165
    goto :goto_0

    .line 1174
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 1175
    iget v5, p0, Lcom/google/r/b/a/gw;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/gw;->a:I

    .line 1176
    iput-object v4, p0, Lcom/google/r/b/a/gw;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 1190
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 1191
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1196
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 1197
    iget-object v1, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    .line 1199
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/gw;->au:Lcom/google/n/bn;

    throw v0

    .line 1180
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 1181
    and-int/lit8 v5, v0, 0x2

    if-eq v5, v6, :cond_2

    .line 1182
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    .line 1183
    or-int/lit8 v0, v0, 0x2

    .line 1185
    :cond_2
    iget-object v5, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 1192
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 1193
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1194
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1196
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 1197
    iget-object v0, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    .line 1199
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/gw;->au:Lcom/google/n/bn;

    .line 1200
    return-void

    .line 1196
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 1162
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1143
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1289
    iput-byte v0, p0, Lcom/google/r/b/a/gw;->e:B

    .line 1311
    iput v0, p0, Lcom/google/r/b/a/gw;->f:I

    .line 1144
    return-void
.end method

.method public static d()Lcom/google/r/b/a/gw;
    .locals 1

    .prologue
    .line 1652
    sget-object v0, Lcom/google/r/b/a/gw;->d:Lcom/google/r/b/a/gw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/gy;
    .locals 1

    .prologue
    .line 1399
    new-instance v0, Lcom/google/r/b/a/gy;

    invoke-direct {v0}, Lcom/google/r/b/a/gy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/gw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1214
    sget-object v0, Lcom/google/r/b/a/gw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1301
    invoke-virtual {p0}, Lcom/google/r/b/a/gw;->c()I

    .line 1302
    iget v0, p0, Lcom/google/r/b/a/gw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1303
    iget-object v0, p0, Lcom/google/r/b/a/gw;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/gw;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1305
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1306
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1305
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1303
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1308
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/gw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1309
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1291
    iget-byte v1, p0, Lcom/google/r/b/a/gw;->e:B

    .line 1292
    if-ne v1, v0, :cond_0

    .line 1296
    :goto_0
    return v0

    .line 1293
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1295
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/gw;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1313
    iget v0, p0, Lcom/google/r/b/a/gw;->f:I

    .line 1314
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1332
    :goto_0
    return v0

    .line 1317
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/gw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 1319
    iget-object v0, p0, Lcom/google/r/b/a/gw;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/gw;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    .line 1323
    :goto_3
    iget-object v3, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1324
    iget-object v3, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    .line 1325
    invoke-interface {v3, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 1323
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1319
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1327
    :cond_2
    add-int/2addr v0, v2

    .line 1328
    iget-object v1, p0, Lcom/google/r/b/a/gw;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1330
    iget-object v1, p0, Lcom/google/r/b/a/gw;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1331
    iput v0, p0, Lcom/google/r/b/a/gw;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1137
    invoke-static {}, Lcom/google/r/b/a/gw;->newBuilder()Lcom/google/r/b/a/gy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/gy;->a(Lcom/google/r/b/a/gw;)Lcom/google/r/b/a/gy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1137
    invoke-static {}, Lcom/google/r/b/a/gw;->newBuilder()Lcom/google/r/b/a/gy;

    move-result-object v0

    return-object v0
.end method
