.class public final Lcom/google/r/b/a/my;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/my;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/my;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4441
    new-instance v0, Lcom/google/r/b/a/mz;

    invoke-direct {v0}, Lcom/google/r/b/a/mz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/my;->PARSER:Lcom/google/n/ax;

    .line 4631
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/my;->i:Lcom/google/n/aw;

    .line 5103
    new-instance v0, Lcom/google/r/b/a/my;

    invoke-direct {v0}, Lcom/google/r/b/a/my;-><init>()V

    sput-object v0, Lcom/google/r/b/a/my;->f:Lcom/google/r/b/a/my;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 4365
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4458
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/my;->b:Lcom/google/n/ao;

    .line 4574
    iput-byte v2, p0, Lcom/google/r/b/a/my;->g:B

    .line 4602
    iput v2, p0, Lcom/google/r/b/a/my;->h:I

    .line 4366
    iget-object v0, p0, Lcom/google/r/b/a/my;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4367
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/my;->c:Ljava/lang/Object;

    .line 4368
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    .line 4369
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/my;->e:I

    .line 4370
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v7, 0x4

    .line 4376
    invoke-direct {p0}, Lcom/google/r/b/a/my;-><init>()V

    .line 4379
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 4382
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 4383
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 4384
    sparse-switch v4, :sswitch_data_0

    .line 4389
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 4391
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 4387
    goto :goto_0

    .line 4396
    :sswitch_1
    iget-object v4, p0, Lcom/google/r/b/a/my;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 4397
    iget v4, p0, Lcom/google/r/b/a/my;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/my;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4429
    :catch_0
    move-exception v0

    .line 4430
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4435
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_1

    .line 4436
    iget-object v1, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    .line 4438
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/my;->au:Lcom/google/n/bn;

    throw v0

    .line 4401
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 4402
    iget v5, p0, Lcom/google/r/b/a/my;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/my;->a:I

    .line 4403
    iput-object v4, p0, Lcom/google/r/b/a/my;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4431
    :catch_1
    move-exception v0

    .line 4432
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 4433
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4407
    :sswitch_3
    and-int/lit8 v4, v1, 0x4

    if-eq v4, v7, :cond_2

    .line 4408
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    .line 4410
    or-int/lit8 v1, v1, 0x4

    .line 4412
    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 4413
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 4412
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4417
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 4418
    invoke-static {v4}, Lcom/google/maps/g/fo;->a(I)Lcom/google/maps/g/fo;

    move-result-object v5

    .line 4419
    if-nez v5, :cond_3

    .line 4420
    const/4 v5, 0x4

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 4422
    :cond_3
    iget v5, p0, Lcom/google/r/b/a/my;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/my;->a:I

    .line 4423
    iput v4, p0, Lcom/google/r/b/a/my;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 4435
    :cond_4
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v7, :cond_5

    .line 4436
    iget-object v0, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    .line 4438
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/my;->au:Lcom/google/n/bn;

    .line 4439
    return-void

    .line 4384
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 4363
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4458
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/my;->b:Lcom/google/n/ao;

    .line 4574
    iput-byte v1, p0, Lcom/google/r/b/a/my;->g:B

    .line 4602
    iput v1, p0, Lcom/google/r/b/a/my;->h:I

    .line 4364
    return-void
.end method

.method public static d()Lcom/google/r/b/a/my;
    .locals 1

    .prologue
    .line 5106
    sget-object v0, Lcom/google/r/b/a/my;->f:Lcom/google/r/b/a/my;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/na;
    .locals 1

    .prologue
    .line 4693
    new-instance v0, Lcom/google/r/b/a/na;

    invoke-direct {v0}, Lcom/google/r/b/a/na;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/my;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4453
    sget-object v0, Lcom/google/r/b/a/my;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4586
    invoke-virtual {p0}, Lcom/google/r/b/a/my;->c()I

    .line 4587
    iget v0, p0, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4588
    iget-object v0, p0, Lcom/google/r/b/a/my;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4590
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4591
    iget-object v0, p0, Lcom/google/r/b/a/my;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/my;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4593
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 4594
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4593
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 4591
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 4596
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_4

    .line 4597
    iget v0, p0, Lcom/google/r/b/a/my;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 4599
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/my;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4600
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4576
    iget-byte v1, p0, Lcom/google/r/b/a/my;->g:B

    .line 4577
    if-ne v1, v0, :cond_0

    .line 4581
    :goto_0
    return v0

    .line 4578
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 4580
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/my;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4604
    iget v0, p0, Lcom/google/r/b/a/my;->h:I

    .line 4605
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4626
    :goto_0
    return v0

    .line 4608
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 4609
    iget-object v0, p0, Lcom/google/r/b/a/my;->b:Lcom/google/n/ao;

    .line 4610
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 4612
    :goto_1
    iget v0, p0, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 4614
    iget-object v0, p0, Lcom/google/r/b/a/my;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/my;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v3, v1

    move v1, v2

    .line 4616
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 4617
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/my;->d:Ljava/util/List;

    .line 4618
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 4616
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4614
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 4620
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/my;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 4621
    iget v0, p0, Lcom/google/r/b/a/my;->e:I

    .line 4622
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 4624
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/my;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 4625
    iput v0, p0, Lcom/google/r/b/a/my;->h:I

    goto/16 :goto_0

    .line 4622
    :cond_5
    const/16 v0, 0xa

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4357
    invoke-static {}, Lcom/google/r/b/a/my;->newBuilder()Lcom/google/r/b/a/na;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/na;->a(Lcom/google/r/b/a/my;)Lcom/google/r/b/a/na;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4357
    invoke-static {}, Lcom/google/r/b/a/my;->newBuilder()Lcom/google/r/b/a/na;

    move-result-object v0

    return-object v0
.end method
