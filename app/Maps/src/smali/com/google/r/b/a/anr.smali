.class public final Lcom/google/r/b/a/anr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/anu;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/anr;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/r/b/a/anr;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/aq;

.field e:Lcom/google/n/aq;

.field f:Lcom/google/n/ao;

.field g:Ljava/lang/Object;

.field h:I

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/google/r/b/a/ans;

    invoke-direct {v0}, Lcom/google/r/b/a/ans;-><init>()V

    sput-object v0, Lcom/google/r/b/a/anr;->PARSER:Lcom/google/n/ax;

    .line 384
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/anr;->l:Lcom/google/n/aw;

    .line 1066
    new-instance v0, Lcom/google/r/b/a/anr;

    invoke-direct {v0}, Lcom/google/r/b/a/anr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/anr;->i:Lcom/google/r/b/a/anr;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 128
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anr;->b:Lcom/google/n/ao;

    .line 144
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anr;->c:Lcom/google/n/ao;

    .line 218
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    .line 290
    iput-byte v3, p0, Lcom/google/r/b/a/anr;->j:B

    .line 333
    iput v3, p0, Lcom/google/r/b/a/anr;->k:I

    .line 18
    iget-object v0, p0, Lcom/google/r/b/a/anr;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/r/b/a/anr;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    .line 21
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    .line 22
    iget-object v0, p0, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/anr;->g:Ljava/lang/Object;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/anr;->h:I

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/r/b/a/anr;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 37
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 39
    sparse-switch v4, :sswitch_data_0

    .line 44
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 46
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    iget-object v4, p0, Lcom/google/r/b/a/anr;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 52
    iget v4, p0, Lcom/google/r/b/a/anr;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/anr;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x4

    if-ne v2, v6, :cond_1

    .line 103
    iget-object v2, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    .line 105
    :cond_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_2

    .line 106
    iget-object v1, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    .line 108
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/anr;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    iget-object v4, p0, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 57
    iget v4, p0, Lcom/google/r/b/a/anr;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/anr;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 98
    :catch_1
    move-exception v0

    .line 99
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 100
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 62
    and-int/lit8 v5, v1, 0x4

    if-eq v5, v6, :cond_3

    .line 63
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    .line 64
    or-int/lit8 v1, v1, 0x4

    .line 66
    :cond_3
    iget-object v5, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 70
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 71
    and-int/lit8 v5, v1, 0x8

    if-eq v5, v7, :cond_4

    .line 72
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    .line 73
    or-int/lit8 v1, v1, 0x8

    .line 75
    :cond_4
    iget-object v5, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 79
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 80
    iget v5, p0, Lcom/google/r/b/a/anr;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/r/b/a/anr;->a:I

    .line 81
    iput-object v4, p0, Lcom/google/r/b/a/anr;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 85
    :sswitch_6
    iget v4, p0, Lcom/google/r/b/a/anr;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/anr;->a:I

    .line 86
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/anr;->h:I

    goto/16 :goto_0

    .line 90
    :sswitch_7
    iget-object v4, p0, Lcom/google/r/b/a/anr;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 91
    iget v4, p0, Lcom/google/r/b/a/anr;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/anr;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 102
    :cond_5
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v6, :cond_6

    .line 103
    iget-object v0, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    .line 105
    :cond_6
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_7

    .line 106
    iget-object v0, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    .line 108
    :cond_7
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anr;->au:Lcom/google/n/bn;

    .line 109
    return-void

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 128
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anr;->b:Lcom/google/n/ao;

    .line 144
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anr;->c:Lcom/google/n/ao;

    .line 218
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    .line 290
    iput-byte v1, p0, Lcom/google/r/b/a/anr;->j:B

    .line 333
    iput v1, p0, Lcom/google/r/b/a/anr;->k:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/anr;
    .locals 1

    .prologue
    .line 1069
    sget-object v0, Lcom/google/r/b/a/anr;->i:Lcom/google/r/b/a/anr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ant;
    .locals 1

    .prologue
    .line 446
    new-instance v0, Lcom/google/r/b/a/ant;

    invoke-direct {v0}, Lcom/google/r/b/a/ant;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/anr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/google/r/b/a/anr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 308
    invoke-virtual {p0}, Lcom/google/r/b/a/anr;->c()I

    .line 309
    iget v0, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/r/b/a/anr;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 312
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 313
    iget-object v0, p0, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_1
    move v0, v1

    .line 315
    :goto_0
    iget-object v2, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 316
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 315
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 319
    iget-object v0, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 318
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 321
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 322
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/anr;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anr;->g:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 324
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 325
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/anr;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 327
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    .line 328
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/r/b/a/anr;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 330
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/anr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 331
    return-void

    .line 322
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 292
    iget-byte v0, p0, Lcom/google/r/b/a/anr;->j:B

    .line 293
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 303
    :goto_0
    return v0

    .line 294
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 296
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 297
    iget-object v0, p0, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 298
    iput-byte v2, p0, Lcom/google/r/b/a/anr;->j:B

    move v0, v2

    .line 299
    goto :goto_0

    :cond_2
    move v0, v2

    .line 296
    goto :goto_1

    .line 302
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/anr;->j:B

    move v0, v1

    .line 303
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 335
    iget v0, p0, Lcom/google/r/b/a/anr;->k:I

    .line 336
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 379
    :goto_0
    return v0

    .line 339
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 340
    iget-object v0, p0, Lcom/google/r/b/a/anr;->b:Lcom/google/n/ao;

    .line 341
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 343
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 344
    iget-object v2, p0, Lcom/google/r/b/a/anr;->f:Lcom/google/n/ao;

    .line 345
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v1

    .line 349
    :goto_2
    iget-object v4, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 350
    iget-object v4, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    .line 351
    invoke-interface {v4, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 349
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 353
    :cond_2
    add-int/2addr v0, v3

    .line 354
    iget-object v2, p0, Lcom/google/r/b/a/anr;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v0, v1

    move v2, v1

    .line 358
    :goto_3
    iget-object v4, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 359
    iget-object v4, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    .line 360
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 358
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 362
    :cond_3
    add-int v0, v3, v2

    .line 363
    iget-object v2, p0, Lcom/google/r/b/a/anr;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 365
    iget v0, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_8

    .line 366
    const/4 v3, 0x5

    .line 367
    iget-object v0, p0, Lcom/google/r/b/a/anr;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/anr;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 369
    :goto_5
    iget v2, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 370
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/r/b/a/anr;->h:I

    .line 371
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_6
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 373
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/anr;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_5

    .line 374
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/r/b/a/anr;->c:Lcom/google/n/ao;

    .line 375
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 377
    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/anr;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 378
    iput v0, p0, Lcom/google/r/b/a/anr;->k:I

    goto/16 :goto_0

    .line 367
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 371
    :cond_7
    const/16 v2, 0xa

    goto :goto_6

    :cond_8
    move v0, v2

    goto :goto_5

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/anr;->newBuilder()Lcom/google/r/b/a/ant;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ant;->a(Lcom/google/r/b/a/anr;)Lcom/google/r/b/a/ant;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/anr;->newBuilder()Lcom/google/r/b/a/ant;

    move-result-object v0

    return-object v0
.end method
