.class public final Lcom/google/r/b/a/ale;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/alf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/alc;",
        "Lcom/google/r/b/a/ale;",
        ">;",
        "Lcom/google/r/b/a/alf;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1267
    sget-object v0, Lcom/google/r/b/a/alc;->e:Lcom/google/r/b/a/alc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1331
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ale;->b:Ljava/lang/Object;

    .line 1407
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ale;->c:Ljava/lang/Object;

    .line 1483
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ale;->d:Ljava/lang/Object;

    .line 1268
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1259
    new-instance v2, Lcom/google/r/b/a/alc;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/alc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ale;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/ale;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/alc;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ale;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/alc;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ale;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/alc;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/alc;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1259
    check-cast p1, Lcom/google/r/b/a/alc;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ale;->a(Lcom/google/r/b/a/alc;)Lcom/google/r/b/a/ale;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/alc;)Lcom/google/r/b/a/ale;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1305
    invoke-static {}, Lcom/google/r/b/a/alc;->d()Lcom/google/r/b/a/alc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1322
    :goto_0
    return-object p0

    .line 1306
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/alc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1307
    iget v2, p0, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ale;->a:I

    .line 1308
    iget-object v2, p1, Lcom/google/r/b/a/alc;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ale;->b:Ljava/lang/Object;

    .line 1311
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/alc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1312
    iget v2, p0, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ale;->a:I

    .line 1313
    iget-object v2, p1, Lcom/google/r/b/a/alc;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ale;->c:Ljava/lang/Object;

    .line 1316
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/alc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 1317
    iget v0, p0, Lcom/google/r/b/a/ale;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ale;->a:I

    .line 1318
    iget-object v0, p1, Lcom/google/r/b/a/alc;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/ale;->d:Ljava/lang/Object;

    .line 1321
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/alc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 1306
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1311
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1316
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1326
    const/4 v0, 0x1

    return v0
.end method
