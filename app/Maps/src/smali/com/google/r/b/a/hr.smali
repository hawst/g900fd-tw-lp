.class public final Lcom/google/r/b/a/hr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/hu;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/hr;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/r/b/a/hr;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/aq;

.field c:Lcom/google/n/aq;

.field d:Lcom/google/n/aq;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/n/ao;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Ljava/lang/Object;

.field l:I

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    new-instance v0, Lcom/google/r/b/a/hs;

    invoke-direct {v0}, Lcom/google/r/b/a/hs;-><init>()V

    sput-object v0, Lcom/google/r/b/a/hr;->PARSER:Lcom/google/n/ax;

    .line 607
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/hr;->p:Lcom/google/n/aw;

    .line 1790
    new-instance v0, Lcom/google/r/b/a/hr;

    invoke-direct {v0}, Lcom/google/r/b/a/hr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/hr;->m:Lcom/google/r/b/a/hr;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 305
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/hr;->f:Lcom/google/n/ao;

    .line 364
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    .line 380
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    .line 396
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    .line 468
    iput-byte v3, p0, Lcom/google/r/b/a/hr;->n:B

    .line 535
    iput v3, p0, Lcom/google/r/b/a/hr;->o:I

    .line 18
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    .line 19
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    .line 20
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    .line 22
    iget-object v0, p0, Lcom/google/r/b/a/hr;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/hr;->k:Ljava/lang/Object;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/hr;->l:I

    .line 29
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/r/b/a/hr;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 41
    :cond_0
    :goto_0
    if-nez v0, :cond_b

    .line 42
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 43
    sparse-switch v4, :sswitch_data_0

    .line 48
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 50
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 46
    goto :goto_0

    .line 55
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 56
    and-int/lit8 v5, v1, 0x1

    if-eq v5, v2, :cond_1

    .line 57
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    .line 58
    or-int/lit8 v1, v1, 0x1

    .line 60
    :cond_1
    iget-object v5, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_2

    .line 141
    iget-object v2, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    .line 143
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_3

    .line 144
    iget-object v2, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    .line 146
    :cond_3
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v8, :cond_4

    .line 147
    iget-object v2, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    .line 149
    :cond_4
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v9, :cond_5

    .line 150
    iget-object v2, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    .line 152
    :cond_5
    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 153
    iget-object v1, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    .line 155
    :cond_6
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/hr;->au:Lcom/google/n/bn;

    throw v0

    .line 64
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 65
    and-int/lit8 v5, v1, 0x2

    if-eq v5, v7, :cond_7

    .line 66
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    .line 67
    or-int/lit8 v1, v1, 0x2

    .line 69
    :cond_7
    iget-object v5, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 136
    :catch_1
    move-exception v0

    .line 137
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 138
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 73
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 74
    and-int/lit8 v5, v1, 0x4

    if-eq v5, v8, :cond_8

    .line 75
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    .line 76
    or-int/lit8 v1, v1, 0x4

    .line 78
    :cond_8
    iget-object v5, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 82
    :sswitch_4
    and-int/lit8 v4, v1, 0x8

    if-eq v4, v9, :cond_9

    .line 83
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    .line 85
    or-int/lit8 v1, v1, 0x8

    .line 87
    :cond_9
    iget-object v4, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 87
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 92
    :sswitch_5
    iget-object v4, p0, Lcom/google/r/b/a/hr;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 93
    iget v4, p0, Lcom/google/r/b/a/hr;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/hr;->a:I

    goto/16 :goto_0

    .line 97
    :sswitch_6
    and-int/lit8 v4, v1, 0x20

    const/16 v5, 0x20

    if-eq v4, v5, :cond_a

    .line 98
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    .line 100
    or-int/lit8 v1, v1, 0x20

    .line 102
    :cond_a
    iget-object v4, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 103
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 102
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 107
    :sswitch_7
    iget-object v4, p0, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 108
    iget v4, p0, Lcom/google/r/b/a/hr;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/hr;->a:I

    goto/16 :goto_0

    .line 112
    :sswitch_8
    iget-object v4, p0, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 113
    iget v4, p0, Lcom/google/r/b/a/hr;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/hr;->a:I

    goto/16 :goto_0

    .line 117
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 118
    iget v5, p0, Lcom/google/r/b/a/hr;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/r/b/a/hr;->a:I

    .line 119
    iput-object v4, p0, Lcom/google/r/b/a/hr;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 123
    :sswitch_a
    iget-object v4, p0, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 124
    iget v4, p0, Lcom/google/r/b/a/hr;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/hr;->a:I

    goto/16 :goto_0

    .line 128
    :sswitch_b
    iget v4, p0, Lcom/google/r/b/a/hr;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/hr;->a:I

    .line 129
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/hr;->l:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 140
    :cond_b
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_c

    .line 141
    iget-object v0, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    .line 143
    :cond_c
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_d

    .line 144
    iget-object v0, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    .line 146
    :cond_d
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v8, :cond_e

    .line 147
    iget-object v0, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    .line 149
    :cond_e
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v9, :cond_f

    .line 150
    iget-object v0, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    .line 152
    :cond_f
    and-int/lit8 v0, v1, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_10

    .line 153
    iget-object v0, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    .line 155
    :cond_10
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->au:Lcom/google/n/bn;

    .line 156
    return-void

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 305
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/hr;->f:Lcom/google/n/ao;

    .line 364
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    .line 380
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    .line 396
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    .line 468
    iput-byte v1, p0, Lcom/google/r/b/a/hr;->n:B

    .line 535
    iput v1, p0, Lcom/google/r/b/a/hr;->o:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/hr;
    .locals 1

    .prologue
    .line 1793
    sget-object v0, Lcom/google/r/b/a/hr;->m:Lcom/google/r/b/a/hr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ht;
    .locals 1

    .prologue
    .line 669
    new-instance v0, Lcom/google/r/b/a/ht;

    invoke-direct {v0}, Lcom/google/r/b/a/ht;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/hr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    sget-object v0, Lcom/google/r/b/a/hr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 498
    invoke-virtual {p0}, Lcom/google/r/b/a/hr;->c()I

    move v0, v1

    .line 499
    :goto_0
    iget-object v2, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 500
    iget-object v2, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 499
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 502
    :goto_1
    iget-object v2, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 503
    iget-object v2, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v5, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 502
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 505
    :goto_2
    iget-object v2, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 506
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 505
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v2, v1

    .line 508
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 509
    iget-object v0, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 508
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 511
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 512
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/r/b/a/hr;->f:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 514
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 515
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 514
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 517
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_6

    .line 518
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 520
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_7

    .line 521
    iget-object v0, p0, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v7, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 523
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 524
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/r/b/a/hr;->k:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->k:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 526
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_9

    .line 527
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 529
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 530
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/r/b/a/hr;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 532
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/hr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 533
    return-void

    .line 524
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 470
    iget-byte v0, p0, Lcom/google/r/b/a/hr;->n:B

    .line 471
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 493
    :goto_0
    return v0

    .line 472
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 474
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 475
    iget-object v0, p0, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 476
    iput-byte v2, p0, Lcom/google/r/b/a/hr;->n:B

    move v0, v2

    .line 477
    goto :goto_0

    :cond_2
    move v0, v2

    .line 474
    goto :goto_1

    .line 480
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 481
    iget-object v0, p0, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 482
    iput-byte v2, p0, Lcom/google/r/b/a/hr;->n:B

    move v0, v2

    .line 483
    goto :goto_0

    :cond_4
    move v0, v2

    .line 480
    goto :goto_2

    .line 486
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 487
    iget-object v0, p0, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 488
    iput-byte v2, p0, Lcom/google/r/b/a/hr;->n:B

    move v0, v2

    .line 489
    goto :goto_0

    :cond_6
    move v0, v2

    .line 486
    goto :goto_3

    .line 492
    :cond_7
    iput-byte v1, p0, Lcom/google/r/b/a/hr;->n:B

    move v0, v1

    .line 493
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v4, 0xa

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x0

    .line 537
    iget v0, p0, Lcom/google/r/b/a/hr;->o:I

    .line 538
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 602
    :goto_0
    return v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 543
    :goto_1
    iget-object v3, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 544
    iget-object v3, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    .line 545
    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v5

    add-int/2addr v2, v3

    .line 543
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 547
    :cond_1
    add-int/lit8 v0, v2, 0x0

    .line 548
    iget-object v2, p0, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v0, v1

    move v2, v1

    .line 552
    :goto_2
    iget-object v5, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 553
    iget-object v5, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    .line 554
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 552
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 556
    :cond_2
    add-int v0, v3, v2

    .line 557
    iget-object v2, p0, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v0, v1

    move v2, v1

    .line 561
    :goto_3
    iget-object v5, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_3

    .line 562
    iget-object v5, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    .line 563
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 561
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 565
    :cond_3
    add-int v0, v3, v2

    .line 566
    iget-object v2, p0, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v1

    move v3, v0

    .line 568
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 569
    iget-object v0, p0, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    .line 570
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 568
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 572
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    .line 573
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/r/b/a/hr;->f:Lcom/google/n/ao;

    .line 574
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_5
    move v2, v1

    .line 576
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 577
    const/4 v5, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    .line 578
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 576
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 580
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    .line 581
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    .line 582
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 584
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_8

    .line 585
    iget-object v0, p0, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    .line 586
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 588
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_9

    .line 589
    const/16 v2, 0x9

    .line 590
    iget-object v0, p0, Lcom/google/r/b/a/hr;->k:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hr;->k:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 592
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_a

    .line 593
    iget-object v0, p0, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    .line 594
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 596
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_b

    .line 597
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/r/b/a/hr;->l:I

    .line 598
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v2, :cond_d

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 600
    :cond_b
    iget-object v0, p0, Lcom/google/r/b/a/hr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 601
    iput v0, p0, Lcom/google/r/b/a/hr;->o:I

    goto/16 :goto_0

    .line 590
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_d
    move v0, v4

    .line 598
    goto :goto_7
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/hr;->newBuilder()Lcom/google/r/b/a/ht;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ht;->a(Lcom/google/r/b/a/hr;)Lcom/google/r/b/a/ht;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/hr;->newBuilder()Lcom/google/r/b/a/ht;

    move-result-object v0

    return-object v0
.end method
