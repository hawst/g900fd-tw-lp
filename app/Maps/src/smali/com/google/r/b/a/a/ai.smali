.class public final enum Lcom/google/r/b/a/a/ai;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/a/ai;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/a/ai;

.field public static final enum b:Lcom/google/r/b/a/a/ai;

.field public static final enum c:Lcom/google/r/b/a/a/ai;

.field public static final enum d:Lcom/google/r/b/a/a/ai;

.field public static final enum e:Lcom/google/r/b/a/a/ai;

.field private static final synthetic g:[Lcom/google/r/b/a/a/ai;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 660
    new-instance v0, Lcom/google/r/b/a/a/ai;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/a/ai;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    .line 664
    new-instance v0, Lcom/google/r/b/a/a/ai;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/a/ai;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a/ai;->b:Lcom/google/r/b/a/a/ai;

    .line 668
    new-instance v0, Lcom/google/r/b/a/a/ai;

    const-string v1, "TO_BE_ADDED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/a/ai;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    .line 672
    new-instance v0, Lcom/google/r/b/a/a/ai;

    const-string v1, "TO_BE_REMOVED"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/a/ai;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a/ai;->d:Lcom/google/r/b/a/a/ai;

    .line 676
    new-instance v0, Lcom/google/r/b/a/a/ai;

    const-string v1, "TILE_BROKEN"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/a/ai;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a/ai;->e:Lcom/google/r/b/a/a/ai;

    .line 655
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/r/b/a/a/ai;

    sget-object v1, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/a/ai;->b:Lcom/google/r/b/a/a/ai;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/a/ai;->d:Lcom/google/r/b/a/a/ai;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/a/ai;->e:Lcom/google/r/b/a/a/ai;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/r/b/a/a/ai;->g:[Lcom/google/r/b/a/a/ai;

    .line 721
    new-instance v0, Lcom/google/r/b/a/a/aj;

    invoke-direct {v0}, Lcom/google/r/b/a/a/aj;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 730
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 731
    iput p3, p0, Lcom/google/r/b/a/a/ai;->f:I

    .line 732
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/a/ai;
    .locals 1

    .prologue
    .line 706
    packed-switch p0, :pswitch_data_0

    .line 712
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 707
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    goto :goto_0

    .line 708
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/a/ai;->b:Lcom/google/r/b/a/a/ai;

    goto :goto_0

    .line 709
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    goto :goto_0

    .line 710
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/a/ai;->d:Lcom/google/r/b/a/a/ai;

    goto :goto_0

    .line 711
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/a/ai;->e:Lcom/google/r/b/a/a/ai;

    goto :goto_0

    .line 706
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/a/ai;
    .locals 1

    .prologue
    .line 655
    const-class v0, Lcom/google/r/b/a/a/ai;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/ai;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/a/ai;
    .locals 1

    .prologue
    .line 655
    sget-object v0, Lcom/google/r/b/a/a/ai;->g:[Lcom/google/r/b/a/a/ai;

    invoke-virtual {v0}, [Lcom/google/r/b/a/a/ai;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/a/ai;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 702
    iget v0, p0, Lcom/google/r/b/a/a/ai;->f:I

    return v0
.end method
