.class public final Lcom/google/r/b/a/ql;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/qj;",
        "Lcom/google/r/b/a/ql;",
        ">;",
        "Lcom/google/r/b/a/qm;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Z

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 7738
    sget-object v0, Lcom/google/r/b/a/qj;->g:Lcom/google/r/b/a/qj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 7844
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    .line 7980
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ql;->c:Lcom/google/n/ao;

    .line 8039
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ql;->f:Lcom/google/n/ao;

    .line 8098
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ql;->d:Lcom/google/n/ao;

    .line 8157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/ql;->e:Z

    .line 7739
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7730
    new-instance v2, Lcom/google/r/b/a/qj;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/qj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ql;->a:I

    iget v4, p0, Lcom/google/r/b/a/ql;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/ql;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/ql;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ql;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ql;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/qj;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ql;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ql;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/qj;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ql;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ql;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-boolean v1, p0, Lcom/google/r/b/a/ql;->e:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/qj;->f:Z

    iput v0, v2, Lcom/google/r/b/a/qj;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 7730
    check-cast p1, Lcom/google/r/b/a/qj;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ql;->a(Lcom/google/r/b/a/qj;)Lcom/google/r/b/a/ql;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/qj;)Lcom/google/r/b/a/ql;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 7795
    invoke-static {}, Lcom/google/r/b/a/qj;->d()Lcom/google/r/b/a/qj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 7822
    :goto_0
    return-object p0

    .line 7796
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 7797
    iget-object v2, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 7798
    iget-object v2, p1, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    .line 7799
    iget v2, p0, Lcom/google/r/b/a/ql;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/ql;->a:I

    .line 7806
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 7807
    iget-object v2, p0, Lcom/google/r/b/a/ql;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 7808
    iget v2, p0, Lcom/google/r/b/a/ql;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ql;->a:I

    .line 7810
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 7811
    iget-object v2, p0, Lcom/google/r/b/a/ql;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/qj;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 7812
    iget v2, p0, Lcom/google/r/b/a/ql;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/ql;->a:I

    .line 7814
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 7815
    iget-object v2, p0, Lcom/google/r/b/a/ql;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/qj;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 7816
    iget v2, p0, Lcom/google/r/b/a/ql;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/ql;->a:I

    .line 7818
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 7819
    iget-boolean v0, p1, Lcom/google/r/b/a/qj;->f:Z

    iget v1, p0, Lcom/google/r/b/a/ql;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/r/b/a/ql;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/ql;->e:Z

    .line 7821
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/qj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 7801
    :cond_6
    invoke-virtual {p0}, Lcom/google/r/b/a/ql;->c()V

    .line 7802
    iget-object v2, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_7
    move v2, v1

    .line 7806
    goto :goto_2

    :cond_8
    move v2, v1

    .line 7810
    goto :goto_3

    :cond_9
    move v2, v1

    .line 7814
    goto :goto_4

    :cond_a
    move v0, v1

    .line 7818
    goto :goto_5
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7826
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 7827
    iget-object v0, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/qf;->d()Lcom/google/r/b/a/qf;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/qf;

    invoke-virtual {v0}, Lcom/google/r/b/a/qf;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 7838
    :cond_0
    :goto_1
    return v2

    .line 7826
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 7832
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ql;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 7833
    iget-object v0, p0, Lcom/google/r/b/a/ql;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/qr;->d()Lcom/google/r/b/a/qr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/qr;

    invoke-virtual {v0}, Lcom/google/r/b/a/qr;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v3

    .line 7838
    goto :goto_1

    :cond_4
    move v0, v2

    .line 7832
    goto :goto_2
.end method

.method public c()V
    .locals 2

    .prologue
    .line 7846
    iget v0, p0, Lcom/google/r/b/a/ql;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 7847
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ql;->b:Ljava/util/List;

    .line 7850
    iget v0, p0, Lcom/google/r/b/a/ql;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ql;->a:I

    .line 7852
    :cond_0
    return-void
.end method
