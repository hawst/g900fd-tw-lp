.class public final Lcom/google/r/b/a/qt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/qr;",
        "Lcom/google/r/b/a/qt;",
        ">;",
        "Lcom/google/r/b/a/qu;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3991
    sget-object v0, Lcom/google/r/b/a/qr;->d:Lcom/google/r/b/a/qr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4052
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qt;->b:Lcom/google/n/ao;

    .line 4111
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qt;->c:Lcom/google/n/ao;

    .line 3992
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3983
    new-instance v2, Lcom/google/r/b/a/qr;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/qr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/qt;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/qr;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/qt;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/qt;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v3, v2, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/qt;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/qt;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/qr;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3983
    check-cast p1, Lcom/google/r/b/a/qr;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/qt;->a(Lcom/google/r/b/a/qr;)Lcom/google/r/b/a/qt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/qr;)Lcom/google/r/b/a/qt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4027
    invoke-static {}, Lcom/google/r/b/a/qr;->d()Lcom/google/r/b/a/qr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4037
    :goto_0
    return-object p0

    .line 4028
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/qr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4029
    iget-object v2, p0, Lcom/google/r/b/a/qt;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/qr;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4030
    iget v2, p0, Lcom/google/r/b/a/qt;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/qt;->a:I

    .line 4032
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/qr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 4033
    iget-object v0, p0, Lcom/google/r/b/a/qt;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/qr;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4034
    iget v0, p0, Lcom/google/r/b/a/qt;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/qt;->a:I

    .line 4036
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/qr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 4028
    goto :goto_1

    :cond_4
    move v0, v1

    .line 4032
    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 4041
    iget v0, p0, Lcom/google/r/b/a/qt;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 4042
    iget-object v0, p0, Lcom/google/r/b/a/qt;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ana;->d()Lcom/google/r/b/a/ana;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ana;

    invoke-virtual {v0}, Lcom/google/r/b/a/ana;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 4047
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 4041
    goto :goto_0

    :cond_1
    move v0, v2

    .line 4047
    goto :goto_1
.end method
