.class public final enum Lcom/google/r/b/a/ka;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ka;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ka;

.field public static final enum b:Lcom/google/r/b/a/ka;

.field public static final enum c:Lcom/google/r/b/a/ka;

.field private static final synthetic e:[Lcom/google/r/b/a/ka;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 130
    new-instance v0, Lcom/google/r/b/a/ka;

    const-string v1, "DELETE_LOCATION_HISTORY_DURATION"

    invoke-direct {v0, v1, v3, v6}, Lcom/google/r/b/a/ka;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ka;->a:Lcom/google/r/b/a/ka;

    .line 131
    new-instance v0, Lcom/google/r/b/a/ka;

    const-string v1, "DELETE_ALL_LOCATION_HISTORY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, Lcom/google/r/b/a/ka;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ka;->b:Lcom/google/r/b/a/ka;

    .line 132
    new-instance v0, Lcom/google/r/b/a/ka;

    const-string v1, "DELETELOCATIONHISTORYOPTIONS_NOT_SET"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/r/b/a/ka;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ka;->c:Lcom/google/r/b/a/ka;

    .line 128
    new-array v0, v6, [Lcom/google/r/b/a/ka;

    sget-object v1, Lcom/google/r/b/a/ka;->a:Lcom/google/r/b/a/ka;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/ka;->b:Lcom/google/r/b/a/ka;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/ka;->c:Lcom/google/r/b/a/ka;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/ka;->e:[Lcom/google/r/b/a/ka;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 133
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ka;->d:I

    .line 135
    iput p3, p0, Lcom/google/r/b/a/ka;->d:I

    .line 136
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ka;
    .locals 2

    .prologue
    .line 138
    packed-switch p0, :pswitch_data_0

    .line 142
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value is undefined for this oneof enum."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ka;->a:Lcom/google/r/b/a/ka;

    .line 141
    :goto_0
    return-object v0

    .line 140
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/ka;->b:Lcom/google/r/b/a/ka;

    goto :goto_0

    .line 141
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/ka;->c:Lcom/google/r/b/a/ka;

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ka;
    .locals 1

    .prologue
    .line 128
    const-class v0, Lcom/google/r/b/a/ka;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ka;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ka;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/google/r/b/a/ka;->e:[Lcom/google/r/b/a/ka;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ka;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ka;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/google/r/b/a/ka;->d:I

    return v0
.end method
