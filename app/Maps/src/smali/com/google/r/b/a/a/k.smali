.class public final Lcom/google/r/b/a/a/k;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/n;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/k;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/a/k;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113
    new-instance v0, Lcom/google/r/b/a/a/l;

    invoke-direct {v0}, Lcom/google/r/b/a/a/l;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/k;->PARSER:Lcom/google/n/ax;

    .line 1226
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/k;->h:Lcom/google/n/aw;

    .line 1472
    new-instance v0, Lcom/google/r/b/a/a/k;

    invoke-direct {v0}, Lcom/google/r/b/a/a/k;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/k;->e:Lcom/google/r/b/a/a/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 1046
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1176
    iput-byte v0, p0, Lcom/google/r/b/a/a/k;->f:B

    .line 1201
    iput v0, p0, Lcom/google/r/b/a/a/k;->g:I

    .line 1047
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/a/k;->b:I

    .line 1048
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/a/k;->c:I

    .line 1049
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/a/k;->d:J

    .line 1050
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1056
    invoke-direct {p0}, Lcom/google/r/b/a/a/k;-><init>()V

    .line 1057
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1061
    const/4 v0, 0x0

    .line 1062
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 1063
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1064
    sparse-switch v3, :sswitch_data_0

    .line 1069
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1071
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1067
    goto :goto_0

    .line 1076
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1077
    invoke-static {v3}, Lcom/google/maps/g/a/fr;->a(I)Lcom/google/maps/g/a/fr;

    move-result-object v4

    .line 1078
    if-nez v4, :cond_1

    .line 1079
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1104
    :catch_0
    move-exception v0

    .line 1105
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1110
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/k;->au:Lcom/google/n/bn;

    throw v0

    .line 1081
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/a/k;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/a/k;->a:I

    .line 1082
    iput v3, p0, Lcom/google/r/b/a/a/k;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1106
    :catch_1
    move-exception v0

    .line 1107
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1108
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1087
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1088
    invoke-static {v3}, Lcom/google/maps/g/a/fu;->a(I)Lcom/google/maps/g/a/fu;

    move-result-object v4

    .line 1089
    if-nez v4, :cond_2

    .line 1090
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 1092
    :cond_2
    iget v4, p0, Lcom/google/r/b/a/a/k;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/a/k;->a:I

    .line 1093
    iput v3, p0, Lcom/google/r/b/a/a/k;->c:I

    goto :goto_0

    .line 1098
    :sswitch_3
    iget v3, p0, Lcom/google/r/b/a/a/k;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/a/k;->a:I

    .line 1099
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/a/k;->d:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1110
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/k;->au:Lcom/google/n/bn;

    .line 1111
    return-void

    .line 1064
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1044
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1176
    iput-byte v0, p0, Lcom/google/r/b/a/a/k;->f:B

    .line 1201
    iput v0, p0, Lcom/google/r/b/a/a/k;->g:I

    .line 1045
    return-void
.end method

.method public static d()Lcom/google/r/b/a/a/k;
    .locals 1

    .prologue
    .line 1475
    sget-object v0, Lcom/google/r/b/a/a/k;->e:Lcom/google/r/b/a/a/k;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/m;
    .locals 1

    .prologue
    .line 1288
    new-instance v0, Lcom/google/r/b/a/a/m;

    invoke-direct {v0}, Lcom/google/r/b/a/a/m;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1125
    sget-object v0, Lcom/google/r/b/a/a/k;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1188
    invoke-virtual {p0}, Lcom/google/r/b/a/a/k;->c()I

    .line 1189
    iget v0, p0, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1190
    iget v0, p0, Lcom/google/r/b/a/a/k;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 1192
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1193
    iget v0, p0, Lcom/google/r/b/a/a/k;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 1195
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1196
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/r/b/a/a/k;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 1198
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/a/k;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1199
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1178
    iget-byte v1, p0, Lcom/google/r/b/a/a/k;->f:B

    .line 1179
    if-ne v1, v0, :cond_0

    .line 1183
    :goto_0
    return v0

    .line 1180
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1182
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/a/k;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1203
    iget v0, p0, Lcom/google/r/b/a/a/k;->g:I

    .line 1204
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1221
    :goto_0
    return v0

    .line 1207
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 1208
    iget v0, p0, Lcom/google/r/b/a/a/k;->b:I

    .line 1209
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 1211
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 1212
    iget v3, p0, Lcom/google/r/b/a/a/k;->c:I

    .line 1213
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 1215
    :cond_2
    iget v1, p0, Lcom/google/r/b/a/a/k;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_3

    .line 1216
    const/4 v1, 0x3

    iget-wide v4, p0, Lcom/google/r/b/a/a/k;->d:J

    .line 1217
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1219
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/a/k;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1220
    iput v0, p0, Lcom/google/r/b/a/a/k;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1209
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1038
    invoke-static {}, Lcom/google/r/b/a/a/k;->newBuilder()Lcom/google/r/b/a/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/m;->a(Lcom/google/r/b/a/a/k;)Lcom/google/r/b/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1038
    invoke-static {}, Lcom/google/r/b/a/a/k;->newBuilder()Lcom/google/r/b/a/a/m;

    move-result-object v0

    return-object v0
.end method
