.class public final Lcom/google/r/b/a/adi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/adn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/adi;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/adi;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/n/ao;

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11831
    new-instance v0, Lcom/google/r/b/a/adj;

    invoke-direct {v0}, Lcom/google/r/b/a/adj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/adi;->PARSER:Lcom/google/n/ax;

    .line 12386
    new-instance v0, Lcom/google/r/b/a/adk;

    invoke-direct {v0}, Lcom/google/r/b/a/adk;-><init>()V

    .line 12417
    new-instance v0, Lcom/google/r/b/a/adl;

    invoke-direct {v0}, Lcom/google/r/b/a/adl;-><init>()V

    .line 12543
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/adi;->i:Lcom/google/n/aw;

    .line 12952
    new-instance v0, Lcom/google/r/b/a/adi;

    invoke-direct {v0}, Lcom/google/r/b/a/adi;-><init>()V

    sput-object v0, Lcom/google/r/b/a/adi;->f:Lcom/google/r/b/a/adi;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 11708
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 12446
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/adi;->d:Lcom/google/n/ao;

    .line 12476
    iput-byte v2, p0, Lcom/google/r/b/a/adi;->g:B

    .line 12504
    iput v2, p0, Lcom/google/r/b/a/adi;->h:I

    .line 11709
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    .line 11710
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    .line 11711
    iget-object v0, p0, Lcom/google/r/b/a/adi;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 11712
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/r/b/a/adi;->e:I

    .line 11713
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v8, 0x2

    const/4 v4, 0x1

    .line 11719
    invoke-direct {p0}, Lcom/google/r/b/a/adi;-><init>()V

    .line 11722
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 11725
    :cond_0
    :goto_0
    if-nez v3, :cond_c

    .line 11726
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 11727
    sparse-switch v1, :sswitch_data_0

    .line 11732
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 11734
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 11730
    goto :goto_0

    .line 11739
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    .line 11740
    invoke-static {v6}, Lcom/google/maps/g/f/i;->a(I)Lcom/google/maps/g/f/i;

    move-result-object v1

    .line 11741
    if-nez v1, :cond_3

    .line 11742
    const/4 v1, 0x1

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 11816
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 11817
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 11822
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_1

    .line 11823
    iget-object v2, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    .line 11825
    :cond_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v8, :cond_2

    .line 11826
    iget-object v1, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    .line 11828
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/adi;->au:Lcom/google/n/bn;

    throw v0

    .line 11744
    :cond_3
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v4, :cond_11

    .line 11745
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 11746
    or-int/lit8 v1, v0, 0x1

    .line 11748
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 11750
    goto :goto_0

    .line 11753
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 11754
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v6

    move v1, v0

    .line 11755
    :goto_4
    :try_start_5
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_4

    move v0, v2

    :goto_5
    if-lez v0, :cond_7

    .line 11756
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 11757
    invoke-static {v0}, Lcom/google/maps/g/f/i;->a(I)Lcom/google/maps/g/f/i;

    move-result-object v7

    .line 11758
    if-nez v7, :cond_5

    .line 11759
    const/4 v7, 0x1

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_4

    .line 11816
    :catch_1
    move-exception v0

    goto :goto_1

    .line 11755
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_5

    .line 11761
    :cond_5
    and-int/lit8 v7, v1, 0x1

    if-eq v7, v4, :cond_6

    .line 11762
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    .line 11763
    or-int/lit8 v1, v1, 0x1

    .line 11765
    :cond_6
    iget-object v7, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 11818
    :catch_2
    move-exception v0

    .line 11819
    :goto_6
    :try_start_6
    new-instance v2, Lcom/google/n/ak;

    .line 11820
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 11768
    :cond_7
    :try_start_7
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 11769
    goto/16 :goto_0

    .line 11772
    :sswitch_3
    :try_start_8
    iget-object v1, p0, Lcom/google/r/b/a/adi;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 11773
    iget v1, p0, Lcom/google/r/b/a/adi;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/adi;->a:I

    goto/16 :goto_0

    .line 11818
    :catch_3
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_6

    .line 11777
    :sswitch_4
    iget v1, p0, Lcom/google/r/b/a/adi;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/adi;->a:I

    .line 11778
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/adi;->e:I

    goto/16 :goto_0

    .line 11822
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto/16 :goto_2

    .line 11782
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v6

    .line 11783
    invoke-static {v6}, Lcom/google/r/b/a/aiy;->a(I)Lcom/google/r/b/a/aiy;

    move-result-object v1

    .line 11784
    if-nez v1, :cond_8

    .line 11785
    const/4 v1, 0x4

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 11787
    :cond_8
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v8, :cond_10

    .line 11788
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 11789
    or-int/lit8 v1, v0, 0x2

    .line 11791
    :goto_7
    :try_start_9
    iget-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 11793
    goto/16 :goto_0

    .line 11796
    :sswitch_6
    :try_start_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 11797
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 11798
    :goto_8
    iget v1, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v1, v7, :cond_9

    move v1, v2

    :goto_9
    if-lez v1, :cond_b

    .line 11799
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v7

    .line 11800
    invoke-static {v7}, Lcom/google/r/b/a/aiy;->a(I)Lcom/google/r/b/a/aiy;

    move-result-object v1

    .line 11801
    if-nez v1, :cond_a

    .line 11802
    const/4 v1, 0x4

    invoke-virtual {v5, v1, v7}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_8

    .line 11798
    :cond_9
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_9

    .line 11804
    :cond_a
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v8, :cond_f

    .line 11805
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 11806
    or-int/lit8 v1, v0, 0x2

    .line 11808
    :goto_a
    :try_start_b
    iget-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v1

    .line 11810
    goto :goto_8

    .line 11811
    :cond_b
    :try_start_c
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 11822
    :cond_c
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v4, :cond_d

    .line 11823
    iget-object v1, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    .line 11825
    :cond_d
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_e

    .line 11826
    iget-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    .line 11828
    :cond_e
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adi;->au:Lcom/google/n/bn;

    .line 11829
    return-void

    :cond_f
    move v1, v0

    goto :goto_a

    :cond_10
    move v1, v0

    goto :goto_7

    :cond_11
    move v1, v0

    goto/16 :goto_3

    .line 11727
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x20 -> :sswitch_5
        0x22 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 11706
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 12446
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/adi;->d:Lcom/google/n/ao;

    .line 12476
    iput-byte v1, p0, Lcom/google/r/b/a/adi;->g:B

    .line 12504
    iput v1, p0, Lcom/google/r/b/a/adi;->h:I

    .line 11707
    return-void
.end method

.method public static d()Lcom/google/r/b/a/adi;
    .locals 1

    .prologue
    .line 12955
    sget-object v0, Lcom/google/r/b/a/adi;->f:Lcom/google/r/b/a/adi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/adm;
    .locals 1

    .prologue
    .line 12605
    new-instance v0, Lcom/google/r/b/a/adm;

    invoke-direct {v0}, Lcom/google/r/b/a/adm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/adi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11843
    sget-object v0, Lcom/google/r/b/a/adi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12488
    invoke-virtual {p0}, Lcom/google/r/b/a/adi;->c()I

    move v1, v2

    .line 12489
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 12490
    iget-object v0, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 12489
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 12492
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/adi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 12493
    iget-object v0, p0, Lcom/google/r/b/a/adi;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 12495
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/adi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 12496
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/adi;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 12498
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 12499
    const/4 v1, 0x4

    iget-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 12498
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 12501
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/adi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12502
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 12478
    iget-byte v1, p0, Lcom/google/r/b/a/adi;->g:B

    .line 12479
    if-ne v1, v0, :cond_0

    .line 12483
    :goto_0
    return v0

    .line 12480
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 12482
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/adi;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 12506
    iget v0, p0, Lcom/google/r/b/a/adi;->h:I

    .line 12507
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 12538
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 12512
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 12513
    iget-object v0, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    .line 12514
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    .line 12512
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v4

    .line 12514
    goto :goto_2

    .line 12516
    :cond_2
    add-int/lit8 v0, v3, 0x0

    .line 12517
    iget-object v1, p0, Lcom/google/r/b/a/adi;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12519
    iget v1, p0, Lcom/google/r/b/a/adi;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    .line 12520
    iget-object v1, p0, Lcom/google/r/b/a/adi;->d:Lcom/google/n/ao;

    .line 12521
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 12523
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/adi;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v5, :cond_7

    .line 12524
    const/4 v1, 0x3

    iget v3, p0, Lcom/google/r/b/a/adi;->e:I

    .line 12525
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_3
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    move v1, v0

    :goto_4
    move v3, v2

    .line 12529
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 12530
    iget-object v0, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    .line 12531
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v3

    .line 12529
    add-int/lit8 v2, v2, 0x1

    move v3, v0

    goto :goto_5

    :cond_4
    move v1, v4

    .line 12525
    goto :goto_3

    :cond_5
    move v0, v4

    .line 12531
    goto :goto_6

    .line 12533
    :cond_6
    add-int v0, v1, v3

    .line 12534
    iget-object v1, p0, Lcom/google/r/b/a/adi;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12536
    iget-object v1, p0, Lcom/google/r/b/a/adi;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 12537
    iput v0, p0, Lcom/google/r/b/a/adi;->h:I

    goto/16 :goto_0

    :cond_7
    move v1, v0

    goto :goto_4
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 11700
    invoke-static {}, Lcom/google/r/b/a/adi;->newBuilder()Lcom/google/r/b/a/adm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/adm;->a(Lcom/google/r/b/a/adi;)Lcom/google/r/b/a/adm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 11700
    invoke-static {}, Lcom/google/r/b/a/adi;->newBuilder()Lcom/google/r/b/a/adm;

    move-result-object v0

    return-object v0
.end method
