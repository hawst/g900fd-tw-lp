.class public final Lcom/google/r/b/a/abp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/abs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/abn;",
        "Lcom/google/r/b/a/abp;",
        ">;",
        "Lcom/google/r/b/a/abs;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 954
    sget-object v0, Lcom/google/r/b/a/abn;->d:Lcom/google/r/b/a/abn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1011
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/abp;->b:I

    .line 1048
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    .line 955
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 946
    new-instance v2, Lcom/google/r/b/a/abn;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/abn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/abp;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/abp;->b:I

    iput v1, v2, Lcom/google/r/b/a/abn;->b:I

    iget v1, p0, Lcom/google/r/b/a/abp;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/abp;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/abp;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/abn;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 946
    check-cast p1, Lcom/google/r/b/a/abn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/abp;->a(Lcom/google/r/b/a/abn;)Lcom/google/r/b/a/abp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/abn;)Lcom/google/r/b/a/abp;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 987
    invoke-static {}, Lcom/google/r/b/a/abn;->d()Lcom/google/r/b/a/abn;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1002
    :goto_0
    return-object p0

    .line 988
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/abn;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 989
    iget v0, p1, Lcom/google/r/b/a/abn;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/abq;->a(I)Lcom/google/r/b/a/abq;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/abq;->a:Lcom/google/r/b/a/abq;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 988
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 989
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/abp;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/abp;->a:I

    iget v0, v0, Lcom/google/r/b/a/abq;->e:I

    iput v0, p0, Lcom/google/r/b/a/abp;->b:I

    .line 991
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 992
    iget-object v0, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 993
    iget-object v0, p1, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    .line 994
    iget v0, p0, Lcom/google/r/b/a/abp;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/abp;->a:I

    .line 1001
    :cond_5
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/abn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 996
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/abp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/abp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/abp;->a:I

    .line 997
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/abp;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1006
    const/4 v0, 0x1

    return v0
.end method
