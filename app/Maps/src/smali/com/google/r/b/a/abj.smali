.class public final Lcom/google/r/b/a/abj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/abm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/abj;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/abj;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/google/r/b/a/abk;

    invoke-direct {v0}, Lcom/google/r/b/a/abk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/abj;->PARSER:Lcom/google/n/ax;

    .line 253
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/abj;->h:Lcom/google/n/aw;

    .line 567
    new-instance v0, Lcom/google/r/b/a/abj;

    invoke-direct {v0}, Lcom/google/r/b/a/abj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/abj;->e:Lcom/google/r/b/a/abj;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/abj;->b:Lcom/google/n/ao;

    .line 203
    iput-byte v2, p0, Lcom/google/r/b/a/abj;->f:B

    .line 228
    iput v2, p0, Lcom/google/r/b/a/abj;->g:I

    .line 59
    iget-object v0, p0, Lcom/google/r/b/a/abj;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/abj;->c:Ljava/lang/Object;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/abj;->d:I

    .line 62
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Lcom/google/r/b/a/abj;-><init>()V

    .line 69
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 74
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 75
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 76
    sparse-switch v3, :sswitch_data_0

    .line 81
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 83
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 79
    goto :goto_0

    .line 88
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/abj;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 89
    iget v3, p0, Lcom/google/r/b/a/abj;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/abj;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/abj;->au:Lcom/google/n/bn;

    throw v0

    .line 93
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 94
    iget v4, p0, Lcom/google/r/b/a/abj;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/abj;->a:I

    .line 95
    iput-object v3, p0, Lcom/google/r/b/a/abj;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 107
    :catch_1
    move-exception v0

    .line 108
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 109
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 99
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/abj;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/abj;->a:I

    .line 100
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/abj;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 111
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abj;->au:Lcom/google/n/bn;

    .line 112
    return-void

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 56
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/abj;->b:Lcom/google/n/ao;

    .line 203
    iput-byte v1, p0, Lcom/google/r/b/a/abj;->f:B

    .line 228
    iput v1, p0, Lcom/google/r/b/a/abj;->g:I

    .line 57
    return-void
.end method

.method public static d()Lcom/google/r/b/a/abj;
    .locals 1

    .prologue
    .line 570
    sget-object v0, Lcom/google/r/b/a/abj;->e:Lcom/google/r/b/a/abj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/abl;
    .locals 1

    .prologue
    .line 315
    new-instance v0, Lcom/google/r/b/a/abl;

    invoke-direct {v0}, Lcom/google/r/b/a/abl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/abj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/google/r/b/a/abj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 215
    invoke-virtual {p0}, Lcom/google/r/b/a/abj;->c()I

    .line 216
    iget v0, p0, Lcom/google/r/b/a/abj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/r/b/a/abj;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 219
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/abj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 220
    iget-object v0, p0, Lcom/google/r/b/a/abj;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abj;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 222
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/abj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 223
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/abj;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/abj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 226
    return-void

    .line 220
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 205
    iget-byte v1, p0, Lcom/google/r/b/a/abj;->f:B

    .line 206
    if-ne v1, v0, :cond_0

    .line 210
    :goto_0
    return v0

    .line 207
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 209
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/abj;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 230
    iget v0, p0, Lcom/google/r/b/a/abj;->g:I

    .line 231
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 248
    :goto_0
    return v0

    .line 234
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/abj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 235
    iget-object v0, p0, Lcom/google/r/b/a/abj;->b:Lcom/google/n/ao;

    .line 236
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 238
    :goto_1
    iget v0, p0, Lcom/google/r/b/a/abj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 240
    iget-object v0, p0, Lcom/google/r/b/a/abj;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abj;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 242
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/abj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 243
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/r/b/a/abj;->d:I

    .line 244
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/abj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 247
    iput v0, p0, Lcom/google/r/b/a/abj;->g:I

    goto :goto_0

    .line 240
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 244
    :cond_4
    const/16 v0, 0xa

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/r/b/a/abj;->newBuilder()Lcom/google/r/b/a/abl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/abl;->a(Lcom/google/r/b/a/abj;)Lcom/google/r/b/a/abl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/r/b/a/abj;->newBuilder()Lcom/google/r/b/a/abl;

    move-result-object v0

    return-object v0
.end method
