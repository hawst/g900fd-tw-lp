.class public final Lcom/google/r/b/a/yp;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ys;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/yp;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/yp;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/f;

.field public c:Lcom/google/n/f;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4816
    new-instance v0, Lcom/google/r/b/a/yq;

    invoke-direct {v0}, Lcom/google/r/b/a/yq;-><init>()V

    sput-object v0, Lcom/google/r/b/a/yp;->PARSER:Lcom/google/n/ax;

    .line 4905
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/yp;->g:Lcom/google/n/aw;

    .line 5108
    new-instance v0, Lcom/google/r/b/a/yp;

    invoke-direct {v0}, Lcom/google/r/b/a/yp;-><init>()V

    sput-object v0, Lcom/google/r/b/a/yp;->d:Lcom/google/r/b/a/yp;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4767
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4862
    iput-byte v0, p0, Lcom/google/r/b/a/yp;->e:B

    .line 4884
    iput v0, p0, Lcom/google/r/b/a/yp;->f:I

    .line 4768
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/yp;->b:Lcom/google/n/f;

    .line 4769
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/yp;->c:Lcom/google/n/f;

    .line 4770
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 4776
    invoke-direct {p0}, Lcom/google/r/b/a/yp;-><init>()V

    .line 4777
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 4781
    const/4 v0, 0x0

    .line 4782
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 4783
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 4784
    sparse-switch v3, :sswitch_data_0

    .line 4789
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 4791
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4787
    goto :goto_0

    .line 4796
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/yp;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/yp;->a:I

    .line 4797
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/r/b/a/yp;->b:Lcom/google/n/f;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4807
    :catch_0
    move-exception v0

    .line 4808
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4813
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/yp;->au:Lcom/google/n/bn;

    throw v0

    .line 4801
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/yp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/yp;->a:I

    .line 4802
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/r/b/a/yp;->c:Lcom/google/n/f;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4809
    :catch_1
    move-exception v0

    .line 4810
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4811
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4813
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yp;->au:Lcom/google/n/bn;

    .line 4814
    return-void

    .line 4784
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1da -> :sswitch_1
        0x1e2 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4765
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4862
    iput-byte v0, p0, Lcom/google/r/b/a/yp;->e:B

    .line 4884
    iput v0, p0, Lcom/google/r/b/a/yp;->f:I

    .line 4766
    return-void
.end method

.method public static a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;
    .locals 1

    .prologue
    .line 4970
    invoke-static {}, Lcom/google/r/b/a/yp;->newBuilder()Lcom/google/r/b/a/yr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yr;->a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/yp;
    .locals 1

    .prologue
    .line 5111
    sget-object v0, Lcom/google/r/b/a/yp;->d:Lcom/google/r/b/a/yp;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/yr;
    .locals 1

    .prologue
    .line 4967
    new-instance v0, Lcom/google/r/b/a/yr;

    invoke-direct {v0}, Lcom/google/r/b/a/yr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/yp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4828
    sget-object v0, Lcom/google/r/b/a/yp;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 4874
    invoke-virtual {p0}, Lcom/google/r/b/a/yp;->c()I

    .line 4875
    iget v0, p0, Lcom/google/r/b/a/yp;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 4876
    const/16 v0, 0x3b

    iget-object v1, p0, Lcom/google/r/b/a/yp;->b:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4878
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/yp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 4879
    const/16 v0, 0x3c

    iget-object v1, p0, Lcom/google/r/b/a/yp;->c:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4881
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/yp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4882
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4864
    iget-byte v1, p0, Lcom/google/r/b/a/yp;->e:B

    .line 4865
    if-ne v1, v0, :cond_0

    .line 4869
    :goto_0
    return v0

    .line 4866
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 4868
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/yp;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4886
    iget v0, p0, Lcom/google/r/b/a/yp;->f:I

    .line 4887
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4900
    :goto_0
    return v0

    .line 4890
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/yp;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 4891
    const/16 v0, 0x3b

    iget-object v2, p0, Lcom/google/r/b/a/yp;->b:Lcom/google/n/f;

    .line 4892
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 4894
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/yp;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 4895
    const/16 v2, 0x3c

    iget-object v3, p0, Lcom/google/r/b/a/yp;->c:Lcom/google/n/f;

    .line 4896
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 4898
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/yp;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4899
    iput v0, p0, Lcom/google/r/b/a/yp;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4759
    invoke-static {}, Lcom/google/r/b/a/yp;->newBuilder()Lcom/google/r/b/a/yr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yr;->a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4759
    invoke-static {}, Lcom/google/r/b/a/yp;->newBuilder()Lcom/google/r/b/a/yr;

    move-result-object v0

    return-object v0
.end method
