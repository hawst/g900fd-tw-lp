.class public final Lcom/google/r/b/a/afq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aft;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afq;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/r/b/a/afq;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12919
    new-instance v0, Lcom/google/r/b/a/afr;

    invoke-direct {v0}, Lcom/google/r/b/a/afr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afq;->PARSER:Lcom/google/n/ax;

    .line 12963
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/afq;->d:Lcom/google/n/aw;

    .line 13074
    new-instance v0, Lcom/google/r/b/a/afq;

    invoke-direct {v0}, Lcom/google/r/b/a/afq;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afq;->a:Lcom/google/r/b/a/afq;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12883
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 12934
    iput-byte v0, p0, Lcom/google/r/b/a/afq;->b:B

    .line 12950
    iput v0, p0, Lcom/google/r/b/a/afq;->c:I

    .line 12884
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 12890
    invoke-direct {p0}, Lcom/google/r/b/a/afq;-><init>()V

    .line 12892
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 12894
    const/4 v0, 0x0

    .line 12895
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 12896
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 12897
    packed-switch v3, :pswitch_data_0

    .line 12902
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 12904
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 12900
    goto :goto_0

    .line 12916
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afq;->au:Lcom/google/n/bn;

    .line 12917
    return-void

    .line 12910
    :catch_0
    move-exception v0

    .line 12911
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12916
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afq;->au:Lcom/google/n/bn;

    throw v0

    .line 12912
    :catch_1
    move-exception v0

    .line 12913
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 12914
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 12897
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12881
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 12934
    iput-byte v0, p0, Lcom/google/r/b/a/afq;->b:B

    .line 12950
    iput v0, p0, Lcom/google/r/b/a/afq;->c:I

    .line 12882
    return-void
.end method

.method public static d()Lcom/google/r/b/a/afq;
    .locals 1

    .prologue
    .line 13077
    sget-object v0, Lcom/google/r/b/a/afq;->a:Lcom/google/r/b/a/afq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/afs;
    .locals 1

    .prologue
    .line 13025
    new-instance v0, Lcom/google/r/b/a/afs;

    invoke-direct {v0}, Lcom/google/r/b/a/afs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12931
    sget-object v0, Lcom/google/r/b/a/afq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 12946
    iget v0, p0, Lcom/google/r/b/a/afq;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 12947
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/afq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12948
    return-void

    .line 12946
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/afq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/afq;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 12936
    iget-byte v1, p0, Lcom/google/r/b/a/afq;->b:B

    .line 12937
    if-ne v1, v0, :cond_0

    .line 12941
    :goto_0
    return v0

    .line 12938
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 12940
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/afq;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 12952
    iget v0, p0, Lcom/google/r/b/a/afq;->c:I

    .line 12953
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 12958
    :goto_0
    return v0

    .line 12955
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/afq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12957
    iput v0, p0, Lcom/google/r/b/a/afq;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12875
    invoke-static {}, Lcom/google/r/b/a/afq;->newBuilder()Lcom/google/r/b/a/afs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/afs;->a(Lcom/google/r/b/a/afq;)Lcom/google/r/b/a/afs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12875
    invoke-static {}, Lcom/google/r/b/a/afq;->newBuilder()Lcom/google/r/b/a/afs;

    move-result-object v0

    return-object v0
.end method
