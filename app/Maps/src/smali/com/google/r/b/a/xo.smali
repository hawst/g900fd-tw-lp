.class public final Lcom/google/r/b/a/xo;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/xs;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xo;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/r/b/a/xo;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field j:Lcom/google/n/ao;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 233
    new-instance v0, Lcom/google/r/b/a/xp;

    invoke-direct {v0}, Lcom/google/r/b/a/xp;-><init>()V

    sput-object v0, Lcom/google/r/b/a/xo;->PARSER:Lcom/google/n/ax;

    .line 364
    new-instance v0, Lcom/google/r/b/a/xq;

    invoke-direct {v0}, Lcom/google/r/b/a/xq;-><init>()V

    .line 517
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/xo;->n:Lcom/google/n/aw;

    .line 1303
    new-instance v0, Lcom/google/r/b/a/xo;

    invoke-direct {v0}, Lcom/google/r/b/a/xo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/xo;->k:Lcom/google/r/b/a/xo;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 250
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    .line 266
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->c:Lcom/google/n/ao;

    .line 282
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->d:Lcom/google/n/ao;

    .line 298
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->e:Lcom/google/n/ao;

    .line 314
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->f:Lcom/google/n/ao;

    .line 330
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->g:Lcom/google/n/ao;

    .line 346
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->h:Lcom/google/n/ao;

    .line 393
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    .line 408
    iput-byte v3, p0, Lcom/google/r/b/a/xo;->l:B

    .line 463
    iput v3, p0, Lcom/google/r/b/a/xo;->m:I

    .line 112
    iget-object v0, p0, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 113
    iget-object v0, p0, Lcom/google/r/b/a/xo;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 114
    iget-object v0, p0, Lcom/google/r/b/a/xo;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 115
    iget-object v0, p0, Lcom/google/r/b/a/xo;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 116
    iget-object v0, p0, Lcom/google/r/b/a/xo;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 117
    iget-object v0, p0, Lcom/google/r/b/a/xo;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 118
    iget-object v0, p0, Lcom/google/r/b/a/xo;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 119
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    .line 120
    iget-object v0, p0, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 121
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/16 v7, 0x80

    const/4 v0, 0x0

    .line 127
    invoke-direct {p0}, Lcom/google/r/b/a/xo;-><init>()V

    .line 130
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 133
    :cond_0
    :goto_0
    if-nez v2, :cond_6

    .line 134
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 135
    sparse-switch v1, :sswitch_data_0

    .line 140
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 142
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 138
    goto :goto_0

    .line 147
    :sswitch_1
    iget-object v1, p0, Lcom/google/r/b/a/xo;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 148
    iget v1, p0, Lcom/google/r/b/a/xo;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/xo;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 221
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 222
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit16 v1, v1, 0x80

    if-ne v1, v7, :cond_1

    .line 228
    iget-object v1, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    .line 230
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/xo;->au:Lcom/google/n/bn;

    throw v0

    .line 152
    :sswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 153
    iget v1, p0, Lcom/google/r/b/a/xo;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/xo;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 223
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 224
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 225
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 157
    :sswitch_3
    :try_start_4
    iget-object v1, p0, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 158
    iget v1, p0, Lcom/google/r/b/a/xo;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/xo;->a:I

    goto :goto_0

    .line 227
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_2

    .line 162
    :sswitch_4
    iget-object v1, p0, Lcom/google/r/b/a/xo;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 163
    iget v1, p0, Lcom/google/r/b/a/xo;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/xo;->a:I

    goto/16 :goto_0

    .line 167
    :sswitch_5
    iget-object v1, p0, Lcom/google/r/b/a/xo;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 168
    iget v1, p0, Lcom/google/r/b/a/xo;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/xo;->a:I

    goto/16 :goto_0

    .line 172
    :sswitch_6
    iget-object v1, p0, Lcom/google/r/b/a/xo;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 173
    iget v1, p0, Lcom/google/r/b/a/xo;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/r/b/a/xo;->a:I

    goto/16 :goto_0

    .line 177
    :sswitch_7
    iget-object v1, p0, Lcom/google/r/b/a/xo;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 178
    iget v1, p0, Lcom/google/r/b/a/xo;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/r/b/a/xo;->a:I

    goto/16 :goto_0

    .line 182
    :sswitch_8
    iget-object v1, p0, Lcom/google/r/b/a/xo;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 183
    iget v1, p0, Lcom/google/r/b/a/xo;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/r/b/a/xo;->a:I

    goto/16 :goto_0

    .line 187
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    .line 188
    invoke-static {v5}, Lcom/google/r/b/a/acy;->a(I)Lcom/google/r/b/a/acy;

    move-result-object v1

    .line 189
    if-nez v1, :cond_2

    .line 190
    const/16 v1, 0xb

    invoke-virtual {v4, v1, v5}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 192
    :cond_2
    and-int/lit16 v1, v0, 0x80

    if-eq v1, v7, :cond_9

    .line 193
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 194
    or-int/lit16 v1, v0, 0x80

    .line 196
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 198
    goto/16 :goto_0

    .line 201
    :sswitch_a
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 202
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 203
    :goto_5
    iget v1, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v1, v6, :cond_3

    const/4 v1, -0x1

    :goto_6
    if-lez v1, :cond_5

    .line 204
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v6

    .line 205
    invoke-static {v6}, Lcom/google/r/b/a/acy;->a(I)Lcom/google/r/b/a/acy;

    move-result-object v1

    .line 206
    if-nez v1, :cond_4

    .line 207
    const/16 v1, 0xb

    invoke-virtual {v4, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 203
    :cond_3
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v1, v6, v1

    goto :goto_6

    .line 209
    :cond_4
    and-int/lit16 v1, v0, 0x80

    if-eq v1, v7, :cond_8

    .line 210
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 211
    or-int/lit16 v1, v0, 0x80

    .line 213
    :goto_7
    :try_start_7
    iget-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 215
    goto :goto_5

    .line 216
    :cond_5
    :try_start_8
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 227
    :cond_6
    and-int/lit16 v0, v0, 0x80

    if-ne v0, v7, :cond_7

    .line 228
    iget-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    .line 230
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xo;->au:Lcom/google/n/bn;

    .line 231
    return-void

    .line 223
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 221
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto :goto_7

    :cond_9
    move v1, v0

    goto :goto_4

    .line 135
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x58 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 109
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 250
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    .line 266
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->c:Lcom/google/n/ao;

    .line 282
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->d:Lcom/google/n/ao;

    .line 298
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->e:Lcom/google/n/ao;

    .line 314
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->f:Lcom/google/n/ao;

    .line 330
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->g:Lcom/google/n/ao;

    .line 346
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->h:Lcom/google/n/ao;

    .line 393
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    .line 408
    iput-byte v1, p0, Lcom/google/r/b/a/xo;->l:B

    .line 463
    iput v1, p0, Lcom/google/r/b/a/xo;->m:I

    .line 110
    return-void
.end method

.method public static d()Lcom/google/r/b/a/xo;
    .locals 1

    .prologue
    .line 1306
    sget-object v0, Lcom/google/r/b/a/xo;->k:Lcom/google/r/b/a/xo;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/xr;
    .locals 1

    .prologue
    .line 579
    new-instance v0, Lcom/google/r/b/a/xr;

    invoke-direct {v0}, Lcom/google/r/b/a/xr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    sget-object v0, Lcom/google/r/b/a/xo;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 432
    invoke-virtual {p0}, Lcom/google/r/b/a/xo;->c()I

    .line 433
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_0

    .line 434
    iget-object v0, p0, Lcom/google/r/b/a/xo;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 436
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_1

    .line 437
    iget-object v0, p0, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 439
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    .line 440
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 442
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 443
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/xo;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 445
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 446
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/r/b/a/xo;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 448
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 449
    iget-object v0, p0, Lcom/google/r/b/a/xo;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 451
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 452
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/r/b/a/xo;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 454
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 455
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/r/b/a/xo;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 457
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 458
    const/16 v2, 0xb

    iget-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 457
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 460
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/xo;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 461
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 410
    iget-byte v0, p0, Lcom/google/r/b/a/xo;->l:B

    .line 411
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 427
    :goto_0
    return v0

    .line 412
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 414
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 415
    iget-object v0, p0, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mf;->d()Lcom/google/o/h/a/mf;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mf;

    invoke-virtual {v0}, Lcom/google/o/h/a/mf;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 416
    iput-byte v2, p0, Lcom/google/r/b/a/xo;->l:B

    move v0, v2

    .line 417
    goto :goto_0

    :cond_2
    move v0, v2

    .line 414
    goto :goto_1

    .line 420
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 421
    iget-object v0, p0, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 422
    iput-byte v2, p0, Lcom/google/r/b/a/xo;->l:B

    move v0, v2

    .line 423
    goto :goto_0

    :cond_4
    move v0, v2

    .line 420
    goto :goto_2

    .line 426
    :cond_5
    iput-byte v1, p0, Lcom/google/r/b/a/xo;->l:B

    move v0, v1

    .line 427
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v4, 0xa

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 465
    iget v0, p0, Lcom/google/r/b/a/xo;->m:I

    .line 466
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 512
    :goto_0
    return v0

    .line 469
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_a

    .line 470
    iget-object v0, p0, Lcom/google/r/b/a/xo;->c:Lcom/google/n/ao;

    .line 471
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 473
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1

    .line 474
    iget-object v2, p0, Lcom/google/r/b/a/xo;->j:Lcom/google/n/ao;

    .line 475
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 477
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v5, :cond_2

    .line 478
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/r/b/a/xo;->b:Lcom/google/n/ao;

    .line 479
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 481
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 482
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/r/b/a/xo;->d:Lcom/google/n/ao;

    .line 483
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 485
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v7, :cond_4

    .line 486
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/r/b/a/xo;->e:Lcom/google/n/ao;

    .line 487
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 489
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    .line 490
    iget-object v2, p0, Lcom/google/r/b/a/xo;->f:Lcom/google/n/ao;

    .line 491
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 493
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    .line 494
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/r/b/a/xo;->g:Lcom/google/n/ao;

    .line 495
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 497
    :cond_6
    iget v2, p0, Lcom/google/r/b/a/xo;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_9

    .line 498
    iget-object v2, p0, Lcom/google/r/b/a/xo;->h:Lcom/google/n/ao;

    .line 499
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    :goto_2
    move v3, v1

    .line 503
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 504
    iget-object v0, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    .line 505
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    .line 503
    add-int/lit8 v1, v1, 0x1

    move v3, v0

    goto :goto_3

    :cond_7
    move v0, v4

    .line 505
    goto :goto_4

    .line 507
    :cond_8
    add-int v0, v2, v3

    .line 508
    iget-object v1, p0, Lcom/google/r/b/a/xo;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 510
    iget-object v1, p0, Lcom/google/r/b/a/xo;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 511
    iput v0, p0, Lcom/google/r/b/a/xo;->m:I

    goto/16 :goto_0

    :cond_9
    move v2, v0

    goto :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/google/r/b/a/xo;->newBuilder()Lcom/google/r/b/a/xr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/xr;->a(Lcom/google/r/b/a/xo;)Lcom/google/r/b/a/xr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/google/r/b/a/xo;->newBuilder()Lcom/google/r/b/a/xr;

    move-result-object v0

    return-object v0
.end method
