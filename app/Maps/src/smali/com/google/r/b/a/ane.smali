.class public final Lcom/google/r/b/a/ane;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ani;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ane;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/r/b/a/amx;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/ane;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/google/r/b/a/anf;

    invoke-direct {v0}, Lcom/google/r/b/a/anf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ane;->PARSER:Lcom/google/n/ax;

    .line 145
    new-instance v0, Lcom/google/r/b/a/ang;

    invoke-direct {v0}, Lcom/google/r/b/a/ang;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ane;->d:Lcom/google/n/aj;

    .line 271
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ane;->i:Lcom/google/n/aw;

    .line 672
    new-instance v0, Lcom/google/r/b/a/ane;

    invoke-direct {v0}, Lcom/google/r/b/a/ane;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ane;->f:Lcom/google/r/b/a/ane;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 216
    iput-byte v0, p0, Lcom/google/r/b/a/ane;->g:B

    .line 241
    iput v0, p0, Lcom/google/r/b/a/ane;->h:I

    .line 18
    const v0, 0x278d00

    iput v0, p0, Lcom/google/r/b/a/ane;->b:I

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x2

    .line 27
    invoke-direct {p0}, Lcom/google/r/b/a/ane;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 33
    :cond_0
    :goto_0
    if-nez v2, :cond_9

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 35
    sparse-switch v1, :sswitch_data_0

    .line 40
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 42
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget v1, p0, Lcom/google/r/b/a/ane;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/ane;->a:I

    .line 48
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/ane;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 96
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 97
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 103
    iget-object v2, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    .line 105
    :cond_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_2

    .line 106
    iget-object v1, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    .line 108
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ane;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    .line 53
    invoke-static {v5}, Lcom/google/r/b/a/amx;->a(I)Lcom/google/r/b/a/amx;

    move-result-object v1

    .line 54
    if-nez v1, :cond_3

    .line 55
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v5}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 98
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 99
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 100
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :cond_3
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_c

    .line 58
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 59
    or-int/lit8 v1, v0, 0x2

    .line 61
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 63
    goto :goto_0

    .line 66
    :sswitch_3
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 67
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v5

    move v1, v0

    .line 68
    :goto_5
    :try_start_7
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_4

    const/4 v0, -0x1

    :goto_6
    if-lez v0, :cond_7

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 70
    invoke-static {v0}, Lcom/google/r/b/a/amx;->a(I)Lcom/google/r/b/a/amx;

    move-result-object v6

    .line 71
    if-nez v6, :cond_5

    .line 72
    const/4 v6, 0x2

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 96
    :catch_2
    move-exception v0

    goto :goto_1

    .line 68
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_6

    .line 74
    :cond_5
    and-int/lit8 v6, v1, 0x2

    if-eq v6, v7, :cond_6

    .line 75
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    .line 76
    or-int/lit8 v1, v1, 0x2

    .line 78
    :cond_6
    iget-object v6, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 98
    :catch_3
    move-exception v0

    goto :goto_3

    .line 81
    :cond_7
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 82
    goto/16 :goto_0

    .line 85
    :sswitch_4
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_8

    .line 86
    :try_start_8
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    .line 88
    or-int/lit8 v0, v0, 0x4

    .line 90
    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 91
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 90
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 102
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto/16 :goto_2

    :cond_9
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v7, :cond_a

    .line 103
    iget-object v1, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    .line 105
    :cond_a
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_b

    .line 106
    iget-object v0, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    .line 108
    :cond_b
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ane;->au:Lcom/google/n/bn;

    .line 109
    return-void

    :cond_c
    move v1, v0

    goto/16 :goto_4

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 216
    iput-byte v0, p0, Lcom/google/r/b/a/ane;->g:B

    .line 241
    iput v0, p0, Lcom/google/r/b/a/ane;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ane;
    .locals 1

    .prologue
    .line 675
    sget-object v0, Lcom/google/r/b/a/ane;->f:Lcom/google/r/b/a/ane;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/anh;
    .locals 1

    .prologue
    .line 333
    new-instance v0, Lcom/google/r/b/a/anh;

    invoke-direct {v0}, Lcom/google/r/b/a/anh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ane;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/google/r/b/a/ane;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 228
    invoke-virtual {p0}, Lcom/google/r/b/a/ane;->c()I

    .line 229
    iget v0, p0, Lcom/google/r/b/a/ane;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 230
    iget v0, p0, Lcom/google/r/b/a/ane;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    :cond_0
    move v1, v2

    .line 232
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 233
    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 232
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 235
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 236
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 235
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 238
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ane;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 239
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 218
    iget-byte v1, p0, Lcom/google/r/b/a/ane;->g:B

    .line 219
    if-ne v1, v0, :cond_0

    .line 223
    :goto_0
    return v0

    .line 220
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 222
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ane;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 243
    iget v0, p0, Lcom/google/r/b/a/ane;->h:I

    .line 244
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 266
    :goto_0
    return v0

    .line 247
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ane;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 248
    iget v0, p0, Lcom/google/r/b/a/ane;->b:I

    .line 249
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    :goto_2
    move v4, v3

    move v5, v3

    .line 253
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 254
    iget-object v0, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    .line 255
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v5, v0

    .line 253
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 249
    goto :goto_1

    :cond_2
    move v0, v1

    .line 255
    goto :goto_4

    .line 257
    :cond_3
    add-int v0, v2, v5

    .line 258
    iget-object v1, p0, Lcom/google/r/b/a/ane;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v3

    move v2, v0

    .line 260
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 261
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/ane;->e:Ljava/util/List;

    .line 262
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 260
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 264
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/ane;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 265
    iput v0, p0, Lcom/google/r/b/a/ane;->h:I

    goto :goto_0

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ane;->newBuilder()Lcom/google/r/b/a/anh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/anh;->a(Lcom/google/r/b/a/ane;)Lcom/google/r/b/a/anh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ane;->newBuilder()Lcom/google/r/b/a/anh;

    move-result-object v0

    return-object v0
.end method
