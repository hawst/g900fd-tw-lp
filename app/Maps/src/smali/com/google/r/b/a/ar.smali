.class public final Lcom/google/r/b/a/ar;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/au;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ar;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/ar;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Z

.field c:Z

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/google/r/b/a/as;

    invoke-direct {v0}, Lcom/google/r/b/a/as;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ar;->PARSER:Lcom/google/n/ax;

    .line 155
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ar;->g:Lcom/google/n/aw;

    .line 352
    new-instance v0, Lcom/google/r/b/a/ar;

    invoke-direct {v0}, Lcom/google/r/b/a/ar;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ar;->d:Lcom/google/r/b/a/ar;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 112
    iput-byte v0, p0, Lcom/google/r/b/a/ar;->e:B

    .line 134
    iput v0, p0, Lcom/google/r/b/a/ar;->f:I

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/ar;->b:Z

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/ar;->c:Z

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/r/b/a/ar;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 32
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 34
    sparse-switch v0, :sswitch_data_0

    .line 39
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/ar;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ar;->a:I

    .line 47
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/ar;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ar;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 47
    goto :goto_1

    .line 51
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/ar;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ar;->a:I

    .line 52
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/ar;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 59
    :catch_1
    move-exception v0

    .line 60
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 61
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v0, v2

    .line 52
    goto :goto_2

    .line 63
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ar;->au:Lcom/google/n/bn;

    .line 64
    return-void

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 112
    iput-byte v0, p0, Lcom/google/r/b/a/ar;->e:B

    .line 134
    iput v0, p0, Lcom/google/r/b/a/ar;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ar;
    .locals 1

    .prologue
    .line 355
    sget-object v0, Lcom/google/r/b/a/ar;->d:Lcom/google/r/b/a/ar;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/at;
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/r/b/a/at;

    invoke-direct {v0}, Lcom/google/r/b/a/at;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/google/r/b/a/ar;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 124
    invoke-virtual {p0}, Lcom/google/r/b/a/ar;->c()I

    .line 125
    iget v0, p0, Lcom/google/r/b/a/ar;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 126
    iget-boolean v0, p0, Lcom/google/r/b/a/ar;->b:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IZ)V

    .line 128
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ar;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 129
    iget-boolean v0, p0, Lcom/google/r/b/a/ar;->c:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/ar;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 132
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 114
    iget-byte v1, p0, Lcom/google/r/b/a/ar;->e:B

    .line 115
    if-ne v1, v0, :cond_0

    .line 119
    :goto_0
    return v0

    .line 116
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ar;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 136
    iget v0, p0, Lcom/google/r/b/a/ar;->f:I

    .line 137
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 150
    :goto_0
    return v0

    .line 140
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ar;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 141
    iget-boolean v0, p0, Lcom/google/r/b/a/ar;->b:Z

    .line 142
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 144
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/ar;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 145
    iget-boolean v2, p0, Lcom/google/r/b/a/ar;->c:Z

    .line 146
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ar;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    iput v0, p0, Lcom/google/r/b/a/ar;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ar;->newBuilder()Lcom/google/r/b/a/at;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/at;->a(Lcom/google/r/b/a/ar;)Lcom/google/r/b/a/at;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ar;->newBuilder()Lcom/google/r/b/a/at;

    move-result-object v0

    return-object v0
.end method
