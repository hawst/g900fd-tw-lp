.class public final Lcom/google/r/b/a/a/af;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/ak;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/af;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/r/b/a/a/af;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field public d:I

.field e:Ljava/lang/Object;

.field public f:J

.field public g:Z

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 637
    new-instance v0, Lcom/google/r/b/a/a/ag;

    invoke-direct {v0}, Lcom/google/r/b/a/a/ag;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/af;->PARSER:Lcom/google/n/ax;

    .line 961
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/af;->k:Lcom/google/n/aw;

    .line 1454
    new-instance v0, Lcom/google/r/b/a/a/af;

    invoke-direct {v0}, Lcom/google/r/b/a/a/af;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/af;->h:Lcom/google/r/b/a/a/af;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 556
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 739
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    .line 884
    iput-byte v2, p0, Lcom/google/r/b/a/a/af;->i:B

    .line 924
    iput v2, p0, Lcom/google/r/b/a/a/af;->j:I

    .line 557
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 558
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    .line 559
    iput v3, p0, Lcom/google/r/b/a/a/af;->d:I

    .line 560
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->e:Ljava/lang/Object;

    .line 561
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/a/af;->f:J

    .line 562
    iput-boolean v3, p0, Lcom/google/r/b/a/a/af;->g:Z

    .line 563
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 569
    invoke-direct {p0}, Lcom/google/r/b/a/a/af;-><init>()V

    .line 570
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 575
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 576
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 577
    sparse-switch v0, :sswitch_data_0

    .line 582
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 584
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 580
    goto :goto_0

    .line 589
    :sswitch_1
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 590
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/a/af;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 628
    :catch_0
    move-exception v0

    .line 629
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 634
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/af;->au:Lcom/google/n/bn;

    throw v0

    .line 594
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 595
    iget v5, p0, Lcom/google/r/b/a/a/af;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/a/af;->a:I

    .line 596
    iput-object v0, p0, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 630
    :catch_1
    move-exception v0

    .line 631
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 632
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 600
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 601
    invoke-static {v0}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v5

    .line 602
    if-nez v5, :cond_1

    .line 603
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 605
    :cond_1
    iget v5, p0, Lcom/google/r/b/a/a/af;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/a/af;->a:I

    .line 606
    iput v0, p0, Lcom/google/r/b/a/a/af;->d:I

    goto :goto_0

    .line 611
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 612
    iget v5, p0, Lcom/google/r/b/a/a/af;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/r/b/a/a/af;->a:I

    .line 613
    iput-object v0, p0, Lcom/google/r/b/a/a/af;->e:Ljava/lang/Object;

    goto :goto_0

    .line 617
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/a/af;->a:I

    .line 618
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/a/af;->f:J

    goto :goto_0

    .line 622
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/a/af;->a:I

    .line 623
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/a/af;->g:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 634
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->au:Lcom/google/n/bn;

    .line 635
    return-void

    .line 577
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 554
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 739
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    .line 884
    iput-byte v1, p0, Lcom/google/r/b/a/a/af;->i:B

    .line 924
    iput v1, p0, Lcom/google/r/b/a/a/af;->j:I

    .line 555
    return-void
.end method

.method public static g()Lcom/google/r/b/a/a/af;
    .locals 1

    .prologue
    .line 1457
    sget-object v0, Lcom/google/r/b/a/a/af;->h:Lcom/google/r/b/a/a/af;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/ah;
    .locals 1

    .prologue
    .line 1023
    new-instance v0, Lcom/google/r/b/a/a/ah;

    invoke-direct {v0}, Lcom/google/r/b/a/a/ah;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 649
    sget-object v0, Lcom/google/r/b/a/a/af;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 902
    invoke-virtual {p0}, Lcom/google/r/b/a/a/af;->c()I

    .line 903
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 904
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 906
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 907
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 909
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 910
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/a/af;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 912
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 913
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 915
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 916
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/r/b/a/a/af;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 918
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 919
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/r/b/a/a/af;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 921
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 922
    return-void

    .line 907
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 913
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 886
    iget-byte v0, p0, Lcom/google/r/b/a/a/af;->i:B

    .line 887
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 897
    :goto_0
    return v0

    .line 888
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 890
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 891
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/al;->d()Lcom/google/r/b/a/a/al;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/al;

    invoke-virtual {v0}, Lcom/google/r/b/a/a/al;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 892
    iput-byte v2, p0, Lcom/google/r/b/a/a/af;->i:B

    move v0, v2

    .line 893
    goto :goto_0

    :cond_2
    move v0, v2

    .line 890
    goto :goto_1

    .line 896
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/a/af;->i:B

    move v0, v1

    .line 897
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 926
    iget v0, p0, Lcom/google/r/b/a/a/af;->j:I

    .line 927
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 956
    :goto_0
    return v0

    .line 930
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 931
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    .line 932
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 934
    :goto_1
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 936
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 938
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 939
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/r/b/a/a/af;->d:I

    .line 940
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 942
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 944
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/af;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 946
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 947
    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/google/r/b/a/a/af;->f:J

    .line 948
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 950
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 951
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/r/b/a/a/af;->g:Z

    .line 952
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 954
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 955
    iput v0, p0, Lcom/google/r/b/a/a/af;->j:I

    goto/16 :goto_0

    .line 936
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 940
    :cond_7
    const/16 v0, 0xa

    goto :goto_3

    .line 944
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_9
    move v1, v2

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    .line 767
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 768
    check-cast v0, Ljava/lang/String;

    .line 776
    :goto_0
    return-object v0

    .line 770
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 772
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 773
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 774
    iput-object v1, p0, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 776
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 548
    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 548
    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v0

    return-object v0
.end method
