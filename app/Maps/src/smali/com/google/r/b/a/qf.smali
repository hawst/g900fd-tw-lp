.class public final Lcom/google/r/b/a/qf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qf;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/qf;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field d:Z

.field public e:J

.field public f:J

.field public g:J

.field public h:Lcom/google/n/ao;

.field public i:J

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6497
    new-instance v0, Lcom/google/r/b/a/qg;

    invoke-direct {v0}, Lcom/google/r/b/a/qg;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qf;->PARSER:Lcom/google/n/ax;

    .line 6753
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/qf;->m:Lcom/google/n/aw;

    .line 7308
    new-instance v0, Lcom/google/r/b/a/qf;

    invoke-direct {v0}, Lcom/google/r/b/a/qf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qf;->j:Lcom/google/r/b/a/qf;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 6411
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 6514
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    .line 6632
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qf;->h:Lcom/google/n/ao;

    .line 6662
    iput-byte v4, p0, Lcom/google/r/b/a/qf;->k:B

    .line 6708
    iput v4, p0, Lcom/google/r/b/a/qf;->l:I

    .line 6412
    iget-object v0, p0, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 6413
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/qf;->c:Ljava/lang/Object;

    .line 6414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/qf;->d:Z

    .line 6415
    iput-wide v2, p0, Lcom/google/r/b/a/qf;->e:J

    .line 6416
    iput-wide v2, p0, Lcom/google/r/b/a/qf;->f:J

    .line 6417
    iput-wide v2, p0, Lcom/google/r/b/a/qf;->g:J

    .line 6418
    iget-object v0, p0, Lcom/google/r/b/a/qf;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 6419
    iput-wide v2, p0, Lcom/google/r/b/a/qf;->i:J

    .line 6420
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 6426
    invoke-direct {p0}, Lcom/google/r/b/a/qf;-><init>()V

    .line 6427
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 6432
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 6433
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 6434
    sparse-switch v0, :sswitch_data_0

    .line 6439
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 6441
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 6437
    goto :goto_0

    .line 6446
    :sswitch_1
    iget-object v0, p0, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 6447
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/qf;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 6488
    :catch_0
    move-exception v0

    .line 6489
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6494
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/qf;->au:Lcom/google/n/bn;

    throw v0

    .line 6451
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/qf;->a:I

    .line 6452
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/qf;->d:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 6490
    :catch_1
    move-exception v0

    .line 6491
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 6492
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 6452
    goto :goto_1

    .line 6456
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/qf;->a:I

    .line 6457
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/qf;->e:J

    goto :goto_0

    .line 6461
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/qf;->a:I

    .line 6462
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/qf;->f:J

    goto :goto_0

    .line 6466
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/qf;->a:I

    .line 6467
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/qf;->g:J

    goto :goto_0

    .line 6471
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/qf;->a:I

    .line 6472
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/qf;->i:J

    goto/16 :goto_0

    .line 6476
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 6477
    iget v5, p0, Lcom/google/r/b/a/qf;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/qf;->a:I

    .line 6478
    iput-object v0, p0, Lcom/google/r/b/a/qf;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 6482
    :sswitch_8
    iget-object v0, p0, Lcom/google/r/b/a/qf;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 6483
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/qf;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 6494
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qf;->au:Lcom/google/n/bn;

    .line 6495
    return-void

    .line 6434
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 6409
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 6514
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    .line 6632
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qf;->h:Lcom/google/n/ao;

    .line 6662
    iput-byte v1, p0, Lcom/google/r/b/a/qf;->k:B

    .line 6708
    iput v1, p0, Lcom/google/r/b/a/qf;->l:I

    .line 6410
    return-void
.end method

.method public static d()Lcom/google/r/b/a/qf;
    .locals 1

    .prologue
    .line 7311
    sget-object v0, Lcom/google/r/b/a/qf;->j:Lcom/google/r/b/a/qf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/qh;
    .locals 1

    .prologue
    .line 6815
    new-instance v0, Lcom/google/r/b/a/qh;

    invoke-direct {v0}, Lcom/google/r/b/a/qh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6509
    sget-object v0, Lcom/google/r/b/a/qf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 6680
    invoke-virtual {p0}, Lcom/google/r/b/a/qf;->c()I

    .line 6681
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 6682
    iget-object v0, p0, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6684
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 6685
    iget-boolean v0, p0, Lcom/google/r/b/a/qf;->d:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 6687
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 6688
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/r/b/a/qf;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->a(IJ)V

    .line 6690
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 6691
    iget-wide v0, p0, Lcom/google/r/b/a/qf;->f:J

    invoke-virtual {p1, v5, v0, v1}, Lcom/google/n/l;->a(IJ)V

    .line 6693
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 6694
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/r/b/a/qf;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->a(IJ)V

    .line 6696
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 6697
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/r/b/a/qf;->i:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 6699
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    .line 6700
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/qf;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qf;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6702
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 6703
    iget-object v0, p0, Lcom/google/r/b/a/qf;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6705
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/qf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6706
    return-void

    .line 6700
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 6664
    iget-byte v0, p0, Lcom/google/r/b/a/qf;->k:B

    .line 6665
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 6675
    :goto_0
    return v0

    .line 6666
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 6668
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 6669
    iget-object v0, p0, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/px;->d()Lcom/google/r/b/a/px;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/px;

    invoke-virtual {v0}, Lcom/google/r/b/a/px;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 6670
    iput-byte v2, p0, Lcom/google/r/b/a/qf;->k:B

    move v0, v2

    .line 6671
    goto :goto_0

    :cond_2
    move v0, v2

    .line 6668
    goto :goto_1

    .line 6674
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/qf;->k:B

    move v0, v1

    .line 6675
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 6710
    iget v0, p0, Lcom/google/r/b/a/qf;->l:I

    .line 6711
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 6748
    :goto_0
    return v0

    .line 6714
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 6715
    iget-object v0, p0, Lcom/google/r/b/a/qf;->b:Lcom/google/n/ao;

    .line 6716
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 6718
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_1

    .line 6719
    iget-boolean v2, p0, Lcom/google/r/b/a/qf;->d:Z

    .line 6720
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 6722
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v8, :cond_2

    .line 6723
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/r/b/a/qf;->e:J

    .line 6724
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 6726
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    .line 6727
    iget-wide v2, p0, Lcom/google/r/b/a/qf;->f:J

    .line 6728
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 6730
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_4

    .line 6731
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/r/b/a/qf;->g:J

    .line 6732
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 6734
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8

    .line 6735
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/r/b/a/qf;->i:J

    .line 6736
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 6738
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_5

    .line 6739
    const/4 v3, 0x7

    .line 6740
    iget-object v0, p0, Lcom/google/r/b/a/qf;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qf;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 6742
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/qf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 6743
    iget-object v0, p0, Lcom/google/r/b/a/qf;->h:Lcom/google/n/ao;

    .line 6744
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 6746
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/qf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 6747
    iput v0, p0, Lcom/google/r/b/a/qf;->l:I

    goto/16 :goto_0

    .line 6740
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_8
    move v2, v0

    goto :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6403
    invoke-static {}, Lcom/google/r/b/a/qf;->newBuilder()Lcom/google/r/b/a/qh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/qh;->a(Lcom/google/r/b/a/qf;)Lcom/google/r/b/a/qh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6403
    invoke-static {}, Lcom/google/r/b/a/qf;->newBuilder()Lcom/google/r/b/a/qh;

    move-result-object v0

    return-object v0
.end method
