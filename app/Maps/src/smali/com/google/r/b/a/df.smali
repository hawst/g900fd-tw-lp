.class public final enum Lcom/google/r/b/a/df;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/df;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/df;

.field public static final enum b:Lcom/google/r/b/a/df;

.field public static final enum c:Lcom/google/r/b/a/df;

.field public static final enum d:Lcom/google/r/b/a/df;

.field private static final synthetic f:[Lcom/google/r/b/a/df;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/google/r/b/a/df;

    const-string v1, "DEFAULT_COLUMN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/df;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/df;->a:Lcom/google/r/b/a/df;

    .line 23
    new-instance v0, Lcom/google/r/b/a/df;

    const-string v1, "COLUMN_INDEX"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/df;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/df;->b:Lcom/google/r/b/a/df;

    .line 27
    new-instance v0, Lcom/google/r/b/a/df;

    const-string v1, "SAME_COLUMN_AS_PREVIOUS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/df;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/df;->c:Lcom/google/r/b/a/df;

    .line 31
    new-instance v0, Lcom/google/r/b/a/df;

    const-string v1, "SPAN_COLUMNS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/df;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/df;->d:Lcom/google/r/b/a/df;

    .line 14
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/df;

    sget-object v1, Lcom/google/r/b/a/df;->a:Lcom/google/r/b/a/df;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/df;->b:Lcom/google/r/b/a/df;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/df;->c:Lcom/google/r/b/a/df;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/df;->d:Lcom/google/r/b/a/df;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/df;->f:[Lcom/google/r/b/a/df;

    .line 71
    new-instance v0, Lcom/google/r/b/a/dg;

    invoke-direct {v0}, Lcom/google/r/b/a/dg;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 81
    iput p3, p0, Lcom/google/r/b/a/df;->e:I

    .line 82
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/df;
    .locals 1

    .prologue
    .line 57
    packed-switch p0, :pswitch_data_0

    .line 62
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 58
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/df;->a:Lcom/google/r/b/a/df;

    goto :goto_0

    .line 59
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/df;->b:Lcom/google/r/b/a/df;

    goto :goto_0

    .line 60
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/df;->c:Lcom/google/r/b/a/df;

    goto :goto_0

    .line 61
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/df;->d:Lcom/google/r/b/a/df;

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/df;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/r/b/a/df;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/df;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/df;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/r/b/a/df;->f:[Lcom/google/r/b/a/df;

    invoke-virtual {v0}, [Lcom/google/r/b/a/df;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/df;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/r/b/a/df;->e:I

    return v0
.end method
