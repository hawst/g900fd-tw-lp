.class public final Lcom/google/r/b/a/ahj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ahm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ahj;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/ahj;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/aq;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 680
    new-instance v0, Lcom/google/r/b/a/ahk;

    invoke-direct {v0}, Lcom/google/r/b/a/ahk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ahj;->PARSER:Lcom/google/n/ax;

    .line 815
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ahj;->g:Lcom/google/n/aw;

    .line 1127
    new-instance v0, Lcom/google/r/b/a/ahj;

    invoke-direct {v0}, Lcom/google/r/b/a/ahj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ahj;->d:Lcom/google/r/b/a/ahj;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 623
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 767
    iput-byte v0, p0, Lcom/google/r/b/a/ahj;->e:B

    .line 789
    iput v0, p0, Lcom/google/r/b/a/ahj;->f:I

    .line 624
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ahj;->b:Ljava/lang/Object;

    .line 625
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    .line 626
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x2

    .line 632
    invoke-direct {p0}, Lcom/google/r/b/a/ahj;-><init>()V

    .line 635
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 638
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 639
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 640
    sparse-switch v4, :sswitch_data_0

    .line 645
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 647
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 643
    goto :goto_0

    .line 652
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 653
    iget v5, p0, Lcom/google/r/b/a/ahj;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/ahj;->a:I

    .line 654
    iput-object v4, p0, Lcom/google/r/b/a/ahj;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 668
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 669
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 674
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 675
    iget-object v1, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    .line 677
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ahj;->au:Lcom/google/n/bn;

    throw v0

    .line 658
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 659
    and-int/lit8 v5, v0, 0x2

    if-eq v5, v6, :cond_2

    .line 660
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    .line 661
    or-int/lit8 v0, v0, 0x2

    .line 663
    :cond_2
    iget-object v5, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 670
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 671
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 672
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 674
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 675
    iget-object v0, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    .line 677
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahj;->au:Lcom/google/n/bn;

    .line 678
    return-void

    .line 674
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 640
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 621
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 767
    iput-byte v0, p0, Lcom/google/r/b/a/ahj;->e:B

    .line 789
    iput v0, p0, Lcom/google/r/b/a/ahj;->f:I

    .line 622
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ahj;
    .locals 1

    .prologue
    .line 1130
    sget-object v0, Lcom/google/r/b/a/ahj;->d:Lcom/google/r/b/a/ahj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ahl;
    .locals 1

    .prologue
    .line 877
    new-instance v0, Lcom/google/r/b/a/ahl;

    invoke-direct {v0}, Lcom/google/r/b/a/ahl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ahj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 692
    sget-object v0, Lcom/google/r/b/a/ahj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 779
    invoke-virtual {p0}, Lcom/google/r/b/a/ahj;->c()I

    .line 780
    iget v0, p0, Lcom/google/r/b/a/ahj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 781
    iget-object v0, p0, Lcom/google/r/b/a/ahj;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahj;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 783
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 784
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 783
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 781
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 786
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ahj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 787
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 769
    iget-byte v1, p0, Lcom/google/r/b/a/ahj;->e:B

    .line 770
    if-ne v1, v0, :cond_0

    .line 774
    :goto_0
    return v0

    .line 771
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 773
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ahj;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 791
    iget v0, p0, Lcom/google/r/b/a/ahj;->f:I

    .line 792
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 810
    :goto_0
    return v0

    .line 795
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ahj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 797
    iget-object v0, p0, Lcom/google/r/b/a/ahj;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ahj;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    .line 801
    :goto_3
    iget-object v3, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 802
    iget-object v3, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    .line 803
    invoke-interface {v3, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 801
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 797
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 805
    :cond_2
    add-int/2addr v0, v2

    .line 806
    iget-object v1, p0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 808
    iget-object v1, p0, Lcom/google/r/b/a/ahj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 809
    iput v0, p0, Lcom/google/r/b/a/ahj;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 615
    invoke-static {}, Lcom/google/r/b/a/ahj;->newBuilder()Lcom/google/r/b/a/ahl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ahl;->a(Lcom/google/r/b/a/ahj;)Lcom/google/r/b/a/ahl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 615
    invoke-static {}, Lcom/google/r/b/a/ahj;->newBuilder()Lcom/google/r/b/a/ahl;

    move-result-object v0

    return-object v0
.end method
