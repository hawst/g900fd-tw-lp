.class public final Lcom/google/r/b/a/aby;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/acb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aby;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/aby;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:Ljava/lang/Object;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17754
    new-instance v0, Lcom/google/r/b/a/abz;

    invoke-direct {v0}, Lcom/google/r/b/a/abz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aby;->PARSER:Lcom/google/n/ax;

    .line 17893
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aby;->h:Lcom/google/n/aw;

    .line 18181
    new-instance v0, Lcom/google/r/b/a/aby;

    invoke-direct {v0}, Lcom/google/r/b/a/aby;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aby;->e:Lcom/google/r/b/a/aby;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17692
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 17843
    iput-byte v0, p0, Lcom/google/r/b/a/aby;->f:B

    .line 17868
    iput v0, p0, Lcom/google/r/b/a/aby;->g:I

    .line 17693
    iput v1, p0, Lcom/google/r/b/a/aby;->b:I

    .line 17694
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aby;->c:Ljava/lang/Object;

    .line 17695
    iput v1, p0, Lcom/google/r/b/a/aby;->d:I

    .line 17696
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 17702
    invoke-direct {p0}, Lcom/google/r/b/a/aby;-><init>()V

    .line 17703
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 17707
    const/4 v0, 0x0

    .line 17708
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 17709
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 17710
    sparse-switch v3, :sswitch_data_0

    .line 17715
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 17717
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 17713
    goto :goto_0

    .line 17722
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/aby;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aby;->a:I

    .line 17723
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/aby;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 17745
    :catch_0
    move-exception v0

    .line 17746
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17751
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aby;->au:Lcom/google/n/bn;

    throw v0

    .line 17727
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 17728
    iget v4, p0, Lcom/google/r/b/a/aby;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/aby;->a:I

    .line 17729
    iput-object v3, p0, Lcom/google/r/b/a/aby;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 17747
    :catch_1
    move-exception v0

    .line 17748
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 17749
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 17733
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 17734
    invoke-static {v3}, Lcom/google/r/b/a/acc;->a(I)Lcom/google/r/b/a/acc;

    move-result-object v4

    .line 17735
    if-nez v4, :cond_1

    .line 17736
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 17738
    :cond_1
    iget v4, p0, Lcom/google/r/b/a/aby;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/aby;->a:I

    .line 17739
    iput v3, p0, Lcom/google/r/b/a/aby;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 17751
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aby;->au:Lcom/google/n/bn;

    .line 17752
    return-void

    .line 17710
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17690
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 17843
    iput-byte v0, p0, Lcom/google/r/b/a/aby;->f:B

    .line 17868
    iput v0, p0, Lcom/google/r/b/a/aby;->g:I

    .line 17691
    return-void
.end method

.method public static a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;
    .locals 1

    .prologue
    .line 17958
    invoke-static {}, Lcom/google/r/b/a/aby;->newBuilder()Lcom/google/r/b/a/aca;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aca;->a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/aby;
    .locals 1

    .prologue
    .line 18184
    sget-object v0, Lcom/google/r/b/a/aby;->e:Lcom/google/r/b/a/aby;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aca;
    .locals 1

    .prologue
    .line 17955
    new-instance v0, Lcom/google/r/b/a/aca;

    invoke-direct {v0}, Lcom/google/r/b/a/aca;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aby;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17766
    sget-object v0, Lcom/google/r/b/a/aby;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 17855
    invoke-virtual {p0}, Lcom/google/r/b/a/aby;->c()I

    .line 17856
    iget v0, p0, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 17857
    iget v0, p0, Lcom/google/r/b/a/aby;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 17859
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 17860
    iget-object v0, p0, Lcom/google/r/b/a/aby;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aby;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 17862
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 17863
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/aby;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 17865
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/aby;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 17866
    return-void

    .line 17860
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 17845
    iget-byte v1, p0, Lcom/google/r/b/a/aby;->f:B

    .line 17846
    if-ne v1, v0, :cond_0

    .line 17850
    :goto_0
    return v0

    .line 17847
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 17849
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aby;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17870
    iget v0, p0, Lcom/google/r/b/a/aby;->g:I

    .line 17871
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 17888
    :goto_0
    return v0

    .line 17874
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 17875
    iget v0, p0, Lcom/google/r/b/a/aby;->b:I

    .line 17876
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 17878
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 17880
    iget-object v0, p0, Lcom/google/r/b/a/aby;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aby;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 17882
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_3

    .line 17883
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/r/b/a/aby;->d:I

    .line 17884
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 17886
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/aby;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 17887
    iput v0, p0, Lcom/google/r/b/a/aby;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 17876
    goto :goto_1

    .line 17880
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 17684
    invoke-static {}, Lcom/google/r/b/a/aby;->newBuilder()Lcom/google/r/b/a/aca;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aca;->a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 17684
    invoke-static {}, Lcom/google/r/b/a/aby;->newBuilder()Lcom/google/r/b/a/aca;

    move-result-object v0

    return-object v0
.end method
