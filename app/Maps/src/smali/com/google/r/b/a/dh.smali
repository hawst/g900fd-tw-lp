.class public final enum Lcom/google/r/b/a/dh;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/dh;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/dh;

.field public static final enum b:Lcom/google/r/b/a/dh;

.field public static final enum c:Lcom/google/r/b/a/dh;

.field public static final enum d:Lcom/google/r/b/a/dh;

.field public static final enum e:Lcom/google/r/b/a/dh;

.field public static final enum f:Lcom/google/r/b/a/dh;

.field public static final enum g:Lcom/google/r/b/a/dh;

.field public static final enum h:Lcom/google/r/b/a/dh;

.field public static final enum i:Lcom/google/r/b/a/dh;

.field public static final enum j:Lcom/google/r/b/a/dh;

.field public static final enum k:Lcom/google/r/b/a/dh;

.field public static final enum l:Lcom/google/r/b/a/dh;

.field public static final enum m:Lcom/google/r/b/a/dh;

.field public static final enum n:Lcom/google/r/b/a/dh;

.field public static final enum o:Lcom/google/r/b/a/dh;

.field public static final enum p:Lcom/google/r/b/a/dh;

.field private static final synthetic r:[Lcom/google/r/b/a/dh;


# instance fields
.field public final q:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "OTHER_CELL_NETWORK"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->a:Lcom/google/r/b/a/dh;

    .line 18
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "EDGE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->b:Lcom/google/r/b/a/dh;

    .line 22
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "GPRS"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->c:Lcom/google/r/b/a/dh;

    .line 26
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "UMTS"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->d:Lcom/google/r/b/a/dh;

    .line 30
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "CDMA"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->e:Lcom/google/r/b/a/dh;

    .line 34
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "CDMA2000_1XRTT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->f:Lcom/google/r/b/a/dh;

    .line 38
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "CDMA2000_1XEVDO_0"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->g:Lcom/google/r/b/a/dh;

    .line 42
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "CDMA2000_1XEVDO_A"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->h:Lcom/google/r/b/a/dh;

    .line 46
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "CDMA2000_1XEVDO_B"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->i:Lcom/google/r/b/a/dh;

    .line 50
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "IDEN"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->j:Lcom/google/r/b/a/dh;

    .line 54
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "HSPA"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->k:Lcom/google/r/b/a/dh;

    .line 58
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "HSDPA"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->l:Lcom/google/r/b/a/dh;

    .line 62
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "HSUPA"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->m:Lcom/google/r/b/a/dh;

    .line 66
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "HSPAP"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->n:Lcom/google/r/b/a/dh;

    .line 70
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "EHRPD"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->o:Lcom/google/r/b/a/dh;

    .line 74
    new-instance v0, Lcom/google/r/b/a/dh;

    const-string v1, "LTE"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/dh;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/dh;->p:Lcom/google/r/b/a/dh;

    .line 9
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/r/b/a/dh;

    sget-object v1, Lcom/google/r/b/a/dh;->a:Lcom/google/r/b/a/dh;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/dh;->b:Lcom/google/r/b/a/dh;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/dh;->c:Lcom/google/r/b/a/dh;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/dh;->d:Lcom/google/r/b/a/dh;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/dh;->e:Lcom/google/r/b/a/dh;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/dh;->f:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/dh;->g:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/r/b/a/dh;->h:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/dh;->i:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/r/b/a/dh;->j:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/r/b/a/dh;->k:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/r/b/a/dh;->l:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/r/b/a/dh;->m:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/r/b/a/dh;->n:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/r/b/a/dh;->o:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/r/b/a/dh;->p:Lcom/google/r/b/a/dh;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/dh;->r:[Lcom/google/r/b/a/dh;

    .line 174
    new-instance v0, Lcom/google/r/b/a/di;

    invoke-direct {v0}, Lcom/google/r/b/a/di;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 184
    iput p3, p0, Lcom/google/r/b/a/dh;->q:I

    .line 185
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/dh;
    .locals 1

    .prologue
    .line 148
    packed-switch p0, :pswitch_data_0

    .line 165
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 149
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/dh;->a:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 150
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/dh;->b:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 151
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/dh;->c:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 152
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/dh;->d:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 153
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/dh;->e:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 154
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/dh;->f:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 155
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/dh;->g:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 156
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/dh;->h:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 157
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/dh;->i:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 158
    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/dh;->j:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 159
    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/dh;->k:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 160
    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/dh;->l:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 161
    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/dh;->m:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 162
    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/dh;->n:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 163
    :pswitch_e
    sget-object v0, Lcom/google/r/b/a/dh;->o:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 164
    :pswitch_f
    sget-object v0, Lcom/google/r/b/a/dh;->p:Lcom/google/r/b/a/dh;

    goto :goto_0

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/dh;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/r/b/a/dh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/dh;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/dh;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/r/b/a/dh;->r:[Lcom/google/r/b/a/dh;

    invoke-virtual {v0}, [Lcom/google/r/b/a/dh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/dh;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/google/r/b/a/dh;->q:I

    return v0
.end method
