.class public final Lcom/google/r/b/a/amb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/alz;",
        "Lcom/google/r/b/a/amb;",
        ">;",
        "Lcom/google/r/b/a/amc;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:I

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/f;

.field private g:Z

.field private h:Z

.field private i:I

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 569
    sget-object v0, Lcom/google/r/b/a/alz;->l:Lcom/google/r/b/a/alz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 711
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amb;->b:Lcom/google/n/ao;

    .line 770
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/amb;->g:Z

    .line 834
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/r/b/a/amb;->c:I

    .line 898
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amb;->j:Lcom/google/n/ao;

    .line 957
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amb;->d:Lcom/google/n/ao;

    .line 1016
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amb;->k:Lcom/google/n/ao;

    .line 1075
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amb;->e:Lcom/google/n/ao;

    .line 1134
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/amb;->f:Lcom/google/n/f;

    .line 570
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 561
    new-instance v2, Lcom/google/r/b/a/alz;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/alz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/amb;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/alz;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amb;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amb;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/r/b/a/amb;->g:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/alz;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v4, p0, Lcom/google/r/b/a/amb;->h:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/alz;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/r/b/a/amb;->c:I

    iput v4, v2, Lcom/google/r/b/a/alz;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/r/b/a/amb;->i:I

    iput v4, v2, Lcom/google/r/b/a/alz;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/alz;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amb;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amb;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/alz;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amb;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amb;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amb;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amb;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/r/b/a/alz;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/amb;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/amb;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/amb;->f:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/alz;->k:Lcom/google/n/f;

    iput v0, v2, Lcom/google/r/b/a/alz;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 561
    check-cast p1, Lcom/google/r/b/a/alz;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/amb;->a(Lcom/google/r/b/a/alz;)Lcom/google/r/b/a/amb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/alz;)Lcom/google/r/b/a/amb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 659
    invoke-static {}, Lcom/google/r/b/a/alz;->d()Lcom/google/r/b/a/alz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 696
    :goto_0
    return-object p0

    .line 660
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 661
    iget-object v2, p0, Lcom/google/r/b/a/amb;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alz;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 662
    iget v2, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/amb;->a:I

    .line 664
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 665
    iget-boolean v2, p1, Lcom/google/r/b/a/alz;->c:Z

    iget v3, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/amb;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/amb;->g:Z

    .line 667
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 668
    iget-boolean v2, p1, Lcom/google/r/b/a/alz;->d:Z

    iget v3, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/amb;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/amb;->h:Z

    .line 670
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 671
    iget v2, p1, Lcom/google/r/b/a/alz;->e:I

    iget v3, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/amb;->a:I

    iput v2, p0, Lcom/google/r/b/a/amb;->c:I

    .line 673
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 674
    iget v2, p1, Lcom/google/r/b/a/alz;->f:I

    iget v3, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/amb;->a:I

    iput v2, p0, Lcom/google/r/b/a/amb;->i:I

    .line 676
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 677
    iget-object v2, p0, Lcom/google/r/b/a/amb;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alz;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 678
    iget v2, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/amb;->a:I

    .line 680
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 681
    iget-object v2, p0, Lcom/google/r/b/a/amb;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alz;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 682
    iget v2, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/amb;->a:I

    .line 684
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 685
    iget-object v2, p0, Lcom/google/r/b/a/amb;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 686
    iget v2, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/amb;->a:I

    .line 688
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 689
    iget-object v2, p0, Lcom/google/r/b/a/amb;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/alz;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 690
    iget v2, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/r/b/a/amb;->a:I

    .line 692
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_13

    :goto_a
    if-eqz v0, :cond_15

    .line 693
    iget-object v0, p1, Lcom/google/r/b/a/alz;->k:Lcom/google/n/f;

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 660
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 664
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 667
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 670
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 673
    goto/16 :goto_5

    :cond_f
    move v2, v1

    .line 676
    goto :goto_6

    :cond_10
    move v2, v1

    .line 680
    goto :goto_7

    :cond_11
    move v2, v1

    .line 684
    goto :goto_8

    :cond_12
    move v2, v1

    .line 688
    goto :goto_9

    :cond_13
    move v0, v1

    .line 692
    goto :goto_a

    .line 693
    :cond_14
    iget v1, p0, Lcom/google/r/b/a/amb;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/r/b/a/amb;->a:I

    iput-object v0, p0, Lcom/google/r/b/a/amb;->f:Lcom/google/n/f;

    .line 695
    :cond_15
    iget-object v0, p1, Lcom/google/r/b/a/alz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 700
    iget v0, p0, Lcom/google/r/b/a/amb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/google/r/b/a/amb;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 706
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 700
    goto :goto_0

    :cond_1
    move v0, v2

    .line 706
    goto :goto_1
.end method
