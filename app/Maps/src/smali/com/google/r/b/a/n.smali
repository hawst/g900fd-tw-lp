.class public final Lcom/google/r/b/a/n;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/q;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/n;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/n;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/f;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2801
    new-instance v0, Lcom/google/r/b/a/o;

    invoke-direct {v0}, Lcom/google/r/b/a/o;-><init>()V

    sput-object v0, Lcom/google/r/b/a/n;->PARSER:Lcom/google/n/ax;

    .line 2914
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/n;->h:Lcom/google/n/aw;

    .line 3215
    new-instance v0, Lcom/google/r/b/a/n;

    invoke-direct {v0}, Lcom/google/r/b/a/n;-><init>()V

    sput-object v0, Lcom/google/r/b/a/n;->e:Lcom/google/r/b/a/n;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2746
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2833
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/n;->c:Lcom/google/n/ao;

    .line 2849
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/n;->d:Lcom/google/n/ao;

    .line 2864
    iput-byte v2, p0, Lcom/google/r/b/a/n;->f:B

    .line 2889
    iput v2, p0, Lcom/google/r/b/a/n;->g:I

    .line 2747
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/n;->b:Lcom/google/n/f;

    .line 2748
    iget-object v0, p0, Lcom/google/r/b/a/n;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 2749
    iget-object v0, p0, Lcom/google/r/b/a/n;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 2750
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2756
    invoke-direct {p0}, Lcom/google/r/b/a/n;-><init>()V

    .line 2757
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2762
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2763
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2764
    sparse-switch v3, :sswitch_data_0

    .line 2769
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2771
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2767
    goto :goto_0

    .line 2776
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/n;->a:I

    .line 2777
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/r/b/a/n;->b:Lcom/google/n/f;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2792
    :catch_0
    move-exception v0

    .line 2793
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2798
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/n;->au:Lcom/google/n/bn;

    throw v0

    .line 2781
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/n;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 2782
    iget v3, p0, Lcom/google/r/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/n;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2794
    :catch_1
    move-exception v0

    .line 2795
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2796
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2786
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/n;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 2787
    iget v3, p0, Lcom/google/r/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/n;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2798
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/n;->au:Lcom/google/n/bn;

    .line 2799
    return-void

    .line 2764
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2744
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2833
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/n;->c:Lcom/google/n/ao;

    .line 2849
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/n;->d:Lcom/google/n/ao;

    .line 2864
    iput-byte v1, p0, Lcom/google/r/b/a/n;->f:B

    .line 2889
    iput v1, p0, Lcom/google/r/b/a/n;->g:I

    .line 2745
    return-void
.end method

.method public static d()Lcom/google/r/b/a/n;
    .locals 1

    .prologue
    .line 3218
    sget-object v0, Lcom/google/r/b/a/n;->e:Lcom/google/r/b/a/n;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/p;
    .locals 1

    .prologue
    .line 2976
    new-instance v0, Lcom/google/r/b/a/p;

    invoke-direct {v0}, Lcom/google/r/b/a/p;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2813
    sget-object v0, Lcom/google/r/b/a/n;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2876
    invoke-virtual {p0}, Lcom/google/r/b/a/n;->c()I

    .line 2877
    iget v0, p0, Lcom/google/r/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2878
    iget-object v0, p0, Lcom/google/r/b/a/n;->b:Lcom/google/n/f;

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2880
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2881
    iget-object v0, p0, Lcom/google/r/b/a/n;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2883
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2884
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/n;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2886
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/n;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2887
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2866
    iget-byte v1, p0, Lcom/google/r/b/a/n;->f:B

    .line 2867
    if-ne v1, v0, :cond_0

    .line 2871
    :goto_0
    return v0

    .line 2868
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2870
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/n;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2891
    iget v0, p0, Lcom/google/r/b/a/n;->g:I

    .line 2892
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2909
    :goto_0
    return v0

    .line 2895
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 2896
    iget-object v0, p0, Lcom/google/r/b/a/n;->b:Lcom/google/n/f;

    .line 2897
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 2899
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2900
    iget-object v2, p0, Lcom/google/r/b/a/n;->c:Lcom/google/n/ao;

    .line 2901
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2903
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 2904
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/n;->d:Lcom/google/n/ao;

    .line 2905
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2907
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/n;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2908
    iput v0, p0, Lcom/google/r/b/a/n;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2738
    invoke-static {}, Lcom/google/r/b/a/n;->newBuilder()Lcom/google/r/b/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/p;->a(Lcom/google/r/b/a/n;)Lcom/google/r/b/a/p;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2738
    invoke-static {}, Lcom/google/r/b/a/n;->newBuilder()Lcom/google/r/b/a/p;

    move-result-object v0

    return-object v0
.end method
