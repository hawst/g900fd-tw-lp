.class public final Lcom/google/r/b/a/ael;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aeq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ael;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final t:Lcom/google/r/b/a/ael;

.field private static volatile w:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field g:Lcom/google/n/ao;

.field h:Z

.field i:Z

.field j:Lcom/google/n/ao;

.field public k:Lcom/google/n/ao;

.field l:Lcom/google/n/ao;

.field m:Lcom/google/n/ao;

.field n:Lcom/google/n/ao;

.field public o:Lcom/google/n/ao;

.field p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field q:Lcom/google/n/ao;

.field r:Lcom/google/n/ao;

.field s:Lcom/google/n/ao;

.field private u:B

.field private v:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 415
    new-instance v0, Lcom/google/r/b/a/aem;

    invoke-direct {v0}, Lcom/google/r/b/a/aem;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ael;->PARSER:Lcom/google/n/ax;

    .line 550
    new-instance v0, Lcom/google/r/b/a/aen;

    invoke-direct {v0}, Lcom/google/r/b/a/aen;-><init>()V

    .line 723
    new-instance v0, Lcom/google/r/b/a/aeo;

    invoke-direct {v0}, Lcom/google/r/b/a/aeo;-><init>()V

    .line 970
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ael;->w:Lcom/google/n/aw;

    .line 2381
    new-instance v0, Lcom/google/r/b/a/ael;

    invoke-direct {v0}, Lcom/google/r/b/a/ael;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ael;->t:Lcom/google/r/b/a/ael;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 206
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 516
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->d:Lcom/google/n/ao;

    .line 532
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->e:Lcom/google/n/ao;

    .line 579
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->g:Lcom/google/n/ao;

    .line 625
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->j:Lcom/google/n/ao;

    .line 641
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    .line 657
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->l:Lcom/google/n/ao;

    .line 673
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    .line 689
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->n:Lcom/google/n/ao;

    .line 705
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    .line 752
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->q:Lcom/google/n/ao;

    .line 768
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->r:Lcom/google/n/ao;

    .line 784
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->s:Lcom/google/n/ao;

    .line 799
    iput-byte v3, p0, Lcom/google/r/b/a/ael;->u:B

    .line 875
    iput v3, p0, Lcom/google/r/b/a/ael;->v:I

    .line 207
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    .line 208
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    .line 209
    iget-object v0, p0, Lcom/google/r/b/a/ael;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 210
    iget-object v0, p0, Lcom/google/r/b/a/ael;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 211
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    .line 212
    iget-object v0, p0, Lcom/google/r/b/a/ael;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 213
    iput-boolean v2, p0, Lcom/google/r/b/a/ael;->h:Z

    .line 214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/ael;->i:Z

    .line 215
    iget-object v0, p0, Lcom/google/r/b/a/ael;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 216
    iget-object v0, p0, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 217
    iget-object v0, p0, Lcom/google/r/b/a/ael;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 218
    iget-object v0, p0, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 219
    iget-object v0, p0, Lcom/google/r/b/a/ael;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 220
    iget-object v0, p0, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 221
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    .line 222
    iget-object v0, p0, Lcom/google/r/b/a/ael;->q:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 223
    iget-object v0, p0, Lcom/google/r/b/a/ael;->r:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 224
    iget-object v0, p0, Lcom/google/r/b/a/ael;->s:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 225
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/16 v9, 0x4000

    const/16 v8, 0x10

    const/4 v0, 0x0

    .line 231
    invoke-direct {p0}, Lcom/google/r/b/a/ael;-><init>()V

    .line 234
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 237
    :cond_0
    :goto_0
    if-nez v3, :cond_f

    .line 238
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 239
    sparse-switch v0, :sswitch_data_0

    .line 244
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 246
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 242
    goto :goto_0

    .line 251
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 252
    iget v6, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/r/b/a/ael;->a:I

    .line 253
    iput-object v0, p0, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 400
    :catch_0
    move-exception v0

    .line 401
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x10

    if-ne v2, v8, :cond_1

    .line 407
    iget-object v2, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    .line 409
    :cond_1
    and-int/lit16 v1, v1, 0x4000

    if-ne v1, v9, :cond_2

    .line 410
    iget-object v1, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    .line 412
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ael;->au:Lcom/google/n/bn;

    throw v0

    .line 257
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 258
    iget v6, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/r/b/a/ael;->a:I

    .line 259
    iput-object v0, p0, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 402
    :catch_1
    move-exception v0

    .line 403
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 404
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 263
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/r/b/a/ael;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 264
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto :goto_0

    .line 268
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 269
    invoke-static {v0}, Lcom/google/r/b/a/acy;->a(I)Lcom/google/r/b/a/acy;

    move-result-object v6

    .line 270
    if-nez v6, :cond_3

    .line 271
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 273
    :cond_3
    and-int/lit8 v6, v1, 0x10

    if-eq v6, v8, :cond_4

    .line 274
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    .line 275
    or-int/lit8 v1, v1, 0x10

    .line 277
    :cond_4
    iget-object v6, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 282
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 283
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 284
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_5

    move v0, v2

    :goto_2
    if-lez v0, :cond_8

    .line 285
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 286
    invoke-static {v0}, Lcom/google/r/b/a/acy;->a(I)Lcom/google/r/b/a/acy;

    move-result-object v7

    .line 287
    if-nez v7, :cond_6

    .line 288
    const/4 v7, 0x4

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 284
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_2

    .line 290
    :cond_6
    and-int/lit8 v7, v1, 0x10

    if-eq v7, v8, :cond_7

    .line 291
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    .line 292
    or-int/lit8 v1, v1, 0x10

    .line 294
    :cond_7
    iget-object v7, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 297
    :cond_8
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 301
    :sswitch_6
    iget-object v0, p0, Lcom/google/r/b/a/ael;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 302
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 306
    :sswitch_7
    iget-object v0, p0, Lcom/google/r/b/a/ael;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 307
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 311
    :sswitch_8
    iget-object v0, p0, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 312
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 316
    :sswitch_9
    iget-object v0, p0, Lcom/google/r/b/a/ael;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 317
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 321
    :sswitch_a
    iget-object v0, p0, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 322
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 326
    :sswitch_b
    iget-object v0, p0, Lcom/google/r/b/a/ael;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 327
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 331
    :sswitch_c
    iget-object v0, p0, Lcom/google/r/b/a/ael;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 332
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 336
    :sswitch_d
    iget-object v0, p0, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 337
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 341
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 342
    invoke-static {v0}, Lcom/google/maps/g/s;->a(I)Lcom/google/maps/g/s;

    move-result-object v6

    .line 343
    if-nez v6, :cond_9

    .line 344
    const/16 v6, 0xd

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 346
    :cond_9
    and-int/lit16 v6, v1, 0x4000

    if-eq v6, v9, :cond_a

    .line 347
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    .line 348
    or-int/lit16 v1, v1, 0x4000

    .line 350
    :cond_a
    iget-object v6, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 355
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 356
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 357
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_b

    move v0, v2

    :goto_4
    if-lez v0, :cond_e

    .line 358
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 359
    invoke-static {v0}, Lcom/google/maps/g/s;->a(I)Lcom/google/maps/g/s;

    move-result-object v7

    .line 360
    if-nez v7, :cond_c

    .line 361
    const/16 v7, 0xd

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_3

    .line 357
    :cond_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 363
    :cond_c
    and-int/lit16 v7, v1, 0x4000

    if-eq v7, v9, :cond_d

    .line 364
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    .line 365
    or-int/lit16 v1, v1, 0x4000

    .line 367
    :cond_d
    iget-object v7, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 370
    :cond_e
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 374
    :sswitch_10
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    .line 375
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/ael;->h:Z

    goto/16 :goto_0

    .line 379
    :sswitch_11
    iget-object v0, p0, Lcom/google/r/b/a/ael;->q:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 380
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 384
    :sswitch_12
    iget-object v0, p0, Lcom/google/r/b/a/ael;->r:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 385
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    goto/16 :goto_0

    .line 389
    :sswitch_13
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I

    .line 390
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/ael;->i:Z

    goto/16 :goto_0

    .line 394
    :sswitch_14
    iget-object v0, p0, Lcom/google/r/b/a/ael;->s:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 395
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    const v6, 0x8000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/r/b/a/ael;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 406
    :cond_f
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v8, :cond_10

    .line 407
    iget-object v0, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    .line 409
    :cond_10
    and-int/lit16 v0, v1, 0x4000

    if-ne v0, v9, :cond_11

    .line 410
    iget-object v0, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    .line 412
    :cond_11
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->au:Lcom/google/n/bn;

    .line 413
    return-void

    .line 239
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
        0x5a -> :sswitch_c
        0x62 -> :sswitch_d
        0x68 -> :sswitch_e
        0x6a -> :sswitch_f
        0x70 -> :sswitch_10
        0x7a -> :sswitch_11
        0x82 -> :sswitch_12
        0x88 -> :sswitch_13
        0x92 -> :sswitch_14
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 204
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 516
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->d:Lcom/google/n/ao;

    .line 532
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->e:Lcom/google/n/ao;

    .line 579
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->g:Lcom/google/n/ao;

    .line 625
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->j:Lcom/google/n/ao;

    .line 641
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    .line 657
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->l:Lcom/google/n/ao;

    .line 673
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    .line 689
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->n:Lcom/google/n/ao;

    .line 705
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    .line 752
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->q:Lcom/google/n/ao;

    .line 768
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->r:Lcom/google/n/ao;

    .line 784
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ael;->s:Lcom/google/n/ao;

    .line 799
    iput-byte v1, p0, Lcom/google/r/b/a/ael;->u:B

    .line 875
    iput v1, p0, Lcom/google/r/b/a/ael;->v:I

    .line 205
    return-void
.end method

.method public static g()Lcom/google/r/b/a/ael;
    .locals 1

    .prologue
    .line 2384
    sget-object v0, Lcom/google/r/b/a/ael;->t:Lcom/google/r/b/a/ael;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aep;
    .locals 1

    .prologue
    .line 1032
    new-instance v0, Lcom/google/r/b/a/aep;

    invoke-direct {v0}, Lcom/google/r/b/a/aep;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ael;",
            ">;"
        }
    .end annotation

    .prologue
    .line 427
    sget-object v0, Lcom/google/r/b/a/ael;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 817
    invoke-virtual {p0}, Lcom/google/r/b/a/ael;->c()I

    .line 818
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 819
    iget-object v0, p0, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 821
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 822
    iget-object v0, p0, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 824
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 825
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/ael;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_2
    move v1, v2

    .line 827
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 828
    iget-object v0, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 827
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 819
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 822
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 830
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 831
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/r/b/a/ael;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 833
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 834
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/ael;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 836
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 837
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 839
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 840
    iget-object v0, p0, Lcom/google/r/b/a/ael;->l:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 842
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 843
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 845
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_b

    .line 846
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/r/b/a/ael;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 848
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_c

    .line 849
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/r/b/a/ael;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 851
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_d

    .line 852
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 854
    :cond_d
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 855
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 854
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 857
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_f

    .line 858
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/r/b/a/ael;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 860
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_10

    .line 861
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/r/b/a/ael;->q:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 863
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_11

    .line 864
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/r/b/a/ael;->r:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 866
    :cond_11
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_12

    .line 867
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/r/b/a/ael;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 869
    :cond_12
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_13

    .line 870
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/r/b/a/ael;->s:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 872
    :cond_13
    iget-object v0, p0, Lcom/google/r/b/a/ael;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 873
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 801
    iget-byte v0, p0, Lcom/google/r/b/a/ael;->u:B

    .line 802
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 812
    :goto_0
    return v0

    .line 803
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 805
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 806
    iget-object v0, p0, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 807
    iput-byte v2, p0, Lcom/google/r/b/a/ael;->u:B

    move v0, v2

    .line 808
    goto :goto_0

    :cond_2
    move v0, v2

    .line 805
    goto :goto_1

    .line 811
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/ael;->u:B

    move v0, v1

    .line 812
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v5, 0xa

    const/4 v2, 0x0

    .line 877
    iget v0, p0, Lcom/google/r/b/a/ael;->v:I

    .line 878
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 965
    :goto_0
    return v0

    .line 881
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_16

    .line 883
    iget-object v0, p0, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 885
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 887
    iget-object v0, p0, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 889
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 890
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/ael;->e:Lcom/google/n/ao;

    .line 891
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_2
    move v3, v2

    move v4, v2

    .line 895
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 896
    iget-object v0, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    .line 897
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v4, v0

    .line 895
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 883
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 887
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_5
    move v0, v5

    .line 897
    goto :goto_5

    .line 899
    :cond_6
    add-int v0, v1, v4

    .line 900
    iget-object v1, p0, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 902
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_7

    .line 903
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/r/b/a/ael;->g:Lcom/google/n/ao;

    .line 904
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 906
    :cond_7
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_8

    .line 907
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/r/b/a/ael;->j:Lcom/google/n/ao;

    .line 908
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 910
    :cond_8
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_9

    .line 911
    const/4 v1, 0x7

    iget-object v3, p0, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    .line 912
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 914
    :cond_9
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_a

    .line 915
    iget-object v1, p0, Lcom/google/r/b/a/ael;->l:Lcom/google/n/ao;

    .line 916
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 918
    :cond_a
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_b

    .line 919
    const/16 v1, 0x9

    iget-object v3, p0, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    .line 920
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 922
    :cond_b
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_c

    .line 923
    iget-object v1, p0, Lcom/google/r/b/a/ael;->d:Lcom/google/n/ao;

    .line 924
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 926
    :cond_c
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_d

    .line 927
    const/16 v1, 0xb

    iget-object v3, p0, Lcom/google/r/b/a/ael;->n:Lcom/google/n/ao;

    .line 928
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 930
    :cond_d
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_15

    .line 931
    const/16 v1, 0xc

    iget-object v3, p0, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    .line 932
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    :goto_6
    move v3, v2

    move v4, v2

    .line 936
    :goto_7
    iget-object v0, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_f

    .line 937
    iget-object v0, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    .line 938
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v4, v0

    .line 936
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    :cond_e
    move v0, v5

    .line 938
    goto :goto_8

    .line 940
    :cond_f
    add-int v0, v1, v4

    .line 941
    iget-object v1, p0, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 943
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_10

    .line 944
    const/16 v1, 0xe

    iget-boolean v3, p0, Lcom/google/r/b/a/ael;->h:Z

    .line 945
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 947
    :cond_10
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_11

    .line 948
    const/16 v1, 0xf

    iget-object v3, p0, Lcom/google/r/b/a/ael;->q:Lcom/google/n/ao;

    .line 949
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 951
    :cond_11
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_12

    .line 952
    const/16 v1, 0x10

    iget-object v3, p0, Lcom/google/r/b/a/ael;->r:Lcom/google/n/ao;

    .line 953
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 955
    :cond_12
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_13

    .line 956
    const/16 v1, 0x11

    iget-boolean v3, p0, Lcom/google/r/b/a/ael;->i:Z

    .line 957
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 959
    :cond_13
    iget v1, p0, Lcom/google/r/b/a/ael;->a:I

    const v3, 0x8000

    and-int/2addr v1, v3

    const v3, 0x8000

    if-ne v1, v3, :cond_14

    .line 960
    const/16 v1, 0x12

    iget-object v3, p0, Lcom/google/r/b/a/ael;->s:Lcom/google/n/ao;

    .line 961
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 963
    :cond_14
    iget-object v1, p0, Lcom/google/r/b/a/ael;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 964
    iput v0, p0, Lcom/google/r/b/a/ael;->v:I

    goto/16 :goto_0

    :cond_15
    move v1, v0

    goto/16 :goto_6

    :cond_16
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    .line 444
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 445
    check-cast v0, Ljava/lang/String;

    .line 453
    :goto_0
    return-object v0

    .line 447
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 449
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 450
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451
    iput-object v1, p0, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 453
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 198
    invoke-static {}, Lcom/google/r/b/a/ael;->newBuilder()Lcom/google/r/b/a/aep;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aep;->a(Lcom/google/r/b/a/ael;)Lcom/google/r/b/a/aep;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 198
    invoke-static {}, Lcom/google/r/b/a/ael;->newBuilder()Lcom/google/r/b/a/aep;

    move-result-object v0

    return-object v0
.end method
