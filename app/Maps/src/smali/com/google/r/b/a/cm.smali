.class public final enum Lcom/google/r/b/a/cm;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/cm;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/cm;

.field public static final enum b:Lcom/google/r/b/a/cm;

.field private static final synthetic d:[Lcom/google/r/b/a/cm;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1253
    new-instance v0, Lcom/google/r/b/a/cm;

    const-string v1, "PLATFORM_V1"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/r/b/a/cm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/cm;->a:Lcom/google/r/b/a/cm;

    .line 1257
    new-instance v0, Lcom/google/r/b/a/cm;

    const-string v1, "PLATFORM_V2"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/r/b/a/cm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/cm;->b:Lcom/google/r/b/a/cm;

    .line 1248
    new-array v0, v4, [Lcom/google/r/b/a/cm;

    sget-object v1, Lcom/google/r/b/a/cm;->a:Lcom/google/r/b/a/cm;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/cm;->b:Lcom/google/r/b/a/cm;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/r/b/a/cm;->d:[Lcom/google/r/b/a/cm;

    .line 1287
    new-instance v0, Lcom/google/r/b/a/cn;

    invoke-direct {v0}, Lcom/google/r/b/a/cn;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1296
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1297
    iput p3, p0, Lcom/google/r/b/a/cm;->c:I

    .line 1298
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/cm;
    .locals 1

    .prologue
    .line 1275
    packed-switch p0, :pswitch_data_0

    .line 1278
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1276
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/cm;->a:Lcom/google/r/b/a/cm;

    goto :goto_0

    .line 1277
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/cm;->b:Lcom/google/r/b/a/cm;

    goto :goto_0

    .line 1275
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/cm;
    .locals 1

    .prologue
    .line 1248
    const-class v0, Lcom/google/r/b/a/cm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/cm;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/cm;
    .locals 1

    .prologue
    .line 1248
    sget-object v0, Lcom/google/r/b/a/cm;->d:[Lcom/google/r/b/a/cm;

    invoke-virtual {v0}, [Lcom/google/r/b/a/cm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/cm;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1271
    iget v0, p0, Lcom/google/r/b/a/cm;->c:I

    return v0
.end method
