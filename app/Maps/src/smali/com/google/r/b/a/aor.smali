.class public final Lcom/google/r/b/a/aor;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aou;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aor;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/aor;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/aq;

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/google/r/b/a/aos;

    invoke-direct {v0}, Lcom/google/r/b/a/aos;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aor;->PARSER:Lcom/google/n/ax;

    .line 557
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aor;->g:Lcom/google/n/aw;

    .line 853
    new-instance v0, Lcom/google/r/b/a/aor;

    invoke-direct {v0}, Lcom/google/r/b/a/aor;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aor;->d:Lcom/google/r/b/a/aor;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 494
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aor;->c:Lcom/google/n/ao;

    .line 509
    iput-byte v2, p0, Lcom/google/r/b/a/aor;->e:B

    .line 531
    iput v2, p0, Lcom/google/r/b/a/aor;->f:I

    .line 55
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    .line 56
    iget-object v0, p0, Lcom/google/r/b/a/aor;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 63
    invoke-direct {p0}, Lcom/google/r/b/a/aor;-><init>()V

    .line 66
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 69
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 71
    sparse-switch v4, :sswitch_data_0

    .line 76
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 78
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 84
    and-int/lit8 v5, v1, 0x1

    if-eq v5, v2, :cond_1

    .line 85
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    .line 86
    or-int/lit8 v1, v1, 0x1

    .line 88
    :cond_1
    iget-object v5, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 105
    iget-object v1, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    .line 107
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aor;->au:Lcom/google/n/bn;

    throw v0

    .line 92
    :sswitch_2
    :try_start_2
    iget-object v4, p0, Lcom/google/r/b/a/aor;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 93
    iget v4, p0, Lcom/google/r/b/a/aor;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/aor;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 100
    :catch_1
    move-exception v0

    .line 101
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 102
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 104
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 105
    iget-object v0, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    .line 107
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aor;->au:Lcom/google/n/bn;

    .line 108
    return-void

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 52
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 494
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aor;->c:Lcom/google/n/ao;

    .line 509
    iput-byte v1, p0, Lcom/google/r/b/a/aor;->e:B

    .line 531
    iput v1, p0, Lcom/google/r/b/a/aor;->f:I

    .line 53
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aor;
    .locals 1

    .prologue
    .line 856
    sget-object v0, Lcom/google/r/b/a/aor;->d:Lcom/google/r/b/a/aor;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aot;
    .locals 1

    .prologue
    .line 619
    new-instance v0, Lcom/google/r/b/a/aot;

    invoke-direct {v0}, Lcom/google/r/b/a/aot;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    sget-object v0, Lcom/google/r/b/a/aor;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 521
    invoke-virtual {p0}, Lcom/google/r/b/a/aor;->c()I

    .line 522
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 523
    iget-object v1, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v1, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 522
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 525
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aor;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 526
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/r/b/a/aor;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 528
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aor;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 529
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 511
    iget-byte v1, p0, Lcom/google/r/b/a/aor;->e:B

    .line 512
    if-ne v1, v0, :cond_0

    .line 516
    :goto_0
    return v0

    .line 513
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 515
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aor;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 533
    iget v0, p0, Lcom/google/r/b/a/aor;->f:I

    .line 534
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 552
    :goto_0
    return v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 539
    :goto_1
    iget-object v3, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 540
    iget-object v3, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    .line 541
    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 539
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 543
    :cond_1
    add-int/lit8 v0, v2, 0x0

    .line 544
    iget-object v2, p0, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 546
    iget v2, p0, Lcom/google/r/b/a/aor;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 547
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/r/b/a/aor;->c:Lcom/google/n/ao;

    .line 548
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 550
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/aor;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 551
    iput v0, p0, Lcom/google/r/b/a/aor;->f:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/r/b/a/aor;->newBuilder()Lcom/google/r/b/a/aot;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aot;->a(Lcom/google/r/b/a/aor;)Lcom/google/r/b/a/aot;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/r/b/a/aor;->newBuilder()Lcom/google/r/b/a/aot;

    move-result-object v0

    return-object v0
.end method
