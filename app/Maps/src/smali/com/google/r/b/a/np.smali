.class public final enum Lcom/google/r/b/a/np;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/np;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/np;

.field public static final enum b:Lcom/google/r/b/a/np;

.field public static final enum c:Lcom/google/r/b/a/np;

.field public static final enum d:Lcom/google/r/b/a/np;

.field public static final enum e:Lcom/google/r/b/a/np;

.field public static final enum f:Lcom/google/r/b/a/np;

.field public static final enum g:Lcom/google/r/b/a/np;

.field private static final synthetic i:[Lcom/google/r/b/a/np;


# instance fields
.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1053
    new-instance v0, Lcom/google/r/b/a/np;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/np;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/np;->a:Lcom/google/r/b/a/np;

    .line 1057
    new-instance v0, Lcom/google/r/b/a/np;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/np;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/np;->b:Lcom/google/r/b/a/np;

    .line 1061
    new-instance v0, Lcom/google/r/b/a/np;

    const-string v1, "NOT_AUTHORIZED"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/np;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/np;->c:Lcom/google/r/b/a/np;

    .line 1065
    new-instance v0, Lcom/google/r/b/a/np;

    const-string v1, "NOT_FOUND"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/r/b/a/np;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/np;->d:Lcom/google/r/b/a/np;

    .line 1069
    new-instance v0, Lcom/google/r/b/a/np;

    const-string v1, "UNSUPPORTED"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/r/b/a/np;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/np;->e:Lcom/google/r/b/a/np;

    .line 1073
    new-instance v0, Lcom/google/r/b/a/np;

    const-string v1, "BAD_REQUEST"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/np;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/np;->f:Lcom/google/r/b/a/np;

    .line 1077
    new-instance v0, Lcom/google/r/b/a/np;

    const-string v1, "FAILURE"

    const/4 v2, 0x6

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/np;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/np;->g:Lcom/google/r/b/a/np;

    .line 1048
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/r/b/a/np;

    sget-object v1, Lcom/google/r/b/a/np;->a:Lcom/google/r/b/a/np;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/np;->b:Lcom/google/r/b/a/np;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/np;->c:Lcom/google/r/b/a/np;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/np;->d:Lcom/google/r/b/a/np;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/np;->e:Lcom/google/r/b/a/np;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/np;->f:Lcom/google/r/b/a/np;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/np;->g:Lcom/google/r/b/a/np;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/np;->i:[Lcom/google/r/b/a/np;

    .line 1132
    new-instance v0, Lcom/google/r/b/a/nq;

    invoke-direct {v0}, Lcom/google/r/b/a/nq;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1142
    iput p3, p0, Lcom/google/r/b/a/np;->h:I

    .line 1143
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/np;
    .locals 1

    .prologue
    .line 1115
    sparse-switch p0, :sswitch_data_0

    .line 1123
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1116
    :sswitch_0
    sget-object v0, Lcom/google/r/b/a/np;->a:Lcom/google/r/b/a/np;

    goto :goto_0

    .line 1117
    :sswitch_1
    sget-object v0, Lcom/google/r/b/a/np;->b:Lcom/google/r/b/a/np;

    goto :goto_0

    .line 1118
    :sswitch_2
    sget-object v0, Lcom/google/r/b/a/np;->c:Lcom/google/r/b/a/np;

    goto :goto_0

    .line 1119
    :sswitch_3
    sget-object v0, Lcom/google/r/b/a/np;->d:Lcom/google/r/b/a/np;

    goto :goto_0

    .line 1120
    :sswitch_4
    sget-object v0, Lcom/google/r/b/a/np;->e:Lcom/google/r/b/a/np;

    goto :goto_0

    .line 1121
    :sswitch_5
    sget-object v0, Lcom/google/r/b/a/np;->f:Lcom/google/r/b/a/np;

    goto :goto_0

    .line 1122
    :sswitch_6
    sget-object v0, Lcom/google/r/b/a/np;->g:Lcom/google/r/b/a/np;

    goto :goto_0

    .line 1115
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x64 -> :sswitch_6
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/np;
    .locals 1

    .prologue
    .line 1048
    const-class v0, Lcom/google/r/b/a/np;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/np;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/np;
    .locals 1

    .prologue
    .line 1048
    sget-object v0, Lcom/google/r/b/a/np;->i:[Lcom/google/r/b/a/np;

    invoke-virtual {v0}, [Lcom/google/r/b/a/np;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/np;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1111
    iget v0, p0, Lcom/google/r/b/a/np;->h:I

    return v0
.end method
