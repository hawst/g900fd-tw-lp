.class public final Lcom/google/r/b/a/aha;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ahh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aha;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/r/b/a/aha;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field c:Lcom/google/n/f;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/f;",
            ">;"
        }
    .end annotation
.end field

.field e:I

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/f;

.field k:I

.field l:I

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field n:I

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5322
    new-instance v0, Lcom/google/r/b/a/ahb;

    invoke-direct {v0}, Lcom/google/r/b/a/ahb;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aha;->PARSER:Lcom/google/n/ax;

    .line 6135
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aha;->r:Lcom/google/n/aw;

    .line 7240
    new-instance v0, Lcom/google/r/b/a/aha;

    invoke-direct {v0}, Lcom/google/r/b/a/aha;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aha;->o:Lcom/google/r/b/a/aha;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5171
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5876
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aha;->h:Lcom/google/n/ao;

    .line 5892
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aha;->i:Lcom/google/n/ao;

    .line 6010
    iput-byte v4, p0, Lcom/google/r/b/a/aha;->p:B

    .line 6065
    iput v4, p0, Lcom/google/r/b/a/aha;->q:I

    .line 5172
    iput v3, p0, Lcom/google/r/b/a/aha;->b:I

    .line 5173
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/aha;->c:Lcom/google/n/f;

    .line 5174
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    .line 5175
    iput v2, p0, Lcom/google/r/b/a/aha;->e:I

    .line 5176
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    .line 5177
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    .line 5178
    iget-object v0, p0, Lcom/google/r/b/a/aha;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 5179
    iget-object v0, p0, Lcom/google/r/b/a/aha;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 5180
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/aha;->j:Lcom/google/n/f;

    .line 5181
    iput v2, p0, Lcom/google/r/b/a/aha;->k:I

    .line 5182
    iput v2, p0, Lcom/google/r/b/a/aha;->l:I

    .line 5183
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    .line 5184
    iput v2, p0, Lcom/google/r/b/a/aha;->n:I

    .line 5185
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v9, 0x20

    const/16 v8, 0x10

    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 5191
    invoke-direct {p0}, Lcom/google/r/b/a/aha;-><init>()V

    .line 5194
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 5197
    :cond_0
    :goto_0
    if-nez v2, :cond_7

    .line 5198
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 5199
    sparse-switch v1, :sswitch_data_0

    .line 5204
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 5206
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 5202
    goto :goto_0

    .line 5211
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 5212
    invoke-static {v1}, Lcom/google/maps/g/wq;->a(I)Lcom/google/maps/g/wq;

    move-result-object v5

    .line 5213
    if-nez v5, :cond_5

    .line 5214
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 5301
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 5302
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5307
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v7, :cond_1

    .line 5308
    iget-object v2, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    .line 5310
    :cond_1
    and-int/lit16 v2, v1, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_2

    .line 5311
    iget-object v2, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    .line 5313
    :cond_2
    and-int/lit8 v2, v1, 0x10

    if-ne v2, v8, :cond_3

    .line 5314
    iget-object v2, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    .line 5316
    :cond_3
    and-int/lit8 v1, v1, 0x20

    if-ne v1, v9, :cond_4

    .line 5317
    iget-object v1, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    .line 5319
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aha;->au:Lcom/google/n/bn;

    throw v0

    .line 5216
    :cond_5
    :try_start_2
    iget v5, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/aha;->a:I

    .line 5217
    iput v1, p0, Lcom/google/r/b/a/aha;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 5303
    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 5304
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 5305
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5222
    :sswitch_2
    :try_start_4
    iget v1, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/aha;->a:I

    .line 5223
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aha;->c:Lcom/google/n/f;

    goto/16 :goto_0

    .line 5307
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_2

    .line 5227
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v7, :cond_e

    .line 5228
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 5229
    or-int/lit8 v1, v0, 0x4

    .line 5231
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 5232
    goto/16 :goto_0

    .line 5235
    :sswitch_4
    :try_start_6
    iget-object v1, p0, Lcom/google/r/b/a/aha;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 5236
    iget v1, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/r/b/a/aha;->a:I

    goto/16 :goto_0

    .line 5240
    :sswitch_5
    iget-object v1, p0, Lcom/google/r/b/a/aha;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 5241
    iget v1, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/r/b/a/aha;->a:I

    goto/16 :goto_0

    .line 5245
    :sswitch_6
    iget v1, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/r/b/a/aha;->a:I

    .line 5246
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aha;->j:Lcom/google/n/f;

    goto/16 :goto_0

    .line 5250
    :sswitch_7
    iget v1, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/r/b/a/aha;->a:I

    .line 5251
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/aha;->k:I

    goto/16 :goto_0

    .line 5255
    :sswitch_8
    iget v1, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/aha;->a:I

    .line 5256
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/aha;->l:I

    goto/16 :goto_0

    .line 5260
    :sswitch_9
    iget v1, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/aha;->a:I

    .line 5261
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/aha;->e:I

    goto/16 :goto_0

    .line 5265
    :sswitch_a
    and-int/lit16 v1, v0, 0x800

    const/16 v5, 0x800

    if-eq v1, v5, :cond_d

    .line 5266
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 5268
    or-int/lit16 v1, v0, 0x800

    .line 5270
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 5271
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 5270
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 5272
    goto/16 :goto_0

    .line 5275
    :sswitch_b
    :try_start_8
    iget v1, p0, Lcom/google/r/b/a/aha;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/r/b/a/aha;->a:I

    .line 5276
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v1

    iput v1, p0, Lcom/google/r/b/a/aha;->n:I

    goto/16 :goto_0

    .line 5280
    :sswitch_c
    and-int/lit8 v1, v0, 0x10

    if-eq v1, v8, :cond_c

    .line 5281
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 5283
    or-int/lit8 v1, v0, 0x10

    .line 5285
    :goto_6
    :try_start_9
    iget-object v0, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 5286
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 5285
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 5287
    goto/16 :goto_0

    .line 5290
    :sswitch_d
    and-int/lit8 v1, v0, 0x20

    if-eq v1, v9, :cond_6

    .line 5291
    :try_start_a
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    .line 5293
    or-int/lit8 v0, v0, 0x20

    .line 5295
    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 5296
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 5295
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_0

    .line 5307
    :cond_7
    and-int/lit8 v1, v0, 0x4

    if-ne v1, v7, :cond_8

    .line 5308
    iget-object v1, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    .line 5310
    :cond_8
    and-int/lit16 v1, v0, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_9

    .line 5311
    iget-object v1, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    .line 5313
    :cond_9
    and-int/lit8 v1, v0, 0x10

    if-ne v1, v8, :cond_a

    .line 5314
    iget-object v1, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    .line 5316
    :cond_a
    and-int/lit8 v0, v0, 0x20

    if-ne v0, v9, :cond_b

    .line 5317
    iget-object v0, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    .line 5319
    :cond_b
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aha;->au:Lcom/google/n/bn;

    .line 5320
    return-void

    .line 5303
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 5301
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_c
    move v1, v0

    goto :goto_6

    :cond_d
    move v1, v0

    goto/16 :goto_5

    :cond_e
    move v1, v0

    goto/16 :goto_4

    .line 5199
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 5169
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5876
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aha;->h:Lcom/google/n/ao;

    .line 5892
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aha;->i:Lcom/google/n/ao;

    .line 6010
    iput-byte v1, p0, Lcom/google/r/b/a/aha;->p:B

    .line 6065
    iput v1, p0, Lcom/google/r/b/a/aha;->q:I

    .line 5170
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aha;
    .locals 1

    .prologue
    .line 7243
    sget-object v0, Lcom/google/r/b/a/aha;->o:Lcom/google/r/b/a/aha;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ahc;
    .locals 1

    .prologue
    .line 6197
    new-instance v0, Lcom/google/r/b/a/ahc;

    invoke-direct {v0}, Lcom/google/r/b/a/ahc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aha;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5334
    sget-object v0, Lcom/google/r/b/a/aha;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 6022
    invoke-virtual {p0}, Lcom/google/r/b/a/aha;->c()I

    .line 6023
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 6024
    iget v0, p0, Lcom/google/r/b/a/aha;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 6026
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 6027
    iget-object v0, p0, Lcom/google/r/b/a/aha;->c:Lcom/google/n/f;

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 6029
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 6030
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/f;

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6029
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 6032
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 6033
    iget-object v0, p0, Lcom/google/r/b/a/aha;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6035
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 6036
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/r/b/a/aha;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6038
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 6039
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/aha;->j:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6041
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 6042
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/aha;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 6044
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 6045
    iget v0, p0, Lcom/google/r/b/a/aha;->l:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(II)V

    .line 6047
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_8

    .line 6048
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/r/b/a/aha;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    :cond_8
    move v1, v2

    .line 6050
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 6051
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6050
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 6053
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 6054
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/r/b/a/aha;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    :cond_a
    move v1, v2

    .line 6056
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 6057
    const/16 v3, 0xc

    iget-object v0, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6056
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 6059
    :cond_b
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 6060
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6059
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 6062
    :cond_c
    iget-object v0, p0, Lcom/google/r/b/a/aha;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6063
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6012
    iget-byte v1, p0, Lcom/google/r/b/a/aha;->p:B

    .line 6013
    if-ne v1, v0, :cond_0

    .line 6017
    :goto_0
    return v0

    .line 6014
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 6016
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aha;->p:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 6067
    iget v0, p0, Lcom/google/r/b/a/aha;->q:I

    .line 6068
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 6130
    :goto_0
    return v0

    .line 6071
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_12

    .line 6072
    iget v0, p0, Lcom/google/r/b/a/aha;->b:I

    .line 6073
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 6075
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_11

    .line 6076
    iget-object v3, p0, Lcom/google/r/b/a/aha;->c:Lcom/google/n/f;

    .line 6077
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v5

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    :goto_3
    move v4, v2

    move v5, v2

    .line 6081
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 6082
    iget-object v0, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    .line 6083
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v6

    add-int/2addr v5, v0

    .line 6081
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_1
    move v0, v1

    .line 6073
    goto :goto_1

    .line 6085
    :cond_2
    add-int v0, v3, v5

    .line 6086
    iget-object v3, p0, Lcom/google/r/b/a/aha;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 6088
    iget v3, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 6089
    iget-object v3, p0, Lcom/google/r/b/a/aha;->h:Lcom/google/n/ao;

    .line 6090
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 6092
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 6093
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/r/b/a/aha;->i:Lcom/google/n/ao;

    .line 6094
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 6096
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 6097
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/r/b/a/aha;->j:Lcom/google/n/f;

    .line 6098
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 6100
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 6101
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/r/b/a/aha;->k:I

    .line 6102
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 6104
    :cond_6
    iget v3, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 6105
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/r/b/a/aha;->l:I

    .line 6106
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 6108
    :cond_7
    iget v3, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_8

    .line 6109
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/r/b/a/aha;->e:I

    .line 6110
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    :cond_8
    move v3, v2

    move v4, v0

    .line 6112
    :goto_8
    iget-object v0, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_c

    .line 6113
    iget-object v0, p0, Lcom/google/r/b/a/aha;->m:Ljava/util/List;

    .line 6114
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 6112
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    :cond_9
    move v3, v1

    .line 6102
    goto :goto_5

    :cond_a
    move v3, v1

    .line 6106
    goto :goto_6

    :cond_b
    move v3, v1

    .line 6110
    goto :goto_7

    .line 6116
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/aha;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_e

    .line 6117
    const/16 v0, 0xb

    iget v3, p0, Lcom/google/r/b/a/aha;->n:I

    .line 6118
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v3, :cond_d

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_d
    add-int/2addr v0, v1

    add-int/2addr v4, v0

    :cond_e
    move v1, v2

    .line 6120
    :goto_9
    iget-object v0, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 6121
    const/16 v3, 0xc

    iget-object v0, p0, Lcom/google/r/b/a/aha;->f:Ljava/util/List;

    .line 6122
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 6120
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :cond_f
    move v1, v2

    .line 6124
    :goto_a
    iget-object v0, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 6125
    const/16 v3, 0xd

    iget-object v0, p0, Lcom/google/r/b/a/aha;->g:Ljava/util/List;

    .line 6126
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 6124
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 6128
    :cond_10
    iget-object v0, p0, Lcom/google/r/b/a/aha;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 6129
    iput v0, p0, Lcom/google/r/b/a/aha;->q:I

    goto/16 :goto_0

    :cond_11
    move v3, v0

    goto/16 :goto_3

    :cond_12
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5163
    invoke-static {}, Lcom/google/r/b/a/aha;->newBuilder()Lcom/google/r/b/a/ahc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ahc;->a(Lcom/google/r/b/a/aha;)Lcom/google/r/b/a/ahc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5163
    invoke-static {}, Lcom/google/r/b/a/aha;->newBuilder()Lcom/google/r/b/a/ahc;

    move-result-object v0

    return-object v0
.end method
