.class public final enum Lcom/google/r/b/a/ft;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ft;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ft;

.field public static final enum b:Lcom/google/r/b/a/ft;

.field public static final enum c:Lcom/google/r/b/a/ft;

.field public static final enum d:Lcom/google/r/b/a/ft;

.field public static final enum e:Lcom/google/r/b/a/ft;

.field public static final enum f:Lcom/google/r/b/a/ft;

.field public static final enum g:Lcom/google/r/b/a/ft;

.field public static final enum h:Lcom/google/r/b/a/ft;

.field public static final enum i:Lcom/google/r/b/a/ft;

.field public static final enum j:Lcom/google/r/b/a/ft;

.field public static final enum k:Lcom/google/r/b/a/ft;

.field public static final enum l:Lcom/google/r/b/a/ft;

.field public static final enum m:Lcom/google/r/b/a/ft;

.field public static final enum n:Lcom/google/r/b/a/ft;

.field private static final synthetic p:[Lcom/google/r/b/a/ft;


# instance fields
.field public final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 4723
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "ERROR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    .line 4727
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->b:Lcom/google/r/b/a/ft;

    .line 4731
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->c:Lcom/google/r/b/a/ft;

    .line 4735
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "LOCATION_DETAILS"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->d:Lcom/google/r/b/a/ft;

    .line 4739
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "DIRECTIONS_DEFAULT"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->e:Lcom/google/r/b/a/ft;

    .line 4743
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "DIRECTIONS_NAVIGATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->f:Lcom/google/r/b/a/ft;

    .line 4747
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "DIRECTIONS_TRIP_DETAILS"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->g:Lcom/google/r/b/a/ft;

    .line 4751
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "URL_REDIRECTION_BROWSER"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->h:Lcom/google/r/b/a/ft;

    .line 4755
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "URL_REDIRECTION_WEBVIEW"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->i:Lcom/google/r/b/a/ft;

    .line 4759
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "PLACE_DETAILS_BASIC"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->j:Lcom/google/r/b/a/ft;

    .line 4763
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "PLACE_DETAILS_FULL"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->k:Lcom/google/r/b/a/ft;

    .line 4767
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "STREET_VIEW"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->l:Lcom/google/r/b/a/ft;

    .line 4771
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "HANDLE_MFE_URL"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->m:Lcom/google/r/b/a/ft;

    .line 4775
    new-instance v0, Lcom/google/r/b/a/ft;

    const-string v1, "MAPS_ENGINE_MAP"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/ft;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ft;->n:Lcom/google/r/b/a/ft;

    .line 4718
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/r/b/a/ft;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/r/b/a/ft;->b:Lcom/google/r/b/a/ft;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/ft;->c:Lcom/google/r/b/a/ft;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/ft;->d:Lcom/google/r/b/a/ft;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/ft;->e:Lcom/google/r/b/a/ft;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/ft;->f:Lcom/google/r/b/a/ft;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/ft;->g:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/r/b/a/ft;->h:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/ft;->i:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/r/b/a/ft;->j:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/r/b/a/ft;->k:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/r/b/a/ft;->l:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/r/b/a/ft;->m:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/r/b/a/ft;->n:Lcom/google/r/b/a/ft;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/ft;->p:[Lcom/google/r/b/a/ft;

    .line 4865
    new-instance v0, Lcom/google/r/b/a/fu;

    invoke-direct {v0}, Lcom/google/r/b/a/fu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 4874
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4875
    iput p3, p0, Lcom/google/r/b/a/ft;->o:I

    .line 4876
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ft;
    .locals 1

    .prologue
    .line 4841
    packed-switch p0, :pswitch_data_0

    .line 4856
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4842
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4843
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ft;->b:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4844
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/ft;->c:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4845
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/ft;->d:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4846
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/ft;->e:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4847
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/ft;->f:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4848
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/ft;->g:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4849
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/ft;->h:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4850
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/ft;->i:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4851
    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/ft;->j:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4852
    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/ft;->k:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4853
    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/ft;->l:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4854
    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/ft;->m:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4855
    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/ft;->n:Lcom/google/r/b/a/ft;

    goto :goto_0

    .line 4841
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ft;
    .locals 1

    .prologue
    .line 4718
    const-class v0, Lcom/google/r/b/a/ft;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ft;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ft;
    .locals 1

    .prologue
    .line 4718
    sget-object v0, Lcom/google/r/b/a/ft;->p:[Lcom/google/r/b/a/ft;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ft;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ft;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 4837
    iget v0, p0, Lcom/google/r/b/a/ft;->o:I

    return v0
.end method
