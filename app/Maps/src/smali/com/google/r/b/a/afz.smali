.class public final Lcom/google/r/b/a/afz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/agc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afz;",
            ">;"
        }
    .end annotation
.end field

.field static final p:Lcom/google/r/b/a/afz;

.field private static volatile s:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Lcom/google/n/ao;

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field j:Z

.field public k:Lcom/google/n/ao;

.field l:Lcom/google/n/ao;

.field m:Z

.field n:Lcom/google/n/ao;

.field o:Z

.field private q:B

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3508
    new-instance v0, Lcom/google/r/b/a/aga;

    invoke-direct {v0}, Lcom/google/r/b/a/aga;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afz;->PARSER:Lcom/google/n/ax;

    .line 4097
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/afz;->s:Lcom/google/n/aw;

    .line 5008
    new-instance v0, Lcom/google/r/b/a/afz;

    invoke-direct {v0}, Lcom/google/r/b/a/afz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afz;->p:Lcom/google/r/b/a/afz;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3368
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3743
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    .line 3759
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    .line 3835
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->h:Lcom/google/n/ao;

    .line 3888
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    .line 3904
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->l:Lcom/google/n/ao;

    .line 3935
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->n:Lcom/google/n/ao;

    .line 3965
    iput-byte v4, p0, Lcom/google/r/b/a/afz;->q:B

    .line 4023
    iput v4, p0, Lcom/google/r/b/a/afz;->r:I

    .line 3369
    iget-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3370
    iget-object v0, p0, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3371
    iput-boolean v3, p0, Lcom/google/r/b/a/afz;->d:Z

    .line 3372
    iput-boolean v2, p0, Lcom/google/r/b/a/afz;->e:Z

    .line 3373
    iput-boolean v3, p0, Lcom/google/r/b/a/afz;->f:Z

    .line 3374
    iput-boolean v3, p0, Lcom/google/r/b/a/afz;->g:Z

    .line 3375
    iget-object v0, p0, Lcom/google/r/b/a/afz;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3376
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    .line 3377
    iput-boolean v3, p0, Lcom/google/r/b/a/afz;->j:Z

    .line 3378
    iget-object v0, p0, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3379
    iget-object v0, p0, Lcom/google/r/b/a/afz;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3380
    iput-boolean v3, p0, Lcom/google/r/b/a/afz;->m:Z

    .line 3381
    iget-object v0, p0, Lcom/google/r/b/a/afz;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3382
    iput-boolean v3, p0, Lcom/google/r/b/a/afz;->o:Z

    .line 3383
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    .line 3389
    invoke-direct {p0}, Lcom/google/r/b/a/afz;-><init>()V

    .line 3390
    const/4 v1, 0x0

    .line 3392
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 3394
    const/4 v0, 0x0

    move v2, v0

    .line 3395
    :cond_0
    :goto_0
    if-nez v2, :cond_b

    .line 3396
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 3397
    sparse-switch v0, :sswitch_data_0

    .line 3402
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3404
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 3399
    :sswitch_0
    const/4 v0, 0x1

    move v2, v0

    .line 3400
    goto :goto_0

    .line 3409
    :sswitch_1
    iget-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 3410
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3496
    :catch_0
    move-exception v0

    .line 3497
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3502
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_1

    .line 3503
    iget-object v1, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    .line 3505
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afz;->au:Lcom/google/n/bn;

    throw v0

    .line 3414
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 3415
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3498
    :catch_1
    move-exception v0

    .line 3499
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 3500
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3419
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    .line 3420
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/afz;->e:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 3424
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    .line 3425
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/afz;->f:Z

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 3429
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    .line 3430
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/google/r/b/a/afz;->d:Z

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 3434
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    .line 3435
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/r/b/a/afz;->g:Z

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    .line 3439
    :sswitch_7
    iget-object v0, p0, Lcom/google/r/b/a/afz;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 3440
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    goto/16 :goto_0

    .line 3444
    :sswitch_8
    and-int/lit16 v0, v1, 0x80

    const/16 v4, 0x80

    if-eq v0, v4, :cond_6

    .line 3445
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    .line 3446
    or-int/lit16 v1, v1, 0x80

    .line 3448
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 3452
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 3453
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 3454
    and-int/lit16 v0, v1, 0x80

    const/16 v5, 0x80

    if-eq v0, v5, :cond_7

    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_8

    const/4 v0, -0x1

    :goto_5
    if-lez v0, :cond_7

    .line 3455
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    .line 3456
    or-int/lit16 v1, v1, 0x80

    .line 3458
    :cond_7
    :goto_6
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_9

    const/4 v0, -0x1

    :goto_7
    if-lez v0, :cond_a

    .line 3459
    iget-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 3454
    :cond_8
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_5

    .line 3458
    :cond_9
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_7

    .line 3461
    :cond_a
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 3465
    :sswitch_a
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    .line 3466
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/afz;->j:Z

    goto/16 :goto_0

    .line 3470
    :sswitch_b
    iget-object v0, p0, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 3471
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    goto/16 :goto_0

    .line 3475
    :sswitch_c
    iget-object v0, p0, Lcom/google/r/b/a/afz;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 3476
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    goto/16 :goto_0

    .line 3480
    :sswitch_d
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    .line 3481
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/afz;->m:Z

    goto/16 :goto_0

    .line 3485
    :sswitch_e
    iget-object v0, p0, Lcom/google/r/b/a/afz;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 3486
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    goto/16 :goto_0

    .line 3490
    :sswitch_f
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/afz;->a:I

    .line 3491
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/afz;->o:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 3502
    :cond_b
    and-int/lit16 v0, v1, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 3503
    iget-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    .line 3505
    :cond_c
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afz;->au:Lcom/google/n/bn;

    .line 3506
    return-void

    .line 3397
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x38 -> :sswitch_5
        0x50 -> :sswitch_6
        0x5a -> :sswitch_7
        0x78 -> :sswitch_8
        0x7a -> :sswitch_9
        0x80 -> :sswitch_a
        0x8a -> :sswitch_b
        0x92 -> :sswitch_c
        0x98 -> :sswitch_d
        0xa2 -> :sswitch_e
        0xa8 -> :sswitch_f
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 3366
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3743
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    .line 3759
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    .line 3835
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->h:Lcom/google/n/ao;

    .line 3888
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    .line 3904
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->l:Lcom/google/n/ao;

    .line 3935
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afz;->n:Lcom/google/n/ao;

    .line 3965
    iput-byte v1, p0, Lcom/google/r/b/a/afz;->q:B

    .line 4023
    iput v1, p0, Lcom/google/r/b/a/afz;->r:I

    .line 3367
    return-void
.end method

.method public static a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;
    .locals 1

    .prologue
    .line 4162
    invoke-static {}, Lcom/google/r/b/a/afz;->newBuilder()Lcom/google/r/b/a/agb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/afz;
    .locals 1

    .prologue
    .line 5011
    sget-object v0, Lcom/google/r/b/a/afz;->p:Lcom/google/r/b/a/afz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/agb;
    .locals 1

    .prologue
    .line 4159
    new-instance v0, Lcom/google/r/b/a/agb;

    invoke-direct {v0}, Lcom/google/r/b/a/agb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3520
    sget-object v0, Lcom/google/r/b/a/afz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3977
    invoke-virtual {p0}, Lcom/google/r/b/a/afz;->c()I

    .line 3978
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3979
    iget-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3981
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3982
    iget-object v0, p0, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3984
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 3985
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/afz;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 3987
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v3, :cond_3

    .line 3988
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/afz;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 3990
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 3991
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/r/b/a/afz;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 3993
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 3994
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/r/b/a/afz;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 3996
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 3997
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/r/b/a/afz;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3999
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 4000
    const/16 v2, 0xf

    iget-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 3999
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4002
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 4003
    iget-boolean v0, p0, Lcom/google/r/b/a/afz;->j:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 4005
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 4006
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4008
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 4009
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/r/b/a/afz;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4011
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_b

    .line 4012
    const/16 v0, 0x13

    iget-boolean v1, p0, Lcom/google/r/b/a/afz;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 4014
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_c

    .line 4015
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/r/b/a/afz;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4017
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_d

    .line 4018
    const/16 v0, 0x15

    iget-boolean v1, p0, Lcom/google/r/b/a/afz;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 4020
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/afz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4021
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3967
    iget-byte v1, p0, Lcom/google/r/b/a/afz;->q:B

    .line 3968
    if-ne v1, v0, :cond_0

    .line 3972
    :goto_0
    return v0

    .line 3969
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3971
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/afz;->q:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0xa

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 4025
    iget v0, p0, Lcom/google/r/b/a/afz;->r:I

    .line 4026
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4092
    :goto_0
    return v0

    .line 4029
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 4030
    iget-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    .line 4031
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 4033
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 4034
    iget-object v2, p0, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    .line 4035
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4037
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 4038
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/r/b/a/afz;->e:Z

    .line 4039
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4041
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v6, :cond_3

    .line 4042
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/r/b/a/afz;->f:Z

    .line 4043
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4045
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 4046
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/r/b/a/afz;->d:Z

    .line 4047
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4049
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 4050
    iget-boolean v2, p0, Lcom/google/r/b/a/afz;->g:Z

    .line 4051
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4053
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_e

    .line 4054
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/r/b/a/afz;->h:Lcom/google/n/ao;

    .line 4055
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    :goto_2
    move v3, v1

    move v4, v1

    .line 4059
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 4060
    iget-object v0, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    .line 4061
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v4, v0

    .line 4059
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_6
    move v0, v5

    .line 4061
    goto :goto_4

    .line 4063
    :cond_7
    add-int v0, v2, v4

    .line 4064
    iget-object v2, p0, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4066
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8

    .line 4067
    iget-boolean v2, p0, Lcom/google/r/b/a/afz;->j:Z

    .line 4068
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4070
    :cond_8
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_9

    .line 4071
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    .line 4072
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4074
    :cond_9
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_a

    .line 4075
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/r/b/a/afz;->l:Lcom/google/n/ao;

    .line 4076
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4078
    :cond_a
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_b

    .line 4079
    const/16 v2, 0x13

    iget-boolean v3, p0, Lcom/google/r/b/a/afz;->m:Z

    .line 4080
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4082
    :cond_b
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_c

    .line 4083
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/r/b/a/afz;->n:Lcom/google/n/ao;

    .line 4084
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4086
    :cond_c
    iget v2, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_d

    .line 4087
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/r/b/a/afz;->o:Z

    .line 4088
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4090
    :cond_d
    iget-object v1, p0, Lcom/google/r/b/a/afz;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4091
    iput v0, p0, Lcom/google/r/b/a/afz;->r:I

    goto/16 :goto_0

    :cond_e
    move v2, v0

    goto/16 :goto_2

    :cond_f
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3360
    invoke-static {}, Lcom/google/r/b/a/afz;->newBuilder()Lcom/google/r/b/a/agb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3360
    invoke-static {}, Lcom/google/r/b/a/afz;->newBuilder()Lcom/google/r/b/a/agb;

    move-result-object v0

    return-object v0
.end method
