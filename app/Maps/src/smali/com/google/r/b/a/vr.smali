.class public final Lcom/google/r/b/a/vr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/vu;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/vr;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/vr;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3282
    new-instance v0, Lcom/google/r/b/a/vs;

    invoke-direct {v0}, Lcom/google/r/b/a/vs;-><init>()V

    sput-object v0, Lcom/google/r/b/a/vr;->PARSER:Lcom/google/n/ax;

    .line 3425
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/vr;->g:Lcom/google/n/aw;

    .line 3714
    new-instance v0, Lcom/google/r/b/a/vr;

    invoke-direct {v0}, Lcom/google/r/b/a/vr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/vr;->d:Lcom/google/r/b/a/vr;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3231
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3382
    iput-byte v0, p0, Lcom/google/r/b/a/vr;->e:B

    .line 3404
    iput v0, p0, Lcom/google/r/b/a/vr;->f:I

    .line 3232
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/vr;->b:Ljava/lang/Object;

    .line 3233
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/vr;->c:Ljava/lang/Object;

    .line 3234
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 3240
    invoke-direct {p0}, Lcom/google/r/b/a/vr;-><init>()V

    .line 3241
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 3245
    const/4 v0, 0x0

    .line 3246
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 3247
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 3248
    sparse-switch v3, :sswitch_data_0

    .line 3253
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 3255
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 3251
    goto :goto_0

    .line 3260
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3261
    iget v4, p0, Lcom/google/r/b/a/vr;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/vr;->a:I

    .line 3262
    iput-object v3, p0, Lcom/google/r/b/a/vr;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3273
    :catch_0
    move-exception v0

    .line 3274
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3279
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/vr;->au:Lcom/google/n/bn;

    throw v0

    .line 3266
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3267
    iget v4, p0, Lcom/google/r/b/a/vr;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/vr;->a:I

    .line 3268
    iput-object v3, p0, Lcom/google/r/b/a/vr;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3275
    :catch_1
    move-exception v0

    .line 3276
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3277
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3279
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vr;->au:Lcom/google/n/bn;

    .line 3280
    return-void

    .line 3248
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3229
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3382
    iput-byte v0, p0, Lcom/google/r/b/a/vr;->e:B

    .line 3404
    iput v0, p0, Lcom/google/r/b/a/vr;->f:I

    .line 3230
    return-void
.end method

.method public static d()Lcom/google/r/b/a/vr;
    .locals 1

    .prologue
    .line 3717
    sget-object v0, Lcom/google/r/b/a/vr;->d:Lcom/google/r/b/a/vr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/vt;
    .locals 1

    .prologue
    .line 3487
    new-instance v0, Lcom/google/r/b/a/vt;

    invoke-direct {v0}, Lcom/google/r/b/a/vt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/vr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3294
    sget-object v0, Lcom/google/r/b/a/vr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3394
    invoke-virtual {p0}, Lcom/google/r/b/a/vr;->c()I

    .line 3395
    iget v0, p0, Lcom/google/r/b/a/vr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 3396
    iget-object v0, p0, Lcom/google/r/b/a/vr;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vr;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3398
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/vr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 3399
    iget-object v0, p0, Lcom/google/r/b/a/vr;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vr;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3401
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/vr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3402
    return-void

    .line 3396
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 3399
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3384
    iget-byte v1, p0, Lcom/google/r/b/a/vr;->e:B

    .line 3385
    if-ne v1, v0, :cond_0

    .line 3389
    :goto_0
    return v0

    .line 3386
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3388
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/vr;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3406
    iget v0, p0, Lcom/google/r/b/a/vr;->f:I

    .line 3407
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3420
    :goto_0
    return v0

    .line 3410
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/vr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 3412
    iget-object v0, p0, Lcom/google/r/b/a/vr;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vr;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 3414
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/vr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 3416
    iget-object v0, p0, Lcom/google/r/b/a/vr;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vr;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 3418
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/vr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 3419
    iput v0, p0, Lcom/google/r/b/a/vr;->f:I

    goto :goto_0

    .line 3412
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 3416
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3223
    invoke-static {}, Lcom/google/r/b/a/vr;->newBuilder()Lcom/google/r/b/a/vt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/vt;->a(Lcom/google/r/b/a/vr;)Lcom/google/r/b/a/vt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3223
    invoke-static {}, Lcom/google/r/b/a/vr;->newBuilder()Lcom/google/r/b/a/vt;

    move-result-object v0

    return-object v0
.end method
