.class public final Lcom/google/r/b/a/a/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/a/a/b;",
        "Lcom/google/r/b/a/a/a/d;",
        ">;",
        "Lcom/google/r/b/a/a/a/e;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field public d:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 299
    sget-object v0, Lcom/google/r/b/a/a/a/b;->e:Lcom/google/r/b/a/a/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 300
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 291
    new-instance v2, Lcom/google/r/b/a/a/a/b;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/a/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/a/a/d;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-wide v4, p0, Lcom/google/r/b/a/a/a/d;->b:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/a/b;->b:J

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/r/b/a/a/a/d;->c:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/a/b;->c:J

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/r/b/a/a/a/d;->d:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/a/b;->d:J

    iput v0, v2, Lcom/google/r/b/a/a/a/b;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 291
    check-cast p1, Lcom/google/r/b/a/a/a/b;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/a/a/d;->a(Lcom/google/r/b/a/a/a/b;)Lcom/google/r/b/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/a/a/b;)Lcom/google/r/b/a/a/a/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 337
    invoke-static {}, Lcom/google/r/b/a/a/a/b;->d()Lcom/google/r/b/a/a/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 348
    :goto_0
    return-object p0

    .line 338
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 339
    iget-wide v2, p1, Lcom/google/r/b/a/a/a/b;->b:J

    iget v4, p0, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/a/a/d;->b:J

    .line 341
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 342
    iget-wide v2, p1, Lcom/google/r/b/a/a/a/b;->c:J

    iget v4, p0, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/a/a/d;->c:J

    .line 344
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 345
    iget-wide v0, p1, Lcom/google/r/b/a/a/a/b;->d:J

    iget v2, p0, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v0, p0, Lcom/google/r/b/a/a/a/d;->d:J

    .line 347
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/a/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 338
    goto :goto_1

    :cond_5
    move v2, v1

    .line 341
    goto :goto_2

    :cond_6
    move v0, v1

    .line 344
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x1

    return v0
.end method
