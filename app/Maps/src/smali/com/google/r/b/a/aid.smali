.class public final enum Lcom/google/r/b/a/aid;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/aid;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/aid;

.field public static final enum b:Lcom/google/r/b/a/aid;

.field public static final enum c:Lcom/google/r/b/a/aid;

.field public static final enum d:Lcom/google/r/b/a/aid;

.field private static final synthetic f:[Lcom/google/r/b/a/aid;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 644
    new-instance v0, Lcom/google/r/b/a/aid;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/aid;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aid;->a:Lcom/google/r/b/a/aid;

    .line 648
    new-instance v0, Lcom/google/r/b/a/aid;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/aid;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aid;->b:Lcom/google/r/b/a/aid;

    .line 652
    new-instance v0, Lcom/google/r/b/a/aid;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/aid;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aid;->c:Lcom/google/r/b/a/aid;

    .line 656
    new-instance v0, Lcom/google/r/b/a/aid;

    const-string v1, "REQUEST_ERROR"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/aid;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aid;->d:Lcom/google/r/b/a/aid;

    .line 639
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/aid;

    sget-object v1, Lcom/google/r/b/a/aid;->a:Lcom/google/r/b/a/aid;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/aid;->b:Lcom/google/r/b/a/aid;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/aid;->c:Lcom/google/r/b/a/aid;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/aid;->d:Lcom/google/r/b/a/aid;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/aid;->f:[Lcom/google/r/b/a/aid;

    .line 696
    new-instance v0, Lcom/google/r/b/a/aie;

    invoke-direct {v0}, Lcom/google/r/b/a/aie;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 705
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 706
    iput p3, p0, Lcom/google/r/b/a/aid;->e:I

    .line 707
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/aid;
    .locals 1

    .prologue
    .line 682
    packed-switch p0, :pswitch_data_0

    .line 687
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 683
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/aid;->a:Lcom/google/r/b/a/aid;

    goto :goto_0

    .line 684
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/aid;->b:Lcom/google/r/b/a/aid;

    goto :goto_0

    .line 685
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/aid;->c:Lcom/google/r/b/a/aid;

    goto :goto_0

    .line 686
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/aid;->d:Lcom/google/r/b/a/aid;

    goto :goto_0

    .line 682
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/aid;
    .locals 1

    .prologue
    .line 639
    const-class v0, Lcom/google/r/b/a/aid;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aid;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/aid;
    .locals 1

    .prologue
    .line 639
    sget-object v0, Lcom/google/r/b/a/aid;->f:[Lcom/google/r/b/a/aid;

    invoke-virtual {v0}, [Lcom/google/r/b/a/aid;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/aid;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 678
    iget v0, p0, Lcom/google/r/b/a/aid;->e:I

    return v0
.end method
