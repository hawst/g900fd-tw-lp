.class public final Lcom/google/r/b/a/kp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/kq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/kn;",
        "Lcom/google/r/b/a/kp;",
        ">;",
        "Lcom/google/r/b/a/kq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 348
    sget-object v0, Lcom/google/r/b/a/kn;->e:Lcom/google/r/b/a/kn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 417
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/kp;->b:I

    .line 454
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    .line 590
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/kp;->d:Lcom/google/n/ao;

    .line 349
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 340
    new-instance v2, Lcom/google/r/b/a/kn;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/kn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/kp;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/kp;->b:I

    iput v4, v2, Lcom/google/r/b/a/kn;->b:I

    iget v4, p0, Lcom/google/r/b/a/kp;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/kp;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/kp;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v3, v2, Lcom/google/r/b/a/kn;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/kp;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/kp;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/kn;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 340
    check-cast p1, Lcom/google/r/b/a/kn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/kp;->a(Lcom/google/r/b/a/kn;)Lcom/google/r/b/a/kp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/kn;)Lcom/google/r/b/a/kp;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 389
    invoke-static {}, Lcom/google/r/b/a/kn;->d()Lcom/google/r/b/a/kn;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 408
    :goto_0
    return-object p0

    .line 390
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/kn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 391
    iget v2, p1, Lcom/google/r/b/a/kn;->b:I

    invoke-static {v2}, Lcom/google/maps/g/il;->a(I)Lcom/google/maps/g/il;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/il;->a:Lcom/google/maps/g/il;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 390
    goto :goto_1

    .line 391
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/kp;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/kp;->a:I

    iget v2, v2, Lcom/google/maps/g/il;->d:I

    iput v2, p0, Lcom/google/r/b/a/kp;->b:I

    .line 393
    :cond_4
    iget-object v2, p1, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 394
    iget-object v2, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 395
    iget-object v2, p1, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    .line 396
    iget v2, p0, Lcom/google/r/b/a/kp;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/r/b/a/kp;->a:I

    .line 403
    :cond_5
    :goto_2
    iget v2, p1, Lcom/google/r/b/a/kn;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_9

    :goto_3
    if-eqz v0, :cond_6

    .line 404
    iget-object v0, p0, Lcom/google/r/b/a/kp;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/kn;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 405
    iget v0, p0, Lcom/google/r/b/a/kp;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/kp;->a:I

    .line 407
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/kn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 398
    :cond_7
    iget v2, p0, Lcom/google/r/b/a/kp;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/kp;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/kp;->a:I

    .line 399
    :cond_8
    iget-object v2, p0, Lcom/google/r/b/a/kp;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/kn;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v0, v1

    .line 403
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x1

    return v0
.end method
