.class public final Lcom/google/r/b/a/yt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/yw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/yt;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/yt;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field e:I

.field public f:I

.field g:I

.field public h:I

.field public i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2502
    new-instance v0, Lcom/google/r/b/a/yu;

    invoke-direct {v0}, Lcom/google/r/b/a/yu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/yt;->PARSER:Lcom/google/n/ax;

    .line 2724
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/yt;->m:Lcom/google/n/aw;

    .line 3171
    new-instance v0, Lcom/google/r/b/a/yt;

    invoke-direct {v0}, Lcom/google/r/b/a/yt;-><init>()V

    sput-object v0, Lcom/google/r/b/a/yt;->j:Lcom/google/r/b/a/yt;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 2411
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2639
    iput-byte v1, p0, Lcom/google/r/b/a/yt;->k:B

    .line 2679
    iput v1, p0, Lcom/google/r/b/a/yt;->l:I

    .line 2412
    iput v0, p0, Lcom/google/r/b/a/yt;->b:I

    .line 2413
    iput v0, p0, Lcom/google/r/b/a/yt;->c:I

    .line 2414
    iput v0, p0, Lcom/google/r/b/a/yt;->d:I

    .line 2415
    iput v0, p0, Lcom/google/r/b/a/yt;->e:I

    .line 2416
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/r/b/a/yt;->f:I

    .line 2417
    const v0, 0x4c4b400

    iput v0, p0, Lcom/google/r/b/a/yt;->g:I

    .line 2418
    const v0, -0x55d4a80

    iput v0, p0, Lcom/google/r/b/a/yt;->h:I

    .line 2419
    const v0, 0x55d4a80

    iput v0, p0, Lcom/google/r/b/a/yt;->i:I

    .line 2420
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2426
    invoke-direct {p0}, Lcom/google/r/b/a/yt;-><init>()V

    .line 2427
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2431
    const/4 v0, 0x0

    .line 2432
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 2433
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2434
    sparse-switch v3, :sswitch_data_0

    .line 2439
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2441
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2437
    goto :goto_0

    .line 2446
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/yt;->a:I

    .line 2447
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yt;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2493
    :catch_0
    move-exception v0

    .line 2494
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2499
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/yt;->au:Lcom/google/n/bn;

    throw v0

    .line 2451
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/yt;->a:I

    .line 2452
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yt;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2495
    :catch_1
    move-exception v0

    .line 2496
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2497
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2456
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/yt;->a:I

    .line 2457
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yt;->d:I

    goto :goto_0

    .line 2461
    :sswitch_4
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/yt;->a:I

    .line 2462
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yt;->e:I

    goto :goto_0

    .line 2466
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 2467
    invoke-static {v3}, Lcom/google/r/b/a/yx;->a(I)Lcom/google/r/b/a/yx;

    move-result-object v4

    .line 2468
    if-nez v4, :cond_1

    .line 2469
    const/16 v4, 0x26

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 2471
    :cond_1
    iget v4, p0, Lcom/google/r/b/a/yt;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/yt;->a:I

    .line 2472
    iput v3, p0, Lcom/google/r/b/a/yt;->f:I

    goto :goto_0

    .line 2477
    :sswitch_6
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/yt;->a:I

    .line 2478
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yt;->g:I

    goto/16 :goto_0

    .line 2482
    :sswitch_7
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/yt;->a:I

    .line 2483
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yt;->h:I

    goto/16 :goto_0

    .line 2487
    :sswitch_8
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/yt;->a:I

    .line 2488
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/yt;->i:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2499
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yt;->au:Lcom/google/n/bn;

    .line 2500
    return-void

    .line 2434
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x110 -> :sswitch_1
        0x118 -> :sswitch_2
        0x120 -> :sswitch_3
        0x128 -> :sswitch_4
        0x130 -> :sswitch_5
        0x138 -> :sswitch_6
        0x140 -> :sswitch_7
        0x148 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2409
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2639
    iput-byte v0, p0, Lcom/google/r/b/a/yt;->k:B

    .line 2679
    iput v0, p0, Lcom/google/r/b/a/yt;->l:I

    .line 2410
    return-void
.end method

.method public static a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;
    .locals 1

    .prologue
    .line 2789
    invoke-static {}, Lcom/google/r/b/a/yt;->newBuilder()Lcom/google/r/b/a/yv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yv;->a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/yt;
    .locals 1

    .prologue
    .line 3174
    sget-object v0, Lcom/google/r/b/a/yt;->j:Lcom/google/r/b/a/yt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/yv;
    .locals 1

    .prologue
    .line 2786
    new-instance v0, Lcom/google/r/b/a/yv;

    invoke-direct {v0}, Lcom/google/r/b/a/yv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/yt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2514
    sget-object v0, Lcom/google/r/b/a/yt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 2651
    invoke-virtual {p0}, Lcom/google/r/b/a/yt;->c()I

    .line 2652
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2653
    const/16 v0, 0x22

    iget v1, p0, Lcom/google/r/b/a/yt;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 2655
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2656
    const/16 v0, 0x23

    iget v1, p0, Lcom/google/r/b/a/yt;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 2658
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2659
    const/16 v0, 0x24

    iget v1, p0, Lcom/google/r/b/a/yt;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 2661
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2662
    const/16 v0, 0x25

    iget v1, p0, Lcom/google/r/b/a/yt;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 2664
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2665
    const/16 v0, 0x26

    iget v1, p0, Lcom/google/r/b/a/yt;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 2667
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 2668
    const/16 v0, 0x27

    iget v1, p0, Lcom/google/r/b/a/yt;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 2670
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 2671
    const/16 v0, 0x28

    iget v1, p0, Lcom/google/r/b/a/yt;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 2673
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 2674
    const/16 v0, 0x29

    iget v1, p0, Lcom/google/r/b/a/yt;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 2676
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/yt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2677
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2641
    iget-byte v1, p0, Lcom/google/r/b/a/yt;->k:B

    .line 2642
    if-ne v1, v0, :cond_0

    .line 2646
    :goto_0
    return v0

    .line 2643
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2645
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/yt;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 2681
    iget v0, p0, Lcom/google/r/b/a/yt;->l:I

    .line 2682
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 2719
    :goto_0
    return v0

    .line 2685
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_10

    .line 2686
    const/16 v0, 0x22

    iget v3, p0, Lcom/google/r/b/a/yt;->b:I

    .line 2687
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x0

    .line 2689
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 2690
    const/16 v3, 0x23

    iget v4, p0, Lcom/google/r/b/a/yt;->c:I

    .line 2691
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2693
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 2694
    const/16 v3, 0x24

    iget v4, p0, Lcom/google/r/b/a/yt;->d:I

    .line 2695
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2697
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 2698
    const/16 v3, 0x25

    iget v4, p0, Lcom/google/r/b/a/yt;->e:I

    .line 2699
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2701
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 2702
    const/16 v3, 0x26

    iget v4, p0, Lcom/google/r/b/a/yt;->f:I

    .line 2703
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2705
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 2706
    const/16 v3, 0x27

    iget v4, p0, Lcom/google/r/b/a/yt;->g:I

    .line 2707
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2709
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 2710
    const/16 v3, 0x28

    iget v4, p0, Lcom/google/r/b/a/yt;->h:I

    .line 2711
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2713
    :cond_6
    iget v3, p0, Lcom/google/r/b/a/yt;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_8

    .line 2714
    const/16 v3, 0x29

    iget v4, p0, Lcom/google/r/b/a/yt;->i:I

    .line 2715
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_7
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 2717
    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/yt;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2718
    iput v0, p0, Lcom/google/r/b/a/yt;->l:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 2687
    goto/16 :goto_1

    :cond_a
    move v3, v1

    .line 2691
    goto/16 :goto_3

    :cond_b
    move v3, v1

    .line 2695
    goto/16 :goto_4

    :cond_c
    move v3, v1

    .line 2699
    goto/16 :goto_5

    :cond_d
    move v3, v1

    .line 2703
    goto :goto_6

    :cond_e
    move v3, v1

    .line 2707
    goto :goto_7

    :cond_f
    move v3, v1

    .line 2711
    goto :goto_8

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2403
    invoke-static {}, Lcom/google/r/b/a/yt;->newBuilder()Lcom/google/r/b/a/yv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yv;->a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2403
    invoke-static {}, Lcom/google/r/b/a/yt;->newBuilder()Lcom/google/r/b/a/yv;

    move-result-object v0

    return-object v0
.end method
