.class public final Lcom/google/r/b/a/air;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ais;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aip;",
        "Lcom/google/r/b/a/air;",
        ">;",
        "Lcom/google/r/b/a/ais;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/f;

.field public c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1597
    sget-object v0, Lcom/google/r/b/a/aip;->f:Lcom/google/r/b/a/aip;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1672
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/air;->d:Lcom/google/n/ao;

    .line 1731
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/air;->b:Lcom/google/n/f;

    .line 1766
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/air;->c:Lcom/google/n/ao;

    .line 1825
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/air;->e:Ljava/lang/Object;

    .line 1598
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1589
    new-instance v2, Lcom/google/r/b/a/aip;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aip;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/air;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/aip;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/air;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/air;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/air;->b:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/r/b/a/aip;->c:Lcom/google/n/f;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/aip;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/air;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/air;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/air;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/aip;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/aip;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1589
    check-cast p1, Lcom/google/r/b/a/aip;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/air;->a(Lcom/google/r/b/a/aip;)Lcom/google/r/b/a/air;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aip;)Lcom/google/r/b/a/air;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1645
    invoke-static {}, Lcom/google/r/b/a/aip;->d()Lcom/google/r/b/a/aip;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1663
    :goto_0
    return-object p0

    .line 1646
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1647
    iget-object v2, p0, Lcom/google/r/b/a/air;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aip;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1648
    iget v2, p0, Lcom/google/r/b/a/air;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/air;->a:I

    .line 1650
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1651
    iget-object v2, p1, Lcom/google/r/b/a/aip;->c:Lcom/google/n/f;

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1646
    goto :goto_1

    :cond_3
    move v2, v1

    .line 1650
    goto :goto_2

    .line 1651
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/air;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/air;->a:I

    iput-object v2, p0, Lcom/google/r/b/a/air;->b:Lcom/google/n/f;

    .line 1653
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 1654
    iget-object v2, p0, Lcom/google/r/b/a/air;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aip;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1655
    iget v2, p0, Lcom/google/r/b/a/air;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/air;->a:I

    .line 1657
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    :goto_4
    if-eqz v0, :cond_7

    .line 1658
    iget v0, p0, Lcom/google/r/b/a/air;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/air;->a:I

    .line 1659
    iget-object v0, p1, Lcom/google/r/b/a/aip;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/air;->e:Ljava/lang/Object;

    .line 1662
    :cond_7
    iget-object v0, p1, Lcom/google/r/b/a/aip;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_8
    move v2, v1

    .line 1653
    goto :goto_3

    :cond_9
    move v0, v1

    .line 1657
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1667
    const/4 v0, 0x1

    return v0
.end method
