.class public final Lcom/google/r/b/a/wz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/xa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/wx;",
        "Lcom/google/r/b/a/wz;",
        ">;",
        "Lcom/google/r/b/a/xa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lcom/google/r/b/a/wx;->f:Lcom/google/r/b/a/wx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 478
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/wz;->b:Ljava/lang/Object;

    .line 554
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/wz;->c:Ljava/lang/Object;

    .line 630
    const-string v0, "https://geo0.ggpht.com/cbk"

    iput-object v0, p0, Lcom/google/r/b/a/wz;->d:Ljava/lang/Object;

    .line 706
    const-string v0, "https://kh.google.com/rt/earth"

    iput-object v0, p0, Lcom/google/r/b/a/wz;->e:Ljava/lang/Object;

    .line 404
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 395
    new-instance v2, Lcom/google/r/b/a/wx;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/wx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/wz;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/wz;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/wx;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/wz;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/wx;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/wz;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/wx;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/wz;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/wx;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/wx;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 395
    check-cast p1, Lcom/google/r/b/a/wx;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/wz;->a(Lcom/google/r/b/a/wx;)Lcom/google/r/b/a/wz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/wx;)Lcom/google/r/b/a/wz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 447
    invoke-static {}, Lcom/google/r/b/a/wx;->d()Lcom/google/r/b/a/wx;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 469
    :goto_0
    return-object p0

    .line 448
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/wx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 449
    iget v2, p0, Lcom/google/r/b/a/wz;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/wz;->a:I

    .line 450
    iget-object v2, p1, Lcom/google/r/b/a/wx;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/wz;->b:Ljava/lang/Object;

    .line 453
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/wx;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 454
    iget v2, p0, Lcom/google/r/b/a/wz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/wz;->a:I

    .line 455
    iget-object v2, p1, Lcom/google/r/b/a/wx;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/wz;->c:Ljava/lang/Object;

    .line 458
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/wx;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 459
    iget v2, p0, Lcom/google/r/b/a/wz;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/wz;->a:I

    .line 460
    iget-object v2, p1, Lcom/google/r/b/a/wx;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/wz;->d:Ljava/lang/Object;

    .line 463
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/wx;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 464
    iget v0, p0, Lcom/google/r/b/a/wz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/wz;->a:I

    .line 465
    iget-object v0, p1, Lcom/google/r/b/a/wx;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/wz;->e:Ljava/lang/Object;

    .line 468
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/wx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 448
    goto :goto_1

    :cond_6
    move v2, v1

    .line 453
    goto :goto_2

    :cond_7
    move v2, v1

    .line 458
    goto :goto_3

    :cond_8
    move v0, v1

    .line 463
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 473
    const/4 v0, 0x1

    return v0
.end method
