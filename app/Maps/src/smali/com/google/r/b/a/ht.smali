.class public final Lcom/google/r/b/a/ht;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/hu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/hr;",
        "Lcom/google/r/b/a/ht;",
        ">;",
        "Lcom/google/r/b/a/hu;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/aq;

.field private c:Lcom/google/n/aq;

.field private d:Lcom/google/n/aq;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/ao;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Lcom/google/n/ao;

.field private k:Ljava/lang/Object;

.field private l:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 687
    sget-object v0, Lcom/google/r/b/a/hr;->m:Lcom/google/r/b/a/hr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 889
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    .line 982
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    .line 1075
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    .line 1169
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    .line 1305
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ht;->f:Lcom/google/n/ao;

    .line 1365
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    .line 1501
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ht;->h:Lcom/google/n/ao;

    .line 1560
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ht;->i:Lcom/google/n/ao;

    .line 1619
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ht;->j:Lcom/google/n/ao;

    .line 1678
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ht;->k:Ljava/lang/Object;

    .line 688
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 679
    new-instance v2, Lcom/google/r/b/a/hr;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/hr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ht;->a:I

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/ht;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/ht;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/r/b/a/ht;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/r/b/a/ht;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_a

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/hr;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ht;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ht;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/r/b/a/ht;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x2

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ht;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ht;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x4

    :cond_6
    iget-object v4, v2, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ht;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ht;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x8

    :cond_7
    iget-object v4, v2, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ht;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ht;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit8 v0, v0, 0x10

    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/ht;->k:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/hr;->k:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit8 v0, v0, 0x20

    :cond_9
    iget v1, p0, Lcom/google/r/b/a/ht;->l:I

    iput v1, v2, Lcom/google/r/b/a/hr;->l:I

    iput v0, v2, Lcom/google/r/b/a/hr;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 679
    check-cast p1, Lcom/google/r/b/a/hr;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ht;->a(Lcom/google/r/b/a/hr;)Lcom/google/r/b/a/ht;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/hr;)Lcom/google/r/b/a/ht;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 786
    invoke-static {}, Lcom/google/r/b/a/hr;->d()Lcom/google/r/b/a/hr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 862
    :goto_0
    return-object p0

    .line 787
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 788
    iget-object v2, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 789
    iget-object v2, p1, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    .line 790
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 797
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 798
    iget-object v2, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 799
    iget-object v2, p1, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    .line 800
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 807
    :cond_2
    :goto_2
    iget-object v2, p1, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 808
    iget-object v2, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 809
    iget-object v2, p1, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    .line 810
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 817
    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 818
    iget-object v2, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 819
    iget-object v2, p1, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    .line 820
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 827
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_14

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 828
    iget-object v2, p0, Lcom/google/r/b/a/ht;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 829
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 831
    :cond_5
    iget-object v2, p1, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 832
    iget-object v2, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 833
    iget-object v2, p1, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    .line 834
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 841
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_17

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 842
    iget-object v2, p0, Lcom/google/r/b/a/ht;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 843
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 845
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_18

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 846
    iget-object v2, p0, Lcom/google/r/b/a/ht;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 847
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 849
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_19

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 850
    iget-object v2, p0, Lcom/google/r/b/a/ht;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 851
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 853
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 854
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 855
    iget-object v2, p1, Lcom/google/r/b/a/hr;->k:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ht;->k:Ljava/lang/Object;

    .line 858
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/hr;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1b

    :goto_b
    if-eqz v0, :cond_b

    .line 859
    iget v0, p1, Lcom/google/r/b/a/hr;->l:I

    iget v1, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/r/b/a/ht;->a:I

    iput v0, p0, Lcom/google/r/b/a/ht;->l:I

    .line 861
    :cond_b
    iget-object v0, p1, Lcom/google/r/b/a/hr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 792
    :cond_c
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_d

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 793
    :cond_d
    iget-object v2, p0, Lcom/google/r/b/a/ht;->b:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->b:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 802
    :cond_e
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_f

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 803
    :cond_f
    iget-object v2, p0, Lcom/google/r/b/a/ht;->c:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->c:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 812
    :cond_10
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v5, :cond_11

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 813
    :cond_11
    iget-object v2, p0, Lcom/google/r/b/a/ht;->d:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->d:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 822
    :cond_12
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v6, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 823
    :cond_13
    iget-object v2, p0, Lcom/google/r/b/a/ht;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_14
    move v2, v1

    .line 827
    goto/16 :goto_5

    .line 836
    :cond_15
    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-eq v2, v3, :cond_16

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/ht;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/ht;->a:I

    .line 837
    :cond_16
    iget-object v2, p0, Lcom/google/r/b/a/ht;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/hr;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_17
    move v2, v1

    .line 841
    goto/16 :goto_7

    :cond_18
    move v2, v1

    .line 845
    goto/16 :goto_8

    :cond_19
    move v2, v1

    .line 849
    goto/16 :goto_9

    :cond_1a
    move v2, v1

    .line 853
    goto/16 :goto_a

    :cond_1b
    move v0, v1

    .line 858
    goto/16 :goto_b
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 866
    iget v0, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 867
    iget-object v0, p0, Lcom/google/r/b/a/ht;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 884
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 866
    goto :goto_0

    .line 872
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 873
    iget-object v0, p0, Lcom/google/r/b/a/ht;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 875
    goto :goto_1

    :cond_2
    move v0, v1

    .line 872
    goto :goto_2

    .line 878
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ht;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 879
    iget-object v0, p0, Lcom/google/r/b/a/ht;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 881
    goto :goto_1

    :cond_4
    move v0, v1

    .line 878
    goto :goto_3

    :cond_5
    move v0, v2

    .line 884
    goto :goto_1
.end method
