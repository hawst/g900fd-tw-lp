.class public final Lcom/google/r/b/a/gq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/gv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/gq;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/gq;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 696
    new-instance v0, Lcom/google/r/b/a/gr;

    invoke-direct {v0}, Lcom/google/r/b/a/gr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/gq;->PARSER:Lcom/google/n/ax;

    .line 854
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/gq;->g:Lcom/google/n/aw;

    .line 1085
    new-instance v0, Lcom/google/r/b/a/gq;

    invoke-direct {v0}, Lcom/google/r/b/a/gq;-><init>()V

    sput-object v0, Lcom/google/r/b/a/gq;->d:Lcom/google/r/b/a/gq;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 641
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 796
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/gq;->c:Lcom/google/n/ao;

    .line 811
    iput-byte v2, p0, Lcom/google/r/b/a/gq;->e:B

    .line 833
    iput v2, p0, Lcom/google/r/b/a/gq;->f:I

    .line 642
    iput v3, p0, Lcom/google/r/b/a/gq;->b:I

    .line 643
    iget-object v0, p0, Lcom/google/r/b/a/gq;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 644
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 650
    invoke-direct {p0}, Lcom/google/r/b/a/gq;-><init>()V

    .line 651
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 656
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 657
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 658
    sparse-switch v3, :sswitch_data_0

    .line 663
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 665
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 661
    goto :goto_0

    .line 670
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 671
    invoke-static {v3}, Lcom/google/r/b/a/gt;->a(I)Lcom/google/r/b/a/gt;

    move-result-object v4

    .line 672
    if-nez v4, :cond_1

    .line 673
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 687
    :catch_0
    move-exception v0

    .line 688
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 693
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/gq;->au:Lcom/google/n/bn;

    throw v0

    .line 675
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/gq;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/gq;->a:I

    .line 676
    iput v3, p0, Lcom/google/r/b/a/gq;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 689
    :catch_1
    move-exception v0

    .line 690
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 691
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 681
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/gq;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 682
    iget v3, p0, Lcom/google/r/b/a/gq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/gq;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 693
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/gq;->au:Lcom/google/n/bn;

    .line 694
    return-void

    .line 658
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 639
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 796
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/gq;->c:Lcom/google/n/ao;

    .line 811
    iput-byte v1, p0, Lcom/google/r/b/a/gq;->e:B

    .line 833
    iput v1, p0, Lcom/google/r/b/a/gq;->f:I

    .line 640
    return-void
.end method

.method public static d()Lcom/google/r/b/a/gq;
    .locals 1

    .prologue
    .line 1088
    sget-object v0, Lcom/google/r/b/a/gq;->d:Lcom/google/r/b/a/gq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/gs;
    .locals 1

    .prologue
    .line 916
    new-instance v0, Lcom/google/r/b/a/gs;

    invoke-direct {v0}, Lcom/google/r/b/a/gs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/gq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 708
    sget-object v0, Lcom/google/r/b/a/gq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 823
    invoke-virtual {p0}, Lcom/google/r/b/a/gq;->c()I

    .line 824
    iget v0, p0, Lcom/google/r/b/a/gq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 825
    iget v0, p0, Lcom/google/r/b/a/gq;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 827
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/gq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 828
    iget-object v0, p0, Lcom/google/r/b/a/gq;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 830
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/gq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 831
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 813
    iget-byte v1, p0, Lcom/google/r/b/a/gq;->e:B

    .line 814
    if-ne v1, v0, :cond_0

    .line 818
    :goto_0
    return v0

    .line 815
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 817
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/gq;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 835
    iget v0, p0, Lcom/google/r/b/a/gq;->f:I

    .line 836
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 849
    :goto_0
    return v0

    .line 839
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/gq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 840
    iget v0, p0, Lcom/google/r/b/a/gq;->b:I

    .line 841
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 843
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/gq;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 844
    iget-object v2, p0, Lcom/google/r/b/a/gq;->c:Lcom/google/n/ao;

    .line 845
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 847
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/gq;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 848
    iput v0, p0, Lcom/google/r/b/a/gq;->f:I

    goto :goto_0

    .line 841
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 633
    invoke-static {}, Lcom/google/r/b/a/gq;->newBuilder()Lcom/google/r/b/a/gs;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/gs;->a(Lcom/google/r/b/a/gq;)Lcom/google/r/b/a/gs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 633
    invoke-static {}, Lcom/google/r/b/a/gq;->newBuilder()Lcom/google/r/b/a/gs;

    move-result-object v0

    return-object v0
.end method
