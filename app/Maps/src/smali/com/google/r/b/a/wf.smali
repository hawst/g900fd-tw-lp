.class public final Lcom/google/r/b/a/wf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/wi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/wf;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/wf;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2263
    new-instance v0, Lcom/google/r/b/a/wg;

    invoke-direct {v0}, Lcom/google/r/b/a/wg;-><init>()V

    sput-object v0, Lcom/google/r/b/a/wf;->PARSER:Lcom/google/n/ax;

    .line 2455
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/wf;->h:Lcom/google/n/aw;

    .line 2831
    new-instance v0, Lcom/google/r/b/a/wf;

    invoke-direct {v0}, Lcom/google/r/b/a/wf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/wf;->e:Lcom/google/r/b/a/wf;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2205
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2405
    iput-byte v0, p0, Lcom/google/r/b/a/wf;->f:B

    .line 2430
    iput v0, p0, Lcom/google/r/b/a/wf;->g:I

    .line 2206
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/wf;->b:Ljava/lang/Object;

    .line 2207
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/wf;->c:Ljava/lang/Object;

    .line 2208
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/wf;->d:Ljava/lang/Object;

    .line 2209
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2215
    invoke-direct {p0}, Lcom/google/r/b/a/wf;-><init>()V

    .line 2216
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2220
    const/4 v0, 0x0

    .line 2221
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2222
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2223
    sparse-switch v3, :sswitch_data_0

    .line 2228
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2230
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2226
    goto :goto_0

    .line 2235
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2236
    iget v4, p0, Lcom/google/r/b/a/wf;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/wf;->a:I

    .line 2237
    iput-object v3, p0, Lcom/google/r/b/a/wf;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2254
    :catch_0
    move-exception v0

    .line 2255
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/wf;->au:Lcom/google/n/bn;

    throw v0

    .line 2241
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2242
    iget v4, p0, Lcom/google/r/b/a/wf;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/wf;->a:I

    .line 2243
    iput-object v3, p0, Lcom/google/r/b/a/wf;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2256
    :catch_1
    move-exception v0

    .line 2257
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2258
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2247
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2248
    iget v4, p0, Lcom/google/r/b/a/wf;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/wf;->a:I

    .line 2249
    iput-object v3, p0, Lcom/google/r/b/a/wf;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2260
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wf;->au:Lcom/google/n/bn;

    .line 2261
    return-void

    .line 2223
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2203
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2405
    iput-byte v0, p0, Lcom/google/r/b/a/wf;->f:B

    .line 2430
    iput v0, p0, Lcom/google/r/b/a/wf;->g:I

    .line 2204
    return-void
.end method

.method public static d()Lcom/google/r/b/a/wf;
    .locals 1

    .prologue
    .line 2834
    sget-object v0, Lcom/google/r/b/a/wf;->e:Lcom/google/r/b/a/wf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/wh;
    .locals 1

    .prologue
    .line 2517
    new-instance v0, Lcom/google/r/b/a/wh;

    invoke-direct {v0}, Lcom/google/r/b/a/wh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/wf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2275
    sget-object v0, Lcom/google/r/b/a/wf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2417
    invoke-virtual {p0}, Lcom/google/r/b/a/wf;->c()I

    .line 2418
    iget v0, p0, Lcom/google/r/b/a/wf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 2419
    iget-object v0, p0, Lcom/google/r/b/a/wf;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wf;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2421
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/wf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 2422
    iget-object v0, p0, Lcom/google/r/b/a/wf;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wf;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2424
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/wf;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2425
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/wf;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wf;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2427
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/wf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2428
    return-void

    .line 2419
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 2422
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 2425
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2407
    iget-byte v1, p0, Lcom/google/r/b/a/wf;->f:B

    .line 2408
    if-ne v1, v0, :cond_0

    .line 2412
    :goto_0
    return v0

    .line 2409
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2411
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/wf;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2432
    iget v0, p0, Lcom/google/r/b/a/wf;->g:I

    .line 2433
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2450
    :goto_0
    return v0

    .line 2436
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/wf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 2438
    iget-object v0, p0, Lcom/google/r/b/a/wf;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wf;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 2440
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/wf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 2442
    iget-object v0, p0, Lcom/google/r/b/a/wf;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wf;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 2444
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/wf;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 2445
    const/4 v3, 0x3

    .line 2446
    iget-object v0, p0, Lcom/google/r/b/a/wf;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wf;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 2448
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/wf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 2449
    iput v0, p0, Lcom/google/r/b/a/wf;->g:I

    goto/16 :goto_0

    .line 2438
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 2442
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 2446
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2197
    invoke-static {}, Lcom/google/r/b/a/wf;->newBuilder()Lcom/google/r/b/a/wh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/wh;->a(Lcom/google/r/b/a/wf;)Lcom/google/r/b/a/wh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2197
    invoke-static {}, Lcom/google/r/b/a/wf;->newBuilder()Lcom/google/r/b/a/wh;

    move-result-object v0

    return-object v0
.end method
