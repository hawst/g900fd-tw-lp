.class public final Lcom/google/r/b/a/to;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/tp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/tm;",
        "Lcom/google/r/b/a/to;",
        ">;",
        "Lcom/google/r/b/a/tp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:F

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/n/ao;

.field private h:Ljava/lang/Object;

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 523
    sget-object v0, Lcom/google/r/b/a/tm;->j:Lcom/google/r/b/a/tm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 646
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/to;->b:Lcom/google/n/ao;

    .line 705
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/to;->c:Ljava/lang/Object;

    .line 813
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/to;->e:Ljava/lang/Object;

    .line 889
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/to;->f:Ljava/lang/Object;

    .line 965
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/to;->g:Lcom/google/n/ao;

    .line 1024
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/to;->h:Ljava/lang/Object;

    .line 524
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 515
    new-instance v2, Lcom/google/r/b/a/tm;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/tm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/to;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/tm;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/to;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/to;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/to;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/tm;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/r/b/a/to;->d:F

    iput v4, v2, Lcom/google/r/b/a/tm;->d:F

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/to;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/tm;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/to;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/tm;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/to;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/to;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/to;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/tm;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/r/b/a/to;->i:I

    iput v1, v2, Lcom/google/r/b/a/tm;->i:I

    iput v0, v2, Lcom/google/r/b/a/tm;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 515
    check-cast p1, Lcom/google/r/b/a/tm;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/to;->a(Lcom/google/r/b/a/tm;)Lcom/google/r/b/a/to;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/tm;)Lcom/google/r/b/a/to;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 595
    invoke-static {}, Lcom/google/r/b/a/tm;->d()Lcom/google/r/b/a/tm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 631
    :goto_0
    return-object p0

    .line 596
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 597
    iget-object v2, p0, Lcom/google/r/b/a/to;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/tm;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 598
    iget v2, p0, Lcom/google/r/b/a/to;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/to;->a:I

    .line 600
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 601
    iget v2, p0, Lcom/google/r/b/a/to;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/to;->a:I

    .line 602
    iget-object v2, p1, Lcom/google/r/b/a/tm;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/to;->c:Ljava/lang/Object;

    .line 605
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 606
    iget v2, p1, Lcom/google/r/b/a/tm;->d:F

    iget v3, p0, Lcom/google/r/b/a/to;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/to;->a:I

    iput v2, p0, Lcom/google/r/b/a/to;->d:F

    .line 608
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 609
    iget v2, p0, Lcom/google/r/b/a/to;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/to;->a:I

    .line 610
    iget-object v2, p1, Lcom/google/r/b/a/tm;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/to;->e:Ljava/lang/Object;

    .line 613
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 614
    iget v2, p0, Lcom/google/r/b/a/to;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/to;->a:I

    .line 615
    iget-object v2, p1, Lcom/google/r/b/a/tm;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/to;->f:Ljava/lang/Object;

    .line 618
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 619
    iget-object v2, p0, Lcom/google/r/b/a/to;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/tm;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 620
    iget v2, p0, Lcom/google/r/b/a/to;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/to;->a:I

    .line 622
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/tm;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 623
    iget v2, p0, Lcom/google/r/b/a/to;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/to;->a:I

    .line 624
    iget-object v2, p1, Lcom/google/r/b/a/tm;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/to;->h:Ljava/lang/Object;

    .line 627
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/tm;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 628
    iget v0, p1, Lcom/google/r/b/a/tm;->i:I

    iget v1, p0, Lcom/google/r/b/a/to;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/to;->a:I

    iput v0, p0, Lcom/google/r/b/a/to;->i:I

    .line 630
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/tm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 596
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 600
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 605
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 608
    goto :goto_4

    :cond_d
    move v2, v1

    .line 613
    goto :goto_5

    :cond_e
    move v2, v1

    .line 618
    goto :goto_6

    :cond_f
    move v2, v1

    .line 622
    goto :goto_7

    :cond_10
    move v0, v1

    .line 627
    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 635
    iget v0, p0, Lcom/google/r/b/a/to;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 636
    iget-object v0, p0, Lcom/google/r/b/a/to;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    invoke-virtual {v0}, Lcom/google/r/b/a/bi;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 641
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 635
    goto :goto_0

    :cond_1
    move v0, v2

    .line 641
    goto :goto_1
.end method
