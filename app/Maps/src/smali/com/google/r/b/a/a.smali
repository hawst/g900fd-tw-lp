.class public final enum Lcom/google/r/b/a/a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/a;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/a;

.field public static final enum b:Lcom/google/r/b/a/a;

.field public static final enum c:Lcom/google/r/b/a/a;

.field public static final enum d:Lcom/google/r/b/a/a;

.field public static final enum e:Lcom/google/r/b/a/a;

.field public static final enum f:Lcom/google/r/b/a/a;

.field public static final enum g:Lcom/google/r/b/a/a;

.field public static final enum h:Lcom/google/r/b/a/a;

.field public static final enum i:Lcom/google/r/b/a/a;

.field public static final enum j:Lcom/google/r/b/a/a;

.field public static final enum k:Lcom/google/r/b/a/a;

.field public static final enum l:Lcom/google/r/b/a/a;

.field public static final enum m:Lcom/google/r/b/a/a;

.field public static final enum n:Lcom/google/r/b/a/a;

.field public static final enum o:Lcom/google/r/b/a/a;

.field public static final enum p:Lcom/google/r/b/a/a;

.field public static final enum q:Lcom/google/r/b/a/a;

.field public static final enum r:Lcom/google/r/b/a/a;

.field public static final enum s:Lcom/google/r/b/a/a;

.field private static final synthetic u:[Lcom/google/r/b/a/a;


# instance fields
.field public final t:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 14
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "CLICK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    .line 18
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "TURN_ON"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    .line 22
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "TURN_OFF"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    .line 26
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "SHAKE"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->d:Lcom/google/r/b/a/a;

    .line 30
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "LONG_PRESS"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->e:Lcom/google/r/b/a/a;

    .line 34
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "SWIPE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    .line 38
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "TIMEOUT"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->g:Lcom/google/r/b/a/a;

    .line 42
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "AUTOMATED"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    .line 46
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "INPUT_VOICE"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->i:Lcom/google/r/b/a/a;

    .line 50
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "PINCH_OPEN"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->j:Lcom/google/r/b/a/a;

    .line 54
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "PINCH_CLOSED"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->k:Lcom/google/r/b/a/a;

    .line 58
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "DRAG"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->l:Lcom/google/r/b/a/a;

    .line 62
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "TWO_FINGER_DRAG_UP"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->m:Lcom/google/r/b/a/a;

    .line 66
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "TWO_FINGER_DRAG_DOWN"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->n:Lcom/google/r/b/a/a;

    .line 70
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "DOUBLE_TAP"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->o:Lcom/google/r/b/a/a;

    .line 74
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "TWO_FINGER_TAP"

    const/16 v2, 0xf

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->p:Lcom/google/r/b/a/a;

    .line 78
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "TAP_THEN_DRAG_UP"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->q:Lcom/google/r/b/a/a;

    .line 82
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "TAP_THEN_DRAG_DOWN"

    const/16 v2, 0x11

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->r:Lcom/google/r/b/a/a;

    .line 86
    new-instance v0, Lcom/google/r/b/a/a;

    const-string v1, "ROLL"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/a;->s:Lcom/google/r/b/a/a;

    .line 9
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/google/r/b/a/a;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/a;->d:Lcom/google/r/b/a/a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/a;->e:Lcom/google/r/b/a/a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/a;->g:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/a;->i:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/r/b/a/a;->j:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/r/b/a/a;->k:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/r/b/a/a;->l:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/r/b/a/a;->m:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/r/b/a/a;->n:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/r/b/a/a;->o:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/r/b/a/a;->p:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/r/b/a/a;->q:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/r/b/a/a;->r:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/r/b/a/a;->s:Lcom/google/r/b/a/a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/a;->u:[Lcom/google/r/b/a/a;

    .line 201
    new-instance v0, Lcom/google/r/b/a/b;

    invoke-direct {v0}, Lcom/google/r/b/a/b;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 211
    iput p3, p0, Lcom/google/r/b/a/a;->t:I

    .line 212
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/a;
    .locals 1

    .prologue
    .line 172
    packed-switch p0, :pswitch_data_0

    .line 192
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 173
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 174
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 175
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 176
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/a;->d:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 177
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/a;->e:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 178
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 179
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/a;->g:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 180
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 181
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/a;->i:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 182
    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/a;->j:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 183
    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/a;->k:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 184
    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/a;->l:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 185
    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/a;->m:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 186
    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/a;->n:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 187
    :pswitch_e
    sget-object v0, Lcom/google/r/b/a/a;->o:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 188
    :pswitch_f
    sget-object v0, Lcom/google/r/b/a/a;->p:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 189
    :pswitch_10
    sget-object v0, Lcom/google/r/b/a/a;->q:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 190
    :pswitch_11
    sget-object v0, Lcom/google/r/b/a/a;->r:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 191
    :pswitch_12
    sget-object v0, Lcom/google/r/b/a/a;->s:Lcom/google/r/b/a/a;

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/a;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/r/b/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/a;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/r/b/a/a;->u:[Lcom/google/r/b/a/a;

    invoke-virtual {v0}, [Lcom/google/r/b/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/a;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/google/r/b/a/a;->t:I

    return v0
.end method
