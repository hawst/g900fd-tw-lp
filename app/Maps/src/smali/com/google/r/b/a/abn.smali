.class public final Lcom/google/r/b/a/abn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/abs;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/abn;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/abn;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 680
    new-instance v0, Lcom/google/r/b/a/abo;

    invoke-direct {v0}, Lcom/google/r/b/a/abo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/abn;->PARSER:Lcom/google/n/ax;

    .line 874
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/abn;->g:Lcom/google/n/aw;

    .line 1188
    new-instance v0, Lcom/google/r/b/a/abn;

    invoke-direct {v0}, Lcom/google/r/b/a/abn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/abn;->d:Lcom/google/r/b/a/abn;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 617
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 831
    iput-byte v0, p0, Lcom/google/r/b/a/abn;->e:B

    .line 853
    iput v0, p0, Lcom/google/r/b/a/abn;->f:I

    .line 618
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/abn;->b:I

    .line 619
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    .line 620
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v2, 0x1

    .line 626
    invoke-direct {p0}, Lcom/google/r/b/a/abn;-><init>()V

    .line 629
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 632
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 633
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 634
    sparse-switch v4, :sswitch_data_0

    .line 639
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 641
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 637
    goto :goto_0

    .line 646
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 647
    invoke-static {v4}, Lcom/google/r/b/a/abq;->a(I)Lcom/google/r/b/a/abq;

    move-result-object v5

    .line 648
    if-nez v5, :cond_2

    .line 649
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 668
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 669
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 674
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 675
    iget-object v1, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    .line 677
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/abn;->au:Lcom/google/n/bn;

    throw v0

    .line 651
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/r/b/a/abn;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/abn;->a:I

    .line 652
    iput v4, p0, Lcom/google/r/b/a/abn;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 670
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 671
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 672
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 657
    :sswitch_2
    and-int/lit8 v4, v0, 0x2

    if-eq v4, v7, :cond_3

    .line 658
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    .line 660
    or-int/lit8 v0, v0, 0x2

    .line 662
    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 663
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 662
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 674
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    :cond_4
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_5

    .line 675
    iget-object v0, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    .line 677
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abn;->au:Lcom/google/n/bn;

    .line 678
    return-void

    .line 634
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 615
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 831
    iput-byte v0, p0, Lcom/google/r/b/a/abn;->e:B

    .line 853
    iput v0, p0, Lcom/google/r/b/a/abn;->f:I

    .line 616
    return-void
.end method

.method public static d()Lcom/google/r/b/a/abn;
    .locals 1

    .prologue
    .line 1191
    sget-object v0, Lcom/google/r/b/a/abn;->d:Lcom/google/r/b/a/abn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/abp;
    .locals 1

    .prologue
    .line 936
    new-instance v0, Lcom/google/r/b/a/abp;

    invoke-direct {v0}, Lcom/google/r/b/a/abp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/abn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 692
    sget-object v0, Lcom/google/r/b/a/abn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 843
    invoke-virtual {p0}, Lcom/google/r/b/a/abn;->c()I

    .line 844
    iget v0, p0, Lcom/google/r/b/a/abn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 845
    iget v0, p0, Lcom/google/r/b/a/abn;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 847
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 848
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 847
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 850
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/abn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 851
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 833
    iget-byte v1, p0, Lcom/google/r/b/a/abn;->e:B

    .line 834
    if-ne v1, v0, :cond_0

    .line 838
    :goto_0
    return v0

    .line 835
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 837
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/abn;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 855
    iget v0, p0, Lcom/google/r/b/a/abn;->f:I

    .line 856
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 869
    :goto_0
    return v0

    .line 859
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/abn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 860
    iget v0, p0, Lcom/google/r/b/a/abn;->b:I

    .line 861
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 863
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 864
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    .line 865
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 863
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 861
    :cond_1
    const/16 v0, 0xa

    goto :goto_1

    .line 867
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/abn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 868
    iput v0, p0, Lcom/google/r/b/a/abn;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 609
    invoke-static {}, Lcom/google/r/b/a/abn;->newBuilder()Lcom/google/r/b/a/abp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/abp;->a(Lcom/google/r/b/a/abn;)Lcom/google/r/b/a/abp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 609
    invoke-static {}, Lcom/google/r/b/a/abn;->newBuilder()Lcom/google/r/b/a/abp;

    move-result-object v0

    return-object v0
.end method
