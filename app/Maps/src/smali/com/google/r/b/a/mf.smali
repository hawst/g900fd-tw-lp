.class public final Lcom/google/r/b/a/mf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/mg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/md;",
        "Lcom/google/r/b/a/mf;",
        ">;",
        "Lcom/google/r/b/a/mg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/maps/g/gy;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2832
    sget-object v0, Lcom/google/r/b/a/md;->c:Lcom/google/r/b/a/md;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2872
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/r/b/a/mf;->b:Lcom/google/maps/g/gy;

    .line 2833
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2824
    new-instance v2, Lcom/google/r/b/a/md;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/md;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/mf;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/mf;->b:Lcom/google/maps/g/gy;

    iput-object v1, v2, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    iput v0, v2, Lcom/google/r/b/a/md;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2824
    check-cast p1, Lcom/google/r/b/a/md;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/mf;->a(Lcom/google/r/b/a/md;)Lcom/google/r/b/a/mf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/md;)Lcom/google/r/b/a/mf;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2858
    invoke-static {}, Lcom/google/r/b/a/md;->d()Lcom/google/r/b/a/md;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2863
    :goto_0
    return-object p0

    .line 2859
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/md;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 2860
    iget-object v0, p1, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v0

    :goto_2
    iget v2, p0, Lcom/google/r/b/a/mf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_4

    iget-object v1, p0, Lcom/google/r/b/a/mf;->b:Lcom/google/maps/g/gy;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/r/b/a/mf;->b:Lcom/google/maps/g/gy;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/r/b/a/mf;->b:Lcom/google/maps/g/gy;

    invoke-static {v1}, Lcom/google/maps/g/gy;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/ha;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/ha;->c()Lcom/google/maps/g/gy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mf;->b:Lcom/google/maps/g/gy;

    :goto_3
    iget v0, p0, Lcom/google/r/b/a/mf;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/mf;->a:I

    .line 2862
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/md;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 2859
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2860
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    goto :goto_2

    :cond_4
    iput-object v0, p0, Lcom/google/r/b/a/mf;->b:Lcom/google/maps/g/gy;

    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2867
    const/4 v0, 0x1

    return v0
.end method
