.class public final enum Lcom/google/r/b/a/yx;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/yx;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/yx;

.field public static final enum b:Lcom/google/r/b/a/yx;

.field private static final synthetic d:[Lcom/google/r/b/a/yx;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 178
    new-instance v0, Lcom/google/r/b/a/yx;

    const-string v1, "SPHERICAL"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, Lcom/google/r/b/a/yx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/yx;->a:Lcom/google/r/b/a/yx;

    .line 182
    new-instance v0, Lcom/google/r/b/a/yx;

    const-string v1, "CUBIC"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, Lcom/google/r/b/a/yx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/yx;->b:Lcom/google/r/b/a/yx;

    .line 173
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/r/b/a/yx;

    sget-object v1, Lcom/google/r/b/a/yx;->a:Lcom/google/r/b/a/yx;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/yx;->b:Lcom/google/r/b/a/yx;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/yx;->d:[Lcom/google/r/b/a/yx;

    .line 212
    new-instance v0, Lcom/google/r/b/a/yy;

    invoke-direct {v0}, Lcom/google/r/b/a/yy;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 222
    iput p3, p0, Lcom/google/r/b/a/yx;->c:I

    .line 223
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/yx;
    .locals 1

    .prologue
    .line 200
    packed-switch p0, :pswitch_data_0

    .line 203
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 201
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/yx;->a:Lcom/google/r/b/a/yx;

    goto :goto_0

    .line 202
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/yx;->b:Lcom/google/r/b/a/yx;

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/yx;
    .locals 1

    .prologue
    .line 173
    const-class v0, Lcom/google/r/b/a/yx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yx;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/yx;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/google/r/b/a/yx;->d:[Lcom/google/r/b/a/yx;

    invoke-virtual {v0}, [Lcom/google/r/b/a/yx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/yx;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/google/r/b/a/yx;->c:I

    return v0
.end method
