.class public final Lcom/google/r/b/a/kz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/la;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/kx;",
        "Lcom/google/r/b/a/kz;",
        ">;",
        "Lcom/google/r/b/a/la;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 386
    sget-object v0, Lcom/google/r/b/a/kx;->e:Lcom/google/r/b/a/kx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 469
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/kz;->b:Lcom/google/n/ao;

    .line 529
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    .line 665
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/kz;->d:Ljava/lang/Object;

    .line 387
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 378
    new-instance v2, Lcom/google/r/b/a/kx;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/kx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/kz;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/kx;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/kz;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/kz;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/r/b/a/kz;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/kz;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/kz;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/kx;->c:Ljava/util/List;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/kz;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/kx;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/kx;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 378
    check-cast p1, Lcom/google/r/b/a/kx;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/kz;->a(Lcom/google/r/b/a/kx;)Lcom/google/r/b/a/kz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/kx;)Lcom/google/r/b/a/kz;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 427
    invoke-static {}, Lcom/google/r/b/a/kx;->h()Lcom/google/r/b/a/kx;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 448
    :goto_0
    return-object p0

    .line 428
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/kx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 429
    iget-object v2, p0, Lcom/google/r/b/a/kz;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/kx;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 430
    iget v2, p0, Lcom/google/r/b/a/kz;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/kz;->a:I

    .line 432
    :cond_1
    iget-object v2, p1, Lcom/google/r/b/a/kx;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 433
    iget-object v2, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 434
    iget-object v2, p1, Lcom/google/r/b/a/kx;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    .line 435
    iget v2, p0, Lcom/google/r/b/a/kz;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/r/b/a/kz;->a:I

    .line 442
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/r/b/a/kx;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 443
    iget v0, p0, Lcom/google/r/b/a/kz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/kz;->a:I

    .line 444
    iget-object v0, p1, Lcom/google/r/b/a/kx;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/kz;->d:Ljava/lang/Object;

    .line 447
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/kx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 428
    goto :goto_1

    .line 437
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/kz;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/kz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/kz;->a:I

    .line 438
    :cond_6
    iget-object v2, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/kx;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_7
    move v0, v1

    .line 442
    goto :goto_3
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 452
    iget v0, p0, Lcom/google/r/b/a/kz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 453
    iget-object v0, p0, Lcom/google/r/b/a/kz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hn;->g()Lcom/google/r/b/a/hn;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hn;

    invoke-virtual {v0}, Lcom/google/r/b/a/hn;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 464
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 452
    goto :goto_0

    :cond_2
    move v1, v2

    .line 458
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 459
    iget-object v0, p0, Lcom/google/r/b/a/kz;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/be;->d()Lcom/google/r/b/a/be;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 464
    goto :goto_1
.end method
