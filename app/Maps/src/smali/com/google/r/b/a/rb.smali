.class public final Lcom/google/r/b/a/rb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/rc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/qz;",
        "Lcom/google/r/b/a/rb;",
        ">;",
        "Lcom/google/r/b/a/rc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x3e8

    const/16 v1, 0x32

    .line 543
    sget-object v0, Lcom/google/r/b/a/qz;->o:Lcom/google/r/b/a/qz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 691
    iput v2, p0, Lcom/google/r/b/a/rb;->b:I

    .line 723
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/google/r/b/a/rb;->c:I

    .line 755
    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/r/b/a/rb;->d:I

    .line 787
    const/16 v0, 0x23

    iput v0, p0, Lcom/google/r/b/a/rb;->e:I

    .line 819
    const/16 v0, 0xf

    iput v0, p0, Lcom/google/r/b/a/rb;->f:I

    .line 851
    iput v1, p0, Lcom/google/r/b/a/rb;->g:I

    .line 883
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/r/b/a/rb;->h:I

    .line 947
    iput v2, p0, Lcom/google/r/b/a/rb;->j:I

    .line 979
    const/16 v0, 0x12c

    iput v0, p0, Lcom/google/r/b/a/rb;->k:I

    .line 1011
    iput v1, p0, Lcom/google/r/b/a/rb;->l:I

    .line 1043
    const/16 v0, 0x2a

    iput v0, p0, Lcom/google/r/b/a/rb;->m:I

    .line 1075
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/google/r/b/a/rb;->n:I

    .line 544
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 535
    new-instance v2, Lcom/google/r/b/a/qz;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/qz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/rb;->b:I

    iput v1, v2, Lcom/google/r/b/a/qz;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/rb;->c:I

    iput v1, v2, Lcom/google/r/b/a/qz;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/rb;->d:I

    iput v1, v2, Lcom/google/r/b/a/qz;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/rb;->e:I

    iput v1, v2, Lcom/google/r/b/a/qz;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/r/b/a/rb;->f:I

    iput v1, v2, Lcom/google/r/b/a/qz;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/r/b/a/rb;->g:I

    iput v1, v2, Lcom/google/r/b/a/qz;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/r/b/a/rb;->h:I

    iput v1, v2, Lcom/google/r/b/a/qz;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/r/b/a/rb;->i:I

    iput v1, v2, Lcom/google/r/b/a/qz;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v1, p0, Lcom/google/r/b/a/rb;->j:I

    iput v1, v2, Lcom/google/r/b/a/qz;->j:I

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v1, p0, Lcom/google/r/b/a/rb;->k:I

    iput v1, v2, Lcom/google/r/b/a/qz;->k:I

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget v1, p0, Lcom/google/r/b/a/rb;->l:I

    iput v1, v2, Lcom/google/r/b/a/qz;->l:I

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget v1, p0, Lcom/google/r/b/a/rb;->m:I

    iput v1, v2, Lcom/google/r/b/a/qz;->m:I

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget v1, p0, Lcom/google/r/b/a/rb;->n:I

    iput v1, v2, Lcom/google/r/b/a/qz;->n:I

    iput v0, v2, Lcom/google/r/b/a/qz;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 535
    check-cast p1, Lcom/google/r/b/a/qz;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/rb;->a(Lcom/google/r/b/a/qz;)Lcom/google/r/b/a/rb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/qz;)Lcom/google/r/b/a/rb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 641
    invoke-static {}, Lcom/google/r/b/a/qz;->d()Lcom/google/r/b/a/qz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 682
    :goto_0
    return-object p0

    .line 642
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_e

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 643
    iget v2, p1, Lcom/google/r/b/a/qz;->b:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->b:I

    .line 645
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 646
    iget v2, p1, Lcom/google/r/b/a/qz;->c:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->c:I

    .line 648
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 649
    iget v2, p1, Lcom/google/r/b/a/qz;->d:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->d:I

    .line 651
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 652
    iget v2, p1, Lcom/google/r/b/a/qz;->e:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->e:I

    .line 654
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 655
    iget v2, p1, Lcom/google/r/b/a/qz;->f:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->f:I

    .line 657
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 658
    iget v2, p1, Lcom/google/r/b/a/qz;->g:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->g:I

    .line 660
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 661
    iget v2, p1, Lcom/google/r/b/a/qz;->h:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->h:I

    .line 663
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 664
    iget v2, p1, Lcom/google/r/b/a/qz;->i:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->i:I

    .line 666
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 667
    iget v2, p1, Lcom/google/r/b/a/qz;->j:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->j:I

    .line 669
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 670
    iget v2, p1, Lcom/google/r/b/a/qz;->k:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->k:I

    .line 672
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 673
    iget v2, p1, Lcom/google/r/b/a/qz;->l:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->l:I

    .line 675
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 676
    iget v2, p1, Lcom/google/r/b/a/qz;->m:I

    iget v3, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/r/b/a/rb;->a:I

    iput v2, p0, Lcom/google/r/b/a/rb;->m:I

    .line 678
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1a

    :goto_d
    if-eqz v0, :cond_d

    .line 679
    iget v0, p1, Lcom/google/r/b/a/qz;->n:I

    iget v1, p0, Lcom/google/r/b/a/rb;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/r/b/a/rb;->a:I

    iput v0, p0, Lcom/google/r/b/a/rb;->n:I

    .line 681
    :cond_d
    iget-object v0, p1, Lcom/google/r/b/a/qz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 642
    goto/16 :goto_1

    :cond_f
    move v2, v1

    .line 645
    goto/16 :goto_2

    :cond_10
    move v2, v1

    .line 648
    goto/16 :goto_3

    :cond_11
    move v2, v1

    .line 651
    goto/16 :goto_4

    :cond_12
    move v2, v1

    .line 654
    goto/16 :goto_5

    :cond_13
    move v2, v1

    .line 657
    goto/16 :goto_6

    :cond_14
    move v2, v1

    .line 660
    goto/16 :goto_7

    :cond_15
    move v2, v1

    .line 663
    goto/16 :goto_8

    :cond_16
    move v2, v1

    .line 666
    goto/16 :goto_9

    :cond_17
    move v2, v1

    .line 669
    goto :goto_a

    :cond_18
    move v2, v1

    .line 672
    goto :goto_b

    :cond_19
    move v2, v1

    .line 675
    goto :goto_c

    :cond_1a
    move v0, v1

    .line 678
    goto :goto_d
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 686
    const/4 v0, 0x1

    return v0
.end method
