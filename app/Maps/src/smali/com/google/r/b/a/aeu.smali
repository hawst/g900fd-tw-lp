.class public final Lcom/google/r/b/a/aeu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aev;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aes;",
        "Lcom/google/r/b/a/aeu;",
        ">;",
        "Lcom/google/r/b/a/aev;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 354
    sget-object v0, Lcom/google/r/b/a/aes;->e:Lcom/google/r/b/a/aes;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 438
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aeu;->b:Lcom/google/n/ao;

    .line 498
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    .line 634
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aeu;->d:Lcom/google/n/ao;

    .line 355
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 346
    new-instance v2, Lcom/google/r/b/a/aes;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aes;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aeu;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aeu;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aeu;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/aeu;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aeu;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/aeu;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v3, v2, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/aeu;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/aeu;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/aes;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 346
    check-cast p1, Lcom/google/r/b/a/aes;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aeu;->a(Lcom/google/r/b/a/aes;)Lcom/google/r/b/a/aeu;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aes;)Lcom/google/r/b/a/aeu;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 397
    invoke-static {}, Lcom/google/r/b/a/aes;->d()Lcom/google/r/b/a/aes;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 417
    :goto_0
    return-object p0

    .line 398
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 399
    iget-object v2, p0, Lcom/google/r/b/a/aeu;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 400
    iget v2, p0, Lcom/google/r/b/a/aeu;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/aeu;->a:I

    .line 402
    :cond_1
    iget-object v2, p1, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 403
    iget-object v2, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 404
    iget-object v2, p1, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    .line 405
    iget v2, p0, Lcom/google/r/b/a/aeu;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/r/b/a/aeu;->a:I

    .line 412
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 413
    iget-object v0, p0, Lcom/google/r/b/a/aeu;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 414
    iget v0, p0, Lcom/google/r/b/a/aeu;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/aeu;->a:I

    .line 416
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/aes;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 398
    goto :goto_1

    .line 407
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/aeu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/aeu;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/aeu;->a:I

    .line 408
    :cond_6
    iget-object v2, p0, Lcom/google/r/b/a/aeu;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_7
    move v0, v1

    .line 412
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 421
    iget v0, p0, Lcom/google/r/b/a/aeu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/google/r/b/a/aeu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 433
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 421
    goto :goto_0

    .line 427
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aeu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 428
    iget-object v0, p0, Lcom/google/r/b/a/aeu;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aag;

    invoke-virtual {v0}, Lcom/google/r/b/a/aag;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 430
    goto :goto_1

    :cond_2
    move v0, v1

    .line 427
    goto :goto_2

    :cond_3
    move v0, v2

    .line 433
    goto :goto_1
.end method
