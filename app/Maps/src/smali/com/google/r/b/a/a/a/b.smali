.class public final Lcom/google/r/b/a/a/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/a/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/a/a/b;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:J

.field public c:J

.field public d:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/r/b/a/a/a/c;

    invoke-direct {v0}, Lcom/google/r/b/a/a/a/c;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/a/b;->PARSER:Lcom/google/n/ax;

    .line 219
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/a/b;->h:Lcom/google/n/aw;

    .line 457
    new-instance v0, Lcom/google/r/b/a/a/a/b;

    invoke-direct {v0}, Lcom/google/r/b/a/a/a/b;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/a/b;->e:Lcom/google/r/b/a/a/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/16 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 169
    iput-byte v2, p0, Lcom/google/r/b/a/a/a/b;->f:B

    .line 194
    iput v2, p0, Lcom/google/r/b/a/a/a/b;->g:I

    .line 54
    iput-wide v0, p0, Lcom/google/r/b/a/a/a/b;->b:J

    .line 55
    iput-wide v0, p0, Lcom/google/r/b/a/a/a/b;->c:J

    .line 56
    iput-wide v0, p0, Lcom/google/r/b/a/a/a/b;->d:J

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 63
    invoke-direct {p0}, Lcom/google/r/b/a/a/a/b;-><init>()V

    .line 64
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 68
    const/4 v0, 0x0

    .line 69
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 71
    sparse-switch v3, :sswitch_data_0

    .line 76
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 78
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/a/a/b;->a:I

    .line 84
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    ushr-long v6, v4, v1

    const-wide/16 v8, 0x1

    and-long/2addr v4, v8

    neg-long v4, v4

    xor-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/r/b/a/a/a/b;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 88
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/a/a/b;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/a/a/b;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 93
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/a/a/b;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/a/a/b;->a:I

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/a/a/b;->d:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/a/b;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 51
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 169
    iput-byte v0, p0, Lcom/google/r/b/a/a/a/b;->f:B

    .line 194
    iput v0, p0, Lcom/google/r/b/a/a/a/b;->g:I

    .line 52
    return-void
.end method

.method public static d()Lcom/google/r/b/a/a/a/b;
    .locals 1

    .prologue
    .line 460
    sget-object v0, Lcom/google/r/b/a/a/a/b;->e:Lcom/google/r/b/a/a/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/a/d;
    .locals 1

    .prologue
    .line 281
    new-instance v0, Lcom/google/r/b/a/a/a/d;

    invoke-direct {v0}, Lcom/google/r/b/a/a/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/r/b/a/a/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 181
    invoke-virtual {p0}, Lcom/google/r/b/a/a/a/b;->c()I

    .line 182
    iget v0, p0, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 183
    iget-wide v0, p0, Lcom/google/r/b/a/a/a/b;->b:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->d(IJ)V

    .line 185
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 186
    iget-wide v0, p0, Lcom/google/r/b/a/a/a/b;->c:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/n/l;->a(IJ)V

    .line 188
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 189
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/r/b/a/a/a/b;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->a(IJ)V

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/a/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 192
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 171
    iget-byte v1, p0, Lcom/google/r/b/a/a/a/b;->f:B

    .line 172
    if-ne v1, v0, :cond_0

    .line 176
    :goto_0
    return v0

    .line 173
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/a/a/b;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 196
    iget v0, p0, Lcom/google/r/b/a/a/a/b;->g:I

    .line 197
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 214
    :goto_0
    return v0

    .line 200
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    .line 201
    iget-wide v2, p0, Lcom/google/r/b/a/a/a/b;->b:J

    .line 202
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    shl-long v4, v2, v4

    const/16 v6, 0x3f

    shr-long/2addr v2, v6

    xor-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 204
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v7, :cond_1

    .line 205
    iget-wide v2, p0, Lcom/google/r/b/a/a/a/b;->c:J

    .line 206
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 208
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 209
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/r/b/a/a/a/b;->d:J

    .line 210
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 212
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/a/a/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    iput v0, p0, Lcom/google/r/b/a/a/a/b;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/r/b/a/a/a/b;->newBuilder()Lcom/google/r/b/a/a/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/a/d;->a(Lcom/google/r/b/a/a/a/b;)Lcom/google/r/b/a/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/r/b/a/a/a/b;->newBuilder()Lcom/google/r/b/a/a/a/d;

    move-result-object v0

    return-object v0
.end method
