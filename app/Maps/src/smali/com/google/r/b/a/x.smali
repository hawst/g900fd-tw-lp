.class public final Lcom/google/r/b/a/x;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/y;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/v;",
        "Lcom/google/r/b/a/x;",
        ">;",
        "Lcom/google/r/b/a/y;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/f;

.field public c:I

.field public d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1800
    sget-object v0, Lcom/google/r/b/a/v;->e:Lcom/google/r/b/a/v;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1860
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/x;->b:Lcom/google/n/f;

    .line 1927
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/x;->d:Ljava/lang/Object;

    .line 1801
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1792
    new-instance v2, Lcom/google/r/b/a/v;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/v;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/x;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/x;->b:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/v;->b:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/x;->c:I

    iput v1, v2, Lcom/google/r/b/a/v;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/x;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/v;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1792
    check-cast p1, Lcom/google/r/b/a/v;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/x;->a(Lcom/google/r/b/a/v;)Lcom/google/r/b/a/x;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/v;)Lcom/google/r/b/a/x;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1838
    invoke-static {}, Lcom/google/r/b/a/v;->d()Lcom/google/r/b/a/v;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1851
    :goto_0
    return-object p0

    .line 1839
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 1840
    iget-object v2, p1, Lcom/google/r/b/a/v;->b:Lcom/google/n/f;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v2, v1

    .line 1839
    goto :goto_1

    .line 1840
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/x;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/x;->a:I

    iput-object v2, p0, Lcom/google/r/b/a/x;->b:Lcom/google/n/f;

    .line 1842
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_4

    .line 1843
    iget v2, p1, Lcom/google/r/b/a/v;->c:I

    iget v3, p0, Lcom/google/r/b/a/x;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/x;->a:I

    iput v2, p0, Lcom/google/r/b/a/x;->c:I

    .line 1845
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_5

    .line 1846
    iget v0, p0, Lcom/google/r/b/a/x;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/x;->a:I

    .line 1847
    iget-object v0, p1, Lcom/google/r/b/a/v;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/x;->d:Ljava/lang/Object;

    .line 1850
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/v;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 1842
    goto :goto_2

    :cond_7
    move v0, v1

    .line 1845
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1855
    const/4 v0, 0x1

    return v0
.end method
