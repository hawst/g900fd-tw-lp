.class public final Lcom/google/r/b/a/te;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/th;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/tc;",
        "Lcom/google/r/b/a/te;",
        ">;",
        "Lcom/google/r/b/a/th;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 364
    sget-object v0, Lcom/google/r/b/a/tc;->d:Lcom/google/r/b/a/tc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 415
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/te;->b:I

    .line 451
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/te;->c:Ljava/lang/Object;

    .line 365
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 356
    new-instance v2, Lcom/google/r/b/a/tc;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/tc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/te;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/te;->b:I

    iput v1, v2, Lcom/google/r/b/a/tc;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/te;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/tc;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/tc;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 356
    check-cast p1, Lcom/google/r/b/a/tc;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/te;->a(Lcom/google/r/b/a/tc;)Lcom/google/r/b/a/te;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/tc;)Lcom/google/r/b/a/te;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 396
    invoke-static {}, Lcom/google/r/b/a/tc;->d()Lcom/google/r/b/a/tc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 406
    :goto_0
    return-object p0

    .line 397
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/tc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 398
    iget v2, p1, Lcom/google/r/b/a/tc;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/tf;->a(I)Lcom/google/r/b/a/tf;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/tf;->a:Lcom/google/r/b/a/tf;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 397
    goto :goto_1

    .line 398
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/te;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/te;->a:I

    iget v2, v2, Lcom/google/r/b/a/tf;->g:I

    iput v2, p0, Lcom/google/r/b/a/te;->b:I

    .line 400
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/tc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 401
    iget v0, p0, Lcom/google/r/b/a/te;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/te;->a:I

    .line 402
    iget-object v0, p1, Lcom/google/r/b/a/tc;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/te;->c:Ljava/lang/Object;

    .line 405
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/tc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 400
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 410
    const/4 v0, 0x1

    return v0
.end method
