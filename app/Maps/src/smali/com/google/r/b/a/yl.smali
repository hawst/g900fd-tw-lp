.class public final Lcom/google/r/b/a/yl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ym;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/yj;",
        "Lcom/google/r/b/a/yl;",
        ">;",
        "Lcom/google/r/b/a/ym;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Ljava/lang/Object;

.field private j:I

.field private k:Lcom/google/r/b/a/ia;

.field private l:Ljava/lang/Object;

.field private m:Ljava/lang/Object;

.field private n:Ljava/lang/Object;

.field private o:I

.field private p:I

.field private q:I

.field private r:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x200

    .line 1331
    sget-object v0, Lcom/google/r/b/a/yj;->s:Lcom/google/r/b/a/yj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1615
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/r/b/a/yl;->d:I

    .line 1711
    iput v1, p0, Lcom/google/r/b/a/yl;->g:I

    .line 1743
    iput v1, p0, Lcom/google/r/b/a/yl;->h:I

    .line 1775
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yl;->i:Ljava/lang/Object;

    .line 1851
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/yl;->j:I

    .line 1883
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    .line 1944
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yl;->l:Ljava/lang/Object;

    .line 2020
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yl;->m:Ljava/lang/Object;

    .line 2096
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yl;->n:Ljava/lang/Object;

    .line 2236
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/yl;->q:I

    .line 2272
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/yl;->r:I

    .line 1332
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1323
    invoke-virtual {p0}, Lcom/google/r/b/a/yl;->c()Lcom/google/r/b/a/yj;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1323
    check-cast p1, Lcom/google/r/b/a/yj;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/yl;->a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;
    .locals 7

    .prologue
    const/16 v4, 0x200

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1453
    invoke-static {}, Lcom/google/r/b/a/yj;->d()Lcom/google/r/b/a/yj;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1514
    :goto_0
    return-object p0

    .line 1454
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_11

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1455
    iget-boolean v0, p1, Lcom/google/r/b/a/yj;->b:Z

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/yl;->b:Z

    .line 1457
    :cond_1
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_12

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 1458
    iget v0, p1, Lcom/google/r/b/a/yj;->c:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->c:I

    .line 1460
    :cond_2
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_13

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 1461
    iget v0, p1, Lcom/google/r/b/a/yj;->d:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->d:I

    .line 1463
    :cond_3
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_14

    move v0, v1

    :goto_4
    if-eqz v0, :cond_4

    .line 1464
    iget v0, p1, Lcom/google/r/b/a/yj;->e:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->e:I

    .line 1466
    :cond_4
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_15

    move v0, v1

    :goto_5
    if-eqz v0, :cond_5

    .line 1467
    iget v0, p1, Lcom/google/r/b/a/yj;->f:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->f:I

    .line 1469
    :cond_5
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_16

    move v0, v1

    :goto_6
    if-eqz v0, :cond_6

    .line 1470
    iget v0, p1, Lcom/google/r/b/a/yj;->g:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->g:I

    .line 1472
    :cond_6
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_17

    move v0, v1

    :goto_7
    if-eqz v0, :cond_7

    .line 1473
    iget v0, p1, Lcom/google/r/b/a/yj;->h:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->h:I

    .line 1475
    :cond_7
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_18

    move v0, v1

    :goto_8
    if-eqz v0, :cond_8

    .line 1476
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/yl;->a:I

    .line 1477
    iget-object v0, p1, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/yl;->i:Ljava/lang/Object;

    .line 1480
    :cond_8
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_19

    move v0, v1

    :goto_9
    if-eqz v0, :cond_9

    .line 1481
    iget v0, p1, Lcom/google/r/b/a/yj;->j:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->j:I

    .line 1483
    :cond_9
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x200

    if-ne v0, v4, :cond_1a

    move v0, v1

    :goto_a
    if-eqz v0, :cond_a

    .line 1484
    iget-object v0, p1, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    if-nez v0, :cond_1b

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v0

    :goto_b
    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    and-int/lit16 v3, v3, 0x200

    if-ne v3, v4, :cond_1c

    iget-object v3, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    if-eqz v3, :cond_1c

    iget-object v3, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v4

    if-eq v3, v4, :cond_1c

    iget-object v3, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    invoke-static {v3}, Lcom/google/r/b/a/ia;->a(Lcom/google/r/b/a/ia;)Lcom/google/r/b/a/ic;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/ic;->a(Lcom/google/r/b/a/ia;)Lcom/google/r/b/a/ic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/ic;->c()Lcom/google/r/b/a/ia;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    :goto_c
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/yl;->a:I

    .line 1486
    :cond_a
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_1d

    move v0, v1

    :goto_d
    if-eqz v0, :cond_b

    .line 1487
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/r/b/a/yl;->a:I

    .line 1488
    iget-object v0, p1, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/yl;->l:Ljava/lang/Object;

    .line 1491
    :cond_b
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_1e

    move v0, v1

    :goto_e
    if-eqz v0, :cond_c

    .line 1492
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/yl;->a:I

    .line 1493
    iget-object v0, p1, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/yl;->m:Ljava/lang/Object;

    .line 1496
    :cond_c
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_1f

    move v0, v1

    :goto_f
    if-eqz v0, :cond_d

    .line 1497
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/yl;->a:I

    .line 1498
    iget-object v0, p1, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/yl;->n:Ljava/lang/Object;

    .line 1501
    :cond_d
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_20

    move v0, v1

    :goto_10
    if-eqz v0, :cond_e

    .line 1502
    iget v0, p1, Lcom/google/r/b/a/yj;->o:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->o:I

    .line 1504
    :cond_e
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_21

    move v0, v1

    :goto_11
    if-eqz v0, :cond_f

    .line 1505
    iget v0, p1, Lcom/google/r/b/a/yj;->p:I

    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iput v0, p0, Lcom/google/r/b/a/yl;->p:I

    .line 1507
    :cond_f
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_22

    move v0, v1

    :goto_12
    if-eqz v0, :cond_24

    .line 1508
    iget v0, p1, Lcom/google/r/b/a/yj;->q:I

    invoke-static {v0}, Lcom/google/r/b/a/yn;->a(I)Lcom/google/r/b/a/yn;

    move-result-object v0

    if-nez v0, :cond_10

    sget-object v0, Lcom/google/r/b/a/yn;->a:Lcom/google/r/b/a/yn;

    :cond_10
    if-nez v0, :cond_23

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    move v0, v2

    .line 1454
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 1457
    goto/16 :goto_2

    :cond_13
    move v0, v2

    .line 1460
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 1463
    goto/16 :goto_4

    :cond_15
    move v0, v2

    .line 1466
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 1469
    goto/16 :goto_6

    :cond_17
    move v0, v2

    .line 1472
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 1475
    goto/16 :goto_8

    :cond_19
    move v0, v2

    .line 1480
    goto/16 :goto_9

    :cond_1a
    move v0, v2

    .line 1483
    goto/16 :goto_a

    .line 1484
    :cond_1b
    iget-object v0, p1, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    goto/16 :goto_b

    :cond_1c
    iput-object v0, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    goto/16 :goto_c

    :cond_1d
    move v0, v2

    .line 1486
    goto/16 :goto_d

    :cond_1e
    move v0, v2

    .line 1491
    goto/16 :goto_e

    :cond_1f
    move v0, v2

    .line 1496
    goto :goto_f

    :cond_20
    move v0, v2

    .line 1501
    goto :goto_10

    :cond_21
    move v0, v2

    .line 1504
    goto :goto_11

    :cond_22
    move v0, v2

    .line 1507
    goto :goto_12

    .line 1508
    :cond_23
    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/r/b/a/yl;->a:I

    iget v0, v0, Lcom/google/r/b/a/yn;->c:I

    iput v0, p0, Lcom/google/r/b/a/yl;->q:I

    .line 1510
    :cond_24
    iget v0, p1, Lcom/google/r/b/a/yj;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_26

    move v0, v1

    :goto_13
    if-eqz v0, :cond_28

    .line 1511
    iget v0, p1, Lcom/google/r/b/a/yj;->r:I

    invoke-static {v0}, Lcom/google/r/b/a/yz;->a(I)Lcom/google/r/b/a/yz;

    move-result-object v0

    if-nez v0, :cond_25

    sget-object v0, Lcom/google/r/b/a/yz;->a:Lcom/google/r/b/a/yz;

    :cond_25
    if-nez v0, :cond_27

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_26
    move v0, v2

    .line 1510
    goto :goto_13

    .line 1511
    :cond_27
    iget v1, p0, Lcom/google/r/b/a/yl;->a:I

    or-int/2addr v1, v6

    iput v1, p0, Lcom/google/r/b/a/yl;->a:I

    iget v0, v0, Lcom/google/r/b/a/yz;->c:I

    iput v0, p0, Lcom/google/r/b/a/yl;->r:I

    .line 1513
    :cond_28
    iget-object v0, p1, Lcom/google/r/b/a/yj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1518
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 1546
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1518
    goto :goto_0

    .line 1522
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-nez v0, :cond_3

    move v0, v1

    .line 1524
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1522
    goto :goto_2

    .line 1526
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    move v0, v1

    .line 1528
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1526
    goto :goto_3

    .line 1530
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_4
    if-nez v0, :cond_7

    move v0, v1

    .line 1532
    goto :goto_1

    :cond_6
    move v0, v1

    .line 1530
    goto :goto_4

    .line 1534
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_8

    move v0, v2

    :goto_5
    if-nez v0, :cond_9

    move v0, v1

    .line 1536
    goto :goto_1

    :cond_8
    move v0, v1

    .line 1534
    goto :goto_5

    .line 1538
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/yl;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_a

    move v0, v2

    :goto_6
    if-nez v0, :cond_b

    move v0, v1

    .line 1540
    goto :goto_1

    :cond_a
    move v0, v1

    .line 1538
    goto :goto_6

    .line 1542
    :cond_b
    iget-object v0, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v0

    :goto_7
    invoke-virtual {v0}, Lcom/google/r/b/a/ia;->b()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    .line 1544
    goto :goto_1

    .line 1542
    :cond_c
    iget-object v0, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    goto :goto_7

    :cond_d
    move v0, v2

    .line 1546
    goto :goto_1
.end method

.method public final c()Lcom/google/r/b/a/yj;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/high16 v6, 0x10000

    const v5, 0x8000

    .line 1377
    new-instance v2, Lcom/google/r/b/a/yj;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/yj;-><init>(Lcom/google/n/v;)V

    .line 1378
    iget v3, p0, Lcom/google/r/b/a/yl;->a:I

    .line 1379
    const/4 v1, 0x0

    .line 1380
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_10

    .line 1383
    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/yl;->b:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/yj;->b:Z

    .line 1384
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1385
    or-int/lit8 v0, v0, 0x2

    .line 1387
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/yl;->c:I

    iput v1, v2, Lcom/google/r/b/a/yj;->c:I

    .line 1388
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1389
    or-int/lit8 v0, v0, 0x4

    .line 1391
    :cond_1
    iget v1, p0, Lcom/google/r/b/a/yl;->d:I

    iput v1, v2, Lcom/google/r/b/a/yj;->d:I

    .line 1392
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 1393
    or-int/lit8 v0, v0, 0x8

    .line 1395
    :cond_2
    iget v1, p0, Lcom/google/r/b/a/yl;->e:I

    iput v1, v2, Lcom/google/r/b/a/yj;->e:I

    .line 1396
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 1397
    or-int/lit8 v0, v0, 0x10

    .line 1399
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/yl;->f:I

    iput v1, v2, Lcom/google/r/b/a/yj;->f:I

    .line 1400
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 1401
    or-int/lit8 v0, v0, 0x20

    .line 1403
    :cond_4
    iget v1, p0, Lcom/google/r/b/a/yl;->g:I

    iput v1, v2, Lcom/google/r/b/a/yj;->g:I

    .line 1404
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 1405
    or-int/lit8 v0, v0, 0x40

    .line 1407
    :cond_5
    iget v1, p0, Lcom/google/r/b/a/yl;->h:I

    iput v1, v2, Lcom/google/r/b/a/yj;->h:I

    .line 1408
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 1409
    or-int/lit16 v0, v0, 0x80

    .line 1411
    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/yl;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    .line 1412
    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 1413
    or-int/lit16 v0, v0, 0x100

    .line 1415
    :cond_7
    iget v1, p0, Lcom/google/r/b/a/yl;->j:I

    iput v1, v2, Lcom/google/r/b/a/yj;->j:I

    .line 1416
    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 1417
    or-int/lit16 v0, v0, 0x200

    .line 1419
    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/yl;->k:Lcom/google/r/b/a/ia;

    iput-object v1, v2, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    .line 1420
    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    .line 1421
    or-int/lit16 v0, v0, 0x400

    .line 1423
    :cond_9
    iget-object v1, p0, Lcom/google/r/b/a/yl;->l:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    .line 1424
    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    .line 1425
    or-int/lit16 v0, v0, 0x800

    .line 1427
    :cond_a
    iget-object v1, p0, Lcom/google/r/b/a/yl;->m:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    .line 1428
    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    .line 1429
    or-int/lit16 v0, v0, 0x1000

    .line 1431
    :cond_b
    iget-object v1, p0, Lcom/google/r/b/a/yl;->n:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    .line 1432
    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    .line 1433
    or-int/lit16 v0, v0, 0x2000

    .line 1435
    :cond_c
    iget v1, p0, Lcom/google/r/b/a/yl;->o:I

    iput v1, v2, Lcom/google/r/b/a/yj;->o:I

    .line 1436
    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    .line 1437
    or-int/lit16 v0, v0, 0x4000

    .line 1439
    :cond_d
    iget v1, p0, Lcom/google/r/b/a/yl;->p:I

    iput v1, v2, Lcom/google/r/b/a/yj;->p:I

    .line 1440
    and-int v1, v3, v5

    if-ne v1, v5, :cond_e

    .line 1441
    or-int/2addr v0, v5

    .line 1443
    :cond_e
    iget v1, p0, Lcom/google/r/b/a/yl;->q:I

    iput v1, v2, Lcom/google/r/b/a/yj;->q:I

    .line 1444
    and-int v1, v3, v6

    if-ne v1, v6, :cond_f

    .line 1445
    or-int/2addr v0, v6

    .line 1447
    :cond_f
    iget v1, p0, Lcom/google/r/b/a/yl;->r:I

    iput v1, v2, Lcom/google/r/b/a/yj;->r:I

    .line 1448
    iput v0, v2, Lcom/google/r/b/a/yj;->a:I

    .line 1449
    return-object v2

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method
