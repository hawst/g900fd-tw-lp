.class public final Lcom/google/r/b/a/ace;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/acl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ace;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/r/b/a/ace;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15801
    new-instance v0, Lcom/google/r/b/a/acf;

    invoke-direct {v0}, Lcom/google/r/b/a/acf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ace;->PARSER:Lcom/google/n/ax;

    .line 16985
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ace;->e:Lcom/google/n/aw;

    .line 17252
    new-instance v0, Lcom/google/r/b/a/ace;

    invoke-direct {v0}, Lcom/google/r/b/a/ace;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ace;->b:Lcom/google/r/b/a/ace;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15750
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 16949
    iput-byte v0, p0, Lcom/google/r/b/a/ace;->c:B

    .line 16968
    iput v0, p0, Lcom/google/r/b/a/ace;->d:I

    .line 15751
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    .line 15752
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 15758
    invoke-direct {p0}, Lcom/google/r/b/a/ace;-><init>()V

    .line 15761
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 15764
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 15765
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 15766
    sparse-switch v4, :sswitch_data_0

    .line 15771
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 15773
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 15769
    goto :goto_0

    .line 15778
    :sswitch_1
    and-int/lit8 v4, v0, 0x1

    if-eq v4, v2, :cond_1

    .line 15779
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    .line 15781
    or-int/lit8 v0, v0, 0x1

    .line 15783
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 15784
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 15783
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 15789
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 15790
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 15795
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 15796
    iget-object v1, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    .line 15798
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ace;->au:Lcom/google/n/bn;

    throw v0

    .line 15795
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 15796
    iget-object v0, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    .line 15798
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ace;->au:Lcom/google/n/bn;

    .line 15799
    return-void

    .line 15791
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 15792
    :try_start_2
    new-instance v4, Lcom/google/n/ak;

    .line 15793
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 15795
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 15766
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15748
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 16949
    iput-byte v0, p0, Lcom/google/r/b/a/ace;->c:B

    .line 16968
    iput v0, p0, Lcom/google/r/b/a/ace;->d:I

    .line 15749
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ace;
    .locals 1

    .prologue
    .line 17255
    sget-object v0, Lcom/google/r/b/a/ace;->b:Lcom/google/r/b/a/ace;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/acg;
    .locals 1

    .prologue
    .line 17047
    new-instance v0, Lcom/google/r/b/a/acg;

    invoke-direct {v0}, Lcom/google/r/b/a/acg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ace;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15813
    sget-object v0, Lcom/google/r/b/a/ace;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 16961
    invoke-virtual {p0}, Lcom/google/r/b/a/ace;->c()I

    .line 16962
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 16963
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 16962
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 16965
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/ace;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 16966
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 16951
    iget-byte v1, p0, Lcom/google/r/b/a/ace;->c:B

    .line 16952
    if-ne v1, v0, :cond_0

    .line 16956
    :goto_0
    return v0

    .line 16953
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 16955
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ace;->c:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 16970
    iget v0, p0, Lcom/google/r/b/a/ace;->d:I

    .line 16971
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 16980
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 16974
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 16975
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/r/b/a/ace;->a:Ljava/util/List;

    .line 16976
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 16974
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 16978
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/ace;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 16979
    iput v0, p0, Lcom/google/r/b/a/ace;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 15742
    invoke-static {}, Lcom/google/r/b/a/ace;->newBuilder()Lcom/google/r/b/a/acg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/acg;->a(Lcom/google/r/b/a/ace;)Lcom/google/r/b/a/acg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 15742
    invoke-static {}, Lcom/google/r/b/a/ace;->newBuilder()Lcom/google/r/b/a/acg;

    move-result-object v0

    return-object v0
.end method
