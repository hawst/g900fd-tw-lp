.class public final Lcom/google/r/b/a/md;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/mg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/md;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/md;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/maps/g/gy;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2685
    new-instance v0, Lcom/google/r/b/a/me;

    invoke-direct {v0}, Lcom/google/r/b/a/me;-><init>()V

    sput-object v0, Lcom/google/r/b/a/md;->PARSER:Lcom/google/n/ax;

    .line 2752
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/md;->f:Lcom/google/n/aw;

    .line 2937
    new-instance v0, Lcom/google/r/b/a/md;

    invoke-direct {v0}, Lcom/google/r/b/a/md;-><init>()V

    sput-object v0, Lcom/google/r/b/a/md;->c:Lcom/google/r/b/a/md;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2635
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2716
    iput-byte v0, p0, Lcom/google/r/b/a/md;->d:B

    .line 2735
    iput v0, p0, Lcom/google/r/b/a/md;->e:I

    .line 2636
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 2642
    invoke-direct {p0}, Lcom/google/r/b/a/md;-><init>()V

    .line 2643
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 2647
    const/4 v0, 0x0

    move v2, v0

    .line 2648
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 2649
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 2650
    sparse-switch v0, :sswitch_data_0

    .line 2655
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 2657
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 2653
    goto :goto_0

    .line 2662
    :sswitch_1
    const/4 v0, 0x0

    .line 2663
    iget v1, p0, Lcom/google/r/b/a/md;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 2664
    iget-object v0, p0, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/ha;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    move-result-object v0

    move-object v1, v0

    .line 2666
    :goto_1
    sget-object v0, Lcom/google/maps/g/gy;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    iput-object v0, p0, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    .line 2667
    if-eqz v1, :cond_1

    .line 2668
    iget-object v0, p0, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    invoke-virtual {v1, v0}, Lcom/google/maps/g/ha;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/ha;

    .line 2669
    invoke-virtual {v1}, Lcom/google/maps/g/ha;->c()Lcom/google/maps/g/gy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    .line 2671
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/md;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/md;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2676
    :catch_0
    move-exception v0

    .line 2677
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2682
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/md;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/md;->au:Lcom/google/n/bn;

    .line 2683
    return-void

    .line 2678
    :catch_1
    move-exception v0

    .line 2679
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 2680
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 2650
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2633
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2716
    iput-byte v0, p0, Lcom/google/r/b/a/md;->d:B

    .line 2735
    iput v0, p0, Lcom/google/r/b/a/md;->e:I

    .line 2634
    return-void
.end method

.method public static d()Lcom/google/r/b/a/md;
    .locals 1

    .prologue
    .line 2940
    sget-object v0, Lcom/google/r/b/a/md;->c:Lcom/google/r/b/a/md;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/mf;
    .locals 1

    .prologue
    .line 2814
    new-instance v0, Lcom/google/r/b/a/mf;

    invoke-direct {v0}, Lcom/google/r/b/a/mf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/md;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2697
    sget-object v0, Lcom/google/r/b/a/md;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2728
    invoke-virtual {p0}, Lcom/google/r/b/a/md;->c()I

    .line 2729
    iget v0, p0, Lcom/google/r/b/a/md;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2730
    iget-object v0, p0, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 2732
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/md;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2733
    return-void

    .line 2730
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2718
    iget-byte v1, p0, Lcom/google/r/b/a/md;->d:B

    .line 2719
    if-ne v1, v0, :cond_0

    .line 2723
    :goto_0
    return v0

    .line 2720
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2722
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/md;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2737
    iget v0, p0, Lcom/google/r/b/a/md;->e:I

    .line 2738
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2747
    :goto_0
    return v0

    .line 2741
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/md;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 2743
    iget-object v0, p0, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 2745
    :goto_2
    iget-object v1, p0, Lcom/google/r/b/a/md;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2746
    iput v0, p0, Lcom/google/r/b/a/md;->e:I

    goto :goto_0

    .line 2743
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/md;->b:Lcom/google/maps/g/gy;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2627
    invoke-static {}, Lcom/google/r/b/a/md;->newBuilder()Lcom/google/r/b/a/mf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/mf;->a(Lcom/google/r/b/a/md;)Lcom/google/r/b/a/mf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2627
    invoke-static {}, Lcom/google/r/b/a/md;->newBuilder()Lcom/google/r/b/a/mf;

    move-result-object v0

    return-object v0
.end method
