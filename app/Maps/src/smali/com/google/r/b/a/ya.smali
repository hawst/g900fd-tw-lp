.class public final Lcom/google/r/b/a/ya;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/yh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ya;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/ya;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/yd;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3292
    new-instance v0, Lcom/google/r/b/a/yb;

    invoke-direct {v0}, Lcom/google/r/b/a/yb;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ya;->PARSER:Lcom/google/n/ax;

    .line 4377
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ya;->h:Lcom/google/n/aw;

    .line 4722
    new-instance v0, Lcom/google/r/b/a/ya;

    invoke-direct {v0}, Lcom/google/r/b/a/ya;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ya;->e:Lcom/google/r/b/a/ya;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3230
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4321
    iput-byte v0, p0, Lcom/google/r/b/a/ya;->f:B

    .line 4352
    iput v0, p0, Lcom/google/r/b/a/ya;->g:I

    .line 3231
    const v0, 0x6ddd0

    iput v0, p0, Lcom/google/r/b/a/ya;->b:I

    .line 3232
    const v0, 0x7a120

    iput v0, p0, Lcom/google/r/b/a/ya;->c:I

    .line 3233
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    .line 3234
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v7, 0x4

    .line 3240
    invoke-direct {p0}, Lcom/google/r/b/a/ya;-><init>()V

    .line 3243
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 3246
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 3247
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 3248
    sparse-switch v4, :sswitch_data_0

    .line 3253
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 3255
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 3251
    goto :goto_0

    .line 3260
    :sswitch_1
    iget v4, p0, Lcom/google/r/b/a/ya;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ya;->a:I

    .line 3261
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/ya;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 3280
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 3281
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3286
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v7, :cond_1

    .line 3287
    iget-object v1, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    .line 3289
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ya;->au:Lcom/google/n/bn;

    throw v0

    .line 3265
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/ya;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/ya;->a:I

    .line 3266
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/ya;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 3282
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 3283
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 3284
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3270
    :sswitch_3
    and-int/lit8 v4, v0, 0x4

    if-eq v4, v7, :cond_2

    .line 3271
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    .line 3272
    or-int/lit8 v0, v0, 0x4

    .line 3274
    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    const/16 v5, 0x34

    sget-object v6, Lcom/google/r/b/a/yd;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, v6, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 3286
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    :cond_3
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 3287
    iget-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    .line 3289
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ya;->au:Lcom/google/n/bn;

    .line 3290
    return-void

    .line 3248
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x190 -> :sswitch_1
        0x198 -> :sswitch_2
        0x1a3 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3228
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4321
    iput-byte v0, p0, Lcom/google/r/b/a/ya;->f:B

    .line 4352
    iput v0, p0, Lcom/google/r/b/a/ya;->g:I

    .line 3229
    return-void
.end method

.method public static a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;
    .locals 1

    .prologue
    .line 4442
    invoke-static {}, Lcom/google/r/b/a/ya;->newBuilder()Lcom/google/r/b/a/yc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yc;->a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/ya;
    .locals 1

    .prologue
    .line 4725
    sget-object v0, Lcom/google/r/b/a/ya;->e:Lcom/google/r/b/a/ya;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/yc;
    .locals 1

    .prologue
    .line 4439
    new-instance v0, Lcom/google/r/b/a/yc;

    invoke-direct {v0}, Lcom/google/r/b/a/yc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ya;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3304
    sget-object v0, Lcom/google/r/b/a/ya;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 4339
    invoke-virtual {p0}, Lcom/google/r/b/a/ya;->c()I

    .line 4340
    iget v0, p0, Lcom/google/r/b/a/ya;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 4341
    const/16 v0, 0x32

    iget v1, p0, Lcom/google/r/b/a/ya;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 4343
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ya;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 4344
    const/16 v0, 0x33

    iget v1, p0, Lcom/google/r/b/a/ya;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 4346
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4347
    const/16 v2, 0x34

    iget-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/at;)V

    .line 4346
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4349
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ya;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4350
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4323
    iget-byte v0, p0, Lcom/google/r/b/a/ya;->f:B

    .line 4324
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 4334
    :cond_0
    :goto_0
    return v2

    .line 4325
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 4327
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 4328
    iget-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yd;

    invoke-virtual {v0}, Lcom/google/r/b/a/yd;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4329
    iput-byte v2, p0, Lcom/google/r/b/a/ya;->f:B

    goto :goto_0

    .line 4327
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 4333
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/ya;->f:B

    move v2, v3

    .line 4334
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 4354
    iget v0, p0, Lcom/google/r/b/a/ya;->g:I

    .line 4355
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 4372
    :goto_0
    return v0

    .line 4358
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ya;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_5

    .line 4359
    const/16 v0, 0x32

    iget v3, p0, Lcom/google/r/b/a/ya;->b:I

    .line 4360
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x0

    .line 4362
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/ya;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 4363
    const/16 v3, 0x33

    iget v4, p0, Lcom/google/r/b/a/ya;->c:I

    .line 4364
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_1

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    :cond_2
    move v1, v2

    move v3, v0

    .line 4366
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 4367
    const/16 v4, 0x34

    iget-object v0, p0, Lcom/google/r/b/a/ya;->d:Ljava/util/List;

    .line 4368
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 4366
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v1

    .line 4360
    goto :goto_1

    .line 4370
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/ya;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 4371
    iput v0, p0, Lcom/google/r/b/a/ya;->g:I

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3222
    invoke-static {}, Lcom/google/r/b/a/ya;->newBuilder()Lcom/google/r/b/a/yc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yc;->a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3222
    invoke-static {}, Lcom/google/r/b/a/ya;->newBuilder()Lcom/google/r/b/a/yc;

    move-result-object v0

    return-object v0
.end method
