.class public final enum Lcom/google/r/b/a/gb;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/gb;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/gb;

.field public static final enum b:Lcom/google/r/b/a/gb;

.field public static final enum c:Lcom/google/r/b/a/gb;

.field public static final enum d:Lcom/google/r/b/a/gb;

.field public static final enum e:Lcom/google/r/b/a/gb;

.field public static final enum f:Lcom/google/r/b/a/gb;

.field public static final enum g:Lcom/google/r/b/a/gb;

.field private static final synthetic i:[Lcom/google/r/b/a/gb;


# instance fields
.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/r/b/a/gb;

    const-string v1, "EIT_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gb;->a:Lcom/google/r/b/a/gb;

    .line 18
    new-instance v0, Lcom/google/r/b/a/gb;

    const-string v1, "EIT_MAP_VIEW"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gb;->b:Lcom/google/r/b/a/gb;

    .line 22
    new-instance v0, Lcom/google/r/b/a/gb;

    const-string v1, "EIT_STREET_VIEW"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gb;->c:Lcom/google/r/b/a/gb;

    .line 26
    new-instance v0, Lcom/google/r/b/a/gb;

    const-string v1, "EIT_SEARCH"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/r/b/a/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gb;->d:Lcom/google/r/b/a/gb;

    .line 30
    new-instance v0, Lcom/google/r/b/a/gb;

    const-string v1, "EIT_DIRECTIONS"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/r/b/a/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gb;->e:Lcom/google/r/b/a/gb;

    .line 34
    new-instance v0, Lcom/google/r/b/a/gb;

    const-string v1, "EIT_NAVIGATION"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gb;->f:Lcom/google/r/b/a/gb;

    .line 38
    new-instance v0, Lcom/google/r/b/a/gb;

    const-string v1, "EIT_HANDLE_MFE_URL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/gb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gb;->g:Lcom/google/r/b/a/gb;

    .line 9
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/r/b/a/gb;

    sget-object v1, Lcom/google/r/b/a/gb;->a:Lcom/google/r/b/a/gb;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/gb;->b:Lcom/google/r/b/a/gb;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/gb;->c:Lcom/google/r/b/a/gb;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/gb;->d:Lcom/google/r/b/a/gb;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/gb;->e:Lcom/google/r/b/a/gb;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/gb;->f:Lcom/google/r/b/a/gb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/gb;->g:Lcom/google/r/b/a/gb;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/gb;->i:[Lcom/google/r/b/a/gb;

    .line 93
    new-instance v0, Lcom/google/r/b/a/gc;

    invoke-direct {v0}, Lcom/google/r/b/a/gc;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 103
    iput p3, p0, Lcom/google/r/b/a/gb;->h:I

    .line 104
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 76
    packed-switch p0, :pswitch_data_0

    .line 84
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 77
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/gb;->a:Lcom/google/r/b/a/gb;

    goto :goto_0

    .line 78
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/gb;->b:Lcom/google/r/b/a/gb;

    goto :goto_0

    .line 79
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/gb;->c:Lcom/google/r/b/a/gb;

    goto :goto_0

    .line 80
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/gb;->d:Lcom/google/r/b/a/gb;

    goto :goto_0

    .line 81
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/gb;->e:Lcom/google/r/b/a/gb;

    goto :goto_0

    .line 82
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/gb;->f:Lcom/google/r/b/a/gb;

    goto :goto_0

    .line 83
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/gb;->g:Lcom/google/r/b/a/gb;

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/r/b/a/gb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/gb;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/r/b/a/gb;->i:[Lcom/google/r/b/a/gb;

    invoke-virtual {v0}, [Lcom/google/r/b/a/gb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/gb;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/r/b/a/gb;->h:I

    return v0
.end method
