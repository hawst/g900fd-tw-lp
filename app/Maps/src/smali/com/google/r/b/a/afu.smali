.class public final Lcom/google/r/b/a/afu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afu;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/r/b/a/afu;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13137
    new-instance v0, Lcom/google/r/b/a/afv;

    invoke-direct {v0}, Lcom/google/r/b/a/afv;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afu;->PARSER:Lcom/google/n/ax;

    .line 13181
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/afu;->d:Lcom/google/n/aw;

    .line 13292
    new-instance v0, Lcom/google/r/b/a/afu;

    invoke-direct {v0}, Lcom/google/r/b/a/afu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afu;->a:Lcom/google/r/b/a/afu;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 13101
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 13152
    iput-byte v0, p0, Lcom/google/r/b/a/afu;->b:B

    .line 13168
    iput v0, p0, Lcom/google/r/b/a/afu;->c:I

    .line 13102
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 13108
    invoke-direct {p0}, Lcom/google/r/b/a/afu;-><init>()V

    .line 13110
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 13112
    const/4 v0, 0x0

    .line 13113
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 13114
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 13115
    packed-switch v3, :pswitch_data_0

    .line 13120
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 13122
    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 13118
    goto :goto_0

    .line 13134
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afu;->au:Lcom/google/n/bn;

    .line 13135
    return-void

    .line 13128
    :catch_0
    move-exception v0

    .line 13129
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13134
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afu;->au:Lcom/google/n/bn;

    throw v0

    .line 13130
    :catch_1
    move-exception v0

    .line 13131
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 13132
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 13115
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 13099
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 13152
    iput-byte v0, p0, Lcom/google/r/b/a/afu;->b:B

    .line 13168
    iput v0, p0, Lcom/google/r/b/a/afu;->c:I

    .line 13100
    return-void
.end method

.method public static a(Lcom/google/r/b/a/afu;)Lcom/google/r/b/a/afw;
    .locals 1

    .prologue
    .line 13246
    invoke-static {}, Lcom/google/r/b/a/afu;->newBuilder()Lcom/google/r/b/a/afw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/afw;->a(Lcom/google/r/b/a/afu;)Lcom/google/r/b/a/afw;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/afu;
    .locals 1

    .prologue
    .line 13295
    sget-object v0, Lcom/google/r/b/a/afu;->a:Lcom/google/r/b/a/afu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/afw;
    .locals 1

    .prologue
    .line 13243
    new-instance v0, Lcom/google/r/b/a/afw;

    invoke-direct {v0}, Lcom/google/r/b/a/afw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13149
    sget-object v0, Lcom/google/r/b/a/afu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 13164
    iget v0, p0, Lcom/google/r/b/a/afu;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13165
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/afu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 13166
    return-void

    .line 13164
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/afu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/afu;->c:I

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 13154
    iget-byte v1, p0, Lcom/google/r/b/a/afu;->b:B

    .line 13155
    if-ne v1, v0, :cond_0

    .line 13159
    :goto_0
    return v0

    .line 13156
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 13158
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/afu;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 13170
    iget v0, p0, Lcom/google/r/b/a/afu;->c:I

    .line 13171
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13176
    :goto_0
    return v0

    .line 13173
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/afu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13175
    iput v0, p0, Lcom/google/r/b/a/afu;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13093
    invoke-static {}, Lcom/google/r/b/a/afu;->newBuilder()Lcom/google/r/b/a/afw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/afw;->a(Lcom/google/r/b/a/afu;)Lcom/google/r/b/a/afw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13093
    invoke-static {}, Lcom/google/r/b/a/afu;->newBuilder()Lcom/google/r/b/a/afw;

    move-result-object v0

    return-object v0
.end method
