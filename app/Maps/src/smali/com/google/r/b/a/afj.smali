.class public final Lcom/google/r/b/a/afj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afj;",
            ">;"
        }
    .end annotation
.end field

.field static final q:Lcom/google/r/b/a/afj;

.field private static final serialVersionUID:J

.field private static volatile t:Lcom/google/n/aw;


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Lcom/google/n/ao;

.field e:I

.field public f:Lcom/google/n/ao;

.field public g:I

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/Object;

.field j:Z

.field public k:Lcom/google/n/ao;

.field l:Z

.field m:Z

.field n:Z

.field public o:Lcom/google/n/ao;

.field public p:Z

.field private r:B

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7579
    new-instance v0, Lcom/google/r/b/a/afk;

    invoke-direct {v0}, Lcom/google/r/b/a/afk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afj;->PARSER:Lcom/google/n/ax;

    .line 8050
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/afj;->t:Lcom/google/n/aw;

    .line 9190
    new-instance v0, Lcom/google/r/b/a/afj;

    invoke-direct {v0}, Lcom/google/r/b/a/afj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afj;->q:Lcom/google/r/b/a/afj;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 7417
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 7655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    .line 7687
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    .line 7819
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    .line 7880
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    .line 7910
    iput-byte v4, p0, Lcom/google/r/b/a/afj;->r:B

    .line 7977
    iput v4, p0, Lcom/google/r/b/a/afj;->s:I

    .line 7418
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    .line 7419
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/r/b/a/afj;->c:I

    .line 7420
    iget-object v0, p0, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 7421
    iput v5, p0, Lcom/google/r/b/a/afj;->e:I

    .line 7422
    iget-object v0, p0, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 7423
    iput v5, p0, Lcom/google/r/b/a/afj;->g:I

    .line 7424
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    .line 7425
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    .line 7426
    iput-boolean v2, p0, Lcom/google/r/b/a/afj;->j:Z

    .line 7427
    iget-object v0, p0, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 7428
    iput-boolean v2, p0, Lcom/google/r/b/a/afj;->l:Z

    .line 7429
    iput-boolean v2, p0, Lcom/google/r/b/a/afj;->m:Z

    .line 7430
    iput-boolean v2, p0, Lcom/google/r/b/a/afj;->n:Z

    .line 7431
    iget-object v0, p0, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 7432
    iput-boolean v2, p0, Lcom/google/r/b/a/afj;->p:Z

    .line 7433
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/16 v7, 0x40

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 7439
    invoke-direct {p0}, Lcom/google/r/b/a/afj;-><init>()V

    .line 7442
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 7445
    :cond_0
    :goto_0
    if-nez v0, :cond_8

    .line 7446
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 7447
    sparse-switch v4, :sswitch_data_0

    .line 7452
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 7454
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 7450
    goto :goto_0

    .line 7459
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 7460
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    .line 7462
    or-int/lit8 v1, v1, 0x1

    .line 7464
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 7465
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 7464
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 7564
    :catch_0
    move-exception v0

    .line 7565
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7570
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_2

    .line 7571
    iget-object v2, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    .line 7573
    :cond_2
    and-int/lit8 v1, v1, 0x40

    if-ne v1, v7, :cond_3

    .line 7574
    iget-object v1, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    .line 7576
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afj;->au:Lcom/google/n/bn;

    throw v0

    .line 7469
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 7470
    invoke-static {v4}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v5

    .line 7471
    if-nez v5, :cond_4

    .line 7472
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 7566
    :catch_1
    move-exception v0

    .line 7567
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 7568
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7474
    :cond_4
    :try_start_4
    iget v5, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7475
    iput v4, p0, Lcom/google/r/b/a/afj;->c:I

    goto :goto_0

    .line 7480
    :sswitch_3
    iget-object v4, p0, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 7481
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    goto/16 :goto_0

    .line 7485
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 7486
    invoke-static {v4}, Lcom/google/maps/g/a/dx;->a(I)Lcom/google/maps/g/a/dx;

    move-result-object v5

    .line 7487
    if-nez v5, :cond_5

    .line 7488
    const/4 v5, 0x5

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 7490
    :cond_5
    iget v5, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7491
    iput v4, p0, Lcom/google/r/b/a/afj;->e:I

    goto/16 :goto_0

    .line 7496
    :sswitch_5
    iget-object v4, p0, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 7497
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    goto/16 :goto_0

    .line 7501
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 7502
    invoke-static {v4}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v5

    .line 7503
    if-nez v5, :cond_6

    .line 7504
    const/4 v5, 0x7

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 7506
    :cond_6
    iget v5, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7507
    iput v4, p0, Lcom/google/r/b/a/afj;->g:I

    goto/16 :goto_0

    .line 7512
    :sswitch_7
    and-int/lit8 v4, v1, 0x40

    if-eq v4, v7, :cond_7

    .line 7513
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    .line 7515
    or-int/lit8 v1, v1, 0x40

    .line 7517
    :cond_7
    iget-object v4, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 7518
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 7517
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 7522
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 7523
    iget v5, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7524
    iput-object v4, p0, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 7528
    :sswitch_9
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7529
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/afj;->j:Z

    goto/16 :goto_0

    .line 7533
    :sswitch_a
    iget-object v4, p0, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 7534
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    goto/16 :goto_0

    .line 7538
    :sswitch_b
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7539
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/afj;->l:Z

    goto/16 :goto_0

    .line 7543
    :sswitch_c
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7544
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/afj;->m:Z

    goto/16 :goto_0

    .line 7548
    :sswitch_d
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7549
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/afj;->n:Z

    goto/16 :goto_0

    .line 7553
    :sswitch_e
    iget-object v4, p0, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 7554
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit16 v4, v4, 0x800

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    goto/16 :goto_0

    .line 7558
    :sswitch_f
    iget v4, p0, Lcom/google/r/b/a/afj;->a:I

    or-int/lit16 v4, v4, 0x1000

    iput v4, p0, Lcom/google/r/b/a/afj;->a:I

    .line 7559
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/afj;->p:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 7570
    :cond_8
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_9

    .line 7571
    iget-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    .line 7573
    :cond_9
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v7, :cond_a

    .line 7574
    iget-object v0, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    .line 7576
    :cond_a
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afj;->au:Lcom/google/n/bn;

    .line 7577
    return-void

    .line 7447
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x4a -> :sswitch_7
        0x6a -> :sswitch_8
        0x70 -> :sswitch_9
        0x7a -> :sswitch_a
        0x80 -> :sswitch_b
        0x88 -> :sswitch_c
        0x90 -> :sswitch_d
        0x9a -> :sswitch_e
        0xb0 -> :sswitch_f
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 7415
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 7655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    .line 7687
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    .line 7819
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    .line 7880
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    .line 7910
    iput-byte v1, p0, Lcom/google/r/b/a/afj;->r:B

    .line 7977
    iput v1, p0, Lcom/google/r/b/a/afj;->s:I

    .line 7416
    return-void
.end method

.method public static g()Lcom/google/r/b/a/afj;
    .locals 1

    .prologue
    .line 9193
    sget-object v0, Lcom/google/r/b/a/afj;->q:Lcom/google/r/b/a/afj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/afl;
    .locals 1

    .prologue
    .line 8112
    new-instance v0, Lcom/google/r/b/a/afl;

    invoke-direct {v0}, Lcom/google/r/b/a/afl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7591
    sget-object v0, Lcom/google/r/b/a/afj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7928
    invoke-virtual {p0}, Lcom/google/r/b/a/afj;->c()I

    move v1, v2

    .line 7929
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 7930
    iget-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7929
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 7932
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 7933
    iget v0, p0, Lcom/google/r/b/a/afj;->c:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(II)V

    .line 7935
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 7936
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7938
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 7939
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/afj;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 7941
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 7942
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7944
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_5

    .line 7945
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/afj;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 7947
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 7948
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7947
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7950
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 7951
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7953
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 7954
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/r/b/a/afj;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 7956
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 7957
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7959
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 7960
    iget-boolean v0, p0, Lcom/google/r/b/a/afj;->l:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(IZ)V

    .line 7962
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    .line 7963
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/r/b/a/afj;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 7965
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 7966
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/r/b/a/afj;->n:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 7968
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 7969
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7971
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_e

    .line 7972
    const/16 v0, 0x16

    iget-boolean v1, p0, Lcom/google/r/b/a/afj;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 7974
    :cond_e
    iget-object v0, p0, Lcom/google/r/b/a/afj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 7975
    return-void

    .line 7951
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 7912
    iget-byte v0, p0, Lcom/google/r/b/a/afj;->r:B

    .line 7913
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 7923
    :goto_0
    return v0

    .line 7914
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 7916
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 7917
    iget-object v0, p0, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 7918
    iput-byte v2, p0, Lcom/google/r/b/a/afj;->r:B

    move v0, v2

    .line 7919
    goto :goto_0

    :cond_2
    move v0, v2

    .line 7916
    goto :goto_1

    .line 7922
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/afj;->r:B

    move v0, v1

    .line 7923
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 7979
    iget v0, p0, Lcom/google/r/b/a/afj;->s:I

    .line 7980
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 8045
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 7983
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 7984
    iget-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    .line 7985
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 7983
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 7987
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_2

    .line 7988
    iget v0, p0, Lcom/google/r/b/a/afj;->c:I

    .line 7989
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 7991
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_3

    .line 7992
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    .line 7993
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 7995
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 7996
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/afj;->e:I

    .line 7997
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_9

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 7999
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 8000
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    .line 8001
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 8003
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v8, :cond_7

    .line 8004
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/afj;->g:I

    .line 8005
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_6

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_6
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    :cond_7
    move v1, v2

    .line 8007
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 8008
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    .line 8009
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 8007
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_8
    move v0, v4

    .line 7989
    goto/16 :goto_2

    :cond_9
    move v0, v4

    .line 7997
    goto :goto_3

    .line 8011
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_b

    .line 8012
    const/16 v1, 0xd

    .line 8013
    iget-object v0, p0, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 8015
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_c

    .line 8016
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/r/b/a/afj;->j:Z

    .line 8017
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 8019
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_d

    .line 8020
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    .line 8021
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 8023
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_e

    .line 8024
    iget-boolean v0, p0, Lcom/google/r/b/a/afj;->l:Z

    .line 8025
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 8027
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_f

    .line 8028
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/r/b/a/afj;->m:Z

    .line 8029
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 8031
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_10

    .line 8032
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/r/b/a/afj;->n:Z

    .line 8033
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 8035
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_11

    .line 8036
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    .line 8037
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 8039
    :cond_11
    iget v0, p0, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_12

    .line 8040
    const/16 v0, 0x16

    iget-boolean v1, p0, Lcom/google/r/b/a/afj;->p:Z

    .line 8041
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 8043
    :cond_12
    iget-object v0, p0, Lcom/google/r/b/a/afj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 8044
    iput v0, p0, Lcom/google/r/b/a/afj;->s:I

    goto/16 :goto_0

    .line 8013
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/je;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7602
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    .line 7603
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 7604
    iget-object v0, p0, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 7605
    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7607
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7409
    invoke-static {}, Lcom/google/r/b/a/afj;->newBuilder()Lcom/google/r/b/a/afl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/afl;->a(Lcom/google/r/b/a/afj;)Lcom/google/r/b/a/afl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7409
    invoke-static {}, Lcom/google/r/b/a/afj;->newBuilder()Lcom/google/r/b/a/afl;

    move-result-object v0

    return-object v0
.end method
