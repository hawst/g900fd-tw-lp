.class public final Lcom/google/r/b/a/ams;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amz;


# static fields
.field private static volatile A:Lcom/google/n/aw;

.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ams;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final x:Lcom/google/r/b/a/ams;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:J

.field f:I

.field g:I

.field h:I

.field i:I

.field j:Lcom/google/n/ao;

.field k:Ljava/lang/Object;

.field l:Ljava/lang/Object;

.field m:Ljava/lang/Object;

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field o:Ljava/lang/Object;

.field p:Ljava/lang/Object;

.field q:Ljava/lang/Object;

.field r:Lcom/google/n/ao;

.field s:Ljava/lang/Object;

.field t:Lcom/google/n/ao;

.field u:I

.field v:I

.field w:I

.field private y:B

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9084
    new-instance v0, Lcom/google/r/b/a/amt;

    invoke-direct {v0}, Lcom/google/r/b/a/amt;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ams;->PARSER:Lcom/google/n/ax;

    .line 10277
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ams;->A:Lcom/google/n/aw;

    .line 11778
    new-instance v0, Lcom/google/r/b/a/ams;

    invoke-direct {v0}, Lcom/google/r/b/a/ams;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ams;->x:Lcom/google/r/b/a/ams;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 8877
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 9658
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ams;->j:Lcom/google/n/ao;

    .line 9948
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    .line 10006
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ams;->t:Lcom/google/n/ao;

    .line 10067
    iput-byte v5, p0, Lcom/google/r/b/a/ams;->y:B

    .line 10171
    iput v5, p0, Lcom/google/r/b/a/ams;->z:I

    .line 8878
    iput v3, p0, Lcom/google/r/b/a/ams;->b:I

    .line 8879
    iput v3, p0, Lcom/google/r/b/a/ams;->c:I

    .line 8880
    iput v3, p0, Lcom/google/r/b/a/ams;->d:I

    .line 8881
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/ams;->e:J

    .line 8882
    iput v4, p0, Lcom/google/r/b/a/ams;->f:I

    .line 8883
    iput v3, p0, Lcom/google/r/b/a/ams;->g:I

    .line 8884
    iput v5, p0, Lcom/google/r/b/a/ams;->h:I

    .line 8885
    iput v3, p0, Lcom/google/r/b/a/ams;->i:I

    .line 8886
    iget-object v0, p0, Lcom/google/r/b/a/ams;->j:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 8887
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ams;->k:Ljava/lang/Object;

    .line 8888
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ams;->l:Ljava/lang/Object;

    .line 8889
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ams;->m:Ljava/lang/Object;

    .line 8890
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    .line 8891
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ams;->o:Ljava/lang/Object;

    .line 8892
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ams;->p:Ljava/lang/Object;

    .line 8893
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ams;->q:Ljava/lang/Object;

    .line 8894
    iget-object v0, p0, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 8895
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ams;->s:Ljava/lang/Object;

    .line 8896
    iget-object v0, p0, Lcom/google/r/b/a/ams;->t:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 8897
    iput v3, p0, Lcom/google/r/b/a/ams;->u:I

    .line 8898
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ams;->v:I

    .line 8899
    iput v3, p0, Lcom/google/r/b/a/ams;->w:I

    .line 8900
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const v9, 0x7fffffff

    const/4 v2, -0x1

    const/4 v4, 0x1

    const/16 v8, 0x1000

    const/4 v0, 0x0

    .line 8906
    invoke-direct {p0}, Lcom/google/r/b/a/ams;-><init>()V

    .line 8909
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 8912
    :cond_0
    :goto_0
    if-nez v3, :cond_9

    .line 8913
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 8914
    sparse-switch v0, :sswitch_data_0

    .line 8919
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 8921
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 8917
    goto :goto_0

    .line 8926
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 8927
    invoke-static {v0}, Lcom/google/r/b/a/amx;->a(I)Lcom/google/r/b/a/amx;

    move-result-object v6

    .line 8928
    if-nez v6, :cond_2

    .line 8929
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 9072
    :catch_0
    move-exception v0

    .line 9073
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 9078
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x1000

    if-ne v1, v8, :cond_1

    .line 9079
    iget-object v1, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    .line 9081
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ams;->au:Lcom/google/n/bn;

    throw v0

    .line 8931
    :cond_2
    :try_start_2
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v7, 0x80000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8932
    iput v0, p0, Lcom/google/r/b/a/ams;->v:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 9074
    :catch_1
    move-exception v0

    .line 9075
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 9076
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 8937
    :sswitch_2
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8938
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ams;->b:I

    goto :goto_0

    .line 8942
    :sswitch_3
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8943
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ams;->c:I

    goto :goto_0

    .line 8947
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8948
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ams;->d:I

    goto :goto_0

    .line 8952
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 8953
    invoke-static {v0}, Lcom/google/r/b/a/amv;->a(I)Lcom/google/r/b/a/amv;

    move-result-object v6

    .line 8954
    if-nez v6, :cond_3

    .line 8955
    const/4 v6, 0x5

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 8957
    :cond_3
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8958
    iput v0, p0, Lcom/google/r/b/a/ams;->f:I

    goto/16 :goto_0

    .line 8963
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 8964
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit16 v6, v6, 0x200

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8965
    iput-object v0, p0, Lcom/google/r/b/a/ams;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 8969
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v6, 0x100000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8970
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ams;->w:I

    goto/16 :goto_0

    .line 8974
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8975
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ams;->g:I

    goto/16 :goto_0

    .line 8979
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 8980
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit16 v6, v6, 0x400

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8981
    iput-object v0, p0, Lcom/google/r/b/a/ams;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 8985
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 8986
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit16 v6, v6, 0x800

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 8987
    iput-object v0, p0, Lcom/google/r/b/a/ams;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 8991
    :sswitch_b
    iget-object v0, p0, Lcom/google/r/b/a/ams;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 8992
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    goto/16 :goto_0

    .line 8996
    :sswitch_c
    and-int/lit16 v0, v1, 0x1000

    if-eq v0, v8, :cond_4

    .line 8997
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    .line 8998
    or-int/lit16 v1, v1, 0x1000

    .line 9000
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 9004
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 9005
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 9006
    and-int/lit16 v0, v1, 0x1000

    if-eq v0, v8, :cond_5

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_6

    move v0, v2

    :goto_1
    if-lez v0, :cond_5

    .line 9007
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    .line 9008
    or-int/lit16 v1, v1, 0x1000

    .line 9010
    :cond_5
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_7

    move v0, v2

    :goto_3
    if-lez v0, :cond_8

    .line 9011
    iget-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 9006
    :cond_6
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 9010
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 9013
    :cond_8
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 9017
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 9018
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit16 v6, v6, 0x1000

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 9019
    iput-object v0, p0, Lcom/google/r/b/a/ams;->o:Ljava/lang/Object;

    goto/16 :goto_0

    .line 9023
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 9024
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit16 v6, v6, 0x2000

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 9025
    iput-object v0, p0, Lcom/google/r/b/a/ams;->p:Ljava/lang/Object;

    goto/16 :goto_0

    .line 9029
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 9030
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit16 v6, v6, 0x4000

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 9031
    iput-object v0, p0, Lcom/google/r/b/a/ams;->q:Ljava/lang/Object;

    goto/16 :goto_0

    .line 9035
    :sswitch_11
    iget-object v0, p0, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 9036
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const v6, 0x8000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    goto/16 :goto_0

    .line 9040
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 9041
    iget v6, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v7, 0x10000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/r/b/a/ams;->a:I

    .line 9042
    iput-object v0, p0, Lcom/google/r/b/a/ams;->s:Ljava/lang/Object;

    goto/16 :goto_0

    .line 9046
    :sswitch_13
    iget-object v0, p0, Lcom/google/r/b/a/ams;->t:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 9047
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v6, 0x20000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    goto/16 :goto_0

    .line 9051
    :sswitch_14
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 9052
    invoke-virtual {p1}, Lcom/google/n/j;->b()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/ams;->e:J

    goto/16 :goto_0

    .line 9056
    :sswitch_15
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 9057
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ams;->h:I

    goto/16 :goto_0

    .line 9061
    :sswitch_16
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v6, 0x40000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 9062
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ams;->u:I

    goto/16 :goto_0

    .line 9066
    :sswitch_17
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/ams;->a:I

    .line 9067
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ams;->i:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 9078
    :cond_9
    and-int/lit16 v0, v1, 0x1000

    if-ne v0, v8, :cond_a

    .line 9079
    iget-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    .line 9081
    :cond_a
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->au:Lcom/google/n/bn;

    .line 9082
    return-void

    .line 8914
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x62 -> :sswitch_d
        0x6a -> :sswitch_e
        0x72 -> :sswitch_f
        0x7a -> :sswitch_10
        0xda -> :sswitch_11
        0xe2 -> :sswitch_12
        0xea -> :sswitch_13
        0xf0 -> :sswitch_14
        0xf8 -> :sswitch_15
        0x100 -> :sswitch_16
        0x108 -> :sswitch_17
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8875
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 9658
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ams;->j:Lcom/google/n/ao;

    .line 9948
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    .line 10006
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ams;->t:Lcom/google/n/ao;

    .line 10067
    iput-byte v1, p0, Lcom/google/r/b/a/ams;->y:B

    .line 10171
    iput v1, p0, Lcom/google/r/b/a/ams;->z:I

    .line 8876
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ams;
    .locals 1

    .prologue
    .line 11781
    sget-object v0, Lcom/google/r/b/a/ams;->x:Lcom/google/r/b/a/ams;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/amu;
    .locals 1

    .prologue
    .line 10339
    new-instance v0, Lcom/google/r/b/a/amu;

    invoke-direct {v0}, Lcom/google/r/b/a/amu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9096
    sget-object v0, Lcom/google/r/b/a/ams;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 10101
    invoke-virtual {p0}, Lcom/google/r/b/a/ams;->c()I

    .line 10102
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v2, 0x80000

    and-int/2addr v0, v2

    const/high16 v2, 0x80000

    if-ne v0, v2, :cond_0

    .line 10103
    iget v0, p0, Lcom/google/r/b/a/ams;->v:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 10105
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 10106
    iget v0, p0, Lcom/google/r/b/a/ams;->b:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 10108
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 10109
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/r/b/a/ams;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(II)V

    .line 10111
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 10112
    iget v0, p0, Lcom/google/r/b/a/ams;->d:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(II)V

    .line 10114
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 10115
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/r/b/a/ams;->f:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->b(II)V

    .line 10117
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_5

    .line 10118
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/ams;->k:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->k:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10120
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v2, 0x100000

    and-int/2addr v0, v2

    const/high16 v2, 0x100000

    if-ne v0, v2, :cond_6

    .line 10121
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/r/b/a/ams;->w:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 10123
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_7

    .line 10124
    iget v0, p0, Lcom/google/r/b/a/ams;->g:I

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(II)V

    .line 10126
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_8

    .line 10127
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/r/b/a/ams;->l:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->l:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10129
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_9

    .line 10130
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/ams;->m:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->m:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10132
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_a

    .line 10133
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/r/b/a/ams;->j:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10135
    :cond_a
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 10136
    const/16 v2, 0xc

    iget-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 10135
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 10118
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 10127
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 10130
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 10138
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_f

    .line 10139
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/r/b/a/ams;->o:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->o:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10141
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_10

    .line 10142
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/r/b/a/ams;->p:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->p:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10144
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_11

    .line 10145
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/r/b/a/ams;->q:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->q:Ljava/lang/Object;

    :goto_6
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10147
    :cond_11
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_12

    .line 10148
    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10150
    :cond_12
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_13

    .line 10151
    const/16 v1, 0x1c

    iget-object v0, p0, Lcom/google/r/b/a/ams;->s:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->s:Ljava/lang/Object;

    :goto_7
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10153
    :cond_13
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_14

    .line 10154
    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/r/b/a/ams;->t:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 10156
    :cond_14
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_15

    .line 10157
    const/16 v0, 0x1e

    iget-wide v2, p0, Lcom/google/r/b/a/ams;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->a(IJ)V

    .line 10159
    :cond_15
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_16

    .line 10160
    const/16 v0, 0x1f

    iget v1, p0, Lcom/google/r/b/a/ams;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 10162
    :cond_16
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_17

    .line 10163
    const/16 v0, 0x20

    iget v1, p0, Lcom/google/r/b/a/ams;->u:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 10165
    :cond_17
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_18

    .line 10166
    const/16 v0, 0x21

    iget v1, p0, Lcom/google/r/b/a/ams;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 10168
    :cond_18
    iget-object v0, p0, Lcom/google/r/b/a/ams;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 10169
    return-void

    .line 10139
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 10142
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 10145
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 10151
    :cond_1c
    check-cast v0, Lcom/google/n/f;

    goto :goto_7
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/high16 v5, 0x80000

    const v4, 0x8000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 10069
    iget-byte v0, p0, Lcom/google/r/b/a/ams;->y:B

    .line 10070
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 10096
    :goto_0
    return v0

    .line 10071
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 10073
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 10074
    iput-byte v2, p0, Lcom/google/r/b/a/ams;->y:B

    move v0, v2

    .line 10075
    goto :goto_0

    :cond_2
    move v0, v2

    .line 10073
    goto :goto_1

    .line 10077
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    .line 10078
    iput-byte v2, p0, Lcom/google/r/b/a/ams;->y:B

    move v0, v2

    .line 10079
    goto :goto_0

    :cond_4
    move v0, v2

    .line 10077
    goto :goto_2

    .line 10081
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-nez v0, :cond_7

    .line 10082
    iput-byte v2, p0, Lcom/google/r/b/a/ams;->y:B

    move v0, v2

    .line 10083
    goto :goto_0

    :cond_6
    move v0, v2

    .line 10081
    goto :goto_3

    .line 10085
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_8

    move v0, v1

    :goto_4
    if-nez v0, :cond_9

    .line 10086
    iput-byte v2, p0, Lcom/google/r/b/a/ams;->y:B

    move v0, v2

    .line 10087
    goto :goto_0

    :cond_8
    move v0, v2

    .line 10085
    goto :goto_4

    .line 10089
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_b

    .line 10090
    iget-object v0, p0, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/e;->d()Lcom/google/maps/f/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/e;

    invoke-virtual {v0}, Lcom/google/maps/f/e;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 10091
    iput-byte v2, p0, Lcom/google/r/b/a/ams;->y:B

    move v0, v2

    .line 10092
    goto :goto_0

    :cond_a
    move v0, v2

    .line 10089
    goto :goto_5

    .line 10095
    :cond_b
    iput-byte v1, p0, Lcom/google/r/b/a/ams;->y:B

    move v0, v1

    .line 10096
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 10173
    iget v0, p0, Lcom/google/r/b/a/ams;->z:I

    .line 10174
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 10272
    :goto_0
    return v0

    .line 10177
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v0, v3

    const/high16 v3, 0x80000

    if-ne v0, v3, :cond_27

    .line 10178
    iget v0, p0, Lcom/google/r/b/a/ams;->v:I

    .line 10179
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 10181
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v4, :cond_1

    .line 10182
    iget v3, p0, Lcom/google/r/b/a/ams;->b:I

    .line 10183
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_b

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 10185
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 10186
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/ams;->c:I

    .line 10187
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 10189
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_3

    .line 10190
    iget v3, p0, Lcom/google/r/b/a/ams;->d:I

    .line 10191
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_d

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 10193
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_26

    .line 10194
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/r/b/a/ams;->f:I

    .line 10195
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    move v3, v0

    .line 10197
    :goto_7
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_4

    .line 10198
    const/4 v4, 0x6

    .line 10199
    iget-object v0, p0, Lcom/google/r/b/a/ams;->k:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->k:Ljava/lang/Object;

    :goto_8
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10201
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v4, 0x100000

    and-int/2addr v0, v4

    const/high16 v4, 0x100000

    if-ne v0, v4, :cond_5

    .line 10202
    const/4 v0, 0x7

    iget v4, p0, Lcom/google/r/b/a/ams;->w:I

    .line 10203
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10205
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_6

    .line 10206
    const/16 v0, 0x8

    iget v4, p0, Lcom/google/r/b/a/ams;->g:I

    .line 10207
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 10209
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_7

    .line 10210
    const/16 v4, 0x9

    .line 10211
    iget-object v0, p0, Lcom/google/r/b/a/ams;->l:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->l:Ljava/lang/Object;

    :goto_a
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10213
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_8

    .line 10215
    iget-object v0, p0, Lcom/google/r/b/a/ams;->m:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->m:Ljava/lang/Object;

    :goto_b
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10217
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_9

    .line 10218
    const/16 v0, 0xb

    iget-object v4, p0, Lcom/google/r/b/a/ams;->j:Lcom/google/n/ao;

    .line 10219
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    :cond_9
    move v4, v2

    move v5, v2

    .line 10223
    :goto_c
    iget-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_14

    .line 10224
    iget-object v0, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    .line 10225
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_13

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_d
    add-int/2addr v5, v0

    .line 10223
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_c

    :cond_a
    move v0, v1

    .line 10179
    goto/16 :goto_1

    :cond_b
    move v3, v1

    .line 10183
    goto/16 :goto_3

    :cond_c
    move v3, v1

    .line 10187
    goto/16 :goto_4

    :cond_d
    move v3, v1

    .line 10191
    goto/16 :goto_5

    :cond_e
    move v3, v1

    .line 10195
    goto/16 :goto_6

    .line 10199
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    :cond_10
    move v0, v1

    .line 10207
    goto/16 :goto_9

    .line 10211
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    .line 10215
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto :goto_b

    :cond_13
    move v0, v1

    .line 10225
    goto :goto_d

    .line 10227
    :cond_14
    add-int v0, v3, v5

    .line 10228
    iget-object v3, p0, Lcom/google/r/b/a/ams;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v0

    .line 10230
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_15

    .line 10231
    const/16 v4, 0xd

    .line 10232
    iget-object v0, p0, Lcom/google/r/b/a/ams;->o:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_20

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->o:Ljava/lang/Object;

    :goto_e
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    move v3, v0

    .line 10234
    :cond_15
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v0, v4, :cond_16

    .line 10235
    const/16 v4, 0xe

    .line 10236
    iget-object v0, p0, Lcom/google/r/b/a/ams;->p:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_21

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->p:Ljava/lang/Object;

    :goto_f
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10238
    :cond_16
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v0, v4, :cond_17

    .line 10239
    const/16 v4, 0xf

    .line 10240
    iget-object v0, p0, Lcom/google/r/b/a/ams;->q:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_22

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->q:Ljava/lang/Object;

    :goto_10
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10242
    :cond_17
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const v4, 0x8000

    and-int/2addr v0, v4

    const v4, 0x8000

    if-ne v0, v4, :cond_18

    .line 10243
    const/16 v0, 0x1b

    iget-object v4, p0, Lcom/google/r/b/a/ams;->r:Lcom/google/n/ao;

    .line 10244
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10246
    :cond_18
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v4, 0x10000

    and-int/2addr v0, v4

    const/high16 v4, 0x10000

    if-ne v0, v4, :cond_19

    .line 10247
    const/16 v4, 0x1c

    .line 10248
    iget-object v0, p0, Lcom/google/r/b/a/ams;->s:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_23

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ams;->s:Ljava/lang/Object;

    :goto_11
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10250
    :cond_19
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v4, 0x20000

    and-int/2addr v0, v4

    const/high16 v4, 0x20000

    if-ne v0, v4, :cond_1a

    .line 10251
    const/16 v0, 0x1d

    iget-object v4, p0, Lcom/google/r/b/a/ams;->t:Lcom/google/n/ao;

    .line 10252
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10254
    :cond_1a
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_1b

    .line 10255
    const/16 v0, 0x1e

    iget-wide v4, p0, Lcom/google/r/b/a/ams;->e:J

    .line 10256
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 10258
    :cond_1b
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_1c

    .line 10259
    const/16 v0, 0x1f

    iget v4, p0, Lcom/google/r/b/a/ams;->h:I

    .line 10260
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_24

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_12
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 10262
    :cond_1c
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    const/high16 v4, 0x40000

    and-int/2addr v0, v4

    const/high16 v4, 0x40000

    if-ne v0, v4, :cond_1d

    .line 10263
    const/16 v0, 0x20

    iget v4, p0, Lcom/google/r/b/a/ams;->u:I

    .line 10264
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_25

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_13
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 10266
    :cond_1d
    iget v0, p0, Lcom/google/r/b/a/ams;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_1f

    .line 10267
    const/16 v0, 0x21

    iget v4, p0, Lcom/google/r/b/a/ams;->i:I

    .line 10268
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_1e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1e
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 10270
    :cond_1f
    iget-object v0, p0, Lcom/google/r/b/a/ams;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 10271
    iput v0, p0, Lcom/google/r/b/a/ams;->z:I

    goto/16 :goto_0

    .line 10232
    :cond_20
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_e

    .line 10236
    :cond_21
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_f

    .line 10240
    :cond_22
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_10

    .line 10248
    :cond_23
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_11

    :cond_24
    move v0, v1

    .line 10260
    goto :goto_12

    :cond_25
    move v0, v1

    .line 10264
    goto :goto_13

    :cond_26
    move v3, v0

    goto/16 :goto_7

    :cond_27
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8869
    invoke-static {}, Lcom/google/r/b/a/ams;->newBuilder()Lcom/google/r/b/a/amu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/amu;->a(Lcom/google/r/b/a/ams;)Lcom/google/r/b/a/amu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8869
    invoke-static {}, Lcom/google/r/b/a/ams;->newBuilder()Lcom/google/r/b/a/amu;

    move-result-object v0

    return-object v0
.end method
