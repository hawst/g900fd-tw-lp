.class public final Lcom/google/r/b/a/rr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/rw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/rr;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/rr;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/ao;

.field h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/google/r/b/a/rs;

    invoke-direct {v0}, Lcom/google/r/b/a/rs;-><init>()V

    sput-object v0, Lcom/google/r/b/a/rr;->PARSER:Lcom/google/n/ax;

    .line 575
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/rr;->m:Lcom/google/n/aw;

    .line 1328
    new-instance v0, Lcom/google/r/b/a/rr;

    invoke-direct {v0}, Lcom/google/r/b/a/rr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/rr;->j:Lcom/google/r/b/a/rr;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 391
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rr;->g:Lcom/google/n/ao;

    .line 490
    iput-byte v2, p0, Lcom/google/r/b/a/rr;->k:B

    .line 530
    iput v2, p0, Lcom/google/r/b/a/rr;->l:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/rr;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rr;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rr;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rr;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rr;->f:Ljava/lang/Object;

    .line 23
    iget-object v0, p0, Lcom/google/r/b/a/rr;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rr;->h:Ljava/lang/Object;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rr;->i:Ljava/lang/Object;

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Lcom/google/r/b/a/rr;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 40
    sparse-switch v3, :sswitch_data_0

    .line 45
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 53
    invoke-static {v3}, Lcom/google/r/b/a/ru;->a(I)Lcom/google/r/b/a/ru;

    move-result-object v4

    .line 54
    if-nez v4, :cond_1

    .line 55
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/rr;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/rr;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/rr;->a:I

    .line 58
    iput v3, p0, Lcom/google/r/b/a/rr;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 107
    :catch_1
    move-exception v0

    .line 108
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 109
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 64
    iget v4, p0, Lcom/google/r/b/a/rr;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/rr;->a:I

    .line 65
    iput-object v3, p0, Lcom/google/r/b/a/rr;->c:Ljava/lang/Object;

    goto :goto_0

    .line 69
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 70
    iget v4, p0, Lcom/google/r/b/a/rr;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/rr;->a:I

    .line 71
    iput-object v3, p0, Lcom/google/r/b/a/rr;->d:Ljava/lang/Object;

    goto :goto_0

    .line 75
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 76
    iget v4, p0, Lcom/google/r/b/a/rr;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/rr;->a:I

    .line 77
    iput-object v3, p0, Lcom/google/r/b/a/rr;->e:Ljava/lang/Object;

    goto :goto_0

    .line 81
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 82
    iget v4, p0, Lcom/google/r/b/a/rr;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/rr;->a:I

    .line 83
    iput-object v3, p0, Lcom/google/r/b/a/rr;->f:Ljava/lang/Object;

    goto :goto_0

    .line 87
    :sswitch_6
    iget-object v3, p0, Lcom/google/r/b/a/rr;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 88
    iget v3, p0, Lcom/google/r/b/a/rr;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/rr;->a:I

    goto/16 :goto_0

    .line 92
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 93
    iget v4, p0, Lcom/google/r/b/a/rr;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/rr;->a:I

    .line 94
    iput-object v3, p0, Lcom/google/r/b/a/rr;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 98
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 99
    iget v4, p0, Lcom/google/r/b/a/rr;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/r/b/a/rr;->a:I

    .line 100
    iput-object v3, p0, Lcom/google/r/b/a/rr;->i:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 111
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->au:Lcom/google/n/bn;

    .line 112
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 391
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rr;->g:Lcom/google/n/ao;

    .line 490
    iput-byte v1, p0, Lcom/google/r/b/a/rr;->k:B

    .line 530
    iput v1, p0, Lcom/google/r/b/a/rr;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/rr;
    .locals 1

    .prologue
    .line 1331
    sget-object v0, Lcom/google/r/b/a/rr;->j:Lcom/google/r/b/a/rr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/rt;
    .locals 1

    .prologue
    .line 637
    new-instance v0, Lcom/google/r/b/a/rt;

    invoke-direct {v0}, Lcom/google/r/b/a/rt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/rr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/google/r/b/a/rr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 502
    invoke-virtual {p0}, Lcom/google/r/b/a/rr;->c()I

    .line 503
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 504
    iget v0, p0, Lcom/google/r/b/a/rr;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 506
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 507
    iget-object v0, p0, Lcom/google/r/b/a/rr;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 509
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 510
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/rr;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 512
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 513
    iget-object v0, p0, Lcom/google/r/b/a/rr;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->e:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 515
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 516
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/rr;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->f:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 518
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 519
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/rr;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 521
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 522
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/rr;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->h:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 524
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 525
    iget-object v0, p0, Lcom/google/r/b/a/rr;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->i:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 527
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/rr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 528
    return-void

    .line 507
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 510
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 513
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 516
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 522
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 525
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 492
    iget-byte v1, p0, Lcom/google/r/b/a/rr;->k:B

    .line 493
    if-ne v1, v0, :cond_0

    .line 497
    :goto_0
    return v0

    .line 494
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 496
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/rr;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 532
    iget v0, p0, Lcom/google/r/b/a/rr;->l:I

    .line 533
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 570
    :goto_0
    return v0

    .line 536
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 537
    iget v0, p0, Lcom/google/r/b/a/rr;->b:I

    .line 538
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 540
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 542
    iget-object v0, p0, Lcom/google/r/b/a/rr;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 544
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 545
    const/4 v3, 0x3

    .line 546
    iget-object v0, p0, Lcom/google/r/b/a/rr;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 548
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 550
    iget-object v0, p0, Lcom/google/r/b/a/rr;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 552
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 553
    const/4 v3, 0x5

    .line 554
    iget-object v0, p0, Lcom/google/r/b/a/rr;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 556
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 557
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/r/b/a/rr;->g:Lcom/google/n/ao;

    .line 558
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 560
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 561
    const/4 v3, 0x7

    .line 562
    iget-object v0, p0, Lcom/google/r/b/a/rr;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->h:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 564
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/rr;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 566
    iget-object v0, p0, Lcom/google/r/b/a/rr;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/rr;->i:Ljava/lang/Object;

    :goto_8
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 568
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/rr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 569
    iput v0, p0, Lcom/google/r/b/a/rr;->l:I

    goto/16 :goto_0

    .line 538
    :cond_8
    const/16 v0, 0xa

    goto/16 :goto_1

    .line 542
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 546
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 550
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 554
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 562
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 566
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_f
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/rr;->newBuilder()Lcom/google/r/b/a/rt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/rt;->a(Lcom/google/r/b/a/rr;)Lcom/google/r/b/a/rt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/rr;->newBuilder()Lcom/google/r/b/a/rt;

    move-result-object v0

    return-object v0
.end method
