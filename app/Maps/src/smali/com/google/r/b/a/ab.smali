.class public final Lcom/google/r/b/a/ab;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ac;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/z;",
        "Lcom/google/r/b/a/ab;",
        ">;",
        "Lcom/google/r/b/a/ac;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2351
    sget-object v0, Lcom/google/r/b/a/z;->c:Lcom/google/r/b/a/z;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2415
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    .line 2552
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    .line 2352
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 2343
    new-instance v0, Lcom/google/r/b/a/z;

    invoke-direct {v0, p0}, Lcom/google/r/b/a/z;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/r/b/a/ab;->a:I

    iget v1, p0, Lcom/google/r/b/a/ab;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ab;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/ab;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ab;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ab;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/ab;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    iput-object v1, v0, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2343
    check-cast p1, Lcom/google/r/b/a/z;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ab;->a(Lcom/google/r/b/a/z;)Lcom/google/r/b/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/z;)Lcom/google/r/b/a/ab;
    .locals 2

    .prologue
    .line 2383
    invoke-static {}, Lcom/google/r/b/a/z;->g()Lcom/google/r/b/a/z;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2405
    :goto_0
    return-object p0

    .line 2384
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2385
    iget-object v0, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2386
    iget-object v0, p1, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    .line 2387
    iget v0, p0, Lcom/google/r/b/a/ab;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/r/b/a/ab;->a:I

    .line 2394
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2395
    iget-object v0, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2396
    iget-object v0, p1, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    .line 2397
    iget v0, p0, Lcom/google/r/b/a/ab;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/ab;->a:I

    .line 2404
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/z;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 2389
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ab;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/ab;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ab;->a:I

    .line 2390
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/ab;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/z;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2399
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/ab;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ab;->a:I

    .line 2400
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/ab;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/z;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2409
    const/4 v0, 0x1

    return v0
.end method
