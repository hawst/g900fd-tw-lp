.class public final Lcom/google/r/b/a/sk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/sn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/sk;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/sk;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 151
    new-instance v0, Lcom/google/r/b/a/sl;

    invoke-direct {v0}, Lcom/google/r/b/a/sl;-><init>()V

    sput-object v0, Lcom/google/r/b/a/sk;->PARSER:Lcom/google/n/ax;

    .line 245
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/sk;->f:Lcom/google/n/aw;

    .line 447
    new-instance v0, Lcom/google/r/b/a/sk;

    invoke-direct {v0}, Lcom/google/r/b/a/sk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/sk;->c:Lcom/google/r/b/a/sk;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 107
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 209
    iput-byte v0, p0, Lcom/google/r/b/a/sk;->d:B

    .line 228
    iput v0, p0, Lcom/google/r/b/a/sk;->e:I

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/sk;->b:Ljava/lang/Object;

    .line 109
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 115
    invoke-direct {p0}, Lcom/google/r/b/a/sk;-><init>()V

    .line 116
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 120
    const/4 v0, 0x0

    .line 121
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 122
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 123
    sparse-switch v3, :sswitch_data_0

    .line 128
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 130
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 126
    goto :goto_0

    .line 135
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 136
    iget v4, p0, Lcom/google/r/b/a/sk;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/sk;->a:I

    .line 137
    iput-object v3, p0, Lcom/google/r/b/a/sk;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/sk;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/sk;->au:Lcom/google/n/bn;

    .line 149
    return-void

    .line 144
    :catch_1
    move-exception v0

    .line 145
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 146
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 105
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 209
    iput-byte v0, p0, Lcom/google/r/b/a/sk;->d:B

    .line 228
    iput v0, p0, Lcom/google/r/b/a/sk;->e:I

    .line 106
    return-void
.end method

.method public static d()Lcom/google/r/b/a/sk;
    .locals 1

    .prologue
    .line 450
    sget-object v0, Lcom/google/r/b/a/sk;->c:Lcom/google/r/b/a/sk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/sm;
    .locals 1

    .prologue
    .line 307
    new-instance v0, Lcom/google/r/b/a/sm;

    invoke-direct {v0}, Lcom/google/r/b/a/sm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/sk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    sget-object v0, Lcom/google/r/b/a/sk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 221
    invoke-virtual {p0}, Lcom/google/r/b/a/sk;->c()I

    .line 222
    iget v0, p0, Lcom/google/r/b/a/sk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/r/b/a/sk;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/sk;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/sk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 226
    return-void

    .line 223
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 211
    iget-byte v1, p0, Lcom/google/r/b/a/sk;->d:B

    .line 212
    if-ne v1, v0, :cond_0

    .line 216
    :goto_0
    return v0

    .line 213
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 215
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/sk;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 230
    iget v0, p0, Lcom/google/r/b/a/sk;->e:I

    .line 231
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 240
    :goto_0
    return v0

    .line 234
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/sk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 236
    iget-object v0, p0, Lcom/google/r/b/a/sk;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/sk;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 238
    :goto_2
    iget-object v1, p0, Lcom/google/r/b/a/sk;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    iput v0, p0, Lcom/google/r/b/a/sk;->e:I

    goto :goto_0

    .line 236
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/r/b/a/sk;->newBuilder()Lcom/google/r/b/a/sm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/sm;->a(Lcom/google/r/b/a/sk;)Lcom/google/r/b/a/sm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/google/r/b/a/sk;->newBuilder()Lcom/google/r/b/a/sm;

    move-result-object v0

    return-object v0
.end method
