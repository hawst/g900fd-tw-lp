.class public final Lcom/google/r/b/a/apq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/apv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/apq;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/r/b/a/apq;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:Z

.field public d:I

.field public e:Z

.field public f:I

.field public g:I

.field h:Z

.field public i:Ljava/lang/Object;

.field j:Z

.field public k:I

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/google/r/b/a/apr;

    invoke-direct {v0}, Lcom/google/r/b/a/apr;-><init>()V

    sput-object v0, Lcom/google/r/b/a/apq;->PARSER:Lcom/google/n/ax;

    .line 508
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/apq;->o:Lcom/google/n/aw;

    .line 1083
    new-instance v0, Lcom/google/r/b/a/apq;

    invoke-direct {v0}, Lcom/google/r/b/a/apq;-><init>()V

    sput-object v0, Lcom/google/r/b/a/apq;->l:Lcom/google/r/b/a/apq;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 409
    iput-byte v0, p0, Lcom/google/r/b/a/apq;->m:B

    .line 455
    iput v0, p0, Lcom/google/r/b/a/apq;->n:I

    .line 18
    const/16 v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/apq;->b:I

    .line 19
    iput-boolean v1, p0, Lcom/google/r/b/a/apq;->c:Z

    .line 20
    const/16 v0, 0x19

    iput v0, p0, Lcom/google/r/b/a/apq;->d:I

    .line 21
    iput-boolean v1, p0, Lcom/google/r/b/a/apq;->e:Z

    .line 22
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/r/b/a/apq;->f:I

    .line 23
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/r/b/a/apq;->g:I

    .line 24
    iput-boolean v1, p0, Lcom/google/r/b/a/apq;->h:Z

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    .line 26
    iput-boolean v1, p0, Lcom/google/r/b/a/apq;->j:Z

    .line 27
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/apq;->k:I

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 34
    invoke-direct {p0}, Lcom/google/r/b/a/apq;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 40
    :cond_0
    :goto_0
    if-nez v3, :cond_6

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 47
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 49
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/apq;->a:I

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/apq;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/apq;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/apq;->a:I

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/apq;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 114
    :catch_1
    move-exception v0

    .line 115
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 116
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 60
    goto :goto_1

    .line 64
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/apq;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/apq;->d:I

    goto :goto_0

    .line 69
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/apq;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/apq;->e:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 74
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/apq;->a:I

    .line 75
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/apq;->f:I

    goto :goto_0

    .line 79
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/apq;->a:I

    .line 80
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/apq;->g:I

    goto/16 :goto_0

    .line 84
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/apq;->a:I

    .line 85
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/r/b/a/apq;->h:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 89
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 90
    iget v5, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/r/b/a/apq;->a:I

    .line 91
    iput-object v0, p0, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 95
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/apq;->a:I

    .line 96
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/r/b/a/apq;->j:Z

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_4

    .line 100
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 101
    invoke-static {v0}, Lcom/google/r/b/a/apt;->a(I)Lcom/google/r/b/a/apt;

    move-result-object v5

    .line 102
    if-nez v5, :cond_5

    .line 103
    const/16 v5, 0xa

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 105
    :cond_5
    iget v5, p0, Lcom/google/r/b/a/apq;->a:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/r/b/a/apq;->a:I

    .line 106
    iput v0, p0, Lcom/google/r/b/a/apq;->k:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 118
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apq;->au:Lcom/google/n/bn;

    .line 119
    return-void

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 409
    iput-byte v0, p0, Lcom/google/r/b/a/apq;->m:B

    .line 455
    iput v0, p0, Lcom/google/r/b/a/apq;->n:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/apq;
    .locals 1

    .prologue
    .line 1086
    sget-object v0, Lcom/google/r/b/a/apq;->l:Lcom/google/r/b/a/apq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aps;
    .locals 1

    .prologue
    .line 570
    new-instance v0, Lcom/google/r/b/a/aps;

    invoke-direct {v0}, Lcom/google/r/b/a/aps;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/apq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    sget-object v0, Lcom/google/r/b/a/apq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 421
    invoke-virtual {p0}, Lcom/google/r/b/a/apq;->c()I

    .line 422
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 423
    iget v0, p0, Lcom/google/r/b/a/apq;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 425
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 426
    iget-boolean v0, p0, Lcom/google/r/b/a/apq;->c:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 428
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 429
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/apq;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 431
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 432
    iget-boolean v0, p0, Lcom/google/r/b/a/apq;->e:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 434
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 435
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/apq;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 437
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 438
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/apq;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 440
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 441
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/r/b/a/apq;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 443
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 444
    iget-object v0, p0, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 446
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 447
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/apq;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 449
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 450
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/r/b/a/apq;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 452
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/apq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 453
    return-void

    .line 444
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 411
    iget-byte v1, p0, Lcom/google/r/b/a/apq;->m:B

    .line 412
    if-ne v1, v0, :cond_0

    .line 416
    :goto_0
    return v0

    .line 413
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 415
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/apq;->m:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 457
    iget v0, p0, Lcom/google/r/b/a/apq;->n:I

    .line 458
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 503
    :goto_0
    return v0

    .line 461
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_10

    .line 462
    iget v0, p0, Lcom/google/r/b/a/apq;->b:I

    .line 463
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 465
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 466
    iget-boolean v3, p0, Lcom/google/r/b/a/apq;->c:Z

    .line 467
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 469
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 470
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/apq;->d:I

    .line 471
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 473
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 474
    iget-boolean v3, p0, Lcom/google/r/b/a/apq;->e:Z

    .line 475
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 477
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 478
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/r/b/a/apq;->f:I

    .line 479
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 481
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 482
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/r/b/a/apq;->g:I

    .line 483
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 485
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_f

    .line 486
    const/4 v3, 0x7

    iget-boolean v4, p0, Lcom/google/r/b/a/apq;->h:Z

    .line 487
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    move v3, v0

    .line 489
    :goto_6
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_6

    .line 490
    const/16 v4, 0x8

    .line 491
    iget-object v0, p0, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    :goto_7
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 493
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_7

    .line 494
    const/16 v0, 0x9

    iget-boolean v4, p0, Lcom/google/r/b/a/apq;->j:Z

    .line 495
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 497
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_9

    .line 498
    iget v0, p0, Lcom/google/r/b/a/apq;->k:I

    .line 499
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_8
    add-int v0, v2, v1

    add-int/2addr v3, v0

    .line 501
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/apq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 502
    iput v0, p0, Lcom/google/r/b/a/apq;->n:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 463
    goto/16 :goto_1

    :cond_b
    move v3, v1

    .line 471
    goto/16 :goto_3

    :cond_c
    move v3, v1

    .line 479
    goto/16 :goto_4

    :cond_d
    move v3, v1

    .line 483
    goto/16 :goto_5

    .line 491
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    :cond_f
    move v3, v0

    goto :goto_6

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/apq;->newBuilder()Lcom/google/r/b/a/aps;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aps;->a(Lcom/google/r/b/a/apq;)Lcom/google/r/b/a/aps;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/apq;->newBuilder()Lcom/google/r/b/a/aps;

    move-result-object v0

    return-object v0
.end method
