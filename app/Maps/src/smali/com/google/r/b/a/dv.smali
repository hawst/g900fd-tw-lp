.class public final Lcom/google/r/b/a/dv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/dy;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/dv;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/r/b/a/dv;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field public i:Ljava/lang/Object;

.field public j:Ljava/lang/Object;

.field k:Ljava/lang/Object;

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/google/r/b/a/dw;

    invoke-direct {v0}, Lcom/google/r/b/a/dw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/dv;->PARSER:Lcom/google/n/ax;

    .line 659
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/dv;->o:Lcom/google/n/aw;

    .line 1644
    new-instance v0, Lcom/google/r/b/a/dv;

    invoke-direct {v0}, Lcom/google/r/b/a/dv;-><init>()V

    sput-object v0, Lcom/google/r/b/a/dv;->l:Lcom/google/r/b/a/dv;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 560
    iput-byte v0, p0, Lcom/google/r/b/a/dv;->m:B

    .line 606
    iput v0, p0, Lcom/google/r/b/a/dv;->n:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dv;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dv;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dv;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dv;->e:Ljava/lang/Object;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dv;->f:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dv;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dv;->h:Ljava/lang/Object;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/dv;->i:Ljava/lang/Object;

    .line 26
    const-string v0, "https://support.google.com/gmm/?p=place_questions"

    iput-object v0, p0, Lcom/google/r/b/a/dv;->j:Ljava/lang/Object;

    .line 27
    const-string v0, "https://support.google.com/maps/?p=ios_send_to_phone"

    iput-object v0, p0, Lcom/google/r/b/a/dv;->k:Ljava/lang/Object;

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 34
    invoke-direct {p0}, Lcom/google/r/b/a/dv;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 39
    const/4 v0, 0x0

    .line 40
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 42
    sparse-switch v3, :sswitch_data_0

    .line 47
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 49
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 55
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 56
    iput-object v3, p0, Lcom/google/r/b/a/dv;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/dv;->au:Lcom/google/n/bn;

    throw v0

    .line 60
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 61
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 62
    iput-object v3, p0, Lcom/google/r/b/a/dv;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 117
    :catch_1
    move-exception v0

    .line 118
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 119
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 66
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 67
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 68
    iput-object v3, p0, Lcom/google/r/b/a/dv;->d:Ljava/lang/Object;

    goto :goto_0

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 73
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 74
    iput-object v3, p0, Lcom/google/r/b/a/dv;->e:Ljava/lang/Object;

    goto :goto_0

    .line 78
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 79
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 80
    iput-object v3, p0, Lcom/google/r/b/a/dv;->f:Ljava/lang/Object;

    goto :goto_0

    .line 84
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 85
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 86
    iput-object v3, p0, Lcom/google/r/b/a/dv;->g:Ljava/lang/Object;

    goto :goto_0

    .line 90
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 91
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 92
    iput-object v3, p0, Lcom/google/r/b/a/dv;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 96
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 97
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 98
    iput-object v3, p0, Lcom/google/r/b/a/dv;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 102
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 103
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 104
    iput-object v3, p0, Lcom/google/r/b/a/dv;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 108
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 109
    iget v4, p0, Lcom/google/r/b/a/dv;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/r/b/a/dv;->a:I

    .line 110
    iput-object v3, p0, Lcom/google/r/b/a/dv;->k:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 121
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->au:Lcom/google/n/bn;

    .line 122
    return-void

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 560
    iput-byte v0, p0, Lcom/google/r/b/a/dv;->m:B

    .line 606
    iput v0, p0, Lcom/google/r/b/a/dv;->n:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/dv;
    .locals 1

    .prologue
    .line 1647
    sget-object v0, Lcom/google/r/b/a/dv;->l:Lcom/google/r/b/a/dv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/dx;
    .locals 1

    .prologue
    .line 721
    new-instance v0, Lcom/google/r/b/a/dx;

    invoke-direct {v0}, Lcom/google/r/b/a/dx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/dv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/google/r/b/a/dv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 572
    invoke-virtual {p0}, Lcom/google/r/b/a/dv;->c()I

    .line 573
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 574
    iget-object v0, p0, Lcom/google/r/b/a/dv;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 576
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 577
    iget-object v0, p0, Lcom/google/r/b/a/dv;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 579
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 580
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/dv;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 582
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 583
    iget-object v0, p0, Lcom/google/r/b/a/dv;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->e:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 585
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 586
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/dv;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->f:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 588
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 589
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/dv;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->g:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 591
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 592
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/dv;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->h:Ljava/lang/Object;

    :goto_6
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 594
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 595
    iget-object v0, p0, Lcom/google/r/b/a/dv;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->i:Ljava/lang/Object;

    :goto_7
    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 597
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 598
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/r/b/a/dv;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->j:Ljava/lang/Object;

    :goto_8
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 600
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 601
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/dv;->k:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->k:Ljava/lang/Object;

    :goto_9
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 603
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/dv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 604
    return-void

    .line 574
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 577
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 580
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 583
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 586
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 589
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 592
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 595
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 598
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 601
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_9
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 562
    iget-byte v1, p0, Lcom/google/r/b/a/dv;->m:B

    .line 563
    if-ne v1, v0, :cond_0

    .line 567
    :goto_0
    return v0

    .line 564
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 566
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/dv;->m:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 608
    iget v0, p0, Lcom/google/r/b/a/dv;->n:I

    .line 609
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 654
    :goto_0
    return v0

    .line 612
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_14

    .line 614
    iget-object v0, p0, Lcom/google/r/b/a/dv;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 616
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 618
    iget-object v0, p0, Lcom/google/r/b/a/dv;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 620
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 621
    const/4 v3, 0x3

    .line 622
    iget-object v0, p0, Lcom/google/r/b/a/dv;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 624
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 626
    iget-object v0, p0, Lcom/google/r/b/a/dv;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 628
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 629
    const/4 v3, 0x5

    .line 630
    iget-object v0, p0, Lcom/google/r/b/a/dv;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 632
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 633
    const/4 v3, 0x6

    .line 634
    iget-object v0, p0, Lcom/google/r/b/a/dv;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->g:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 636
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 637
    const/4 v3, 0x7

    .line 638
    iget-object v0, p0, Lcom/google/r/b/a/dv;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->h:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 640
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 642
    iget-object v0, p0, Lcom/google/r/b/a/dv;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->i:Ljava/lang/Object;

    :goto_9
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 644
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    .line 645
    const/16 v3, 0x9

    .line 646
    iget-object v0, p0, Lcom/google/r/b/a/dv;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->j:Ljava/lang/Object;

    :goto_a
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 648
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/dv;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_9

    .line 649
    const/16 v3, 0xa

    .line 650
    iget-object v0, p0, Lcom/google/r/b/a/dv;->k:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/dv;->k:Ljava/lang/Object;

    :goto_b
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 652
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/dv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 653
    iput v0, p0, Lcom/google/r/b/a/dv;->n:I

    goto/16 :goto_0

    .line 614
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 618
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 622
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 626
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 630
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 634
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 638
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    .line 642
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 646
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto :goto_a

    .line 650
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_b

    :cond_14
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/dv;->newBuilder()Lcom/google/r/b/a/dx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/dx;->a(Lcom/google/r/b/a/dv;)Lcom/google/r/b/a/dx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/dv;->newBuilder()Lcom/google/r/b/a/dx;

    move-result-object v0

    return-object v0
.end method
