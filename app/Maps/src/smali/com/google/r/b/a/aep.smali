.class public final Lcom/google/r/b/a/aep;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aeq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ael;",
        "Lcom/google/r/b/a/aep;",
        ">;",
        "Lcom/google/r/b/a/aeq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/n/ao;

.field private h:Z

.field private i:Z

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;

.field private m:Lcom/google/n/ao;

.field private n:Lcom/google/n/ao;

.field private o:Lcom/google/n/ao;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/google/n/ao;

.field private r:Lcom/google/n/ao;

.field private s:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1050
    sget-object v0, Lcom/google/r/b/a/ael;->t:Lcom/google/r/b/a/ael;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1305
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aep;->b:Ljava/lang/Object;

    .line 1381
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aep;->c:Ljava/lang/Object;

    .line 1457
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->d:Lcom/google/n/ao;

    .line 1516
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->e:Lcom/google/n/ao;

    .line 1576
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    .line 1649
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->g:Lcom/google/n/ao;

    .line 1708
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/aep;->h:Z

    .line 1772
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->j:Lcom/google/n/ao;

    .line 1831
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->k:Lcom/google/n/ao;

    .line 1890
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->l:Lcom/google/n/ao;

    .line 1949
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->m:Lcom/google/n/ao;

    .line 2008
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->n:Lcom/google/n/ao;

    .line 2067
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->o:Lcom/google/n/ao;

    .line 2127
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    .line 2200
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->q:Lcom/google/n/ao;

    .line 2259
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->r:Lcom/google/n/ao;

    .line 2318
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aep;->s:Lcom/google/n/ao;

    .line 1051
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 10

    .prologue
    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const/4 v0, 0x1

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1042
    new-instance v2, Lcom/google/r/b/a/ael;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ael;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_11

    :goto_0
    iget-object v4, p0, Lcom/google/r/b/a/aep;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/aep;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/ael;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/ael;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/r/b/a/aep;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/ael;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-boolean v4, p0, Lcom/google/r/b/a/aep;->h:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/ael;->h:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-boolean v4, p0, Lcom/google/r/b/a/aep;->i:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/ael;->i:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v4, v2, Lcom/google/r/b/a/ael;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v4, v2, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v4, v2, Lcom/google/r/b/a/ael;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget-object v4, v2, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x800

    :cond_b
    iget-object v4, v2, Lcom/google/r/b/a/ael;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x1000

    :cond_c
    iget-object v4, v2, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->o:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->o:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit16 v4, v4, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    iget-object v4, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit16 v4, v4, -0x4001

    iput v4, p0, Lcom/google/r/b/a/aep;->a:I

    :cond_d
    iget-object v4, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    or-int/lit16 v0, v0, 0x2000

    :cond_e
    iget-object v4, v2, Lcom/google/r/b/a/ael;->q:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->q:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->q:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int v4, v3, v8

    if-ne v4, v8, :cond_f

    or-int/lit16 v0, v0, 0x4000

    :cond_f
    iget-object v4, v2, Lcom/google/r/b/a/ael;->r:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aep;->r:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aep;->r:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/2addr v3, v9

    if-ne v3, v9, :cond_10

    or-int/2addr v0, v7

    :cond_10
    iget-object v3, v2, Lcom/google/r/b/a/ael;->s:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/aep;->s:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/aep;->s:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/ael;->a:I

    return-object v2

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1042
    check-cast p1, Lcom/google/r/b/a/ael;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aep;->a(Lcom/google/r/b/a/ael;)Lcom/google/r/b/a/aep;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ael;)Lcom/google/r/b/a/aep;
    .locals 7

    .prologue
    const/16 v6, 0x4000

    const/16 v5, 0x10

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1204
    invoke-static {}, Lcom/google/r/b/a/ael;->g()Lcom/google/r/b/a/ael;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1290
    :goto_0
    return-object p0

    .line 1205
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_13

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1206
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1207
    iget-object v2, p1, Lcom/google/r/b/a/ael;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aep;->b:Ljava/lang/Object;

    .line 1210
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1211
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1212
    iget-object v2, p1, Lcom/google/r/b/a/ael;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aep;->c:Ljava/lang/Object;

    .line 1215
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1216
    iget-object v2, p0, Lcom/google/r/b/a/aep;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1217
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1219
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1220
    iget-object v2, p0, Lcom/google/r/b/a/aep;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1221
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1223
    :cond_4
    iget-object v2, p1, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1224
    iget-object v2, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1225
    iget-object v2, p1, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    .line 1226
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1233
    :cond_5
    :goto_5
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v5, :cond_19

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1234
    iget-object v2, p0, Lcom/google/r/b/a/aep;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1235
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1237
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1238
    iget-boolean v2, p1, Lcom/google/r/b/a/ael;->h:Z

    iget v3, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/aep;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aep;->h:Z

    .line 1240
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1241
    iget-boolean v2, p1, Lcom/google/r/b/a/ael;->i:Z

    iget v3, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/aep;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aep;->i:Z

    .line 1243
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1244
    iget-object v2, p0, Lcom/google/r/b/a/aep;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1245
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1247
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 1248
    iget-object v2, p0, Lcom/google/r/b/a/aep;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1249
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1251
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 1252
    iget-object v2, p0, Lcom/google/r/b/a/aep;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1253
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1255
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 1256
    iget-object v2, p0, Lcom/google/r/b/a/aep;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1257
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1259
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 1260
    iget-object v2, p0, Lcom/google/r/b/a/aep;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1261
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1263
    :cond_d
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_e
    if-eqz v2, :cond_e

    .line 1264
    iget-object v2, p0, Lcom/google/r/b/a/aep;->o:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1265
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1267
    :cond_e
    iget-object v2, p1, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 1268
    iget-object v2, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1269
    iget-object v2, p1, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    .line 1270
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit16 v2, v2, -0x4001

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1277
    :cond_f
    :goto_f
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_24

    move v2, v0

    :goto_10
    if-eqz v2, :cond_10

    .line 1278
    iget-object v2, p0, Lcom/google/r/b/a/aep;->q:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->q:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1279
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1281
    :cond_10
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/lit16 v2, v2, 0x4000

    if-ne v2, v6, :cond_25

    move v2, v0

    :goto_11
    if-eqz v2, :cond_11

    .line 1282
    iget-object v2, p0, Lcom/google/r/b/a/aep;->r:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->r:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1283
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    const/high16 v3, 0x10000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1285
    :cond_11
    iget v2, p1, Lcom/google/r/b/a/ael;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_26

    :goto_12
    if-eqz v0, :cond_12

    .line 1286
    iget-object v0, p0, Lcom/google/r/b/a/aep;->s:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/ael;->s:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1287
    iget v0, p0, Lcom/google/r/b/a/aep;->a:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1289
    :cond_12
    iget-object v0, p1, Lcom/google/r/b/a/ael;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v2, v1

    .line 1205
    goto/16 :goto_1

    :cond_14
    move v2, v1

    .line 1210
    goto/16 :goto_2

    :cond_15
    move v2, v1

    .line 1215
    goto/16 :goto_3

    :cond_16
    move v2, v1

    .line 1219
    goto/16 :goto_4

    .line 1228
    :cond_17
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eq v2, v5, :cond_18

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1229
    :cond_18
    iget-object v2, p0, Lcom/google/r/b/a/aep;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    :cond_19
    move v2, v1

    .line 1233
    goto/16 :goto_6

    :cond_1a
    move v2, v1

    .line 1237
    goto/16 :goto_7

    :cond_1b
    move v2, v1

    .line 1240
    goto/16 :goto_8

    :cond_1c
    move v2, v1

    .line 1243
    goto/16 :goto_9

    :cond_1d
    move v2, v1

    .line 1247
    goto/16 :goto_a

    :cond_1e
    move v2, v1

    .line 1251
    goto/16 :goto_b

    :cond_1f
    move v2, v1

    .line 1255
    goto/16 :goto_c

    :cond_20
    move v2, v1

    .line 1259
    goto/16 :goto_d

    :cond_21
    move v2, v1

    .line 1263
    goto/16 :goto_e

    .line 1272
    :cond_22
    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit16 v2, v2, 0x4000

    if-eq v2, v6, :cond_23

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/aep;->a:I

    or-int/lit16 v2, v2, 0x4000

    iput v2, p0, Lcom/google/r/b/a/aep;->a:I

    .line 1273
    :cond_23
    iget-object v2, p0, Lcom/google/r/b/a/aep;->p:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/ael;->p:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_f

    :cond_24
    move v2, v1

    .line 1277
    goto/16 :goto_10

    :cond_25
    move v2, v1

    .line 1281
    goto/16 :goto_11

    :cond_26
    move v0, v1

    .line 1285
    goto/16 :goto_12
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1294
    iget v0, p0, Lcom/google/r/b/a/aep;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1295
    iget-object v0, p0, Lcom/google/r/b/a/aep;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1300
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1294
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1300
    goto :goto_1
.end method
