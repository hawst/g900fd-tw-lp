.class public final Lcom/google/r/b/a/xh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/xm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xh;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/xh;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/google/r/b/a/xi;

    invoke-direct {v0}, Lcom/google/r/b/a/xi;-><init>()V

    sput-object v0, Lcom/google/r/b/a/xh;->PARSER:Lcom/google/n/ax;

    .line 271
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/xh;->g:Lcom/google/n/aw;

    .line 522
    new-instance v0, Lcom/google/r/b/a/xh;

    invoke-direct {v0}, Lcom/google/r/b/a/xh;-><init>()V

    sput-object v0, Lcom/google/r/b/a/xh;->d:Lcom/google/r/b/a/xh;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 224
    iput-byte v0, p0, Lcom/google/r/b/a/xh;->e:B

    .line 250
    iput v0, p0, Lcom/google/r/b/a/xh;->f:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/xh;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/r/b/a/xh;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 31
    const/4 v0, 0x0

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 34
    sparse-switch v3, :sswitch_data_0

    .line 39
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 47
    invoke-static {v3}, Lcom/google/r/b/a/xk;->a(I)Lcom/google/r/b/a/xk;

    move-result-object v4

    .line 48
    if-nez v4, :cond_1

    .line 49
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/xh;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/xh;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/xh;->a:I

    .line 52
    iput v3, p0, Lcom/google/r/b/a/xh;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 66
    :catch_1
    move-exception v0

    .line 67
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 68
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 58
    iget v4, p0, Lcom/google/r/b/a/xh;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/xh;->a:I

    .line 59
    iput-object v3, p0, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 70
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xh;->au:Lcom/google/n/bn;

    .line 71
    return-void

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 224
    iput-byte v0, p0, Lcom/google/r/b/a/xh;->e:B

    .line 250
    iput v0, p0, Lcom/google/r/b/a/xh;->f:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/r/b/a/xh;
    .locals 1

    .prologue
    .line 525
    sget-object v0, Lcom/google/r/b/a/xh;->d:Lcom/google/r/b/a/xh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/xj;
    .locals 1

    .prologue
    .line 333
    new-instance v0, Lcom/google/r/b/a/xj;

    invoke-direct {v0}, Lcom/google/r/b/a/xj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    sget-object v0, Lcom/google/r/b/a/xh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 240
    invoke-virtual {p0}, Lcom/google/r/b/a/xh;->c()I

    .line 241
    iget v0, p0, Lcom/google/r/b/a/xh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 242
    iget v0, p0, Lcom/google/r/b/a/xh;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 244
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/xh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 245
    iget-object v0, p0, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/xh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 248
    return-void

    .line 245
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 226
    iget-byte v2, p0, Lcom/google/r/b/a/xh;->e:B

    .line 227
    if-ne v2, v0, :cond_0

    .line 235
    :goto_0
    return v0

    .line 228
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 230
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/xh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 231
    iput-byte v1, p0, Lcom/google/r/b/a/xh;->e:B

    move v0, v1

    .line 232
    goto :goto_0

    :cond_2
    move v2, v1

    .line 230
    goto :goto_1

    .line 234
    :cond_3
    iput-byte v0, p0, Lcom/google/r/b/a/xh;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 252
    iget v0, p0, Lcom/google/r/b/a/xh;->f:I

    .line 253
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 266
    :goto_0
    return v0

    .line 256
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/xh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 257
    iget v0, p0, Lcom/google/r/b/a/xh;->b:I

    .line 258
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 260
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/xh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 262
    iget-object v0, p0, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/xh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 265
    iput v0, p0, Lcom/google/r/b/a/xh;->f:I

    goto :goto_0

    .line 258
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 262
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    .line 195
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 196
    check-cast v0, Ljava/lang/String;

    .line 204
    :goto_0
    return-object v0

    .line 198
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 200
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 201
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    iput-object v1, p0, Lcom/google/r/b/a/xh;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 204
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/xh;->newBuilder()Lcom/google/r/b/a/xj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/xj;->a(Lcom/google/r/b/a/xh;)Lcom/google/r/b/a/xj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/xh;->newBuilder()Lcom/google/r/b/a/xj;

    move-result-object v0

    return-object v0
.end method
