.class public final Lcom/google/r/b/a/a/ah;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/a/af;",
        "Lcom/google/r/b/a/a/ah;",
        ">;",
        "Lcom/google/r/b/a/a/ak;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field public d:I

.field public e:J

.field public f:Z

.field private g:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1041
    sget-object v0, Lcom/google/r/b/a/a/af;->h:Lcom/google/r/b/a/a/af;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1139
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/ah;->b:Lcom/google/n/ao;

    .line 1198
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/a/ah;->c:Ljava/lang/Object;

    .line 1274
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/a/ah;->d:I

    .line 1310
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/a/ah;->g:Ljava/lang/Object;

    .line 1042
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1033
    new-instance v2, Lcom/google/r/b/a/a/af;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/a/af;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/a/ah;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/a/ah;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/a/ah;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/a/ah;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/a/ah;->d:I

    iput v1, v2, Lcom/google/r/b/a/a/af;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/a/ah;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/a/af;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/r/b/a/a/ah;->e:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/af;->f:J

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v1, p0, Lcom/google/r/b/a/a/ah;->f:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/a/af;->g:Z

    iput v0, v2, Lcom/google/r/b/a/a/af;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1033
    check-cast p1, Lcom/google/r/b/a/a/af;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/a/ah;->a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/a/af;)Lcom/google/r/b/a/a/ah;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1099
    invoke-static {}, Lcom/google/r/b/a/a/af;->g()Lcom/google/r/b/a/a/af;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1124
    :goto_0
    return-object p0

    .line 1100
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1101
    iget-object v2, p0, Lcom/google/r/b/a/a/ah;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/a/af;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1102
    iget v2, p0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/a/ah;->a:I

    .line 1104
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1105
    iget v2, p0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/a/ah;->a:I

    .line 1106
    iget-object v2, p1, Lcom/google/r/b/a/a/af;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/a/ah;->c:Ljava/lang/Object;

    .line 1109
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 1110
    iget v2, p1, Lcom/google/r/b/a/a/af;->d:I

    invoke-static {v2}, Lcom/google/r/b/a/a/ai;->a(I)Lcom/google/r/b/a/a/ai;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/r/b/a/a/ai;->a:Lcom/google/r/b/a/a/ai;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 1100
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1104
    goto :goto_2

    :cond_6
    move v2, v1

    .line 1109
    goto :goto_3

    .line 1110
    :cond_7
    iget v3, p0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/a/ah;->a:I

    iget v2, v2, Lcom/google/r/b/a/a/ai;->f:I

    iput v2, p0, Lcom/google/r/b/a/a/ah;->d:I

    .line 1112
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    .line 1113
    iget v2, p0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/a/ah;->a:I

    .line 1114
    iget-object v2, p1, Lcom/google/r/b/a/a/af;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/a/ah;->g:Ljava/lang/Object;

    .line 1117
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_a

    .line 1118
    iget-wide v2, p1, Lcom/google/r/b/a/a/af;->f:J

    iget v4, p0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/a/ah;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/a/ah;->e:J

    .line 1120
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/a/af;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    :goto_6
    if-eqz v0, :cond_b

    .line 1121
    iget-boolean v0, p1, Lcom/google/r/b/a/a/af;->g:Z

    iget v1, p0, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/r/b/a/a/ah;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/a/ah;->f:Z

    .line 1123
    :cond_b
    iget-object v0, p1, Lcom/google/r/b/a/a/af;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 1112
    goto :goto_4

    :cond_d
    move v2, v1

    .line 1117
    goto :goto_5

    :cond_e
    move v0, v1

    .line 1120
    goto :goto_6
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1128
    iget v0, p0, Lcom/google/r/b/a/a/ah;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1129
    iget-object v0, p0, Lcom/google/r/b/a/a/ah;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/al;->d()Lcom/google/r/b/a/a/al;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/al;

    invoke-virtual {v0}, Lcom/google/r/b/a/a/al;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1134
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1128
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1134
    goto :goto_1
.end method
