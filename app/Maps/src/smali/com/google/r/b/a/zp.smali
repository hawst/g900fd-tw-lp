.class public final Lcom/google/r/b/a/zp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/zn;",
        "Lcom/google/r/b/a/zp;",
        ">;",
        "Lcom/google/r/b/a/zw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1778
    sget-object v0, Lcom/google/r/b/a/zn;->d:Lcom/google/r/b/a/zn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1838
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    .line 1974
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zp;->c:Ljava/lang/Object;

    .line 1779
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1770
    new-instance v2, Lcom/google/r/b/a/zn;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/zn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/zp;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/r/b/a/zp;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/zp;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/zp;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/zn;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/zp;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zn;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/zn;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1770
    check-cast p1, Lcom/google/r/b/a/zn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/zp;->a(Lcom/google/r/b/a/zn;)Lcom/google/r/b/a/zp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/zn;)Lcom/google/r/b/a/zp;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1811
    invoke-static {}, Lcom/google/r/b/a/zn;->d()Lcom/google/r/b/a/zn;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1828
    :goto_0
    return-object p0

    .line 1812
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/zn;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1813
    iget-object v1, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1814
    iget-object v1, p1, Lcom/google/r/b/a/zn;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    .line 1815
    iget v1, p0, Lcom/google/r/b/a/zp;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/zp;->a:I

    .line 1822
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/r/b/a/zn;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_2
    if-eqz v0, :cond_2

    .line 1823
    iget v0, p0, Lcom/google/r/b/a/zp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/zp;->a:I

    .line 1824
    iget-object v0, p1, Lcom/google/r/b/a/zn;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/zp;->c:Ljava/lang/Object;

    .line 1827
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/zn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1817
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/zp;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/zp;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/zp;->a:I

    .line 1818
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/zp;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/zn;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1822
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1832
    const/4 v0, 0x1

    return v0
.end method
