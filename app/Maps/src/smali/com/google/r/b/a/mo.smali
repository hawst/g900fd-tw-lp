.class public final Lcom/google/r/b/a/mo;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/mr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/mo;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/mo;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/r/b/a/ads;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 629
    new-instance v0, Lcom/google/r/b/a/mp;

    invoke-direct {v0}, Lcom/google/r/b/a/mp;-><init>()V

    sput-object v0, Lcom/google/r/b/a/mo;->PARSER:Lcom/google/n/ax;

    .line 702
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/mo;->f:Lcom/google/n/aw;

    .line 893
    new-instance v0, Lcom/google/r/b/a/mo;

    invoke-direct {v0}, Lcom/google/r/b/a/mo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/mo;->c:Lcom/google/r/b/a/mo;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 579
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 660
    iput-byte v0, p0, Lcom/google/r/b/a/mo;->d:B

    .line 685
    iput v0, p0, Lcom/google/r/b/a/mo;->e:I

    .line 580
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 586
    invoke-direct {p0}, Lcom/google/r/b/a/mo;-><init>()V

    .line 587
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 591
    const/4 v0, 0x0

    move v2, v0

    .line 592
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 593
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 594
    sparse-switch v0, :sswitch_data_0

    .line 599
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 601
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 597
    goto :goto_0

    .line 606
    :sswitch_1
    const/4 v0, 0x0

    .line 607
    iget v1, p0, Lcom/google/r/b/a/mo;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 608
    iget-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    invoke-static {v0}, Lcom/google/r/b/a/ads;->a(Lcom/google/r/b/a/ads;)Lcom/google/r/b/a/adu;

    move-result-object v0

    move-object v1, v0

    .line 610
    :goto_1
    sget-object v0, Lcom/google/r/b/a/ads;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    iput-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    .line 611
    if-eqz v1, :cond_1

    .line 612
    iget-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/adu;->a(Lcom/google/r/b/a/ads;)Lcom/google/r/b/a/adu;

    .line 613
    invoke-virtual {v1}, Lcom/google/r/b/a/adu;->c()Lcom/google/r/b/a/ads;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    .line 615
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/mo;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/mo;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 620
    :catch_0
    move-exception v0

    .line 621
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 626
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/mo;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mo;->au:Lcom/google/n/bn;

    .line 627
    return-void

    .line 622
    :catch_1
    move-exception v0

    .line 623
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 624
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 594
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 577
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 660
    iput-byte v0, p0, Lcom/google/r/b/a/mo;->d:B

    .line 685
    iput v0, p0, Lcom/google/r/b/a/mo;->e:I

    .line 578
    return-void
.end method

.method public static d()Lcom/google/r/b/a/mo;
    .locals 1

    .prologue
    .line 896
    sget-object v0, Lcom/google/r/b/a/mo;->c:Lcom/google/r/b/a/mo;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/mq;
    .locals 1

    .prologue
    .line 764
    new-instance v0, Lcom/google/r/b/a/mq;

    invoke-direct {v0}, Lcom/google/r/b/a/mq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/mo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 641
    sget-object v0, Lcom/google/r/b/a/mo;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 678
    invoke-virtual {p0}, Lcom/google/r/b/a/mo;->c()I

    .line 679
    iget v0, p0, Lcom/google/r/b/a/mo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 680
    iget-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 682
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/mo;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 683
    return-void

    .line 680
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 662
    iget-byte v0, p0, Lcom/google/r/b/a/mo;->d:B

    .line 663
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 673
    :goto_0
    return v0

    .line 664
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 666
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/mo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 667
    iget-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 668
    iput-byte v2, p0, Lcom/google/r/b/a/mo;->d:B

    move v0, v2

    .line 669
    goto :goto_0

    :cond_2
    move v0, v2

    .line 666
    goto :goto_1

    .line 667
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    goto :goto_2

    .line 672
    :cond_4
    iput-byte v1, p0, Lcom/google/r/b/a/mo;->d:B

    move v0, v1

    .line 673
    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 687
    iget v0, p0, Lcom/google/r/b/a/mo;->e:I

    .line 688
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 697
    :goto_0
    return v0

    .line 691
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/mo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 693
    iget-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 695
    :goto_2
    iget-object v1, p0, Lcom/google/r/b/a/mo;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 696
    iput v0, p0, Lcom/google/r/b/a/mo;->e:I

    goto :goto_0

    .line 693
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 571
    invoke-static {}, Lcom/google/r/b/a/mo;->newBuilder()Lcom/google/r/b/a/mq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/mq;->a(Lcom/google/r/b/a/mo;)Lcom/google/r/b/a/mq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 571
    invoke-static {}, Lcom/google/r/b/a/mo;->newBuilder()Lcom/google/r/b/a/mq;

    move-result-object v0

    return-object v0
.end method
