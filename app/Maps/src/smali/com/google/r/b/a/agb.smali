.class public final Lcom/google/r/b/a/agb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/agc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/afz;",
        "Lcom/google/r/b/a/agb;",
        ">;",
        "Lcom/google/r/b/a/agc;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Z

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Lcom/google/n/ao;

.field public h:Lcom/google/n/ao;

.field public i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Lcom/google/n/ao;

.field private n:Lcom/google/n/ao;

.field private o:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4177
    sget-object v0, Lcom/google/r/b/a/afz;->p:Lcom/google/r/b/a/afz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4360
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    .line 4419
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agb;->c:Lcom/google/n/ao;

    .line 4510
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/agb;->j:Z

    .line 4606
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agb;->m:Lcom/google/n/ao;

    .line 4665
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    .line 4763
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agb;->g:Lcom/google/n/ao;

    .line 4822
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agb;->n:Lcom/google/n/ao;

    .line 4913
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agb;->h:Lcom/google/n/ao;

    .line 4178
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 4667
    iget v0, p0, Lcom/google/r/b/a/agb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 4668
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    .line 4669
    iget v0, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4671
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 4169
    invoke-virtual {p0}, Lcom/google/r/b/a/agb;->c()Lcom/google/r/b/a/afz;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4169
    check-cast p1, Lcom/google/r/b/a/afz;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/maps/g/a/ho;)Lcom/google/r/b/a/agb;
    .locals 2

    .prologue
    .line 4778
    if-nez p1, :cond_0

    .line 4779
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4781
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/agb;->g:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4783
    iget v0, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4784
    return-object p0
.end method

.method public final a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4294
    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4351
    :goto_0
    return-object p0

    .line 4295
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_f

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4296
    iget-object v2, p0, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4297
    iget v2, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4299
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 4300
    iget-object v2, p0, Lcom/google/r/b/a/agb;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4301
    iget v2, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4303
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 4304
    iget-boolean v2, p1, Lcom/google/r/b/a/afz;->d:Z

    iget v3, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agb;->d:Z

    .line 4306
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 4307
    iget-boolean v2, p1, Lcom/google/r/b/a/afz;->e:Z

    iget v3, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agb;->j:Z

    .line 4309
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 4310
    iget-boolean v2, p1, Lcom/google/r/b/a/afz;->f:Z

    iget v3, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agb;->k:Z

    .line 4312
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 4313
    iget-boolean v2, p1, Lcom/google/r/b/a/afz;->g:Z

    iget v3, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agb;->l:Z

    .line 4315
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 4316
    iget-object v2, p0, Lcom/google/r/b/a/agb;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afz;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4317
    iget v2, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4319
    :cond_7
    iget-object v2, p1, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 4320
    iget-object v2, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 4321
    iget-object v2, p1, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    .line 4322
    iget v2, p0, Lcom/google/r/b/a/agb;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4329
    :cond_8
    :goto_8
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 4330
    iget-boolean v2, p1, Lcom/google/r/b/a/afz;->j:Z

    iget v3, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agb;->f:Z

    .line 4332
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 4333
    iget-object v2, p0, Lcom/google/r/b/a/agb;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4334
    iget v2, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4336
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 4337
    iget-object v2, p0, Lcom/google/r/b/a/agb;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afz;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4338
    iget v2, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4340
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 4341
    iget-boolean v2, p1, Lcom/google/r/b/a/afz;->m:Z

    iget v3, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/agb;->o:Z

    .line 4343
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 4344
    iget-object v2, p0, Lcom/google/r/b/a/agb;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afz;->n:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4345
    iget v2, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4347
    :cond_d
    iget v2, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1c

    :goto_e
    if-eqz v0, :cond_e

    .line 4348
    iget-boolean v0, p1, Lcom/google/r/b/a/afz;->o:Z

    iget v1, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit16 v1, v1, 0x2000

    iput v1, p0, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/agb;->i:Z

    .line 4350
    :cond_e
    iget-object v0, p1, Lcom/google/r/b/a/afz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v2, v1

    .line 4295
    goto/16 :goto_1

    :cond_10
    move v2, v1

    .line 4299
    goto/16 :goto_2

    :cond_11
    move v2, v1

    .line 4303
    goto/16 :goto_3

    :cond_12
    move v2, v1

    .line 4306
    goto/16 :goto_4

    :cond_13
    move v2, v1

    .line 4309
    goto/16 :goto_5

    :cond_14
    move v2, v1

    .line 4312
    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 4315
    goto/16 :goto_7

    .line 4324
    :cond_16
    invoke-direct {p0}, Lcom/google/r/b/a/agb;->d()V

    .line 4325
    iget-object v2, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_17
    move v2, v1

    .line 4329
    goto/16 :goto_9

    :cond_18
    move v2, v1

    .line 4332
    goto/16 :goto_a

    :cond_19
    move v2, v1

    .line 4336
    goto/16 :goto_b

    :cond_1a
    move v2, v1

    .line 4340
    goto :goto_c

    :cond_1b
    move v2, v1

    .line 4343
    goto :goto_d

    :cond_1c
    move v0, v1

    .line 4347
    goto :goto_e
.end method

.method public final a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agb;
    .locals 2

    .prologue
    .line 4375
    if-nez p1, :cond_0

    .line 4376
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4378
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4380
    iget v0, p0, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4381
    return-object p0
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/r/b/a/agb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/r/b/a/agb;"
        }
    .end annotation

    .prologue
    .line 4715
    invoke-direct {p0}, Lcom/google/r/b/a/agb;->d()V

    .line 4716
    iget-object v0, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/n/b;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 4719
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4355
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/r/b/a/afz;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4217
    new-instance v2, Lcom/google/r/b/a/afz;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/afz;-><init>(Lcom/google/n/v;)V

    .line 4218
    iget v3, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4220
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_d

    .line 4223
    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    .line 4224
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    .line 4225
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 4223
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 4226
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 4227
    or-int/lit8 v0, v0, 0x2

    .line 4229
    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/agb;->c:Lcom/google/n/ao;

    .line 4230
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/agb;->c:Lcom/google/n/ao;

    .line 4231
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 4229
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 4232
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 4233
    or-int/lit8 v0, v0, 0x4

    .line 4235
    :cond_1
    iget-boolean v4, p0, Lcom/google/r/b/a/agb;->d:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afz;->d:Z

    .line 4236
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 4237
    or-int/lit8 v0, v0, 0x8

    .line 4239
    :cond_2
    iget-boolean v4, p0, Lcom/google/r/b/a/agb;->j:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afz;->e:Z

    .line 4240
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 4241
    or-int/lit8 v0, v0, 0x10

    .line 4243
    :cond_3
    iget-boolean v4, p0, Lcom/google/r/b/a/agb;->k:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afz;->f:Z

    .line 4244
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 4245
    or-int/lit8 v0, v0, 0x20

    .line 4247
    :cond_4
    iget-boolean v4, p0, Lcom/google/r/b/a/agb;->l:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afz;->g:Z

    .line 4248
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 4249
    or-int/lit8 v0, v0, 0x40

    .line 4251
    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/afz;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/agb;->m:Lcom/google/n/ao;

    .line 4252
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/agb;->m:Lcom/google/n/ao;

    .line 4253
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 4251
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 4254
    iget v4, p0, Lcom/google/r/b/a/agb;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 4255
    iget-object v4, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    .line 4256
    iget v4, p0, Lcom/google/r/b/a/agb;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/r/b/a/agb;->a:I

    .line 4258
    :cond_6
    iget-object v4, p0, Lcom/google/r/b/a/agb;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/afz;->i:Ljava/util/List;

    .line 4259
    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    .line 4260
    or-int/lit16 v0, v0, 0x80

    .line 4262
    :cond_7
    iget-boolean v4, p0, Lcom/google/r/b/a/agb;->f:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afz;->j:Z

    .line 4263
    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    .line 4264
    or-int/lit16 v0, v0, 0x100

    .line 4266
    :cond_8
    iget-object v4, v2, Lcom/google/r/b/a/afz;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/agb;->g:Lcom/google/n/ao;

    .line 4267
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/agb;->g:Lcom/google/n/ao;

    .line 4268
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 4266
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 4269
    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    .line 4270
    or-int/lit16 v0, v0, 0x200

    .line 4272
    :cond_9
    iget-object v4, v2, Lcom/google/r/b/a/afz;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/agb;->n:Lcom/google/n/ao;

    .line 4273
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/agb;->n:Lcom/google/n/ao;

    .line 4274
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 4272
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 4275
    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    .line 4276
    or-int/lit16 v0, v0, 0x400

    .line 4278
    :cond_a
    iget-boolean v4, p0, Lcom/google/r/b/a/agb;->o:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afz;->m:Z

    .line 4279
    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    .line 4280
    or-int/lit16 v0, v0, 0x800

    .line 4282
    :cond_b
    iget-object v4, v2, Lcom/google/r/b/a/afz;->n:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/agb;->h:Lcom/google/n/ao;

    .line 4283
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/agb;->h:Lcom/google/n/ao;

    .line 4284
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 4282
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 4285
    and-int/lit16 v1, v3, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_c

    .line 4286
    or-int/lit16 v0, v0, 0x1000

    .line 4288
    :cond_c
    iget-boolean v1, p0, Lcom/google/r/b/a/agb;->i:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/afz;->o:Z

    .line 4289
    iput v0, v2, Lcom/google/r/b/a/afz;->a:I

    .line 4290
    return-object v2

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method
