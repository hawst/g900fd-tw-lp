.class public final Lcom/google/r/b/a/xt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/xw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xt;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/xt;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Z

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1425
    new-instance v0, Lcom/google/r/b/a/xu;

    invoke-direct {v0}, Lcom/google/r/b/a/xu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/xt;->PARSER:Lcom/google/n/ax;

    .line 1571
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/xt;->h:Lcom/google/n/aw;

    .line 1958
    new-instance v0, Lcom/google/r/b/a/xt;

    invoke-direct {v0}, Lcom/google/r/b/a/xt;-><init>()V

    sput-object v0, Lcom/google/r/b/a/xt;->e:Lcom/google/r/b/a/xt;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1362
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1442
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    .line 1515
    iput-byte v2, p0, Lcom/google/r/b/a/xt;->f:B

    .line 1546
    iput v2, p0, Lcom/google/r/b/a/xt;->g:I

    .line 1363
    iget-object v0, p0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1364
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/xt;->c:Z

    .line 1365
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    .line 1366
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/4 v10, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1372
    invoke-direct {p0}, Lcom/google/r/b/a/xt;-><init>()V

    .line 1375
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v0, v3

    .line 1378
    :cond_0
    :goto_0
    if-nez v4, :cond_4

    .line 1379
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 1380
    sparse-switch v1, :sswitch_data_0

    .line 1385
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    .line 1387
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 1383
    goto :goto_0

    .line 1392
    :sswitch_1
    iget-object v1, p0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 1393
    iget v1, p0, Lcom/google/r/b/a/xt;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/xt;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 1413
    :catch_0
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 1414
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1419
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v10, :cond_1

    .line 1420
    iget-object v1, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    .line 1422
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/xt;->au:Lcom/google/n/bn;

    throw v0

    .line 1397
    :sswitch_2
    :try_start_2
    iget v1, p0, Lcom/google/r/b/a/xt;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/xt;->a:I

    .line 1398
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/google/r/b/a/xt;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 1415
    :catch_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 1416
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1417
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v1, v3

    .line 1398
    goto :goto_2

    .line 1402
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v10, :cond_3

    .line 1403
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    .line 1405
    or-int/lit8 v0, v0, 0x4

    .line 1407
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 1408
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 1407
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 1419
    :catchall_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto :goto_1

    :cond_4
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v10, :cond_5

    .line 1420
    iget-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    .line 1422
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xt;->au:Lcom/google/n/bn;

    .line 1423
    return-void

    .line 1380
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x28 -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1360
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1442
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    .line 1515
    iput-byte v1, p0, Lcom/google/r/b/a/xt;->f:B

    .line 1546
    iput v1, p0, Lcom/google/r/b/a/xt;->g:I

    .line 1361
    return-void
.end method

.method public static g()Lcom/google/r/b/a/xt;
    .locals 1

    .prologue
    .line 1961
    sget-object v0, Lcom/google/r/b/a/xt;->e:Lcom/google/r/b/a/xt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/xv;
    .locals 1

    .prologue
    .line 1633
    new-instance v0, Lcom/google/r/b/a/xv;

    invoke-direct {v0}, Lcom/google/r/b/a/xv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1437
    sget-object v0, Lcom/google/r/b/a/xt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 1533
    invoke-virtual {p0}, Lcom/google/r/b/a/xt;->c()I

    .line 1534
    iget v0, p0, Lcom/google/r/b/a/xt;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1535
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1537
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/xt;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1538
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/xt;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1540
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1541
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1540
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1543
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/xt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1544
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1517
    iget-byte v0, p0, Lcom/google/r/b/a/xt;->f:B

    .line 1518
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1528
    :goto_0
    return v0

    .line 1519
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1521
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/xt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1522
    iget-object v0, p0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mj;

    invoke-virtual {v0}, Lcom/google/o/h/a/mj;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1523
    iput-byte v2, p0, Lcom/google/r/b/a/xt;->f:B

    move v0, v2

    .line 1524
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1521
    goto :goto_1

    .line 1527
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/xt;->f:B

    move v0, v1

    .line 1528
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1548
    iget v0, p0, Lcom/google/r/b/a/xt;->g:I

    .line 1549
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1566
    :goto_0
    return v0

    .line 1552
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/xt;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 1553
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    .line 1554
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1556
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/xt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1557
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/r/b/a/xt;->c:Z

    .line 1558
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v0

    .line 1560
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1561
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    .line 1562
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1560
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1564
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/xt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1565
    iput v0, p0, Lcom/google/r/b/a/xt;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1479
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    .line 1480
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1481
    iget-object v0, p0, Lcom/google/r/b/a/xt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1482
    invoke-static {}, Lcom/google/r/b/a/ada;->g()Lcom/google/r/b/a/ada;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ada;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1484
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1354
    invoke-static {}, Lcom/google/r/b/a/xt;->newBuilder()Lcom/google/r/b/a/xv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/xv;->a(Lcom/google/r/b/a/xt;)Lcom/google/r/b/a/xv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1354
    invoke-static {}, Lcom/google/r/b/a/xt;->newBuilder()Lcom/google/r/b/a/xv;

    move-result-object v0

    return-object v0
.end method
