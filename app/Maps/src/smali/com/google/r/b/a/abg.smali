.class public final Lcom/google/r/b/a/abg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/abh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/abe;",
        "Lcom/google/r/b/a/abg;",
        ">;",
        "Lcom/google/r/b/a/abh;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2867
    sget-object v0, Lcom/google/r/b/a/abe;->d:Lcom/google/r/b/a/abe;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2934
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    .line 3070
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/abg;->c:Lcom/google/n/ao;

    .line 2868
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2859
    new-instance v2, Lcom/google/r/b/a/abe;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/abe;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/abg;->b:I

    iget v4, p0, Lcom/google/r/b/a/abg;->b:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/abg;->b:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/abg;->b:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v3, v2, Lcom/google/r/b/a/abe;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/abg;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/abg;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/abe;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2859
    check-cast p1, Lcom/google/r/b/a/abe;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/abg;->a(Lcom/google/r/b/a/abe;)Lcom/google/r/b/a/abg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/abe;)Lcom/google/r/b/a/abg;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2902
    invoke-static {}, Lcom/google/r/b/a/abe;->d()Lcom/google/r/b/a/abe;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 2918
    :goto_0
    return-object p0

    .line 2903
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2904
    iget-object v1, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2905
    iget-object v1, p1, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    .line 2906
    iget v1, p0, Lcom/google/r/b/a/abg;->b:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/abg;->b:I

    .line 2913
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/r/b/a/abe;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 2914
    iget-object v0, p0, Lcom/google/r/b/a/abg;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/abe;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2915
    iget v0, p0, Lcom/google/r/b/a/abg;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/abg;->b:I

    .line 2917
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/abe;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 2908
    :cond_3
    invoke-virtual {p0}, Lcom/google/r/b/a/abg;->c()V

    .line 2909
    iget-object v1, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2913
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2922
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2923
    iget-object v0, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aba;->d()Lcom/google/r/b/a/aba;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aba;

    invoke-virtual {v0}, Lcom/google/r/b/a/aba;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2928
    :goto_1
    return v2

    .line 2922
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2928
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2936
    iget v0, p0, Lcom/google/r/b/a/abg;->b:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 2937
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/abg;->a:Ljava/util/List;

    .line 2940
    iget v0, p0, Lcom/google/r/b/a/abg;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/abg;->b:I

    .line 2942
    :cond_0
    return-void
.end method
