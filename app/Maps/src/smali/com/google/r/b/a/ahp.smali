.class public final Lcom/google/r/b/a/ahp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ahq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ahn;",
        "Lcom/google/r/b/a/ahp;",
        ">;",
        "Lcom/google/r/b/a/ahq;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/aq;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 331
    sget-object v0, Lcom/google/r/b/a/ahn;->d:Lcom/google/r/b/a/ahn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 390
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ahp;->b:Ljava/lang/Object;

    .line 466
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    .line 332
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 323
    new-instance v2, Lcom/google/r/b/a/ahn;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ahn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ahp;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/ahp;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ahn;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/r/b/a/ahp;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/r/b/a/ahp;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/ahp;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    iput v0, v2, Lcom/google/r/b/a/ahn;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 323
    check-cast p1, Lcom/google/r/b/a/ahn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ahp;->a(Lcom/google/r/b/a/ahn;)Lcom/google/r/b/a/ahp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ahn;)Lcom/google/r/b/a/ahp;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 364
    invoke-static {}, Lcom/google/r/b/a/ahn;->d()Lcom/google/r/b/a/ahn;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 381
    :goto_0
    return-object p0

    .line 365
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/ahn;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    .line 366
    iget v0, p0, Lcom/google/r/b/a/ahp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ahp;->a:I

    .line 367
    iget-object v0, p1, Lcom/google/r/b/a/ahn;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/ahp;->b:Ljava/lang/Object;

    .line 370
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 371
    iget-object v0, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 372
    iget-object v0, p1, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    .line 373
    iget v0, p0, Lcom/google/r/b/a/ahp;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/ahp;->a:I

    .line 380
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/ahn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 365
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 375
    :cond_4
    invoke-virtual {p0}, Lcom/google/r/b/a/ahp;->c()V

    .line 376
    iget-object v0, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/r/b/a/ahn;->c:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 385
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 468
    iget v0, p0, Lcom/google/r/b/a/ahp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 469
    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/r/b/a/ahp;->c:Lcom/google/n/aq;

    .line 470
    iget v0, p0, Lcom/google/r/b/a/ahp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ahp;->a:I

    .line 472
    :cond_0
    return-void
.end method
