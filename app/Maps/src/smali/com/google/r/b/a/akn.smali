.class public final Lcom/google/r/b/a/akn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/akq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/akn;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/akn;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Z

.field c:Z

.field d:Lcom/google/n/ao;

.field e:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14235
    new-instance v0, Lcom/google/r/b/a/ako;

    invoke-direct {v0}, Lcom/google/r/b/a/ako;-><init>()V

    sput-object v0, Lcom/google/r/b/a/akn;->PARSER:Lcom/google/n/ax;

    .line 14369
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/akn;->i:Lcom/google/n/aw;

    .line 14678
    new-instance v0, Lcom/google/r/b/a/akn;

    invoke-direct {v0}, Lcom/google/r/b/a/akn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/akn;->f:Lcom/google/r/b/a/akn;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 14174
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 14282
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/akn;->d:Lcom/google/n/ao;

    .line 14312
    iput-byte v3, p0, Lcom/google/r/b/a/akn;->g:B

    .line 14340
    iput v3, p0, Lcom/google/r/b/a/akn;->h:I

    .line 14175
    iput-boolean v2, p0, Lcom/google/r/b/a/akn;->b:Z

    .line 14176
    iput-boolean v2, p0, Lcom/google/r/b/a/akn;->c:Z

    .line 14177
    iget-object v0, p0, Lcom/google/r/b/a/akn;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 14178
    iput-boolean v2, p0, Lcom/google/r/b/a/akn;->e:Z

    .line 14179
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 14185
    invoke-direct {p0}, Lcom/google/r/b/a/akn;-><init>()V

    .line 14186
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 14191
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 14192
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 14193
    sparse-switch v0, :sswitch_data_0

    .line 14198
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 14200
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 14196
    goto :goto_0

    .line 14205
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/akn;->a:I

    .line 14206
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/akn;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 14226
    :catch_0
    move-exception v0

    .line 14227
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 14232
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/akn;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 14206
    goto :goto_1

    .line 14210
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/akn;->a:I

    .line 14211
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/akn;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 14228
    :catch_1
    move-exception v0

    .line 14229
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 14230
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v0, v2

    .line 14211
    goto :goto_2

    .line 14215
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/r/b/a/akn;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 14216
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/akn;->a:I

    goto :goto_0

    .line 14220
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/akn;->a:I

    .line 14221
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/r/b/a/akn;->e:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 14232
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/akn;->au:Lcom/google/n/bn;

    .line 14233
    return-void

    .line 14193
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 14172
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 14282
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/akn;->d:Lcom/google/n/ao;

    .line 14312
    iput-byte v1, p0, Lcom/google/r/b/a/akn;->g:B

    .line 14340
    iput v1, p0, Lcom/google/r/b/a/akn;->h:I

    .line 14173
    return-void
.end method

.method public static d()Lcom/google/r/b/a/akn;
    .locals 1

    .prologue
    .line 14681
    sget-object v0, Lcom/google/r/b/a/akn;->f:Lcom/google/r/b/a/akn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/akp;
    .locals 1

    .prologue
    .line 14431
    new-instance v0, Lcom/google/r/b/a/akp;

    invoke-direct {v0}, Lcom/google/r/b/a/akp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/akn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14247
    sget-object v0, Lcom/google/r/b/a/akn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 14324
    invoke-virtual {p0}, Lcom/google/r/b/a/akn;->c()I

    .line 14325
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 14326
    iget-boolean v0, p0, Lcom/google/r/b/a/akn;->b:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IZ)V

    .line 14328
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 14329
    iget-boolean v0, p0, Lcom/google/r/b/a/akn;->c:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 14331
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 14332
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/akn;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14334
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 14335
    iget-boolean v0, p0, Lcom/google/r/b/a/akn;->e:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 14337
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/akn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 14338
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 14314
    iget-byte v1, p0, Lcom/google/r/b/a/akn;->g:B

    .line 14315
    if-ne v1, v0, :cond_0

    .line 14319
    :goto_0
    return v0

    .line 14316
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 14318
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/akn;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 14342
    iget v0, p0, Lcom/google/r/b/a/akn;->h:I

    .line 14343
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 14364
    :goto_0
    return v0

    .line 14346
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/akn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 14347
    iget-boolean v0, p0, Lcom/google/r/b/a/akn;->b:Z

    .line 14348
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 14350
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/akn;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 14351
    iget-boolean v2, p0, Lcom/google/r/b/a/akn;->c:Z

    .line 14352
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 14354
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/akn;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 14355
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/akn;->d:Lcom/google/n/ao;

    .line 14356
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 14358
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/akn;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 14359
    iget-boolean v2, p0, Lcom/google/r/b/a/akn;->e:Z

    .line 14360
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14362
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/akn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 14363
    iput v0, p0, Lcom/google/r/b/a/akn;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 14166
    invoke-static {}, Lcom/google/r/b/a/akn;->newBuilder()Lcom/google/r/b/a/akp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/akp;->a(Lcom/google/r/b/a/akn;)Lcom/google/r/b/a/akp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 14166
    invoke-static {}, Lcom/google/r/b/a/akn;->newBuilder()Lcom/google/r/b/a/akp;

    move-result-object v0

    return-object v0
.end method
