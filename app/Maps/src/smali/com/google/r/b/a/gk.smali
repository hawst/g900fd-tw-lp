.class public final Lcom/google/r/b/a/gk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/gp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/gk;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/gk;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:I

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/google/r/b/a/gl;

    invoke-direct {v0}, Lcom/google/r/b/a/gl;-><init>()V

    sput-object v0, Lcom/google/r/b/a/gk;->PARSER:Lcom/google/n/ax;

    .line 308
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/gk;->h:Lcom/google/n/aw;

    .line 596
    new-instance v0, Lcom/google/r/b/a/gk;

    invoke-direct {v0}, Lcom/google/r/b/a/gk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/gk;->e:Lcom/google/r/b/a/gk;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 58
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 258
    iput-byte v0, p0, Lcom/google/r/b/a/gk;->f:B

    .line 283
    iput v0, p0, Lcom/google/r/b/a/gk;->g:I

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/gk;->b:Ljava/lang/Object;

    .line 60
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/gk;->c:I

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/gk;->d:Z

    .line 62
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 68
    invoke-direct {p0}, Lcom/google/r/b/a/gk;-><init>()V

    .line 69
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 74
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 75
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 76
    sparse-switch v0, :sswitch_data_0

    .line 81
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 83
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 79
    goto :goto_0

    .line 88
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 89
    iget v5, p0, Lcom/google/r/b/a/gk;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/gk;->a:I

    .line 90
    iput-object v0, p0, Lcom/google/r/b/a/gk;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/gk;->au:Lcom/google/n/bn;

    throw v0

    .line 94
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 95
    invoke-static {v0}, Lcom/google/r/b/a/gn;->a(I)Lcom/google/r/b/a/gn;

    move-result-object v5

    .line 96
    if-nez v5, :cond_1

    .line 97
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 113
    :catch_1
    move-exception v0

    .line 114
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 115
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 99
    :cond_1
    :try_start_4
    iget v5, p0, Lcom/google/r/b/a/gk;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/gk;->a:I

    .line 100
    iput v0, p0, Lcom/google/r/b/a/gk;->c:I

    goto :goto_0

    .line 105
    :sswitch_3
    iget v0, p0, Lcom/google/r/b/a/gk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/gk;->a:I

    .line 106
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/gk;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 117
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/gk;->au:Lcom/google/n/bn;

    .line 118
    return-void

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 56
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 258
    iput-byte v0, p0, Lcom/google/r/b/a/gk;->f:B

    .line 283
    iput v0, p0, Lcom/google/r/b/a/gk;->g:I

    .line 57
    return-void
.end method

.method public static d()Lcom/google/r/b/a/gk;
    .locals 1

    .prologue
    .line 599
    sget-object v0, Lcom/google/r/b/a/gk;->e:Lcom/google/r/b/a/gk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/gm;
    .locals 1

    .prologue
    .line 370
    new-instance v0, Lcom/google/r/b/a/gm;

    invoke-direct {v0}, Lcom/google/r/b/a/gm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/gk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lcom/google/r/b/a/gk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 270
    invoke-virtual {p0}, Lcom/google/r/b/a/gk;->c()I

    .line 271
    iget v0, p0, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 272
    iget-object v0, p0, Lcom/google/r/b/a/gk;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/gk;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 274
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 275
    iget v0, p0, Lcom/google/r/b/a/gk;->c:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 277
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 278
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/gk;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/gk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 281
    return-void

    .line 272
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 260
    iget-byte v1, p0, Lcom/google/r/b/a/gk;->f:B

    .line 261
    if-ne v1, v0, :cond_0

    .line 265
    :goto_0
    return v0

    .line 262
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 264
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/gk;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 285
    iget v0, p0, Lcom/google/r/b/a/gk;->g:I

    .line 286
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 303
    :goto_0
    return v0

    .line 289
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 291
    iget-object v0, p0, Lcom/google/r/b/a/gk;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/gk;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 293
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 294
    iget v2, p0, Lcom/google/r/b/a/gk;->c:I

    .line 295
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 297
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/gk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 298
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/r/b/a/gk;->d:Z

    .line 299
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 301
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/gk;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 302
    iput v0, p0, Lcom/google/r/b/a/gk;->g:I

    goto :goto_0

    .line 291
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 295
    :cond_4
    const/16 v2, 0xa

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/r/b/a/gk;->newBuilder()Lcom/google/r/b/a/gm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/gm;->a(Lcom/google/r/b/a/gk;)Lcom/google/r/b/a/gm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/r/b/a/gk;->newBuilder()Lcom/google/r/b/a/gm;

    move-result-object v0

    return-object v0
.end method
