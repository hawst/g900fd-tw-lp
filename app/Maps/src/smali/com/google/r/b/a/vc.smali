.class public final Lcom/google/r/b/a/vc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/vf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/vc;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/r/b/a/vc;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Z

.field public c:Lcom/google/n/f;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:J

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1294
    new-instance v0, Lcom/google/r/b/a/vd;

    invoke-direct {v0}, Lcom/google/r/b/a/vd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/vc;->PARSER:Lcom/google/n/ax;

    .line 1493
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/vc;->l:Lcom/google/n/aw;

    .line 1898
    new-instance v0, Lcom/google/r/b/a/vc;

    invoke-direct {v0}, Lcom/google/r/b/a/vc;-><init>()V

    sput-object v0, Lcom/google/r/b/a/vc;->i:Lcom/google/r/b/a/vc;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x3e8

    const/4 v0, -0x1

    .line 1215
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1415
    iput-byte v0, p0, Lcom/google/r/b/a/vc;->j:B

    .line 1452
    iput v0, p0, Lcom/google/r/b/a/vc;->k:I

    .line 1216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/vc;->b:Z

    .line 1217
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/vc;->c:Lcom/google/n/f;

    .line 1218
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/vc;->d:I

    .line 1219
    const/16 v0, 0x708

    iput v0, p0, Lcom/google/r/b/a/vc;->e:I

    .line 1220
    iput v1, p0, Lcom/google/r/b/a/vc;->f:I

    .line 1221
    iput v1, p0, Lcom/google/r/b/a/vc;->g:I

    .line 1222
    const-wide/16 v0, 0x3c

    iput-wide v0, p0, Lcom/google/r/b/a/vc;->h:J

    .line 1223
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1229
    invoke-direct {p0}, Lcom/google/r/b/a/vc;-><init>()V

    .line 1230
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 1235
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 1236
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1237
    sparse-switch v0, :sswitch_data_0

    .line 1242
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 1244
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 1240
    goto :goto_0

    .line 1249
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/vc;->a:I

    .line 1250
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/vc;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1285
    :catch_0
    move-exception v0

    .line 1286
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1291
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/vc;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 1250
    goto :goto_1

    .line 1254
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/vc;->a:I

    .line 1255
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vc;->c:Lcom/google/n/f;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1287
    :catch_1
    move-exception v0

    .line 1288
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1289
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1259
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/vc;->a:I

    .line 1260
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/vc;->d:I

    goto :goto_0

    .line 1264
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/vc;->a:I

    .line 1265
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/vc;->e:I

    goto :goto_0

    .line 1269
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/vc;->a:I

    .line 1270
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/vc;->f:I

    goto :goto_0

    .line 1274
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/vc;->a:I

    .line 1275
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/vc;->g:I

    goto/16 :goto_0

    .line 1279
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/vc;->a:I

    .line 1280
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/vc;->h:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1291
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vc;->au:Lcom/google/n/bn;

    .line 1292
    return-void

    .line 1237
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1213
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1415
    iput-byte v0, p0, Lcom/google/r/b/a/vc;->j:B

    .line 1452
    iput v0, p0, Lcom/google/r/b/a/vc;->k:I

    .line 1214
    return-void
.end method

.method public static d()Lcom/google/r/b/a/vc;
    .locals 1

    .prologue
    .line 1901
    sget-object v0, Lcom/google/r/b/a/vc;->i:Lcom/google/r/b/a/vc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ve;
    .locals 1

    .prologue
    .line 1555
    new-instance v0, Lcom/google/r/b/a/ve;

    invoke-direct {v0}, Lcom/google/r/b/a/ve;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/vc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1306
    sget-object v0, Lcom/google/r/b/a/vc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1427
    invoke-virtual {p0}, Lcom/google/r/b/a/vc;->c()I

    .line 1428
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1429
    iget-boolean v0, p0, Lcom/google/r/b/a/vc;->b:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IZ)V

    .line 1431
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1432
    iget-object v0, p0, Lcom/google/r/b/a/vc;->c:Lcom/google/n/f;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1434
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 1435
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/vc;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1437
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1438
    iget v0, p0, Lcom/google/r/b/a/vc;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 1440
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1441
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/vc;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1443
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 1444
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/vc;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1446
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 1447
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/r/b/a/vc;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 1449
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/vc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1450
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1417
    iget-byte v1, p0, Lcom/google/r/b/a/vc;->j:B

    .line 1418
    if-ne v1, v0, :cond_0

    .line 1422
    :goto_0
    return v0

    .line 1419
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1421
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/vc;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1454
    iget v0, p0, Lcom/google/r/b/a/vc;->k:I

    .line 1455
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1488
    :goto_0
    return v0

    .line 1458
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_b

    .line 1459
    iget-boolean v0, p0, Lcom/google/r/b/a/vc;->b:Z

    .line 1460
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1462
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 1463
    iget-object v2, p0, Lcom/google/r/b/a/vc;->c:Lcom/google/n/f;

    .line 1464
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v5

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1466
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 1467
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/r/b/a/vc;->d:I

    .line 1468
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1470
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_3

    .line 1471
    iget v2, p0, Lcom/google/r/b/a/vc;->e:I

    .line 1472
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1474
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_4

    .line 1475
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/r/b/a/vc;->f:I

    .line 1476
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1478
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_6

    .line 1479
    const/4 v2, 0x6

    iget v4, p0, Lcom/google/r/b/a/vc;->g:I

    .line 1480
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_5
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1482
    :cond_6
    iget v2, p0, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    .line 1483
    const/4 v2, 0x7

    iget-wide v4, p0, Lcom/google/r/b/a/vc;->h:J

    .line 1484
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1486
    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/vc;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1487
    iput v0, p0, Lcom/google/r/b/a/vc;->k:I

    goto/16 :goto_0

    :cond_8
    move v2, v3

    .line 1468
    goto :goto_2

    :cond_9
    move v2, v3

    .line 1472
    goto :goto_3

    :cond_a
    move v2, v3

    .line 1476
    goto :goto_4

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1207
    invoke-static {}, Lcom/google/r/b/a/vc;->newBuilder()Lcom/google/r/b/a/ve;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ve;->a(Lcom/google/r/b/a/vc;)Lcom/google/r/b/a/ve;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1207
    invoke-static {}, Lcom/google/r/b/a/vc;->newBuilder()Lcom/google/r/b/a/ve;

    move-result-object v0

    return-object v0
.end method
