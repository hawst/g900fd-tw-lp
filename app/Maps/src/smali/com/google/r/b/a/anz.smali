.class public final enum Lcom/google/r/b/a/anz;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/anz;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/anz;

.field public static final enum b:Lcom/google/r/b/a/anz;

.field public static final enum c:Lcom/google/r/b/a/anz;

.field public static final enum d:Lcom/google/r/b/a/anz;

.field public static final enum e:Lcom/google/r/b/a/anz;

.field private static final synthetic g:[Lcom/google/r/b/a/anz;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1387
    new-instance v0, Lcom/google/r/b/a/anz;

    const-string v1, "INCIDENT_ROAD_CLOSED"

    invoke-direct {v0, v1, v6, v3}, Lcom/google/r/b/a/anz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/anz;->a:Lcom/google/r/b/a/anz;

    .line 1391
    new-instance v0, Lcom/google/r/b/a/anz;

    const-string v1, "INCIDENT_ACCIDENT"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/r/b/a/anz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/anz;->b:Lcom/google/r/b/a/anz;

    .line 1395
    new-instance v0, Lcom/google/r/b/a/anz;

    const-string v1, "INCIDENT_CONSTRUCTION"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/r/b/a/anz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/anz;->c:Lcom/google/r/b/a/anz;

    .line 1399
    new-instance v0, Lcom/google/r/b/a/anz;

    const-string v1, "INCIDENT_JAM"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v5, v2}, Lcom/google/r/b/a/anz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/anz;->d:Lcom/google/r/b/a/anz;

    .line 1403
    new-instance v0, Lcom/google/r/b/a/anz;

    const-string v1, "INCIDENT_OTHER"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v7, v2}, Lcom/google/r/b/a/anz;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/anz;->e:Lcom/google/r/b/a/anz;

    .line 1382
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/r/b/a/anz;

    sget-object v1, Lcom/google/r/b/a/anz;->a:Lcom/google/r/b/a/anz;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/anz;->b:Lcom/google/r/b/a/anz;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/anz;->c:Lcom/google/r/b/a/anz;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/anz;->d:Lcom/google/r/b/a/anz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/anz;->e:Lcom/google/r/b/a/anz;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/r/b/a/anz;->g:[Lcom/google/r/b/a/anz;

    .line 1448
    new-instance v0, Lcom/google/r/b/a/aoa;

    invoke-direct {v0}, Lcom/google/r/b/a/aoa;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1457
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1458
    iput p3, p0, Lcom/google/r/b/a/anz;->f:I

    .line 1459
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/anz;
    .locals 1

    .prologue
    .line 1433
    sparse-switch p0, :sswitch_data_0

    .line 1439
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1434
    :sswitch_0
    sget-object v0, Lcom/google/r/b/a/anz;->a:Lcom/google/r/b/a/anz;

    goto :goto_0

    .line 1435
    :sswitch_1
    sget-object v0, Lcom/google/r/b/a/anz;->b:Lcom/google/r/b/a/anz;

    goto :goto_0

    .line 1436
    :sswitch_2
    sget-object v0, Lcom/google/r/b/a/anz;->c:Lcom/google/r/b/a/anz;

    goto :goto_0

    .line 1437
    :sswitch_3
    sget-object v0, Lcom/google/r/b/a/anz;->d:Lcom/google/r/b/a/anz;

    goto :goto_0

    .line 1438
    :sswitch_4
    sget-object v0, Lcom/google/r/b/a/anz;->e:Lcom/google/r/b/a/anz;

    goto :goto_0

    .line 1433
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x10 -> :sswitch_3
        0x7f -> :sswitch_4
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/anz;
    .locals 1

    .prologue
    .line 1382
    const-class v0, Lcom/google/r/b/a/anz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anz;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/anz;
    .locals 1

    .prologue
    .line 1382
    sget-object v0, Lcom/google/r/b/a/anz;->g:[Lcom/google/r/b/a/anz;

    invoke-virtual {v0}, [Lcom/google/r/b/a/anz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/anz;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1429
    iget v0, p0, Lcom/google/r/b/a/anz;->f:I

    return v0
.end method
