.class public final Lcom/google/r/b/a/apd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/apg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/apd;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/r/b/a/apd;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:J

.field e:Ljava/lang/Object;

.field f:Lcom/google/n/ao;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/ao;

.field l:Lcom/google/n/ao;

.field m:Lcom/google/n/ao;

.field n:Lcom/google/n/ao;

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcom/google/r/b/a/ape;

    invoke-direct {v0}, Lcom/google/r/b/a/ape;-><init>()V

    sput-object v0, Lcom/google/r/b/a/apd;->PARSER:Lcom/google/n/ax;

    .line 820
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/apd;->r:Lcom/google/n/aw;

    .line 2040
    new-instance v0, Lcom/google/r/b/a/apd;

    invoke-direct {v0}, Lcom/google/r/b/a/apd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/apd;->o:Lcom/google/r/b/a/apd;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 384
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    .line 500
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    .line 559
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    .line 575
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->i:Lcom/google/n/ao;

    .line 591
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->j:Lcom/google/n/ao;

    .line 607
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->k:Lcom/google/n/ao;

    .line 623
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->l:Lcom/google/n/ao;

    .line 639
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    .line 655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->n:Lcom/google/n/ao;

    .line 670
    iput-byte v1, p0, Lcom/google/r/b/a/apd;->p:B

    .line 755
    iput v1, p0, Lcom/google/r/b/a/apd;->q:I

    .line 18
    iget-object v0, p0, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/apd;->d:J

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/apd;->e:Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/r/b/a/apd;->i:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/r/b/a/apd;->j:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/r/b/a/apd;->k:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iget-object v0, p0, Lcom/google/r/b/a/apd;->l:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 29
    iget-object v0, p0, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 30
    iget-object v0, p0, Lcom/google/r/b/a/apd;->n:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v8, 0x20

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/r/b/a/apd;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 43
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 44
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 45
    sparse-switch v4, :sswitch_data_0

    .line 50
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 52
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 48
    goto :goto_0

    .line 57
    :sswitch_1
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_1

    .line 58
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    .line 60
    or-int/lit8 v1, v1, 0x2

    .line 62
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 62
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_2

    .line 141
    iget-object v2, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    .line 143
    :cond_2
    and-int/lit8 v1, v1, 0x20

    if-ne v1, v8, :cond_3

    .line 144
    iget-object v1, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    .line 146
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/apd;->au:Lcom/google/n/bn;

    throw v0

    .line 67
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/apd;->d:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 136
    :catch_1
    move-exception v0

    .line 137
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 138
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 72
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 73
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    goto :goto_0

    .line 77
    :sswitch_4
    iget-object v4, p0, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 78
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    goto/16 :goto_0

    .line 82
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 83
    iget v5, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/apd;->a:I

    .line 84
    iput-object v4, p0, Lcom/google/r/b/a/apd;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 88
    :sswitch_6
    and-int/lit8 v4, v1, 0x20

    if-eq v4, v8, :cond_4

    .line 89
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    .line 91
    or-int/lit8 v1, v1, 0x20

    .line 93
    :cond_4
    iget-object v4, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 93
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 98
    :sswitch_7
    iget-object v4, p0, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 99
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    goto/16 :goto_0

    .line 103
    :sswitch_8
    iget-object v4, p0, Lcom/google/r/b/a/apd;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 104
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    goto/16 :goto_0

    .line 108
    :sswitch_9
    iget-object v4, p0, Lcom/google/r/b/a/apd;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 109
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    goto/16 :goto_0

    .line 113
    :sswitch_a
    iget-object v4, p0, Lcom/google/r/b/a/apd;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 114
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    goto/16 :goto_0

    .line 118
    :sswitch_b
    iget-object v4, p0, Lcom/google/r/b/a/apd;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 119
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    goto/16 :goto_0

    .line 123
    :sswitch_c
    iget-object v4, p0, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 124
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I

    goto/16 :goto_0

    .line 128
    :sswitch_d
    iget-object v4, p0, Lcom/google/r/b/a/apd;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 129
    iget v4, p0, Lcom/google/r/b/a/apd;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/r/b/a/apd;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 140
    :cond_5
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_6

    .line 141
    iget-object v0, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    .line 143
    :cond_6
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v8, :cond_7

    .line 144
    iget-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    .line 146
    :cond_7
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apd;->au:Lcom/google/n/bn;

    .line 147
    return-void

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x82 -> :sswitch_8
        0x8a -> :sswitch_9
        0x92 -> :sswitch_a
        0x9a -> :sswitch_b
        0xa2 -> :sswitch_c
        0xaa -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 384
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    .line 500
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    .line 559
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    .line 575
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->i:Lcom/google/n/ao;

    .line 591
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->j:Lcom/google/n/ao;

    .line 607
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->k:Lcom/google/n/ao;

    .line 623
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->l:Lcom/google/n/ao;

    .line 639
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    .line 655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apd;->n:Lcom/google/n/ao;

    .line 670
    iput-byte v1, p0, Lcom/google/r/b/a/apd;->p:B

    .line 755
    iput v1, p0, Lcom/google/r/b/a/apd;->q:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/apd;
    .locals 1

    .prologue
    .line 2043
    sget-object v0, Lcom/google/r/b/a/apd;->o:Lcom/google/r/b/a/apd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/apf;
    .locals 1

    .prologue
    .line 882
    new-instance v0, Lcom/google/r/b/a/apf;

    invoke-direct {v0}, Lcom/google/r/b/a/apf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/apd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    sget-object v0, Lcom/google/r/b/a/apd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 712
    invoke-virtual {p0}, Lcom/google/r/b/a/apd;->c()I

    move v1, v2

    .line 713
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 713
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 716
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 717
    iget-wide v0, p0, Lcom/google/r/b/a/apd;->d:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 719
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 720
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 722
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 723
    iget-object v0, p0, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 725
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 726
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/apd;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apd;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 728
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 729
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 728
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 726
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 731
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_7

    .line 732
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 734
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 735
    iget-object v0, p0, Lcom/google/r/b/a/apd;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 737
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 738
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/r/b/a/apd;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 740
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 741
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/r/b/a/apd;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 743
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 744
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/r/b/a/apd;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 746
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_c

    .line 747
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 749
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_d

    .line 750
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/r/b/a/apd;->n:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 752
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/apd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 753
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 672
    iget-byte v0, p0, Lcom/google/r/b/a/apd;->p:B

    .line 673
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 707
    :cond_0
    :goto_0
    return v2

    .line 674
    :cond_1
    if-eqz v0, :cond_0

    .line 676
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 677
    iget-object v0, p0, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/ba;->d()Lcom/google/b/f/b/a/ba;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ba;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/ba;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 678
    iput-byte v2, p0, Lcom/google/r/b/a/apd;->p:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 676
    goto :goto_1

    .line 682
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 683
    iget-object v0, p0, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 684
    iput-byte v2, p0, Lcom/google/r/b/a/apd;->p:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 682
    goto :goto_2

    :cond_5
    move v1, v2

    .line 688
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 689
    iget-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 690
    iput-byte v2, p0, Lcom/google/r/b/a/apd;->p:B

    goto :goto_0

    .line 688
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 694
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_9

    .line 695
    iget-object v0, p0, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/ad;->d()Lcom/google/b/f/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ad;

    invoke-virtual {v0}, Lcom/google/b/f/ad;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 696
    iput-byte v2, p0, Lcom/google/r/b/a/apd;->p:B

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 694
    goto :goto_4

    .line 700
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    move v0, v3

    :goto_5
    if-eqz v0, :cond_b

    .line 701
    iget-object v0, p0, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aqm;->d()Lcom/google/r/b/a/aqm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aqm;

    invoke-virtual {v0}, Lcom/google/r/b/a/aqm;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 702
    iput-byte v2, p0, Lcom/google/r/b/a/apd;->p:B

    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 700
    goto :goto_5

    .line 706
    :cond_b
    iput-byte v3, p0, Lcom/google/r/b/a/apd;->p:B

    move v2, v3

    .line 707
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 757
    iget v0, p0, Lcom/google/r/b/a/apd;->q:I

    .line 758
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 815
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 761
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    .line 763
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 761
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 765
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    .line 766
    iget-wide v0, p0, Lcom/google/r/b/a/apd;->d:J

    .line 767
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0, v1}, Lcom/google/n/l;->b(J)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 769
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 770
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    .line 771
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 773
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_4

    .line 774
    iget-object v0, p0, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    .line 775
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 777
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_5

    .line 778
    const/4 v1, 0x5

    .line 779
    iget-object v0, p0, Lcom/google/r/b/a/apd;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apd;->e:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_5
    move v1, v2

    .line 781
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 782
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    .line 783
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 781
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 779
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 785
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v8, :cond_8

    .line 786
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    .line 787
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 789
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 790
    iget-object v0, p0, Lcom/google/r/b/a/apd;->i:Lcom/google/n/ao;

    .line 791
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 793
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    .line 794
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/r/b/a/apd;->j:Lcom/google/n/ao;

    .line 795
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 797
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_b

    .line 798
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/r/b/a/apd;->k:Lcom/google/n/ao;

    .line 799
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 801
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_c

    .line 802
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/r/b/a/apd;->l:Lcom/google/n/ao;

    .line 803
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 805
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_d

    .line 806
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    .line 807
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 809
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 810
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/r/b/a/apd;->n:Lcom/google/n/ao;

    .line 811
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 813
    :cond_e
    iget-object v0, p0, Lcom/google/r/b/a/apd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 814
    iput v0, p0, Lcom/google/r/b/a/apd;->q:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/apd;->newBuilder()Lcom/google/r/b/a/apf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/apf;->a(Lcom/google/r/b/a/apd;)Lcom/google/r/b/a/apf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/apd;->newBuilder()Lcom/google/r/b/a/apf;

    move-result-object v0

    return-object v0
.end method
