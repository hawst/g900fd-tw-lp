.class public final Lcom/google/r/b/a/agd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/agk;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/agd;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/agd;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field d:Z

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/n/ao;

.field g:Z

.field h:Z

.field i:Z

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/google/r/b/a/age;

    invoke-direct {v0}, Lcom/google/r/b/a/age;-><init>()V

    sput-object v0, Lcom/google/r/b/a/agd;->PARSER:Lcom/google/n/ax;

    .line 364
    new-instance v0, Lcom/google/r/b/a/agf;

    invoke-direct {v0}, Lcom/google/r/b/a/agf;-><init>()V

    .line 410
    new-instance v0, Lcom/google/r/b/a/agg;

    invoke-direct {v0}, Lcom/google/r/b/a/agg;-><init>()V

    .line 600
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/agd;->m:Lcom/google/n/aw;

    .line 1209
    new-instance v0, Lcom/google/r/b/a/agd;

    invoke-direct {v0}, Lcom/google/r/b/a/agd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/agd;->j:Lcom/google/r/b/a/agd;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 346
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    .line 439
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    .line 499
    iput-byte v3, p0, Lcom/google/r/b/a/agd;->k:B

    .line 545
    iput v3, p0, Lcom/google/r/b/a/agd;->l:I

    .line 107
    iget-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 108
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    .line 109
    iput-boolean v2, p0, Lcom/google/r/b/a/agd;->d:Z

    .line 110
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    .line 111
    iget-object v0, p0, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 112
    iput-boolean v2, p0, Lcom/google/r/b/a/agd;->g:Z

    .line 113
    iput-boolean v2, p0, Lcom/google/r/b/a/agd;->h:Z

    .line 114
    iput-boolean v2, p0, Lcom/google/r/b/a/agd;->i:Z

    .line 115
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/16 v9, 0x8

    const/4 v0, 0x0

    const/4 v8, 0x2

    .line 121
    invoke-direct {p0}, Lcom/google/r/b/a/agd;-><init>()V

    .line 124
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 127
    :cond_0
    :goto_0
    if-nez v3, :cond_f

    .line 128
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 129
    sparse-switch v0, :sswitch_data_0

    .line 134
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 136
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 132
    goto :goto_0

    .line 141
    :sswitch_1
    iget-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 142
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/agd;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 239
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v8, :cond_1

    .line 245
    iget-object v2, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    .line 247
    :cond_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v9, :cond_2

    .line 248
    iget-object v1, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    .line 250
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agd;->au:Lcom/google/n/bn;

    throw v0

    .line 146
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 147
    invoke-static {v0}, Lcom/google/r/b/a/acy;->a(I)Lcom/google/r/b/a/acy;

    move-result-object v6

    .line 148
    if-nez v6, :cond_3

    .line 149
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 240
    :catch_1
    move-exception v0

    .line 241
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 242
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 151
    :cond_3
    and-int/lit8 v6, v1, 0x2

    if-eq v6, v8, :cond_4

    .line 152
    :try_start_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    .line 153
    or-int/lit8 v1, v1, 0x2

    .line 155
    :cond_4
    iget-object v6, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 160
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 161
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 162
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_5

    move v0, v2

    :goto_2
    if-lez v0, :cond_8

    .line 163
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 164
    invoke-static {v0}, Lcom/google/r/b/a/acy;->a(I)Lcom/google/r/b/a/acy;

    move-result-object v7

    .line 165
    if-nez v7, :cond_6

    .line 166
    const/4 v7, 0x2

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 162
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_2

    .line 168
    :cond_6
    and-int/lit8 v7, v1, 0x2

    if-eq v7, v8, :cond_7

    .line 169
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    .line 170
    or-int/lit8 v1, v1, 0x2

    .line 172
    :cond_7
    iget-object v7, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 175
    :cond_8
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 179
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/agd;->a:I

    .line 180
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/agd;->d:Z

    goto/16 :goto_0

    .line 184
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 185
    invoke-static {v0}, Lcom/google/r/b/a/agi;->a(I)Lcom/google/r/b/a/agi;

    move-result-object v6

    .line 186
    if-nez v6, :cond_9

    .line 187
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 189
    :cond_9
    and-int/lit8 v6, v1, 0x8

    if-eq v6, v9, :cond_a

    .line 190
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    .line 191
    or-int/lit8 v1, v1, 0x8

    .line 193
    :cond_a
    iget-object v6, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 198
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 199
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 200
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_b

    move v0, v2

    :goto_4
    if-lez v0, :cond_e

    .line 201
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 202
    invoke-static {v0}, Lcom/google/r/b/a/agi;->a(I)Lcom/google/r/b/a/agi;

    move-result-object v7

    .line 203
    if-nez v7, :cond_c

    .line 204
    const/4 v7, 0x4

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_3

    .line 200
    :cond_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 206
    :cond_c
    and-int/lit8 v7, v1, 0x8

    if-eq v7, v9, :cond_d

    .line 207
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    .line 208
    or-int/lit8 v1, v1, 0x8

    .line 210
    :cond_d
    iget-object v7, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 213
    :cond_e
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 217
    :sswitch_7
    iget-object v0, p0, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 218
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/agd;->a:I

    goto/16 :goto_0

    .line 222
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/agd;->a:I

    .line 223
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/agd;->g:Z

    goto/16 :goto_0

    .line 227
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/agd;->a:I

    .line 228
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/agd;->i:Z

    goto/16 :goto_0

    .line 232
    :sswitch_a
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/agd;->a:I

    .line 233
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/agd;->h:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 244
    :cond_f
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v8, :cond_10

    .line 245
    iget-object v0, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    .line 247
    :cond_10
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v9, :cond_11

    .line 248
    iget-object v0, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    .line 250
    :cond_11
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agd;->au:Lcom/google/n/bn;

    .line 251
    return-void

    .line 129
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x20 -> :sswitch_5
        0x22 -> :sswitch_6
        0x32 -> :sswitch_7
        0x38 -> :sswitch_8
        0x40 -> :sswitch_9
        0x48 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 104
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 346
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    .line 439
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    .line 499
    iput-byte v1, p0, Lcom/google/r/b/a/agd;->k:B

    .line 545
    iput v1, p0, Lcom/google/r/b/a/agd;->l:I

    .line 105
    return-void
.end method

.method public static d()Lcom/google/r/b/a/agd;
    .locals 1

    .prologue
    .line 1212
    sget-object v0, Lcom/google/r/b/a/agd;->j:Lcom/google/r/b/a/agd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/agh;
    .locals 1

    .prologue
    .line 662
    new-instance v0, Lcom/google/r/b/a/agh;

    invoke-direct {v0}, Lcom/google/r/b/a/agh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/agd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    sget-object v0, Lcom/google/r/b/a/agd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 517
    invoke-virtual {p0}, Lcom/google/r/b/a/agd;->c()I

    .line 518
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 519
    iget-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 521
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 522
    iget-object v0, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 521
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 524
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 525
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/agd;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 527
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 528
    iget-object v0, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(II)V

    .line 527
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 530
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_4

    .line 531
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 533
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_5

    .line 534
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/r/b/a/agd;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 536
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 537
    iget-boolean v0, p0, Lcom/google/r/b/a/agd;->i:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(IZ)V

    .line 539
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 540
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/agd;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 542
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/agd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 543
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 501
    iget-byte v0, p0, Lcom/google/r/b/a/agd;->k:B

    .line 502
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 512
    :goto_0
    return v0

    .line 503
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 505
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 506
    iget-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afj;->g()Lcom/google/r/b/a/afj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afj;

    invoke-virtual {v0}, Lcom/google/r/b/a/afj;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 507
    iput-byte v2, p0, Lcom/google/r/b/a/agd;->k:B

    move v0, v2

    .line 508
    goto :goto_0

    :cond_2
    move v0, v2

    .line 505
    goto :goto_1

    .line 511
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/agd;->k:B

    move v0, v1

    .line 512
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v5, 0xa

    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 547
    iget v0, p0, Lcom/google/r/b/a/agd;->l:I

    .line 548
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 595
    :goto_0
    return v0

    .line 551
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 552
    iget-object v0, p0, Lcom/google/r/b/a/agd;->b:Lcom/google/n/ao;

    .line 553
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    :goto_1
    move v3, v2

    move v4, v2

    .line 557
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    .line 559
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v4, v0

    .line 557
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    move v0, v5

    .line 559
    goto :goto_3

    .line 561
    :cond_2
    add-int v0, v1, v4

    .line 562
    iget-object v1, p0, Lcom/google/r/b/a/agd;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 564
    iget v1, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_9

    .line 565
    const/4 v1, 0x3

    iget-boolean v3, p0, Lcom/google/r/b/a/agd;->d:Z

    .line 566
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v0

    :goto_4
    move v3, v2

    move v4, v2

    .line 570
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 571
    iget-object v0, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    .line 572
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v4, v0

    .line 570
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_3
    move v0, v5

    .line 572
    goto :goto_6

    .line 574
    :cond_4
    add-int v0, v1, v4

    .line 575
    iget-object v1, p0, Lcom/google/r/b/a/agd;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 577
    iget v1, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_5

    .line 578
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/r/b/a/agd;->f:Lcom/google/n/ao;

    .line 579
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 581
    :cond_5
    iget v1, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_6

    .line 582
    const/4 v1, 0x7

    iget-boolean v3, p0, Lcom/google/r/b/a/agd;->g:Z

    .line 583
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 585
    :cond_6
    iget v1, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_7

    .line 586
    iget-boolean v1, p0, Lcom/google/r/b/a/agd;->i:Z

    .line 587
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 589
    :cond_7
    iget v1, p0, Lcom/google/r/b/a/agd;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_8

    .line 590
    const/16 v1, 0x9

    iget-boolean v3, p0, Lcom/google/r/b/a/agd;->h:Z

    .line 591
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 593
    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/agd;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 594
    iput v0, p0, Lcom/google/r/b/a/agd;->l:I

    goto/16 :goto_0

    :cond_9
    move v1, v0

    goto/16 :goto_4

    :cond_a
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Lcom/google/r/b/a/agd;->newBuilder()Lcom/google/r/b/a/agh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/agh;->a(Lcom/google/r/b/a/agd;)Lcom/google/r/b/a/agh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Lcom/google/r/b/a/agd;->newBuilder()Lcom/google/r/b/a/agh;

    move-result-object v0

    return-object v0
.end method
