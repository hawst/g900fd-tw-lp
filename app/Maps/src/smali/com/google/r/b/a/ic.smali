.class public final Lcom/google/r/b/a/ic;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/id;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ia;",
        "Lcom/google/r/b/a/ic;",
        ">;",
        "Lcom/google/r/b/a/id;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 270
    sget-object v0, Lcom/google/r/b/a/ia;->d:Lcom/google/r/b/a/ia;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 271
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/r/b/a/ic;->c()Lcom/google/r/b/a/ia;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 262
    check-cast p1, Lcom/google/r/b/a/ia;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ic;->a(Lcom/google/r/b/a/ia;)Lcom/google/r/b/a/ic;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ia;)Lcom/google/r/b/a/ic;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 302
    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 310
    :goto_0
    return-object p0

    .line 303
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ia;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 304
    iget v2, p1, Lcom/google/r/b/a/ia;->b:I

    iget v3, p0, Lcom/google/r/b/a/ic;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ic;->a:I

    iput v2, p0, Lcom/google/r/b/a/ic;->b:I

    .line 306
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ia;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 307
    iget v0, p1, Lcom/google/r/b/a/ia;->c:I

    iget v1, p0, Lcom/google/r/b/a/ic;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/ic;->a:I

    iput v0, p0, Lcom/google/r/b/a/ic;->c:I

    .line 309
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/ia;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 303
    goto :goto_1

    :cond_4
    move v0, v1

    .line 306
    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 314
    iget v2, p0, Lcom/google/r/b/a/ic;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 322
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 314
    goto :goto_0

    .line 318
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/ic;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    .line 322
    goto :goto_1

    :cond_3
    move v2, v0

    .line 318
    goto :goto_2
.end method

.method public final c()Lcom/google/r/b/a/ia;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 286
    new-instance v2, Lcom/google/r/b/a/ia;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ia;-><init>(Lcom/google/n/v;)V

    .line 287
    iget v3, p0, Lcom/google/r/b/a/ic;->a:I

    .line 288
    const/4 v1, 0x0

    .line 289
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 292
    :goto_0
    iget v1, p0, Lcom/google/r/b/a/ic;->b:I

    iput v1, v2, Lcom/google/r/b/a/ia;->b:I

    .line 293
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 294
    or-int/lit8 v0, v0, 0x2

    .line 296
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/ic;->c:I

    iput v1, v2, Lcom/google/r/b/a/ia;->c:I

    .line 297
    iput v0, v2, Lcom/google/r/b/a/ia;->a:I

    .line 298
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
