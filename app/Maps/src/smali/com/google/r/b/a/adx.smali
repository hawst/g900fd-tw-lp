.class public final Lcom/google/r/b/a/adx;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aea;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/adv;",
        "Lcom/google/r/b/a/adx;",
        ">;",
        "Lcom/google/r/b/a/aea;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/nc;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2224
    sget-object v0, Lcom/google/r/b/a/adv;->d:Lcom/google/r/b/a/adv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2282
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    .line 2406
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/adx;->c:I

    .line 2225
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2216
    new-instance v2, Lcom/google/r/b/a/adv;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/adv;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/adx;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/r/b/a/adx;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/adx;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/adx;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/adx;->c:I

    iput v1, v2, Lcom/google/r/b/a/adv;->c:I

    iput v0, v2, Lcom/google/r/b/a/adv;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2216
    check-cast p1, Lcom/google/r/b/a/adv;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/adx;->a(Lcom/google/r/b/a/adv;)Lcom/google/r/b/a/adx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/adv;)Lcom/google/r/b/a/adx;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2257
    invoke-static {}, Lcom/google/r/b/a/adv;->d()Lcom/google/r/b/a/adv;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 2272
    :goto_0
    return-object p0

    .line 2258
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2259
    iget-object v1, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2260
    iget-object v1, p1, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    .line 2261
    iget v1, p0, Lcom/google/r/b/a/adx;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/adx;->a:I

    .line 2268
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/r/b/a/adv;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_2
    if-eqz v0, :cond_7

    .line 2269
    iget v0, p1, Lcom/google/r/b/a/adv;->c:I

    invoke-static {v0}, Lcom/google/r/b/a/ady;->a(I)Lcom/google/r/b/a/ady;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/r/b/a/ady;->a:Lcom/google/r/b/a/ady;

    :cond_2
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2263
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/adx;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/adx;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/adx;->a:I

    .line 2264
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/adx;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2268
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 2269
    :cond_6
    iget v1, p0, Lcom/google/r/b/a/adx;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/adx;->a:I

    iget v0, v0, Lcom/google/r/b/a/ady;->d:I

    iput v0, p0, Lcom/google/r/b/a/adx;->c:I

    .line 2271
    :cond_7
    iget-object v0, p1, Lcom/google/r/b/a/adv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2276
    const/4 v0, 0x1

    return v0
.end method
