.class public final Lcom/google/r/b/a/afd;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/afb;",
        "Lcom/google/r/b/a/afd;",
        ">;",
        "Lcom/google/r/b/a/afi;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/hu;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aex;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/n/f;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/jm;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field private i:I

.field private j:Lcom/google/r/b/a/afz;

.field private k:Lcom/google/r/b/a/afe;

.field private l:Z

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/fw;",
            ">;"
        }
    .end annotation
.end field

.field private n:Z

.field private o:Ljava/lang/Object;

.field private p:Ljava/lang/Object;

.field private q:Lcom/google/maps/c/a/a;

.field private r:Lcom/google/maps/g/a/is;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11254
    sget-object v0, Lcom/google/r/b/a/afb;->s:Lcom/google/r/b/a/afb;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 11481
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    .line 11606
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    .line 11826
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/afd;->i:I

    .line 11862
    iput-object v1, p0, Lcom/google/r/b/a/afd;->j:Lcom/google/r/b/a/afz;

    .line 11923
    iput-object v1, p0, Lcom/google/r/b/a/afd;->k:Lcom/google/r/b/a/afe;

    .line 12017
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    .line 12141
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/afd;->f:Lcom/google/n/f;

    .line 12177
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    .line 12333
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afd;->o:Ljava/lang/Object;

    .line 12409
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afd;->p:Ljava/lang/Object;

    .line 12485
    iput-object v1, p0, Lcom/google/r/b/a/afd;->q:Lcom/google/maps/c/a/a;

    .line 12546
    iput-object v1, p0, Lcom/google/r/b/a/afd;->r:Lcom/google/maps/g/a/is;

    .line 11255
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 11246
    invoke-virtual {p0}, Lcom/google/r/b/a/afd;->c()Lcom/google/r/b/a/afb;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 11246
    check-cast p1, Lcom/google/r/b/a/afb;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/afd;->a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;
    .locals 8

    .prologue
    const/16 v7, 0x40

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 11380
    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 11465
    :goto_0
    return-object p0

    .line 11381
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 11382
    iget-object v0, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 11383
    iget-object v0, p1, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    .line 11384
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11391
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 11392
    iget-object v0, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 11393
    iget-object v0, p1, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    .line 11394
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11401
    :cond_2
    :goto_2
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 11402
    iget v0, p1, Lcom/google/r/b/a/afb;->d:I

    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/afd;->a:I

    iput v0, p0, Lcom/google/r/b/a/afd;->c:I

    .line 11404
    :cond_3
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_4
    if-eqz v0, :cond_4

    .line 11405
    iget v0, p1, Lcom/google/r/b/a/afb;->e:I

    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/afd;->a:I

    iput v0, p0, Lcom/google/r/b/a/afd;->d:I

    .line 11407
    :cond_4
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_5
    if-eqz v0, :cond_5

    .line 11408
    iget v0, p1, Lcom/google/r/b/a/afb;->f:I

    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/afd;->a:I

    iput v0, p0, Lcom/google/r/b/a/afd;->h:I

    .line 11410
    :cond_5
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_d

    move v0, v1

    :goto_6
    if-eqz v0, :cond_f

    .line 11411
    iget v0, p1, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_6
    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11386
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v1, :cond_8

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11387
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 11396
    :cond_9
    invoke-virtual {p0}, Lcom/google/r/b/a/afd;->d()V

    .line 11397
    iget-object v0, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_a
    move v0, v2

    .line 11401
    goto :goto_3

    :cond_b
    move v0, v2

    .line 11404
    goto :goto_4

    :cond_c
    move v0, v2

    .line 11407
    goto :goto_5

    :cond_d
    move v0, v2

    .line 11410
    goto :goto_6

    .line 11411
    :cond_e
    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/afd;->a:I

    iget v0, v0, Lcom/google/maps/g/a/z;->h:I

    iput v0, p0, Lcom/google/r/b/a/afd;->i:I

    .line 11413
    :cond_f
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_14

    move v0, v1

    :goto_7
    if-eqz v0, :cond_10

    .line 11414
    iget-object v0, p1, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    if-nez v0, :cond_15

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v0

    :goto_8
    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v3, v3, 0x40

    if-ne v3, v7, :cond_16

    iget-object v3, p0, Lcom/google/r/b/a/afd;->j:Lcom/google/r/b/a/afz;

    if-eqz v3, :cond_16

    iget-object v3, p0, Lcom/google/r/b/a/afd;->j:Lcom/google/r/b/a/afz;

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v4

    if-eq v3, v4, :cond_16

    iget-object v3, p0, Lcom/google/r/b/a/afd;->j:Lcom/google/r/b/a/afz;

    invoke-static {v3}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/agb;->c()Lcom/google/r/b/a/afz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afd;->j:Lcom/google/r/b/a/afz;

    :goto_9
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11416
    :cond_10
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_17

    move v0, v1

    :goto_a
    if-eqz v0, :cond_11

    .line 11417
    iget-object v0, p1, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    if-nez v0, :cond_18

    invoke-static {}, Lcom/google/r/b/a/afe;->d()Lcom/google/r/b/a/afe;

    move-result-object v0

    :goto_b
    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_19

    iget-object v3, p0, Lcom/google/r/b/a/afd;->k:Lcom/google/r/b/a/afe;

    if-eqz v3, :cond_19

    iget-object v3, p0, Lcom/google/r/b/a/afd;->k:Lcom/google/r/b/a/afe;

    invoke-static {}, Lcom/google/r/b/a/afe;->d()Lcom/google/r/b/a/afe;

    move-result-object v4

    if-eq v3, v4, :cond_19

    iget-object v3, p0, Lcom/google/r/b/a/afd;->k:Lcom/google/r/b/a/afe;

    invoke-static {v3}, Lcom/google/r/b/a/afe;->a(Lcom/google/r/b/a/afe;)Lcom/google/r/b/a/afg;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/afg;->a(Lcom/google/r/b/a/afe;)Lcom/google/r/b/a/afg;

    move-result-object v0

    new-instance v3, Lcom/google/r/b/a/afe;

    invoke-direct {v3, v0}, Lcom/google/r/b/a/afe;-><init>(Lcom/google/n/v;)V

    iput-object v3, p0, Lcom/google/r/b/a/afd;->k:Lcom/google/r/b/a/afe;

    :goto_c
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11419
    :cond_11
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x40

    if-ne v0, v7, :cond_1a

    move v0, v1

    :goto_d
    if-eqz v0, :cond_12

    .line 11420
    iget-boolean v0, p1, Lcom/google/r/b/a/afb;->j:Z

    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/afd;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/afd;->l:Z

    .line 11422
    :cond_12
    iget-object v0, p1, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 11423
    iget-object v0, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 11424
    iget-object v0, p1, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    .line 11425
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11432
    :cond_13
    :goto_e
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_1c

    move v0, v1

    :goto_f
    if-eqz v0, :cond_1e

    .line 11433
    iget-object v0, p1, Lcom/google/r/b/a/afb;->l:Lcom/google/n/f;

    if-nez v0, :cond_1d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    move v0, v2

    .line 11413
    goto/16 :goto_7

    .line 11414
    :cond_15
    iget-object v0, p1, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    goto/16 :goto_8

    :cond_16
    iput-object v0, p0, Lcom/google/r/b/a/afd;->j:Lcom/google/r/b/a/afz;

    goto/16 :goto_9

    :cond_17
    move v0, v2

    .line 11416
    goto/16 :goto_a

    .line 11417
    :cond_18
    iget-object v0, p1, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    goto :goto_b

    :cond_19
    iput-object v0, p0, Lcom/google/r/b/a/afd;->k:Lcom/google/r/b/a/afe;

    goto :goto_c

    :cond_1a
    move v0, v2

    .line 11419
    goto :goto_d

    .line 11427
    :cond_1b
    invoke-virtual {p0}, Lcom/google/r/b/a/afd;->i()V

    .line 11428
    iget-object v0, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_e

    :cond_1c
    move v0, v2

    .line 11432
    goto :goto_f

    .line 11433
    :cond_1d
    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/afd;->a:I

    iput-object v0, p0, Lcom/google/r/b/a/afd;->f:Lcom/google/n/f;

    .line 11435
    :cond_1e
    iget-object v0, p1, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 11436
    iget-object v0, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 11437
    iget-object v0, p1, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    .line 11438
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11445
    :cond_1f
    :goto_10
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_27

    move v0, v1

    :goto_11
    if-eqz v0, :cond_20

    .line 11446
    iget-boolean v0, p1, Lcom/google/r/b/a/afb;->n:Z

    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/r/b/a/afd;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/afd;->n:Z

    .line 11448
    :cond_20
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_28

    move v0, v1

    :goto_12
    if-eqz v0, :cond_21

    .line 11449
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11450
    iget-object v0, p1, Lcom/google/r/b/a/afb;->o:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/afd;->o:Ljava/lang/Object;

    .line 11453
    :cond_21
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_29

    move v0, v1

    :goto_13
    if-eqz v0, :cond_22

    .line 11454
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11455
    iget-object v0, p1, Lcom/google/r/b/a/afb;->p:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/afd;->p:Ljava/lang/Object;

    .line 11458
    :cond_22
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_2a

    move v0, v1

    :goto_14
    if-eqz v0, :cond_23

    .line 11459
    iget-object v0, p1, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    if-nez v0, :cond_2b

    invoke-static {}, Lcom/google/maps/c/a/a;->d()Lcom/google/maps/c/a/a;

    move-result-object v0

    :goto_15
    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/2addr v3, v5

    if-ne v3, v5, :cond_2c

    iget-object v3, p0, Lcom/google/r/b/a/afd;->q:Lcom/google/maps/c/a/a;

    if-eqz v3, :cond_2c

    iget-object v3, p0, Lcom/google/r/b/a/afd;->q:Lcom/google/maps/c/a/a;

    invoke-static {}, Lcom/google/maps/c/a/a;->d()Lcom/google/maps/c/a/a;

    move-result-object v4

    if-eq v3, v4, :cond_2c

    iget-object v3, p0, Lcom/google/r/b/a/afd;->q:Lcom/google/maps/c/a/a;

    invoke-static {v3}, Lcom/google/maps/c/a/a;->a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/c/a/c;->a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/c/a/c;->c()Lcom/google/maps/c/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afd;->q:Lcom/google/maps/c/a/a;

    :goto_16
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11461
    :cond_23
    iget v0, p1, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_2d

    move v0, v1

    :goto_17
    if-eqz v0, :cond_24

    .line 11462
    iget-object v0, p1, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    if-nez v0, :cond_2e

    invoke-static {}, Lcom/google/maps/g/a/is;->d()Lcom/google/maps/g/a/is;

    move-result-object v0

    :goto_18
    iget v1, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/2addr v1, v6

    if-ne v1, v6, :cond_2f

    iget-object v1, p0, Lcom/google/r/b/a/afd;->r:Lcom/google/maps/g/a/is;

    if-eqz v1, :cond_2f

    iget-object v1, p0, Lcom/google/r/b/a/afd;->r:Lcom/google/maps/g/a/is;

    invoke-static {}, Lcom/google/maps/g/a/is;->d()Lcom/google/maps/g/a/is;

    move-result-object v2

    if-eq v1, v2, :cond_2f

    iget-object v1, p0, Lcom/google/r/b/a/afd;->r:Lcom/google/maps/g/a/is;

    invoke-static {v1}, Lcom/google/maps/g/a/is;->a(Lcom/google/maps/g/a/is;)Lcom/google/maps/g/a/iu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/a/iu;->a(Lcom/google/maps/g/a/is;)Lcom/google/maps/g/a/iu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/a/iu;->c()Lcom/google/maps/g/a/is;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afd;->r:Lcom/google/maps/g/a/is;

    :goto_19
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11464
    :cond_24
    iget-object v0, p1, Lcom/google/r/b/a/afb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 11440
    :cond_25
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-eq v0, v3, :cond_26

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11441
    :cond_26
    iget-object v0, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_10

    :cond_27
    move v0, v2

    .line 11445
    goto/16 :goto_11

    :cond_28
    move v0, v2

    .line 11448
    goto/16 :goto_12

    :cond_29
    move v0, v2

    .line 11453
    goto/16 :goto_13

    :cond_2a
    move v0, v2

    .line 11458
    goto/16 :goto_14

    .line 11459
    :cond_2b
    iget-object v0, p1, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    goto/16 :goto_15

    :cond_2c
    iput-object v0, p0, Lcom/google/r/b/a/afd;->q:Lcom/google/maps/c/a/a;

    goto :goto_16

    :cond_2d
    move v0, v2

    .line 11461
    goto :goto_17

    .line 11462
    :cond_2e
    iget-object v0, p1, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    goto :goto_18

    :cond_2f
    iput-object v0, p0, Lcom/google/r/b/a/afd;->r:Lcom/google/maps/g/a/is;

    goto :goto_19
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11469
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 11470
    iget-object v0, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-virtual {v0}, Lcom/google/maps/g/a/hu;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 11475
    :goto_1
    return v2

    .line 11469
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 11475
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final c()Lcom/google/r/b/a/afb;
    .locals 8

    .prologue
    const/high16 v7, 0x10000

    const v6, 0x8000

    const/4 v0, 0x1

    .line 11300
    new-instance v2, Lcom/google/r/b/a/afb;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/afb;-><init>(Lcom/google/n/v;)V

    .line 11301
    iget v3, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11302
    const/4 v1, 0x0

    .line 11303
    iget v4, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 11304
    iget-object v4, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    .line 11305
    iget v4, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11307
    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/afd;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    .line 11308
    iget v4, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 11309
    iget-object v4, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    .line 11310
    iget v4, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11312
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    .line 11313
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_10

    .line 11316
    :goto_0
    iget v1, p0, Lcom/google/r/b/a/afd;->c:I

    iput v1, v2, Lcom/google/r/b/a/afb;->d:I

    .line 11317
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 11318
    or-int/lit8 v0, v0, 0x2

    .line 11320
    :cond_2
    iget v1, p0, Lcom/google/r/b/a/afd;->d:I

    iput v1, v2, Lcom/google/r/b/a/afb;->e:I

    .line 11321
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 11322
    or-int/lit8 v0, v0, 0x4

    .line 11324
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/afd;->h:I

    iput v1, v2, Lcom/google/r/b/a/afb;->f:I

    .line 11325
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 11326
    or-int/lit8 v0, v0, 0x8

    .line 11328
    :cond_4
    iget v1, p0, Lcom/google/r/b/a/afd;->i:I

    iput v1, v2, Lcom/google/r/b/a/afb;->g:I

    .line 11329
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 11330
    or-int/lit8 v0, v0, 0x10

    .line 11332
    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/afd;->j:Lcom/google/r/b/a/afz;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    .line 11333
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 11334
    or-int/lit8 v0, v0, 0x20

    .line 11336
    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/afd;->k:Lcom/google/r/b/a/afe;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    .line 11337
    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 11338
    or-int/lit8 v0, v0, 0x40

    .line 11340
    :cond_7
    iget-boolean v1, p0, Lcom/google/r/b/a/afd;->l:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/afb;->j:Z

    .line 11341
    iget v1, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 11342
    iget-object v1, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    .line 11343
    iget v1, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11345
    :cond_8
    iget-object v1, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    .line 11346
    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    .line 11347
    or-int/lit16 v0, v0, 0x80

    .line 11349
    :cond_9
    iget-object v1, p0, Lcom/google/r/b/a/afd;->f:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->l:Lcom/google/n/f;

    .line 11350
    iget v1, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    .line 11351
    iget-object v1, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    .line 11352
    iget v1, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v1, v1, -0x801

    iput v1, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11354
    :cond_a
    iget-object v1, p0, Lcom/google/r/b/a/afd;->m:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    .line 11355
    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    .line 11356
    or-int/lit16 v0, v0, 0x100

    .line 11358
    :cond_b
    iget-boolean v1, p0, Lcom/google/r/b/a/afd;->n:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/afb;->n:Z

    .line 11359
    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    .line 11360
    or-int/lit16 v0, v0, 0x200

    .line 11362
    :cond_c
    iget-object v1, p0, Lcom/google/r/b/a/afd;->o:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->o:Ljava/lang/Object;

    .line 11363
    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    .line 11364
    or-int/lit16 v0, v0, 0x400

    .line 11366
    :cond_d
    iget-object v1, p0, Lcom/google/r/b/a/afd;->p:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->p:Ljava/lang/Object;

    .line 11367
    and-int v1, v3, v6

    if-ne v1, v6, :cond_e

    .line 11368
    or-int/lit16 v0, v0, 0x800

    .line 11370
    :cond_e
    iget-object v1, p0, Lcom/google/r/b/a/afd;->q:Lcom/google/maps/c/a/a;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    .line 11371
    and-int v1, v3, v7

    if-ne v1, v7, :cond_f

    .line 11372
    or-int/lit16 v0, v0, 0x1000

    .line 11374
    :cond_f
    iget-object v1, p0, Lcom/google/r/b/a/afd;->r:Lcom/google/maps/g/a/is;

    iput-object v1, v2, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    .line 11375
    iput v0, v2, Lcom/google/r/b/a/afb;->a:I

    .line 11376
    return-object v2

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 11608
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 11609
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    .line 11610
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 11612
    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 12019
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    .line 12020
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    .line 12021
    iget v0, p0, Lcom/google/r/b/a/afd;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/afd;->a:I

    .line 12023
    :cond_0
    return-void
.end method
