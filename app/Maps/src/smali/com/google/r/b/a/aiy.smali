.class public final enum Lcom/google/r/b/a/aiy;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/aiy;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/aiy;

.field public static final enum b:Lcom/google/r/b/a/aiy;

.field public static final enum c:Lcom/google/r/b/a/aiy;

.field private static final synthetic e:[Lcom/google/r/b/a/aiy;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 19
    new-instance v0, Lcom/google/r/b/a/aiy;

    const-string v1, "OUTDOOR_PANO"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/r/b/a/aiy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aiy;->a:Lcom/google/r/b/a/aiy;

    .line 23
    new-instance v0, Lcom/google/r/b/a/aiy;

    const-string v1, "INDOOR_PANO"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/aiy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aiy;->b:Lcom/google/r/b/a/aiy;

    .line 27
    new-instance v0, Lcom/google/r/b/a/aiy;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/r/b/a/aiy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/aiy;->c:Lcom/google/r/b/a/aiy;

    .line 14
    new-array v0, v5, [Lcom/google/r/b/a/aiy;

    sget-object v1, Lcom/google/r/b/a/aiy;->a:Lcom/google/r/b/a/aiy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/aiy;->b:Lcom/google/r/b/a/aiy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/aiy;->c:Lcom/google/r/b/a/aiy;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/r/b/a/aiy;->e:[Lcom/google/r/b/a/aiy;

    .line 62
    new-instance v0, Lcom/google/r/b/a/aiz;

    invoke-direct {v0}, Lcom/google/r/b/a/aiz;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput p3, p0, Lcom/google/r/b/a/aiy;->d:I

    .line 73
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/aiy;
    .locals 1

    .prologue
    .line 49
    packed-switch p0, :pswitch_data_0

    .line 53
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 50
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/aiy;->a:Lcom/google/r/b/a/aiy;

    goto :goto_0

    .line 51
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/aiy;->b:Lcom/google/r/b/a/aiy;

    goto :goto_0

    .line 52
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/aiy;->c:Lcom/google/r/b/a/aiy;

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/aiy;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/r/b/a/aiy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aiy;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/aiy;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/r/b/a/aiy;->e:[Lcom/google/r/b/a/aiy;

    invoke-virtual {v0}, [Lcom/google/r/b/a/aiy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/aiy;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/r/b/a/aiy;->d:I

    return v0
.end method
