.class public final Lcom/google/r/b/a/oc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/od;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/oa;",
        "Lcom/google/r/b/a/oc;",
        ">;",
        "Lcom/google/r/b/a/od;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 468
    sget-object v0, Lcom/google/r/b/a/oa;->h:Lcom/google/r/b/a/oa;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 692
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    .line 828
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    .line 895
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    .line 469
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 460
    new-instance v2, Lcom/google/r/b/a/oa;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/oa;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/oc;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/oc;->b:I

    iput v1, v2, Lcom/google/r/b/a/oa;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/oc;->c:I

    iput v1, v2, Lcom/google/r/b/a/oa;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/oc;->d:I

    iput v1, v2, Lcom/google/r/b/a/oa;->d:I

    iget v1, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/r/b/a/oc;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/r/b/a/oc;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/r/b/a/oc;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/oa;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 460
    check-cast p1, Lcom/google/r/b/a/oa;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/oc;->a(Lcom/google/r/b/a/oa;)Lcom/google/r/b/a/oc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/oa;)Lcom/google/r/b/a/oc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 527
    invoke-static {}, Lcom/google/r/b/a/oa;->d()Lcom/google/r/b/a/oa;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 568
    :goto_0
    return-object p0

    .line 528
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 529
    iget v2, p1, Lcom/google/r/b/a/oa;->b:I

    iget v3, p0, Lcom/google/r/b/a/oc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/oc;->a:I

    iput v2, p0, Lcom/google/r/b/a/oc;->b:I

    .line 531
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 532
    iget v2, p1, Lcom/google/r/b/a/oa;->c:I

    iget v3, p0, Lcom/google/r/b/a/oc;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/oc;->a:I

    iput v2, p0, Lcom/google/r/b/a/oc;->c:I

    .line 534
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/oa;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    :goto_3
    if-eqz v0, :cond_3

    .line 535
    iget v0, p1, Lcom/google/r/b/a/oa;->d:I

    iget v1, p0, Lcom/google/r/b/a/oc;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/oc;->a:I

    iput v0, p0, Lcom/google/r/b/a/oc;->d:I

    .line 537
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 538
    iget-object v0, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 539
    iget-object v0, p1, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    .line 540
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/r/b/a/oc;->a:I

    .line 547
    :cond_4
    :goto_4
    iget-object v0, p1, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 548
    iget-object v0, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 549
    iget-object v0, p1, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    .line 550
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/r/b/a/oc;->a:I

    .line 557
    :cond_5
    :goto_5
    iget-object v0, p1, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 558
    iget-object v0, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 559
    iget-object v0, p1, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    .line 560
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/r/b/a/oc;->a:I

    .line 567
    :cond_6
    :goto_6
    iget-object v0, p1, Lcom/google/r/b/a/oa;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 528
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 531
    goto :goto_2

    :cond_9
    move v0, v1

    .line 534
    goto :goto_3

    .line 542
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_b

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/oc;->a:I

    .line 543
    :cond_b
    iget-object v0, p0, Lcom/google/r/b/a/oc;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/oa;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 552
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/oc;->a:I

    .line 553
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/oc;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    .line 562
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_f

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/oc;->a:I

    .line 563
    :cond_f
    iget-object v0, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/oa;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 572
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 590
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 572
    goto :goto_0

    .line 576
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    move v0, v3

    :goto_2
    if-eqz v0, :cond_0

    .line 580
    iget v0, p0, Lcom/google/r/b/a/oc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_3
    if-eqz v0, :cond_0

    move v1, v2

    .line 584
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 585
    iget-object v0, p0, Lcom/google/r/b/a/oc;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/nw;->d()Lcom/google/r/b/a/nw;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/nw;

    invoke-virtual {v0}, Lcom/google/r/b/a/nw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_3
    move v0, v2

    .line 576
    goto :goto_2

    :cond_4
    move v0, v2

    .line 580
    goto :goto_3

    :cond_5
    move v2, v3

    .line 590
    goto :goto_1
.end method
