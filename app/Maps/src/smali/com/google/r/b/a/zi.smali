.class public final Lcom/google/r/b/a/zi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/zi;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/zi;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:I

.field public c:I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 634
    new-instance v0, Lcom/google/r/b/a/zj;

    invoke-direct {v0}, Lcom/google/r/b/a/zj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/zi;->PARSER:Lcom/google/n/ax;

    .line 779
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/zi;->i:Lcom/google/n/aw;

    .line 1100
    new-instance v0, Lcom/google/r/b/a/zi;

    invoke-direct {v0}, Lcom/google/r/b/a/zi;-><init>()V

    sput-object v0, Lcom/google/r/b/a/zi;->f:Lcom/google/r/b/a/zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 554
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 717
    iput-byte v0, p0, Lcom/google/r/b/a/zi;->g:B

    .line 745
    iput v0, p0, Lcom/google/r/b/a/zi;->h:I

    .line 555
    iput v0, p0, Lcom/google/r/b/a/zi;->b:I

    .line 556
    iput v1, p0, Lcom/google/r/b/a/zi;->c:I

    .line 557
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    .line 558
    iput v1, p0, Lcom/google/r/b/a/zi;->e:I

    .line 559
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const v9, 0x7fffffff

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v8, 0x4

    .line 565
    invoke-direct {p0}, Lcom/google/r/b/a/zi;-><init>()V

    .line 568
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 571
    :cond_0
    :goto_0
    if-nez v3, :cond_7

    .line 572
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 573
    sparse-switch v0, :sswitch_data_0

    .line 578
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 580
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 576
    goto :goto_0

    .line 585
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/zi;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/zi;->a:I

    .line 586
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zi;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 622
    :catch_0
    move-exception v0

    .line 623
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 628
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_1

    .line 629
    iget-object v1, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    .line 631
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/zi;->au:Lcom/google/n/bn;

    throw v0

    .line 590
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/zi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/zi;->a:I

    .line 591
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zi;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 624
    :catch_1
    move-exception v0

    .line 625
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 626
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 595
    :sswitch_3
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_2

    .line 596
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    .line 597
    or-int/lit8 v1, v1, 0x4

    .line 599
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 603
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 604
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 605
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_3

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_4

    move v0, v2

    :goto_1
    if-lez v0, :cond_3

    .line 606
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    .line 607
    or-int/lit8 v1, v1, 0x4

    .line 609
    :cond_3
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_5

    move v0, v2

    :goto_3
    if-lez v0, :cond_6

    .line 610
    iget-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 605
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 609
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 612
    :cond_6
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 616
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/zi;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/zi;->a:I

    .line 617
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zi;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 628
    :cond_7
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v8, :cond_8

    .line 629
    iget-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    .line 631
    :cond_8
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zi;->au:Lcom/google/n/bn;

    .line 632
    return-void

    .line 573
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 552
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 717
    iput-byte v0, p0, Lcom/google/r/b/a/zi;->g:B

    .line 745
    iput v0, p0, Lcom/google/r/b/a/zi;->h:I

    .line 553
    return-void
.end method

.method public static d()Lcom/google/r/b/a/zi;
    .locals 1

    .prologue
    .line 1103
    sget-object v0, Lcom/google/r/b/a/zi;->f:Lcom/google/r/b/a/zi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/zk;
    .locals 1

    .prologue
    .line 841
    new-instance v0, Lcom/google/r/b/a/zk;

    invoke-direct {v0}, Lcom/google/r/b/a/zk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/zi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 646
    sget-object v0, Lcom/google/r/b/a/zi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 729
    invoke-virtual {p0}, Lcom/google/r/b/a/zi;->c()I

    .line 730
    iget v0, p0, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 731
    iget v0, p0, Lcom/google/r/b/a/zi;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 733
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 734
    iget v0, p0, Lcom/google/r/b/a/zi;->c:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_1
    move v1, v2

    .line 736
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 737
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 736
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 739
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 740
    iget v0, p0, Lcom/google/r/b/a/zi;->e:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 742
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/zi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 743
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 719
    iget-byte v1, p0, Lcom/google/r/b/a/zi;->g:B

    .line 720
    if-ne v1, v0, :cond_0

    .line 724
    :goto_0
    return v0

    .line 721
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 723
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/zi;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v1, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 747
    iget v0, p0, Lcom/google/r/b/a/zi;->h:I

    .line 748
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 774
    :goto_0
    return v0

    .line 751
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 752
    iget v0, p0, Lcom/google/r/b/a/zi;->b:I

    .line 753
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 755
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_5

    .line 756
    iget v3, p0, Lcom/google/r/b/a/zi;->c:I

    .line 757
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    :goto_3
    move v4, v2

    move v5, v2

    .line 761
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 762
    iget-object v0, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    .line 763
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v5, v0

    .line 761
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_1
    move v0, v1

    .line 753
    goto :goto_1

    .line 765
    :cond_2
    add-int v0, v3, v5

    .line 766
    iget-object v3, p0, Lcom/google/r/b/a/zi;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 768
    iget v3, p0, Lcom/google/r/b/a/zi;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_4

    .line 769
    iget v3, p0, Lcom/google/r/b/a/zi;->e:I

    .line 770
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 772
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/zi;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 773
    iput v0, p0, Lcom/google/r/b/a/zi;->h:I

    goto :goto_0

    :cond_5
    move v3, v0

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 546
    invoke-static {}, Lcom/google/r/b/a/zi;->newBuilder()Lcom/google/r/b/a/zk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/zk;->a(Lcom/google/r/b/a/zi;)Lcom/google/r/b/a/zk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 546
    invoke-static {}, Lcom/google/r/b/a/zi;->newBuilder()Lcom/google/r/b/a/zk;

    move-result-object v0

    return-object v0
.end method
