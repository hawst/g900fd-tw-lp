.class public final Lcom/google/r/b/a/lj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/lk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/lh;",
        "Lcom/google/r/b/a/lj;",
        ">;",
        "Lcom/google/r/b/a/lk;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5969
    sget-object v0, Lcom/google/r/b/a/lh;->e:Lcom/google/r/b/a/lh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6048
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lj;->b:Lcom/google/n/ao;

    .line 6107
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lj;->c:Lcom/google/n/ao;

    .line 6166
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/lj;->d:Lcom/google/n/ao;

    .line 5970
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5961
    new-instance v2, Lcom/google/r/b/a/lh;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/lh;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/lj;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/lj;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/lj;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/lj;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/lj;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, v2, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/lj;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/lj;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/lh;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5961
    check-cast p1, Lcom/google/r/b/a/lh;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/lj;->a(Lcom/google/r/b/a/lh;)Lcom/google/r/b/a/lj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/lh;)Lcom/google/r/b/a/lj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6013
    invoke-static {}, Lcom/google/r/b/a/lh;->d()Lcom/google/r/b/a/lh;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6027
    :goto_0
    return-object p0

    .line 6014
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 6015
    iget-object v2, p0, Lcom/google/r/b/a/lj;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/lh;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6016
    iget v2, p0, Lcom/google/r/b/a/lj;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/lj;->a:I

    .line 6018
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 6019
    iget-object v2, p0, Lcom/google/r/b/a/lj;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/lh;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6020
    iget v2, p0, Lcom/google/r/b/a/lj;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/lj;->a:I

    .line 6022
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/lh;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 6023
    iget-object v0, p0, Lcom/google/r/b/a/lj;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/lh;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6024
    iget v0, p0, Lcom/google/r/b/a/lj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/lj;->a:I

    .line 6026
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/lh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 6014
    goto :goto_1

    :cond_5
    move v2, v1

    .line 6018
    goto :goto_2

    :cond_6
    move v0, v1

    .line 6022
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 6031
    iget v0, p0, Lcom/google/r/b/a/lj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 6032
    iget-object v0, p0, Lcom/google/r/b/a/lj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/be;->d()Lcom/google/r/b/a/be;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 6043
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 6031
    goto :goto_0

    .line 6037
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/lj;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 6038
    iget-object v0, p0, Lcom/google/r/b/a/lj;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/lt;->d()Lcom/google/r/b/a/lt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/lt;

    invoke-virtual {v0}, Lcom/google/r/b/a/lt;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 6040
    goto :goto_1

    :cond_2
    move v0, v1

    .line 6037
    goto :goto_2

    :cond_3
    move v0, v2

    .line 6043
    goto :goto_1
.end method
