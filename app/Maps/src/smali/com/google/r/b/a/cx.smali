.class public final Lcom/google/r/b/a/cx;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/dc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/cv;",
        "Lcom/google/r/b/a/cx;",
        ">;",
        "Lcom/google/r/b/a/dc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:I

.field private g:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6028
    sget-object v0, Lcom/google/r/b/a/cv;->h:Lcom/google/r/b/a/cv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6134
    iput v1, p0, Lcom/google/r/b/a/cx;->b:I

    .line 6170
    iput v1, p0, Lcom/google/r/b/a/cx;->c:I

    .line 6206
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cx;->d:Lcom/google/n/ao;

    .line 6265
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cx;->e:Lcom/google/n/ao;

    .line 6324
    iput v1, p0, Lcom/google/r/b/a/cx;->f:I

    .line 6360
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cx;->g:Lcom/google/n/ao;

    .line 6029
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6020
    new-instance v2, Lcom/google/r/b/a/cv;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/cv;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/cx;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/cx;->b:I

    iput v4, v2, Lcom/google/r/b/a/cv;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/r/b/a/cx;->c:I

    iput v4, v2, Lcom/google/r/b/a/cv;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/cv;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/cx;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/cx;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/cx;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/cx;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/r/b/a/cx;->f:I

    iput v4, v2, Lcom/google/r/b/a/cv;->f:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v3, v2, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/cx;->g:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/cx;->g:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/cv;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 6020
    check-cast p1, Lcom/google/r/b/a/cv;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/cx;->a(Lcom/google/r/b/a/cv;)Lcom/google/r/b/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/cv;)Lcom/google/r/b/a/cx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6090
    invoke-static {}, Lcom/google/r/b/a/cv;->d()Lcom/google/r/b/a/cv;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6113
    :goto_0
    return-object p0

    .line 6091
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 6092
    iget v2, p1, Lcom/google/r/b/a/cv;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/da;->a(I)Lcom/google/r/b/a/da;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/da;->a:Lcom/google/r/b/a/da;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 6091
    goto :goto_1

    .line 6092
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/cx;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/cx;->a:I

    iget v2, v2, Lcom/google/r/b/a/da;->c:I

    iput v2, p0, Lcom/google/r/b/a/cx;->b:I

    .line 6094
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 6095
    iget v2, p1, Lcom/google/r/b/a/cv;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/cy;->a(I)Lcom/google/r/b/a/cy;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/r/b/a/cy;->a:Lcom/google/r/b/a/cy;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 6094
    goto :goto_2

    .line 6095
    :cond_7
    iget v3, p0, Lcom/google/r/b/a/cx;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/cx;->a:I

    iget v2, v2, Lcom/google/r/b/a/cy;->e:I

    iput v2, p0, Lcom/google/r/b/a/cx;->c:I

    .line 6097
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 6098
    iget-object v2, p0, Lcom/google/r/b/a/cx;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/cv;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6099
    iget v2, p0, Lcom/google/r/b/a/cx;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/cx;->a:I

    .line 6101
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 6102
    iget-object v2, p0, Lcom/google/r/b/a/cx;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6103
    iget v2, p0, Lcom/google/r/b/a/cx;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/cx;->a:I

    .line 6105
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_10

    .line 6106
    iget v2, p1, Lcom/google/r/b/a/cv;->f:I

    invoke-static {v2}, Lcom/google/r/b/a/cs;->a(I)Lcom/google/r/b/a/cs;

    move-result-object v2

    if-nez v2, :cond_b

    sget-object v2, Lcom/google/r/b/a/cs;->a:Lcom/google/r/b/a/cs;

    :cond_b
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    move v2, v1

    .line 6097
    goto :goto_3

    :cond_d
    move v2, v1

    .line 6101
    goto :goto_4

    :cond_e
    move v2, v1

    .line 6105
    goto :goto_5

    .line 6106
    :cond_f
    iget v3, p0, Lcom/google/r/b/a/cx;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/cx;->a:I

    iget v2, v2, Lcom/google/r/b/a/cs;->c:I

    iput v2, p0, Lcom/google/r/b/a/cx;->f:I

    .line 6108
    :cond_10
    iget v2, p1, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_12

    :goto_6
    if-eqz v0, :cond_11

    .line 6109
    iget-object v0, p0, Lcom/google/r/b/a/cx;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6110
    iget v0, p0, Lcom/google/r/b/a/cx;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/cx;->a:I

    .line 6112
    :cond_11
    iget-object v0, p1, Lcom/google/r/b/a/cv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_12
    move v0, v1

    .line 6108
    goto :goto_6
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 6117
    iget v0, p0, Lcom/google/r/b/a/cx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 6118
    iget-object v0, p0, Lcom/google/r/b/a/cx;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 6129
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 6117
    goto :goto_0

    .line 6123
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/cx;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 6124
    iget-object v0, p0, Lcom/google/r/b/a/cx;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 6126
    goto :goto_1

    :cond_2
    move v0, v1

    .line 6123
    goto :goto_2

    :cond_3
    move v0, v2

    .line 6129
    goto :goto_1
.end method
