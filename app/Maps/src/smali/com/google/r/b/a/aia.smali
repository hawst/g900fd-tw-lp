.class public final Lcom/google/r/b/a/aia;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aif;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aia;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/aia;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:I

.field public c:Lcom/google/n/ao;

.field d:Lcom/google/n/aq;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 621
    new-instance v0, Lcom/google/r/b/a/aib;

    invoke-direct {v0}, Lcom/google/r/b/a/aib;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aia;->PARSER:Lcom/google/n/ax;

    .line 829
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aia;->h:Lcom/google/n/aw;

    .line 1170
    new-instance v0, Lcom/google/r/b/a/aia;

    invoke-direct {v0}, Lcom/google/r/b/a/aia;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aia;->e:Lcom/google/r/b/a/aia;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 553
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 730
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aia;->c:Lcom/google/n/ao;

    .line 774
    iput-byte v2, p0, Lcom/google/r/b/a/aia;->f:B

    .line 799
    iput v2, p0, Lcom/google/r/b/a/aia;->g:I

    .line 554
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aia;->b:I

    .line 555
    iget-object v0, p0, Lcom/google/r/b/a/aia;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 556
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    .line 557
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 563
    invoke-direct {p0}, Lcom/google/r/b/a/aia;-><init>()V

    .line 566
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 569
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 570
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 571
    sparse-switch v4, :sswitch_data_0

    .line 576
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 578
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 574
    goto :goto_0

    .line 583
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 584
    invoke-static {v4}, Lcom/google/r/b/a/aid;->a(I)Lcom/google/r/b/a/aid;

    move-result-object v5

    .line 585
    if-nez v5, :cond_2

    .line 586
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 609
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 610
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 615
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_1

    .line 616
    iget-object v1, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    .line 618
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aia;->au:Lcom/google/n/bn;

    throw v0

    .line 588
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/r/b/a/aia;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/aia;->a:I

    .line 589
    iput v4, p0, Lcom/google/r/b/a/aia;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 611
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 612
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 613
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 594
    :sswitch_2
    :try_start_4
    iget-object v4, p0, Lcom/google/r/b/a/aia;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 595
    iget v4, p0, Lcom/google/r/b/a/aia;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/aia;->a:I

    goto :goto_0

    .line 615
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 599
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 600
    and-int/lit8 v5, v0, 0x4

    if-eq v5, v6, :cond_3

    .line 601
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    .line 602
    or-int/lit8 v0, v0, 0x4

    .line 604
    :cond_3
    iget-object v5, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 615
    :cond_4
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_5

    .line 616
    iget-object v0, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    .line 618
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aia;->au:Lcom/google/n/bn;

    .line 619
    return-void

    .line 571
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 551
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 730
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aia;->c:Lcom/google/n/ao;

    .line 774
    iput-byte v1, p0, Lcom/google/r/b/a/aia;->f:B

    .line 799
    iput v1, p0, Lcom/google/r/b/a/aia;->g:I

    .line 552
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aia;
    .locals 1

    .prologue
    .line 1173
    sget-object v0, Lcom/google/r/b/a/aia;->e:Lcom/google/r/b/a/aia;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aic;
    .locals 1

    .prologue
    .line 891
    new-instance v0, Lcom/google/r/b/a/aic;

    invoke-direct {v0}, Lcom/google/r/b/a/aic;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aia;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633
    sget-object v0, Lcom/google/r/b/a/aia;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 786
    invoke-virtual {p0}, Lcom/google/r/b/a/aia;->c()I

    .line 787
    iget v0, p0, Lcom/google/r/b/a/aia;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 788
    iget v0, p0, Lcom/google/r/b/a/aia;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 790
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aia;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 791
    iget-object v0, p0, Lcom/google/r/b/a/aia;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 793
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 794
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 793
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 796
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/aia;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 797
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 776
    iget-byte v1, p0, Lcom/google/r/b/a/aia;->f:B

    .line 777
    if-ne v1, v0, :cond_0

    .line 781
    :goto_0
    return v0

    .line 778
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 780
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aia;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 801
    iget v0, p0, Lcom/google/r/b/a/aia;->g:I

    .line 802
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 824
    :goto_0
    return v0

    .line 805
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aia;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 806
    iget v0, p0, Lcom/google/r/b/a/aia;->b:I

    .line 807
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 809
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/aia;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 810
    iget-object v2, p0, Lcom/google/r/b/a/aia;->c:Lcom/google/n/ao;

    .line 811
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    .line 815
    :goto_3
    iget-object v3, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 816
    iget-object v3, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    .line 817
    invoke-interface {v3, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 815
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 807
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 819
    :cond_3
    add-int/2addr v0, v2

    .line 820
    iget-object v1, p0, Lcom/google/r/b/a/aia;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 822
    iget-object v1, p0, Lcom/google/r/b/a/aia;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 823
    iput v0, p0, Lcom/google/r/b/a/aia;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 545
    invoke-static {}, Lcom/google/r/b/a/aia;->newBuilder()Lcom/google/r/b/a/aic;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aic;->a(Lcom/google/r/b/a/aia;)Lcom/google/r/b/a/aic;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 545
    invoke-static {}, Lcom/google/r/b/a/aia;->newBuilder()Lcom/google/r/b/a/aic;

    move-result-object v0

    return-object v0
.end method
