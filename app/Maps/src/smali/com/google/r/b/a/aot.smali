.class public final Lcom/google/r/b/a/aot;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aou;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aor;",
        "Lcom/google/r/b/a/aot;",
        ">;",
        "Lcom/google/r/b/a/aou;"
    }
.end annotation


# instance fields
.field public a:Lcom/google/n/aq;

.field private b:I

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 637
    sget-object v0, Lcom/google/r/b/a/aor;->d:Lcom/google/r/b/a/aor;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 697
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    .line 790
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aot;->c:Lcom/google/n/ao;

    .line 638
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 629
    new-instance v2, Lcom/google/r/b/a/aor;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aor;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aot;->b:I

    iget v4, p0, Lcom/google/r/b/a/aot;->b:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/r/b/a/aot;->b:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/aot;->b:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v3, v2, Lcom/google/r/b/a/aor;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/aot;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/aot;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/aor;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 629
    check-cast p1, Lcom/google/r/b/a/aor;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aot;->a(Lcom/google/r/b/a/aor;)Lcom/google/r/b/a/aot;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aor;)Lcom/google/r/b/a/aot;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 672
    invoke-static {}, Lcom/google/r/b/a/aor;->d()Lcom/google/r/b/a/aor;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 688
    :goto_0
    return-object p0

    .line 673
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 674
    iget-object v1, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 675
    iget-object v1, p1, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    iput-object v1, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    .line 676
    iget v1, p0, Lcom/google/r/b/a/aot;->b:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/aot;->b:I

    .line 683
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/r/b/a/aor;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 684
    iget-object v0, p0, Lcom/google/r/b/a/aot;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/aor;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 685
    iget v0, p0, Lcom/google/r/b/a/aot;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/aot;->b:I

    .line 687
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/aor;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 678
    :cond_3
    invoke-virtual {p0}, Lcom/google/r/b/a/aot;->c()V

    .line 679
    iget-object v1, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    iget-object v2, p1, Lcom/google/r/b/a/aor;->b:Lcom/google/n/aq;

    invoke-interface {v1, v2}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 683
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 692
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 699
    iget v0, p0, Lcom/google/r/b/a/aot;->b:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 700
    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/r/b/a/aot;->a:Lcom/google/n/aq;

    .line 701
    iget v0, p0, Lcom/google/r/b/a/aot;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/aot;->b:I

    .line 703
    :cond_0
    return-void
.end method
