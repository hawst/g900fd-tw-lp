.class public final Lcom/google/r/b/a/vi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/vg;",
        "Lcom/google/r/b/a/vi;",
        ">;",
        "Lcom/google/r/b/a/vj;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/f;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 303
    sget-object v0, Lcom/google/r/b/a/vg;->d:Lcom/google/r/b/a/vg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 360
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/vi;->b:Lcom/google/n/f;

    .line 396
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    .line 304
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 398
    iget v0, p0, Lcom/google/r/b/a/vi;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 399
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    .line 402
    iget v0, p0, Lcom/google/r/b/a/vi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/vi;->a:I

    .line 404
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 295
    new-instance v2, Lcom/google/r/b/a/vg;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/vg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/vi;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/vi;->b:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/vg;->b:Lcom/google/n/f;

    iget v1, p0, Lcom/google/r/b/a/vi;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/vi;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/vi;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/vg;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/vg;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 295
    check-cast p1, Lcom/google/r/b/a/vg;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/vi;->a(Lcom/google/r/b/a/vg;)Lcom/google/r/b/a/vi;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/vg;)Lcom/google/r/b/a/vi;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 336
    invoke-static {}, Lcom/google/r/b/a/vg;->d()Lcom/google/r/b/a/vg;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 351
    :goto_0
    return-object p0

    .line 337
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/vg;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_1

    :goto_1
    if-eqz v0, :cond_3

    .line 338
    iget-object v0, p1, Lcom/google/r/b/a/vg;->b:Lcom/google/n/f;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 337
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 338
    :cond_2
    iget v1, p0, Lcom/google/r/b/a/vi;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/vi;->a:I

    iput-object v0, p0, Lcom/google/r/b/a/vi;->b:Lcom/google/n/f;

    .line 340
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/vg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 341
    iget-object v0, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 342
    iget-object v0, p1, Lcom/google/r/b/a/vg;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    .line 343
    iget v0, p0, Lcom/google/r/b/a/vi;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/vi;->a:I

    .line 350
    :cond_4
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/vg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 345
    :cond_5
    invoke-direct {p0}, Lcom/google/r/b/a/vi;->c()V

    .line 346
    iget-object v0, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/vg;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/r/b/a/vi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/p/a/b;",
            ">;)",
            "Lcom/google/r/b/a/vi;"
        }
    .end annotation

    .prologue
    .line 506
    invoke-direct {p0}, Lcom/google/r/b/a/vi;->c()V

    .line 507
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 508
    iget-object v2, p0, Lcom/google/r/b/a/vi;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/google/n/ao;->a(Lcom/google/n/at;)Lcom/google/n/ao;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 511
    :cond_0
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 355
    const/4 v0, 0x1

    return v0
.end method
