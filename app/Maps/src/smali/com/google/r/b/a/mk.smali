.class public final Lcom/google/r/b/a/mk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/mv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/mi;",
        "Lcom/google/r/b/a/mk;",
        ">;",
        "Lcom/google/r/b/a/mv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/r/b/a/ay;

.field private c:Lcom/google/r/b/a/ay;

.field private d:Ljava/lang/Object;

.field private e:Lcom/google/n/aq;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Lcom/google/r/b/a/ml;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1859
    sget-object v0, Lcom/google/r/b/a/mi;->i:Lcom/google/r/b/a/mi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1989
    iput-object v1, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    .line 2050
    iput-object v1, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    .line 2111
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/mk;->d:Ljava/lang/Object;

    .line 2187
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    .line 2281
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    .line 2405
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/mk;->g:I

    .line 2441
    iput-object v1, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    .line 1860
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1851
    new-instance v2, Lcom/google/r/b/a/mi;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/mi;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/mk;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    iput-object v1, v2, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    iput-object v1, v2, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/mk;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/mi;->d:Ljava/lang/Object;

    iget v1, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/r/b/a/mk;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/r/b/a/mk;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget v1, p0, Lcom/google/r/b/a/mk;->g:I

    iput v1, v2, Lcom/google/r/b/a/mi;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    iput-object v1, v2, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    iput v0, v2, Lcom/google/r/b/a/mi;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1851
    check-cast p1, Lcom/google/r/b/a/mi;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/mk;->a(Lcom/google/r/b/a/mi;)Lcom/google/r/b/a/mk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/mi;)Lcom/google/r/b/a/mk;
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/16 v6, 0x8

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1923
    invoke-static {}, Lcom/google/r/b/a/mi;->d()Lcom/google/r/b/a/mi;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1962
    :goto_0
    return-object p0

    .line 1924
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1925
    iget-object v0, p1, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_9

    iget-object v3, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v4

    if-eq v3, v4, :cond_9

    iget-object v3, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    invoke-static {v3}, Lcom/google/r/b/a/ay;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/ba;->c()Lcom/google/r/b/a/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    :goto_3
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/mk;->a:I

    .line 1927
    :cond_1
    iget v0, p1, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_a

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 1928
    iget-object v0, p1, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_5
    iget v3, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_c

    iget-object v3, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v4

    if-eq v3, v4, :cond_c

    iget-object v3, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    invoke-static {v3}, Lcom/google/r/b/a/ay;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/ba;->a(Lcom/google/r/b/a/ay;)Lcom/google/r/b/a/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/ba;->c()Lcom/google/r/b/a/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    :goto_6
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/mk;->a:I

    .line 1930
    :cond_2
    iget v0, p1, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_d

    move v0, v1

    :goto_7
    if-eqz v0, :cond_3

    .line 1931
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/mk;->a:I

    .line 1932
    iget-object v0, p1, Lcom/google/r/b/a/mi;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/mk;->d:Ljava/lang/Object;

    .line 1935
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1936
    iget-object v0, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1937
    iget-object v0, p1, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    .line 1938
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/r/b/a/mk;->a:I

    .line 1945
    :cond_4
    :goto_8
    iget-object v0, p1, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1946
    iget-object v0, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1947
    iget-object v0, p1, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    .line 1948
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/r/b/a/mk;->a:I

    .line 1955
    :cond_5
    :goto_9
    iget v0, p1, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_12

    move v0, v1

    :goto_a
    if-eqz v0, :cond_14

    .line 1956
    iget v0, p1, Lcom/google/r/b/a/mi;->g:I

    invoke-static {v0}, Lcom/google/r/b/a/mt;->a(I)Lcom/google/r/b/a/mt;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/r/b/a/mt;->a:Lcom/google/r/b/a/mt;

    :cond_6
    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v0, v2

    .line 1924
    goto/16 :goto_1

    .line 1925
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/mi;->b:Lcom/google/r/b/a/ay;

    goto/16 :goto_2

    :cond_9
    iput-object v0, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    goto/16 :goto_3

    :cond_a
    move v0, v2

    .line 1927
    goto/16 :goto_4

    .line 1928
    :cond_b
    iget-object v0, p1, Lcom/google/r/b/a/mi;->c:Lcom/google/r/b/a/ay;

    goto/16 :goto_5

    :cond_c
    iput-object v0, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    goto :goto_6

    :cond_d
    move v0, v2

    .line 1930
    goto :goto_7

    .line 1940
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v0, v0, 0x8

    if-eq v0, v6, :cond_f

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/mk;->a:I

    .line 1941
    :cond_f
    iget-object v0, p0, Lcom/google/r/b/a/mk;->e:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/r/b/a/mi;->e:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    .line 1950
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v0, v0, 0x10

    if-eq v0, v7, :cond_11

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/mk;->a:I

    .line 1951
    :cond_11
    iget-object v0, p0, Lcom/google/r/b/a/mk;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/mi;->f:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9

    :cond_12
    move v0, v2

    .line 1955
    goto :goto_a

    .line 1956
    :cond_13
    iget v3, p0, Lcom/google/r/b/a/mk;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/mk;->a:I

    iget v0, v0, Lcom/google/r/b/a/mt;->e:I

    iput v0, p0, Lcom/google/r/b/a/mk;->g:I

    .line 1958
    :cond_14
    iget v0, p1, Lcom/google/r/b/a/mi;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_16

    move v0, v1

    :goto_b
    if-eqz v0, :cond_15

    .line 1959
    iget-object v0, p1, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    if-nez v0, :cond_17

    invoke-static {}, Lcom/google/r/b/a/ml;->d()Lcom/google/r/b/a/ml;

    move-result-object v0

    :goto_c
    iget v1, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_18

    iget-object v1, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    invoke-static {}, Lcom/google/r/b/a/ml;->d()Lcom/google/r/b/a/ml;

    move-result-object v2

    if-eq v1, v2, :cond_18

    iget-object v1, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    invoke-static {v1}, Lcom/google/r/b/a/ml;->a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/mn;->a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/mn;->c()Lcom/google/r/b/a/ml;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    :goto_d
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/mk;->a:I

    .line 1961
    :cond_15
    iget-object v0, p1, Lcom/google/r/b/a/mi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_16
    move v0, v2

    .line 1958
    goto :goto_b

    .line 1959
    :cond_17
    iget-object v0, p1, Lcom/google/r/b/a/mi;->h:Lcom/google/r/b/a/ml;

    goto :goto_c

    :cond_18
    iput-object v0, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    goto :goto_d
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1966
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 1967
    iget-object v0, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/r/b/a/ay;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1984
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 1966
    goto :goto_0

    .line 1967
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/mk;->b:Lcom/google/r/b/a/ay;

    goto :goto_1

    .line 1972
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 1973
    iget-object v0, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/r/b/a/ay;->d()Lcom/google/r/b/a/ay;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/r/b/a/ay;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 1975
    goto :goto_2

    :cond_3
    move v0, v1

    .line 1972
    goto :goto_3

    .line 1973
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/mk;->c:Lcom/google/r/b/a/ay;

    goto :goto_4

    .line 1978
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/mk;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_5
    if-eqz v0, :cond_8

    .line 1979
    iget-object v0, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/r/b/a/ml;->d()Lcom/google/r/b/a/ml;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, Lcom/google/r/b/a/ml;->b()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 1981
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1978
    goto :goto_5

    .line 1979
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/mk;->h:Lcom/google/r/b/a/ml;

    goto :goto_6

    :cond_8
    move v0, v2

    .line 1984
    goto :goto_2
.end method
