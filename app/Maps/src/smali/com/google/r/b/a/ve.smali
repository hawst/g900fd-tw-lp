.class public final Lcom/google/r/b/a/ve;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/vf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/vc;",
        "Lcom/google/r/b/a/ve;",
        ">;",
        "Lcom/google/r/b/a/vf;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Lcom/google/n/f;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:J


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x3e8

    .line 1573
    sget-object v0, Lcom/google/r/b/a/vc;->i:Lcom/google/r/b/a/vc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1699
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/ve;->c:Lcom/google/n/f;

    .line 1734
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/ve;->d:I

    .line 1766
    const/16 v0, 0x708

    iput v0, p0, Lcom/google/r/b/a/ve;->e:I

    .line 1798
    iput v1, p0, Lcom/google/r/b/a/ve;->f:I

    .line 1830
    iput v1, p0, Lcom/google/r/b/a/ve;->g:I

    .line 1862
    const-wide/16 v0, 0x3c

    iput-wide v0, p0, Lcom/google/r/b/a/ve;->h:J

    .line 1574
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1565
    new-instance v2, Lcom/google/r/b/a/vc;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/vc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ve;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/ve;->b:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/vc;->b:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ve;->c:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/vc;->c:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/ve;->d:I

    iput v1, v2, Lcom/google/r/b/a/vc;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/ve;->e:I

    iput v1, v2, Lcom/google/r/b/a/vc;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/r/b/a/ve;->f:I

    iput v1, v2, Lcom/google/r/b/a/vc;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/r/b/a/ve;->g:I

    iput v1, v2, Lcom/google/r/b/a/vc;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-wide v4, p0, Lcom/google/r/b/a/ve;->h:J

    iput-wide v4, v2, Lcom/google/r/b/a/vc;->h:J

    iput v0, v2, Lcom/google/r/b/a/vc;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1565
    check-cast p1, Lcom/google/r/b/a/vc;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ve;->a(Lcom/google/r/b/a/vc;)Lcom/google/r/b/a/ve;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/vc;)Lcom/google/r/b/a/ve;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1635
    invoke-static {}, Lcom/google/r/b/a/vc;->d()Lcom/google/r/b/a/vc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1658
    :goto_0
    return-object p0

    .line 1636
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1637
    iget-boolean v2, p1, Lcom/google/r/b/a/vc;->b:Z

    iget v3, p0, Lcom/google/r/b/a/ve;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ve;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/ve;->b:Z

    .line 1639
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1640
    iget-object v2, p1, Lcom/google/r/b/a/vc;->c:Lcom/google/n/f;

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1636
    goto :goto_1

    :cond_3
    move v2, v1

    .line 1639
    goto :goto_2

    .line 1640
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/ve;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ve;->a:I

    iput-object v2, p0, Lcom/google/r/b/a/ve;->c:Lcom/google/n/f;

    .line 1642
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 1643
    iget v2, p1, Lcom/google/r/b/a/vc;->d:I

    iget v3, p0, Lcom/google/r/b/a/ve;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/ve;->a:I

    iput v2, p0, Lcom/google/r/b/a/ve;->d:I

    .line 1645
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 1646
    iget v2, p1, Lcom/google/r/b/a/vc;->e:I

    iget v3, p0, Lcom/google/r/b/a/ve;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/ve;->a:I

    iput v2, p0, Lcom/google/r/b/a/ve;->e:I

    .line 1648
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 1649
    iget v2, p1, Lcom/google/r/b/a/vc;->f:I

    iget v3, p0, Lcom/google/r/b/a/ve;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/ve;->a:I

    iput v2, p0, Lcom/google/r/b/a/ve;->f:I

    .line 1651
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 1652
    iget v2, p1, Lcom/google/r/b/a/vc;->g:I

    iget v3, p0, Lcom/google/r/b/a/ve;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/ve;->a:I

    iput v2, p0, Lcom/google/r/b/a/ve;->g:I

    .line 1654
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/vc;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    :goto_7
    if-eqz v0, :cond_a

    .line 1655
    iget-wide v0, p1, Lcom/google/r/b/a/vc;->h:J

    iget v2, p0, Lcom/google/r/b/a/ve;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/ve;->a:I

    iput-wide v0, p0, Lcom/google/r/b/a/ve;->h:J

    .line 1657
    :cond_a
    iget-object v0, p1, Lcom/google/r/b/a/vc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 1642
    goto :goto_3

    :cond_c
    move v2, v1

    .line 1645
    goto :goto_4

    :cond_d
    move v2, v1

    .line 1648
    goto :goto_5

    :cond_e
    move v2, v1

    .line 1651
    goto :goto_6

    :cond_f
    move v0, v1

    .line 1654
    goto :goto_7
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1662
    const/4 v0, 0x1

    return v0
.end method
