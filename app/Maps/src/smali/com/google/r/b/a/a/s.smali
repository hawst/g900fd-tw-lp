.class public final Lcom/google/r/b/a/a/s;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/v;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/s;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/a/s;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/r/b/a/a/t;

    invoke-direct {v0}, Lcom/google/r/b/a/a/t;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/s;->PARSER:Lcom/google/n/ax;

    .line 222
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/s;->h:Lcom/google/n/aw;

    .line 550
    new-instance v0, Lcom/google/r/b/a/a/s;

    invoke-direct {v0}, Lcom/google/r/b/a/a/s;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/s;->e:Lcom/google/r/b/a/a/s;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 125
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    .line 141
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    .line 157
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/s;->d:Lcom/google/n/ao;

    .line 172
    iput-byte v3, p0, Lcom/google/r/b/a/a/s;->f:B

    .line 197
    iput v3, p0, Lcom/google/r/b/a/a/s;->g:I

    .line 54
    iget-object v0, p0, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 55
    iget-object v0, p0, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 56
    iget-object v0, p0, Lcom/google/r/b/a/a/s;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Lcom/google/r/b/a/a/s;-><init>()V

    .line 64
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 69
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 71
    sparse-switch v3, :sswitch_data_0

    .line 76
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 78
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 84
    iget v3, p0, Lcom/google/r/b/a/a/s;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/a/s;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/s;->au:Lcom/google/n/bn;

    throw v0

    .line 88
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 89
    iget v3, p0, Lcom/google/r/b/a/a/s;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/a/s;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 93
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/a/s;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 94
    iget v3, p0, Lcom/google/r/b/a/a/s;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/a/s;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/s;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 51
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 125
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    .line 141
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    .line 157
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/s;->d:Lcom/google/n/ao;

    .line 172
    iput-byte v1, p0, Lcom/google/r/b/a/a/s;->f:B

    .line 197
    iput v1, p0, Lcom/google/r/b/a/a/s;->g:I

    .line 52
    return-void
.end method

.method public static a([B)Lcom/google/r/b/a/a/s;
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lcom/google/r/b/a/a/s;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, p0}, Lcom/google/n/ax;->b([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/s;

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/a/s;
    .locals 1

    .prologue
    .line 553
    sget-object v0, Lcom/google/r/b/a/a/s;->e:Lcom/google/r/b/a/a/s;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/u;
    .locals 1

    .prologue
    .line 284
    new-instance v0, Lcom/google/r/b/a/a/u;

    invoke-direct {v0}, Lcom/google/r/b/a/a/u;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/r/b/a/a/s;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 184
    invoke-virtual {p0}, Lcom/google/r/b/a/a/s;->c()I

    .line 185
    iget v0, p0, Lcom/google/r/b/a/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 188
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/s;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 189
    iget-object v0, p0, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 191
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/s;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 192
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/a/s;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/a/s;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 195
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 174
    iget-byte v1, p0, Lcom/google/r/b/a/a/s;->f:B

    .line 175
    if-ne v1, v0, :cond_0

    .line 179
    :goto_0
    return v0

    .line 176
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 178
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/a/s;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 199
    iget v0, p0, Lcom/google/r/b/a/a/s;->g:I

    .line 200
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 217
    :goto_0
    return v0

    .line 203
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 204
    iget-object v0, p0, Lcom/google/r/b/a/a/s;->b:Lcom/google/n/ao;

    .line 205
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 207
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/a/s;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 208
    iget-object v2, p0, Lcom/google/r/b/a/a/s;->c:Lcom/google/n/ao;

    .line 209
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 211
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/a/s;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 212
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/a/s;->d:Lcom/google/n/ao;

    .line 213
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/a/s;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    iput v0, p0, Lcom/google/r/b/a/a/s;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/r/b/a/a/s;->newBuilder()Lcom/google/r/b/a/a/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/u;->a(Lcom/google/r/b/a/a/s;)Lcom/google/r/b/a/a/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/r/b/a/a/s;->newBuilder()Lcom/google/r/b/a/a/u;

    move-result-object v0

    return-object v0
.end method
