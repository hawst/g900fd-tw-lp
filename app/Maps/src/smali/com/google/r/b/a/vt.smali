.class public final Lcom/google/r/b/a/vt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/vu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/vr;",
        "Lcom/google/r/b/a/vt;",
        ">;",
        "Lcom/google/r/b/a/vu;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3505
    sget-object v0, Lcom/google/r/b/a/vr;->d:Lcom/google/r/b/a/vr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3558
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/vt;->b:Ljava/lang/Object;

    .line 3634
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/vt;->c:Ljava/lang/Object;

    .line 3506
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3497
    new-instance v2, Lcom/google/r/b/a/vr;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/vr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/vt;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/vt;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/vr;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/vt;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/vr;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/vr;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3497
    check-cast p1, Lcom/google/r/b/a/vr;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/vt;->a(Lcom/google/r/b/a/vr;)Lcom/google/r/b/a/vt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/vr;)Lcom/google/r/b/a/vt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3537
    invoke-static {}, Lcom/google/r/b/a/vr;->d()Lcom/google/r/b/a/vr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3549
    :goto_0
    return-object p0

    .line 3538
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/vr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 3539
    iget v2, p0, Lcom/google/r/b/a/vt;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/vt;->a:I

    .line 3540
    iget-object v2, p1, Lcom/google/r/b/a/vr;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/vt;->b:Ljava/lang/Object;

    .line 3543
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/vr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 3544
    iget v0, p0, Lcom/google/r/b/a/vt;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/vt;->a:I

    .line 3545
    iget-object v0, p1, Lcom/google/r/b/a/vr;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/vt;->c:Ljava/lang/Object;

    .line 3548
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/vr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 3538
    goto :goto_1

    :cond_4
    move v0, v1

    .line 3543
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3553
    const/4 v0, 0x1

    return v0
.end method
