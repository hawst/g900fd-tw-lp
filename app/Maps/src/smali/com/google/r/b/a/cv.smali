.class public final Lcom/google/r/b/a/cv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/dc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/cv;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/r/b/a/cv;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:I

.field g:Lcom/google/n/ao;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5619
    new-instance v0, Lcom/google/r/b/a/cw;

    invoke-direct {v0}, Lcom/google/r/b/a/cw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/cv;->PARSER:Lcom/google/n/ax;

    .line 5948
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/cv;->k:Lcom/google/n/aw;

    .line 6423
    new-instance v0, Lcom/google/r/b/a/cv;

    invoke-direct {v0}, Lcom/google/r/b/a/cv;-><init>()V

    sput-object v0, Lcom/google/r/b/a/cv;->h:Lcom/google/r/b/a/cv;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5528
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5802
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cv;->d:Lcom/google/n/ao;

    .line 5818
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    .line 5850
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    .line 5865
    iput-byte v4, p0, Lcom/google/r/b/a/cv;->i:B

    .line 5911
    iput v4, p0, Lcom/google/r/b/a/cv;->j:I

    .line 5529
    iput v2, p0, Lcom/google/r/b/a/cv;->b:I

    .line 5530
    iput v2, p0, Lcom/google/r/b/a/cv;->c:I

    .line 5531
    iget-object v0, p0, Lcom/google/r/b/a/cv;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 5532
    iget-object v0, p0, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 5533
    iput v2, p0, Lcom/google/r/b/a/cv;->f:I

    .line 5534
    iget-object v0, p0, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 5535
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5541
    invoke-direct {p0}, Lcom/google/r/b/a/cv;-><init>()V

    .line 5542
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 5547
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 5548
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 5549
    sparse-switch v3, :sswitch_data_0

    .line 5554
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 5556
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 5552
    goto :goto_0

    .line 5561
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 5562
    invoke-static {v3}, Lcom/google/r/b/a/da;->a(I)Lcom/google/r/b/a/da;

    move-result-object v4

    .line 5563
    if-nez v4, :cond_1

    .line 5564
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 5610
    :catch_0
    move-exception v0

    .line 5611
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5616
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/cv;->au:Lcom/google/n/bn;

    throw v0

    .line 5566
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/cv;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/cv;->a:I

    .line 5567
    iput v3, p0, Lcom/google/r/b/a/cv;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 5612
    :catch_1
    move-exception v0

    .line 5613
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 5614
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5572
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 5573
    iget v3, p0, Lcom/google/r/b/a/cv;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/cv;->a:I

    goto :goto_0

    .line 5577
    :sswitch_3
    iget-object v3, p0, Lcom/google/r/b/a/cv;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 5578
    iget v3, p0, Lcom/google/r/b/a/cv;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/cv;->a:I

    goto :goto_0

    .line 5582
    :sswitch_4
    iget-object v3, p0, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 5583
    iget v3, p0, Lcom/google/r/b/a/cv;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/cv;->a:I

    goto :goto_0

    .line 5587
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 5588
    invoke-static {v3}, Lcom/google/r/b/a/cs;->a(I)Lcom/google/r/b/a/cs;

    move-result-object v4

    .line 5589
    if-nez v4, :cond_2

    .line 5590
    const/4 v4, 0x5

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 5592
    :cond_2
    iget v4, p0, Lcom/google/r/b/a/cv;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/cv;->a:I

    .line 5593
    iput v3, p0, Lcom/google/r/b/a/cv;->f:I

    goto/16 :goto_0

    .line 5598
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 5599
    invoke-static {v3}, Lcom/google/r/b/a/cy;->a(I)Lcom/google/r/b/a/cy;

    move-result-object v4

    .line 5600
    if-nez v4, :cond_3

    .line 5601
    const/4 v4, 0x6

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 5603
    :cond_3
    iget v4, p0, Lcom/google/r/b/a/cv;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/cv;->a:I

    .line 5604
    iput v3, p0, Lcom/google/r/b/a/cv;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 5616
    :cond_4
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/cv;->au:Lcom/google/n/bn;

    .line 5617
    return-void

    .line 5549
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 5526
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5802
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cv;->d:Lcom/google/n/ao;

    .line 5818
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    .line 5850
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    .line 5865
    iput-byte v1, p0, Lcom/google/r/b/a/cv;->i:B

    .line 5911
    iput v1, p0, Lcom/google/r/b/a/cv;->j:I

    .line 5527
    return-void
.end method

.method public static d()Lcom/google/r/b/a/cv;
    .locals 1

    .prologue
    .line 6426
    sget-object v0, Lcom/google/r/b/a/cv;->h:Lcom/google/r/b/a/cv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/cx;
    .locals 1

    .prologue
    .line 6010
    new-instance v0, Lcom/google/r/b/a/cx;

    invoke-direct {v0}, Lcom/google/r/b/a/cx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/cv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5631
    sget-object v0, Lcom/google/r/b/a/cv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5889
    invoke-virtual {p0}, Lcom/google/r/b/a/cv;->c()I

    .line 5890
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5891
    iget v0, p0, Lcom/google/r/b/a/cv;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 5893
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 5894
    iget-object v0, p0, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5896
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 5897
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/cv;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5899
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 5900
    iget-object v0, p0, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5902
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 5903
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/cv;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 5905
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_5

    .line 5906
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/cv;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 5908
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/cv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5909
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5867
    iget-byte v0, p0, Lcom/google/r/b/a/cv;->i:B

    .line 5868
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 5884
    :goto_0
    return v0

    .line 5869
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 5871
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 5872
    iget-object v0, p0, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 5873
    iput-byte v2, p0, Lcom/google/r/b/a/cv;->i:B

    move v0, v2

    .line 5874
    goto :goto_0

    :cond_2
    move v0, v2

    .line 5871
    goto :goto_1

    .line 5877
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 5878
    iget-object v0, p0, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5879
    iput-byte v2, p0, Lcom/google/r/b/a/cv;->i:B

    move v0, v2

    .line 5880
    goto :goto_0

    :cond_4
    move v0, v2

    .line 5877
    goto :goto_2

    .line 5883
    :cond_5
    iput-byte v1, p0, Lcom/google/r/b/a/cv;->i:B

    move v0, v1

    .line 5884
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 5913
    iget v0, p0, Lcom/google/r/b/a/cv;->j:I

    .line 5914
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 5943
    :goto_0
    return v0

    .line 5917
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 5918
    iget v0, p0, Lcom/google/r/b/a/cv;->b:I

    .line 5919
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 5921
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 5922
    iget-object v3, p0, Lcom/google/r/b/a/cv;->e:Lcom/google/n/ao;

    .line 5923
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 5925
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v5, :cond_2

    .line 5926
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/r/b/a/cv;->d:Lcom/google/n/ao;

    .line 5927
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 5929
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_3

    .line 5930
    iget-object v3, p0, Lcom/google/r/b/a/cv;->g:Lcom/google/n/ao;

    .line 5931
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 5933
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 5934
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/r/b/a/cv;->f:I

    .line 5935
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 5937
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/cv;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v6, :cond_6

    .line 5938
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/r/b/a/cv;->c:I

    .line 5939
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_5
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 5941
    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/cv;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 5942
    iput v0, p0, Lcom/google/r/b/a/cv;->j:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 5919
    goto/16 :goto_1

    :cond_8
    move v3, v1

    .line 5935
    goto :goto_3

    :cond_9
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5520
    invoke-static {}, Lcom/google/r/b/a/cv;->newBuilder()Lcom/google/r/b/a/cx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/cx;->a(Lcom/google/r/b/a/cv;)Lcom/google/r/b/a/cx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5520
    invoke-static {}, Lcom/google/r/b/a/cv;->newBuilder()Lcom/google/r/b/a/cx;

    move-result-object v0

    return-object v0
.end method
