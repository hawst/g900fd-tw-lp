.class public final Lcom/google/r/b/a/qj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qj;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/qj;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Z

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7460
    new-instance v0, Lcom/google/r/b/a/qk;

    invoke-direct {v0}, Lcom/google/r/b/a/qk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qj;->PARSER:Lcom/google/n/ax;

    .line 7658
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/qj;->j:Lcom/google/n/aw;

    .line 8193
    new-instance v0, Lcom/google/r/b/a/qj;

    invoke-direct {v0}, Lcom/google/r/b/a/qj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qj;->g:Lcom/google/r/b/a/qj;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 7385
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 7520
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    .line 7536
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qj;->d:Lcom/google/n/ao;

    .line 7552
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qj;->e:Lcom/google/n/ao;

    .line 7582
    iput-byte v3, p0, Lcom/google/r/b/a/qj;->h:B

    .line 7625
    iput v3, p0, Lcom/google/r/b/a/qj;->i:I

    .line 7386
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    .line 7387
    iget-object v0, p0, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 7388
    iget-object v0, p0, Lcom/google/r/b/a/qj;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 7389
    iget-object v0, p0, Lcom/google/r/b/a/qj;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 7390
    iput-boolean v2, p0, Lcom/google/r/b/a/qj;->f:Z

    .line 7391
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 7397
    invoke-direct {p0}, Lcom/google/r/b/a/qj;-><init>()V

    .line 7400
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 7403
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 7404
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 7405
    sparse-switch v4, :sswitch_data_0

    .line 7410
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 7412
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 7408
    goto :goto_0

    .line 7417
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 7418
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    .line 7420
    or-int/lit8 v1, v1, 0x1

    .line 7422
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 7423
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 7422
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 7448
    :catch_0
    move-exception v0

    .line 7449
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7454
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 7455
    iget-object v1, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    .line 7457
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/qj;->au:Lcom/google/n/bn;

    throw v0

    .line 7427
    :sswitch_2
    :try_start_2
    iget-object v4, p0, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 7428
    iget v4, p0, Lcom/google/r/b/a/qj;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/qj;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 7450
    :catch_1
    move-exception v0

    .line 7451
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 7452
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 7432
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/r/b/a/qj;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 7433
    iget v4, p0, Lcom/google/r/b/a/qj;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/qj;->a:I

    goto :goto_0

    .line 7437
    :sswitch_4
    iget-object v4, p0, Lcom/google/r/b/a/qj;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 7438
    iget v4, p0, Lcom/google/r/b/a/qj;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/qj;->a:I

    goto/16 :goto_0

    .line 7442
    :sswitch_5
    iget v4, p0, Lcom/google/r/b/a/qj;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/qj;->a:I

    .line 7443
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/qj;->f:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 7454
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 7455
    iget-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    .line 7457
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qj;->au:Lcom/google/n/bn;

    .line 7458
    return-void

    .line 7405
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 7383
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 7520
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    .line 7536
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qj;->d:Lcom/google/n/ao;

    .line 7552
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/qj;->e:Lcom/google/n/ao;

    .line 7582
    iput-byte v1, p0, Lcom/google/r/b/a/qj;->h:B

    .line 7625
    iput v1, p0, Lcom/google/r/b/a/qj;->i:I

    .line 7384
    return-void
.end method

.method public static d()Lcom/google/r/b/a/qj;
    .locals 1

    .prologue
    .line 8196
    sget-object v0, Lcom/google/r/b/a/qj;->g:Lcom/google/r/b/a/qj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ql;
    .locals 1

    .prologue
    .line 7720
    new-instance v0, Lcom/google/r/b/a/ql;

    invoke-direct {v0}, Lcom/google/r/b/a/ql;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7472
    sget-object v0, Lcom/google/r/b/a/qj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 7606
    invoke-virtual {p0}, Lcom/google/r/b/a/qj;->c()I

    .line 7607
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 7608
    iget-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7607
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 7610
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 7611
    iget-object v0, p0, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7613
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 7614
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/qj;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7616
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 7617
    iget-object v0, p0, Lcom/google/r/b/a/qj;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7619
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 7620
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/qj;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 7622
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/qj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 7623
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7584
    iget-byte v0, p0, Lcom/google/r/b/a/qj;->h:B

    .line 7585
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 7601
    :cond_0
    :goto_0
    return v2

    .line 7586
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 7588
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 7589
    iget-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/qf;->d()Lcom/google/r/b/a/qf;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/qf;

    invoke-virtual {v0}, Lcom/google/r/b/a/qf;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 7590
    iput-byte v2, p0, Lcom/google/r/b/a/qj;->h:B

    goto :goto_0

    .line 7588
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 7594
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 7595
    iget-object v0, p0, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/qr;->d()Lcom/google/r/b/a/qr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/qr;

    invoke-virtual {v0}, Lcom/google/r/b/a/qr;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 7596
    iput-byte v2, p0, Lcom/google/r/b/a/qj;->h:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 7594
    goto :goto_2

    .line 7600
    :cond_5
    iput-byte v3, p0, Lcom/google/r/b/a/qj;->h:B

    move v2, v3

    .line 7601
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 7627
    iget v0, p0, Lcom/google/r/b/a/qj;->i:I

    .line 7628
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 7653
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 7631
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 7632
    iget-object v0, p0, Lcom/google/r/b/a/qj;->b:Ljava/util/List;

    .line 7633
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 7631
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 7635
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 7636
    iget-object v0, p0, Lcom/google/r/b/a/qj;->c:Lcom/google/n/ao;

    .line 7637
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 7639
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 7640
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/qj;->d:Lcom/google/n/ao;

    .line 7641
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 7643
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 7644
    iget-object v0, p0, Lcom/google/r/b/a/qj;->e:Lcom/google/n/ao;

    .line 7645
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 7647
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/qj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 7648
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/qj;->f:Z

    .line 7649
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 7651
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/qj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 7652
    iput v0, p0, Lcom/google/r/b/a/qj;->i:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7377
    invoke-static {}, Lcom/google/r/b/a/qj;->newBuilder()Lcom/google/r/b/a/ql;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ql;->a(Lcom/google/r/b/a/qj;)Lcom/google/r/b/a/ql;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 7377
    invoke-static {}, Lcom/google/r/b/a/qj;->newBuilder()Lcom/google/r/b/a/ql;

    move-result-object v0

    return-object v0
.end method
