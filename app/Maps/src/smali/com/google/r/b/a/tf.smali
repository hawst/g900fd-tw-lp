.class public final enum Lcom/google/r/b/a/tf;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/tf;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/tf;

.field public static final enum b:Lcom/google/r/b/a/tf;

.field public static final enum c:Lcom/google/r/b/a/tf;

.field public static final enum d:Lcom/google/r/b/a/tf;

.field public static final enum e:Lcom/google/r/b/a/tf;

.field public static final enum f:Lcom/google/r/b/a/tf;

.field private static final synthetic h:[Lcom/google/r/b/a/tf;


# instance fields
.field public final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 96
    new-instance v0, Lcom/google/r/b/a/tf;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/tf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/tf;->a:Lcom/google/r/b/a/tf;

    .line 100
    new-instance v0, Lcom/google/r/b/a/tf;

    const-string v1, "DIRECTIONS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/tf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/tf;->b:Lcom/google/r/b/a/tf;

    .line 104
    new-instance v0, Lcom/google/r/b/a/tf;

    const-string v1, "PLACE_DETAILS"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/r/b/a/tf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/tf;->c:Lcom/google/r/b/a/tf;

    .line 108
    new-instance v0, Lcom/google/r/b/a/tf;

    const-string v1, "START_PAGE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/r/b/a/tf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/tf;->d:Lcom/google/r/b/a/tf;

    .line 112
    new-instance v0, Lcom/google/r/b/a/tf;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/r/b/a/tf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/tf;->e:Lcom/google/r/b/a/tf;

    .line 116
    new-instance v0, Lcom/google/r/b/a/tf;

    const-string v1, "TILE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/tf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/tf;->f:Lcom/google/r/b/a/tf;

    .line 91
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/r/b/a/tf;

    sget-object v1, Lcom/google/r/b/a/tf;->a:Lcom/google/r/b/a/tf;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/tf;->b:Lcom/google/r/b/a/tf;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/tf;->c:Lcom/google/r/b/a/tf;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/tf;->d:Lcom/google/r/b/a/tf;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/tf;->e:Lcom/google/r/b/a/tf;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/tf;->f:Lcom/google/r/b/a/tf;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/tf;->h:[Lcom/google/r/b/a/tf;

    .line 166
    new-instance v0, Lcom/google/r/b/a/tg;

    invoke-direct {v0}, Lcom/google/r/b/a/tg;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 175
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 176
    iput p3, p0, Lcom/google/r/b/a/tf;->g:I

    .line 177
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/tf;
    .locals 1

    .prologue
    .line 150
    packed-switch p0, :pswitch_data_0

    .line 157
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 151
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/tf;->a:Lcom/google/r/b/a/tf;

    goto :goto_0

    .line 152
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/tf;->b:Lcom/google/r/b/a/tf;

    goto :goto_0

    .line 153
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/tf;->c:Lcom/google/r/b/a/tf;

    goto :goto_0

    .line 154
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/tf;->d:Lcom/google/r/b/a/tf;

    goto :goto_0

    .line 155
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/tf;->e:Lcom/google/r/b/a/tf;

    goto :goto_0

    .line 156
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/tf;->f:Lcom/google/r/b/a/tf;

    goto :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/tf;
    .locals 1

    .prologue
    .line 91
    const-class v0, Lcom/google/r/b/a/tf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/tf;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/tf;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/r/b/a/tf;->h:[Lcom/google/r/b/a/tf;

    invoke-virtual {v0}, [Lcom/google/r/b/a/tf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/tf;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/google/r/b/a/tf;->g:I

    return v0
.end method
