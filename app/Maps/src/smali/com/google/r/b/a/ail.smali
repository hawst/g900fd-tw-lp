.class public final Lcom/google/r/b/a/ail;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aio;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ail;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/ail;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:I

.field c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 725
    new-instance v0, Lcom/google/r/b/a/aim;

    invoke-direct {v0}, Lcom/google/r/b/a/aim;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ail;->PARSER:Lcom/google/n/ax;

    .line 891
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ail;->h:Lcom/google/n/aw;

    .line 1225
    new-instance v0, Lcom/google/r/b/a/ail;

    invoke-direct {v0}, Lcom/google/r/b/a/ail;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ail;->e:Lcom/google/r/b/a/ail;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 662
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 841
    iput-byte v0, p0, Lcom/google/r/b/a/ail;->f:B

    .line 866
    iput v0, p0, Lcom/google/r/b/a/ail;->g:I

    .line 663
    iput v0, p0, Lcom/google/r/b/a/ail;->b:I

    .line 664
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ail;->c:Ljava/lang/Object;

    .line 665
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ail;->d:Ljava/lang/Object;

    .line 666
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 672
    invoke-direct {p0}, Lcom/google/r/b/a/ail;-><init>()V

    .line 673
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 677
    const/4 v0, 0x0

    .line 678
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 679
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 680
    sparse-switch v3, :sswitch_data_0

    .line 685
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 687
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 683
    goto :goto_0

    .line 692
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 693
    invoke-static {v3}, Lcom/google/maps/g/c/d;->a(I)Lcom/google/maps/g/c/d;

    move-result-object v4

    .line 694
    if-nez v4, :cond_1

    .line 695
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 716
    :catch_0
    move-exception v0

    .line 717
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 722
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ail;->au:Lcom/google/n/bn;

    throw v0

    .line 697
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/ail;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ail;->a:I

    .line 698
    iput v3, p0, Lcom/google/r/b/a/ail;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 718
    :catch_1
    move-exception v0

    .line 719
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 720
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 703
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 704
    iget v4, p0, Lcom/google/r/b/a/ail;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/ail;->a:I

    .line 705
    iput-object v3, p0, Lcom/google/r/b/a/ail;->c:Ljava/lang/Object;

    goto :goto_0

    .line 709
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 710
    iget v4, p0, Lcom/google/r/b/a/ail;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/ail;->a:I

    .line 711
    iput-object v3, p0, Lcom/google/r/b/a/ail;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 722
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ail;->au:Lcom/google/n/bn;

    .line 723
    return-void

    .line 680
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 660
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 841
    iput-byte v0, p0, Lcom/google/r/b/a/ail;->f:B

    .line 866
    iput v0, p0, Lcom/google/r/b/a/ail;->g:I

    .line 661
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ail;
    .locals 1

    .prologue
    .line 1228
    sget-object v0, Lcom/google/r/b/a/ail;->e:Lcom/google/r/b/a/ail;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ain;
    .locals 1

    .prologue
    .line 953
    new-instance v0, Lcom/google/r/b/a/ain;

    invoke-direct {v0}, Lcom/google/r/b/a/ain;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ail;",
            ">;"
        }
    .end annotation

    .prologue
    .line 737
    sget-object v0, Lcom/google/r/b/a/ail;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 853
    invoke-virtual {p0}, Lcom/google/r/b/a/ail;->c()I

    .line 854
    iget v0, p0, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 855
    iget v0, p0, Lcom/google/r/b/a/ail;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 857
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 858
    iget-object v0, p0, Lcom/google/r/b/a/ail;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ail;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 860
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 861
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/ail;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ail;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 863
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ail;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 864
    return-void

    .line 858
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 861
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 843
    iget-byte v1, p0, Lcom/google/r/b/a/ail;->f:B

    .line 844
    if-ne v1, v0, :cond_0

    .line 848
    :goto_0
    return v0

    .line 845
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 847
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ail;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 868
    iget v0, p0, Lcom/google/r/b/a/ail;->g:I

    .line 869
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 886
    :goto_0
    return v0

    .line 872
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 873
    iget v0, p0, Lcom/google/r/b/a/ail;->b:I

    .line 874
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 876
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 878
    iget-object v0, p0, Lcom/google/r/b/a/ail;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ail;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 880
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ail;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 881
    const/4 v3, 0x3

    .line 882
    iget-object v0, p0, Lcom/google/r/b/a/ail;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ail;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 884
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ail;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 885
    iput v0, p0, Lcom/google/r/b/a/ail;->g:I

    goto :goto_0

    .line 874
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    .line 878
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 882
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 654
    invoke-static {}, Lcom/google/r/b/a/ail;->newBuilder()Lcom/google/r/b/a/ain;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ain;->a(Lcom/google/r/b/a/ail;)Lcom/google/r/b/a/ain;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 654
    invoke-static {}, Lcom/google/r/b/a/ail;->newBuilder()Lcom/google/r/b/a/ain;

    move-result-object v0

    return-object v0
.end method
