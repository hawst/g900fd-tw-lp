.class public final Lcom/google/r/b/a/t;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/r;",
        "Lcom/google/r/b/a/t;",
        ">;",
        "Lcom/google/r/b/a/u;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3630
    sget-object v0, Lcom/google/r/b/a/r;->e:Lcom/google/r/b/a/r;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3706
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/t;->b:Ljava/lang/Object;

    .line 3783
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    .line 3920
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    .line 3631
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 3622
    new-instance v2, Lcom/google/r/b/a/r;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/r;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/t;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/t;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/r;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/r/b/a/t;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/t;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/t;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/t;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/t;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/r/b/a/t;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/r;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3622
    check-cast p1, Lcom/google/r/b/a/r;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/t;->a(Lcom/google/r/b/a/r;)Lcom/google/r/b/a/t;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/r;)Lcom/google/r/b/a/t;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3670
    invoke-static {}, Lcom/google/r/b/a/r;->g()Lcom/google/r/b/a/r;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 3697
    :goto_0
    return-object p0

    .line 3671
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/r;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_1
    if-eqz v0, :cond_1

    .line 3672
    iget v0, p0, Lcom/google/r/b/a/t;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/t;->a:I

    .line 3673
    iget-object v0, p1, Lcom/google/r/b/a/r;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/t;->b:Ljava/lang/Object;

    .line 3676
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3677
    iget-object v0, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3678
    iget-object v0, p1, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    .line 3679
    iget v0, p0, Lcom/google/r/b/a/t;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/t;->a:I

    .line 3686
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3687
    iget-object v0, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3688
    iget-object v0, p1, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    .line 3689
    iget v0, p0, Lcom/google/r/b/a/t;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/r/b/a/t;->a:I

    .line 3696
    :cond_3
    :goto_3
    iget-object v0, p1, Lcom/google/r/b/a/r;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 3671
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 3681
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/t;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/t;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/t;->a:I

    .line 3682
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/t;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/r;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 3691
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/t;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_8

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/t;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/t;->a:I

    .line 3692
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/t;->d:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3701
    const/4 v0, 0x1

    return v0
.end method
