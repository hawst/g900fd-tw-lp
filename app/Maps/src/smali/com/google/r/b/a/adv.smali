.class public final Lcom/google/r/b/a/adv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aea;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/adv;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/adv;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/nc;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1967
    new-instance v0, Lcom/google/r/b/a/adw;

    invoke-direct {v0}, Lcom/google/r/b/a/adw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/adv;->PARSER:Lcom/google/n/ax;

    .line 2144
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/adv;->g:Lcom/google/n/aw;

    .line 2446
    new-instance v0, Lcom/google/r/b/a/adv;

    invoke-direct {v0}, Lcom/google/r/b/a/adv;-><init>()V

    sput-object v0, Lcom/google/r/b/a/adv;->d:Lcom/google/r/b/a/adv;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1906
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2101
    iput-byte v0, p0, Lcom/google/r/b/a/adv;->e:B

    .line 2123
    iput v0, p0, Lcom/google/r/b/a/adv;->f:I

    .line 1907
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    .line 1908
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/adv;->c:I

    .line 1909
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1915
    invoke-direct {p0}, Lcom/google/r/b/a/adv;-><init>()V

    .line 1918
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 1921
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 1922
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 1923
    sparse-switch v4, :sswitch_data_0

    .line 1928
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 1930
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 1926
    goto :goto_0

    .line 1935
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 1936
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    .line 1937
    or-int/lit8 v1, v1, 0x1

    .line 1939
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    sget-object v5, Lcom/google/maps/g/nc;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1955
    :catch_0
    move-exception v0

    .line 1956
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1961
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 1962
    iget-object v1, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    .line 1964
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/adv;->au:Lcom/google/n/bn;

    throw v0

    .line 1943
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 1944
    invoke-static {v4}, Lcom/google/r/b/a/ady;->a(I)Lcom/google/r/b/a/ady;

    move-result-object v5

    .line 1945
    if-nez v5, :cond_3

    .line 1946
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1957
    :catch_1
    move-exception v0

    .line 1958
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 1959
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1948
    :cond_3
    :try_start_4
    iget v5, p0, Lcom/google/r/b/a/adv;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/adv;->a:I

    .line 1949
    iput v4, p0, Lcom/google/r/b/a/adv;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1961
    :cond_4
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_5

    .line 1962
    iget-object v0, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    .line 1964
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/adv;->au:Lcom/google/n/bn;

    .line 1965
    return-void

    .line 1923
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1904
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2101
    iput-byte v0, p0, Lcom/google/r/b/a/adv;->e:B

    .line 2123
    iput v0, p0, Lcom/google/r/b/a/adv;->f:I

    .line 1905
    return-void
.end method

.method public static d()Lcom/google/r/b/a/adv;
    .locals 1

    .prologue
    .line 2449
    sget-object v0, Lcom/google/r/b/a/adv;->d:Lcom/google/r/b/a/adv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/adx;
    .locals 1

    .prologue
    .line 2206
    new-instance v0, Lcom/google/r/b/a/adx;

    invoke-direct {v0}, Lcom/google/r/b/a/adx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/adv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1979
    sget-object v0, Lcom/google/r/b/a/adv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2113
    invoke-virtual {p0}, Lcom/google/r/b/a/adv;->c()I

    .line 2114
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2115
    iget-object v0, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 2114
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2117
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/adv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 2118
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/r/b/a/adv;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 2120
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/adv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2121
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2103
    iget-byte v1, p0, Lcom/google/r/b/a/adv;->e:B

    .line 2104
    if-ne v1, v0, :cond_0

    .line 2108
    :goto_0
    return v0

    .line 2105
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2107
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/adv;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2125
    iget v0, p0, Lcom/google/r/b/a/adv;->f:I

    .line 2126
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2139
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 2129
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2130
    iget-object v0, p0, Lcom/google/r/b/a/adv;->b:Ljava/util/List;

    .line 2131
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2129
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2133
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/adv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_2

    .line 2134
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/r/b/a/adv;->c:I

    .line 2135
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_3

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 2137
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/adv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2138
    iput v0, p0, Lcom/google/r/b/a/adv;->f:I

    goto :goto_0

    .line 2135
    :cond_3
    const/16 v0, 0xa

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1898
    invoke-static {}, Lcom/google/r/b/a/adv;->newBuilder()Lcom/google/r/b/a/adx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/adx;->a(Lcom/google/r/b/a/adv;)Lcom/google/r/b/a/adx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1898
    invoke-static {}, Lcom/google/r/b/a/adv;->newBuilder()Lcom/google/r/b/a/adx;

    move-result-object v0

    return-object v0
.end method
