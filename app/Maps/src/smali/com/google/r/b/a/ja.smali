.class public final enum Lcom/google/r/b/a/ja;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ja;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ja;

.field public static final enum b:Lcom/google/r/b/a/ja;

.field public static final enum c:Lcom/google/r/b/a/ja;

.field private static final synthetic e:[Lcom/google/r/b/a/ja;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 187
    new-instance v0, Lcom/google/r/b/a/ja;

    const-string v1, "INVALID_PAGE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/ja;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ja;->a:Lcom/google/r/b/a/ja;

    .line 191
    new-instance v0, Lcom/google/r/b/a/ja;

    const-string v1, "MAPS_ACTIVITIES_SUMMARY_PAGE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/ja;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ja;->b:Lcom/google/r/b/a/ja;

    .line 195
    new-instance v0, Lcom/google/r/b/a/ja;

    const-string v1, "TODO_LIST_PAGE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/ja;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ja;->c:Lcom/google/r/b/a/ja;

    .line 182
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/ja;

    sget-object v1, Lcom/google/r/b/a/ja;->a:Lcom/google/r/b/a/ja;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/ja;->b:Lcom/google/r/b/a/ja;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/ja;->c:Lcom/google/r/b/a/ja;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/ja;->e:[Lcom/google/r/b/a/ja;

    .line 230
    new-instance v0, Lcom/google/r/b/a/jb;

    invoke-direct {v0}, Lcom/google/r/b/a/jb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 239
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 240
    iput p3, p0, Lcom/google/r/b/a/ja;->d:I

    .line 241
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ja;
    .locals 1

    .prologue
    .line 217
    packed-switch p0, :pswitch_data_0

    .line 221
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 218
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/ja;->a:Lcom/google/r/b/a/ja;

    goto :goto_0

    .line 219
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ja;->b:Lcom/google/r/b/a/ja;

    goto :goto_0

    .line 220
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/ja;->c:Lcom/google/r/b/a/ja;

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ja;
    .locals 1

    .prologue
    .line 182
    const-class v0, Lcom/google/r/b/a/ja;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ja;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ja;
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcom/google/r/b/a/ja;->e:[Lcom/google/r/b/a/ja;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ja;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ja;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/google/r/b/a/ja;->d:I

    return v0
.end method
