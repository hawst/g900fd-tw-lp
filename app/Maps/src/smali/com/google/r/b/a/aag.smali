.class public final Lcom/google/r/b/a/aag;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aaj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aag;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/aag;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 519
    new-instance v0, Lcom/google/r/b/a/aah;

    invoke-direct {v0}, Lcom/google/r/b/a/aah;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aag;->PARSER:Lcom/google/n/ax;

    .line 702
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aag;->j:Lcom/google/n/aw;

    .line 1166
    new-instance v0, Lcom/google/r/b/a/aag;

    invoke-direct {v0}, Lcom/google/r/b/a/aag;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aag;->g:Lcom/google/r/b/a/aag;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 452
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 551
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    .line 567
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    .line 583
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    .line 599
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    .line 614
    iput-byte v3, p0, Lcom/google/r/b/a/aag;->h:B

    .line 669
    iput v3, p0, Lcom/google/r/b/a/aag;->i:I

    .line 453
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aag;->b:I

    .line 454
    iget-object v0, p0, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 455
    iget-object v0, p0, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 456
    iget-object v0, p0, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 457
    iget-object v0, p0, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 458
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 464
    invoke-direct {p0}, Lcom/google/r/b/a/aag;-><init>()V

    .line 465
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 470
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 471
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 472
    sparse-switch v3, :sswitch_data_0

    .line 477
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 479
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 475
    goto :goto_0

    .line 484
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/aag;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aag;->a:I

    .line 485
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/aag;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 510
    :catch_0
    move-exception v0

    .line 511
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 516
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aag;->au:Lcom/google/n/bn;

    throw v0

    .line 489
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 490
    iget v3, p0, Lcom/google/r/b/a/aag;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/aag;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 512
    :catch_1
    move-exception v0

    .line 513
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 514
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 494
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 495
    iget v3, p0, Lcom/google/r/b/a/aag;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/aag;->a:I

    goto :goto_0

    .line 499
    :sswitch_4
    iget-object v3, p0, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 500
    iget v3, p0, Lcom/google/r/b/a/aag;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/aag;->a:I

    goto :goto_0

    .line 504
    :sswitch_5
    iget-object v3, p0, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 505
    iget v3, p0, Lcom/google/r/b/a/aag;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/aag;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 516
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aag;->au:Lcom/google/n/bn;

    .line 517
    return-void

    .line 472
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 450
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 551
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    .line 567
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    .line 583
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    .line 599
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    .line 614
    iput-byte v1, p0, Lcom/google/r/b/a/aag;->h:B

    .line 669
    iput v1, p0, Lcom/google/r/b/a/aag;->i:I

    .line 451
    return-void
.end method

.method public static a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;
    .locals 1

    .prologue
    .line 767
    invoke-static {}, Lcom/google/r/b/a/aag;->newBuilder()Lcom/google/r/b/a/aai;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aai;->a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/aag;
    .locals 1

    .prologue
    .line 1169
    sget-object v0, Lcom/google/r/b/a/aag;->g:Lcom/google/r/b/a/aag;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aai;
    .locals 1

    .prologue
    .line 764
    new-instance v0, Lcom/google/r/b/a/aai;

    invoke-direct {v0}, Lcom/google/r/b/a/aai;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 531
    sget-object v0, Lcom/google/r/b/a/aag;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 650
    invoke-virtual {p0}, Lcom/google/r/b/a/aag;->c()I

    .line 651
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 652
    iget v0, p0, Lcom/google/r/b/a/aag;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 654
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 655
    iget-object v0, p0, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 657
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 658
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 660
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 661
    iget-object v0, p0, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 663
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 664
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 666
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/aag;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 667
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 616
    iget-byte v0, p0, Lcom/google/r/b/a/aag;->h:B

    .line 617
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 645
    :goto_0
    return v0

    .line 618
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 620
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 621
    iget-object v0, p0, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/abe;->d()Lcom/google/r/b/a/abe;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abe;

    invoke-virtual {v0}, Lcom/google/r/b/a/abe;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 622
    iput-byte v2, p0, Lcom/google/r/b/a/aag;->h:B

    move v0, v2

    .line 623
    goto :goto_0

    :cond_2
    move v0, v2

    .line 620
    goto :goto_1

    .line 626
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 627
    iget-object v0, p0, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/abe;->d()Lcom/google/r/b/a/abe;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abe;

    invoke-virtual {v0}, Lcom/google/r/b/a/abe;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 628
    iput-byte v2, p0, Lcom/google/r/b/a/aag;->h:B

    move v0, v2

    .line 629
    goto :goto_0

    :cond_4
    move v0, v2

    .line 626
    goto :goto_2

    .line 632
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 633
    iget-object v0, p0, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aao;->d()Lcom/google/r/b/a/aao;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aao;

    invoke-virtual {v0}, Lcom/google/r/b/a/aao;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 634
    iput-byte v2, p0, Lcom/google/r/b/a/aag;->h:B

    move v0, v2

    .line 635
    goto :goto_0

    :cond_6
    move v0, v2

    .line 632
    goto :goto_3

    .line 638
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 639
    iget-object v0, p0, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aaw;->d()Lcom/google/r/b/a/aaw;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aaw;

    invoke-virtual {v0}, Lcom/google/r/b/a/aaw;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 640
    iput-byte v2, p0, Lcom/google/r/b/a/aag;->h:B

    move v0, v2

    .line 641
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 638
    goto :goto_4

    .line 644
    :cond_9
    iput-byte v1, p0, Lcom/google/r/b/a/aag;->h:B

    move v0, v1

    .line 645
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 671
    iget v0, p0, Lcom/google/r/b/a/aag;->i:I

    .line 672
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 697
    :goto_0
    return v0

    .line 675
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 676
    iget v0, p0, Lcom/google/r/b/a/aag;->b:I

    .line 677
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 679
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 680
    iget-object v2, p0, Lcom/google/r/b/a/aag;->c:Lcom/google/n/ao;

    .line 681
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 683
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 684
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/aag;->d:Lcom/google/n/ao;

    .line 685
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 687
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 688
    iget-object v2, p0, Lcom/google/r/b/a/aag;->e:Lcom/google/n/ao;

    .line 689
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 691
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/aag;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 692
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/r/b/a/aag;->f:Lcom/google/n/ao;

    .line 693
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 695
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/aag;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 696
    iput v0, p0, Lcom/google/r/b/a/aag;->i:I

    goto/16 :goto_0

    .line 677
    :cond_5
    const/16 v0, 0xa

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 444
    invoke-static {}, Lcom/google/r/b/a/aag;->newBuilder()Lcom/google/r/b/a/aai;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aai;->a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 444
    invoke-static {}, Lcom/google/r/b/a/aag;->newBuilder()Lcom/google/r/b/a/aai;

    move-result-object v0

    return-object v0
.end method
