.class public final Lcom/google/r/b/a/a/w;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/z;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/w;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/a/w;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1557
    new-instance v0, Lcom/google/r/b/a/a/x;

    invoke-direct {v0}, Lcom/google/r/b/a/a/x;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/w;->PARSER:Lcom/google/n/ax;

    .line 1625
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/w;->f:Lcom/google/n/aw;

    .line 1785
    new-instance v0, Lcom/google/r/b/a/a/w;

    invoke-direct {v0}, Lcom/google/r/b/a/a/w;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/w;->c:Lcom/google/r/b/a/a/w;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1508
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1589
    iput-byte v0, p0, Lcom/google/r/b/a/a/w;->d:B

    .line 1608
    iput v0, p0, Lcom/google/r/b/a/a/w;->e:I

    .line 1509
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/a/w;->b:I

    .line 1510
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1516
    invoke-direct {p0}, Lcom/google/r/b/a/a/w;-><init>()V

    .line 1517
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1521
    const/4 v0, 0x0

    .line 1522
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 1523
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1524
    sparse-switch v3, :sswitch_data_0

    .line 1529
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1531
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1527
    goto :goto_0

    .line 1536
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1537
    invoke-static {v3}, Lcom/google/maps/g/a/he;->a(I)Lcom/google/maps/g/a/he;

    move-result-object v4

    .line 1538
    if-nez v4, :cond_1

    .line 1539
    const/16 v4, 0x8

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1548
    :catch_0
    move-exception v0

    .line 1549
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1554
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/w;->au:Lcom/google/n/bn;

    throw v0

    .line 1541
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/a/w;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/a/w;->a:I

    .line 1542
    iput v3, p0, Lcom/google/r/b/a/a/w;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1550
    :catch_1
    move-exception v0

    .line 1551
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1552
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1554
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/w;->au:Lcom/google/n/bn;

    .line 1555
    return-void

    .line 1524
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x40 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1506
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1589
    iput-byte v0, p0, Lcom/google/r/b/a/a/w;->d:B

    .line 1608
    iput v0, p0, Lcom/google/r/b/a/a/w;->e:I

    .line 1507
    return-void
.end method

.method public static d()Lcom/google/r/b/a/a/w;
    .locals 1

    .prologue
    .line 1788
    sget-object v0, Lcom/google/r/b/a/a/w;->c:Lcom/google/r/b/a/a/w;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/y;
    .locals 1

    .prologue
    .line 1687
    new-instance v0, Lcom/google/r/b/a/a/y;

    invoke-direct {v0}, Lcom/google/r/b/a/a/y;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1569
    sget-object v0, Lcom/google/r/b/a/a/w;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 1601
    invoke-virtual {p0}, Lcom/google/r/b/a/a/w;->c()I

    .line 1602
    iget v0, p0, Lcom/google/r/b/a/a/w;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1603
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/r/b/a/a/w;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 1605
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/a/w;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1606
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1591
    iget-byte v1, p0, Lcom/google/r/b/a/a/w;->d:B

    .line 1592
    if-ne v1, v0, :cond_0

    .line 1596
    :goto_0
    return v0

    .line 1593
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1595
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/a/w;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1610
    iget v1, p0, Lcom/google/r/b/a/a/w;->e:I

    .line 1611
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 1620
    :goto_0
    return v0

    .line 1614
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/a/w;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1615
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/r/b/a/a/w;->b:I

    .line 1616
    invoke-static {v1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v2, :cond_2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 1618
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/a/w;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1619
    iput v0, p0, Lcom/google/r/b/a/a/w;->e:I

    goto :goto_0

    .line 1616
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1500
    invoke-static {}, Lcom/google/r/b/a/a/w;->newBuilder()Lcom/google/r/b/a/a/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/y;->a(Lcom/google/r/b/a/a/w;)Lcom/google/r/b/a/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1500
    invoke-static {}, Lcom/google/r/b/a/a/w;->newBuilder()Lcom/google/r/b/a/a/y;

    move-result-object v0

    return-object v0
.end method
