.class public final enum Lcom/google/r/b/a/yn;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/yn;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/yn;

.field public static final enum b:Lcom/google/r/b/a/yn;

.field private static final synthetic d:[Lcom/google/r/b/a/yn;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 236
    new-instance v0, Lcom/google/r/b/a/yn;

    const-string v1, "GOOGLE"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/r/b/a/yn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/yn;->a:Lcom/google/r/b/a/yn;

    .line 240
    new-instance v0, Lcom/google/r/b/a/yn;

    const-string v1, "IMMERSIVE"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/r/b/a/yn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/yn;->b:Lcom/google/r/b/a/yn;

    .line 231
    new-array v0, v4, [Lcom/google/r/b/a/yn;

    sget-object v1, Lcom/google/r/b/a/yn;->a:Lcom/google/r/b/a/yn;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/yn;->b:Lcom/google/r/b/a/yn;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/r/b/a/yn;->d:[Lcom/google/r/b/a/yn;

    .line 270
    new-instance v0, Lcom/google/r/b/a/yo;

    invoke-direct {v0}, Lcom/google/r/b/a/yo;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 279
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 280
    iput p3, p0, Lcom/google/r/b/a/yn;->c:I

    .line 281
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/yn;
    .locals 1

    .prologue
    .line 258
    packed-switch p0, :pswitch_data_0

    .line 261
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 259
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/yn;->a:Lcom/google/r/b/a/yn;

    goto :goto_0

    .line 260
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/yn;->b:Lcom/google/r/b/a/yn;

    goto :goto_0

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/yn;
    .locals 1

    .prologue
    .line 231
    const-class v0, Lcom/google/r/b/a/yn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yn;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/yn;
    .locals 1

    .prologue
    .line 231
    sget-object v0, Lcom/google/r/b/a/yn;->d:[Lcom/google/r/b/a/yn;

    invoke-virtual {v0}, [Lcom/google/r/b/a/yn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/yn;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/google/r/b/a/yn;->c:I

    return v0
.end method
