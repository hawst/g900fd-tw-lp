.class public final Lcom/google/r/b/a/at;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/au;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ar;",
        "Lcom/google/r/b/a/at;",
        ">;",
        "Lcom/google/r/b/a/au;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lcom/google/r/b/a/ar;->d:Lcom/google/r/b/a/ar;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/at;->c:Z

    .line 236
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 227
    new-instance v2, Lcom/google/r/b/a/ar;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ar;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/at;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/at;->b:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/ar;->b:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/r/b/a/at;->c:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/ar;->c:Z

    iput v0, v2, Lcom/google/r/b/a/ar;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 227
    check-cast p1, Lcom/google/r/b/a/ar;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/at;->a(Lcom/google/r/b/a/ar;)Lcom/google/r/b/a/at;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ar;)Lcom/google/r/b/a/at;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 267
    invoke-static {}, Lcom/google/r/b/a/ar;->d()Lcom/google/r/b/a/ar;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 275
    :goto_0
    return-object p0

    .line 268
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ar;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 269
    iget-boolean v2, p1, Lcom/google/r/b/a/ar;->b:Z

    iget v3, p0, Lcom/google/r/b/a/at;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/at;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/at;->b:Z

    .line 271
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ar;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 272
    iget-boolean v0, p1, Lcom/google/r/b/a/ar;->c:Z

    iget v1, p0, Lcom/google/r/b/a/at;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/at;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/at;->c:Z

    .line 274
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/ar;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 268
    goto :goto_1

    :cond_4
    move v0, v1

    .line 271
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x1

    return v0
.end method
