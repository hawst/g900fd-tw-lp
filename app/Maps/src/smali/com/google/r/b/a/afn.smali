.class public final Lcom/google/r/b/a/afn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afy;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afn;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/afn;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/r/b/a/afb;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/afq;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/maps/g/jy;

.field public e:Lcom/google/maps/a/a;

.field f:Lcom/google/r/b/a/afu;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field public i:Lcom/google/r/b/a/aag;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12853
    new-instance v0, Lcom/google/r/b/a/afo;

    invoke-direct {v0}, Lcom/google/r/b/a/afo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afn;->PARSER:Lcom/google/n/ax;

    .line 13602
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/afn;->m:Lcom/google/n/aw;

    .line 14401
    new-instance v0, Lcom/google/r/b/a/afn;

    invoke-direct {v0}, Lcom/google/r/b/a/afn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afn;->j:Lcom/google/r/b/a/afn;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12725
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 13499
    iput-byte v0, p0, Lcom/google/r/b/a/afn;->k:B

    .line 13557
    iput v0, p0, Lcom/google/r/b/a/afn;->l:I

    .line 12726
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    .line 12727
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afn;->g:Ljava/lang/Object;

    .line 12728
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    .line 12729
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v3, 0x0

    .line 12735
    invoke-direct {p0}, Lcom/google/r/b/a/afn;-><init>()V

    .line 12738
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v6

    move v4, v0

    move v1, v0

    .line 12741
    :cond_0
    :goto_0
    if-nez v4, :cond_8

    .line 12742
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 12743
    sparse-switch v0, :sswitch_data_0

    .line 12748
    invoke-virtual {v6, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v5

    .line 12750
    goto :goto_0

    :sswitch_0
    move v4, v5

    .line 12746
    goto :goto_0

    .line 12756
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_e

    .line 12757
    iget-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    invoke-static {}, Lcom/google/r/b/a/afb;->newBuilder()Lcom/google/r/b/a/afd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/afd;->a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;

    move-result-object v0

    move-object v2, v0

    .line 12759
    :goto_1
    sget-object v0, Lcom/google/r/b/a/afb;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afb;

    iput-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    .line 12760
    if-eqz v2, :cond_1

    .line 12761
    iget-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/afd;->a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;

    .line 12762
    invoke-virtual {v2}, Lcom/google/r/b/a/afd;->c()Lcom/google/r/b/a/afb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    .line 12764
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/afn;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 12841
    :catch_0
    move-exception v0

    .line 12842
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12847
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_2

    .line 12848
    iget-object v1, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    .line 12850
    :cond_2
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afn;->au:Lcom/google/n/bn;

    throw v0

    .line 12768
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v7, :cond_3

    .line 12769
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    .line 12770
    or-int/lit8 v1, v1, 0x2

    .line 12772
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/afq;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 12843
    :catch_1
    move-exception v0

    .line 12844
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 12845
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 12777
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_d

    .line 12778
    iget-object v0, p0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    invoke-static {}, Lcom/google/maps/a/a;->newBuilder()Lcom/google/maps/a/c;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/a/c;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    move-result-object v0

    move-object v2, v0

    .line 12780
    :goto_2
    sget-object v0, Lcom/google/maps/a/a;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    iput-object v0, p0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    .line 12781
    if-eqz v2, :cond_4

    .line 12782
    iget-object v0, p0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    invoke-virtual {v2, v0}, Lcom/google/maps/a/c;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    .line 12783
    invoke-virtual {v2}, Lcom/google/maps/a/c;->c()Lcom/google/maps/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    .line 12785
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/afn;->a:I

    goto/16 :goto_0

    .line 12789
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 12790
    iget v2, p0, Lcom/google/r/b/a/afn;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/afn;->a:I

    .line 12791
    iput-object v0, p0, Lcom/google/r/b/a/afn;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 12795
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 12796
    iget v2, p0, Lcom/google/r/b/a/afn;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/afn;->a:I

    .line 12797
    iput-object v0, p0, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 12802
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_c

    .line 12803
    iget-object v0, p0, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    invoke-static {v0}, Lcom/google/r/b/a/afu;->a(Lcom/google/r/b/a/afu;)Lcom/google/r/b/a/afw;

    move-result-object v0

    move-object v2, v0

    .line 12805
    :goto_3
    sget-object v0, Lcom/google/r/b/a/afu;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afu;

    iput-object v0, p0, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    .line 12806
    if-eqz v2, :cond_5

    .line 12807
    iget-object v0, p0, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/afw;->a(Lcom/google/r/b/a/afu;)Lcom/google/r/b/a/afw;

    .line 12808
    new-instance v0, Lcom/google/r/b/a/afu;

    invoke-direct {v0, v2}, Lcom/google/r/b/a/afu;-><init>(Lcom/google/n/v;)V

    iput-object v0, p0, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    .line 12810
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/afn;->a:I

    goto/16 :goto_0

    .line 12815
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_b

    .line 12816
    iget-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    invoke-static {}, Lcom/google/maps/g/jy;->newBuilder()Lcom/google/maps/g/kc;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/kc;->a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;

    move-result-object v0

    move-object v2, v0

    .line 12818
    :goto_4
    sget-object v0, Lcom/google/maps/g/jy;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    iput-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    .line 12819
    if-eqz v2, :cond_6

    .line 12820
    iget-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/kc;->a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;

    .line 12821
    invoke-virtual {v2}, Lcom/google/maps/g/kc;->c()Lcom/google/maps/g/jy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    .line 12823
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/afn;->a:I

    goto/16 :goto_0

    .line 12828
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_a

    .line 12829
    iget-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    invoke-static {}, Lcom/google/r/b/a/aag;->newBuilder()Lcom/google/r/b/a/aai;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/aai;->a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;

    move-result-object v0

    move-object v2, v0

    .line 12831
    :goto_5
    sget-object v0, Lcom/google/r/b/a/aag;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aag;

    iput-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    .line 12832
    if-eqz v2, :cond_7

    .line 12833
    iget-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/aai;->a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;

    .line 12834
    invoke-virtual {v2}, Lcom/google/r/b/a/aai;->c()Lcom/google/r/b/a/aag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    .line 12836
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/afn;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 12847
    :cond_8
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_9

    .line 12848
    iget-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    .line 12850
    :cond_9
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->au:Lcom/google/n/bn;

    .line 12851
    return-void

    :cond_a
    move-object v2, v3

    goto :goto_5

    :cond_b
    move-object v2, v3

    goto :goto_4

    :cond_c
    move-object v2, v3

    goto/16 :goto_3

    :cond_d
    move-object v2, v3

    goto/16 :goto_2

    :cond_e
    move-object v2, v3

    goto/16 :goto_1

    .line 12743
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12723
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 13499
    iput-byte v0, p0, Lcom/google/r/b/a/afn;->k:B

    .line 13557
    iput v0, p0, Lcom/google/r/b/a/afn;->l:I

    .line 12724
    return-void
.end method

.method public static a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;
    .locals 1

    .prologue
    .line 13667
    invoke-static {}, Lcom/google/r/b/a/afn;->newBuilder()Lcom/google/r/b/a/afp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/afp;->a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lcom/google/r/b/a/afn;
    .locals 1

    .prologue
    .line 14404
    sget-object v0, Lcom/google/r/b/a/afn;->j:Lcom/google/r/b/a/afn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/afp;
    .locals 1

    .prologue
    .line 13664
    new-instance v0, Lcom/google/r/b/a/afp;

    invoke-direct {v0}, Lcom/google/r/b/a/afp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12865
    sget-object v0, Lcom/google/r/b/a/afn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 13529
    invoke-virtual {p0}, Lcom/google/r/b/a/afn;->c()I

    .line 13530
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 13531
    iget-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 13533
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 13534
    iget-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 13533
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 13531
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto :goto_0

    .line 13536
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v2, :cond_3

    .line 13537
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 13539
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 13540
    iget-object v0, p0, Lcom/google/r/b/a/afn;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->g:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13542
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 13543
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13545
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_6

    .line 13546
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/r/b/a/afu;->d()Lcom/google/r/b/a/afu;

    move-result-object v0

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 13548
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_7

    .line 13549
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v0

    :goto_6
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 13551
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 13552
    iget-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v0

    :goto_7
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 13554
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/afn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 13555
    return-void

    .line 13537
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    goto :goto_2

    .line 13540
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 13543
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 13546
    :cond_c
    iget-object v0, p0, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    goto :goto_5

    .line 13549
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    goto :goto_6

    .line 13552
    :cond_e
    iget-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    goto :goto_7
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 13501
    iget-byte v0, p0, Lcom/google/r/b/a/afn;->k:B

    .line 13502
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 13524
    :goto_0
    return v0

    .line 13503
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 13505
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 13506
    iget-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/r/b/a/afb;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 13507
    iput-byte v2, p0, Lcom/google/r/b/a/afn;->k:B

    move v0, v2

    .line 13508
    goto :goto_0

    :cond_2
    move v0, v2

    .line 13505
    goto :goto_1

    .line 13506
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto :goto_2

    .line 13511
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 13512
    iget-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 13513
    iput-byte v2, p0, Lcom/google/r/b/a/afn;->k:B

    move v0, v2

    .line 13514
    goto :goto_0

    :cond_5
    move v0, v2

    .line 13511
    goto :goto_3

    .line 13512
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    goto :goto_4

    .line 13517
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    .line 13518
    iget-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, Lcom/google/r/b/a/aag;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 13519
    iput-byte v2, p0, Lcom/google/r/b/a/afn;->k:B

    move v0, v2

    .line 13520
    goto :goto_0

    :cond_8
    move v0, v2

    .line 13517
    goto :goto_5

    .line 13518
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    goto :goto_6

    .line 13523
    :cond_a
    iput-byte v1, p0, Lcom/google/r/b/a/afn;->k:B

    move v0, v1

    .line 13524
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 13559
    iget v0, p0, Lcom/google/r/b/a/afn;->l:I

    .line 13560
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 13597
    :goto_0
    return v0

    .line 13563
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 13565
    iget-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 13567
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 13568
    iget-object v0, p0, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    .line 13569
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 13567
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 13565
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto :goto_1

    .line 13571
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_3

    .line 13572
    const/4 v2, 0x3

    .line 13573
    iget-object v0, p0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v0

    :goto_4
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 13575
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 13577
    iget-object v0, p0, Lcom/google/r/b/a/afn;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 13579
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 13580
    const/4 v2, 0x5

    .line 13581
    iget-object v0, p0, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 13583
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_6

    .line 13584
    const/4 v2, 0x6

    .line 13585
    iget-object v0, p0, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/r/b/a/afu;->d()Lcom/google/r/b/a/afu;

    move-result-object v0

    :goto_7
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 13587
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_7

    .line 13588
    const/4 v2, 0x7

    .line 13589
    iget-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v0

    :goto_8
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 13591
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_8

    .line 13593
    iget-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v0

    :goto_9
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 13595
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/afn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 13596
    iput v0, p0, Lcom/google/r/b/a/afn;->l:I

    goto/16 :goto_0

    .line 13573
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    goto/16 :goto_4

    .line 13577
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 13581
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 13585
    :cond_c
    iget-object v0, p0, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    goto :goto_7

    .line 13589
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    goto :goto_8

    .line 13593
    :cond_e
    iget-object v0, p0, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    goto :goto_9

    :cond_f
    move v0, v1

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13454
    iget-object v0, p0, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    .line 13455
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13456
    check-cast v0, Ljava/lang/String;

    .line 13464
    :goto_0
    return-object v0

    .line 13458
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 13460
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 13461
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13462
    iput-object v1, p0, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 13464
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12717
    invoke-static {}, Lcom/google/r/b/a/afn;->newBuilder()Lcom/google/r/b/a/afp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/afp;->a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12717
    invoke-static {}, Lcom/google/r/b/a/afn;->newBuilder()Lcom/google/r/b/a/afp;

    move-result-object v0

    return-object v0
.end method
