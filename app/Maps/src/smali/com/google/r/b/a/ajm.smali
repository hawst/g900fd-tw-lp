.class public final Lcom/google/r/b/a/ajm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ajn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ajk;",
        "Lcom/google/r/b/a/ajm;",
        ">;",
        "Lcom/google/r/b/a/ajn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4013
    sget-object v0, Lcom/google/r/b/a/ajk;->d:Lcom/google/r/b/a/ajk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4071
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    .line 4014
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4005
    new-instance v2, Lcom/google/r/b/a/ajk;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ajk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ajm;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/r/b/a/ajm;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/ajm;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/ajm;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/ajm;->c:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/ajk;->c:Z

    iput v0, v2, Lcom/google/r/b/a/ajk;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4005
    check-cast p1, Lcom/google/r/b/a/ajk;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ajm;->a(Lcom/google/r/b/a/ajk;)Lcom/google/r/b/a/ajm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ajk;)Lcom/google/r/b/a/ajm;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 4046
    invoke-static {}, Lcom/google/r/b/a/ajk;->d()Lcom/google/r/b/a/ajk;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 4061
    :goto_0
    return-object p0

    .line 4047
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 4048
    iget-object v1, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4049
    iget-object v1, p1, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    .line 4050
    iget v1, p0, Lcom/google/r/b/a/ajm;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/ajm;->a:I

    .line 4057
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/r/b/a/ajk;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_2
    if-eqz v0, :cond_2

    .line 4058
    iget-boolean v0, p1, Lcom/google/r/b/a/ajk;->c:Z

    iget v1, p0, Lcom/google/r/b/a/ajm;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/ajm;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/ajm;->c:Z

    .line 4060
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/ajk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 4052
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/ajm;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ajm;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/ajm;->a:I

    .line 4053
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/ajm;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 4057
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4065
    const/4 v0, 0x1

    return v0
.end method
