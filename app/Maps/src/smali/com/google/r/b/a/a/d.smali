.class public final Lcom/google/r/b/a/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/a/b;",
        "Lcom/google/r/b/a/a/d;",
        ">;",
        "Lcom/google/r/b/a/a/e;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:J

.field public d:J

.field public e:Lcom/google/n/ao;

.field public f:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 860
    sget-object v0, Lcom/google/r/b/a/a/b;->g:Lcom/google/r/b/a/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 941
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/a/d;->b:Ljava/lang/Object;

    .line 1081
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/d;->e:Lcom/google/n/ao;

    .line 861
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 852
    new-instance v2, Lcom/google/r/b/a/a/b;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/a/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, p0, Lcom/google/r/b/a/a/d;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/r/b/a/a/d;->c:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/b;->c:J

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/r/b/a/a/d;->d:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/b;->d:J

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/a/b;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/a/d;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/a/d;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/r/b/a/a/d;->f:J

    iput-wide v4, v2, Lcom/google/r/b/a/a/b;->f:J

    iput v0, v2, Lcom/google/r/b/a/a/b;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 852
    check-cast p1, Lcom/google/r/b/a/a/b;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/a/d;->a(Lcom/google/r/b/a/a/b;)Lcom/google/r/b/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/a/b;)Lcom/google/r/b/a/a/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 912
    invoke-static {}, Lcom/google/r/b/a/a/b;->d()Lcom/google/r/b/a/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 932
    :goto_0
    return-object p0

    .line 913
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 914
    iget v2, p0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/a/d;->a:I

    .line 915
    iget-object v2, p1, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/a/d;->b:Ljava/lang/Object;

    .line 918
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 919
    iget-wide v2, p1, Lcom/google/r/b/a/a/b;->c:J

    iget v4, p0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/a/d;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/a/d;->c:J

    .line 921
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 922
    iget-wide v2, p1, Lcom/google/r/b/a/a/b;->d:J

    iget v4, p0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/a/d;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/a/d;->d:J

    .line 924
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 925
    iget-object v2, p0, Lcom/google/r/b/a/a/d;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/a/b;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 926
    iget v2, p0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/a/d;->a:I

    .line 928
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 929
    iget-wide v0, p1, Lcom/google/r/b/a/a/b;->f:J

    iget v2, p0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/a/d;->a:I

    iput-wide v0, p0, Lcom/google/r/b/a/a/d;->f:J

    .line 931
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 913
    goto :goto_1

    :cond_7
    move v2, v1

    .line 918
    goto :goto_2

    :cond_8
    move v2, v1

    .line 921
    goto :goto_3

    :cond_9
    move v2, v1

    .line 924
    goto :goto_4

    :cond_a
    move v0, v1

    .line 928
    goto :goto_5
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 936
    const/4 v0, 0x1

    return v0
.end method
