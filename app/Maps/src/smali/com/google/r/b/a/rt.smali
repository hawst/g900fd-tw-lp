.class public final Lcom/google/r/b/a/rt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/rw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/rr;",
        "Lcom/google/r/b/a/rt;",
        ">;",
        "Lcom/google/r/b/a/rw;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field public g:Lcom/google/n/ao;

.field public h:Ljava/lang/Object;

.field public i:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 655
    sget-object v0, Lcom/google/r/b/a/rr;->j:Lcom/google/r/b/a/rr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 773
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/rt;->b:I

    .line 809
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rt;->c:Ljava/lang/Object;

    .line 885
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rt;->d:Ljava/lang/Object;

    .line 961
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rt;->e:Ljava/lang/Object;

    .line 1037
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rt;->f:Ljava/lang/Object;

    .line 1113
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/rt;->g:Lcom/google/n/ao;

    .line 1172
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rt;->h:Ljava/lang/Object;

    .line 1248
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/rt;->i:Ljava/lang/Object;

    .line 656
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 647
    new-instance v2, Lcom/google/r/b/a/rr;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/rr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/rt;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/rt;->b:I

    iput v4, v2, Lcom/google/r/b/a/rr;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/rt;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/rr;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/rt;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/rr;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/rt;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/rr;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/rt;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/rr;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/rr;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/rt;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/rt;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/rt;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/rr;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/rt;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/rr;->i:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/rr;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 647
    check-cast p1, Lcom/google/r/b/a/rr;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/rt;->a(Lcom/google/r/b/a/rr;)Lcom/google/r/b/a/rt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/rr;)Lcom/google/r/b/a/rt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 725
    invoke-static {}, Lcom/google/r/b/a/rr;->d()Lcom/google/r/b/a/rr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 764
    :goto_0
    return-object p0

    .line 726
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 727
    iget v2, p1, Lcom/google/r/b/a/rr;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/ru;->a(I)Lcom/google/r/b/a/ru;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/ru;->a:Lcom/google/r/b/a/ru;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 726
    goto :goto_1

    .line 727
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/rt;->a:I

    iget v2, v2, Lcom/google/r/b/a/ru;->e:I

    iput v2, p0, Lcom/google/r/b/a/rt;->b:I

    .line 729
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 730
    iget v2, p0, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/rt;->a:I

    .line 731
    iget-object v2, p1, Lcom/google/r/b/a/rr;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/rt;->c:Ljava/lang/Object;

    .line 734
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 735
    iget v2, p0, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/rt;->a:I

    .line 736
    iget-object v2, p1, Lcom/google/r/b/a/rr;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/rt;->d:Ljava/lang/Object;

    .line 739
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 740
    iget v2, p0, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/rt;->a:I

    .line 741
    iget-object v2, p1, Lcom/google/r/b/a/rr;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/rt;->e:Ljava/lang/Object;

    .line 744
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 745
    iget v2, p0, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/rt;->a:I

    .line 746
    iget-object v2, p1, Lcom/google/r/b/a/rr;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/rt;->f:Ljava/lang/Object;

    .line 749
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 750
    iget-object v2, p0, Lcom/google/r/b/a/rt;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/rr;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 751
    iget v2, p0, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/rt;->a:I

    .line 753
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/rr;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 754
    iget v2, p0, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/rt;->a:I

    .line 755
    iget-object v2, p1, Lcom/google/r/b/a/rr;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/rt;->h:Ljava/lang/Object;

    .line 758
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/rr;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_b

    .line 759
    iget v0, p0, Lcom/google/r/b/a/rt;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/rt;->a:I

    .line 760
    iget-object v0, p1, Lcom/google/r/b/a/rr;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/rt;->i:Ljava/lang/Object;

    .line 763
    :cond_b
    iget-object v0, p1, Lcom/google/r/b/a/rr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 729
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 734
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 739
    goto :goto_4

    :cond_f
    move v2, v1

    .line 744
    goto :goto_5

    :cond_10
    move v2, v1

    .line 749
    goto :goto_6

    :cond_11
    move v2, v1

    .line 753
    goto :goto_7

    :cond_12
    move v0, v1

    .line 758
    goto :goto_8
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 768
    const/4 v0, 0x1

    return v0
.end method
