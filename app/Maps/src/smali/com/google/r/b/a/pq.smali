.class public final Lcom/google/r/b/a/pq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/pr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/po;",
        "Lcom/google/r/b/a/pq;",
        ">;",
        "Lcom/google/r/b/a/pr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:J

.field private f:Ljava/lang/Object;

.field private g:J

.field private h:Ljava/lang/Object;

.field private i:Z

.field private j:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 487
    sget-object v0, Lcom/google/r/b/a/po;->k:Lcom/google/r/b/a/po;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 603
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/r/b/a/pq;->b:I

    .line 635
    const v0, 0xdbba00

    iput v0, p0, Lcom/google/r/b/a/pq;->c:I

    .line 667
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/r/b/a/pq;->d:I

    .line 699
    const-wide/32 v0, 0x9d21

    iput-wide v0, p0, Lcom/google/r/b/a/pq;->e:J

    .line 731
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/pq;->f:Ljava/lang/Object;

    .line 807
    const-wide/16 v0, 0x3eda

    iput-wide v0, p0, Lcom/google/r/b/a/pq;->g:J

    .line 839
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/pq;->h:Ljava/lang/Object;

    .line 915
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/pq;->i:Z

    .line 488
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 479
    new-instance v2, Lcom/google/r/b/a/po;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/po;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/pq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/pq;->b:I

    iput v1, v2, Lcom/google/r/b/a/po;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/pq;->c:I

    iput v1, v2, Lcom/google/r/b/a/po;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/pq;->d:I

    iput v1, v2, Lcom/google/r/b/a/po;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/r/b/a/pq;->e:J

    iput-wide v4, v2, Lcom/google/r/b/a/po;->e:J

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/pq;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/po;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-wide v4, p0, Lcom/google/r/b/a/pq;->g:J

    iput-wide v4, v2, Lcom/google/r/b/a/po;->g:J

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/pq;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/po;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v1, p0, Lcom/google/r/b/a/pq;->i:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/po;->i:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v1, p0, Lcom/google/r/b/a/pq;->j:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/po;->j:Z

    iput v0, v2, Lcom/google/r/b/a/po;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 479
    check-cast p1, Lcom/google/r/b/a/po;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/pq;->a(Lcom/google/r/b/a/po;)Lcom/google/r/b/a/pq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/po;)Lcom/google/r/b/a/pq;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 561
    invoke-static {}, Lcom/google/r/b/a/po;->d()Lcom/google/r/b/a/po;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 594
    :goto_0
    return-object p0

    .line 562
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 563
    iget v2, p1, Lcom/google/r/b/a/po;->b:I

    iget v3, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/pq;->a:I

    iput v2, p0, Lcom/google/r/b/a/pq;->b:I

    .line 565
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 566
    iget v2, p1, Lcom/google/r/b/a/po;->c:I

    iget v3, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/pq;->a:I

    iput v2, p0, Lcom/google/r/b/a/pq;->c:I

    .line 568
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 569
    iget v2, p1, Lcom/google/r/b/a/po;->d:I

    iget v3, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/pq;->a:I

    iput v2, p0, Lcom/google/r/b/a/pq;->d:I

    .line 571
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 572
    iget-wide v2, p1, Lcom/google/r/b/a/po;->e:J

    iget v4, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/pq;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/pq;->e:J

    .line 574
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 575
    iget v2, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/pq;->a:I

    .line 576
    iget-object v2, p1, Lcom/google/r/b/a/po;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/pq;->f:Ljava/lang/Object;

    .line 579
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 580
    iget-wide v2, p1, Lcom/google/r/b/a/po;->g:J

    iget v4, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/pq;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/pq;->g:J

    .line 582
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 583
    iget v2, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/pq;->a:I

    .line 584
    iget-object v2, p1, Lcom/google/r/b/a/po;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/pq;->h:Ljava/lang/Object;

    .line 587
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 588
    iget-boolean v2, p1, Lcom/google/r/b/a/po;->i:Z

    iget v3, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/pq;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/pq;->i:Z

    .line 590
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/po;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_12

    :goto_9
    if-eqz v0, :cond_9

    .line 591
    iget-boolean v0, p1, Lcom/google/r/b/a/po;->j:Z

    iget v1, p0, Lcom/google/r/b/a/pq;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/r/b/a/pq;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/pq;->j:Z

    .line 593
    :cond_9
    iget-object v0, p1, Lcom/google/r/b/a/po;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 562
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 565
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 568
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 571
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 574
    goto :goto_5

    :cond_f
    move v2, v1

    .line 579
    goto :goto_6

    :cond_10
    move v2, v1

    .line 582
    goto :goto_7

    :cond_11
    move v2, v1

    .line 587
    goto :goto_8

    :cond_12
    move v0, v1

    .line 590
    goto :goto_9
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 598
    const/4 v0, 0x1

    return v0
.end method
