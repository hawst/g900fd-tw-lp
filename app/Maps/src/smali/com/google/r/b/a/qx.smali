.class public final Lcom/google/r/b/a/qx;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/qv;",
        "Lcom/google/r/b/a/qx;",
        ">;",
        "Lcom/google/r/b/a/qy;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lcom/google/r/b/a/qv;->j:Lcom/google/r/b/a/qv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 506
    const v0, 0x2a300

    iput v0, p0, Lcom/google/r/b/a/qx;->b:I

    .line 538
    const v0, 0x15180

    iput v0, p0, Lcom/google/r/b/a/qx;->c:I

    .line 570
    const/16 v0, 0x5460

    iput v0, p0, Lcom/google/r/b/a/qx;->d:I

    .line 602
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/r/b/a/qx;->e:I

    .line 634
    const v0, 0x69780

    iput v0, p0, Lcom/google/r/b/a/qx;->f:I

    .line 666
    const/16 v0, 0x19

    iput v0, p0, Lcom/google/r/b/a/qx;->g:I

    .line 698
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/qx;->h:I

    .line 730
    const/16 v0, 0x2710

    iput v0, p0, Lcom/google/r/b/a/qx;->i:I

    .line 404
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 395
    new-instance v2, Lcom/google/r/b/a/qv;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/qv;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/qx;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/qx;->b:I

    iput v1, v2, Lcom/google/r/b/a/qv;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/qx;->c:I

    iput v1, v2, Lcom/google/r/b/a/qv;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/qx;->d:I

    iput v1, v2, Lcom/google/r/b/a/qv;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/qx;->e:I

    iput v1, v2, Lcom/google/r/b/a/qv;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/r/b/a/qx;->f:I

    iput v1, v2, Lcom/google/r/b/a/qv;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/r/b/a/qx;->g:I

    iput v1, v2, Lcom/google/r/b/a/qv;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/r/b/a/qx;->h:I

    iput v1, v2, Lcom/google/r/b/a/qv;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/r/b/a/qx;->i:I

    iput v1, v2, Lcom/google/r/b/a/qv;->i:I

    iput v0, v2, Lcom/google/r/b/a/qv;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 395
    check-cast p1, Lcom/google/r/b/a/qv;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/qx;->a(Lcom/google/r/b/a/qv;)Lcom/google/r/b/a/qx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/qv;)Lcom/google/r/b/a/qx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 471
    invoke-static {}, Lcom/google/r/b/a/qv;->d()Lcom/google/r/b/a/qv;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 497
    :goto_0
    return-object p0

    .line 472
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 473
    iget v2, p1, Lcom/google/r/b/a/qv;->b:I

    iget v3, p0, Lcom/google/r/b/a/qx;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/qx;->a:I

    iput v2, p0, Lcom/google/r/b/a/qx;->b:I

    .line 475
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 476
    iget v2, p1, Lcom/google/r/b/a/qv;->c:I

    iget v3, p0, Lcom/google/r/b/a/qx;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/qx;->a:I

    iput v2, p0, Lcom/google/r/b/a/qx;->c:I

    .line 478
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 479
    iget v2, p1, Lcom/google/r/b/a/qv;->d:I

    iget v3, p0, Lcom/google/r/b/a/qx;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/qx;->a:I

    iput v2, p0, Lcom/google/r/b/a/qx;->d:I

    .line 481
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 482
    iget v2, p1, Lcom/google/r/b/a/qv;->e:I

    iget v3, p0, Lcom/google/r/b/a/qx;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/qx;->a:I

    iput v2, p0, Lcom/google/r/b/a/qx;->e:I

    .line 484
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 485
    iget v2, p1, Lcom/google/r/b/a/qv;->f:I

    iget v3, p0, Lcom/google/r/b/a/qx;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/qx;->a:I

    iput v2, p0, Lcom/google/r/b/a/qx;->f:I

    .line 487
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 488
    iget v2, p1, Lcom/google/r/b/a/qv;->g:I

    iget v3, p0, Lcom/google/r/b/a/qx;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/qx;->a:I

    iput v2, p0, Lcom/google/r/b/a/qx;->g:I

    .line 490
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/qv;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 491
    iget v2, p1, Lcom/google/r/b/a/qv;->h:I

    iget v3, p0, Lcom/google/r/b/a/qx;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/qx;->a:I

    iput v2, p0, Lcom/google/r/b/a/qx;->h:I

    .line 493
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/qv;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 494
    iget v0, p1, Lcom/google/r/b/a/qv;->i:I

    iget v1, p0, Lcom/google/r/b/a/qx;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/qx;->a:I

    iput v0, p0, Lcom/google/r/b/a/qx;->i:I

    .line 496
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/qv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 472
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 475
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 478
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 481
    goto :goto_4

    :cond_d
    move v2, v1

    .line 484
    goto :goto_5

    :cond_e
    move v2, v1

    .line 487
    goto :goto_6

    :cond_f
    move v2, v1

    .line 490
    goto :goto_7

    :cond_10
    move v0, v1

    .line 493
    goto :goto_8
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 501
    const/4 v0, 0x1

    return v0
.end method
