.class public final Lcom/google/r/b/a/abe;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/abh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/abe;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/abe;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2663
    new-instance v0, Lcom/google/r/b/a/abf;

    invoke-direct {v0}, Lcom/google/r/b/a/abf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/abe;->PARSER:Lcom/google/n/ax;

    .line 2787
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/abe;->g:Lcom/google/n/aw;

    .line 3133
    new-instance v0, Lcom/google/r/b/a/abe;

    invoke-direct {v0}, Lcom/google/r/b/a/abe;-><init>()V

    sput-object v0, Lcom/google/r/b/a/abe;->d:Lcom/google/r/b/a/abe;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2606
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2723
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/abe;->c:Lcom/google/n/ao;

    .line 2738
    iput-byte v2, p0, Lcom/google/r/b/a/abe;->e:B

    .line 2766
    iput v2, p0, Lcom/google/r/b/a/abe;->f:I

    .line 2607
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    .line 2608
    iget-object v0, p0, Lcom/google/r/b/a/abe;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 2609
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 2615
    invoke-direct {p0}, Lcom/google/r/b/a/abe;-><init>()V

    .line 2618
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 2621
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 2622
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 2623
    sparse-switch v4, :sswitch_data_0

    .line 2628
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 2630
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 2626
    goto :goto_0

    .line 2635
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 2636
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    .line 2638
    or-int/lit8 v1, v1, 0x1

    .line 2640
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 2641
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 2640
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2651
    :catch_0
    move-exception v0

    .line 2652
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2657
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 2658
    iget-object v1, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    .line 2660
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/abe;->au:Lcom/google/n/bn;

    throw v0

    .line 2645
    :sswitch_2
    :try_start_2
    iget-object v4, p0, Lcom/google/r/b/a/abe;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 2646
    iget v4, p0, Lcom/google/r/b/a/abe;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/abe;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2653
    :catch_1
    move-exception v0

    .line 2654
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 2655
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2657
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 2658
    iget-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    .line 2660
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abe;->au:Lcom/google/n/bn;

    .line 2661
    return-void

    .line 2623
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2604
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2723
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/abe;->c:Lcom/google/n/ao;

    .line 2738
    iput-byte v1, p0, Lcom/google/r/b/a/abe;->e:B

    .line 2766
    iput v1, p0, Lcom/google/r/b/a/abe;->f:I

    .line 2605
    return-void
.end method

.method public static d()Lcom/google/r/b/a/abe;
    .locals 1

    .prologue
    .line 3136
    sget-object v0, Lcom/google/r/b/a/abe;->d:Lcom/google/r/b/a/abe;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/abg;
    .locals 1

    .prologue
    .line 2849
    new-instance v0, Lcom/google/r/b/a/abg;

    invoke-direct {v0}, Lcom/google/r/b/a/abg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/abe;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2675
    sget-object v0, Lcom/google/r/b/a/abe;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2756
    invoke-virtual {p0}, Lcom/google/r/b/a/abe;->c()I

    .line 2757
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2758
    iget-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2757
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2760
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/abe;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 2761
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/r/b/a/abe;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2763
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/abe;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2764
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2740
    iget-byte v0, p0, Lcom/google/r/b/a/abe;->e:B

    .line 2741
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 2751
    :cond_0
    :goto_0
    return v2

    .line 2742
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 2744
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2745
    iget-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aba;->d()Lcom/google/r/b/a/aba;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aba;

    invoke-virtual {v0}, Lcom/google/r/b/a/aba;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2746
    iput-byte v2, p0, Lcom/google/r/b/a/abe;->e:B

    goto :goto_0

    .line 2744
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2750
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/abe;->e:B

    move v2, v3

    .line 2751
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2768
    iget v0, p0, Lcom/google/r/b/a/abe;->f:I

    .line 2769
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2782
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 2772
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2773
    iget-object v0, p0, Lcom/google/r/b/a/abe;->b:Ljava/util/List;

    .line 2774
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2772
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2776
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/abe;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 2777
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/r/b/a/abe;->c:Lcom/google/n/ao;

    .line 2778
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 2780
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/abe;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2781
    iput v0, p0, Lcom/google/r/b/a/abe;->f:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2598
    invoke-static {}, Lcom/google/r/b/a/abe;->newBuilder()Lcom/google/r/b/a/abg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/abg;->a(Lcom/google/r/b/a/abe;)Lcom/google/r/b/a/abg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2598
    invoke-static {}, Lcom/google/r/b/a/abe;->newBuilder()Lcom/google/r/b/a/abg;

    move-result-object v0

    return-object v0
.end method
