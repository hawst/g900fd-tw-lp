.class public final Lcom/google/r/b/a/ajc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ajd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aja;",
        "Lcom/google/r/b/a/ajc;",
        ">;",
        "Lcom/google/r/b/a/ajd;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3136
    sget-object v0, Lcom/google/r/b/a/aja;->d:Lcom/google/r/b/a/aja;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3190
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ajc;->b:Ljava/lang/Object;

    .line 3266
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ajc;->c:Lcom/google/n/ao;

    .line 3137
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3128
    new-instance v2, Lcom/google/r/b/a/aja;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aja;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ajc;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, p0, Lcom/google/r/b/a/ajc;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v3, v2, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/ajc;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/ajc;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/aja;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3128
    check-cast p1, Lcom/google/r/b/a/aja;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ajc;->a(Lcom/google/r/b/a/aja;)Lcom/google/r/b/a/ajc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aja;)Lcom/google/r/b/a/ajc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3170
    invoke-static {}, Lcom/google/r/b/a/aja;->d()Lcom/google/r/b/a/aja;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3181
    :goto_0
    return-object p0

    .line 3171
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aja;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 3172
    iget v2, p0, Lcom/google/r/b/a/ajc;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ajc;->a:I

    .line 3173
    iget-object v2, p1, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ajc;->b:Ljava/lang/Object;

    .line 3176
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/aja;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 3177
    iget-object v0, p0, Lcom/google/r/b/a/ajc;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3178
    iget v0, p0, Lcom/google/r/b/a/ajc;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ajc;->a:I

    .line 3180
    :cond_2
    iget-object v0, p1, Lcom/google/r/b/a/aja;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 3171
    goto :goto_1

    :cond_4
    move v0, v1

    .line 3176
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3185
    const/4 v0, 0x1

    return v0
.end method
