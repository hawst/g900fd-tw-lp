.class public final Lcom/google/r/b/a/aca;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/acb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aby;",
        "Lcom/google/r/b/a/aca;",
        ">;",
        "Lcom/google/r/b/a/acb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 17973
    sget-object v0, Lcom/google/r/b/a/aby;->e:Lcom/google/r/b/a/aby;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 18065
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aca;->c:Ljava/lang/Object;

    .line 18141
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aca;->d:I

    .line 17974
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 17965
    invoke-virtual {p0}, Lcom/google/r/b/a/aca;->c()Lcom/google/r/b/a/aby;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 17965
    check-cast p1, Lcom/google/r/b/a/aby;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aca;->a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aby;)Lcom/google/r/b/a/aca;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 18011
    invoke-static {}, Lcom/google/r/b/a/aby;->d()Lcom/google/r/b/a/aby;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 18024
    :goto_0
    return-object p0

    .line 18012
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 18013
    iget v2, p1, Lcom/google/r/b/a/aby;->b:I

    iget v3, p0, Lcom/google/r/b/a/aca;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aca;->a:I

    iput v2, p0, Lcom/google/r/b/a/aca;->b:I

    .line 18015
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 18016
    iget v2, p0, Lcom/google/r/b/a/aca;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/aca;->a:I

    .line 18017
    iget-object v2, p1, Lcom/google/r/b/a/aby;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aca;->c:Ljava/lang/Object;

    .line 18020
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/aby;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_8

    .line 18021
    iget v0, p1, Lcom/google/r/b/a/aby;->d:I

    invoke-static {v0}, Lcom/google/r/b/a/acc;->a(I)Lcom/google/r/b/a/acc;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/r/b/a/acc;->a:Lcom/google/r/b/a/acc;

    :cond_3
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 18012
    goto :goto_1

    :cond_5
    move v2, v1

    .line 18015
    goto :goto_2

    :cond_6
    move v0, v1

    .line 18020
    goto :goto_3

    .line 18021
    :cond_7
    iget v1, p0, Lcom/google/r/b/a/aca;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/aca;->a:I

    iget v0, v0, Lcom/google/r/b/a/acc;->d:I

    iput v0, p0, Lcom/google/r/b/a/aca;->d:I

    .line 18023
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/aby;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 18028
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/r/b/a/aby;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 17991
    new-instance v2, Lcom/google/r/b/a/aby;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aby;-><init>(Lcom/google/n/v;)V

    .line 17992
    iget v3, p0, Lcom/google/r/b/a/aca;->a:I

    .line 17993
    const/4 v1, 0x0

    .line 17994
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 17997
    :goto_0
    iget v1, p0, Lcom/google/r/b/a/aca;->b:I

    iput v1, v2, Lcom/google/r/b/a/aby;->b:I

    .line 17998
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 17999
    or-int/lit8 v0, v0, 0x2

    .line 18001
    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/aca;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/aby;->c:Ljava/lang/Object;

    .line 18002
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 18003
    or-int/lit8 v0, v0, 0x4

    .line 18005
    :cond_1
    iget v1, p0, Lcom/google/r/b/a/aca;->d:I

    iput v1, v2, Lcom/google/r/b/a/aby;->d:I

    .line 18006
    iput v0, v2, Lcom/google/r/b/a/aby;->a:I

    .line 18007
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
