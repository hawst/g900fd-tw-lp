.class public final Lcom/google/r/b/a/qn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qn;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/qn;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8328
    new-instance v0, Lcom/google/r/b/a/qo;

    invoke-direct {v0}, Lcom/google/r/b/a/qo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qn;->PARSER:Lcom/google/n/ax;

    .line 8501
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/qn;->h:Lcom/google/n/aw;

    .line 8971
    new-instance v0, Lcom/google/r/b/a/qn;

    invoke-direct {v0}, Lcom/google/r/b/a/qn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qn;->e:Lcom/google/r/b/a/qn;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 8257
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 8445
    iput-byte v0, p0, Lcom/google/r/b/a/qn;->f:B

    .line 8476
    iput v0, p0, Lcom/google/r/b/a/qn;->g:I

    .line 8258
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    .line 8259
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    .line 8260
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/qn;->d:J

    .line 8261
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v2, 0x1

    .line 8267
    invoke-direct {p0}, Lcom/google/r/b/a/qn;-><init>()V

    .line 8270
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 8273
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 8274
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 8275
    sparse-switch v4, :sswitch_data_0

    .line 8280
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 8282
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 8278
    goto :goto_0

    .line 8287
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 8288
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    .line 8290
    or-int/lit8 v1, v1, 0x1

    .line 8292
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 8293
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 8292
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 8313
    :catch_0
    move-exception v0

    .line 8314
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8319
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_2

    .line 8320
    iget-object v2, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    .line 8322
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_3

    .line 8323
    iget-object v1, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    .line 8325
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/qn;->au:Lcom/google/n/bn;

    throw v0

    .line 8297
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_4

    .line 8298
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    .line 8300
    or-int/lit8 v1, v1, 0x2

    .line 8302
    :cond_4
    iget-object v4, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 8303
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 8302
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 8315
    :catch_1
    move-exception v0

    .line 8316
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 8317
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 8307
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/r/b/a/qn;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/qn;->a:I

    .line 8308
    invoke-virtual {p1}, Lcom/google/n/j;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/qn;->d:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 8319
    :cond_5
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_6

    .line 8320
    iget-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    .line 8322
    :cond_6
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_7

    .line 8323
    iget-object v0, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    .line 8325
    :cond_7
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qn;->au:Lcom/google/n/bn;

    .line 8326
    return-void

    .line 8275
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 8255
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 8445
    iput-byte v0, p0, Lcom/google/r/b/a/qn;->f:B

    .line 8476
    iput v0, p0, Lcom/google/r/b/a/qn;->g:I

    .line 8256
    return-void
.end method

.method public static d()Lcom/google/r/b/a/qn;
    .locals 1

    .prologue
    .line 8974
    sget-object v0, Lcom/google/r/b/a/qn;->e:Lcom/google/r/b/a/qn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/qp;
    .locals 1

    .prologue
    .line 8563
    new-instance v0, Lcom/google/r/b/a/qp;

    invoke-direct {v0}, Lcom/google/r/b/a/qp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8340
    sget-object v0, Lcom/google/r/b/a/qn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8463
    invoke-virtual {p0}, Lcom/google/r/b/a/qn;->c()I

    move v1, v2

    .line 8464
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 8465
    iget-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 8464
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 8467
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 8468
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 8467
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 8470
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/qn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 8471
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/r/b/a/qn;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 8473
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/qn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 8474
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8447
    iget-byte v0, p0, Lcom/google/r/b/a/qn;->f:B

    .line 8448
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 8458
    :cond_0
    :goto_0
    return v2

    .line 8449
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 8451
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 8452
    iget-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/qf;->d()Lcom/google/r/b/a/qf;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/qf;

    invoke-virtual {v0}, Lcom/google/r/b/a/qf;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 8453
    iput-byte v2, p0, Lcom/google/r/b/a/qn;->f:B

    goto :goto_0

    .line 8451
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 8457
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/qn;->f:B

    move v2, v3

    .line 8458
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 8478
    iget v0, p0, Lcom/google/r/b/a/qn;->g:I

    .line 8479
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 8496
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 8482
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 8483
    iget-object v0, p0, Lcom/google/r/b/a/qn;->b:Ljava/util/List;

    .line 8484
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 8482
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 8486
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 8487
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/qn;->c:Ljava/util/List;

    .line 8488
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 8486
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 8490
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/qn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 8491
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/r/b/a/qn;->d:J

    .line 8492
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 8494
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/qn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 8495
    iput v0, p0, Lcom/google/r/b/a/qn;->g:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8249
    invoke-static {}, Lcom/google/r/b/a/qn;->newBuilder()Lcom/google/r/b/a/qp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/qp;->a(Lcom/google/r/b/a/qn;)Lcom/google/r/b/a/qp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8249
    invoke-static {}, Lcom/google/r/b/a/qn;->newBuilder()Lcom/google/r/b/a/qp;

    move-result-object v0

    return-object v0
.end method
