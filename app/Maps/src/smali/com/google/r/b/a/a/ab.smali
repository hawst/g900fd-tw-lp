.class public final Lcom/google/r/b/a/a/ab;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/ae;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/ab;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/a/ab;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Lcom/google/n/ao;

.field public e:J

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1591
    new-instance v0, Lcom/google/r/b/a/a/ac;

    invoke-direct {v0}, Lcom/google/r/b/a/a/ac;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/ab;->PARSER:Lcom/google/n/ax;

    .line 1759
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/ab;->i:Lcom/google/n/aw;

    .line 2187
    new-instance v0, Lcom/google/r/b/a/a/ab;

    invoke-direct {v0}, Lcom/google/r/b/a/a/ab;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/ab;->f:Lcom/google/r/b/a/a/ab;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1522
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1666
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/ab;->d:Lcom/google/n/ao;

    .line 1696
    iput-byte v2, p0, Lcom/google/r/b/a/a/ab;->g:B

    .line 1730
    iput v2, p0, Lcom/google/r/b/a/a/ab;->h:I

    .line 1523
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    .line 1524
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/a/ab;->c:I

    .line 1525
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1526
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/a/ab;->e:J

    .line 1527
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1533
    invoke-direct {p0}, Lcom/google/r/b/a/a/ab;-><init>()V

    .line 1536
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 1539
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 1540
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 1541
    sparse-switch v4, :sswitch_data_0

    .line 1546
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 1548
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 1544
    goto :goto_0

    .line 1553
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 1554
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    .line 1556
    or-int/lit8 v1, v1, 0x1

    .line 1558
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 1559
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 1558
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1579
    :catch_0
    move-exception v0

    .line 1580
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1585
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 1586
    iget-object v1, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    .line 1588
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/ab;->au:Lcom/google/n/bn;

    throw v0

    .line 1563
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/a/ab;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/a/ab;->a:I

    .line 1564
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/r/b/a/a/ab;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1581
    :catch_1
    move-exception v0

    .line 1582
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 1583
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1568
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/r/b/a/a/ab;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 1569
    iget v4, p0, Lcom/google/r/b/a/a/ab;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/a/ab;->a:I

    goto :goto_0

    .line 1573
    :sswitch_4
    iget v4, p0, Lcom/google/r/b/a/a/ab;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/a/ab;->a:I

    .line 1574
    invoke-virtual {p1}, Lcom/google/n/j;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/r/b/a/a/ab;->e:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1585
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 1586
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    .line 1588
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/ab;->au:Lcom/google/n/bn;

    .line 1589
    return-void

    .line 1541
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1520
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1666
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/ab;->d:Lcom/google/n/ao;

    .line 1696
    iput-byte v1, p0, Lcom/google/r/b/a/a/ab;->g:B

    .line 1730
    iput v1, p0, Lcom/google/r/b/a/a/ab;->h:I

    .line 1521
    return-void
.end method

.method public static g()Lcom/google/r/b/a/a/ab;
    .locals 1

    .prologue
    .line 2190
    sget-object v0, Lcom/google/r/b/a/a/ab;->f:Lcom/google/r/b/a/a/ab;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/ad;
    .locals 1

    .prologue
    .line 1821
    new-instance v0, Lcom/google/r/b/a/a/ad;

    invoke-direct {v0}, Lcom/google/r/b/a/a/ad;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/ab;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1603
    sget-object v0, Lcom/google/r/b/a/a/ab;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1714
    invoke-virtual {p0}, Lcom/google/r/b/a/a/ab;->c()I

    .line 1715
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1716
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1715
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1718
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 1719
    iget v0, p0, Lcom/google/r/b/a/a/ab;->c:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 1721
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 1722
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/a/ab;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1724
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 1725
    iget-wide v0, p0, Lcom/google/r/b/a/a/ab;->e:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 1727
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1728
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1698
    iget-byte v0, p0, Lcom/google/r/b/a/a/ab;->g:B

    .line 1699
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1709
    :cond_0
    :goto_0
    return v2

    .line 1700
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1702
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1703
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/a/af;->g()Lcom/google/r/b/a/a/af;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    invoke-virtual {v0}, Lcom/google/r/b/a/a/af;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1704
    iput-byte v2, p0, Lcom/google/r/b/a/a/ab;->g:B

    goto :goto_0

    .line 1702
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1708
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/a/ab;->g:B

    move v2, v3

    .line 1709
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1732
    iget v0, p0, Lcom/google/r/b/a/a/ab;->h:I

    .line 1733
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1754
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1736
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1737
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    .line 1738
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1736
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1740
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 1741
    iget v0, p0, Lcom/google/r/b/a/a/ab;->c:I

    .line 1742
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1744
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 1745
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/a/ab;->d:Lcom/google/n/ao;

    .line 1746
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1748
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/a/ab;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 1749
    iget-wide v0, p0, Lcom/google/r/b/a/a/ab;->e:J

    .line 1750
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0, v1}, Lcom/google/n/l;->b(J)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1752
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1753
    iput v0, p0, Lcom/google/r/b/a/a/ab;->h:I

    goto :goto_0

    .line 1742
    :cond_5
    const/16 v0, 0xa

    goto :goto_2
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/a/af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1614
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    .line 1615
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1616
    iget-object v0, p0, Lcom/google/r/b/a/a/ab;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1617
    invoke-static {}, Lcom/google/r/b/a/a/af;->g()Lcom/google/r/b/a/a/af;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1619
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1514
    invoke-static {}, Lcom/google/r/b/a/a/ab;->newBuilder()Lcom/google/r/b/a/a/ad;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/ad;->a(Lcom/google/r/b/a/a/ab;)Lcom/google/r/b/a/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1514
    invoke-static {}, Lcom/google/r/b/a/a/ab;->newBuilder()Lcom/google/r/b/a/a/ad;

    move-result-object v0

    return-object v0
.end method
