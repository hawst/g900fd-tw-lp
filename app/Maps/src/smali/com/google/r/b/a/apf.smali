.class public final Lcom/google/r/b/a/apf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/apg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/apd;",
        "Lcom/google/r/b/a/apf;",
        ">;",
        "Lcom/google/r/b/a/apg;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:J

.field public e:Ljava/lang/Object;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/n/ao;

.field public h:Lcom/google/n/ao;

.field public i:Lcom/google/n/ao;

.field public j:Lcom/google/n/ao;

.field public k:Lcom/google/n/ao;

.field public l:Lcom/google/n/ao;

.field private m:Lcom/google/n/ao;

.field private n:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 900
    sget-object v0, Lcom/google/r/b/a/apd;->o:Lcom/google/r/b/a/apd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->b:Lcom/google/n/ao;

    .line 1183
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    .line 1351
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/apf;->e:Ljava/lang/Object;

    .line 1427
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->m:Lcom/google/n/ao;

    .line 1487
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    .line 1623
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->g:Lcom/google/n/ao;

    .line 1682
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->n:Lcom/google/n/ao;

    .line 1741
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->h:Lcom/google/n/ao;

    .line 1800
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->i:Lcom/google/n/ao;

    .line 1859
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->j:Lcom/google/n/ao;

    .line 1918
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->k:Lcom/google/n/ao;

    .line 1977
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->l:Lcom/google/n/ao;

    .line 901
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 892
    new-instance v2, Lcom/google/r/b/a/apd;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/apd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/apf;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/apf;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/apf;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-wide v4, p0, Lcom/google/r/b/a/apf;->d:J

    iput-wide v4, v2, Lcom/google/r/b/a/apd;->d:J

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/apf;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/apd;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, v2, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/apf;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/apf;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/r/b/a/apf;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/apf;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/apf;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v4, v2, Lcom/google/r/b/a/apd;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/apf;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/apf;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget-object v4, v2, Lcom/google/r/b/a/apd;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/apf;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/apf;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-object v4, v2, Lcom/google/r/b/a/apd;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/apf;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/apf;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x100

    :cond_9
    iget-object v4, v2, Lcom/google/r/b/a/apd;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/apf;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/apf;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x200

    :cond_a
    iget-object v4, v2, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/apf;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/apf;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_b

    or-int/lit16 v0, v0, 0x400

    :cond_b
    iget-object v3, v2, Lcom/google/r/b/a/apd;->n:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/apf;->l:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/apf;->l:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/apd;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 892
    check-cast p1, Lcom/google/r/b/a/apd;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/apf;->a(Lcom/google/r/b/a/apd;)Lcom/google/r/b/a/apf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/maps/g/hy;)Lcom/google/r/b/a/apf;
    .locals 2

    .prologue
    .line 1442
    if-nez p1, :cond_0

    .line 1443
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1445
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/apf;->m:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1447
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1448
    return-object p0
.end method

.method public final a(Lcom/google/r/b/a/apd;)Lcom/google/r/b/a/apf;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1018
    invoke-static {}, Lcom/google/r/b/a/apd;->d()Lcom/google/r/b/a/apd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1084
    :goto_0
    return-object p0

    .line 1019
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_e

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1020
    iget-object v2, p0, Lcom/google/r/b/a/apf;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1021
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1023
    :cond_1
    iget-object v2, p1, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1024
    iget-object v2, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1025
    iget-object v2, p1, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    .line 1026
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1033
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1034
    iget-wide v2, p1, Lcom/google/r/b/a/apd;->d:J

    iget v4, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/apf;->a:I

    iput-wide v2, p0, Lcom/google/r/b/a/apf;->d:J

    .line 1036
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1037
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1038
    iget-object v2, p1, Lcom/google/r/b/a/apd;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/apf;->e:Ljava/lang/Object;

    .line 1041
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1042
    iget-object v2, p0, Lcom/google/r/b/a/apf;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1043
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1045
    :cond_5
    iget-object v2, p1, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1046
    iget-object v2, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1047
    iget-object v2, p1, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    .line 1048
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1055
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1056
    iget-object v2, p0, Lcom/google/r/b/a/apf;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1057
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1059
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 1060
    iget-object v2, p0, Lcom/google/r/b/a/apf;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1061
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1063
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 1064
    iget-object v2, p0, Lcom/google/r/b/a/apf;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1065
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1067
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 1068
    iget-object v2, p0, Lcom/google/r/b/a/apf;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1069
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1071
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 1072
    iget-object v2, p0, Lcom/google/r/b/a/apf;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1073
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1075
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 1076
    iget-object v2, p0, Lcom/google/r/b/a/apf;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1077
    iget v2, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1079
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/apd;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1a

    :goto_d
    if-eqz v0, :cond_d

    .line 1080
    iget-object v0, p0, Lcom/google/r/b/a/apf;->l:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/apd;->n:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1081
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1083
    :cond_d
    iget-object v0, p1, Lcom/google/r/b/a/apd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 1019
    goto/16 :goto_1

    .line 1028
    :cond_f
    invoke-virtual {p0}, Lcom/google/r/b/a/apf;->c()V

    .line 1029
    iget-object v2, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_10
    move v2, v1

    .line 1033
    goto/16 :goto_3

    :cond_11
    move v2, v1

    .line 1036
    goto/16 :goto_4

    :cond_12
    move v2, v1

    .line 1041
    goto/16 :goto_5

    .line 1050
    :cond_13
    invoke-virtual {p0}, Lcom/google/r/b/a/apf;->d()V

    .line 1051
    iget-object v2, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/apd;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_14
    move v2, v1

    .line 1055
    goto/16 :goto_7

    :cond_15
    move v2, v1

    .line 1059
    goto/16 :goto_8

    :cond_16
    move v2, v1

    .line 1063
    goto/16 :goto_9

    :cond_17
    move v2, v1

    .line 1067
    goto/16 :goto_a

    :cond_18
    move v2, v1

    .line 1071
    goto :goto_b

    :cond_19
    move v2, v1

    .line 1075
    goto :goto_c

    :cond_1a
    move v0, v1

    .line 1079
    goto :goto_d
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1088
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1089
    iget-object v0, p0, Lcom/google/r/b/a/apf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b/a/ba;->d()Lcom/google/b/f/b/a/ba;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ba;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/ba;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1118
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1088
    goto :goto_0

    .line 1094
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1095
    iget-object v0, p0, Lcom/google/r/b/a/apf;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 1100
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1101
    iget-object v0, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/cm;->d()Lcom/google/b/f/cm;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/cm;

    invoke-virtual {v0}, Lcom/google/b/f/cm;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 1094
    goto :goto_2

    .line 1106
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_6

    .line 1107
    iget-object v0, p0, Lcom/google/r/b/a/apf;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/ad;->d()Lcom/google/b/f/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/ad;

    invoke-virtual {v0}, Lcom/google/b/f/ad;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1112
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_5
    if-eqz v0, :cond_7

    .line 1113
    iget-object v0, p0, Lcom/google/r/b/a/apf;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aqm;->d()Lcom/google/r/b/a/aqm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aqm;

    invoke-virtual {v0}, Lcom/google/r/b/a/aqm;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_7
    move v2, v3

    .line 1118
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 1106
    goto :goto_4

    :cond_9
    move v0, v2

    .line 1112
    goto :goto_5
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1185
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1186
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->c:Ljava/util/List;

    .line 1189
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1191
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1489
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 1490
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/apf;->f:Ljava/util/List;

    .line 1493
    iget v0, p0, Lcom/google/r/b/a/apf;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/apf;->a:I

    .line 1495
    :cond_0
    return-void
.end method
