.class public final Lcom/google/r/b/a/ajk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ajn;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ajk;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/ajk;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3816
    new-instance v0, Lcom/google/r/b/a/ajl;

    invoke-direct {v0}, Lcom/google/r/b/a/ajl;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ajk;->PARSER:Lcom/google/n/ax;

    .line 3933
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ajk;->g:Lcom/google/n/aw;

    .line 4243
    new-instance v0, Lcom/google/r/b/a/ajk;

    invoke-direct {v0}, Lcom/google/r/b/a/ajk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ajk;->d:Lcom/google/r/b/a/ajk;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3759
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3890
    iput-byte v0, p0, Lcom/google/r/b/a/ajk;->e:B

    .line 3912
    iput v0, p0, Lcom/google/r/b/a/ajk;->f:I

    .line 3760
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    .line 3761
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/ajk;->c:Z

    .line 3762
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 3768
    invoke-direct {p0}, Lcom/google/r/b/a/ajk;-><init>()V

    .line 3771
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 3774
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 3775
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 3776
    sparse-switch v4, :sswitch_data_0

    .line 3781
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 3783
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 3779
    goto :goto_0

    .line 3788
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 3789
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    .line 3791
    or-int/lit8 v1, v1, 0x1

    .line 3793
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 3794
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 3793
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3804
    :catch_0
    move-exception v0

    .line 3805
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3810
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 3811
    iget-object v1, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    .line 3813
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ajk;->au:Lcom/google/n/bn;

    throw v0

    .line 3798
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/ajk;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ajk;->a:I

    .line 3799
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/r/b/a/ajk;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3806
    :catch_1
    move-exception v0

    .line 3807
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 3808
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3810
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 3811
    iget-object v0, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    .line 3813
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ajk;->au:Lcom/google/n/bn;

    .line 3814
    return-void

    .line 3776
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3757
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3890
    iput-byte v0, p0, Lcom/google/r/b/a/ajk;->e:B

    .line 3912
    iput v0, p0, Lcom/google/r/b/a/ajk;->f:I

    .line 3758
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ajk;
    .locals 1

    .prologue
    .line 4246
    sget-object v0, Lcom/google/r/b/a/ajk;->d:Lcom/google/r/b/a/ajk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ajm;
    .locals 1

    .prologue
    .line 3995
    new-instance v0, Lcom/google/r/b/a/ajm;

    invoke-direct {v0}, Lcom/google/r/b/a/ajm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ajk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3828
    sget-object v0, Lcom/google/r/b/a/ajk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3902
    invoke-virtual {p0}, Lcom/google/r/b/a/ajk;->c()I

    .line 3903
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 3904
    iget-object v0, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3903
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3906
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ajk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 3907
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/r/b/a/ajk;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 3909
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/ajk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3910
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3892
    iget-byte v1, p0, Lcom/google/r/b/a/ajk;->e:B

    .line 3893
    if-ne v1, v0, :cond_0

    .line 3897
    :goto_0
    return v0

    .line 3894
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3896
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ajk;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 3914
    iget v0, p0, Lcom/google/r/b/a/ajk;->f:I

    .line 3915
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3928
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 3918
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 3919
    iget-object v0, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    .line 3920
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3918
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 3922
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ajk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 3923
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/r/b/a/ajk;->c:Z

    .line 3924
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 3926
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ajk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 3927
    iput v0, p0, Lcom/google/r/b/a/ajk;->f:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3751
    invoke-static {}, Lcom/google/r/b/a/ajk;->newBuilder()Lcom/google/r/b/a/ajm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ajm;->a(Lcom/google/r/b/a/ajk;)Lcom/google/r/b/a/ajm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3751
    invoke-static {}, Lcom/google/r/b/a/ajk;->newBuilder()Lcom/google/r/b/a/ajm;

    move-result-object v0

    return-object v0
.end method
