.class public final Lcom/google/r/b/a/nm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/nm;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/nm;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field public c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1030
    new-instance v0, Lcom/google/r/b/a/nn;

    invoke-direct {v0}, Lcom/google/r/b/a/nn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/nm;->PARSER:Lcom/google/n/ax;

    .line 1224
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/nm;->g:Lcom/google/n/aw;

    .line 1455
    new-instance v0, Lcom/google/r/b/a/nm;

    invoke-direct {v0}, Lcom/google/r/b/a/nm;-><init>()V

    sput-object v0, Lcom/google/r/b/a/nm;->d:Lcom/google/r/b/a/nm;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 975
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1166
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/nm;->c:Lcom/google/n/ao;

    .line 1181
    iput-byte v2, p0, Lcom/google/r/b/a/nm;->e:B

    .line 1203
    iput v2, p0, Lcom/google/r/b/a/nm;->f:I

    .line 976
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/nm;->b:I

    .line 977
    iget-object v0, p0, Lcom/google/r/b/a/nm;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 978
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 984
    invoke-direct {p0}, Lcom/google/r/b/a/nm;-><init>()V

    .line 985
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 990
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 991
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 992
    sparse-switch v3, :sswitch_data_0

    .line 997
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 999
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 995
    goto :goto_0

    .line 1004
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1005
    invoke-static {v3}, Lcom/google/r/b/a/np;->a(I)Lcom/google/r/b/a/np;

    move-result-object v4

    .line 1006
    if-nez v4, :cond_1

    .line 1007
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1021
    :catch_0
    move-exception v0

    .line 1022
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1027
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/nm;->au:Lcom/google/n/bn;

    throw v0

    .line 1009
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/nm;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/nm;->a:I

    .line 1010
    iput v3, p0, Lcom/google/r/b/a/nm;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1023
    :catch_1
    move-exception v0

    .line 1024
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1025
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1015
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/nm;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1016
    iget v3, p0, Lcom/google/r/b/a/nm;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/nm;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1027
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nm;->au:Lcom/google/n/bn;

    .line 1028
    return-void

    .line 992
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 973
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1166
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/nm;->c:Lcom/google/n/ao;

    .line 1181
    iput-byte v1, p0, Lcom/google/r/b/a/nm;->e:B

    .line 1203
    iput v1, p0, Lcom/google/r/b/a/nm;->f:I

    .line 974
    return-void
.end method

.method public static d()Lcom/google/r/b/a/nm;
    .locals 1

    .prologue
    .line 1458
    sget-object v0, Lcom/google/r/b/a/nm;->d:Lcom/google/r/b/a/nm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/no;
    .locals 1

    .prologue
    .line 1286
    new-instance v0, Lcom/google/r/b/a/no;

    invoke-direct {v0}, Lcom/google/r/b/a/no;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/nm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1042
    sget-object v0, Lcom/google/r/b/a/nm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1193
    invoke-virtual {p0}, Lcom/google/r/b/a/nm;->c()I

    .line 1194
    iget v0, p0, Lcom/google/r/b/a/nm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1195
    iget v0, p0, Lcom/google/r/b/a/nm;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 1197
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/nm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1198
    iget-object v0, p0, Lcom/google/r/b/a/nm;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1200
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/nm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1201
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1183
    iget-byte v1, p0, Lcom/google/r/b/a/nm;->e:B

    .line 1184
    if-ne v1, v0, :cond_0

    .line 1188
    :goto_0
    return v0

    .line 1185
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1187
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/nm;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1205
    iget v0, p0, Lcom/google/r/b/a/nm;->f:I

    .line 1206
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1219
    :goto_0
    return v0

    .line 1209
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/nm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 1210
    iget v0, p0, Lcom/google/r/b/a/nm;->b:I

    .line 1211
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1213
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/nm;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1214
    iget-object v2, p0, Lcom/google/r/b/a/nm;->c:Lcom/google/n/ao;

    .line 1215
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1217
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/nm;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1218
    iput v0, p0, Lcom/google/r/b/a/nm;->f:I

    goto :goto_0

    .line 1211
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 967
    invoke-static {}, Lcom/google/r/b/a/nm;->newBuilder()Lcom/google/r/b/a/no;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/no;->a(Lcom/google/r/b/a/nm;)Lcom/google/r/b/a/no;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 967
    invoke-static {}, Lcom/google/r/b/a/nm;->newBuilder()Lcom/google/r/b/a/no;

    move-result-object v0

    return-object v0
.end method
