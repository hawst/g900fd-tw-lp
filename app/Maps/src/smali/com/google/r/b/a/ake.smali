.class public final Lcom/google/r/b/a/ake;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/akj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ake;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/ake;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:I

.field c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 754
    new-instance v0, Lcom/google/r/b/a/akf;

    invoke-direct {v0}, Lcom/google/r/b/a/akf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ake;->PARSER:Lcom/google/n/ax;

    .line 950
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ake;->h:Lcom/google/n/aw;

    .line 1258
    new-instance v0, Lcom/google/r/b/a/ake;

    invoke-direct {v0}, Lcom/google/r/b/a/ake;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ake;->e:Lcom/google/r/b/a/ake;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 693
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 863
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ake;->c:Lcom/google/n/ao;

    .line 879
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ake;->d:Lcom/google/n/ao;

    .line 894
    iput-byte v2, p0, Lcom/google/r/b/a/ake;->f:B

    .line 925
    iput v2, p0, Lcom/google/r/b/a/ake;->g:I

    .line 694
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ake;->b:I

    .line 695
    iget-object v0, p0, Lcom/google/r/b/a/ake;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 696
    iget-object v0, p0, Lcom/google/r/b/a/ake;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 697
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 703
    invoke-direct {p0}, Lcom/google/r/b/a/ake;-><init>()V

    .line 704
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 709
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 710
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 711
    sparse-switch v3, :sswitch_data_0

    .line 716
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 718
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 714
    goto :goto_0

    .line 723
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 724
    invoke-static {v3}, Lcom/google/r/b/a/akh;->a(I)Lcom/google/r/b/a/akh;

    move-result-object v4

    .line 725
    if-nez v4, :cond_1

    .line 726
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 745
    :catch_0
    move-exception v0

    .line 746
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 751
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ake;->au:Lcom/google/n/bn;

    throw v0

    .line 728
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/ake;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ake;->a:I

    .line 729
    iput v3, p0, Lcom/google/r/b/a/ake;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 747
    :catch_1
    move-exception v0

    .line 748
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 749
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 734
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/ake;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 735
    iget v3, p0, Lcom/google/r/b/a/ake;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ake;->a:I

    goto :goto_0

    .line 739
    :sswitch_3
    iget-object v3, p0, Lcom/google/r/b/a/ake;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 740
    iget v3, p0, Lcom/google/r/b/a/ake;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/ake;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 751
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ake;->au:Lcom/google/n/bn;

    .line 752
    return-void

    .line 711
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 691
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 863
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ake;->c:Lcom/google/n/ao;

    .line 879
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ake;->d:Lcom/google/n/ao;

    .line 894
    iput-byte v1, p0, Lcom/google/r/b/a/ake;->f:B

    .line 925
    iput v1, p0, Lcom/google/r/b/a/ake;->g:I

    .line 692
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ake;
    .locals 1

    .prologue
    .line 1261
    sget-object v0, Lcom/google/r/b/a/ake;->e:Lcom/google/r/b/a/ake;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/akg;
    .locals 1

    .prologue
    .line 1012
    new-instance v0, Lcom/google/r/b/a/akg;

    invoke-direct {v0}, Lcom/google/r/b/a/akg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ake;",
            ">;"
        }
    .end annotation

    .prologue
    .line 766
    sget-object v0, Lcom/google/r/b/a/ake;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 912
    invoke-virtual {p0}, Lcom/google/r/b/a/ake;->c()I

    .line 913
    iget v0, p0, Lcom/google/r/b/a/ake;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 914
    iget v0, p0, Lcom/google/r/b/a/ake;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 916
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ake;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 917
    iget-object v0, p0, Lcom/google/r/b/a/ake;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 919
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ake;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 920
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/ake;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 922
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ake;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 923
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 896
    iget-byte v0, p0, Lcom/google/r/b/a/ake;->f:B

    .line 897
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 907
    :goto_0
    return v0

    .line 898
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 900
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ake;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 901
    iget-object v0, p0, Lcom/google/r/b/a/ake;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 902
    iput-byte v2, p0, Lcom/google/r/b/a/ake;->f:B

    move v0, v2

    .line 903
    goto :goto_0

    :cond_2
    move v0, v2

    .line 900
    goto :goto_1

    .line 906
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/ake;->f:B

    move v0, v1

    .line 907
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 927
    iget v0, p0, Lcom/google/r/b/a/ake;->g:I

    .line 928
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 945
    :goto_0
    return v0

    .line 931
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ake;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 932
    iget v0, p0, Lcom/google/r/b/a/ake;->b:I

    .line 933
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 935
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/ake;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 936
    iget-object v2, p0, Lcom/google/r/b/a/ake;->c:Lcom/google/n/ao;

    .line 937
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 939
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/ake;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 940
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/ake;->d:Lcom/google/n/ao;

    .line 941
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 943
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/ake;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 944
    iput v0, p0, Lcom/google/r/b/a/ake;->g:I

    goto :goto_0

    .line 933
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 685
    invoke-static {}, Lcom/google/r/b/a/ake;->newBuilder()Lcom/google/r/b/a/akg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/akg;->a(Lcom/google/r/b/a/ake;)Lcom/google/r/b/a/akg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 685
    invoke-static {}, Lcom/google/r/b/a/ake;->newBuilder()Lcom/google/r/b/a/akg;

    move-result-object v0

    return-object v0
.end method
