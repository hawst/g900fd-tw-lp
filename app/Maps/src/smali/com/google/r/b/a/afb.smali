.class public final Lcom/google/r/b/a/afb;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afb;",
            ">;"
        }
    .end annotation
.end field

.field static final s:Lcom/google/r/b/a/afb;

.field private static final serialVersionUID:J

.field private static volatile v:Lcom/google/n/aw;


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/jm;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/hu;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field h:Lcom/google/r/b/a/afz;

.field i:Lcom/google/r/b/a/afe;

.field public j:Z

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aex;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/google/n/f;

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/fw;",
            ">;"
        }
    .end annotation
.end field

.field n:Z

.field o:Ljava/lang/Object;

.field p:Ljava/lang/Object;

.field public q:Lcom/google/maps/c/a/a;

.field public r:Lcom/google/maps/g/a/is;

.field private t:B

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10396
    new-instance v0, Lcom/google/r/b/a/afc;

    invoke-direct {v0}, Lcom/google/r/b/a/afc;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afb;->PARSER:Lcom/google/n/ax;

    .line 11174
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/afb;->v:Lcom/google/n/aw;

    .line 12611
    new-instance v0, Lcom/google/r/b/a/afb;

    invoke-direct {v0}, Lcom/google/r/b/a/afb;-><init>()V

    sput-object v0, Lcom/google/r/b/a/afb;->s:Lcom/google/r/b/a/afb;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 10197
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 11020
    iput-byte v0, p0, Lcom/google/r/b/a/afb;->t:B

    .line 11093
    iput v0, p0, Lcom/google/r/b/a/afb;->u:I

    .line 10198
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    .line 10199
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    .line 10200
    iput v1, p0, Lcom/google/r/b/a/afb;->d:I

    .line 10201
    iput v1, p0, Lcom/google/r/b/a/afb;->e:I

    .line 10202
    iput v1, p0, Lcom/google/r/b/a/afb;->f:I

    .line 10203
    iput v1, p0, Lcom/google/r/b/a/afb;->g:I

    .line 10204
    iput-boolean v1, p0, Lcom/google/r/b/a/afb;->j:Z

    .line 10205
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    .line 10206
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/afb;->l:Lcom/google/n/f;

    .line 10207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    .line 10208
    iput-boolean v1, p0, Lcom/google/r/b/a/afb;->n:Z

    .line 10209
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afb;->o:Ljava/lang/Object;

    .line 10210
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afb;->p:Ljava/lang/Object;

    .line 10211
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/16 v9, 0x200

    const/4 v8, 0x2

    const/4 v3, 0x0

    const/16 v7, 0x800

    const/4 v5, 0x1

    .line 10217
    invoke-direct {p0}, Lcom/google/r/b/a/afb;-><init>()V

    .line 10218
    const/4 v1, 0x0

    .line 10220
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v6

    .line 10222
    const/4 v0, 0x0

    move v4, v0

    .line 10223
    :cond_0
    :goto_0
    if-nez v4, :cond_e

    .line 10224
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 10225
    sparse-switch v0, :sswitch_data_0

    .line 10230
    invoke-virtual {v6, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v5

    .line 10232
    goto :goto_0

    :sswitch_0
    move v4, v5

    .line 10228
    goto :goto_0

    .line 10237
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v5, :cond_1

    .line 10238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    .line 10239
    or-int/lit8 v1, v1, 0x1

    .line 10241
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    sget-object v2, Lcom/google/maps/g/a/jm;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 10375
    :catch_0
    move-exception v0

    .line 10376
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 10381
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x1

    if-ne v2, v5, :cond_2

    .line 10382
    iget-object v2, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    .line 10384
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v8, :cond_3

    .line 10385
    iget-object v2, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    .line 10387
    :cond_3
    and-int/lit16 v2, v1, 0x200

    if-ne v2, v9, :cond_4

    .line 10388
    iget-object v2, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    .line 10390
    :cond_4
    and-int/lit16 v1, v1, 0x800

    if-ne v1, v7, :cond_5

    .line 10391
    iget-object v1, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    .line 10393
    :cond_5
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afb;->au:Lcom/google/n/bn;

    throw v0

    .line 10245
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v8, :cond_6

    .line 10246
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    .line 10247
    or-int/lit8 v1, v1, 0x2

    .line 10249
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    sget-object v2, Lcom/google/maps/g/a/hu;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 10377
    :catch_1
    move-exception v0

    .line 10378
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 10379
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 10253
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10254
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/afb;->d:I

    goto/16 :goto_0

    .line 10258
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 10259
    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v2

    .line 10260
    if-nez v2, :cond_7

    .line 10261
    const/4 v2, 0x4

    invoke-virtual {v6, v2, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 10263
    :cond_7
    iget v2, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10264
    iput v0, p0, Lcom/google/r/b/a/afb;->g:I

    goto/16 :goto_0

    .line 10270
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_16

    .line 10271
    iget-object v0, p0, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    invoke-static {v0}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v0

    move-object v2, v0

    .line 10273
    :goto_1
    sget-object v0, Lcom/google/r/b/a/afz;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    iput-object v0, p0, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    .line 10274
    if-eqz v2, :cond_8

    .line 10275
    iget-object v0, p0, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    .line 10276
    invoke-virtual {v2}, Lcom/google/r/b/a/agb;->c()Lcom/google/r/b/a/afz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    .line 10278
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    goto/16 :goto_0

    .line 10283
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_15

    .line 10284
    iget-object v0, p0, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    invoke-static {v0}, Lcom/google/r/b/a/afe;->a(Lcom/google/r/b/a/afe;)Lcom/google/r/b/a/afg;

    move-result-object v0

    move-object v2, v0

    .line 10286
    :goto_2
    sget-object v0, Lcom/google/r/b/a/afe;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afe;

    iput-object v0, p0, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    .line 10287
    if-eqz v2, :cond_9

    .line 10288
    iget-object v0, p0, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/afg;->a(Lcom/google/r/b/a/afe;)Lcom/google/r/b/a/afg;

    .line 10289
    new-instance v0, Lcom/google/r/b/a/afe;

    invoke-direct {v0, v2}, Lcom/google/r/b/a/afe;-><init>(Lcom/google/n/v;)V

    iput-object v0, p0, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    .line 10291
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    goto/16 :goto_0

    .line 10295
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10296
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/afb;->j:Z

    goto/16 :goto_0

    .line 10300
    :sswitch_8
    and-int/lit16 v0, v1, 0x200

    if-eq v0, v9, :cond_a

    .line 10301
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    .line 10302
    or-int/lit16 v1, v1, 0x200

    .line 10304
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/aex;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 10308
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10309
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->l:Lcom/google/n/f;

    goto/16 :goto_0

    .line 10313
    :sswitch_a
    and-int/lit16 v0, v1, 0x800

    if-eq v0, v7, :cond_b

    .line 10314
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    .line 10315
    or-int/lit16 v1, v1, 0x800

    .line 10317
    :cond_b
    iget-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    sget-object v2, Lcom/google/maps/g/a/fw;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 10321
    :sswitch_b
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10322
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/afb;->n:Z

    goto/16 :goto_0

    .line 10326
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10327
    iget v2, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10328
    iput-object v0, p0, Lcom/google/r/b/a/afb;->o:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10332
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 10333
    iget v2, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10334
    iput-object v0, p0, Lcom/google/r/b/a/afb;->p:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10339
    :sswitch_e
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x800

    if-ne v0, v7, :cond_14

    .line 10340
    iget-object v0, p0, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    invoke-static {}, Lcom/google/maps/c/a/a;->newBuilder()Lcom/google/maps/c/a/c;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/c/a/c;->a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;

    move-result-object v0

    move-object v2, v0

    .line 10342
    :goto_3
    sget-object v0, Lcom/google/maps/c/a/a;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/c/a/a;

    iput-object v0, p0, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    .line 10343
    if-eqz v2, :cond_c

    .line 10344
    iget-object v0, p0, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    invoke-virtual {v2, v0}, Lcom/google/maps/c/a/c;->a(Lcom/google/maps/c/a/a;)Lcom/google/maps/c/a/c;

    .line 10345
    invoke-virtual {v2}, Lcom/google/maps/c/a/c;->c()Lcom/google/maps/c/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    .line 10347
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    goto/16 :goto_0

    .line 10351
    :sswitch_f
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10352
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/afb;->e:I

    goto/16 :goto_0

    .line 10356
    :sswitch_10
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I

    .line 10357
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/afb;->f:I

    goto/16 :goto_0

    .line 10362
    :sswitch_11
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_13

    .line 10363
    iget-object v0, p0, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    invoke-static {}, Lcom/google/maps/g/a/is;->newBuilder()Lcom/google/maps/g/a/iu;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/iu;->a(Lcom/google/maps/g/a/is;)Lcom/google/maps/g/a/iu;

    move-result-object v0

    move-object v2, v0

    .line 10365
    :goto_4
    sget-object v0, Lcom/google/maps/g/a/is;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/is;

    iput-object v0, p0, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    .line 10366
    if-eqz v2, :cond_d

    .line 10367
    iget-object v0, p0, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    invoke-virtual {v2, v0}, Lcom/google/maps/g/a/iu;->a(Lcom/google/maps/g/a/is;)Lcom/google/maps/g/a/iu;

    .line 10368
    invoke-virtual {v2}, Lcom/google/maps/g/a/iu;->c()Lcom/google/maps/g/a/is;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    .line 10370
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/afb;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 10381
    :cond_e
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v5, :cond_f

    .line 10382
    iget-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    .line 10384
    :cond_f
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v8, :cond_10

    .line 10385
    iget-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    .line 10387
    :cond_10
    and-int/lit16 v0, v1, 0x200

    if-ne v0, v9, :cond_11

    .line 10388
    iget-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    .line 10390
    :cond_11
    and-int/lit16 v0, v1, 0x800

    if-ne v0, v7, :cond_12

    .line 10391
    iget-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    .line 10393
    :cond_12
    invoke-virtual {v6}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->au:Lcom/google/n/bn;

    .line 10394
    return-void

    :cond_13
    move-object v2, v3

    goto :goto_4

    :cond_14
    move-object v2, v3

    goto/16 :goto_3

    :cond_15
    move-object v2, v3

    goto/16 :goto_2

    :cond_16
    move-object v2, v3

    goto/16 :goto_1

    .line 10225
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 10195
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 11020
    iput-byte v0, p0, Lcom/google/r/b/a/afb;->t:B

    .line 11093
    iput v0, p0, Lcom/google/r/b/a/afb;->u:I

    .line 10196
    return-void
.end method

.method public static a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;
    .locals 1

    .prologue
    .line 11239
    invoke-static {}, Lcom/google/r/b/a/afb;->newBuilder()Lcom/google/r/b/a/afd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/afd;->a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/afb;
    .locals 1

    .prologue
    .line 12614
    sget-object v0, Lcom/google/r/b/a/afb;->s:Lcom/google/r/b/a/afb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/afd;
    .locals 1

    .prologue
    .line 11236
    new-instance v0, Lcom/google/r/b/a/afd;

    invoke-direct {v0}, Lcom/google/r/b/a/afd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/afb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 10408
    sget-object v0, Lcom/google/r/b/a/afb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11038
    invoke-virtual {p0}, Lcom/google/r/b/a/afb;->c()I

    move v1, v2

    .line 11039
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 11040
    iget-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 11039
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 11042
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 11043
    iget-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 11042
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 11045
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 11046
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/afb;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 11048
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 11049
    iget v0, p0, Lcom/google/r/b/a/afb;->g:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 11051
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 11052
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 11054
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 11055
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/r/b/a/afe;->d()Lcom/google/r/b/a/afe;

    move-result-object v0

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 11057
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 11058
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/r/b/a/afb;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    :cond_6
    move v1, v2

    .line 11060
    :goto_4
    iget-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 11061
    iget-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 11060
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 11052
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    goto :goto_2

    .line 11055
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    goto :goto_3

    .line 11063
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 11064
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/r/b/a/afb;->l:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 11066
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 11067
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 11066
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 11069
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_c

    .line 11070
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/r/b/a/afb;->n:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 11072
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_d

    .line 11073
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/r/b/a/afb;->o:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->o:Ljava/lang/Object;

    :goto_6
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 11075
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 11076
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/r/b/a/afb;->p:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->p:Ljava/lang/Object;

    :goto_7
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 11078
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_f

    .line 11079
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    if-nez v0, :cond_15

    invoke-static {}, Lcom/google/maps/c/a/a;->d()Lcom/google/maps/c/a/a;

    move-result-object v0

    :goto_8
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 11081
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_10

    .line 11082
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/r/b/a/afb;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 11084
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_11

    .line 11085
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/r/b/a/afb;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 11087
    :cond_11
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_12

    .line 11088
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    if-nez v0, :cond_16

    invoke-static {}, Lcom/google/maps/g/a/is;->d()Lcom/google/maps/g/a/is;

    move-result-object v0

    :goto_9
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 11090
    :cond_12
    iget-object v0, p0, Lcom/google/r/b/a/afb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 11091
    return-void

    .line 11073
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 11076
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 11079
    :cond_15
    iget-object v0, p0, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    goto :goto_8

    .line 11088
    :cond_16
    iget-object v0, p0, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    goto :goto_9
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11022
    iget-byte v0, p0, Lcom/google/r/b/a/afb;->t:B

    .line 11023
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 11033
    :cond_0
    :goto_0
    return v2

    .line 11024
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 11026
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 11027
    iget-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-virtual {v0}, Lcom/google/maps/g/a/hu;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 11028
    iput-byte v2, p0, Lcom/google/r/b/a/afb;->t:B

    goto :goto_0

    .line 11026
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 11032
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/afb;->t:B

    move v2, v3

    .line 11033
    goto :goto_0
.end method

.method public final c()I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 11095
    iget v0, p0, Lcom/google/r/b/a/afb;->u:I

    .line 11096
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 11169
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 11099
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 11100
    iget-object v0, p0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    .line 11101
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 11099
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 11103
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 11104
    iget-object v0, p0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    .line 11105
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 11103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 11107
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_3

    .line 11108
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/afb;->d:I

    .line 11109
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_8

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 11111
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 11112
    iget v0, p0, Lcom/google/r/b/a/afb;->g:I

    .line 11113
    invoke-static {v9, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 11115
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 11116
    const/4 v1, 0x5

    .line 11117
    iget-object v0, p0, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v0

    :goto_5
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 11119
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 11120
    const/4 v1, 0x6

    .line 11121
    iget-object v0, p0, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    if-nez v0, :cond_b

    invoke-static {}, Lcom/google/r/b/a/afe;->d()Lcom/google/r/b/a/afe;

    move-result-object v0

    :goto_6
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 11123
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 11124
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/r/b/a/afb;->j:Z

    .line 11125
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_7
    move v1, v2

    .line 11127
    :goto_7
    iget-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 11128
    const/16 v5, 0x8

    iget-object v0, p0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    .line 11129
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 11127
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_8
    move v0, v4

    .line 11109
    goto/16 :goto_3

    :cond_9
    move v0, v4

    .line 11113
    goto/16 :goto_4

    .line 11117
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/afb;->h:Lcom/google/r/b/a/afz;

    goto :goto_5

    .line 11121
    :cond_b
    iget-object v0, p0, Lcom/google/r/b/a/afb;->i:Lcom/google/r/b/a/afe;

    goto :goto_6

    .line 11131
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_d

    .line 11132
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/r/b/a/afb;->l:Lcom/google/n/f;

    .line 11133
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v1, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_d
    move v1, v2

    .line 11135
    :goto_8
    iget-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 11136
    iget-object v0, p0, Lcom/google/r/b/a/afb;->m:Ljava/util/List;

    .line 11137
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 11135
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 11139
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_f

    .line 11140
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/r/b/a/afb;->n:Z

    .line 11141
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 11143
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_10

    .line 11144
    const/16 v1, 0xc

    .line 11145
    iget-object v0, p0, Lcom/google/r/b/a/afb;->o:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->o:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 11147
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_11

    .line 11148
    const/16 v1, 0xd

    .line 11149
    iget-object v0, p0, Lcom/google/r/b/a/afb;->p:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afb;->p:Ljava/lang/Object;

    :goto_a
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 11151
    :cond_11
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_12

    .line 11152
    const/16 v1, 0xe

    .line 11153
    iget-object v0, p0, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    if-nez v0, :cond_19

    invoke-static {}, Lcom/google/maps/c/a/a;->d()Lcom/google/maps/c/a/a;

    move-result-object v0

    :goto_b
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 11155
    :cond_12
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_13

    .line 11156
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/r/b/a/afb;->e:I

    .line 11157
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_1a

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_c
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 11159
    :cond_13
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v9, :cond_15

    .line 11160
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/r/b/a/afb;->f:I

    .line 11161
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_14

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_14
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 11163
    :cond_15
    iget v0, p0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_16

    .line 11164
    const/16 v1, 0x11

    .line 11165
    iget-object v0, p0, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    if-nez v0, :cond_1b

    invoke-static {}, Lcom/google/maps/g/a/is;->d()Lcom/google/maps/g/a/is;

    move-result-object v0

    :goto_d
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 11167
    :cond_16
    iget-object v0, p0, Lcom/google/r/b/a/afb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 11168
    iput v0, p0, Lcom/google/r/b/a/afb;->u:I

    goto/16 :goto_0

    .line 11145
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_9

    .line 11149
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    .line 11153
    :cond_19
    iget-object v0, p0, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    goto/16 :goto_b

    :cond_1a
    move v0, v4

    .line 11157
    goto :goto_c

    .line 11165
    :cond_1b
    iget-object v0, p0, Lcom/google/r/b/a/afb;->r:Lcom/google/maps/g/a/is;

    goto :goto_d
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 10189
    invoke-static {}, Lcom/google/r/b/a/afb;->newBuilder()Lcom/google/r/b/a/afd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/afd;->a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 10189
    invoke-static {}, Lcom/google/r/b/a/afb;->newBuilder()Lcom/google/r/b/a/afd;

    move-result-object v0

    return-object v0
.end method
