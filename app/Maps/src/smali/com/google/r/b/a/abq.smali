.class public final enum Lcom/google/r/b/a/abq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/abq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/abq;

.field public static final enum b:Lcom/google/r/b/a/abq;

.field public static final enum c:Lcom/google/r/b/a/abq;

.field public static final enum d:Lcom/google/r/b/a/abq;

.field private static final synthetic f:[Lcom/google/r/b/a/abq;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 703
    new-instance v0, Lcom/google/r/b/a/abq;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/abq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/abq;->a:Lcom/google/r/b/a/abq;

    .line 707
    new-instance v0, Lcom/google/r/b/a/abq;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/abq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/abq;->b:Lcom/google/r/b/a/abq;

    .line 711
    new-instance v0, Lcom/google/r/b/a/abq;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/abq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/abq;->c:Lcom/google/r/b/a/abq;

    .line 715
    new-instance v0, Lcom/google/r/b/a/abq;

    const-string v1, "REQUEST_ERROR"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/abq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/abq;->d:Lcom/google/r/b/a/abq;

    .line 698
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/abq;

    sget-object v1, Lcom/google/r/b/a/abq;->a:Lcom/google/r/b/a/abq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/abq;->b:Lcom/google/r/b/a/abq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/abq;->c:Lcom/google/r/b/a/abq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/abq;->d:Lcom/google/r/b/a/abq;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/abq;->f:[Lcom/google/r/b/a/abq;

    .line 755
    new-instance v0, Lcom/google/r/b/a/abr;

    invoke-direct {v0}, Lcom/google/r/b/a/abr;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 764
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 765
    iput p3, p0, Lcom/google/r/b/a/abq;->e:I

    .line 766
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/abq;
    .locals 1

    .prologue
    .line 741
    packed-switch p0, :pswitch_data_0

    .line 746
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 742
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/abq;->a:Lcom/google/r/b/a/abq;

    goto :goto_0

    .line 743
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/abq;->b:Lcom/google/r/b/a/abq;

    goto :goto_0

    .line 744
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/abq;->c:Lcom/google/r/b/a/abq;

    goto :goto_0

    .line 745
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/abq;->d:Lcom/google/r/b/a/abq;

    goto :goto_0

    .line 741
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/abq;
    .locals 1

    .prologue
    .line 698
    const-class v0, Lcom/google/r/b/a/abq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abq;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/abq;
    .locals 1

    .prologue
    .line 698
    sget-object v0, Lcom/google/r/b/a/abq;->f:[Lcom/google/r/b/a/abq;

    invoke-virtual {v0}, [Lcom/google/r/b/a/abq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/abq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 737
    iget v0, p0, Lcom/google/r/b/a/abq;->e:I

    return v0
.end method
