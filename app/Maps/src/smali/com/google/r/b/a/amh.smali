.class public final Lcom/google/r/b/a/amh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/amh;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/amh;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Z

.field public c:I

.field public d:I

.field public e:I

.field public f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/r/b/a/ami;

    invoke-direct {v0}, Lcom/google/r/b/a/ami;-><init>()V

    sput-object v0, Lcom/google/r/b/a/amh;->PARSER:Lcom/google/n/ax;

    .line 341
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/amh;->j:Lcom/google/n/aw;

    .line 711
    new-instance v0, Lcom/google/r/b/a/amh;

    invoke-direct {v0}, Lcom/google/r/b/a/amh;-><init>()V

    sput-object v0, Lcom/google/r/b/a/amh;->g:Lcom/google/r/b/a/amh;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 277
    iput-byte v0, p0, Lcom/google/r/b/a/amh;->h:B

    .line 308
    iput v0, p0, Lcom/google/r/b/a/amh;->i:I

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/amh;->b:Z

    .line 19
    iput v1, p0, Lcom/google/r/b/a/amh;->c:I

    .line 20
    iput v1, p0, Lcom/google/r/b/a/amh;->d:I

    .line 21
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/r/b/a/amh;->e:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 29
    invoke-direct {p0}, Lcom/google/r/b/a/amh;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 35
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 37
    sparse-switch v0, :sswitch_data_0

    .line 42
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/amh;->a:I

    .line 50
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/amh;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/amh;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 50
    goto :goto_1

    .line 54
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 55
    invoke-static {v0}, Lcom/google/r/b/a/amk;->a(I)Lcom/google/r/b/a/amk;

    move-result-object v5

    .line 56
    if-nez v5, :cond_2

    .line 57
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 84
    :catch_1
    move-exception v0

    .line 85
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 86
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :cond_2
    :try_start_4
    iget v5, p0, Lcom/google/r/b/a/amh;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/amh;->a:I

    .line 60
    iput v0, p0, Lcom/google/r/b/a/amh;->c:I

    goto :goto_0

    .line 65
    :sswitch_3
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/amh;->a:I

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/amh;->d:I

    goto :goto_0

    .line 70
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/amh;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/amh;->e:I

    goto :goto_0

    .line 75
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 76
    iget v5, p0, Lcom/google/r/b/a/amh;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/r/b/a/amh;->a:I

    .line 77
    iput-object v0, p0, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 88
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amh;->au:Lcom/google/n/bn;

    .line 89
    return-void

    .line 37
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 277
    iput-byte v0, p0, Lcom/google/r/b/a/amh;->h:B

    .line 308
    iput v0, p0, Lcom/google/r/b/a/amh;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/amh;
    .locals 1

    .prologue
    .line 714
    sget-object v0, Lcom/google/r/b/a/amh;->g:Lcom/google/r/b/a/amh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/amj;
    .locals 1

    .prologue
    .line 403
    new-instance v0, Lcom/google/r/b/a/amj;

    invoke-direct {v0}, Lcom/google/r/b/a/amj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/amh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/google/r/b/a/amh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 289
    invoke-virtual {p0}, Lcom/google/r/b/a/amh;->c()I

    .line 290
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 291
    iget-boolean v0, p0, Lcom/google/r/b/a/amh;->b:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IZ)V

    .line 293
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 294
    iget v0, p0, Lcom/google/r/b/a/amh;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 296
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 297
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/amh;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 299
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 300
    iget v0, p0, Lcom/google/r/b/a/amh;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 302
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 303
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 305
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/amh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 306
    return-void

    .line 303
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 279
    iget-byte v1, p0, Lcom/google/r/b/a/amh;->h:B

    .line 280
    if-ne v1, v0, :cond_0

    .line 284
    :goto_0
    return v0

    .line 281
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 283
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/amh;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 310
    iget v0, p0, Lcom/google/r/b/a/amh;->i:I

    .line 311
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 336
    :goto_0
    return v0

    .line 314
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 315
    iget-boolean v0, p0, Lcom/google/r/b/a/amh;->b:Z

    .line 316
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 318
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 319
    iget v2, p0, Lcom/google/r/b/a/amh;->c:I

    .line 320
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_5

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 322
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 323
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/r/b/a/amh;->d:I

    .line 324
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 326
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_8

    .line 327
    iget v2, p0, Lcom/google/r/b/a/amh;->e:I

    .line 328
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_3
    add-int v2, v4, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 330
    :goto_4
    iget v0, p0, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 331
    const/4 v3, 0x5

    .line 332
    iget-object v0, p0, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 334
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/amh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 335
    iput v0, p0, Lcom/google/r/b/a/amh;->i:I

    goto/16 :goto_0

    :cond_5
    move v2, v3

    .line 320
    goto :goto_2

    :cond_6
    move v2, v3

    .line 324
    goto :goto_3

    .line 332
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_8
    move v2, v0

    goto :goto_4

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/amh;->newBuilder()Lcom/google/r/b/a/amj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/amj;->a(Lcom/google/r/b/a/amh;)Lcom/google/r/b/a/amj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/amh;->newBuilder()Lcom/google/r/b/a/amj;

    move-result-object v0

    return-object v0
.end method
