.class public final Lcom/google/r/b/a/qz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/rc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qz;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/r/b/a/qz;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/google/r/b/a/ra;

    invoke-direct {v0}, Lcom/google/r/b/a/ra;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qz;->PARSER:Lcom/google/n/ax;

    .line 463
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/qz;->r:Lcom/google/n/aw;

    .line 1111
    new-instance v0, Lcom/google/r/b/a/qz;

    invoke-direct {v0}, Lcom/google/r/b/a/qz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/qz;->o:Lcom/google/r/b/a/qz;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x3e8

    const/16 v1, 0x32

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 343
    iput-byte v0, p0, Lcom/google/r/b/a/qz;->p:B

    .line 398
    iput v0, p0, Lcom/google/r/b/a/qz;->q:I

    .line 18
    iput v2, p0, Lcom/google/r/b/a/qz;->b:I

    .line 19
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/google/r/b/a/qz;->c:I

    .line 20
    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/r/b/a/qz;->d:I

    .line 21
    const/16 v0, 0x23

    iput v0, p0, Lcom/google/r/b/a/qz;->e:I

    .line 22
    const/16 v0, 0xf

    iput v0, p0, Lcom/google/r/b/a/qz;->f:I

    .line 23
    iput v1, p0, Lcom/google/r/b/a/qz;->g:I

    .line 24
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/r/b/a/qz;->h:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/qz;->i:I

    .line 26
    iput v2, p0, Lcom/google/r/b/a/qz;->j:I

    .line 27
    const/16 v0, 0x12c

    iput v0, p0, Lcom/google/r/b/a/qz;->k:I

    .line 28
    iput v1, p0, Lcom/google/r/b/a/qz;->l:I

    .line 29
    const/16 v0, 0x2a

    iput v0, p0, Lcom/google/r/b/a/qz;->m:I

    .line 30
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/google/r/b/a/qz;->n:I

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 37
    invoke-direct {p0}, Lcom/google/r/b/a/qz;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 42
    const/4 v0, 0x0

    .line 43
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 44
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 45
    sparse-switch v3, :sswitch_data_0

    .line 50
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 52
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 48
    goto :goto_0

    .line 57
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/qz;->au:Lcom/google/n/bn;

    throw v0

    .line 62
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 125
    :catch_1
    move-exception v0

    .line 126
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 127
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->d:I

    goto :goto_0

    .line 72
    :sswitch_4
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->e:I

    goto :goto_0

    .line 77
    :sswitch_5
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 78
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->f:I

    goto :goto_0

    .line 82
    :sswitch_6
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->g:I

    goto :goto_0

    .line 87
    :sswitch_7
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->h:I

    goto/16 :goto_0

    .line 92
    :sswitch_8
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 93
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->i:I

    goto/16 :goto_0

    .line 97
    :sswitch_9
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 98
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->j:I

    goto/16 :goto_0

    .line 102
    :sswitch_a
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 103
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->k:I

    goto/16 :goto_0

    .line 107
    :sswitch_b
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 108
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->l:I

    goto/16 :goto_0

    .line 112
    :sswitch_c
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 113
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->m:I

    goto/16 :goto_0

    .line 117
    :sswitch_d
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/r/b/a/qz;->a:I

    .line 118
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/qz;->n:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 129
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/qz;->au:Lcom/google/n/bn;

    .line 130
    return-void

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 343
    iput-byte v0, p0, Lcom/google/r/b/a/qz;->p:B

    .line 398
    iput v0, p0, Lcom/google/r/b/a/qz;->q:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/qz;
    .locals 1

    .prologue
    .line 1114
    sget-object v0, Lcom/google/r/b/a/qz;->o:Lcom/google/r/b/a/qz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/rb;
    .locals 1

    .prologue
    .line 525
    new-instance v0, Lcom/google/r/b/a/rb;

    invoke-direct {v0}, Lcom/google/r/b/a/rb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/qz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/google/r/b/a/qz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 355
    invoke-virtual {p0}, Lcom/google/r/b/a/qz;->c()I

    .line 356
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 357
    iget v0, p0, Lcom/google/r/b/a/qz;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 359
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 360
    iget v0, p0, Lcom/google/r/b/a/qz;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 362
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 363
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/qz;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 365
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 366
    iget v0, p0, Lcom/google/r/b/a/qz;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 368
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 369
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/qz;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 371
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 372
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/qz;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 374
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 375
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/qz;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 377
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 378
    iget v0, p0, Lcom/google/r/b/a/qz;->i:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 380
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 381
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/r/b/a/qz;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 383
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 384
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/r/b/a/qz;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 386
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 387
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/r/b/a/qz;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 389
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 390
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/r/b/a/qz;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 392
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 393
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/r/b/a/qz;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 395
    :cond_c
    iget-object v0, p0, Lcom/google/r/b/a/qz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 396
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 345
    iget-byte v1, p0, Lcom/google/r/b/a/qz;->p:B

    .line 346
    if-ne v1, v0, :cond_0

    .line 350
    :goto_0
    return v0

    .line 347
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 349
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/qz;->p:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 400
    iget v0, p0, Lcom/google/r/b/a/qz;->q:I

    .line 401
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 458
    :goto_0
    return v0

    .line 404
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1a

    .line 405
    iget v0, p0, Lcom/google/r/b/a/qz;->b:I

    .line 406
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 408
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 409
    iget v3, p0, Lcom/google/r/b/a/qz;->c:I

    .line 410
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_f

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 412
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 413
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/r/b/a/qz;->d:I

    .line 414
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 416
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 417
    iget v3, p0, Lcom/google/r/b/a/qz;->e:I

    .line 418
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_11

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 420
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 421
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/r/b/a/qz;->f:I

    .line 422
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_12

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 424
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 425
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/r/b/a/qz;->g:I

    .line 426
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_13

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 428
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 429
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/r/b/a/qz;->h:I

    .line 430
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_14

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 432
    :cond_6
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 433
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/r/b/a/qz;->i:I

    .line 434
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_15

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_9
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 436
    :cond_7
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 437
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/r/b/a/qz;->j:I

    .line 438
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_16

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_a
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 440
    :cond_8
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 441
    iget v3, p0, Lcom/google/r/b/a/qz;->k:I

    .line 442
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_17

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_b
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 444
    :cond_9
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 445
    const/16 v3, 0xb

    iget v4, p0, Lcom/google/r/b/a/qz;->l:I

    .line 446
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_18

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_c
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 448
    :cond_a
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 449
    const/16 v3, 0xc

    iget v4, p0, Lcom/google/r/b/a/qz;->m:I

    .line 450
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_19

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_d
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 452
    :cond_b
    iget v3, p0, Lcom/google/r/b/a/qz;->a:I

    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_d

    .line 453
    const/16 v3, 0xd

    iget v4, p0, Lcom/google/r/b/a/qz;->n:I

    .line 454
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_c
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 456
    :cond_d
    iget-object v1, p0, Lcom/google/r/b/a/qz;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    iput v0, p0, Lcom/google/r/b/a/qz;->q:I

    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 406
    goto/16 :goto_1

    :cond_f
    move v3, v1

    .line 410
    goto/16 :goto_3

    :cond_10
    move v3, v1

    .line 414
    goto/16 :goto_4

    :cond_11
    move v3, v1

    .line 418
    goto/16 :goto_5

    :cond_12
    move v3, v1

    .line 422
    goto/16 :goto_6

    :cond_13
    move v3, v1

    .line 426
    goto/16 :goto_7

    :cond_14
    move v3, v1

    .line 430
    goto/16 :goto_8

    :cond_15
    move v3, v1

    .line 434
    goto/16 :goto_9

    :cond_16
    move v3, v1

    .line 438
    goto/16 :goto_a

    :cond_17
    move v3, v1

    .line 442
    goto :goto_b

    :cond_18
    move v3, v1

    .line 446
    goto :goto_c

    :cond_19
    move v3, v1

    .line 450
    goto :goto_d

    :cond_1a
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/qz;->newBuilder()Lcom/google/r/b/a/rb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/rb;->a(Lcom/google/r/b/a/qz;)Lcom/google/r/b/a/rb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/qz;->newBuilder()Lcom/google/r/b/a/rb;

    move-result-object v0

    return-object v0
.end method
