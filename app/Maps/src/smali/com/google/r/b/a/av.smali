.class public final enum Lcom/google/r/b/a/av;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/av;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/av;

.field public static final enum b:Lcom/google/r/b/a/av;

.field public static final enum c:Lcom/google/r/b/a/av;

.field public static final enum d:Lcom/google/r/b/a/av;

.field public static final enum e:Lcom/google/r/b/a/av;

.field public static final enum f:Lcom/google/r/b/a/av;

.field public static final enum g:Lcom/google/r/b/a/av;

.field public static final enum h:Lcom/google/r/b/a/av;

.field public static final enum i:Lcom/google/r/b/a/av;

.field public static final enum j:Lcom/google/r/b/a/av;

.field public static final enum k:Lcom/google/r/b/a/av;

.field public static final enum l:Lcom/google/r/b/a/av;

.field public static final enum m:Lcom/google/r/b/a/av;

.field public static final enum n:Lcom/google/r/b/a/av;

.field public static final enum o:Lcom/google/r/b/a/av;

.field public static final enum p:Lcom/google/r/b/a/av;

.field public static final enum q:Lcom/google/r/b/a/av;

.field public static final enum r:Lcom/google/r/b/a/av;

.field public static final enum s:Lcom/google/r/b/a/av;

.field public static final enum t:Lcom/google/r/b/a/av;

.field public static final enum u:Lcom/google/r/b/a/av;

.field private static final synthetic w:[Lcom/google/r/b/a/av;


# instance fields
.field public final v:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "APP_STARTED_COLD"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->a:Lcom/google/r/b/a/av;

    .line 18
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "APP_STARTED_HOT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->b:Lcom/google/r/b/a/av;

    .line 22
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "RUNNING"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v6, v2}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->c:Lcom/google/r/b/a/av;

    .line 26
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "APP_BACKGROUNDED"

    invoke-direct {v0, v1, v7, v6}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->d:Lcom/google/r/b/a/av;

    .line 30
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "PROJECTED_APP_STARTED_COLD"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v8, v2}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->e:Lcom/google/r/b/a/av;

    .line 34
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "PROJECTED_APP_STARTED_HOT"

    const/4 v2, 0x5

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->f:Lcom/google/r/b/a/av;

    .line 38
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "PROJECTED_APP_BACKGROUNDED"

    const/4 v2, 0x6

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->g:Lcom/google/r/b/a/av;

    .line 42
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "APP_CLEARED_DATA"

    const/4 v2, 0x7

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->h:Lcom/google/r/b/a/av;

    .line 46
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "APP_STARTUP_BROWSER_HANDSHAKE_STARTED"

    const/16 v2, 0x8

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->i:Lcom/google/r/b/a/av;

    .line 50
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "APP_STARTUP_BROWSER_HANDSHAKE_ENDED"

    const/16 v2, 0x9

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->j:Lcom/google/r/b/a/av;

    .line 54
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "APP_STARTUP_BROWSER_HANDSHAKE_TIMEOUT"

    const/16 v2, 0xa

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->k:Lcom/google/r/b/a/av;

    .line 58
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "CHARGING_STATE_CHANGED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v7}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->l:Lcom/google/r/b/a/av;

    .line 62
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "NETWORK_TYPE_CHANGED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v8}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->m:Lcom/google/r/b/a/av;

    .line 66
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "NAVIGATION_STARTED"

    const/16 v2, 0xd

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->n:Lcom/google/r/b/a/av;

    .line 70
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "NAVIGATION_ENDED"

    const/16 v2, 0xe

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->o:Lcom/google/r/b/a/av;

    .line 74
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "MANUAL_CACHER_STARTED"

    const/16 v2, 0xf

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->p:Lcom/google/r/b/a/av;

    .line 78
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "MANUAL_CACHER_COMPLETED"

    const/16 v2, 0x10

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->q:Lcom/google/r/b/a/av;

    .line 82
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "POWER_SAVING_MODE_WILL_START"

    const/16 v2, 0x11

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->r:Lcom/google/r/b/a/av;

    .line 86
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "POWER_SAVING_MODE_ENDED"

    const/16 v2, 0x12

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->s:Lcom/google/r/b/a/av;

    .line 90
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "EXTERNAL_INVOCATION_STARTED"

    const/16 v2, 0x13

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->t:Lcom/google/r/b/a/av;

    .line 94
    new-instance v0, Lcom/google/r/b/a/av;

    const-string v1, "EXTERNAL_INVOCATION_COMPLETED"

    const/16 v2, 0x14

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/av;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/av;->u:Lcom/google/r/b/a/av;

    .line 9
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/google/r/b/a/av;

    sget-object v1, Lcom/google/r/b/a/av;->a:Lcom/google/r/b/a/av;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/av;->b:Lcom/google/r/b/a/av;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/av;->c:Lcom/google/r/b/a/av;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/r/b/a/av;->d:Lcom/google/r/b/a/av;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/av;->e:Lcom/google/r/b/a/av;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/r/b/a/av;->f:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/r/b/a/av;->g:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/r/b/a/av;->h:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/r/b/a/av;->i:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/r/b/a/av;->j:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/r/b/a/av;->k:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/r/b/a/av;->l:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/r/b/a/av;->m:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/r/b/a/av;->n:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/r/b/a/av;->o:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/r/b/a/av;->p:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/r/b/a/av;->q:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/r/b/a/av;->r:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/r/b/a/av;->s:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/r/b/a/av;->t:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/r/b/a/av;->u:Lcom/google/r/b/a/av;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/r/b/a/av;->w:[Lcom/google/r/b/a/av;

    .line 219
    new-instance v0, Lcom/google/r/b/a/aw;

    invoke-direct {v0}, Lcom/google/r/b/a/aw;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 229
    iput p3, p0, Lcom/google/r/b/a/av;->v:I

    .line 230
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/av;
    .locals 1

    .prologue
    .line 188
    packed-switch p0, :pswitch_data_0

    .line 210
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 189
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/av;->a:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 190
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/av;->b:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 191
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/av;->c:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 192
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/av;->d:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 193
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/av;->e:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 194
    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/av;->f:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 195
    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/av;->g:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 196
    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/av;->h:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 197
    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/av;->i:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 198
    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/av;->j:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 199
    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/av;->k:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 200
    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/av;->l:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 201
    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/av;->m:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 202
    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/av;->n:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 203
    :pswitch_e
    sget-object v0, Lcom/google/r/b/a/av;->o:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 204
    :pswitch_f
    sget-object v0, Lcom/google/r/b/a/av;->p:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 205
    :pswitch_10
    sget-object v0, Lcom/google/r/b/a/av;->q:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 206
    :pswitch_11
    sget-object v0, Lcom/google/r/b/a/av;->r:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 207
    :pswitch_12
    sget-object v0, Lcom/google/r/b/a/av;->s:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 208
    :pswitch_13
    sget-object v0, Lcom/google/r/b/a/av;->t:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 209
    :pswitch_14
    sget-object v0, Lcom/google/r/b/a/av;->u:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 188
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_7
        :pswitch_2
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/av;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/r/b/a/av;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/av;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/av;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/r/b/a/av;->w:[Lcom/google/r/b/a/av;

    invoke-virtual {v0}, [Lcom/google/r/b/a/av;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/av;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/google/r/b/a/av;->v:I

    return v0
.end method
