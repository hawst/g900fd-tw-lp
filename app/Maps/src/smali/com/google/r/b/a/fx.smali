.class public final Lcom/google/r/b/a/fx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ga;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/fx;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/r/b/a/fx;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:Lcom/google/n/aq;

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/r/b/a/fy;

    invoke-direct {v0}, Lcom/google/r/b/a/fy;-><init>()V

    sput-object v0, Lcom/google/r/b/a/fx;->PARSER:Lcom/google/n/ax;

    .line 152
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/fx;->e:Lcom/google/n/aw;

    .line 375
    new-instance v0, Lcom/google/r/b/a/fx;

    invoke-direct {v0}, Lcom/google/r/b/a/fx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/fx;->b:Lcom/google/r/b/a/fx;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 111
    iput-byte v0, p0, Lcom/google/r/b/a/fx;->c:B

    .line 130
    iput v0, p0, Lcom/google/r/b/a/fx;->d:I

    .line 18
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 25
    invoke-direct {p0}, Lcom/google/r/b/a/fx;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 31
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 33
    sparse-switch v4, :sswitch_data_0

    .line 38
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 40
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 46
    and-int/lit8 v5, v0, 0x1

    if-eq v5, v2, :cond_1

    .line 47
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    .line 48
    or-int/lit8 v0, v0, 0x1

    .line 50
    :cond_1
    iget-object v5, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 55
    :catch_0
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 56
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 62
    iget-object v1, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    .line 64
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/fx;->au:Lcom/google/n/bn;

    throw v0

    .line 61
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 62
    iget-object v0, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    .line 64
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/fx;->au:Lcom/google/n/bn;

    .line 65
    return-void

    .line 57
    :catch_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 58
    :try_start_2
    new-instance v4, Lcom/google/n/ak;

    .line 59
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61
    :catchall_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_1

    .line 33
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 111
    iput-byte v0, p0, Lcom/google/r/b/a/fx;->c:B

    .line 130
    iput v0, p0, Lcom/google/r/b/a/fx;->d:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/fx;
    .locals 1

    .prologue
    .line 378
    sget-object v0, Lcom/google/r/b/a/fx;->b:Lcom/google/r/b/a/fx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/fz;
    .locals 1

    .prologue
    .line 214
    new-instance v0, Lcom/google/r/b/a/fz;

    invoke-direct {v0}, Lcom/google/r/b/a/fz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/fx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/google/r/b/a/fx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/r/b/a/fx;->c()I

    .line 124
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 125
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/fx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 128
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 113
    iget-byte v1, p0, Lcom/google/r/b/a/fx;->c:B

    .line 114
    if-ne v1, v0, :cond_0

    .line 118
    :goto_0
    return v0

    .line 115
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 117
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/fx;->c:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 132
    iget v1, p0, Lcom/google/r/b/a/fx;->d:I

    .line 133
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 147
    :goto_0
    return v0

    :cond_0
    move v1, v0

    .line 138
    :goto_1
    iget-object v2, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 139
    iget-object v2, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    .line 140
    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 142
    :cond_1
    add-int/lit8 v0, v1, 0x0

    .line 143
    iget-object v1, p0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 145
    iget-object v1, p0, Lcom/google/r/b/a/fx;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    iput v0, p0, Lcom/google/r/b/a/fx;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/fx;->newBuilder()Lcom/google/r/b/a/fz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/fz;->a(Lcom/google/r/b/a/fx;)Lcom/google/r/b/a/fz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/fx;->newBuilder()Lcom/google/r/b/a/fz;

    move-result-object v0

    return-object v0
.end method
