.class public final Lcom/google/r/b/a/nu;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ns;",
        "Lcom/google/r/b/a/nu;",
        ">;",
        "Lcom/google/r/b/a/nv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 207
    sget-object v0, Lcom/google/r/b/a/ns;->c:Lcom/google/r/b/a/ns;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 208
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 199
    new-instance v2, Lcom/google/r/b/a/ns;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ns;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/nu;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/nu;->b:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/ns;->b:Z

    iput v0, v2, Lcom/google/r/b/a/ns;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 199
    check-cast p1, Lcom/google/r/b/a/ns;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/nu;->a(Lcom/google/r/b/a/ns;)Lcom/google/r/b/a/nu;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ns;)Lcom/google/r/b/a/nu;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 233
    invoke-static {}, Lcom/google/r/b/a/ns;->d()Lcom/google/r/b/a/ns;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 238
    :goto_0
    return-object p0

    .line 234
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/ns;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 235
    iget-boolean v0, p1, Lcom/google/r/b/a/ns;->b:Z

    iget v1, p0, Lcom/google/r/b/a/nu;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/nu;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/nu;->b:Z

    .line 237
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/ns;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 234
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x1

    return v0
.end method
