.class public final Lcom/google/r/b/a/dl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/dm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/dj;",
        "Lcom/google/r/b/a/dl;",
        ">;",
        "Lcom/google/r/b/a/dm;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 292
    sget-object v0, Lcom/google/r/b/a/dj;->f:Lcom/google/r/b/a/dj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 458
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/dl;->e:Lcom/google/n/ao;

    .line 293
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 284
    new-instance v2, Lcom/google/r/b/a/dj;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/dj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/dl;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-boolean v4, p0, Lcom/google/r/b/a/dl;->b:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/dj;->b:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/r/b/a/dl;->c:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/dj;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v4, p0, Lcom/google/r/b/a/dl;->d:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/dj;->d:Z

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v3, v2, Lcom/google/r/b/a/dj;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/dl;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/dl;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/dj;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 284
    check-cast p1, Lcom/google/r/b/a/dj;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/dl;->a(Lcom/google/r/b/a/dj;)Lcom/google/r/b/a/dl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/dj;)Lcom/google/r/b/a/dl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 338
    invoke-static {}, Lcom/google/r/b/a/dj;->d()Lcom/google/r/b/a/dj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 353
    :goto_0
    return-object p0

    .line 339
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 340
    iget-boolean v2, p1, Lcom/google/r/b/a/dj;->b:Z

    iget v3, p0, Lcom/google/r/b/a/dl;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/dl;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/dl;->b:Z

    .line 342
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 343
    iget-boolean v2, p1, Lcom/google/r/b/a/dj;->c:Z

    iget v3, p0, Lcom/google/r/b/a/dl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/dl;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/dl;->c:Z

    .line 345
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 346
    iget-boolean v2, p1, Lcom/google/r/b/a/dj;->d:Z

    iget v3, p0, Lcom/google/r/b/a/dl;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/dl;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/dl;->d:Z

    .line 348
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/dj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 349
    iget-object v0, p0, Lcom/google/r/b/a/dl;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/dj;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 350
    iget v0, p0, Lcom/google/r/b/a/dl;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/dl;->a:I

    .line 352
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/dj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 339
    goto :goto_1

    :cond_6
    move v2, v1

    .line 342
    goto :goto_2

    :cond_7
    move v2, v1

    .line 345
    goto :goto_3

    :cond_8
    move v0, v1

    .line 348
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x1

    return v0
.end method
