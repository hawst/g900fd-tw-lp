.class public final Lcom/google/r/b/a/ix;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/jc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/iv;",
        "Lcom/google/r/b/a/ix;",
        ">;",
        "Lcom/google/r/b/a/jc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:I

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 503
    sget-object v0, Lcom/google/r/b/a/iv;->g:Lcom/google/r/b/a/iv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 593
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ix;->b:Lcom/google/n/ao;

    .line 652
    iput v1, p0, Lcom/google/r/b/a/ix;->c:I

    .line 688
    iput v1, p0, Lcom/google/r/b/a/ix;->d:I

    .line 724
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ix;->e:Ljava/lang/Object;

    .line 800
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ix;->f:Lcom/google/n/ao;

    .line 504
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 495
    new-instance v2, Lcom/google/r/b/a/iv;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/iv;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ix;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/iv;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/ix;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/ix;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/r/b/a/ix;->c:I

    iput v4, v2, Lcom/google/r/b/a/iv;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/r/b/a/ix;->d:I

    iput v4, v2, Lcom/google/r/b/a/iv;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/ix;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/iv;->e:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v3, v2, Lcom/google/r/b/a/iv;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/ix;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/ix;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/iv;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 495
    check-cast p1, Lcom/google/r/b/a/iv;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ix;->a(Lcom/google/r/b/a/iv;)Lcom/google/r/b/a/ix;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/iv;)Lcom/google/r/b/a/ix;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 557
    invoke-static {}, Lcom/google/r/b/a/iv;->d()Lcom/google/r/b/a/iv;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 578
    :goto_0
    return-object p0

    .line 558
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/iv;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 559
    iget-object v2, p0, Lcom/google/r/b/a/ix;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/iv;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 560
    iget v2, p0, Lcom/google/r/b/a/ix;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ix;->a:I

    .line 562
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/iv;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 563
    iget v2, p1, Lcom/google/r/b/a/iv;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/iy;->a(I)Lcom/google/r/b/a/iy;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/r/b/a/iy;->a:Lcom/google/r/b/a/iy;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 558
    goto :goto_1

    :cond_4
    move v2, v1

    .line 562
    goto :goto_2

    .line 563
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/ix;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/ix;->a:I

    iget v2, v2, Lcom/google/r/b/a/iy;->d:I

    iput v2, p0, Lcom/google/r/b/a/ix;->c:I

    .line 565
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/iv;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 566
    iget v2, p1, Lcom/google/r/b/a/iv;->d:I

    invoke-static {v2}, Lcom/google/r/b/a/ja;->a(I)Lcom/google/r/b/a/ja;

    move-result-object v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/r/b/a/ja;->a:Lcom/google/r/b/a/ja;

    :cond_7
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v1

    .line 565
    goto :goto_3

    .line 566
    :cond_9
    iget v3, p0, Lcom/google/r/b/a/ix;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/ix;->a:I

    iget v2, v2, Lcom/google/r/b/a/ja;->d:I

    iput v2, p0, Lcom/google/r/b/a/ix;->d:I

    .line 568
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/iv;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 569
    iget v2, p0, Lcom/google/r/b/a/ix;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/ix;->a:I

    .line 570
    iget-object v2, p1, Lcom/google/r/b/a/iv;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ix;->e:Ljava/lang/Object;

    .line 573
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/iv;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    :goto_5
    if-eqz v0, :cond_c

    .line 574
    iget-object v0, p0, Lcom/google/r/b/a/ix;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/iv;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 575
    iget v0, p0, Lcom/google/r/b/a/ix;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/ix;->a:I

    .line 577
    :cond_c
    iget-object v0, p1, Lcom/google/r/b/a/iv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 568
    goto :goto_4

    :cond_e
    move v0, v1

    .line 573
    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 582
    iget v0, p0, Lcom/google/r/b/a/ix;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 583
    iget-object v0, p0, Lcom/google/r/b/a/ix;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ks;->d()Lcom/google/r/b/a/ks;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ks;

    invoke-virtual {v0}, Lcom/google/r/b/a/ks;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 588
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 582
    goto :goto_0

    :cond_1
    move v0, v2

    .line 588
    goto :goto_1
.end method
