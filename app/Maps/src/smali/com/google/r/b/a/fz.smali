.class public final Lcom/google/r/b/a/fz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ga;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/fx;",
        "Lcom/google/r/b/a/fz;",
        ">;",
        "Lcom/google/r/b/a/ga;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/aq;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/google/r/b/a/fx;->b:Lcom/google/r/b/a/fx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 278
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    .line 233
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 224
    new-instance v0, Lcom/google/r/b/a/fx;

    invoke-direct {v0, p0}, Lcom/google/r/b/a/fx;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/r/b/a/fz;->a:I

    iget v1, p0, Lcom/google/r/b/a/fz;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/r/b/a/fz;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/fz;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    iput-object v1, v0, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 224
    check-cast p1, Lcom/google/r/b/a/fx;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/fz;->a(Lcom/google/r/b/a/fx;)Lcom/google/r/b/a/fz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/fx;)Lcom/google/r/b/a/fz;
    .locals 2

    .prologue
    .line 257
    invoke-static {}, Lcom/google/r/b/a/fx;->d()Lcom/google/r/b/a/fx;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 269
    :goto_0
    return-object p0

    .line 258
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    iget-object v0, p1, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    .line 261
    iget v0, p0, Lcom/google/r/b/a/fz;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/r/b/a/fz;->a:I

    .line 268
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/r/b/a/fx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 263
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/fz;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/r/b/a/fz;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/fz;->a:I

    .line 264
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/fz;->b:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/r/b/a/fx;->a:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    return v0
.end method
