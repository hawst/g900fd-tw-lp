.class public final Lcom/google/r/b/a/bz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/co;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/bu;",
        "Lcom/google/r/b/a/bz;",
        ">;",
        "Lcom/google/r/b/a/co;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3401
    sget-object v0, Lcom/google/r/b/a/bu;->j:Lcom/google/r/b/a/bu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3539
    iput v1, p0, Lcom/google/r/b/a/bz;->b:I

    .line 3576
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    .line 3650
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    .line 3724
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    .line 3861
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    .line 3966
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/bz;->i:Lcom/google/n/ao;

    .line 4025
    iput v1, p0, Lcom/google/r/b/a/bz;->f:I

    .line 3402
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 3863
    iget v0, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 3864
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    .line 3865
    iget v0, p0, Lcom/google/r/b/a/bz;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3867
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3393
    new-instance v2, Lcom/google/r/b/a/bu;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/bu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/bz;->b:I

    iput v4, v2, Lcom/google/r/b/a/bu;->b:I

    iget v4, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/bz;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/r/b/a/bz;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/r/b/a/bz;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/r/b/a/bz;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x2

    :cond_4
    iget-boolean v4, p0, Lcom/google/r/b/a/bz;->h:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/bu;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x4

    :cond_5
    iget-object v4, v2, Lcom/google/r/b/a/bu;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/bz;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/bz;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit8 v0, v0, 0x8

    :cond_6
    iget v1, p0, Lcom/google/r/b/a/bz;->f:I

    iput v1, v2, Lcom/google/r/b/a/bu;->i:I

    iput v0, v2, Lcom/google/r/b/a/bu;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3393
    check-cast p1, Lcom/google/r/b/a/bu;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/bz;->a(Lcom/google/r/b/a/bu;)Lcom/google/r/b/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/bu;)Lcom/google/r/b/a/bz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3475
    invoke-static {}, Lcom/google/r/b/a/bu;->d()Lcom/google/r/b/a/bu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3530
    :goto_0
    return-object p0

    .line 3476
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 3477
    iget v2, p1, Lcom/google/r/b/a/bu;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/cm;->a(I)Lcom/google/r/b/a/cm;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/cm;->a:Lcom/google/r/b/a/cm;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 3476
    goto :goto_1

    .line 3477
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/bz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/bz;->a:I

    iget v2, v2, Lcom/google/r/b/a/cm;->c:I

    iput v2, p0, Lcom/google/r/b/a/bz;->b:I

    .line 3479
    :cond_4
    iget-object v2, p1, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 3480
    iget-object v2, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 3481
    iget-object v2, p1, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    .line 3482
    iget v2, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3489
    :cond_5
    :goto_2
    iget-object v2, p1, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 3490
    iget-object v2, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 3491
    iget-object v2, p1, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    .line 3492
    iget v2, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3499
    :cond_6
    :goto_3
    iget-object v2, p1, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 3500
    iget-object v2, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 3501
    iget-object v2, p1, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    .line 3502
    iget v2, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3509
    :cond_7
    :goto_4
    iget-object v2, p1, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 3510
    iget-object v2, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 3511
    iget-object v2, p1, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    .line 3512
    iget v2, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3519
    :cond_8
    :goto_5
    iget v2, p1, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 3520
    iget-boolean v2, p1, Lcom/google/r/b/a/bu;->g:Z

    iget v3, p0, Lcom/google/r/b/a/bz;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/bz;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/bz;->h:Z

    .line 3522
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 3523
    iget-object v2, p0, Lcom/google/r/b/a/bz;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/bu;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3524
    iget v2, p0, Lcom/google/r/b/a/bz;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3526
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_b

    .line 3527
    iget v0, p1, Lcom/google/r/b/a/bu;->i:I

    iget v1, p0, Lcom/google/r/b/a/bz;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/bz;->a:I

    iput v0, p0, Lcom/google/r/b/a/bz;->f:I

    .line 3529
    :cond_b
    iget-object v0, p1, Lcom/google/r/b/a/bu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 3484
    :cond_c
    invoke-virtual {p0}, Lcom/google/r/b/a/bz;->c()V

    .line 3485
    iget-object v2, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 3494
    :cond_d
    invoke-virtual {p0}, Lcom/google/r/b/a/bz;->d()V

    .line 3495
    iget-object v2, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 3504
    :cond_e
    invoke-virtual {p0}, Lcom/google/r/b/a/bz;->i()V

    .line 3505
    iget-object v2, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    .line 3514
    :cond_f
    invoke-direct {p0}, Lcom/google/r/b/a/bz;->j()V

    .line 3515
    iget-object v2, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    :cond_10
    move v2, v1

    .line 3519
    goto :goto_6

    :cond_11
    move v2, v1

    .line 3522
    goto :goto_7

    :cond_12
    move v0, v1

    .line 3526
    goto :goto_8
.end method

.method public final a(Lcom/google/r/b/a/ck;)Lcom/google/r/b/a/bz;
    .locals 2

    .prologue
    .line 3904
    if-nez p1, :cond_0

    .line 3905
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3907
    :cond_0
    invoke-direct {p0}, Lcom/google/r/b/a/bz;->j()V

    .line 3908
    iget-object v0, p0, Lcom/google/r/b/a/bz;->g:Ljava/util/List;

    iget v1, p1, Lcom/google/r/b/a/ck;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3910
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3534
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 3578
    iget v0, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 3579
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/bz;->c:Ljava/util/List;

    .line 3580
    iget v0, p0, Lcom/google/r/b/a/bz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3582
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 3652
    iget v0, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 3653
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/bz;->d:Ljava/util/List;

    .line 3654
    iget v0, p0, Lcom/google/r/b/a/bz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3656
    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 3726
    iget v0, p0, Lcom/google/r/b/a/bz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 3727
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/bz;->e:Ljava/util/List;

    .line 3730
    iget v0, p0, Lcom/google/r/b/a/bz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/bz;->a:I

    .line 3732
    :cond_0
    return-void
.end method
