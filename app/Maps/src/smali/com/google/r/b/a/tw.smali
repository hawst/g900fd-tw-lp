.class public final Lcom/google/r/b/a/tw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ud;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/tw;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/tw;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/google/r/b/a/tx;

    invoke-direct {v0}, Lcom/google/r/b/a/tx;-><init>()V

    sput-object v0, Lcom/google/r/b/a/tw;->PARSER:Lcom/google/n/ax;

    .line 705
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/tw;->g:Lcom/google/n/aw;

    .line 1067
    new-instance v0, Lcom/google/r/b/a/tw;

    invoke-direct {v0}, Lcom/google/r/b/a/tw;-><init>()V

    sput-object v0, Lcom/google/r/b/a/tw;->d:Lcom/google/r/b/a/tw;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 54
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 656
    iput-byte v0, p0, Lcom/google/r/b/a/tw;->e:B

    .line 684
    iput v0, p0, Lcom/google/r/b/a/tw;->f:I

    .line 55
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/tw;->c:Ljava/lang/Object;

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 63
    invoke-direct {p0}, Lcom/google/r/b/a/tw;-><init>()V

    .line 66
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 69
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 71
    sparse-switch v4, :sswitch_data_0

    .line 76
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 78
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 84
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    .line 86
    or-int/lit8 v1, v1, 0x1

    .line 88
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 88
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 107
    iget-object v1, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    .line 109
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/tw;->au:Lcom/google/n/bn;

    throw v0

    .line 93
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 94
    iget v5, p0, Lcom/google/r/b/a/tw;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/tw;->a:I

    .line 95
    iput-object v4, p0, Lcom/google/r/b/a/tw;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 102
    :catch_1
    move-exception v0

    .line 103
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 104
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 106
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 107
    iget-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    .line 109
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tw;->au:Lcom/google/n/bn;

    .line 110
    return-void

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 52
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 656
    iput-byte v0, p0, Lcom/google/r/b/a/tw;->e:B

    .line 684
    iput v0, p0, Lcom/google/r/b/a/tw;->f:I

    .line 53
    return-void
.end method

.method public static g()Lcom/google/r/b/a/tw;
    .locals 1

    .prologue
    .line 1070
    sget-object v0, Lcom/google/r/b/a/tw;->d:Lcom/google/r/b/a/tw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ty;
    .locals 1

    .prologue
    .line 767
    new-instance v0, Lcom/google/r/b/a/ty;

    invoke-direct {v0}, Lcom/google/r/b/a/ty;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/tw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/google/r/b/a/tw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 674
    invoke-virtual {p0}, Lcom/google/r/b/a/tw;->c()I

    .line 675
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 675
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 678
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/tw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 679
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/r/b/a/tw;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tw;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 681
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/tw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 682
    return-void

    .line 679
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 658
    iget-byte v0, p0, Lcom/google/r/b/a/tw;->e:B

    .line 659
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 669
    :cond_0
    :goto_0
    return v2

    .line 660
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 662
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 663
    iget-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/tz;->d()Lcom/google/r/b/a/tz;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/tz;

    invoke-virtual {v0}, Lcom/google/r/b/a/tz;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 664
    iput-byte v2, p0, Lcom/google/r/b/a/tw;->e:B

    goto :goto_0

    .line 662
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 668
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/tw;->e:B

    move v2, v3

    .line 669
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 686
    iget v0, p0, Lcom/google/r/b/a/tw;->f:I

    .line 687
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 700
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 690
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 691
    iget-object v0, p0, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    .line 692
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 690
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 694
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/tw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 695
    const/4 v1, 0x2

    .line 696
    iget-object v0, p0, Lcom/google/r/b/a/tw;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/tw;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 698
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/tw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 699
    iput v0, p0, Lcom/google/r/b/a/tw;->f:I

    goto :goto_0

    .line 696
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/r/b/a/tw;->c:Ljava/lang/Object;

    .line 627
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 628
    check-cast v0, Ljava/lang/String;

    .line 636
    :goto_0
    return-object v0

    .line 630
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 632
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 633
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 634
    iput-object v1, p0, Lcom/google/r/b/a/tw;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 636
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/r/b/a/tw;->newBuilder()Lcom/google/r/b/a/ty;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ty;->a(Lcom/google/r/b/a/tw;)Lcom/google/r/b/a/ty;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/r/b/a/tw;->newBuilder()Lcom/google/r/b/a/ty;

    move-result-object v0

    return-object v0
.end method
