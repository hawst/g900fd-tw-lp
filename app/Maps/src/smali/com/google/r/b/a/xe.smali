.class public final enum Lcom/google/r/b/a/xe;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/xe;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/xe;

.field public static final enum b:Lcom/google/r/b/a/xe;

.field private static final synthetic d:[Lcom/google/r/b/a/xe;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 102
    new-instance v0, Lcom/google/r/b/a/xe;

    const-string v1, "GENERIC"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/xe;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/xe;->a:Lcom/google/r/b/a/xe;

    .line 106
    new-instance v0, Lcom/google/r/b/a/xe;

    const-string v1, "MAPS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/xe;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/xe;->b:Lcom/google/r/b/a/xe;

    .line 97
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/r/b/a/xe;

    sget-object v1, Lcom/google/r/b/a/xe;->a:Lcom/google/r/b/a/xe;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/xe;->b:Lcom/google/r/b/a/xe;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/r/b/a/xe;->d:[Lcom/google/r/b/a/xe;

    .line 136
    new-instance v0, Lcom/google/r/b/a/xf;

    invoke-direct {v0}, Lcom/google/r/b/a/xf;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 146
    iput p3, p0, Lcom/google/r/b/a/xe;->c:I

    .line 147
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/xe;
    .locals 1

    .prologue
    .line 124
    packed-switch p0, :pswitch_data_0

    .line 127
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 125
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/xe;->a:Lcom/google/r/b/a/xe;

    goto :goto_0

    .line 126
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/xe;->b:Lcom/google/r/b/a/xe;

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/xe;
    .locals 1

    .prologue
    .line 97
    const-class v0, Lcom/google/r/b/a/xe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/xe;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/xe;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/r/b/a/xe;->d:[Lcom/google/r/b/a/xe;

    invoke-virtual {v0}, [Lcom/google/r/b/a/xe;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/xe;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/google/r/b/a/xe;->c:I

    return v0
.end method
