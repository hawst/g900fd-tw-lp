.class public final Lcom/google/r/b/a/wp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ws;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/wn;",
        "Lcom/google/r/b/a/wp;",
        ">;",
        "Lcom/google/r/b/a/ws;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 514
    sget-object v0, Lcom/google/r/b/a/wn;->j:Lcom/google/r/b/a/wn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 619
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/wp;->b:Ljava/lang/Object;

    .line 695
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/wp;->c:I

    .line 795
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/wp;->f:Z

    .line 515
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 506
    new-instance v2, Lcom/google/r/b/a/wn;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/wn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/wp;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/wp;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/wn;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/wp;->c:I

    iput v1, v2, Lcom/google/r/b/a/wn;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/r/b/a/wp;->d:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/wn;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/r/b/a/wp;->e:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/wn;->e:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/r/b/a/wp;->f:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/wn;->f:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v1, p0, Lcom/google/r/b/a/wp;->g:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/wn;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v1, p0, Lcom/google/r/b/a/wp;->h:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/wn;->h:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v1, p0, Lcom/google/r/b/a/wp;->i:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/wn;->i:Z

    iput v0, v2, Lcom/google/r/b/a/wn;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 506
    check-cast p1, Lcom/google/r/b/a/wn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/wp;->a(Lcom/google/r/b/a/wn;)Lcom/google/r/b/a/wp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/wn;)Lcom/google/r/b/a/wp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 582
    invoke-static {}, Lcom/google/r/b/a/wn;->d()Lcom/google/r/b/a/wn;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 610
    :goto_0
    return-object p0

    .line 583
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 584
    iget v2, p0, Lcom/google/r/b/a/wp;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/wp;->a:I

    .line 585
    iget-object v2, p1, Lcom/google/r/b/a/wn;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/wp;->b:Ljava/lang/Object;

    .line 588
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 589
    iget v2, p1, Lcom/google/r/b/a/wn;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/wq;->a(I)Lcom/google/r/b/a/wq;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/r/b/a/wq;->a:Lcom/google/r/b/a/wq;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 583
    goto :goto_1

    :cond_4
    move v2, v1

    .line 588
    goto :goto_2

    .line 589
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/wp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/wp;->a:I

    iget v2, v2, Lcom/google/r/b/a/wq;->e:I

    iput v2, p0, Lcom/google/r/b/a/wp;->c:I

    .line 591
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 592
    iget-boolean v2, p1, Lcom/google/r/b/a/wn;->d:Z

    iget v3, p0, Lcom/google/r/b/a/wp;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/wp;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/wp;->d:Z

    .line 594
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 595
    iget-boolean v2, p1, Lcom/google/r/b/a/wn;->e:Z

    iget v3, p0, Lcom/google/r/b/a/wp;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/wp;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/wp;->e:Z

    .line 597
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_9

    .line 598
    iget-boolean v2, p1, Lcom/google/r/b/a/wn;->f:Z

    iget v3, p0, Lcom/google/r/b/a/wp;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/wp;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/wp;->f:Z

    .line 600
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_a

    .line 601
    iget-boolean v2, p1, Lcom/google/r/b/a/wn;->g:Z

    iget v3, p0, Lcom/google/r/b/a/wp;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/wp;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/wp;->g:Z

    .line 603
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_b

    .line 604
    iget-boolean v2, p1, Lcom/google/r/b/a/wn;->h:Z

    iget v3, p0, Lcom/google/r/b/a/wp;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/wp;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/wp;->h:Z

    .line 606
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/wn;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_c

    .line 607
    iget-boolean v0, p1, Lcom/google/r/b/a/wn;->i:Z

    iget v1, p0, Lcom/google/r/b/a/wp;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/wp;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/wp;->i:Z

    .line 609
    :cond_c
    iget-object v0, p1, Lcom/google/r/b/a/wn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 591
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 594
    goto :goto_4

    :cond_f
    move v2, v1

    .line 597
    goto :goto_5

    :cond_10
    move v2, v1

    .line 600
    goto :goto_6

    :cond_11
    move v2, v1

    .line 603
    goto :goto_7

    :cond_12
    move v0, v1

    .line 606
    goto :goto_8
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 614
    const/4 v0, 0x1

    return v0
.end method
