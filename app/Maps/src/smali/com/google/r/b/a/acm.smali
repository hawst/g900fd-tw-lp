.class public final Lcom/google/r/b/a/acm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/acp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/acm;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/acm;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13844
    new-instance v0, Lcom/google/r/b/a/acn;

    invoke-direct {v0}, Lcom/google/r/b/a/acn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/acm;->PARSER:Lcom/google/n/ax;

    .line 13987
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/acm;->g:Lcom/google/n/aw;

    .line 14276
    new-instance v0, Lcom/google/r/b/a/acm;

    invoke-direct {v0}, Lcom/google/r/b/a/acm;-><init>()V

    sput-object v0, Lcom/google/r/b/a/acm;->d:Lcom/google/r/b/a/acm;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 13793
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 13944
    iput-byte v0, p0, Lcom/google/r/b/a/acm;->e:B

    .line 13966
    iput v0, p0, Lcom/google/r/b/a/acm;->f:I

    .line 13794
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    .line 13795
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    .line 13796
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 13802
    invoke-direct {p0}, Lcom/google/r/b/a/acm;-><init>()V

    .line 13803
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 13807
    const/4 v0, 0x0

    .line 13808
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 13809
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 13810
    sparse-switch v3, :sswitch_data_0

    .line 13815
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 13817
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 13813
    goto :goto_0

    .line 13822
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 13823
    iget v4, p0, Lcom/google/r/b/a/acm;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/acm;->a:I

    .line 13824
    iput-object v3, p0, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 13835
    :catch_0
    move-exception v0

    .line 13836
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13841
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/acm;->au:Lcom/google/n/bn;

    throw v0

    .line 13828
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 13829
    iget v4, p0, Lcom/google/r/b/a/acm;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/acm;->a:I

    .line 13830
    iput-object v3, p0, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 13837
    :catch_1
    move-exception v0

    .line 13838
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 13839
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 13841
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acm;->au:Lcom/google/n/bn;

    .line 13842
    return-void

    .line 13810
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 13791
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 13944
    iput-byte v0, p0, Lcom/google/r/b/a/acm;->e:B

    .line 13966
    iput v0, p0, Lcom/google/r/b/a/acm;->f:I

    .line 13792
    return-void
.end method

.method public static h()Lcom/google/r/b/a/acm;
    .locals 1

    .prologue
    .line 14279
    sget-object v0, Lcom/google/r/b/a/acm;->d:Lcom/google/r/b/a/acm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aco;
    .locals 1

    .prologue
    .line 14049
    new-instance v0, Lcom/google/r/b/a/aco;

    invoke-direct {v0}, Lcom/google/r/b/a/aco;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/acm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13856
    sget-object v0, Lcom/google/r/b/a/acm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 13956
    invoke-virtual {p0}, Lcom/google/r/b/a/acm;->c()I

    .line 13957
    iget v0, p0, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 13958
    iget-object v0, p0, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13960
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 13961
    iget-object v0, p0, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13963
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/acm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 13964
    return-void

    .line 13958
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 13961
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 13946
    iget-byte v1, p0, Lcom/google/r/b/a/acm;->e:B

    .line 13947
    if-ne v1, v0, :cond_0

    .line 13951
    :goto_0
    return v0

    .line 13948
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 13950
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/acm;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13968
    iget v0, p0, Lcom/google/r/b/a/acm;->f:I

    .line 13969
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13982
    :goto_0
    return v0

    .line 13972
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 13974
    iget-object v0, p0, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 13976
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 13978
    iget-object v0, p0, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 13980
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/acm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 13981
    iput v0, p0, Lcom/google/r/b/a/acm;->f:I

    goto :goto_0

    .line 13974
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 13978
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13872
    iget-object v0, p0, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    .line 13873
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13874
    check-cast v0, Ljava/lang/String;

    .line 13882
    :goto_0
    return-object v0

    .line 13876
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 13878
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 13879
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13880
    iput-object v1, p0, Lcom/google/r/b/a/acm;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 13882
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13785
    invoke-static {}, Lcom/google/r/b/a/acm;->newBuilder()Lcom/google/r/b/a/aco;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aco;->a(Lcom/google/r/b/a/acm;)Lcom/google/r/b/a/aco;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13785
    invoke-static {}, Lcom/google/r/b/a/acm;->newBuilder()Lcom/google/r/b/a/aco;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13914
    iget-object v0, p0, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    .line 13915
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 13916
    check-cast v0, Ljava/lang/String;

    .line 13924
    :goto_0
    return-object v0

    .line 13918
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 13920
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 13921
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13922
    iput-object v1, p0, Lcom/google/r/b/a/acm;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 13924
    goto :goto_0
.end method
