.class public final Lcom/google/r/b/a/agw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/agz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/agt;",
        "Lcom/google/r/b/a/agw;",
        ">;",
        "Lcom/google/r/b/a/agz;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field public e:Z

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2823
    sget-object v0, Lcom/google/r/b/a/agt;->j:Lcom/google/r/b/a/agt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2925
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/r/b/a/agw;->b:I

    .line 2961
    iput v1, p0, Lcom/google/r/b/a/agw;->c:I

    .line 3029
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/agw;->g:I

    .line 3065
    iput v1, p0, Lcom/google/r/b/a/agw;->h:I

    .line 3134
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    .line 2824
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 3136
    iget v0, p0, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_0

    .line 3137
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    .line 3138
    iget v0, p0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/agw;->a:I

    .line 3140
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2815
    new-instance v2, Lcom/google/r/b/a/agt;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/agt;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/agw;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/agw;->b:I

    iput v1, v2, Lcom/google/r/b/a/agt;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/agw;->c:I

    iput v1, v2, Lcom/google/r/b/a/agt;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/r/b/a/agw;->d:J

    iput-wide v4, v2, Lcom/google/r/b/a/agt;->d:J

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/agw;->g:I

    iput v1, v2, Lcom/google/r/b/a/agt;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/r/b/a/agw;->h:I

    iput v1, v2, Lcom/google/r/b/a/agt;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v1, p0, Lcom/google/r/b/a/agw;->e:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/agt;->g:Z

    iget v1, p0, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/google/r/b/a/agw;->a:I

    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/agt;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2815
    check-cast p1, Lcom/google/r/b/a/agt;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/agw;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/maps/g/a/gs;)Lcom/google/r/b/a/agw;
    .locals 1

    .prologue
    .line 3083
    if-nez p1, :cond_0

    .line 3084
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3086
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/agw;->a:I

    .line 3087
    iget v0, p1, Lcom/google/maps/g/a/gs;->d:I

    iput v0, p0, Lcom/google/r/b/a/agw;->h:I

    .line 3089
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;
    .locals 2

    .prologue
    .line 3177
    if-nez p1, :cond_0

    .line 3178
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3180
    :cond_0
    invoke-direct {p0}, Lcom/google/r/b/a/agw;->c()V

    .line 3181
    iget-object v0, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    iget v1, p1, Lcom/google/maps/g/a/iy;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3183
    return-object p0
.end method

.method public final a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2886
    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2916
    :goto_0
    return-object p0

    .line 2887
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 2888
    iget v0, p1, Lcom/google/r/b/a/agt;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hc;->a(I)Lcom/google/maps/g/a/hc;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/hc;->a:Lcom/google/maps/g/a/hc;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    .line 2887
    goto :goto_1

    .line 2888
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/agw;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hc;->i:I

    iput v0, p0, Lcom/google/r/b/a/agw;->b:I

    .line 2890
    :cond_4
    iget v0, p1, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 2891
    iget v0, p1, Lcom/google/r/b/a/agt;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/fu;->a(I)Lcom/google/maps/g/a/fu;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/maps/g/a/fu;->a:Lcom/google/maps/g/a/fu;

    :cond_5
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v0, v2

    .line 2890
    goto :goto_2

    .line 2891
    :cond_7
    iget v3, p0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/agw;->a:I

    iget v0, v0, Lcom/google/maps/g/a/fu;->c:I

    iput v0, p0, Lcom/google/r/b/a/agw;->c:I

    .line 2893
    :cond_8
    iget v0, p1, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_3
    if-eqz v0, :cond_9

    .line 2894
    iget-wide v4, p1, Lcom/google/r/b/a/agt;->d:J

    iget v0, p0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/agw;->a:I

    iput-wide v4, p0, Lcom/google/r/b/a/agw;->d:J

    .line 2896
    :cond_9
    iget v0, p1, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_4
    if-eqz v0, :cond_e

    .line 2897
    iget v0, p1, Lcom/google/r/b/a/agt;->e:I

    invoke-static {v0}, Lcom/google/r/b/a/agx;->a(I)Lcom/google/r/b/a/agx;

    move-result-object v0

    if-nez v0, :cond_a

    sget-object v0, Lcom/google/r/b/a/agx;->a:Lcom/google/r/b/a/agx;

    :cond_a
    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move v0, v2

    .line 2893
    goto :goto_3

    :cond_c
    move v0, v2

    .line 2896
    goto :goto_4

    .line 2897
    :cond_d
    iget v3, p0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/agw;->a:I

    iget v0, v0, Lcom/google/r/b/a/agx;->g:I

    iput v0, p0, Lcom/google/r/b/a/agw;->g:I

    .line 2899
    :cond_e
    iget v0, p1, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_13

    move v0, v1

    :goto_5
    if-eqz v0, :cond_10

    .line 2900
    iget v0, p1, Lcom/google/r/b/a/agt;->f:I

    invoke-static {v0}, Lcom/google/maps/g/a/gs;->a(I)Lcom/google/maps/g/a/gs;

    move-result-object v0

    if-nez v0, :cond_f

    sget-object v0, Lcom/google/maps/g/a/gs;->a:Lcom/google/maps/g/a/gs;

    :cond_f
    invoke-virtual {p0, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/gs;)Lcom/google/r/b/a/agw;

    .line 2902
    :cond_10
    iget v0, p1, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_14

    move v0, v1

    :goto_6
    if-eqz v0, :cond_11

    .line 2903
    iget-boolean v0, p1, Lcom/google/r/b/a/agt;->g:Z

    iget v1, p0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/r/b/a/agw;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/agw;->e:Z

    .line 2905
    :cond_11
    iget-object v0, p1, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 2906
    iget-object v0, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2907
    iget-object v0, p1, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    .line 2908
    iget v0, p0, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/r/b/a/agw;->a:I

    .line 2915
    :cond_12
    :goto_7
    iget-object v0, p1, Lcom/google/r/b/a/agt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v0, v2

    .line 2899
    goto :goto_5

    :cond_14
    move v0, v2

    .line 2902
    goto :goto_6

    .line 2910
    :cond_15
    invoke-direct {p0}, Lcom/google/r/b/a/agw;->c()V

    .line 2911
    iget-object v0, p0, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_7
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2920
    const/4 v0, 0x1

    return v0
.end method
