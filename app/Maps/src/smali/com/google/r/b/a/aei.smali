.class public final Lcom/google/r/b/a/aei;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aej;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aeg;",
        "Lcom/google/r/b/a/aei;",
        ">;",
        "Lcom/google/r/b/a/aej;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 20006
    sget-object v0, Lcom/google/r/b/a/aeg;->f:Lcom/google/r/b/a/aeg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 20088
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    .line 20224
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aei;->c:Ljava/lang/Object;

    .line 20300
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aei;->d:Ljava/lang/Object;

    .line 20376
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aei;->e:Ljava/lang/Object;

    .line 20007
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 19998
    new-instance v2, Lcom/google/r/b/a/aeg;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aeg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aei;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/r/b/a/aei;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aei;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/aei;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/aei;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/aeg;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/aei;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/aeg;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/aei;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/aeg;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/aeg;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 19998
    check-cast p1, Lcom/google/r/b/a/aeg;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aei;->a(Lcom/google/r/b/a/aeg;)Lcom/google/r/b/a/aei;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aeg;)Lcom/google/r/b/a/aei;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 20051
    invoke-static {}, Lcom/google/r/b/a/aeg;->d()Lcom/google/r/b/a/aeg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 20078
    :goto_0
    return-object p0

    .line 20052
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 20053
    iget-object v2, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 20054
    iget-object v2, p1, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    .line 20055
    iget v2, p0, Lcom/google/r/b/a/aei;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/aei;->a:I

    .line 20062
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 20063
    iget v2, p0, Lcom/google/r/b/a/aei;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/aei;->a:I

    .line 20064
    iget-object v2, p1, Lcom/google/r/b/a/aeg;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aei;->c:Ljava/lang/Object;

    .line 20067
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 20068
    iget v2, p0, Lcom/google/r/b/a/aei;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/aei;->a:I

    .line 20069
    iget-object v2, p1, Lcom/google/r/b/a/aeg;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aei;->d:Ljava/lang/Object;

    .line 20072
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/aeg;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    :goto_4
    if-eqz v0, :cond_4

    .line 20073
    iget v0, p0, Lcom/google/r/b/a/aei;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/aei;->a:I

    .line 20074
    iget-object v0, p1, Lcom/google/r/b/a/aeg;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/aei;->e:Ljava/lang/Object;

    .line 20077
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/aeg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 20057
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/aei;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/aei;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/aei;->a:I

    .line 20058
    :cond_6
    iget-object v2, p0, Lcom/google/r/b/a/aei;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/aeg;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_7
    move v2, v1

    .line 20062
    goto :goto_2

    :cond_8
    move v2, v1

    .line 20067
    goto :goto_3

    :cond_9
    move v0, v1

    .line 20072
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 20082
    const/4 v0, 0x1

    return v0
.end method
