.class public final Lcom/google/r/b/a/aes;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aev;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aes;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/aes;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/google/r/b/a/aet;

    invoke-direct {v0}, Lcom/google/r/b/a/aet;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aes;->PARSER:Lcom/google/n/ax;

    .line 274
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aes;->h:Lcom/google/n/aw;

    .line 697
    new-instance v0, Lcom/google/r/b/a/aes;

    invoke-direct {v0}, Lcom/google/r/b/a/aes;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aes;->e:Lcom/google/r/b/a/aes;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 138
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    .line 197
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    .line 212
    iput-byte v2, p0, Lcom/google/r/b/a/aes;->f:B

    .line 249
    iput v2, p0, Lcom/google/r/b/a/aes;->g:I

    .line 59
    iget-object v0, p0, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    .line 61
    iget-object v0, p0, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 62
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Lcom/google/r/b/a/aes;-><init>()V

    .line 71
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 74
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 75
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 76
    sparse-switch v4, :sswitch_data_0

    .line 81
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 83
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 79
    goto :goto_0

    .line 88
    :sswitch_1
    iget-object v4, p0, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 89
    iget v4, p0, Lcom/google/r/b/a/aes;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/aes;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 110
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 116
    iget-object v1, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    .line 118
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aes;->au:Lcom/google/n/bn;

    throw v0

    .line 93
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_2

    .line 94
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    .line 96
    or-int/lit8 v1, v1, 0x2

    .line 98
    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 98
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 111
    :catch_1
    move-exception v0

    .line 112
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 113
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 104
    iget v4, p0, Lcom/google/r/b/a/aes;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/aes;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 115
    :cond_3
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_4

    .line 116
    iget-object v0, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    .line 118
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aes;->au:Lcom/google/n/bn;

    .line 119
    return-void

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 56
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 138
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    .line 197
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    .line 212
    iput-byte v1, p0, Lcom/google/r/b/a/aes;->f:B

    .line 249
    iput v1, p0, Lcom/google/r/b/a/aes;->g:I

    .line 57
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aes;
    .locals 1

    .prologue
    .line 700
    sget-object v0, Lcom/google/r/b/a/aes;->e:Lcom/google/r/b/a/aes;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aeu;
    .locals 1

    .prologue
    .line 336
    new-instance v0, Lcom/google/r/b/a/aeu;

    invoke-direct {v0}, Lcom/google/r/b/a/aeu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aes;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    sget-object v0, Lcom/google/r/b/a/aes;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 236
    invoke-virtual {p0}, Lcom/google/r/b/a/aes;->c()I

    .line 237
    iget v0, p0, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 240
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 240
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 243
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 244
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/aes;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 247
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 214
    iget-byte v0, p0, Lcom/google/r/b/a/aes;->f:B

    .line 215
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 231
    :goto_0
    return v0

    .line 216
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 218
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 219
    iget-object v0, p0, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 220
    iput-byte v2, p0, Lcom/google/r/b/a/aes;->f:B

    move v0, v2

    .line 221
    goto :goto_0

    :cond_2
    move v0, v2

    .line 218
    goto :goto_1

    .line 224
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 225
    iget-object v0, p0, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aag;

    invoke-virtual {v0}, Lcom/google/r/b/a/aag;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 226
    iput-byte v2, p0, Lcom/google/r/b/a/aes;->f:B

    move v0, v2

    .line 227
    goto :goto_0

    :cond_4
    move v0, v2

    .line 224
    goto :goto_2

    .line 230
    :cond_5
    iput-byte v1, p0, Lcom/google/r/b/a/aes;->f:B

    move v0, v1

    .line 231
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 251
    iget v0, p0, Lcom/google/r/b/a/aes;->g:I

    .line 252
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 269
    :goto_0
    return v0

    .line 255
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 256
    iget-object v0, p0, Lcom/google/r/b/a/aes;->b:Lcom/google/n/ao;

    .line 257
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 259
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/google/r/b/a/aes;->c:Ljava/util/List;

    .line 261
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 259
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 263
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aes;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 264
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/r/b/a/aes;->d:Lcom/google/n/ao;

    .line 265
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 267
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/aes;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 268
    iput v0, p0, Lcom/google/r/b/a/aes;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/r/b/a/aes;->newBuilder()Lcom/google/r/b/a/aeu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aeu;->a(Lcom/google/r/b/a/aes;)Lcom/google/r/b/a/aeu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/r/b/a/aes;->newBuilder()Lcom/google/r/b/a/aeu;

    move-result-object v0

    return-object v0
.end method
