.class public final enum Lcom/google/r/b/a/mt;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/mt;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/mt;

.field public static final enum b:Lcom/google/r/b/a/mt;

.field public static final enum c:Lcom/google/r/b/a/mt;

.field public static final enum d:Lcom/google/r/b/a/mt;

.field private static final synthetic f:[Lcom/google/r/b/a/mt;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 347
    new-instance v0, Lcom/google/r/b/a/mt;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/mt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/mt;->a:Lcom/google/r/b/a/mt;

    .line 351
    new-instance v0, Lcom/google/r/b/a/mt;

    const-string v1, "NO_DATA"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/mt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/mt;->b:Lcom/google/r/b/a/mt;

    .line 355
    new-instance v0, Lcom/google/r/b/a/mt;

    const-string v1, "STOP"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/mt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/mt;->c:Lcom/google/r/b/a/mt;

    .line 359
    new-instance v0, Lcom/google/r/b/a/mt;

    const-string v1, "TRAVEL"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/mt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/mt;->d:Lcom/google/r/b/a/mt;

    .line 342
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/mt;

    sget-object v1, Lcom/google/r/b/a/mt;->a:Lcom/google/r/b/a/mt;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/mt;->b:Lcom/google/r/b/a/mt;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/mt;->c:Lcom/google/r/b/a/mt;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/mt;->d:Lcom/google/r/b/a/mt;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/mt;->f:[Lcom/google/r/b/a/mt;

    .line 399
    new-instance v0, Lcom/google/r/b/a/mu;

    invoke-direct {v0}, Lcom/google/r/b/a/mu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 408
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 409
    iput p3, p0, Lcom/google/r/b/a/mt;->e:I

    .line 410
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/mt;
    .locals 1

    .prologue
    .line 385
    packed-switch p0, :pswitch_data_0

    .line 390
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 386
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/mt;->a:Lcom/google/r/b/a/mt;

    goto :goto_0

    .line 387
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/mt;->b:Lcom/google/r/b/a/mt;

    goto :goto_0

    .line 388
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/mt;->c:Lcom/google/r/b/a/mt;

    goto :goto_0

    .line 389
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/mt;->d:Lcom/google/r/b/a/mt;

    goto :goto_0

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/mt;
    .locals 1

    .prologue
    .line 342
    const-class v0, Lcom/google/r/b/a/mt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/mt;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/mt;
    .locals 1

    .prologue
    .line 342
    sget-object v0, Lcom/google/r/b/a/mt;->f:[Lcom/google/r/b/a/mt;

    invoke-virtual {v0}, [Lcom/google/r/b/a/mt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/mt;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lcom/google/r/b/a/mt;->e:I

    return v0
.end method
