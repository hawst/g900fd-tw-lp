.class public final Lcom/google/r/b/a/aip;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ais;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aip;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/aip;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/f;

.field d:Lcom/google/n/ao;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1355
    new-instance v0, Lcom/google/r/b/a/aiq;

    invoke-direct {v0}, Lcom/google/r/b/a/aiq;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aip;->PARSER:Lcom/google/n/ax;

    .line 1517
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aip;->i:Lcom/google/n/aw;

    .line 1905
    new-instance v0, Lcom/google/r/b/a/aip;

    invoke-direct {v0}, Lcom/google/r/b/a/aip;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aip;->f:Lcom/google/r/b/a/aip;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1293
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1372
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aip;->b:Lcom/google/n/ao;

    .line 1403
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aip;->d:Lcom/google/n/ao;

    .line 1460
    iput-byte v2, p0, Lcom/google/r/b/a/aip;->g:B

    .line 1488
    iput v2, p0, Lcom/google/r/b/a/aip;->h:I

    .line 1294
    iget-object v0, p0, Lcom/google/r/b/a/aip;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1295
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/aip;->c:Lcom/google/n/f;

    .line 1296
    iget-object v0, p0, Lcom/google/r/b/a/aip;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1297
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aip;->e:Ljava/lang/Object;

    .line 1298
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1304
    invoke-direct {p0}, Lcom/google/r/b/a/aip;-><init>()V

    .line 1305
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1310
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1311
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1312
    sparse-switch v3, :sswitch_data_0

    .line 1317
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1319
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1315
    goto :goto_0

    .line 1324
    :sswitch_1
    iget v3, p0, Lcom/google/r/b/a/aip;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/aip;->a:I

    .line 1325
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/r/b/a/aip;->c:Lcom/google/n/f;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1346
    :catch_0
    move-exception v0

    .line 1347
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1352
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aip;->au:Lcom/google/n/bn;

    throw v0

    .line 1329
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/aip;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1330
    iget v3, p0, Lcom/google/r/b/a/aip;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/aip;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1348
    :catch_1
    move-exception v0

    .line 1349
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1350
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1334
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1335
    iget v4, p0, Lcom/google/r/b/a/aip;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/aip;->a:I

    .line 1336
    iput-object v3, p0, Lcom/google/r/b/a/aip;->e:Ljava/lang/Object;

    goto :goto_0

    .line 1340
    :sswitch_4
    iget-object v3, p0, Lcom/google/r/b/a/aip;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1341
    iget v3, p0, Lcom/google/r/b/a/aip;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aip;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1352
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aip;->au:Lcom/google/n/bn;

    .line 1353
    return-void

    .line 1312
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1291
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1372
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aip;->b:Lcom/google/n/ao;

    .line 1403
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aip;->d:Lcom/google/n/ao;

    .line 1460
    iput-byte v1, p0, Lcom/google/r/b/a/aip;->g:B

    .line 1488
    iput v1, p0, Lcom/google/r/b/a/aip;->h:I

    .line 1292
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aip;
    .locals 1

    .prologue
    .line 1908
    sget-object v0, Lcom/google/r/b/a/aip;->f:Lcom/google/r/b/a/aip;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/air;
    .locals 1

    .prologue
    .line 1579
    new-instance v0, Lcom/google/r/b/a/air;

    invoke-direct {v0}, Lcom/google/r/b/a/air;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aip;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1367
    sget-object v0, Lcom/google/r/b/a/aip;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x2

    const/4 v3, 0x1

    .line 1472
    invoke-virtual {p0}, Lcom/google/r/b/a/aip;->c()I

    .line 1473
    iget v0, p0, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/google/r/b/a/aip;->c:Lcom/google/n/f;

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1476
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 1477
    iget-object v0, p0, Lcom/google/r/b/a/aip;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1479
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 1480
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/aip;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aip;->e:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1482
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 1483
    iget-object v0, p0, Lcom/google/r/b/a/aip;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1485
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/aip;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1486
    return-void

    .line 1480
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1462
    iget-byte v1, p0, Lcom/google/r/b/a/aip;->g:B

    .line 1463
    if-ne v1, v0, :cond_0

    .line 1467
    :goto_0
    return v0

    .line 1464
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1466
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aip;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1490
    iget v0, p0, Lcom/google/r/b/a/aip;->h:I

    .line 1491
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1512
    :goto_0
    return v0

    .line 1494
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_5

    .line 1495
    iget-object v0, p0, Lcom/google/r/b/a/aip;->c:Lcom/google/n/f;

    .line 1496
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1498
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_4

    .line 1499
    iget-object v2, p0, Lcom/google/r/b/a/aip;->d:Lcom/google/n/ao;

    .line 1500
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 1502
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    .line 1503
    const/4 v3, 0x3

    .line 1504
    iget-object v0, p0, Lcom/google/r/b/a/aip;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aip;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1506
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aip;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 1507
    iget-object v0, p0, Lcom/google/r/b/a/aip;->b:Lcom/google/n/ao;

    .line 1508
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1510
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/aip;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1511
    iput v0, p0, Lcom/google/r/b/a/aip;->h:I

    goto/16 :goto_0

    .line 1504
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v2, v0

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1285
    invoke-static {}, Lcom/google/r/b/a/aip;->newBuilder()Lcom/google/r/b/a/air;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/air;->a(Lcom/google/r/b/a/aip;)Lcom/google/r/b/a/air;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1285
    invoke-static {}, Lcom/google/r/b/a/aip;->newBuilder()Lcom/google/r/b/a/air;

    move-result-object v0

    return-object v0
.end method
