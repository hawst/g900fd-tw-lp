.class public final Lcom/google/r/b/a/amj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/amh;",
        "Lcom/google/r/b/a/amj;",
        ">;",
        "Lcom/google/r/b/a/amm;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 421
    sget-object v0, Lcom/google/r/b/a/amh;->g:Lcom/google/r/b/a/amh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 499
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/amj;->b:Z

    .line 531
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/amj;->c:I

    .line 599
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/r/b/a/amj;->e:I

    .line 631
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amj;->f:Ljava/lang/Object;

    .line 422
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 413
    new-instance v2, Lcom/google/r/b/a/amh;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/amh;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/amj;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-boolean v1, p0, Lcom/google/r/b/a/amj;->b:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/amh;->b:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/r/b/a/amj;->c:I

    iput v1, v2, Lcom/google/r/b/a/amh;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/amj;->d:I

    iput v1, v2, Lcom/google/r/b/a/amh;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/amj;->e:I

    iput v1, v2, Lcom/google/r/b/a/amh;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/amj;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/amh;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 413
    check-cast p1, Lcom/google/r/b/a/amh;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/amj;->a(Lcom/google/r/b/a/amh;)Lcom/google/r/b/a/amj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/amh;)Lcom/google/r/b/a/amj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 471
    invoke-static {}, Lcom/google/r/b/a/amh;->d()Lcom/google/r/b/a/amh;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 490
    :goto_0
    return-object p0

    .line 472
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 473
    iget-boolean v2, p1, Lcom/google/r/b/a/amh;->b:Z

    iget v3, p0, Lcom/google/r/b/a/amj;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/amj;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/amj;->b:Z

    .line 475
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 476
    iget v2, p1, Lcom/google/r/b/a/amh;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/amk;->a(I)Lcom/google/r/b/a/amk;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/r/b/a/amk;->a:Lcom/google/r/b/a/amk;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 472
    goto :goto_1

    :cond_4
    move v2, v1

    .line 475
    goto :goto_2

    .line 476
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/amj;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/amj;->a:I

    iget v2, v2, Lcom/google/r/b/a/amk;->d:I

    iput v2, p0, Lcom/google/r/b/a/amj;->c:I

    .line 478
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 479
    iget v2, p1, Lcom/google/r/b/a/amh;->d:I

    iget v3, p0, Lcom/google/r/b/a/amj;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/amj;->a:I

    iput v2, p0, Lcom/google/r/b/a/amj;->d:I

    .line 481
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 482
    iget v2, p1, Lcom/google/r/b/a/amh;->e:I

    iget v3, p0, Lcom/google/r/b/a/amj;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/amj;->a:I

    iput v2, p0, Lcom/google/r/b/a/amj;->e:I

    .line 484
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/amh;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    :goto_5
    if-eqz v0, :cond_9

    .line 485
    iget v0, p0, Lcom/google/r/b/a/amj;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/amj;->a:I

    .line 486
    iget-object v0, p1, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/amj;->f:Ljava/lang/Object;

    .line 489
    :cond_9
    iget-object v0, p1, Lcom/google/r/b/a/amh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 478
    goto :goto_3

    :cond_b
    move v2, v1

    .line 481
    goto :goto_4

    :cond_c
    move v0, v1

    .line 484
    goto :goto_5
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 494
    const/4 v0, 0x1

    return v0
.end method
