.class public final Lcom/google/r/b/a/mn;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ms;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ml;",
        "Lcom/google/r/b/a/mn;",
        ">;",
        "Lcom/google/r/b/a/ms;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/mo;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1128
    sget-object v0, Lcom/google/r/b/a/ml;->e:Lcom/google/r/b/a/ml;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1209
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    .line 1334
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    .line 1458
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/mn;->d:I

    .line 1129
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1120
    invoke-virtual {p0}, Lcom/google/r/b/a/mn;->c()Lcom/google/r/b/a/ml;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1120
    check-cast p1, Lcom/google/r/b/a/ml;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/mn;->a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ml;)Lcom/google/r/b/a/mn;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1168
    invoke-static {}, Lcom/google/r/b/a/ml;->d()Lcom/google/r/b/a/ml;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1193
    :goto_0
    return-object p0

    .line 1169
    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1170
    iget-object v1, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1171
    iget-object v1, p1, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    .line 1172
    iget v1, p0, Lcom/google/r/b/a/mn;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/r/b/a/mn;->a:I

    .line 1179
    :cond_1
    :goto_1
    iget-object v1, p1, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1180
    iget-object v1, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1181
    iget-object v1, p1, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    iput-object v1, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    .line 1182
    iget v1, p0, Lcom/google/r/b/a/mn;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/mn;->a:I

    .line 1189
    :cond_2
    :goto_2
    iget v1, p1, Lcom/google/r/b/a/ml;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_3
    if-eqz v0, :cond_a

    .line 1190
    iget v0, p1, Lcom/google/r/b/a/ml;->d:I

    invoke-static {v0}, Lcom/google/maps/g/mc;->a(I)Lcom/google/maps/g/mc;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/mc;->c:Lcom/google/maps/g/mc;

    :cond_3
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1174
    :cond_4
    iget v1, p0, Lcom/google/r/b/a/mn;->a:I

    and-int/lit8 v1, v1, 0x1

    if-eq v1, v0, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/mn;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/mn;->a:I

    .line 1175
    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1184
    :cond_6
    iget v1, p0, Lcom/google/r/b/a/mn;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/mn;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/r/b/a/mn;->a:I

    .line 1185
    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    iget-object v2, p1, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 1189
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 1190
    :cond_9
    iget v1, p0, Lcom/google/r/b/a/mn;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/mn;->a:I

    iget v0, v0, Lcom/google/maps/g/mc;->e:I

    iput v0, p0, Lcom/google/r/b/a/mn;->d:I

    .line 1192
    :cond_a
    iget-object v0, p1, Lcom/google/r/b/a/ml;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1197
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1198
    iget-object v0, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/mo;

    invoke-virtual {v0}, Lcom/google/r/b/a/mo;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1203
    :goto_1
    return v2

    .line 1197
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1203
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final c()Lcom/google/r/b/a/ml;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1146
    new-instance v2, Lcom/google/r/b/a/ml;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ml;-><init>(Lcom/google/n/v;)V

    .line 1147
    iget v3, p0, Lcom/google/r/b/a/mn;->a:I

    .line 1148
    const/4 v1, 0x0

    .line 1149
    iget v4, p0, Lcom/google/r/b/a/mn;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 1150
    iget-object v4, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    .line 1151
    iget v4, p0, Lcom/google/r/b/a/mn;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/mn;->a:I

    .line 1153
    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/mn;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ml;->b:Ljava/util/List;

    .line 1154
    iget v4, p0, Lcom/google/r/b/a/mn;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 1155
    iget-object v4, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    .line 1156
    iget v4, p0, Lcom/google/r/b/a/mn;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/r/b/a/mn;->a:I

    .line 1158
    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/mn;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/ml;->c:Ljava/util/List;

    .line 1159
    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 1162
    :goto_0
    iget v1, p0, Lcom/google/r/b/a/mn;->d:I

    iput v1, v2, Lcom/google/r/b/a/ml;->d:I

    .line 1163
    iput v0, v2, Lcom/google/r/b/a/ml;->a:I

    .line 1164
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method
