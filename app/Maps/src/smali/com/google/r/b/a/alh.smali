.class public final Lcom/google/r/b/a/alh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aln;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/alh;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/maps/g/px;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/maps/g/pz;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/maps/g/qj;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/r/b/a/alh;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/n/ao;

.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field public i:Lcom/google/n/ao;

.field public j:Lcom/google/n/ao;

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public m:I

.field public n:Lcom/google/n/aq;

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12917
    new-instance v0, Lcom/google/r/b/a/ali;

    invoke-direct {v0}, Lcom/google/r/b/a/ali;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alh;->PARSER:Lcom/google/n/ax;

    .line 12936
    new-instance v0, Lcom/google/r/b/a/alj;

    invoke-direct {v0}, Lcom/google/r/b/a/alj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alh;->c:Lcom/google/n/aj;

    .line 12999
    new-instance v0, Lcom/google/r/b/a/alk;

    invoke-direct {v0}, Lcom/google/r/b/a/alk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alh;->g:Lcom/google/n/aj;

    .line 13078
    new-instance v0, Lcom/google/r/b/a/all;

    invoke-direct {v0}, Lcom/google/r/b/a/all;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alh;->l:Lcom/google/n/aj;

    .line 13269
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/alh;->r:Lcom/google/n/aw;

    .line 14111
    new-instance v0, Lcom/google/r/b/a/alh;

    invoke-direct {v0}, Lcom/google/r/b/a/alh;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alh;->o:Lcom/google/r/b/a/alh;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 12708
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 12965
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    .line 13044
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    .line 13060
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    .line 13150
    iput-byte v3, p0, Lcom/google/r/b/a/alh;->p:B

    .line 13196
    iput v3, p0, Lcom/google/r/b/a/alh;->q:I

    .line 12709
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    .line 12710
    iget-object v0, p0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 12711
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/alh;->e:I

    .line 12712
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    .line 12713
    iput v4, p0, Lcom/google/r/b/a/alh;->h:I

    .line 12714
    iget-object v0, p0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 12715
    iget-object v0, p0, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 12716
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    .line 12717
    iput v4, p0, Lcom/google/r/b/a/alh;->m:I

    .line 12718
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    .line 12719
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, -0x1

    const/16 v9, 0x80

    const/16 v8, 0x8

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 12725
    invoke-direct {p0}, Lcom/google/r/b/a/alh;-><init>()V

    .line 12728
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 12731
    :cond_0
    :goto_0
    if-nez v3, :cond_1a

    .line 12732
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 12733
    sparse-switch v0, :sswitch_data_0

    .line 12738
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 12740
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 12736
    goto :goto_0

    .line 12745
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 12746
    invoke-static {v0}, Lcom/google/maps/g/px;->a(I)Lcom/google/maps/g/px;

    move-result-object v6

    .line 12747
    if-nez v6, :cond_5

    .line 12748
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 12896
    :catch_0
    move-exception v0

    .line 12897
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12902
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_1

    .line 12903
    iget-object v2, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    .line 12905
    :cond_1
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v8, :cond_2

    .line 12906
    iget-object v2, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    .line 12908
    :cond_2
    and-int/lit16 v2, v1, 0x80

    if-ne v2, v9, :cond_3

    .line 12909
    iget-object v2, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    .line 12911
    :cond_3
    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_4

    .line 12912
    iget-object v1, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    .line 12914
    :cond_4
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/alh;->au:Lcom/google/n/bn;

    throw v0

    .line 12750
    :cond_5
    and-int/lit8 v6, v1, 0x1

    if-eq v6, v4, :cond_6

    .line 12751
    :try_start_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    .line 12752
    or-int/lit8 v1, v1, 0x1

    .line 12754
    :cond_6
    iget-object v6, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 12898
    :catch_1
    move-exception v0

    .line 12899
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 12900
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 12759
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 12760
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 12761
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_7

    move v0, v2

    :goto_2
    if-lez v0, :cond_a

    .line 12762
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 12763
    invoke-static {v0}, Lcom/google/maps/g/px;->a(I)Lcom/google/maps/g/px;

    move-result-object v7

    .line 12764
    if-nez v7, :cond_8

    .line 12765
    const/4 v7, 0x1

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 12761
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_2

    .line 12767
    :cond_8
    and-int/lit8 v7, v1, 0x1

    if-eq v7, v4, :cond_9

    .line 12768
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    .line 12769
    or-int/lit8 v1, v1, 0x1

    .line 12771
    :cond_9
    iget-object v7, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 12774
    :cond_a
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 12778
    :sswitch_3
    iget-object v0, p0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 12779
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/alh;->a:I

    goto/16 :goto_0

    .line 12783
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 12784
    invoke-static {v0}, Lcom/google/maps/g/pv;->a(I)Lcom/google/maps/g/pv;

    move-result-object v6

    .line 12785
    if-nez v6, :cond_b

    .line 12786
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 12788
    :cond_b
    iget v6, p0, Lcom/google/r/b/a/alh;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/r/b/a/alh;->a:I

    .line 12789
    iput v0, p0, Lcom/google/r/b/a/alh;->e:I

    goto/16 :goto_0

    .line 12794
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 12795
    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v6

    .line 12796
    if-nez v6, :cond_c

    .line 12797
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 12799
    :cond_c
    and-int/lit8 v6, v1, 0x8

    if-eq v6, v8, :cond_d

    .line 12800
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    .line 12801
    or-int/lit8 v1, v1, 0x8

    .line 12803
    :cond_d
    iget-object v6, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 12808
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 12809
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 12810
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_e

    move v0, v2

    :goto_4
    if-lez v0, :cond_11

    .line 12811
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 12812
    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v7

    .line 12813
    if-nez v7, :cond_f

    .line 12814
    const/4 v7, 0x4

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_3

    .line 12810
    :cond_e
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_4

    .line 12816
    :cond_f
    and-int/lit8 v7, v1, 0x8

    if-eq v7, v8, :cond_10

    .line 12817
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    .line 12818
    or-int/lit8 v1, v1, 0x8

    .line 12820
    :cond_10
    iget-object v7, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 12823
    :cond_11
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 12827
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 12828
    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v6

    .line 12829
    if-nez v6, :cond_12

    .line 12830
    const/16 v6, 0x8

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 12832
    :cond_12
    iget v6, p0, Lcom/google/r/b/a/alh;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/r/b/a/alh;->a:I

    .line 12833
    iput v0, p0, Lcom/google/r/b/a/alh;->h:I

    goto/16 :goto_0

    .line 12838
    :sswitch_8
    iget-object v0, p0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 12839
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/alh;->a:I

    goto/16 :goto_0

    .line 12843
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 12844
    invoke-static {v0}, Lcom/google/maps/g/qj;->a(I)Lcom/google/maps/g/qj;

    move-result-object v6

    .line 12845
    if-nez v6, :cond_13

    .line 12846
    const/16 v6, 0xa

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 12848
    :cond_13
    and-int/lit16 v6, v1, 0x80

    if-eq v6, v9, :cond_14

    .line 12849
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    .line 12850
    or-int/lit16 v1, v1, 0x80

    .line 12852
    :cond_14
    iget-object v6, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 12857
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 12858
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 12859
    :goto_5
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_15

    move v0, v2

    :goto_6
    if-lez v0, :cond_18

    .line 12860
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 12861
    invoke-static {v0}, Lcom/google/maps/g/qj;->a(I)Lcom/google/maps/g/qj;

    move-result-object v7

    .line 12862
    if-nez v7, :cond_16

    .line 12863
    const/16 v7, 0xa

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 12859
    :cond_15
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_6

    .line 12865
    :cond_16
    and-int/lit16 v7, v1, 0x80

    if-eq v7, v9, :cond_17

    .line 12866
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    .line 12867
    or-int/lit16 v1, v1, 0x80

    .line 12869
    :cond_17
    iget-object v7, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 12872
    :cond_18
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 12876
    :sswitch_b
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/alh;->a:I

    .line 12877
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/alh;->m:I

    goto/16 :goto_0

    .line 12881
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 12882
    and-int/lit16 v6, v1, 0x200

    const/16 v7, 0x200

    if-eq v6, v7, :cond_19

    .line 12883
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    .line 12884
    or-int/lit16 v1, v1, 0x200

    .line 12886
    :cond_19
    iget-object v6, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 12890
    :sswitch_d
    iget-object v0, p0, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 12891
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/alh;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 12902
    :cond_1a
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v4, :cond_1b

    .line 12903
    iget-object v0, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    .line 12905
    :cond_1b
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v8, :cond_1c

    .line 12906
    iget-object v0, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    .line 12908
    :cond_1c
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v9, :cond_1d

    .line 12909
    iget-object v0, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    .line 12911
    :cond_1d
    and-int/lit16 v0, v1, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_1e

    .line 12912
    iget-object v0, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    .line 12914
    :cond_1e
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alh;->au:Lcom/google/n/bn;

    .line 12915
    return-void

    .line 12733
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x20 -> :sswitch_5
        0x22 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 12706
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 12965
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    .line 13044
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    .line 13060
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    .line 13150
    iput-byte v1, p0, Lcom/google/r/b/a/alh;->p:B

    .line 13196
    iput v1, p0, Lcom/google/r/b/a/alh;->q:I

    .line 12707
    return-void
.end method

.method public static d()Lcom/google/r/b/a/alh;
    .locals 1

    .prologue
    .line 14114
    sget-object v0, Lcom/google/r/b/a/alh;->o:Lcom/google/r/b/a/alh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/alm;
    .locals 1

    .prologue
    .line 13331
    new-instance v0, Lcom/google/r/b/a/alm;

    invoke-direct {v0}, Lcom/google/r/b/a/alm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/alh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12929
    sget-object v0, Lcom/google/r/b/a/alh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13162
    invoke-virtual {p0}, Lcom/google/r/b/a/alh;->c()I

    move v1, v2

    .line 13163
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 13164
    iget-object v0, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 13163
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 13166
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    .line 13167
    iget-object v0, p0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13169
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 13170
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/alh;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    :cond_2
    move v1, v2

    .line 13172
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 13173
    iget-object v0, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 13172
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 13175
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 13176
    iget v0, p0, Lcom/google/r/b/a/alh;->h:I

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->b(II)V

    .line 13178
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_5

    .line 13179
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_5
    move v1, v2

    .line 13181
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 13182
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 13181
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 13184
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 13185
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/r/b/a/alh;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 13187
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 13188
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v1, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13187
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 13190
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_9

    .line 13191
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13193
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/alh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 13194
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 13152
    iget-byte v1, p0, Lcom/google/r/b/a/alh;->p:B

    .line 13153
    if-ne v1, v0, :cond_0

    .line 13157
    :goto_0
    return v0

    .line 13154
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 13156
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/alh;->p:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x2

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 13198
    iget v0, p0, Lcom/google/r/b/a/alh;->q:I

    .line 13199
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13264
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 13204
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 13205
    iget-object v0, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    .line 13206
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    .line 13204
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v4

    .line 13206
    goto :goto_2

    .line 13208
    :cond_2
    add-int/lit8 v0, v3, 0x0

    .line 13209
    iget-object v1, p0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13211
    iget v1, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    .line 13212
    iget-object v1, p0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    .line 13213
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 13215
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v5, :cond_10

    .line 13216
    const/4 v1, 0x3

    iget v3, p0, Lcom/google/r/b/a/alh;->e:I

    .line 13217
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_3
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    move v1, v0

    :goto_4
    move v3, v2

    move v5, v2

    .line 13221
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 13222
    iget-object v0, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    .line 13223
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v5, v0

    .line 13221
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_4
    move v1, v4

    .line 13217
    goto :goto_3

    :cond_5
    move v0, v4

    .line 13223
    goto :goto_6

    .line 13225
    :cond_6
    add-int v0, v1, v5

    .line 13226
    iget-object v1, p0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 13228
    iget v0, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_f

    .line 13229
    iget v0, p0, Lcom/google/r/b/a/alh;->h:I

    .line 13230
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 13232
    :goto_8
    iget v1, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_e

    .line 13233
    const/16 v1, 0x9

    iget-object v3, p0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    .line 13234
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    :goto_9
    move v3, v2

    move v5, v2

    .line 13238
    :goto_a
    iget-object v0, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_9

    .line 13239
    iget-object v0, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    .line 13240
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_b
    add-int/2addr v5, v0

    .line 13238
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a

    :cond_7
    move v0, v4

    .line 13230
    goto :goto_7

    :cond_8
    move v0, v4

    .line 13240
    goto :goto_b

    .line 13242
    :cond_9
    add-int v0, v1, v5

    .line 13243
    iget-object v1, p0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13245
    iget v1, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_b

    .line 13246
    const/16 v1, 0xb

    iget v3, p0, Lcom/google/r/b/a/alh;->m:I

    .line 13247
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_a
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    :cond_b
    move v1, v2

    move v3, v2

    .line 13251
    :goto_c
    iget-object v4, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v1, v4, :cond_c

    .line 13252
    iget-object v4, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    .line 13253
    invoke-interface {v4, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 13251
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 13255
    :cond_c
    add-int/2addr v0, v3

    .line 13256
    iget-object v1, p0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13258
    iget v1, p0, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_d

    .line 13259
    const/16 v1, 0xd

    iget-object v3, p0, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    .line 13260
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 13262
    :cond_d
    iget-object v1, p0, Lcom/google/r/b/a/alh;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 13263
    iput v0, p0, Lcom/google/r/b/a/alh;->q:I

    goto/16 :goto_0

    :cond_e
    move v1, v0

    goto/16 :goto_9

    :cond_f
    move v0, v1

    goto/16 :goto_8

    :cond_10
    move v1, v0

    goto/16 :goto_4
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12700
    invoke-static {}, Lcom/google/r/b/a/alh;->newBuilder()Lcom/google/r/b/a/alm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/alm;->a(Lcom/google/r/b/a/alh;)Lcom/google/r/b/a/alm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12700
    invoke-static {}, Lcom/google/r/b/a/alh;->newBuilder()Lcom/google/r/b/a/alm;

    move-result-object v0

    return-object v0
.end method
