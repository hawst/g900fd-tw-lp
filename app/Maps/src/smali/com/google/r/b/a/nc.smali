.class public final Lcom/google/r/b/a/nc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/nc;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/nc;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5748
    new-instance v0, Lcom/google/r/b/a/nd;

    invoke-direct {v0}, Lcom/google/r/b/a/nd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/nc;->PARSER:Lcom/google/n/ax;

    .line 6040
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/nc;->i:Lcom/google/n/aw;

    .line 6528
    new-instance v0, Lcom/google/r/b/a/nc;

    invoke-direct {v0}, Lcom/google/r/b/a/nc;-><init>()V

    sput-object v0, Lcom/google/r/b/a/nc;->f:Lcom/google/r/b/a/nc;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5671
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5983
    iput-byte v0, p0, Lcom/google/r/b/a/nc;->g:B

    .line 6011
    iput v0, p0, Lcom/google/r/b/a/nc;->h:I

    .line 5672
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/nc;->b:I

    .line 5673
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/nc;->c:Ljava/lang/Object;

    .line 5674
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/nc;->d:Ljava/lang/Object;

    .line 5675
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    .line 5676
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/16 v7, 0x8

    const/4 v2, 0x1

    .line 5682
    invoke-direct {p0}, Lcom/google/r/b/a/nc;-><init>()V

    .line 5685
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 5688
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 5689
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 5690
    sparse-switch v4, :sswitch_data_0

    .line 5695
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 5697
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 5693
    goto :goto_0

    .line 5702
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 5703
    invoke-static {v4}, Lcom/google/r/b/a/nf;->a(I)Lcom/google/r/b/a/nf;

    move-result-object v5

    .line 5704
    if-nez v5, :cond_2

    .line 5705
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 5736
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 5737
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5742
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_1

    .line 5743
    iget-object v1, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    .line 5745
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/nc;->au:Lcom/google/n/bn;

    throw v0

    .line 5707
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/r/b/a/nc;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/nc;->a:I

    .line 5708
    iput v4, p0, Lcom/google/r/b/a/nc;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 5738
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 5739
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 5740
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5713
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 5714
    iget v5, p0, Lcom/google/r/b/a/nc;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/nc;->a:I

    .line 5715
    iput-object v4, p0, Lcom/google/r/b/a/nc;->c:Ljava/lang/Object;

    goto :goto_0

    .line 5742
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 5719
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 5720
    iget v5, p0, Lcom/google/r/b/a/nc;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/r/b/a/nc;->a:I

    .line 5721
    iput-object v4, p0, Lcom/google/r/b/a/nc;->d:Ljava/lang/Object;

    goto :goto_0

    .line 5725
    :sswitch_4
    and-int/lit8 v4, v0, 0x8

    if-eq v4, v7, :cond_3

    .line 5726
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    .line 5728
    or-int/lit8 v0, v0, 0x8

    .line 5730
    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 5731
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 5730
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 5742
    :cond_4
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_5

    .line 5743
    iget-object v0, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    .line 5745
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nc;->au:Lcom/google/n/bn;

    .line 5746
    return-void

    .line 5690
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5669
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5983
    iput-byte v0, p0, Lcom/google/r/b/a/nc;->g:B

    .line 6011
    iput v0, p0, Lcom/google/r/b/a/nc;->h:I

    .line 5670
    return-void
.end method

.method public static d()Lcom/google/r/b/a/nc;
    .locals 1

    .prologue
    .line 6531
    sget-object v0, Lcom/google/r/b/a/nc;->f:Lcom/google/r/b/a/nc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ne;
    .locals 1

    .prologue
    .line 6102
    new-instance v0, Lcom/google/r/b/a/ne;

    invoke-direct {v0}, Lcom/google/r/b/a/ne;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/nc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5760
    sget-object v0, Lcom/google/r/b/a/nc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5995
    invoke-virtual {p0}, Lcom/google/r/b/a/nc;->c()I

    .line 5996
    iget v0, p0, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5997
    iget v0, p0, Lcom/google/r/b/a/nc;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 5999
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 6000
    iget-object v0, p0, Lcom/google/r/b/a/nc;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nc;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6002
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 6003
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/nc;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nc;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6005
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 6006
    iget-object v0, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6005
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 6000
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 6003
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 6008
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/nc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6009
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5985
    iget-byte v1, p0, Lcom/google/r/b/a/nc;->g:B

    .line 5986
    if-ne v1, v0, :cond_0

    .line 5990
    :goto_0
    return v0

    .line 5987
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 5989
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/nc;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6013
    iget v0, p0, Lcom/google/r/b/a/nc;->h:I

    .line 6014
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6035
    :goto_0
    return v0

    .line 6017
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 6018
    iget v0, p0, Lcom/google/r/b/a/nc;->b:I

    .line 6019
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 6021
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 6023
    iget-object v0, p0, Lcom/google/r/b/a/nc;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nc;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 6025
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 6026
    const/4 v3, 0x3

    .line 6027
    iget-object v0, p0, Lcom/google/r/b/a/nc;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/nc;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_2
    move v3, v1

    move v1, v2

    .line 6029
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 6030
    iget-object v0, p0, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    .line 6031
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6029
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 6019
    :cond_3
    const/16 v0, 0xa

    goto/16 :goto_1

    .line 6023
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 6027
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 6033
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/nc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 6034
    iput v0, p0, Lcom/google/r/b/a/nc;->h:I

    goto/16 :goto_0

    :cond_7
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5663
    invoke-static {}, Lcom/google/r/b/a/nc;->newBuilder()Lcom/google/r/b/a/ne;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ne;->a(Lcom/google/r/b/a/nc;)Lcom/google/r/b/a/ne;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5663
    invoke-static {}, Lcom/google/r/b/a/nc;->newBuilder()Lcom/google/r/b/a/ne;

    move-result-object v0

    return-object v0
.end method
