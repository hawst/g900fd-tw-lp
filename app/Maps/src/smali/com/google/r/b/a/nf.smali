.class public final enum Lcom/google/r/b/a/nf;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/nf;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/nf;

.field public static final enum b:Lcom/google/r/b/a/nf;

.field public static final enum c:Lcom/google/r/b/a/nf;

.field public static final enum d:Lcom/google/r/b/a/nf;

.field private static final synthetic f:[Lcom/google/r/b/a/nf;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5771
    new-instance v0, Lcom/google/r/b/a/nf;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/nf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/nf;->a:Lcom/google/r/b/a/nf;

    .line 5775
    new-instance v0, Lcom/google/r/b/a/nf;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/nf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/nf;->b:Lcom/google/r/b/a/nf;

    .line 5779
    new-instance v0, Lcom/google/r/b/a/nf;

    const-string v1, "NOT_FOUND"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/nf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/nf;->c:Lcom/google/r/b/a/nf;

    .line 5783
    new-instance v0, Lcom/google/r/b/a/nf;

    const-string v1, "FAILURE"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v6, v2}, Lcom/google/r/b/a/nf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/nf;->d:Lcom/google/r/b/a/nf;

    .line 5766
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/nf;

    sget-object v1, Lcom/google/r/b/a/nf;->a:Lcom/google/r/b/a/nf;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/nf;->b:Lcom/google/r/b/a/nf;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/nf;->c:Lcom/google/r/b/a/nf;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/nf;->d:Lcom/google/r/b/a/nf;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/r/b/a/nf;->f:[Lcom/google/r/b/a/nf;

    .line 5823
    new-instance v0, Lcom/google/r/b/a/ng;

    invoke-direct {v0}, Lcom/google/r/b/a/ng;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 5832
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5833
    iput p3, p0, Lcom/google/r/b/a/nf;->e:I

    .line 5834
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/nf;
    .locals 1

    .prologue
    .line 5809
    sparse-switch p0, :sswitch_data_0

    .line 5814
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 5810
    :sswitch_0
    sget-object v0, Lcom/google/r/b/a/nf;->a:Lcom/google/r/b/a/nf;

    goto :goto_0

    .line 5811
    :sswitch_1
    sget-object v0, Lcom/google/r/b/a/nf;->b:Lcom/google/r/b/a/nf;

    goto :goto_0

    .line 5812
    :sswitch_2
    sget-object v0, Lcom/google/r/b/a/nf;->c:Lcom/google/r/b/a/nf;

    goto :goto_0

    .line 5813
    :sswitch_3
    sget-object v0, Lcom/google/r/b/a/nf;->d:Lcom/google/r/b/a/nf;

    goto :goto_0

    .line 5809
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x64 -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/nf;
    .locals 1

    .prologue
    .line 5766
    const-class v0, Lcom/google/r/b/a/nf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/nf;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/nf;
    .locals 1

    .prologue
    .line 5766
    sget-object v0, Lcom/google/r/b/a/nf;->f:[Lcom/google/r/b/a/nf;

    invoke-virtual {v0}, [Lcom/google/r/b/a/nf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/nf;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 5805
    iget v0, p0, Lcom/google/r/b/a/nf;->e:I

    return v0
.end method
