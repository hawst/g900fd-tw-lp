.class public final Lcom/google/r/b/a/l;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/j;",
        "Lcom/google/r/b/a/l;",
        ">;",
        "Lcom/google/r/b/a/m;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1217
    sget-object v0, Lcom/google/r/b/a/j;->e:Lcom/google/r/b/a/j;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1311
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/l;->c:Ljava/lang/Object;

    .line 1387
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/l;->d:Ljava/lang/Object;

    .line 1218
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1209
    new-instance v2, Lcom/google/r/b/a/j;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/j;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/l;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/l;->b:I

    iput v1, v2, Lcom/google/r/b/a/j;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/l;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/l;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/j;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/j;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1209
    check-cast p1, Lcom/google/r/b/a/j;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/l;->a(Lcom/google/r/b/a/j;)Lcom/google/r/b/a/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/j;)Lcom/google/r/b/a/l;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1255
    invoke-static {}, Lcom/google/r/b/a/j;->g()Lcom/google/r/b/a/j;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1270
    :goto_0
    return-object p0

    .line 1256
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1257
    iget v2, p1, Lcom/google/r/b/a/j;->b:I

    iget v3, p0, Lcom/google/r/b/a/l;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/l;->a:I

    iput v2, p0, Lcom/google/r/b/a/l;->b:I

    .line 1259
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1260
    iget v2, p0, Lcom/google/r/b/a/l;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/l;->a:I

    .line 1261
    iget-object v2, p1, Lcom/google/r/b/a/j;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/l;->c:Ljava/lang/Object;

    .line 1264
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/j;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 1265
    iget v0, p0, Lcom/google/r/b/a/l;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/l;->a:I

    .line 1266
    iget-object v0, p1, Lcom/google/r/b/a/j;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/l;->d:Ljava/lang/Object;

    .line 1269
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/j;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 1256
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1259
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1264
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1274
    const/4 v0, 0x1

    return v0
.end method
