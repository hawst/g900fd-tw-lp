.class public final Lcom/google/r/b/a/pz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/qa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/px;",
        "Lcom/google/r/b/a/pz;",
        ">;",
        "Lcom/google/r/b/a/qa;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/f;

.field public c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1413
    sget-object v0, Lcom/google/r/b/a/px;->e:Lcom/google/r/b/a/px;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1483
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/pz;->b:Lcom/google/n/f;

    .line 1518
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/pz;->d:Lcom/google/n/ao;

    .line 1577
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/pz;->c:Lcom/google/n/ao;

    .line 1414
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1405
    new-instance v2, Lcom/google/r/b/a/px;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/px;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/pz;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, p0, Lcom/google/r/b/a/pz;->b:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/r/b/a/px;->b:Lcom/google/n/f;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/px;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/pz;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/pz;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, v2, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/pz;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/pz;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/px;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1405
    check-cast p1, Lcom/google/r/b/a/px;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/pz;->a(Lcom/google/r/b/a/px;)Lcom/google/r/b/a/pz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/px;)Lcom/google/r/b/a/pz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1455
    invoke-static {}, Lcom/google/r/b/a/px;->d()Lcom/google/r/b/a/px;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1468
    :goto_0
    return-object p0

    .line 1456
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 1457
    iget-object v2, p1, Lcom/google/r/b/a/px;->b:Lcom/google/n/f;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v2, v1

    .line 1456
    goto :goto_1

    .line 1457
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/pz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/pz;->a:I

    iput-object v2, p0, Lcom/google/r/b/a/pz;->b:Lcom/google/n/f;

    .line 1459
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_4

    .line 1460
    iget-object v2, p0, Lcom/google/r/b/a/pz;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/px;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1461
    iget v2, p0, Lcom/google/r/b/a/pz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/pz;->a:I

    .line 1463
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/px;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_5

    .line 1464
    iget-object v0, p0, Lcom/google/r/b/a/pz;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/px;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1465
    iget v0, p0, Lcom/google/r/b/a/pz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/pz;->a:I

    .line 1467
    :cond_5
    iget-object v0, p1, Lcom/google/r/b/a/px;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 1459
    goto :goto_2

    :cond_7
    move v0, v1

    .line 1463
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1472
    iget v0, p0, Lcom/google/r/b/a/pz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1473
    iget-object v0, p0, Lcom/google/r/b/a/pz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/qb;->d()Lcom/google/r/b/a/qb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/qb;

    invoke-virtual {v0}, Lcom/google/r/b/a/qb;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1478
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1472
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1478
    goto :goto_1
.end method
