.class public final Lcom/google/r/b/a/tt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/tu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/tr;",
        "Lcom/google/r/b/a/tt;",
        ">;",
        "Lcom/google/r/b/a/tu;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 334
    sget-object v0, Lcom/google/r/b/a/tr;->e:Lcom/google/r/b/a/tr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 400
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/tt;->b:Lcom/google/n/ao;

    .line 459
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/tt;->c:Lcom/google/n/ao;

    .line 518
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/tt;->d:Ljava/lang/Object;

    .line 335
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    new-instance v2, Lcom/google/r/b/a/tr;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/tr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/tt;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/tr;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/tt;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/tt;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/r/b/a/tr;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/tt;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/tt;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/tt;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/tr;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/tr;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 326
    check-cast p1, Lcom/google/r/b/a/tr;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/tt;->a(Lcom/google/r/b/a/tr;)Lcom/google/r/b/a/tt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/tr;)Lcom/google/r/b/a/tt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 376
    invoke-static {}, Lcom/google/r/b/a/tr;->d()Lcom/google/r/b/a/tr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 391
    :goto_0
    return-object p0

    .line 377
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/tr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 378
    iget-object v2, p0, Lcom/google/r/b/a/tt;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/tr;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 379
    iget v2, p0, Lcom/google/r/b/a/tt;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/tt;->a:I

    .line 381
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/tr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 382
    iget-object v2, p0, Lcom/google/r/b/a/tt;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/tr;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 383
    iget v2, p0, Lcom/google/r/b/a/tt;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/tt;->a:I

    .line 385
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/tr;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 386
    iget v0, p0, Lcom/google/r/b/a/tt;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/tt;->a:I

    .line 387
    iget-object v0, p1, Lcom/google/r/b/a/tr;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/tt;->d:Ljava/lang/Object;

    .line 390
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/tr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 377
    goto :goto_1

    :cond_5
    move v2, v1

    .line 381
    goto :goto_2

    :cond_6
    move v0, v1

    .line 385
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x1

    return v0
.end method
