.class public final Lcom/google/r/b/a/abc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/abd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aba;",
        "Lcom/google/r/b/a/abc;",
        ">;",
        "Lcom/google/r/b/a/abd;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field public g:Ljava/lang/Object;

.field private h:Lcom/google/n/ao;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/n/ao;

.field private k:Z

.field private l:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4195
    sget-object v0, Lcom/google/r/b/a/aba;->m:Lcom/google/r/b/a/aba;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4360
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/abc;->h:Lcom/google/n/ao;

    .line 4419
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/abc;->b:Ljava/lang/Object;

    .line 4495
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/abc;->c:Ljava/lang/Object;

    .line 4571
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/abc;->d:Ljava/lang/Object;

    .line 4647
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/abc;->e:Ljava/lang/Object;

    .line 4723
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/abc;->f:Ljava/lang/Object;

    .line 4800
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    .line 4936
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/abc;->j:Lcom/google/n/ao;

    .line 5027
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/abc;->g:Ljava/lang/Object;

    .line 5103
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/abc;->l:Lcom/google/n/ao;

    .line 4196
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4187
    new-instance v2, Lcom/google/r/b/a/aba;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aba;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/abc;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    :goto_0
    iget-object v4, v2, Lcom/google/r/b/a/aba;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/abc;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/abc;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/abc;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aba;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/abc;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aba;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/abc;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aba;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/abc;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aba;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, p0, Lcom/google/r/b/a/abc;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aba;->g:Ljava/lang/Object;

    iget v4, p0, Lcom/google/r/b/a/abc;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/abc;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/r/b/a/abc;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v4, v2, Lcom/google/r/b/a/aba;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/abc;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/abc;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-boolean v4, p0, Lcom/google/r/b/a/abc;->k:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/aba;->j:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget-object v4, p0, Lcom/google/r/b/a/abc;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aba;->k:Ljava/lang/Object;

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v3, v2, Lcom/google/r/b/a/aba;->l:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/r/b/a/abc;->l:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/r/b/a/abc;->l:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/r/b/a/aba;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4187
    check-cast p1, Lcom/google/r/b/a/aba;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/abc;->a(Lcom/google/r/b/a/aba;)Lcom/google/r/b/a/abc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aba;)Lcom/google/r/b/a/abc;
    .locals 5

    .prologue
    const/16 v4, 0x40

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4288
    invoke-static {}, Lcom/google/r/b/a/aba;->d()Lcom/google/r/b/a/aba;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4345
    :goto_0
    return-object p0

    .line 4289
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4290
    iget-object v2, p0, Lcom/google/r/b/a/abc;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aba;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4291
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4293
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 4294
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4295
    iget-object v2, p1, Lcom/google/r/b/a/aba;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/abc;->b:Ljava/lang/Object;

    .line 4298
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 4299
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4300
    iget-object v2, p1, Lcom/google/r/b/a/aba;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/abc;->c:Ljava/lang/Object;

    .line 4303
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 4304
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4305
    iget-object v2, p1, Lcom/google/r/b/a/aba;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/abc;->d:Ljava/lang/Object;

    .line 4308
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 4309
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4310
    iget-object v2, p1, Lcom/google/r/b/a/aba;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/abc;->e:Ljava/lang/Object;

    .line 4313
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 4314
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4315
    iget-object v2, p1, Lcom/google/r/b/a/aba;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/abc;->f:Ljava/lang/Object;

    .line 4318
    :cond_6
    iget-object v2, p1, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 4319
    iget-object v2, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 4320
    iget-object v2, p1, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    .line 4321
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4328
    :cond_7
    :goto_7
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit8 v2, v2, 0x40

    if-ne v2, v4, :cond_14

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 4329
    iget-object v2, p0, Lcom/google/r/b/a/abc;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aba;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4330
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4332
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 4333
    iget-boolean v2, p1, Lcom/google/r/b/a/aba;->j:Z

    iget v3, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/abc;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/abc;->k:Z

    .line 4335
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 4336
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4337
    iget-object v2, p1, Lcom/google/r/b/a/aba;->k:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/abc;->g:Ljava/lang/Object;

    .line 4340
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/aba;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_17

    :goto_b
    if-eqz v0, :cond_b

    .line 4341
    iget-object v0, p0, Lcom/google/r/b/a/abc;->l:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/r/b/a/aba;->l:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4342
    iget v0, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4344
    :cond_b
    iget-object v0, p1, Lcom/google/r/b/a/aba;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 4289
    goto/16 :goto_1

    :cond_d
    move v2, v1

    .line 4293
    goto/16 :goto_2

    :cond_e
    move v2, v1

    .line 4298
    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 4303
    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 4308
    goto/16 :goto_5

    :cond_11
    move v2, v1

    .line 4313
    goto/16 :goto_6

    .line 4323
    :cond_12
    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    and-int/lit8 v2, v2, 0x40

    if-eq v2, v4, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/abc;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/r/b/a/abc;->a:I

    .line 4324
    :cond_13
    iget-object v2, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/aba;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    :cond_14
    move v2, v1

    .line 4328
    goto/16 :goto_8

    :cond_15
    move v2, v1

    .line 4332
    goto :goto_9

    :cond_16
    move v2, v1

    .line 4335
    goto :goto_a

    :cond_17
    move v0, v1

    .line 4340
    goto :goto_b
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 4349
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 4350
    iget-object v0, p0, Lcom/google/r/b/a/abc;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/zy;->d()Lcom/google/r/b/a/zy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/zy;

    invoke-virtual {v0}, Lcom/google/r/b/a/zy;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4355
    :goto_1
    return v2

    .line 4349
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4355
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
