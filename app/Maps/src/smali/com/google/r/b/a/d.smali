.class public final Lcom/google/r/b/a/d;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/i;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/d;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/d;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4166
    new-instance v0, Lcom/google/r/b/a/e;

    invoke-direct {v0}, Lcom/google/r/b/a/e;-><init>()V

    sput-object v0, Lcom/google/r/b/a/d;->PARSER:Lcom/google/n/ax;

    .line 4359
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/d;->g:Lcom/google/n/aw;

    .line 4606
    new-instance v0, Lcom/google/r/b/a/d;

    invoke-direct {v0}, Lcom/google/r/b/a/d;-><init>()V

    sput-object v0, Lcom/google/r/b/a/d;->d:Lcom/google/r/b/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4110
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4316
    iput-byte v0, p0, Lcom/google/r/b/a/d;->e:B

    .line 4338
    iput v0, p0, Lcom/google/r/b/a/d;->f:I

    .line 4111
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/d;->b:I

    .line 4112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    .line 4113
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 4119
    invoke-direct {p0}, Lcom/google/r/b/a/d;-><init>()V

    .line 4120
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 4124
    const/4 v0, 0x0

    .line 4125
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 4126
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 4127
    sparse-switch v3, :sswitch_data_0

    .line 4132
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 4134
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4130
    goto :goto_0

    .line 4139
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 4140
    invoke-static {v3}, Lcom/google/r/b/a/g;->a(I)Lcom/google/r/b/a/g;

    move-result-object v4

    .line 4141
    if-nez v4, :cond_1

    .line 4142
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4157
    :catch_0
    move-exception v0

    .line 4158
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4163
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/d;->au:Lcom/google/n/bn;

    throw v0

    .line 4144
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/d;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/d;->a:I

    .line 4145
    iput v3, p0, Lcom/google/r/b/a/d;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4159
    :catch_1
    move-exception v0

    .line 4160
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4161
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4150
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 4151
    iget v4, p0, Lcom/google/r/b/a/d;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/d;->a:I

    .line 4152
    iput-object v3, p0, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 4163
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/d;->au:Lcom/google/n/bn;

    .line 4164
    return-void

    .line 4127
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4108
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4316
    iput-byte v0, p0, Lcom/google/r/b/a/d;->e:B

    .line 4338
    iput v0, p0, Lcom/google/r/b/a/d;->f:I

    .line 4109
    return-void
.end method

.method public static d()Lcom/google/r/b/a/d;
    .locals 1

    .prologue
    .line 4609
    sget-object v0, Lcom/google/r/b/a/d;->d:Lcom/google/r/b/a/d;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/f;
    .locals 1

    .prologue
    .line 4421
    new-instance v0, Lcom/google/r/b/a/f;

    invoke-direct {v0}, Lcom/google/r/b/a/f;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4178
    sget-object v0, Lcom/google/r/b/a/d;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4328
    invoke-virtual {p0}, Lcom/google/r/b/a/d;->c()I

    .line 4329
    iget v0, p0, Lcom/google/r/b/a/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4330
    iget v0, p0, Lcom/google/r/b/a/d;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 4332
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/d;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4333
    iget-object v0, p0, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4335
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/d;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4336
    return-void

    .line 4333
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4318
    iget-byte v1, p0, Lcom/google/r/b/a/d;->e:B

    .line 4319
    if-ne v1, v0, :cond_0

    .line 4323
    :goto_0
    return v0

    .line 4320
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 4322
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/d;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4340
    iget v0, p0, Lcom/google/r/b/a/d;->f:I

    .line 4341
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4354
    :goto_0
    return v0

    .line 4344
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/d;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 4345
    iget v0, p0, Lcom/google/r/b/a/d;->b:I

    .line 4346
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 4348
    :goto_2
    iget v0, p0, Lcom/google/r/b/a/d;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 4350
    iget-object v0, p0, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/d;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 4352
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/d;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 4353
    iput v0, p0, Lcom/google/r/b/a/d;->f:I

    goto :goto_0

    .line 4346
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 4350
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4102
    invoke-static {}, Lcom/google/r/b/a/d;->newBuilder()Lcom/google/r/b/a/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/f;->a(Lcom/google/r/b/a/d;)Lcom/google/r/b/a/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4102
    invoke-static {}, Lcom/google/r/b/a/d;->newBuilder()Lcom/google/r/b/a/f;

    move-result-object v0

    return-object v0
.end method
