.class public final Lcom/google/r/b/a/zf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/zd;",
        "Lcom/google/r/b/a/zf;",
        ">;",
        "Lcom/google/r/b/a/zm;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/Object;

.field public e:Lcom/google/n/ao;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljava/lang/Object;

.field public u:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1758
    sget-object v0, Lcom/google/r/b/a/zd;->v:Lcom/google/r/b/a/zd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2024
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/zf;->c:I

    .line 2060
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zf;->d:Ljava/lang/Object;

    .line 2136
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/zf;->e:Lcom/google/n/ao;

    .line 2196
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    .line 2716
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    .line 2782
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zf;->t:Ljava/lang/Object;

    .line 2858
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/zf;->u:Lcom/google/n/f;

    .line 1759
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    .line 1750
    new-instance v2, Lcom/google/r/b/a/zd;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/zd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_13

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/zf;->b:I

    iput v4, v2, Lcom/google/r/b/a/zd;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/r/b/a/zf;->c:I

    iput v4, v2, Lcom/google/r/b/a/zd;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/r/b/a/zf;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/zd;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/r/b/a/zd;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/zf;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/zf;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/r/b/a/zf;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/zf;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/r/b/a/zf;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-boolean v1, p0, Lcom/google/r/b/a/zf;->g:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/zd;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget v1, p0, Lcom/google/r/b/a/zf;->h:I

    iput v1, v2, Lcom/google/r/b/a/zd;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget v1, p0, Lcom/google/r/b/a/zf;->i:I

    iput v1, v2, Lcom/google/r/b/a/zd;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget v1, p0, Lcom/google/r/b/a/zf;->j:I

    iput v1, v2, Lcom/google/r/b/a/zd;->j:I

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget v1, p0, Lcom/google/r/b/a/zf;->k:I

    iput v1, v2, Lcom/google/r/b/a/zd;->k:I

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget v1, p0, Lcom/google/r/b/a/zf;->l:I

    iput v1, v2, Lcom/google/r/b/a/zd;->l:I

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget v1, p0, Lcom/google/r/b/a/zf;->m:I

    iput v1, v2, Lcom/google/r/b/a/zd;->m:I

    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    or-int/lit16 v0, v0, 0x800

    :cond_b
    iget v1, p0, Lcom/google/r/b/a/zf;->n:I

    iput v1, v2, Lcom/google/r/b/a/zd;->n:I

    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    or-int/lit16 v0, v0, 0x1000

    :cond_c
    iget v1, p0, Lcom/google/r/b/a/zf;->o:I

    iput v1, v2, Lcom/google/r/b/a/zd;->o:I

    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    or-int/lit16 v0, v0, 0x2000

    :cond_d
    iget v1, p0, Lcom/google/r/b/a/zf;->p:I

    iput v1, v2, Lcom/google/r/b/a/zd;->p:I

    and-int v1, v3, v7

    if-ne v1, v7, :cond_e

    or-int/lit16 v0, v0, 0x4000

    :cond_e
    iget v1, p0, Lcom/google/r/b/a/zf;->q:I

    iput v1, v2, Lcom/google/r/b/a/zd;->q:I

    and-int v1, v3, v8

    if-ne v1, v8, :cond_f

    or-int/2addr v0, v7

    :cond_f
    iget v1, p0, Lcom/google/r/b/a/zf;->r:I

    iput v1, v2, Lcom/google/r/b/a/zd;->r:I

    iget v1, p0, Lcom/google/r/b/a/zf;->a:I

    and-int/2addr v1, v9

    if-ne v1, v9, :cond_10

    iget-object v1, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/zf;->a:I

    const v4, -0x20001

    and-int/2addr v1, v4

    iput v1, p0, Lcom/google/r/b/a/zf;->a:I

    :cond_10
    iget-object v1, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    const/high16 v1, 0x40000

    and-int/2addr v1, v3

    const/high16 v4, 0x40000

    if-ne v1, v4, :cond_11

    or-int/2addr v0, v8

    :cond_11
    iget-object v1, p0, Lcom/google/r/b/a/zf;->t:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/zd;->t:Ljava/lang/Object;

    const/high16 v1, 0x80000

    and-int/2addr v1, v3

    const/high16 v3, 0x80000

    if-ne v1, v3, :cond_12

    or-int/2addr v0, v9

    :cond_12
    iget-object v1, p0, Lcom/google/r/b/a/zf;->u:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/r/b/a/zd;->u:Lcom/google/n/f;

    iput v0, v2, Lcom/google/r/b/a/zd;->a:I

    return-object v2

    :cond_13
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1750
    check-cast p1, Lcom/google/r/b/a/zd;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/zf;->a(Lcom/google/r/b/a/zd;)Lcom/google/r/b/a/zf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/zd;)Lcom/google/r/b/a/zf;
    .locals 7

    .prologue
    const/high16 v6, 0x20000

    const/high16 v5, 0x10000

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1902
    invoke-static {}, Lcom/google/r/b/a/zd;->d()Lcom/google/r/b/a/zd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1983
    :goto_0
    return-object p0

    .line 1903
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1904
    iget v2, p1, Lcom/google/r/b/a/zd;->b:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->b:I

    .line 1906
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 1907
    iget v2, p1, Lcom/google/r/b/a/zd;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/zg;->a(I)Lcom/google/r/b/a/zg;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/r/b/a/zg;->a:Lcom/google/r/b/a/zg;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 1903
    goto :goto_1

    :cond_4
    move v2, v1

    .line 1906
    goto :goto_2

    .line 1907
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iget v2, v2, Lcom/google/r/b/a/zg;->d:I

    iput v2, p0, Lcom/google/r/b/a/zf;->c:I

    .line 1909
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 1910
    iget v2, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/zf;->a:I

    .line 1911
    iget-object v2, p1, Lcom/google/r/b/a/zd;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zf;->d:Ljava/lang/Object;

    .line 1914
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 1915
    iget-object v2, p0, Lcom/google/r/b/a/zf;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/zd;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1916
    iget v2, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/zf;->a:I

    .line 1918
    :cond_8
    iget-object v2, p1, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1919
    iget-object v2, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1920
    iget-object v2, p1, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    .line 1921
    iget v2, p0, Lcom/google/r/b/a/zf;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/r/b/a/zf;->a:I

    .line 1928
    :cond_9
    :goto_5
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_6
    if-eqz v2, :cond_a

    .line 1929
    iget-boolean v2, p1, Lcom/google/r/b/a/zd;->g:Z

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/zf;->g:Z

    .line 1931
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_7
    if-eqz v2, :cond_b

    .line 1932
    iget v2, p1, Lcom/google/r/b/a/zd;->h:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->h:I

    .line 1934
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_8
    if-eqz v2, :cond_c

    .line 1935
    iget v2, p1, Lcom/google/r/b/a/zd;->i:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->i:I

    .line 1937
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_9
    if-eqz v2, :cond_d

    .line 1938
    iget v2, p1, Lcom/google/r/b/a/zd;->j:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->j:I

    .line 1940
    :cond_d
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_a
    if-eqz v2, :cond_e

    .line 1941
    iget v2, p1, Lcom/google/r/b/a/zd;->k:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->k:I

    .line 1943
    :cond_e
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_b
    if-eqz v2, :cond_f

    .line 1944
    iget v2, p1, Lcom/google/r/b/a/zd;->l:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->l:I

    .line 1946
    :cond_f
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_c
    if-eqz v2, :cond_10

    .line 1947
    iget v2, p1, Lcom/google/r/b/a/zd;->m:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->m:I

    .line 1949
    :cond_10
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_d
    if-eqz v2, :cond_11

    .line 1950
    iget v2, p1, Lcom/google/r/b/a/zd;->n:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->n:I

    .line 1952
    :cond_11
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_23

    move v2, v0

    :goto_e
    if-eqz v2, :cond_12

    .line 1953
    iget v2, p1, Lcom/google/r/b/a/zd;->o:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->o:I

    .line 1955
    :cond_12
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_24

    move v2, v0

    :goto_f
    if-eqz v2, :cond_13

    .line 1956
    iget v2, p1, Lcom/google/r/b/a/zd;->p:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->p:I

    .line 1958
    :cond_13
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_25

    move v2, v0

    :goto_10
    if-eqz v2, :cond_14

    .line 1959
    iget v2, p1, Lcom/google/r/b/a/zd;->q:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->q:I

    .line 1961
    :cond_14
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_26

    move v2, v0

    :goto_11
    if-eqz v2, :cond_15

    .line 1962
    iget v2, p1, Lcom/google/r/b/a/zd;->r:I

    iget v3, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/r/b/a/zf;->a:I

    iput v2, p0, Lcom/google/r/b/a/zf;->r:I

    .line 1964
    :cond_15
    iget-object v2, p1, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_16

    .line 1965
    iget-object v2, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1966
    iget-object v2, p1, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    .line 1967
    iget v2, p0, Lcom/google/r/b/a/zf;->a:I

    const v3, -0x20001

    and-int/2addr v2, v3

    iput v2, p0, Lcom/google/r/b/a/zf;->a:I

    .line 1974
    :cond_16
    :goto_12
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_28

    move v2, v0

    :goto_13
    if-eqz v2, :cond_17

    .line 1975
    iget v2, p0, Lcom/google/r/b/a/zf;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/r/b/a/zf;->a:I

    .line 1976
    iget-object v2, p1, Lcom/google/r/b/a/zd;->t:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/zf;->t:Ljava/lang/Object;

    .line 1979
    :cond_17
    iget v2, p1, Lcom/google/r/b/a/zd;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_29

    :goto_14
    if-eqz v0, :cond_2b

    .line 1980
    iget-object v0, p1, Lcom/google/r/b/a/zd;->u:Lcom/google/n/f;

    if-nez v0, :cond_2a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_18
    move v2, v1

    .line 1909
    goto/16 :goto_3

    :cond_19
    move v2, v1

    .line 1914
    goto/16 :goto_4

    .line 1923
    :cond_1a
    invoke-virtual {p0}, Lcom/google/r/b/a/zf;->c()V

    .line 1924
    iget-object v2, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    :cond_1b
    move v2, v1

    .line 1928
    goto/16 :goto_6

    :cond_1c
    move v2, v1

    .line 1931
    goto/16 :goto_7

    :cond_1d
    move v2, v1

    .line 1934
    goto/16 :goto_8

    :cond_1e
    move v2, v1

    .line 1937
    goto/16 :goto_9

    :cond_1f
    move v2, v1

    .line 1940
    goto/16 :goto_a

    :cond_20
    move v2, v1

    .line 1943
    goto/16 :goto_b

    :cond_21
    move v2, v1

    .line 1946
    goto/16 :goto_c

    :cond_22
    move v2, v1

    .line 1949
    goto/16 :goto_d

    :cond_23
    move v2, v1

    .line 1952
    goto/16 :goto_e

    :cond_24
    move v2, v1

    .line 1955
    goto/16 :goto_f

    :cond_25
    move v2, v1

    .line 1958
    goto/16 :goto_10

    :cond_26
    move v2, v1

    .line 1961
    goto :goto_11

    .line 1969
    :cond_27
    invoke-virtual {p0}, Lcom/google/r/b/a/zf;->d()V

    .line 1970
    iget-object v2, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_12

    :cond_28
    move v2, v1

    .line 1974
    goto :goto_13

    :cond_29
    move v0, v1

    .line 1979
    goto :goto_14

    .line 1980
    :cond_2a
    iget v1, p0, Lcom/google/r/b/a/zf;->a:I

    const/high16 v2, 0x80000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/r/b/a/zf;->a:I

    iput-object v0, p0, Lcom/google/r/b/a/zf;->u:Lcom/google/n/f;

    .line 1982
    :cond_2b
    iget-object v0, p1, Lcom/google/r/b/a/zd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1987
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2198
    iget v0, p0, Lcom/google/r/b/a/zf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 2199
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/zf;->f:Ljava/util/List;

    .line 2202
    iget v0, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/zf;->a:I

    .line 2204
    :cond_0
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    const/high16 v2, 0x20000

    .line 2718
    iget v0, p0, Lcom/google/r/b/a/zf;->a:I

    and-int/2addr v0, v2

    if-eq v0, v2, :cond_0

    .line 2719
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/zf;->s:Ljava/util/List;

    .line 2720
    iget v0, p0, Lcom/google/r/b/a/zf;->a:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/r/b/a/zf;->a:I

    .line 2722
    :cond_0
    return-void
.end method
