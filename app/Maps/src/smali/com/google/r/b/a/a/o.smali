.class public final Lcom/google/r/b/a/a/o;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/a/r;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/o;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/a/o;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 644
    new-instance v0, Lcom/google/r/b/a/a/p;

    invoke-direct {v0}, Lcom/google/r/b/a/a/p;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/o;->PARSER:Lcom/google/n/ax;

    .line 735
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/a/o;->g:Lcom/google/n/aw;

    .line 992
    new-instance v0, Lcom/google/r/b/a/a/o;

    invoke-direct {v0}, Lcom/google/r/b/a/a/o;-><init>()V

    sput-object v0, Lcom/google/r/b/a/a/o;->d:Lcom/google/r/b/a/a/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 595
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 661
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/o;->b:Lcom/google/n/ao;

    .line 677
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/o;->c:Lcom/google/n/ao;

    .line 692
    iput-byte v2, p0, Lcom/google/r/b/a/a/o;->e:B

    .line 714
    iput v2, p0, Lcom/google/r/b/a/a/o;->f:I

    .line 596
    iget-object v0, p0, Lcom/google/r/b/a/a/o;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 597
    iget-object v0, p0, Lcom/google/r/b/a/a/o;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 598
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 604
    invoke-direct {p0}, Lcom/google/r/b/a/a/o;-><init>()V

    .line 605
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 610
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 611
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 612
    sparse-switch v3, :sswitch_data_0

    .line 617
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 619
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 615
    goto :goto_0

    .line 624
    :sswitch_1
    iget-object v3, p0, Lcom/google/r/b/a/a/o;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 625
    iget v3, p0, Lcom/google/r/b/a/a/o;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/a/o;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 635
    :catch_0
    move-exception v0

    .line 636
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 641
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/a/o;->au:Lcom/google/n/bn;

    throw v0

    .line 629
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/r/b/a/a/o;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 630
    iget v3, p0, Lcom/google/r/b/a/a/o;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/a/o;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 637
    :catch_1
    move-exception v0

    .line 638
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 639
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 641
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/a/o;->au:Lcom/google/n/bn;

    .line 642
    return-void

    .line 612
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 593
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 661
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/o;->b:Lcom/google/n/ao;

    .line 677
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/a/o;->c:Lcom/google/n/ao;

    .line 692
    iput-byte v1, p0, Lcom/google/r/b/a/a/o;->e:B

    .line 714
    iput v1, p0, Lcom/google/r/b/a/a/o;->f:I

    .line 594
    return-void
.end method

.method public static d()Lcom/google/r/b/a/a/o;
    .locals 1

    .prologue
    .line 995
    sget-object v0, Lcom/google/r/b/a/a/o;->d:Lcom/google/r/b/a/a/o;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/a/q;
    .locals 1

    .prologue
    .line 797
    new-instance v0, Lcom/google/r/b/a/a/q;

    invoke-direct {v0}, Lcom/google/r/b/a/a/q;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/a/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 656
    sget-object v0, Lcom/google/r/b/a/a/o;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 704
    invoke-virtual {p0}, Lcom/google/r/b/a/a/o;->c()I

    .line 705
    iget v0, p0, Lcom/google/r/b/a/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 706
    iget-object v0, p0, Lcom/google/r/b/a/a/o;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 708
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/o;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 709
    iget-object v0, p0, Lcom/google/r/b/a/a/o;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 711
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/a/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 712
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 694
    iget-byte v1, p0, Lcom/google/r/b/a/a/o;->e:B

    .line 695
    if-ne v1, v0, :cond_0

    .line 699
    :goto_0
    return v0

    .line 696
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 698
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/a/o;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 716
    iget v0, p0, Lcom/google/r/b/a/a/o;->f:I

    .line 717
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 730
    :goto_0
    return v0

    .line 720
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 721
    iget-object v0, p0, Lcom/google/r/b/a/a/o;->b:Lcom/google/n/ao;

    .line 722
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 724
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/a/o;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 725
    iget-object v2, p0, Lcom/google/r/b/a/a/o;->c:Lcom/google/n/ao;

    .line 726
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 728
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/a/o;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 729
    iput v0, p0, Lcom/google/r/b/a/a/o;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 587
    invoke-static {}, Lcom/google/r/b/a/a/o;->newBuilder()Lcom/google/r/b/a/a/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/a/q;->a(Lcom/google/r/b/a/a/o;)Lcom/google/r/b/a/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 587
    invoke-static {}, Lcom/google/r/b/a/a/o;->newBuilder()Lcom/google/r/b/a/a/q;

    move-result-object v0

    return-object v0
.end method
