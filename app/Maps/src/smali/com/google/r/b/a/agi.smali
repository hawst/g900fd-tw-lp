.class public final enum Lcom/google/r/b/a/agi;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/agi;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/agi;

.field public static final enum b:Lcom/google/r/b/a/agi;

.field public static final enum c:Lcom/google/r/b/a/agi;

.field public static final enum d:Lcom/google/r/b/a/agi;

.field private static final synthetic f:[Lcom/google/r/b/a/agi;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 276
    new-instance v0, Lcom/google/r/b/a/agi;

    const-string v1, "TRIP"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/r/b/a/agi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/agi;->a:Lcom/google/r/b/a/agi;

    .line 280
    new-instance v0, Lcom/google/r/b/a/agi;

    const-string v1, "PATH"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/agi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/agi;->b:Lcom/google/r/b/a/agi;

    .line 284
    new-instance v0, Lcom/google/r/b/a/agi;

    const-string v1, "STEP_GROUP"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/r/b/a/agi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/agi;->c:Lcom/google/r/b/a/agi;

    .line 288
    new-instance v0, Lcom/google/r/b/a/agi;

    const-string v1, "STEP"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/r/b/a/agi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/agi;->d:Lcom/google/r/b/a/agi;

    .line 271
    new-array v0, v6, [Lcom/google/r/b/a/agi;

    sget-object v1, Lcom/google/r/b/a/agi;->a:Lcom/google/r/b/a/agi;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/agi;->b:Lcom/google/r/b/a/agi;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/agi;->c:Lcom/google/r/b/a/agi;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/agi;->d:Lcom/google/r/b/a/agi;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/agi;->f:[Lcom/google/r/b/a/agi;

    .line 328
    new-instance v0, Lcom/google/r/b/a/agj;

    invoke-direct {v0}, Lcom/google/r/b/a/agj;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 337
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 338
    iput p3, p0, Lcom/google/r/b/a/agi;->e:I

    .line 339
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/agi;
    .locals 1

    .prologue
    .line 314
    packed-switch p0, :pswitch_data_0

    .line 319
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 315
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/agi;->a:Lcom/google/r/b/a/agi;

    goto :goto_0

    .line 316
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/agi;->b:Lcom/google/r/b/a/agi;

    goto :goto_0

    .line 317
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/agi;->c:Lcom/google/r/b/a/agi;

    goto :goto_0

    .line 318
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/agi;->d:Lcom/google/r/b/a/agi;

    goto :goto_0

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/agi;
    .locals 1

    .prologue
    .line 271
    const-class v0, Lcom/google/r/b/a/agi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agi;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/agi;
    .locals 1

    .prologue
    .line 271
    sget-object v0, Lcom/google/r/b/a/agi;->f:[Lcom/google/r/b/a/agi;

    invoke-virtual {v0}, [Lcom/google/r/b/a/agi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/agi;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/google/r/b/a/agi;->e:I

    return v0
.end method
