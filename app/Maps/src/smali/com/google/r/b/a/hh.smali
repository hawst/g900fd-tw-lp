.class public final Lcom/google/r/b/a/hh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/hm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/hh;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/hh;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/r/b/a/hi;

    invoke-direct {v0}, Lcom/google/r/b/a/hi;-><init>()V

    sput-object v0, Lcom/google/r/b/a/hh;->PARSER:Lcom/google/n/ax;

    .line 277
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/hh;->f:Lcom/google/n/aw;

    .line 441
    new-instance v0, Lcom/google/r/b/a/hh;

    invoke-direct {v0}, Lcom/google/r/b/a/hh;-><init>()V

    sput-object v0, Lcom/google/r/b/a/hh;->c:Lcom/google/r/b/a/hh;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 35
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 237
    iput-byte v0, p0, Lcom/google/r/b/a/hh;->d:B

    .line 260
    iput v0, p0, Lcom/google/r/b/a/hh;->e:I

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/hh;->b:I

    .line 37
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 43
    invoke-direct {p0}, Lcom/google/r/b/a/hh;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 48
    const/4 v0, 0x0

    .line 49
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 50
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 51
    sparse-switch v3, :sswitch_data_0

    .line 56
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 58
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 54
    goto :goto_0

    .line 63
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 64
    invoke-static {v3}, Lcom/google/r/b/a/hk;->a(I)Lcom/google/r/b/a/hk;

    move-result-object v4

    .line 65
    if-nez v4, :cond_1

    .line 66
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/hh;->au:Lcom/google/n/bn;

    throw v0

    .line 68
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/hh;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/hh;->a:I

    .line 69
    iput v3, p0, Lcom/google/r/b/a/hh;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 81
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/hh;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 51
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 33
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 237
    iput-byte v0, p0, Lcom/google/r/b/a/hh;->d:B

    .line 260
    iput v0, p0, Lcom/google/r/b/a/hh;->e:I

    .line 34
    return-void
.end method

.method public static d()Lcom/google/r/b/a/hh;
    .locals 1

    .prologue
    .line 444
    sget-object v0, Lcom/google/r/b/a/hh;->c:Lcom/google/r/b/a/hh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/hj;
    .locals 1

    .prologue
    .line 339
    new-instance v0, Lcom/google/r/b/a/hj;

    invoke-direct {v0}, Lcom/google/r/b/a/hj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/hh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/r/b/a/hh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 253
    invoke-virtual {p0}, Lcom/google/r/b/a/hh;->c()I

    .line 254
    iget v0, p0, Lcom/google/r/b/a/hh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 255
    iget v0, p0, Lcom/google/r/b/a/hh;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/hh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 258
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 239
    iget-byte v2, p0, Lcom/google/r/b/a/hh;->d:B

    .line 240
    if-ne v2, v0, :cond_0

    .line 248
    :goto_0
    return v0

    .line 241
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 243
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/hh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 244
    iput-byte v1, p0, Lcom/google/r/b/a/hh;->d:B

    move v0, v1

    .line 245
    goto :goto_0

    :cond_2
    move v2, v1

    .line 243
    goto :goto_1

    .line 247
    :cond_3
    iput-byte v0, p0, Lcom/google/r/b/a/hh;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 262
    iget v1, p0, Lcom/google/r/b/a/hh;->e:I

    .line 263
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 272
    :goto_0
    return v0

    .line 266
    :cond_0
    iget v1, p0, Lcom/google/r/b/a/hh;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 267
    iget v1, p0, Lcom/google/r/b/a/hh;->b:I

    .line 268
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_2

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 270
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/hh;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    iput v0, p0, Lcom/google/r/b/a/hh;->e:I

    goto :goto_0

    .line 268
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/google/r/b/a/hh;->newBuilder()Lcom/google/r/b/a/hj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/hj;->a(Lcom/google/r/b/a/hh;)Lcom/google/r/b/a/hj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/google/r/b/a/hh;->newBuilder()Lcom/google/r/b/a/hj;

    move-result-object v0

    return-object v0
.end method
