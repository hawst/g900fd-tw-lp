.class public final Lcom/google/r/b/a/hj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/hm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/hh;",
        "Lcom/google/r/b/a/hj;",
        ">;",
        "Lcom/google/r/b/a/hm;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 357
    sget-object v0, Lcom/google/r/b/a/hh;->c:Lcom/google/r/b/a/hh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 401
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/hj;->b:I

    .line 358
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 349
    new-instance v2, Lcom/google/r/b/a/hh;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/hh;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/hj;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/hj;->b:I

    iput v1, v2, Lcom/google/r/b/a/hh;->b:I

    iput v0, v2, Lcom/google/r/b/a/hh;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 349
    check-cast p1, Lcom/google/r/b/a/hh;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/hj;->a(Lcom/google/r/b/a/hh;)Lcom/google/r/b/a/hj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/hh;)Lcom/google/r/b/a/hj;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 383
    invoke-static {}, Lcom/google/r/b/a/hh;->d()Lcom/google/r/b/a/hh;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 388
    :goto_0
    return-object p0

    .line 384
    :cond_0
    iget v1, p1, Lcom/google/r/b/a/hh;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 385
    iget v0, p1, Lcom/google/r/b/a/hh;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/hk;->a(I)Lcom/google/r/b/a/hk;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/hk;->a:Lcom/google/r/b/a/hk;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 384
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 385
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/hj;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/hj;->a:I

    iget v0, v0, Lcom/google/r/b/a/hk;->j:I

    iput v0, p0, Lcom/google/r/b/a/hj;->b:I

    .line 387
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/hh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 392
    iget v2, p0, Lcom/google/r/b/a/hj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 396
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 392
    goto :goto_0

    :cond_1
    move v0, v1

    .line 396
    goto :goto_1
.end method
