.class public final Lcom/google/r/b/a/ni;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ni;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/ni;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/google/r/b/a/nj;

    invoke-direct {v0}, Lcom/google/r/b/a/nj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ni;->PARSER:Lcom/google/n/ax;

    .line 259
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ni;->i:Lcom/google/n/aw;

    .line 628
    new-instance v0, Lcom/google/r/b/a/ni;

    invoke-direct {v0}, Lcom/google/r/b/a/ni;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ni;->f:Lcom/google/r/b/a/ni;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 140
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ni;->b:Lcom/google/n/ao;

    .line 156
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ni;->c:Lcom/google/n/ao;

    .line 172
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ni;->d:Lcom/google/n/ao;

    .line 202
    iput-byte v3, p0, Lcom/google/r/b/a/ni;->g:B

    .line 230
    iput v3, p0, Lcom/google/r/b/a/ni;->h:I

    .line 63
    iget-object v0, p0, Lcom/google/r/b/a/ni;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 64
    iget-object v0, p0, Lcom/google/r/b/a/ni;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 65
    iget-object v0, p0, Lcom/google/r/b/a/ni;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 66
    iput-boolean v2, p0, Lcom/google/r/b/a/ni;->e:Z

    .line 67
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Lcom/google/r/b/a/ni;-><init>()V

    .line 74
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 79
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 80
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 81
    sparse-switch v0, :sswitch_data_0

    .line 86
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 88
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 84
    goto :goto_0

    .line 93
    :sswitch_1
    iget-object v0, p0, Lcom/google/r/b/a/ni;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 94
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ni;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ni;->au:Lcom/google/n/bn;

    throw v0

    .line 98
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/r/b/a/ni;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 99
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ni;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 116
    :catch_1
    move-exception v0

    .line 117
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 118
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/r/b/a/ni;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 104
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ni;->a:I

    goto :goto_0

    .line 108
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/ni;->a:I

    .line 109
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/ni;->e:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 120
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ni;->au:Lcom/google/n/bn;

    .line 121
    return-void

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 60
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 140
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ni;->b:Lcom/google/n/ao;

    .line 156
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ni;->c:Lcom/google/n/ao;

    .line 172
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ni;->d:Lcom/google/n/ao;

    .line 202
    iput-byte v1, p0, Lcom/google/r/b/a/ni;->g:B

    .line 230
    iput v1, p0, Lcom/google/r/b/a/ni;->h:I

    .line 61
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ni;
    .locals 1

    .prologue
    .line 631
    sget-object v0, Lcom/google/r/b/a/ni;->f:Lcom/google/r/b/a/ni;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/nk;
    .locals 1

    .prologue
    .line 321
    new-instance v0, Lcom/google/r/b/a/nk;

    invoke-direct {v0}, Lcom/google/r/b/a/nk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ni;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    sget-object v0, Lcom/google/r/b/a/ni;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 214
    invoke-virtual {p0}, Lcom/google/r/b/a/ni;->c()I

    .line 215
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/r/b/a/ni;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 218
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 219
    iget-object v0, p0, Lcom/google/r/b/a/ni;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 221
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 222
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/b/a/ni;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 224
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 225
    iget-boolean v0, p0, Lcom/google/r/b/a/ni;->e:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 227
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/ni;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 228
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 204
    iget-byte v1, p0, Lcom/google/r/b/a/ni;->g:B

    .line 205
    if-ne v1, v0, :cond_0

    .line 209
    :goto_0
    return v0

    .line 206
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 208
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ni;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 232
    iget v0, p0, Lcom/google/r/b/a/ni;->h:I

    .line 233
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 254
    :goto_0
    return v0

    .line 236
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 237
    iget-object v0, p0, Lcom/google/r/b/a/ni;->b:Lcom/google/n/ao;

    .line 238
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 240
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 241
    iget-object v2, p0, Lcom/google/r/b/a/ni;->c:Lcom/google/n/ao;

    .line 242
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 244
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 245
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/r/b/a/ni;->d:Lcom/google/n/ao;

    .line 246
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 248
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/ni;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 249
    iget-boolean v2, p0, Lcom/google/r/b/a/ni;->e:Z

    .line 250
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 252
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/ni;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    iput v0, p0, Lcom/google/r/b/a/ni;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/r/b/a/ni;->newBuilder()Lcom/google/r/b/a/nk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/nk;->a(Lcom/google/r/b/a/ni;)Lcom/google/r/b/a/nk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/r/b/a/ni;->newBuilder()Lcom/google/r/b/a/nk;

    move-result-object v0

    return-object v0
.end method
