.class public final Lcom/google/r/b/a/afp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/afn;",
        "Lcom/google/r/b/a/afp;",
        ">;",
        "Lcom/google/r/b/a/afy;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/r/b/a/afb;

.field public c:Lcom/google/maps/a/a;

.field public d:Ljava/lang/Object;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/afq;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/maps/g/jy;

.field private g:Lcom/google/r/b/a/afu;

.field private h:Ljava/lang/Object;

.field private i:Lcom/google/r/b/a/aag;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13682
    sget-object v0, Lcom/google/r/b/a/afn;->j:Lcom/google/r/b/a/afn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 13815
    iput-object v1, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    .line 13877
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    .line 14001
    iput-object v1, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    .line 14062
    iput-object v1, p0, Lcom/google/r/b/a/afp;->c:Lcom/google/maps/a/a;

    .line 14123
    iput-object v1, p0, Lcom/google/r/b/a/afp;->g:Lcom/google/r/b/a/afu;

    .line 14184
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afp;->h:Ljava/lang/Object;

    .line 14260
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afp;->d:Ljava/lang/Object;

    .line 14336
    iput-object v1, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    .line 13683
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 13674
    invoke-virtual {p0}, Lcom/google/r/b/a/afp;->c()Lcom/google/r/b/a/afn;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 13674
    check-cast p1, Lcom/google/r/b/a/afn;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/afp;->a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/afn;)Lcom/google/r/b/a/afp;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 13751
    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 13788
    :goto_0
    return-object p0

    .line 13752
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 13753
    iget-object v0, p1, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_b

    iget-object v3, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v4

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    invoke-static {v3}, Lcom/google/r/b/a/afb;->a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/afd;->a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/afd;->c()Lcom/google/r/b/a/afb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    :goto_3
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13755
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 13756
    iget-object v0, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 13757
    iget-object v0, p1, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    .line 13758
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13765
    :cond_2
    :goto_4
    iget v0, p1, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_e

    move v0, v1

    :goto_5
    if-eqz v0, :cond_3

    .line 13766
    iget-object v0, p1, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    if-nez v0, :cond_f

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v0

    :goto_6
    iget v3, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_10

    iget-object v3, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v4

    if-eq v3, v4, :cond_10

    iget-object v3, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    invoke-static {v3}, Lcom/google/maps/g/jy;->a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/g/kc;->a(Lcom/google/maps/g/jy;)Lcom/google/maps/g/kc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/g/kc;->c()Lcom/google/maps/g/jy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    :goto_7
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13768
    :cond_3
    iget v0, p1, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_11

    move v0, v1

    :goto_8
    if-eqz v0, :cond_4

    .line 13769
    iget-object v0, p1, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    if-nez v0, :cond_12

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v0

    :goto_9
    iget v3, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v3, v3, 0x8

    if-ne v3, v7, :cond_13

    iget-object v3, p0, Lcom/google/r/b/a/afp;->c:Lcom/google/maps/a/a;

    if-eqz v3, :cond_13

    iget-object v3, p0, Lcom/google/r/b/a/afp;->c:Lcom/google/maps/a/a;

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v4

    if-eq v3, v4, :cond_13

    iget-object v3, p0, Lcom/google/r/b/a/afp;->c:Lcom/google/maps/a/a;

    invoke-static {v3}, Lcom/google/maps/a/a;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/maps/a/c;->a(Lcom/google/maps/a/a;)Lcom/google/maps/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/a/c;->c()Lcom/google/maps/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afp;->c:Lcom/google/maps/a/a;

    :goto_a
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13771
    :cond_4
    iget v0, p1, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_14

    move v0, v1

    :goto_b
    if-eqz v0, :cond_5

    .line 13772
    iget-object v0, p1, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    if-nez v0, :cond_15

    invoke-static {}, Lcom/google/r/b/a/afu;->d()Lcom/google/r/b/a/afu;

    move-result-object v0

    :goto_c
    iget v3, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_16

    iget-object v3, p0, Lcom/google/r/b/a/afp;->g:Lcom/google/r/b/a/afu;

    if-eqz v3, :cond_16

    iget-object v3, p0, Lcom/google/r/b/a/afp;->g:Lcom/google/r/b/a/afu;

    invoke-static {}, Lcom/google/r/b/a/afu;->d()Lcom/google/r/b/a/afu;

    move-result-object v4

    if-eq v3, v4, :cond_16

    iget-object v3, p0, Lcom/google/r/b/a/afp;->g:Lcom/google/r/b/a/afu;

    invoke-static {v3}, Lcom/google/r/b/a/afu;->a(Lcom/google/r/b/a/afu;)Lcom/google/r/b/a/afw;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/r/b/a/afw;->a(Lcom/google/r/b/a/afu;)Lcom/google/r/b/a/afw;

    move-result-object v0

    new-instance v3, Lcom/google/r/b/a/afu;

    invoke-direct {v3, v0}, Lcom/google/r/b/a/afu;-><init>(Lcom/google/n/v;)V

    iput-object v3, p0, Lcom/google/r/b/a/afp;->g:Lcom/google/r/b/a/afu;

    :goto_d
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13774
    :cond_5
    iget v0, p1, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_17

    move v0, v1

    :goto_e
    if-eqz v0, :cond_6

    .line 13775
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13776
    iget-object v0, p1, Lcom/google/r/b/a/afn;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/afp;->h:Ljava/lang/Object;

    .line 13779
    :cond_6
    iget v0, p1, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_18

    move v0, v1

    :goto_f
    if-eqz v0, :cond_7

    .line 13780
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13781
    iget-object v0, p1, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/afp;->d:Ljava/lang/Object;

    .line 13784
    :cond_7
    iget v0, p1, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_19

    move v0, v1

    :goto_10
    if-eqz v0, :cond_8

    .line 13785
    iget-object v0, p1, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    if-nez v0, :cond_1a

    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v0

    :goto_11
    iget v1, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_1b

    iget-object v1, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v2

    if-eq v1, v2, :cond_1b

    iget-object v1, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    invoke-static {v1}, Lcom/google/r/b/a/aag;->a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/aai;->a(Lcom/google/r/b/a/aag;)Lcom/google/r/b/a/aai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/aai;->c()Lcom/google/r/b/a/aag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    :goto_12
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13787
    :cond_8
    iget-object v0, p1, Lcom/google/r/b/a/afn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 13752
    goto/16 :goto_1

    .line 13753
    :cond_a
    iget-object v0, p1, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto/16 :goto_2

    :cond_b
    iput-object v0, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    goto/16 :goto_3

    .line 13760
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eq v0, v5, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13761
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_e
    move v0, v2

    .line 13765
    goto/16 :goto_5

    .line 13766
    :cond_f
    iget-object v0, p1, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    goto/16 :goto_6

    :cond_10
    iput-object v0, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    goto/16 :goto_7

    :cond_11
    move v0, v2

    .line 13768
    goto/16 :goto_8

    .line 13769
    :cond_12
    iget-object v0, p1, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    goto/16 :goto_9

    :cond_13
    iput-object v0, p0, Lcom/google/r/b/a/afp;->c:Lcom/google/maps/a/a;

    goto/16 :goto_a

    :cond_14
    move v0, v2

    .line 13771
    goto/16 :goto_b

    .line 13772
    :cond_15
    iget-object v0, p1, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    goto/16 :goto_c

    :cond_16
    iput-object v0, p0, Lcom/google/r/b/a/afp;->g:Lcom/google/r/b/a/afu;

    goto/16 :goto_d

    :cond_17
    move v0, v2

    .line 13774
    goto/16 :goto_e

    :cond_18
    move v0, v2

    .line 13779
    goto/16 :goto_f

    :cond_19
    move v0, v2

    .line 13784
    goto/16 :goto_10

    .line 13785
    :cond_1a
    iget-object v0, p1, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    goto/16 :goto_11

    :cond_1b
    iput-object v0, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    goto :goto_12
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 13792
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 13793
    iget-object v0, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/r/b/a/afb;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 13810
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 13792
    goto :goto_0

    .line 13793
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    goto :goto_1

    .line 13798
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 13799
    iget-object v0, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 13801
    goto :goto_2

    :cond_3
    move v0, v1

    .line 13798
    goto :goto_3

    .line 13799
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    goto :goto_4

    .line 13804
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_5
    if-eqz v0, :cond_8

    .line 13805
    iget-object v0, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/r/b/a/aag;->d()Lcom/google/r/b/a/aag;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, Lcom/google/r/b/a/aag;->b()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 13807
    goto :goto_2

    :cond_6
    move v0, v1

    .line 13804
    goto :goto_5

    .line 13805
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    goto :goto_6

    :cond_8
    move v0, v2

    .line 13810
    goto :goto_2
.end method

.method public final c()Lcom/google/r/b/a/afn;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 13710
    new-instance v2, Lcom/google/r/b/a/afn;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/afn;-><init>(Lcom/google/n/v;)V

    .line 13711
    iget v3, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13712
    const/4 v1, 0x0

    .line 13713
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 13716
    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    iput-object v1, v2, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    .line 13717
    iget v1, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 13718
    iget-object v1, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    .line 13719
    iget v1, p0, Lcom/google/r/b/a/afp;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/r/b/a/afp;->a:I

    .line 13721
    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/afp;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/afn;->c:Ljava/util/List;

    .line 13722
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 13723
    or-int/lit8 v0, v0, 0x2

    .line 13725
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/afp;->f:Lcom/google/maps/g/jy;

    iput-object v1, v2, Lcom/google/r/b/a/afn;->d:Lcom/google/maps/g/jy;

    .line 13726
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 13727
    or-int/lit8 v0, v0, 0x4

    .line 13729
    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/afp;->c:Lcom/google/maps/a/a;

    iput-object v1, v2, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    .line 13730
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 13731
    or-int/lit8 v0, v0, 0x8

    .line 13733
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/afp;->g:Lcom/google/r/b/a/afu;

    iput-object v1, v2, Lcom/google/r/b/a/afn;->f:Lcom/google/r/b/a/afu;

    .line 13734
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 13735
    or-int/lit8 v0, v0, 0x10

    .line 13737
    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/afp;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/afn;->g:Ljava/lang/Object;

    .line 13738
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 13739
    or-int/lit8 v0, v0, 0x20

    .line 13741
    :cond_5
    iget-object v1, p0, Lcom/google/r/b/a/afp;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/afn;->h:Ljava/lang/Object;

    .line 13742
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 13743
    or-int/lit8 v0, v0, 0x40

    .line 13745
    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/afp;->i:Lcom/google/r/b/a/aag;

    iput-object v1, v2, Lcom/google/r/b/a/afn;->i:Lcom/google/r/b/a/aag;

    .line 13746
    iput v0, v2, Lcom/google/r/b/a/afn;->a:I

    .line 13747
    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method
