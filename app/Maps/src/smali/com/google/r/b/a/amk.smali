.class public final enum Lcom/google/r/b/a/amk;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/amk;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/amk;

.field public static final enum b:Lcom/google/r/b/a/amk;

.field public static final enum c:Lcom/google/r/b/a/amk;

.field private static final synthetic e:[Lcom/google/r/b/a/amk;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 114
    new-instance v0, Lcom/google/r/b/a/amk;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/amk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amk;->a:Lcom/google/r/b/a/amk;

    .line 118
    new-instance v0, Lcom/google/r/b/a/amk;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/amk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amk;->b:Lcom/google/r/b/a/amk;

    .line 122
    new-instance v0, Lcom/google/r/b/a/amk;

    const-string v1, "HYBRID"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/amk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/amk;->c:Lcom/google/r/b/a/amk;

    .line 109
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/r/b/a/amk;

    sget-object v1, Lcom/google/r/b/a/amk;->a:Lcom/google/r/b/a/amk;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/amk;->b:Lcom/google/r/b/a/amk;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/amk;->c:Lcom/google/r/b/a/amk;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/r/b/a/amk;->e:[Lcom/google/r/b/a/amk;

    .line 157
    new-instance v0, Lcom/google/r/b/a/aml;

    invoke-direct {v0}, Lcom/google/r/b/a/aml;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 167
    iput p3, p0, Lcom/google/r/b/a/amk;->d:I

    .line 168
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/amk;
    .locals 1

    .prologue
    .line 144
    packed-switch p0, :pswitch_data_0

    .line 148
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 145
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/amk;->a:Lcom/google/r/b/a/amk;

    goto :goto_0

    .line 146
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/amk;->b:Lcom/google/r/b/a/amk;

    goto :goto_0

    .line 147
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/amk;->c:Lcom/google/r/b/a/amk;

    goto :goto_0

    .line 144
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/amk;
    .locals 1

    .prologue
    .line 109
    const-class v0, Lcom/google/r/b/a/amk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/amk;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/amk;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/google/r/b/a/amk;->e:[Lcom/google/r/b/a/amk;

    invoke-virtual {v0}, [Lcom/google/r/b/a/amk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/amk;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/google/r/b/a/amk;->d:I

    return v0
.end method
