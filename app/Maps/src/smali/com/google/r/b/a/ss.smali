.class public final Lcom/google/r/b/a/ss;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/sv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ss;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/r/b/a/ss;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 532
    new-instance v0, Lcom/google/r/b/a/st;

    invoke-direct {v0}, Lcom/google/r/b/a/st;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ss;->PARSER:Lcom/google/n/ax;

    .line 626
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ss;->f:Lcom/google/n/aw;

    .line 828
    new-instance v0, Lcom/google/r/b/a/ss;

    invoke-direct {v0}, Lcom/google/r/b/a/ss;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ss;->c:Lcom/google/r/b/a/ss;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 488
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 590
    iput-byte v0, p0, Lcom/google/r/b/a/ss;->d:B

    .line 609
    iput v0, p0, Lcom/google/r/b/a/ss;->e:I

    .line 489
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ss;->b:Ljava/lang/Object;

    .line 490
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 496
    invoke-direct {p0}, Lcom/google/r/b/a/ss;-><init>()V

    .line 497
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 501
    const/4 v0, 0x0

    .line 502
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 503
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 504
    sparse-switch v3, :sswitch_data_0

    .line 509
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 511
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 507
    goto :goto_0

    .line 516
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 517
    iget v4, p0, Lcom/google/r/b/a/ss;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/ss;->a:I

    .line 518
    iput-object v3, p0, Lcom/google/r/b/a/ss;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 523
    :catch_0
    move-exception v0

    .line 524
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ss;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ss;->au:Lcom/google/n/bn;

    .line 530
    return-void

    .line 525
    :catch_1
    move-exception v0

    .line 526
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 527
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 504
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 486
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 590
    iput-byte v0, p0, Lcom/google/r/b/a/ss;->d:B

    .line 609
    iput v0, p0, Lcom/google/r/b/a/ss;->e:I

    .line 487
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ss;
    .locals 1

    .prologue
    .line 831
    sget-object v0, Lcom/google/r/b/a/ss;->c:Lcom/google/r/b/a/ss;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/su;
    .locals 1

    .prologue
    .line 688
    new-instance v0, Lcom/google/r/b/a/su;

    invoke-direct {v0}, Lcom/google/r/b/a/su;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ss;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544
    sget-object v0, Lcom/google/r/b/a/ss;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 602
    invoke-virtual {p0}, Lcom/google/r/b/a/ss;->c()I

    .line 603
    iget v0, p0, Lcom/google/r/b/a/ss;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 604
    iget-object v0, p0, Lcom/google/r/b/a/ss;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ss;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 606
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/ss;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 607
    return-void

    .line 604
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 592
    iget-byte v1, p0, Lcom/google/r/b/a/ss;->d:B

    .line 593
    if-ne v1, v0, :cond_0

    .line 597
    :goto_0
    return v0

    .line 594
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 596
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/ss;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 611
    iget v0, p0, Lcom/google/r/b/a/ss;->e:I

    .line 612
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 621
    :goto_0
    return v0

    .line 615
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ss;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 617
    iget-object v0, p0, Lcom/google/r/b/a/ss;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ss;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 619
    :goto_2
    iget-object v1, p0, Lcom/google/r/b/a/ss;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 620
    iput v0, p0, Lcom/google/r/b/a/ss;->e:I

    goto :goto_0

    .line 617
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 480
    invoke-static {}, Lcom/google/r/b/a/ss;->newBuilder()Lcom/google/r/b/a/su;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/su;->a(Lcom/google/r/b/a/ss;)Lcom/google/r/b/a/su;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 480
    invoke-static {}, Lcom/google/r/b/a/ss;->newBuilder()Lcom/google/r/b/a/su;

    move-result-object v0

    return-object v0
.end method
