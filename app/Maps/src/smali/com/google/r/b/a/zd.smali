.class public final Lcom/google/r/b/a/zd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/zd;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final v:Lcom/google/r/b/a/zd;

.field private static volatile y:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Ljava/lang/Object;

.field e:Lcom/google/n/ao;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Z

.field h:I

.field i:I

.field j:I

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field t:Ljava/lang/Object;

.field u:Lcom/google/n/f;

.field private w:B

.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 417
    new-instance v0, Lcom/google/r/b/a/ze;

    invoke-direct {v0}, Lcom/google/r/b/a/ze;-><init>()V

    sput-object v0, Lcom/google/r/b/a/zd;->PARSER:Lcom/google/n/ax;

    .line 1678
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/zd;->y:Lcom/google/n/aw;

    .line 2897
    new-instance v0, Lcom/google/r/b/a/zd;

    invoke-direct {v0}, Lcom/google/r/b/a/zd;-><init>()V

    sput-object v0, Lcom/google/r/b/a/zd;->v:Lcom/google/r/b/a/zd;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 225
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1187
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/zd;->e:Lcom/google/n/ao;

    .line 1504
    iput-byte v3, p0, Lcom/google/r/b/a/zd;->w:B

    .line 1580
    iput v3, p0, Lcom/google/r/b/a/zd;->x:I

    .line 226
    iput v1, p0, Lcom/google/r/b/a/zd;->b:I

    .line 227
    iput v4, p0, Lcom/google/r/b/a/zd;->c:I

    .line 228
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zd;->d:Ljava/lang/Object;

    .line 229
    iget-object v0, p0, Lcom/google/r/b/a/zd;->e:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 230
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    .line 231
    iput-boolean v1, p0, Lcom/google/r/b/a/zd;->g:Z

    .line 232
    iput v1, p0, Lcom/google/r/b/a/zd;->h:I

    .line 233
    iput v1, p0, Lcom/google/r/b/a/zd;->i:I

    .line 234
    iput v1, p0, Lcom/google/r/b/a/zd;->j:I

    .line 235
    iput v1, p0, Lcom/google/r/b/a/zd;->k:I

    .line 236
    iput v1, p0, Lcom/google/r/b/a/zd;->l:I

    .line 237
    iput v1, p0, Lcom/google/r/b/a/zd;->m:I

    .line 238
    iput v1, p0, Lcom/google/r/b/a/zd;->n:I

    .line 239
    iput v1, p0, Lcom/google/r/b/a/zd;->o:I

    .line 240
    iput v1, p0, Lcom/google/r/b/a/zd;->p:I

    .line 241
    iput v1, p0, Lcom/google/r/b/a/zd;->q:I

    .line 242
    iput v1, p0, Lcom/google/r/b/a/zd;->r:I

    .line 243
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    .line 244
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/zd;->t:Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/zd;->u:Lcom/google/n/f;

    .line 246
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/16 v9, 0x10

    const/4 v0, 0x0

    const/high16 v8, 0x20000

    .line 252
    invoke-direct {p0}, Lcom/google/r/b/a/zd;-><init>()V

    .line 255
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 258
    :cond_0
    :goto_0
    if-nez v3, :cond_a

    .line 259
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 260
    sparse-switch v0, :sswitch_data_0

    .line 265
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 267
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 263
    goto :goto_0

    .line 272
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 273
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 402
    :catch_0
    move-exception v0

    .line 403
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x10

    if-ne v2, v9, :cond_1

    .line 409
    iget-object v2, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    .line 411
    :cond_1
    and-int/2addr v1, v8

    if-ne v1, v8, :cond_2

    .line 412
    iget-object v1, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    .line 414
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/zd;->au:Lcom/google/n/bn;

    throw v0

    .line 277
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 278
    invoke-static {v0}, Lcom/google/r/b/a/zg;->a(I)Lcom/google/r/b/a/zg;

    move-result-object v6

    .line 279
    if-nez v6, :cond_3

    .line 280
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 404
    :catch_1
    move-exception v0

    .line 405
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 406
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 282
    :cond_3
    :try_start_4
    iget v6, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/r/b/a/zd;->a:I

    .line 283
    iput v0, p0, Lcom/google/r/b/a/zd;->c:I

    goto :goto_0

    .line 288
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 289
    iget v6, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/r/b/a/zd;->a:I

    .line 290
    iput-object v0, p0, Lcom/google/r/b/a/zd;->d:Ljava/lang/Object;

    goto :goto_0

    .line 294
    :sswitch_4
    iget-object v0, p0, Lcom/google/r/b/a/zd;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 295
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    goto/16 :goto_0

    .line 299
    :sswitch_5
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v9, :cond_4

    .line 300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    .line 302
    or-int/lit8 v1, v1, 0x10

    .line 304
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 305
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 304
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 309
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 310
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/zd;->g:Z

    goto/16 :goto_0

    .line 314
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 315
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->h:I

    goto/16 :goto_0

    .line 319
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 320
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->i:I

    goto/16 :goto_0

    .line 324
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 325
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->j:I

    goto/16 :goto_0

    .line 329
    :sswitch_a
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 330
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->k:I

    goto/16 :goto_0

    .line 334
    :sswitch_b
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 335
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->l:I

    goto/16 :goto_0

    .line 339
    :sswitch_c
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 340
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->m:I

    goto/16 :goto_0

    .line 344
    :sswitch_d
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 345
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->n:I

    goto/16 :goto_0

    .line 349
    :sswitch_e
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 350
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->o:I

    goto/16 :goto_0

    .line 354
    :sswitch_f
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 355
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->p:I

    goto/16 :goto_0

    .line 359
    :sswitch_10
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 360
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->q:I

    goto/16 :goto_0

    .line 364
    :sswitch_11
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    const v6, 0x8000

    or-int/2addr v0, v6

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 365
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/zd;->r:I

    goto/16 :goto_0

    .line 369
    :sswitch_12
    and-int v0, v1, v8

    if-eq v0, v8, :cond_5

    .line 370
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    .line 371
    or-int/2addr v1, v8

    .line 373
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 377
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 378
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 379
    and-int v0, v1, v8

    if-eq v0, v8, :cond_6

    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_7

    move v0, v2

    :goto_1
    if-lez v0, :cond_6

    .line 380
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    .line 381
    or-int/2addr v1, v8

    .line 383
    :cond_6
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_8

    move v0, v2

    :goto_3
    if-lez v0, :cond_9

    .line 384
    iget-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 379
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 383
    :cond_8
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 386
    :cond_9
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 390
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 391
    iget v6, p0, Lcom/google/r/b/a/zd;->a:I

    const/high16 v7, 0x10000

    or-int/2addr v6, v7

    iput v6, p0, Lcom/google/r/b/a/zd;->a:I

    .line 392
    iput-object v0, p0, Lcom/google/r/b/a/zd;->t:Ljava/lang/Object;

    goto/16 :goto_0

    .line 396
    :sswitch_15
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    or-int/2addr v0, v8

    iput v0, p0, Lcom/google/r/b/a/zd;->a:I

    .line 397
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->u:Lcom/google/n/f;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 408
    :cond_a
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v9, :cond_b

    .line 409
    iget-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    .line 411
    :cond_b
    and-int v0, v1, v8

    if-ne v0, v8, :cond_c

    .line 412
    iget-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    .line 414
    :cond_c
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->au:Lcom/google/n/bn;

    .line 415
    return-void

    .line 260
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x92 -> :sswitch_13
        0x9a -> :sswitch_14
        0xa2 -> :sswitch_15
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 223
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1187
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/zd;->e:Lcom/google/n/ao;

    .line 1504
    iput-byte v1, p0, Lcom/google/r/b/a/zd;->w:B

    .line 1580
    iput v1, p0, Lcom/google/r/b/a/zd;->x:I

    .line 224
    return-void
.end method

.method public static d()Lcom/google/r/b/a/zd;
    .locals 1

    .prologue
    .line 2900
    sget-object v0, Lcom/google/r/b/a/zd;->v:Lcom/google/r/b/a/zd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/zf;
    .locals 1

    .prologue
    .line 1740
    new-instance v0, Lcom/google/r/b/a/zf;

    invoke-direct {v0}, Lcom/google/r/b/a/zf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/zd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 429
    sget-object v0, Lcom/google/r/b/a/zd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1516
    invoke-virtual {p0}, Lcom/google/r/b/a/zd;->c()I

    .line 1517
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1518
    iget v0, p0, Lcom/google/r/b/a/zd;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1520
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1521
    iget v0, p0, Lcom/google/r/b/a/zd;->c:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 1523
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 1524
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/zd;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->d:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1526
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 1527
    iget-object v0, p0, Lcom/google/r/b/a/zd;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_3
    move v1, v2

    .line 1529
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1530
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1529
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1524
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1532
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 1533
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/r/b/a/zd;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1535
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 1536
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/zd;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1538
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 1539
    iget v0, p0, Lcom/google/r/b/a/zd;->i:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1541
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 1542
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/r/b/a/zd;->j:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1544
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 1545
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/r/b/a/zd;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1547
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    .line 1548
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/r/b/a/zd;->l:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1550
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 1551
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/r/b/a/zd;->m:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1553
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 1554
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/r/b/a/zd;->n:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1556
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_e

    .line 1557
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/r/b/a/zd;->o:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1559
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_f

    .line 1560
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/r/b/a/zd;->p:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1562
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_10

    .line 1563
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/r/b/a/zd;->q:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 1565
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_11

    .line 1566
    const/16 v0, 0x11

    iget v1, p0, Lcom/google/r/b/a/zd;->r:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_11
    move v1, v2

    .line 1568
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 1569
    const/16 v3, 0x12

    iget-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 1568
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1571
    :cond_12
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_13

    .line 1572
    const/16 v1, 0x13

    iget-object v0, p0, Lcom/google/r/b/a/zd;->t:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->t:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1574
    :cond_13
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_14

    .line 1575
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/r/b/a/zd;->u:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1577
    :cond_14
    iget-object v0, p0, Lcom/google/r/b/a/zd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1578
    return-void

    .line 1572
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1506
    iget-byte v1, p0, Lcom/google/r/b/a/zd;->w:B

    .line 1507
    if-ne v1, v0, :cond_0

    .line 1511
    :goto_0
    return v0

    .line 1508
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1510
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/zd;->w:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1582
    iget v0, p0, Lcom/google/r/b/a/zd;->x:I

    .line 1583
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1673
    :goto_0
    return v0

    .line 1586
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_17

    .line 1587
    iget v0, p0, Lcom/google/r/b/a/zd;->b:I

    .line 1588
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1590
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_16

    .line 1591
    iget v2, p0, Lcom/google/r/b/a/zd;->c:I

    .line 1592
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 1594
    :goto_3
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 1595
    const/4 v3, 0x3

    .line 1596
    iget-object v0, p0, Lcom/google/r/b/a/zd;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1598
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 1599
    iget-object v0, p0, Lcom/google/r/b/a/zd;->e:Lcom/google/n/ao;

    .line 1600
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    :cond_2
    move v3, v2

    move v2, v1

    .line 1602
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1603
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/zd;->f:Ljava/util/List;

    .line 1604
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1602
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1592
    :cond_3
    const/16 v2, 0xa

    goto :goto_2

    .line 1596
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 1606
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_6

    .line 1607
    const/4 v0, 0x6

    iget-boolean v2, p0, Lcom/google/r/b/a/zd;->g:Z

    .line 1608
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 1610
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_7

    .line 1611
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/r/b/a/zd;->h:I

    .line 1612
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1614
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_8

    .line 1615
    iget v0, p0, Lcom/google/r/b/a/zd;->i:I

    .line 1616
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1618
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_9

    .line 1619
    const/16 v0, 0x9

    iget v2, p0, Lcom/google/r/b/a/zd;->j:I

    .line 1620
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1622
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_a

    .line 1623
    const/16 v0, 0xa

    iget v2, p0, Lcom/google/r/b/a/zd;->k:I

    .line 1624
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1626
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_b

    .line 1627
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/r/b/a/zd;->l:I

    .line 1628
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1630
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_c

    .line 1631
    const/16 v0, 0xc

    iget v2, p0, Lcom/google/r/b/a/zd;->m:I

    .line 1632
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1634
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_d

    .line 1635
    const/16 v0, 0xd

    iget v2, p0, Lcom/google/r/b/a/zd;->n:I

    .line 1636
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1638
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_e

    .line 1639
    const/16 v0, 0xe

    iget v2, p0, Lcom/google/r/b/a/zd;->o:I

    .line 1640
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1642
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_f

    .line 1643
    const/16 v0, 0xf

    iget v2, p0, Lcom/google/r/b/a/zd;->p:I

    .line 1644
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1646
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v2, 0x4000

    if-ne v0, v2, :cond_10

    .line 1647
    const/16 v0, 0x10

    iget v2, p0, Lcom/google/r/b/a/zd;->q:I

    .line 1648
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1650
    :cond_10
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    const v2, 0x8000

    and-int/2addr v0, v2

    const v2, 0x8000

    if-ne v0, v2, :cond_11

    .line 1651
    const/16 v0, 0x11

    iget v2, p0, Lcom/google/r/b/a/zd;->r:I

    .line 1652
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_11
    move v2, v1

    move v4, v1

    .line 1656
    :goto_6
    iget-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_12

    .line 1657
    iget-object v0, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    .line 1658
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v4, v0

    .line 1656
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 1660
    :cond_12
    add-int v0, v3, v4

    .line 1661
    iget-object v2, p0, Lcom/google/r/b/a/zd;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    .line 1663
    iget v0, p0, Lcom/google/r/b/a/zd;->a:I

    const/high16 v3, 0x10000

    and-int/2addr v0, v3

    const/high16 v3, 0x10000

    if-ne v0, v3, :cond_15

    .line 1664
    const/16 v3, 0x13

    .line 1665
    iget-object v0, p0, Lcom/google/r/b/a/zd;->t:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/zd;->t:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 1667
    :goto_8
    iget v2, p0, Lcom/google/r/b/a/zd;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_13

    .line 1668
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/r/b/a/zd;->u:Lcom/google/n/f;

    .line 1669
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1671
    :cond_13
    iget-object v1, p0, Lcom/google/r/b/a/zd;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1672
    iput v0, p0, Lcom/google/r/b/a/zd;->x:I

    goto/16 :goto_0

    .line 1665
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    :cond_15
    move v0, v2

    goto :goto_8

    :cond_16
    move v2, v0

    goto/16 :goto_3

    :cond_17
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 217
    invoke-static {}, Lcom/google/r/b/a/zd;->newBuilder()Lcom/google/r/b/a/zf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/zf;->a(Lcom/google/r/b/a/zd;)Lcom/google/r/b/a/zf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 217
    invoke-static {}, Lcom/google/r/b/a/zd;->newBuilder()Lcom/google/r/b/a/zf;

    move-result-object v0

    return-object v0
.end method
