.class public final Lcom/google/r/b/a/ff;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/fg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/fd;",
        "Lcom/google/r/b/a/ff;",
        ">;",
        "Lcom/google/r/b/a/fg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 680
    sget-object v0, Lcom/google/r/b/a/fd;->h:Lcom/google/r/b/a/fd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 777
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ff;->b:Ljava/lang/Object;

    .line 853
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ff;->c:Ljava/lang/Object;

    .line 929
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ff;->d:Ljava/lang/Object;

    .line 1005
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ff;->e:Ljava/lang/Object;

    .line 1081
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ff;->f:Ljava/lang/Object;

    .line 1157
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ff;->g:Ljava/lang/Object;

    .line 681
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 672
    new-instance v2, Lcom/google/r/b/a/fd;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/fd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ff;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/ff;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/fd;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ff;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/fd;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ff;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/ff;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/ff;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/r/b/a/ff;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/fd;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 672
    check-cast p1, Lcom/google/r/b/a/fd;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ff;->a(Lcom/google/r/b/a/fd;)Lcom/google/r/b/a/ff;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/fd;)Lcom/google/r/b/a/ff;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 736
    invoke-static {}, Lcom/google/r/b/a/fd;->j()Lcom/google/r/b/a/fd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 768
    :goto_0
    return-object p0

    .line 737
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 738
    iget v2, p0, Lcom/google/r/b/a/ff;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/ff;->a:I

    .line 739
    iget-object v2, p1, Lcom/google/r/b/a/fd;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ff;->b:Ljava/lang/Object;

    .line 742
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 743
    iget v2, p0, Lcom/google/r/b/a/ff;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ff;->a:I

    .line 744
    iget-object v2, p1, Lcom/google/r/b/a/fd;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ff;->c:Ljava/lang/Object;

    .line 747
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 748
    iget v2, p0, Lcom/google/r/b/a/ff;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/ff;->a:I

    .line 749
    iget-object v2, p1, Lcom/google/r/b/a/fd;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ff;->d:Ljava/lang/Object;

    .line 752
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 753
    iget v2, p0, Lcom/google/r/b/a/ff;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/ff;->a:I

    .line 754
    iget-object v2, p1, Lcom/google/r/b/a/fd;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ff;->e:Ljava/lang/Object;

    .line 757
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 758
    iget v2, p0, Lcom/google/r/b/a/ff;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/ff;->a:I

    .line 759
    iget-object v2, p1, Lcom/google/r/b/a/fd;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ff;->f:Ljava/lang/Object;

    .line 762
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/fd;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 763
    iget v0, p0, Lcom/google/r/b/a/ff;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/ff;->a:I

    .line 764
    iget-object v0, p1, Lcom/google/r/b/a/fd;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/ff;->g:Ljava/lang/Object;

    .line 767
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/fd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 737
    goto :goto_1

    :cond_8
    move v2, v1

    .line 742
    goto :goto_2

    :cond_9
    move v2, v1

    .line 747
    goto :goto_3

    :cond_a
    move v2, v1

    .line 752
    goto :goto_4

    :cond_b
    move v2, v1

    .line 757
    goto :goto_5

    :cond_c
    move v0, v1

    .line 762
    goto :goto_6
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 772
    const/4 v0, 0x1

    return v0
.end method
