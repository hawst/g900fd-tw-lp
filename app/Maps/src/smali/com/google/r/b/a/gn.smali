.class public final enum Lcom/google/r/b/a/gn;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/gn;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/gn;

.field private static final synthetic c:[Lcom/google/r/b/a/gn;


# instance fields
.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 143
    new-instance v0, Lcom/google/r/b/a/gn;

    const-string v1, "FOLLOWING"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/r/b/a/gn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/gn;->a:Lcom/google/r/b/a/gn;

    .line 138
    new-array v0, v3, [Lcom/google/r/b/a/gn;

    sget-object v1, Lcom/google/r/b/a/gn;->a:Lcom/google/r/b/a/gn;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/r/b/a/gn;->c:[Lcom/google/r/b/a/gn;

    .line 168
    new-instance v0, Lcom/google/r/b/a/go;

    invoke-direct {v0}, Lcom/google/r/b/a/go;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 178
    iput p3, p0, Lcom/google/r/b/a/gn;->b:I

    .line 179
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/gn;
    .locals 1

    .prologue
    .line 157
    packed-switch p0, :pswitch_data_0

    .line 159
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 158
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/gn;->a:Lcom/google/r/b/a/gn;

    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/gn;
    .locals 1

    .prologue
    .line 138
    const-class v0, Lcom/google/r/b/a/gn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/gn;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/gn;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/google/r/b/a/gn;->c:[Lcom/google/r/b/a/gn;

    invoke-virtual {v0}, [Lcom/google/r/b/a/gn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/gn;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/google/r/b/a/gn;->b:I

    return v0
.end method
