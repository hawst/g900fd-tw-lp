.class public final Lcom/google/r/b/a/aee;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aef;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aec;",
        "Lcom/google/r/b/a/aee;",
        ">;",
        "Lcom/google/r/b/a/aef;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:F

.field private d:Z

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/n/ao;

.field private h:I

.field private i:I

.field private j:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 18844
    sget-object v0, Lcom/google/r/b/a/aec;->k:Lcom/google/r/b/a/aec;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 18974
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    .line 19174
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aee;->e:Ljava/lang/Object;

    .line 19250
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aee;->f:Ljava/lang/Object;

    .line 19326
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aee;->g:Lcom/google/n/ao;

    .line 19449
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aee;->j:Ljava/lang/Object;

    .line 18845
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 18836
    new-instance v2, Lcom/google/r/b/a/aec;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aec;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aee;->a:I

    iget v4, p0, Lcom/google/r/b/a/aee;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/aee;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/aee;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_8

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/aee;->c:F

    iput v4, v2, Lcom/google/r/b/a/aec;->c:F

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-boolean v4, p0, Lcom/google/r/b/a/aee;->d:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/aec;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, p0, Lcom/google/r/b/a/aee;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, p0, Lcom/google/r/b/a/aee;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/aec;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/aee;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/aee;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget v1, p0, Lcom/google/r/b/a/aee;->h:I

    iput v1, v2, Lcom/google/r/b/a/aec;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget v1, p0, Lcom/google/r/b/a/aee;->i:I

    iput v1, v2, Lcom/google/r/b/a/aec;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/aee;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/aec;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 18836
    check-cast p1, Lcom/google/r/b/a/aec;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aee;->a(Lcom/google/r/b/a/aec;)Lcom/google/r/b/a/aee;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aec;)Lcom/google/r/b/a/aee;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 18921
    invoke-static {}, Lcom/google/r/b/a/aec;->d()Lcom/google/r/b/a/aec;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 18964
    :goto_0
    return-object p0

    .line 18922
    :cond_0
    iget-object v2, p1, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 18923
    iget-object v2, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 18924
    iget-object v2, p1, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    .line 18925
    iget v2, p0, Lcom/google/r/b/a/aee;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/r/b/a/aee;->a:I

    .line 18932
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 18933
    iget v2, p1, Lcom/google/r/b/a/aec;->c:F

    iget v3, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/aee;->a:I

    iput v2, p0, Lcom/google/r/b/a/aee;->c:F

    .line 18935
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 18936
    iget-boolean v2, p1, Lcom/google/r/b/a/aec;->d:Z

    iget v3, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/aee;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aee;->d:Z

    .line 18938
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 18939
    iget v2, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/r/b/a/aee;->a:I

    .line 18940
    iget-object v2, p1, Lcom/google/r/b/a/aec;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aee;->e:Ljava/lang/Object;

    .line 18943
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 18944
    iget v2, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/r/b/a/aee;->a:I

    .line 18945
    iget-object v2, p1, Lcom/google/r/b/a/aec;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aee;->f:Ljava/lang/Object;

    .line 18948
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 18949
    iget-object v2, p0, Lcom/google/r/b/a/aee;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/aec;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18950
    iget v2, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/r/b/a/aee;->a:I

    .line 18952
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 18953
    iget v2, p1, Lcom/google/r/b/a/aec;->h:I

    iget v3, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/aee;->a:I

    iput v2, p0, Lcom/google/r/b/a/aee;->h:I

    .line 18955
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/aec;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 18956
    iget v2, p1, Lcom/google/r/b/a/aec;->i:I

    iget v3, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/aee;->a:I

    iput v2, p0, Lcom/google/r/b/a/aee;->i:I

    .line 18958
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/aec;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_13

    :goto_9
    if-eqz v0, :cond_9

    .line 18959
    iget v0, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/aee;->a:I

    .line 18960
    iget-object v0, p1, Lcom/google/r/b/a/aec;->j:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/aee;->j:Ljava/lang/Object;

    .line 18963
    :cond_9
    iget-object v0, p1, Lcom/google/r/b/a/aec;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 18927
    :cond_a
    iget v2, p0, Lcom/google/r/b/a/aee;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/r/b/a/aee;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/aee;->a:I

    .line 18928
    :cond_b
    iget-object v2, p0, Lcom/google/r/b/a/aee;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/aec;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 18932
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 18935
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 18938
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 18943
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 18948
    goto/16 :goto_6

    :cond_11
    move v2, v1

    .line 18952
    goto :goto_7

    :cond_12
    move v2, v1

    .line 18955
    goto :goto_8

    :cond_13
    move v0, v1

    .line 18958
    goto :goto_9
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 18968
    const/4 v0, 0x1

    return v0
.end method
