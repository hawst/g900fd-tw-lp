.class public final Lcom/google/r/b/a/yj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ym;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/yj;",
            ">;"
        }
    .end annotation
.end field

.field static final s:Lcom/google/r/b/a/yj;

.field private static final serialVersionUID:J

.field private static volatile v:Lcom/google/n/aw;


# instance fields
.field a:I

.field public b:Z

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:Ljava/lang/Object;

.field public j:I

.field public k:Lcom/google/r/b/a/ia;

.field public l:Ljava/lang/Object;

.field public m:Ljava/lang/Object;

.field public n:Ljava/lang/Object;

.field o:I

.field p:I

.field q:I

.field public r:I

.field private t:B

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 694
    new-instance v0, Lcom/google/r/b/a/yk;

    invoke-direct {v0}, Lcom/google/r/b/a/yk;-><init>()V

    sput-object v0, Lcom/google/r/b/a/yj;->PARSER:Lcom/google/n/ax;

    .line 1251
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/yj;->v:Lcom/google/n/aw;

    .line 2312
    new-instance v0, Lcom/google/r/b/a/yj;

    invoke-direct {v0}, Lcom/google/r/b/a/yj;-><init>()V

    sput-object v0, Lcom/google/r/b/a/yj;->s:Lcom/google/r/b/a/yj;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x200

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 532
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1075
    iput-byte v0, p0, Lcom/google/r/b/a/yj;->t:B

    .line 1170
    iput v0, p0, Lcom/google/r/b/a/yj;->u:I

    .line 533
    iput-boolean v1, p0, Lcom/google/r/b/a/yj;->b:Z

    .line 534
    iput v1, p0, Lcom/google/r/b/a/yj;->c:I

    .line 535
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/r/b/a/yj;->d:I

    .line 536
    iput v1, p0, Lcom/google/r/b/a/yj;->e:I

    .line 537
    iput v1, p0, Lcom/google/r/b/a/yj;->f:I

    .line 538
    iput v2, p0, Lcom/google/r/b/a/yj;->g:I

    .line 539
    iput v2, p0, Lcom/google/r/b/a/yj;->h:I

    .line 540
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    .line 541
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/yj;->j:I

    .line 542
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    .line 543
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    .line 544
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    .line 545
    iput v1, p0, Lcom/google/r/b/a/yj;->o:I

    .line 546
    iput v1, p0, Lcom/google/r/b/a/yj;->p:I

    .line 547
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/yj;->q:I

    .line 548
    iput v1, p0, Lcom/google/r/b/a/yj;->r:I

    .line 549
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 555
    invoke-direct {p0}, Lcom/google/r/b/a/yj;-><init>()V

    .line 556
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    .line 561
    :cond_0
    :goto_0
    if-nez v4, :cond_5

    .line 562
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 563
    sparse-switch v0, :sswitch_data_0

    .line 568
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 570
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 566
    goto :goto_0

    .line 575
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 576
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/yj;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 685
    :catch_0
    move-exception v0

    .line 686
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 691
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/yj;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v3

    .line 576
    goto :goto_1

    .line 580
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 581
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 687
    :catch_1
    move-exception v0

    .line 688
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 689
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 585
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 586
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->e:I

    goto :goto_0

    .line 590
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 591
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->f:I

    goto :goto_0

    .line 595
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 596
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->g:I

    goto :goto_0

    .line 600
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 601
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->h:I

    goto/16 :goto_0

    .line 605
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 606
    iget v1, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/r/b/a/yj;->a:I

    .line 607
    iput-object v0, p0, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 611
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 612
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->j:I

    goto/16 :goto_0

    .line 616
    :sswitch_9
    const/4 v0, 0x0

    .line 617
    iget v1, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v6, 0x200

    if-ne v1, v6, :cond_6

    .line 618
    iget-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    invoke-static {}, Lcom/google/r/b/a/ia;->newBuilder()Lcom/google/r/b/a/ic;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/ic;->a(Lcom/google/r/b/a/ia;)Lcom/google/r/b/a/ic;

    move-result-object v0

    move-object v1, v0

    .line 620
    :goto_2
    sget-object v0, Lcom/google/r/b/a/ia;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ia;

    iput-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    .line 621
    if-eqz v1, :cond_2

    .line 622
    iget-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/ic;->a(Lcom/google/r/b/a/ia;)Lcom/google/r/b/a/ic;

    .line 623
    invoke-virtual {v1}, Lcom/google/r/b/a/ic;->c()Lcom/google/r/b/a/ia;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    .line 625
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    goto/16 :goto_0

    .line 629
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 630
    iget v1, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/r/b/a/yj;->a:I

    .line 631
    iput-object v0, p0, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 635
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 636
    iget v1, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p0, Lcom/google/r/b/a/yj;->a:I

    .line 637
    iput-object v0, p0, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 641
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 642
    iget v1, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/r/b/a/yj;->a:I

    .line 643
    iput-object v0, p0, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    goto/16 :goto_0

    .line 647
    :sswitch_d
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 648
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->o:I

    goto/16 :goto_0

    .line 652
    :sswitch_e
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 653
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->p:I

    goto/16 :goto_0

    .line 657
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 658
    invoke-static {v0}, Lcom/google/r/b/a/yn;->a(I)Lcom/google/r/b/a/yn;

    move-result-object v1

    .line 659
    if-nez v1, :cond_3

    .line 660
    const/16 v1, 0x10

    invoke-virtual {v5, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 662
    :cond_3
    iget v1, p0, Lcom/google/r/b/a/yj;->a:I

    const v6, 0x8000

    or-int/2addr v1, v6

    iput v1, p0, Lcom/google/r/b/a/yj;->a:I

    .line 663
    iput v0, p0, Lcom/google/r/b/a/yj;->q:I

    goto/16 :goto_0

    .line 668
    :sswitch_10
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/yj;->a:I

    .line 669
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/yj;->d:I

    goto/16 :goto_0

    .line 673
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 674
    invoke-static {v0}, Lcom/google/r/b/a/yz;->a(I)Lcom/google/r/b/a/yz;

    move-result-object v1

    .line 675
    if-nez v1, :cond_4

    .line 676
    const/16 v1, 0x3d

    invoke-virtual {v5, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 678
    :cond_4
    iget v1, p0, Lcom/google/r/b/a/yj;->a:I

    const/high16 v6, 0x10000

    or-int/2addr v1, v6

    iput v1, p0, Lcom/google/r/b/a/yj;->a:I

    .line 679
    iput v0, p0, Lcom/google/r/b/a/yj;->r:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 691
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->au:Lcom/google/n/bn;

    .line 692
    return-void

    :cond_6
    move-object v1, v0

    goto/16 :goto_2

    .line 563
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x1e8 -> :sswitch_11
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 530
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1075
    iput-byte v0, p0, Lcom/google/r/b/a/yj;->t:B

    .line 1170
    iput v0, p0, Lcom/google/r/b/a/yj;->u:I

    .line 531
    return-void
.end method

.method public static a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;
    .locals 1

    .prologue
    .line 1316
    invoke-static {}, Lcom/google/r/b/a/yj;->newBuilder()Lcom/google/r/b/a/yl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yl;->a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/yj;
    .locals 1

    .prologue
    .line 2315
    sget-object v0, Lcom/google/r/b/a/yj;->s:Lcom/google/r/b/a/yj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/yl;
    .locals 1

    .prologue
    .line 1313
    new-instance v0, Lcom/google/r/b/a/yl;

    invoke-direct {v0}, Lcom/google/r/b/a/yl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/yj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 706
    sget-object v0, Lcom/google/r/b/a/yj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const v6, 0x8000

    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 1115
    invoke-virtual {p0}, Lcom/google/r/b/a/yj;->c()I

    .line 1116
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1117
    iget-boolean v0, p0, Lcom/google/r/b/a/yj;->b:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IZ)V

    .line 1119
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1120
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/yj;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1122
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_2

    .line 1123
    iget v0, p0, Lcom/google/r/b/a/yj;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 1125
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_3

    .line 1126
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/yj;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1128
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 1129
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/r/b/a/yj;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1131
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 1132
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/r/b/a/yj;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1134
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 1135
    iget-object v0, p0, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1137
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    .line 1138
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/r/b/a/yj;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1140
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_8

    .line 1141
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    if-nez v0, :cond_12

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1143
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_9

    .line 1144
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1146
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_a

    .line 1147
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1149
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_b

    .line 1150
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1152
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_c

    .line 1153
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/r/b/a/yj;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1155
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_d

    .line 1156
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/r/b/a/yj;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1158
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_e

    .line 1159
    iget v0, p0, Lcom/google/r/b/a/yj;->q:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 1161
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_f

    .line 1162
    const/16 v0, 0x11

    iget v1, p0, Lcom/google/r/b/a/yj;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1164
    :cond_f
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_10

    .line 1165
    const/16 v0, 0x3d

    iget v1, p0, Lcom/google/r/b/a/yj;->r:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 1167
    :cond_10
    iget-object v0, p0, Lcom/google/r/b/a/yj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1168
    return-void

    .line 1135
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 1141
    :cond_12
    iget-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    goto/16 :goto_1

    .line 1144
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 1147
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 1150
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1077
    iget-byte v0, p0, Lcom/google/r/b/a/yj;->t:B

    .line 1078
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1110
    :goto_0
    return v0

    .line 1079
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1081
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 1082
    iput-byte v2, p0, Lcom/google/r/b/a/yj;->t:B

    move v0, v2

    .line 1083
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1081
    goto :goto_1

    .line 1085
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    .line 1086
    iput-byte v2, p0, Lcom/google/r/b/a/yj;->t:B

    move v0, v2

    .line 1087
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1085
    goto :goto_2

    .line 1089
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-nez v0, :cond_7

    .line 1090
    iput-byte v2, p0, Lcom/google/r/b/a/yj;->t:B

    move v0, v2

    .line 1091
    goto :goto_0

    :cond_6
    move v0, v2

    .line 1089
    goto :goto_3

    .line 1093
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_4
    if-nez v0, :cond_9

    .line 1094
    iput-byte v2, p0, Lcom/google/r/b/a/yj;->t:B

    move v0, v2

    .line 1095
    goto :goto_0

    :cond_8
    move v0, v2

    .line 1093
    goto :goto_4

    .line 1097
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-nez v0, :cond_b

    .line 1098
    iput-byte v2, p0, Lcom/google/r/b/a/yj;->t:B

    move v0, v2

    .line 1099
    goto :goto_0

    :cond_a
    move v0, v2

    .line 1097
    goto :goto_5

    .line 1101
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v3, 0x4000

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_6
    if-nez v0, :cond_d

    .line 1102
    iput-byte v2, p0, Lcom/google/r/b/a/yj;->t:B

    move v0, v2

    .line 1103
    goto :goto_0

    :cond_c
    move v0, v2

    .line 1101
    goto :goto_6

    .line 1105
    :cond_d
    iget-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v0

    :goto_7
    invoke-virtual {v0}, Lcom/google/r/b/a/ia;->b()Z

    move-result v0

    if-nez v0, :cond_f

    .line 1106
    iput-byte v2, p0, Lcom/google/r/b/a/yj;->t:B

    move v0, v2

    .line 1107
    goto :goto_0

    .line 1105
    :cond_e
    iget-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    goto :goto_7

    .line 1109
    :cond_f
    iput-byte v1, p0, Lcom/google/r/b/a/yj;->t:B

    move v0, v1

    .line 1110
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v4, 0x2

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1172
    iget v0, p0, Lcom/google/r/b/a/yj;->u:I

    .line 1173
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1246
    :goto_0
    return v0

    .line 1176
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_21

    .line 1177
    iget-boolean v0, p0, Lcom/google/r/b/a/yj;->b:Z

    .line 1178
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1180
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1181
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/r/b/a/yj;->c:I

    .line 1182
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_11

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1184
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v7, :cond_2

    .line 1185
    iget v2, p0, Lcom/google/r/b/a/yj;->e:I

    .line 1186
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_12

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1188
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_3

    .line 1189
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/r/b/a/yj;->f:I

    .line 1190
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_13

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1192
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_4

    .line 1193
    const/4 v2, 0x6

    iget v4, p0, Lcom/google/r/b/a/yj;->g:I

    .line 1194
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_14

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_5
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1196
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_20

    .line 1197
    const/4 v2, 0x7

    iget v4, p0, Lcom/google/r/b/a/yj;->h:I

    .line 1198
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_15

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_6
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    move v2, v0

    .line 1200
    :goto_7
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_5

    .line 1202
    iget-object v0, p0, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->i:Ljava/lang/Object;

    :goto_8
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1204
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_6

    .line 1205
    const/16 v0, 0x9

    iget v4, p0, Lcom/google/r/b/a/yj;->j:I

    .line 1206
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_17

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1208
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_7

    .line 1210
    iget-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    if-nez v0, :cond_18

    invoke-static {}, Lcom/google/r/b/a/ia;->d()Lcom/google/r/b/a/ia;

    move-result-object v0

    :goto_a
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1212
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_8

    .line 1213
    const/16 v4, 0xb

    .line 1214
    iget-object v0, p0, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->l:Ljava/lang/Object;

    :goto_b
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1216
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_9

    .line 1217
    const/16 v4, 0xc

    .line 1218
    iget-object v0, p0, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->m:Ljava/lang/Object;

    :goto_c
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1220
    :cond_9
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_a

    .line 1221
    const/16 v4, 0xd

    .line 1222
    iget-object v0, p0, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/yj;->n:Ljava/lang/Object;

    :goto_d
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1224
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v0, v4, :cond_b

    .line 1225
    const/16 v0, 0xe

    iget v4, p0, Lcom/google/r/b/a/yj;->o:I

    .line 1226
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_1c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_e
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1228
    :cond_b
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v0, v4, :cond_c

    .line 1229
    const/16 v0, 0xf

    iget v4, p0, Lcom/google/r/b/a/yj;->p:I

    .line 1230
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_1d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_f
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1232
    :cond_c
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    const v4, 0x8000

    and-int/2addr v0, v4

    const v4, 0x8000

    if-ne v0, v4, :cond_d

    .line 1233
    const/16 v0, 0x10

    iget v4, p0, Lcom/google/r/b/a/yj;->q:I

    .line 1234
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_1e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_10
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1236
    :cond_d
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_e

    .line 1237
    const/16 v0, 0x11

    iget v4, p0, Lcom/google/r/b/a/yj;->d:I

    .line 1238
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_1f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_11
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1240
    :cond_e
    iget v0, p0, Lcom/google/r/b/a/yj;->a:I

    const/high16 v4, 0x10000

    and-int/2addr v0, v4

    const/high16 v4, 0x10000

    if-ne v0, v4, :cond_10

    .line 1241
    const/16 v0, 0x3d

    iget v4, p0, Lcom/google/r/b/a/yj;->r:I

    .line 1242
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_f
    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1244
    :cond_10
    iget-object v0, p0, Lcom/google/r/b/a/yj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1245
    iput v0, p0, Lcom/google/r/b/a/yj;->u:I

    goto/16 :goto_0

    :cond_11
    move v2, v3

    .line 1182
    goto/16 :goto_2

    :cond_12
    move v2, v3

    .line 1186
    goto/16 :goto_3

    :cond_13
    move v2, v3

    .line 1190
    goto/16 :goto_4

    :cond_14
    move v2, v3

    .line 1194
    goto/16 :goto_5

    :cond_15
    move v2, v3

    .line 1198
    goto/16 :goto_6

    .line 1202
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    :cond_17
    move v0, v3

    .line 1206
    goto/16 :goto_9

    .line 1210
    :cond_18
    iget-object v0, p0, Lcom/google/r/b/a/yj;->k:Lcom/google/r/b/a/ia;

    goto/16 :goto_a

    .line 1214
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_b

    .line 1218
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_c

    .line 1222
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_d

    :cond_1c
    move v0, v3

    .line 1226
    goto/16 :goto_e

    :cond_1d
    move v0, v3

    .line 1230
    goto/16 :goto_f

    :cond_1e
    move v0, v3

    .line 1234
    goto :goto_10

    :cond_1f
    move v0, v3

    .line 1238
    goto :goto_11

    :cond_20
    move v2, v0

    goto/16 :goto_7

    :cond_21
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 524
    invoke-static {}, Lcom/google/r/b/a/yj;->newBuilder()Lcom/google/r/b/a/yl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yl;->a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 524
    invoke-static {}, Lcom/google/r/b/a/yj;->newBuilder()Lcom/google/r/b/a/yl;

    move-result-object v0

    return-object v0
.end method
