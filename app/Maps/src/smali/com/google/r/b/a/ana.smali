.class public final Lcom/google/r/b/a/ana;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/and;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ana;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/r/b/a/ana;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3474
    new-instance v0, Lcom/google/r/b/a/anb;

    invoke-direct {v0}, Lcom/google/r/b/a/anb;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ana;->PARSER:Lcom/google/n/ax;

    .line 3654
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ana;->h:Lcom/google/n/aw;

    .line 4160
    new-instance v0, Lcom/google/r/b/a/ana;

    invoke-direct {v0}, Lcom/google/r/b/a/ana;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ana;->e:Lcom/google/r/b/a/ana;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 3403
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3491
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ana;->b:Lcom/google/n/ao;

    .line 3592
    iput-byte v2, p0, Lcom/google/r/b/a/ana;->f:B

    .line 3629
    iput v2, p0, Lcom/google/r/b/a/ana;->g:I

    .line 3404
    iget-object v0, p0, Lcom/google/r/b/a/ana;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 3405
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    .line 3406
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    .line 3407
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 3413
    invoke-direct {p0}, Lcom/google/r/b/a/ana;-><init>()V

    .line 3416
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 3419
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 3420
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 3421
    sparse-switch v1, :sswitch_data_0

    .line 3426
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 3428
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 3424
    goto :goto_0

    .line 3433
    :sswitch_1
    iget-object v1, p0, Lcom/google/r/b/a/ana;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 3434
    iget v1, p0, Lcom/google/r/b/a/ana;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/r/b/a/ana;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 3459
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 3460
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3465
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 3466
    iget-object v2, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    .line 3468
    :cond_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_2

    .line 3469
    iget-object v1, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    .line 3471
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ana;->au:Lcom/google/n/bn;

    throw v0

    .line 3438
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_7

    .line 3439
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3441
    or-int/lit8 v1, v0, 0x2

    .line 3443
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 3444
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 3443
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 3445
    goto :goto_0

    .line 3448
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v8, :cond_3

    .line 3449
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    .line 3451
    or-int/lit8 v0, v0, 0x4

    .line 3453
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 3454
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 3453
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 3461
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 3462
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 3463
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3465
    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v7, :cond_5

    .line 3466
    iget-object v1, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    .line 3468
    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_6

    .line 3469
    iget-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    .line 3471
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ana;->au:Lcom/google/n/bn;

    .line 3472
    return-void

    .line 3465
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto/16 :goto_2

    .line 3461
    :catch_2
    move-exception v0

    goto :goto_4

    .line 3459
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_7
    move v1, v0

    goto :goto_3

    .line 3421
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x4a -> :sswitch_2
        0x52 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 3401
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3491
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ana;->b:Lcom/google/n/ao;

    .line 3592
    iput-byte v1, p0, Lcom/google/r/b/a/ana;->f:B

    .line 3629
    iput v1, p0, Lcom/google/r/b/a/ana;->g:I

    .line 3402
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ana;
    .locals 1

    .prologue
    .line 4163
    sget-object v0, Lcom/google/r/b/a/ana;->e:Lcom/google/r/b/a/ana;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/anc;
    .locals 1

    .prologue
    .line 3716
    new-instance v0, Lcom/google/r/b/a/anc;

    invoke-direct {v0}, Lcom/google/r/b/a/anc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ana;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3486
    sget-object v0, Lcom/google/r/b/a/ana;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3616
    invoke-virtual {p0}, Lcom/google/r/b/a/ana;->c()I

    .line 3617
    iget v0, p0, Lcom/google/r/b/a/ana;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3618
    iget-object v0, p0, Lcom/google/r/b/a/ana;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 3620
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 3621
    const/16 v3, 0x9

    iget-object v0, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3620
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3623
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 3624
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3623
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3626
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ana;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3627
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3594
    iget-byte v0, p0, Lcom/google/r/b/a/ana;->f:B

    .line 3595
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 3611
    :cond_0
    :goto_0
    return v2

    .line 3596
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 3598
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3599
    iget-object v0, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ams;->d()Lcom/google/r/b/a/ams;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ams;

    invoke-virtual {v0}, Lcom/google/r/b/a/ams;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3600
    iput-byte v2, p0, Lcom/google/r/b/a/ana;->f:B

    goto :goto_0

    .line 3598
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 3604
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 3605
    iget-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/amo;->d()Lcom/google/r/b/a/amo;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/amo;

    invoke-virtual {v0}, Lcom/google/r/b/a/amo;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3606
    iput-byte v2, p0, Lcom/google/r/b/a/ana;->f:B

    goto :goto_0

    .line 3604
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3610
    :cond_5
    iput-byte v3, p0, Lcom/google/r/b/a/ana;->f:B

    move v2, v3

    .line 3611
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3631
    iget v0, p0, Lcom/google/r/b/a/ana;->g:I

    .line 3632
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3649
    :goto_0
    return v0

    .line 3635
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ana;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 3636
    iget-object v0, p0, Lcom/google/r/b/a/ana;->b:Lcom/google/n/ao;

    .line 3637
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 3639
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 3640
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/r/b/a/ana;->c:Ljava/util/List;

    .line 3641
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3639
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_1
    move v2, v1

    .line 3643
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 3644
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/r/b/a/ana;->d:Ljava/util/List;

    .line 3645
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3643
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 3647
    :cond_2
    iget-object v0, p0, Lcom/google/r/b/a/ana;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 3648
    iput v0, p0, Lcom/google/r/b/a/ana;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3395
    invoke-static {}, Lcom/google/r/b/a/ana;->newBuilder()Lcom/google/r/b/a/anc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/anc;->a(Lcom/google/r/b/a/ana;)Lcom/google/r/b/a/anc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3395
    invoke-static {}, Lcom/google/r/b/a/ana;->newBuilder()Lcom/google/r/b/a/anc;

    move-result-object v0

    return-object v0
.end method
