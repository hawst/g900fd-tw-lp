.class public final Lcom/google/r/b/a/xy;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/zb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xy;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/xy;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/r/b/a/yj;

.field public c:Lcom/google/r/b/a/yt;

.field public d:Lcom/google/r/b/a/ya;

.field public e:Lcom/google/r/b/a/yp;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/google/r/b/a/xz;

    invoke-direct {v0}, Lcom/google/r/b/a/xz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/xy;->PARSER:Lcom/google/n/ax;

    .line 5258
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/xy;->i:Lcom/google/n/aw;

    .line 5673
    new-instance v0, Lcom/google/r/b/a/xy;

    invoke-direct {v0}, Lcom/google/r/b/a/xy;-><init>()V

    sput-object v0, Lcom/google/r/b/a/xy;->f:Lcom/google/r/b/a/xy;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 62
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5181
    iput-byte v0, p0, Lcom/google/r/b/a/xy;->g:B

    .line 5229
    iput v0, p0, Lcom/google/r/b/a/xy;->h:I

    .line 63
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 69
    invoke-direct {p0}, Lcom/google/r/b/a/xy;-><init>()V

    .line 70
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 74
    const/4 v0, 0x0

    move v3, v0

    .line 75
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 76
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 77
    sparse-switch v0, :sswitch_data_0

    .line 82
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 84
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 80
    goto :goto_0

    .line 90
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 91
    iget-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    invoke-static {v0}, Lcom/google/r/b/a/yj;->a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;

    move-result-object v0

    move-object v1, v0

    .line 93
    :goto_1
    const/4 v0, 0x1

    sget-object v6, Lcom/google/r/b/a/yj;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v6, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yj;

    iput-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    .line 95
    if-eqz v1, :cond_1

    .line 96
    iget-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/yl;->a(Lcom/google/r/b/a/yj;)Lcom/google/r/b/a/yl;

    .line 97
    invoke-virtual {v1}, Lcom/google/r/b/a/yl;->c()Lcom/google/r/b/a/yj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    .line 99
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/xy;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/xy;->au:Lcom/google/n/bn;

    throw v0

    .line 104
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 105
    iget-object v0, p0, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    invoke-static {v0}, Lcom/google/r/b/a/yt;->a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;

    move-result-object v0

    move-object v1, v0

    .line 107
    :goto_2
    const/16 v0, 0x21

    sget-object v6, Lcom/google/r/b/a/yt;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v6, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yt;

    iput-object v0, p0, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    .line 109
    if-eqz v1, :cond_2

    .line 110
    iget-object v0, p0, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/yv;->a(Lcom/google/r/b/a/yt;)Lcom/google/r/b/a/yv;

    .line 111
    invoke-virtual {v1}, Lcom/google/r/b/a/yv;->c()Lcom/google/r/b/a/yt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    .line 113
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/xy;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 148
    :catch_1
    move-exception v0

    .line 149
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 150
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 118
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    .line 119
    iget-object v0, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    invoke-static {v0}, Lcom/google/r/b/a/ya;->a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;

    move-result-object v0

    move-object v1, v0

    .line 121
    :goto_3
    const/16 v0, 0x31

    sget-object v6, Lcom/google/r/b/a/ya;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v6, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ya;

    iput-object v0, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    .line 123
    if-eqz v1, :cond_3

    .line 124
    iget-object v0, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/yc;->a(Lcom/google/r/b/a/ya;)Lcom/google/r/b/a/yc;

    .line 125
    invoke-virtual {v1}, Lcom/google/r/b/a/yc;->c()Lcom/google/r/b/a/ya;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    .line 127
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/xy;->a:I

    goto/16 :goto_0

    .line 132
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 133
    iget-object v0, p0, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    invoke-static {v0}, Lcom/google/r/b/a/yp;->a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;

    move-result-object v0

    move-object v1, v0

    .line 135
    :goto_4
    const/16 v0, 0x3a

    sget-object v6, Lcom/google/r/b/a/yp;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v6, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/yp;

    iput-object v0, p0, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    .line 137
    if-eqz v1, :cond_4

    .line 138
    iget-object v0, p0, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/yr;->a(Lcom/google/r/b/a/yp;)Lcom/google/r/b/a/yr;

    .line 139
    invoke-virtual {v1}, Lcom/google/r/b/a/yr;->c()Lcom/google/r/b/a/yp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    .line 141
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/xy;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 152
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/xy;->au:Lcom/google/n/bn;

    .line 153
    return-void

    :cond_6
    move-object v1, v2

    goto :goto_4

    :cond_7
    move-object v1, v2

    goto :goto_3

    :cond_8
    move-object v1, v2

    goto/16 :goto_2

    :cond_9
    move-object v1, v2

    goto/16 :goto_1

    .line 77
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
        0x10b -> :sswitch_2
        0x18b -> :sswitch_3
        0x1d3 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 60
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5181
    iput-byte v0, p0, Lcom/google/r/b/a/xy;->g:B

    .line 5229
    iput v0, p0, Lcom/google/r/b/a/xy;->h:I

    .line 61
    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/google/r/b/a/xy;
    .locals 1

    .prologue
    .line 5290
    sget-object v0, Lcom/google/r/b/a/xy;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, p0}, Lcom/google/n/ax;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/xy;

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/xy;
    .locals 1

    .prologue
    .line 5676
    sget-object v0, Lcom/google/r/b/a/xy;->f:Lcom/google/r/b/a/xy;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/yi;
    .locals 1

    .prologue
    .line 5320
    new-instance v0, Lcom/google/r/b/a/yi;

    invoke-direct {v0}, Lcom/google/r/b/a/yi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/xy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    sget-object v0, Lcom/google/r/b/a/xy;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 5213
    invoke-virtual {p0}, Lcom/google/r/b/a/xy;->c()I

    .line 5214
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5215
    iget-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/r/b/a/yj;->d()Lcom/google/r/b/a/yj;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/at;)V

    .line 5217
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 5218
    const/16 v1, 0x21

    iget-object v0, p0, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/r/b/a/yt;->d()Lcom/google/r/b/a/yt;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/at;)V

    .line 5220
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 5221
    const/16 v1, 0x31

    iget-object v0, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/r/b/a/ya;->d()Lcom/google/r/b/a/ya;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/at;)V

    .line 5223
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 5224
    const/16 v1, 0x3a

    iget-object v0, p0, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/r/b/a/yp;->d()Lcom/google/r/b/a/yp;

    move-result-object v0

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/at;)V

    .line 5226
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/xy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5227
    return-void

    .line 5215
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    goto :goto_0

    .line 5218
    :cond_5
    iget-object v0, p0, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    goto :goto_1

    .line 5221
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    goto :goto_2

    .line 5224
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5183
    iget-byte v0, p0, Lcom/google/r/b/a/xy;->g:B

    .line 5184
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 5208
    :goto_0
    return v0

    .line 5185
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 5187
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 5188
    iput-byte v2, p0, Lcom/google/r/b/a/xy;->g:B

    move v0, v2

    .line 5189
    goto :goto_0

    :cond_2
    move v0, v2

    .line 5187
    goto :goto_1

    .line 5191
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    .line 5192
    iput-byte v2, p0, Lcom/google/r/b/a/xy;->g:B

    move v0, v2

    .line 5193
    goto :goto_0

    :cond_4
    move v0, v2

    .line 5191
    goto :goto_2

    .line 5195
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-nez v0, :cond_7

    .line 5196
    iput-byte v2, p0, Lcom/google/r/b/a/xy;->g:B

    move v0, v2

    .line 5197
    goto :goto_0

    :cond_6
    move v0, v2

    .line 5195
    goto :goto_3

    .line 5199
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/r/b/a/yj;->d()Lcom/google/r/b/a/yj;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/r/b/a/yj;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 5200
    iput-byte v2, p0, Lcom/google/r/b/a/xy;->g:B

    move v0, v2

    .line 5201
    goto :goto_0

    .line 5199
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    goto :goto_4

    .line 5203
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/r/b/a/ya;->d()Lcom/google/r/b/a/ya;

    move-result-object v0

    :goto_5
    invoke-virtual {v0}, Lcom/google/r/b/a/ya;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 5204
    iput-byte v2, p0, Lcom/google/r/b/a/xy;->g:B

    move v0, v2

    .line 5205
    goto :goto_0

    .line 5203
    :cond_a
    iget-object v0, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    goto :goto_5

    .line 5207
    :cond_b
    iput-byte v1, p0, Lcom/google/r/b/a/xy;->g:B

    move v0, v1

    .line 5208
    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 5231
    iget v0, p0, Lcom/google/r/b/a/xy;->h:I

    .line 5232
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5253
    :goto_0
    return v0

    .line 5235
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 5237
    iget-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/r/b/a/yj;->d()Lcom/google/r/b/a/yj;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 5239
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 5240
    const/16 v3, 0x21

    .line 5241
    iget-object v2, p0, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    if-nez v2, :cond_5

    invoke-static {}, Lcom/google/r/b/a/yt;->d()Lcom/google/r/b/a/yt;

    move-result-object v2

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 5243
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 5244
    const/16 v3, 0x31

    .line 5245
    iget-object v2, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    if-nez v2, :cond_6

    invoke-static {}, Lcom/google/r/b/a/ya;->d()Lcom/google/r/b/a/ya;

    move-result-object v2

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 5247
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/xy;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 5248
    const/16 v3, 0x3a

    .line 5249
    iget-object v2, p0, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    if-nez v2, :cond_7

    invoke-static {}, Lcom/google/r/b/a/yp;->d()Lcom/google/r/b/a/yp;

    move-result-object v2

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    shl-int/lit8 v1, v1, 0x1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 5251
    :cond_3
    iget-object v1, p0, Lcom/google/r/b/a/xy;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 5252
    iput v0, p0, Lcom/google/r/b/a/xy;->h:I

    goto/16 :goto_0

    .line 5237
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/xy;->b:Lcom/google/r/b/a/yj;

    goto/16 :goto_1

    .line 5241
    :cond_5
    iget-object v2, p0, Lcom/google/r/b/a/xy;->c:Lcom/google/r/b/a/yt;

    goto :goto_3

    .line 5245
    :cond_6
    iget-object v2, p0, Lcom/google/r/b/a/xy;->d:Lcom/google/r/b/a/ya;

    goto :goto_4

    .line 5249
    :cond_7
    iget-object v2, p0, Lcom/google/r/b/a/xy;->e:Lcom/google/r/b/a/yp;

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/r/b/a/xy;->newBuilder()Lcom/google/r/b/a/yi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/yi;->a(Lcom/google/r/b/a/xy;)Lcom/google/r/b/a/yi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/r/b/a/xy;->newBuilder()Lcom/google/r/b/a/yi;

    move-result-object v0

    return-object v0
.end method
