.class public final Lcom/google/r/b/a/alz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/alz;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/r/b/a/alz;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Z

.field d:Z

.field e:I

.field f:I

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Lcom/google/n/ao;

.field k:Lcom/google/n/f;

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 213
    new-instance v0, Lcom/google/r/b/a/ama;

    invoke-direct {v0}, Lcom/google/r/b/a/ama;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alz;->PARSER:Lcom/google/n/ax;

    .line 489
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/alz;->o:Lcom/google/n/aw;

    .line 1173
    new-instance v0, Lcom/google/r/b/a/alz;

    invoke-direct {v0}, Lcom/google/r/b/a/alz;-><init>()V

    sput-object v0, Lcom/google/r/b/a/alz;->l:Lcom/google/r/b/a/alz;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 230
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->b:Lcom/google/n/ao;

    .line 306
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->g:Lcom/google/n/ao;

    .line 322
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->h:Lcom/google/n/ao;

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    .line 354
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->j:Lcom/google/n/ao;

    .line 384
    iput-byte v3, p0, Lcom/google/r/b/a/alz;->m:B

    .line 436
    iput v3, p0, Lcom/google/r/b/a/alz;->n:I

    .line 117
    iget-object v0, p0, Lcom/google/r/b/a/alz;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 118
    iput-boolean v2, p0, Lcom/google/r/b/a/alz;->c:Z

    .line 119
    iput-boolean v4, p0, Lcom/google/r/b/a/alz;->d:Z

    .line 120
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/r/b/a/alz;->e:I

    .line 121
    iput v4, p0, Lcom/google/r/b/a/alz;->f:I

    .line 122
    iget-object v0, p0, Lcom/google/r/b/a/alz;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 123
    iget-object v0, p0, Lcom/google/r/b/a/alz;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 124
    iget-object v0, p0, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 125
    iget-object v0, p0, Lcom/google/r/b/a/alz;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 126
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/r/b/a/alz;->k:Lcom/google/n/f;

    .line 127
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Lcom/google/r/b/a/alz;-><init>()V

    .line 134
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 139
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 140
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 141
    sparse-switch v0, :sswitch_data_0

    .line 146
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 148
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 144
    goto :goto_0

    .line 153
    :sswitch_1
    iget-object v0, p0, Lcom/google/r/b/a/alz;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 154
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/alz;->au:Lcom/google/n/bn;

    throw v0

    .line 158
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    .line 159
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/alz;->e:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 206
    :catch_1
    move-exception v0

    .line 207
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 208
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 163
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    .line 164
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/alz;->f:I

    goto :goto_0

    .line 168
    :sswitch_4
    iget-object v0, p0, Lcom/google/r/b/a/alz;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 169
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    goto :goto_0

    .line 173
    :sswitch_5
    iget-object v0, p0, Lcom/google/r/b/a/alz;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 174
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    goto/16 :goto_0

    .line 178
    :sswitch_6
    iget-object v0, p0, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 179
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    goto/16 :goto_0

    .line 183
    :sswitch_7
    iget-object v0, p0, Lcom/google/r/b/a/alz;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 184
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    goto/16 :goto_0

    .line 188
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    .line 189
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alz;->k:Lcom/google/n/f;

    goto/16 :goto_0

    .line 193
    :sswitch_9
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    .line 194
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/alz;->c:Z

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 198
    :sswitch_a
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/alz;->a:I

    .line 199
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/alz;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 210
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/alz;->au:Lcom/google/n/bn;

    .line 211
    return-void

    .line 141
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 114
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 230
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->b:Lcom/google/n/ao;

    .line 306
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->g:Lcom/google/n/ao;

    .line 322
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->h:Lcom/google/n/ao;

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    .line 354
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/alz;->j:Lcom/google/n/ao;

    .line 384
    iput-byte v1, p0, Lcom/google/r/b/a/alz;->m:B

    .line 436
    iput v1, p0, Lcom/google/r/b/a/alz;->n:I

    .line 115
    return-void
.end method

.method public static d()Lcom/google/r/b/a/alz;
    .locals 1

    .prologue
    .line 1176
    sget-object v0, Lcom/google/r/b/a/alz;->l:Lcom/google/r/b/a/alz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/amb;
    .locals 1

    .prologue
    .line 551
    new-instance v0, Lcom/google/r/b/a/amb;

    invoke-direct {v0}, Lcom/google/r/b/a/amb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/alz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    sget-object v0, Lcom/google/r/b/a/alz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 402
    invoke-virtual {p0}, Lcom/google/r/b/a/alz;->c()I

    .line 403
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 404
    iget-object v0, p0, Lcom/google/r/b/a/alz;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 406
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_1

    .line 407
    iget v0, p0, Lcom/google/r/b/a/alz;->e:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 409
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 410
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/alz;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 412
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 413
    iget-object v0, p0, Lcom/google/r/b/a/alz;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 415
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    .line 416
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/r/b/a/alz;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 418
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 419
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 421
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_6

    .line 422
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/r/b/a/alz;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 424
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_7

    .line 425
    iget-object v0, p0, Lcom/google/r/b/a/alz;->k:Lcom/google/n/f;

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 427
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_8

    .line 428
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/alz;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 430
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_9

    .line 431
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/r/b/a/alz;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 433
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/alz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 434
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 386
    iget-byte v0, p0, Lcom/google/r/b/a/alz;->m:B

    .line 387
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 397
    :goto_0
    return v0

    .line 388
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 390
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 391
    iget-object v0, p0, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 392
    iput-byte v2, p0, Lcom/google/r/b/a/alz;->m:B

    move v0, v2

    .line 393
    goto :goto_0

    :cond_2
    move v0, v2

    .line 390
    goto :goto_1

    .line 396
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/alz;->m:B

    move v0, v1

    .line 397
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 438
    iget v0, p0, Lcom/google/r/b/a/alz;->n:I

    .line 439
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 484
    :goto_0
    return v0

    .line 442
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 443
    iget-object v0, p0, Lcom/google/r/b/a/alz;->b:Lcom/google/n/ao;

    .line 444
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 446
    :goto_1
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_1

    .line 447
    iget v2, p0, Lcom/google/r/b/a/alz;->e:I

    .line 448
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_a

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 450
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_2

    .line 451
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/r/b/a/alz;->f:I

    .line 452
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 454
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_3

    .line 455
    iget-object v2, p0, Lcom/google/r/b/a/alz;->g:Lcom/google/n/ao;

    .line 456
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 458
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_4

    .line 459
    const/4 v2, 0x5

    iget-object v4, p0, Lcom/google/r/b/a/alz;->h:Lcom/google/n/ao;

    .line 460
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 462
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_5

    .line 463
    const/4 v2, 0x6

    iget-object v4, p0, Lcom/google/r/b/a/alz;->i:Lcom/google/n/ao;

    .line 464
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 466
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v4, 0x100

    if-ne v2, v4, :cond_6

    .line 467
    const/4 v2, 0x7

    iget-object v4, p0, Lcom/google/r/b/a/alz;->j:Lcom/google/n/ao;

    .line 468
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 470
    :cond_6
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v4, 0x200

    if-ne v2, v4, :cond_7

    .line 471
    const/16 v2, 0x8

    iget-object v4, p0, Lcom/google/r/b/a/alz;->k:Lcom/google/n/f;

    .line 472
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 474
    :cond_7
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_8

    .line 475
    const/16 v2, 0x9

    iget-boolean v4, p0, Lcom/google/r/b/a/alz;->c:Z

    .line 476
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 478
    :cond_8
    iget v2, p0, Lcom/google/r/b/a/alz;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_9

    .line 479
    iget-boolean v2, p0, Lcom/google/r/b/a/alz;->d:Z

    .line 480
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 482
    :cond_9
    iget-object v1, p0, Lcom/google/r/b/a/alz;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 483
    iput v0, p0, Lcom/google/r/b/a/alz;->n:I

    goto/16 :goto_0

    :cond_a
    move v2, v3

    .line 448
    goto/16 :goto_2

    :cond_b
    move v2, v3

    .line 452
    goto/16 :goto_3

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/google/r/b/a/alz;->newBuilder()Lcom/google/r/b/a/amb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/amb;->a(Lcom/google/r/b/a/alz;)Lcom/google/r/b/a/amb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/google/r/b/a/alz;->newBuilder()Lcom/google/r/b/a/amb;

    move-result-object v0

    return-object v0
.end method
