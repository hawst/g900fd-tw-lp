.class public final Lcom/google/r/b/a/aog;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aoj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aog;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/r/b/a/aog;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/r/b/a/aoh;

    invoke-direct {v0}, Lcom/google/r/b/a/aoh;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aog;->PARSER:Lcom/google/n/ax;

    .line 214
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aog;->g:Lcom/google/n/aw;

    .line 453
    new-instance v0, Lcom/google/r/b/a/aog;

    invoke-direct {v0}, Lcom/google/r/b/a/aog;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aog;->d:Lcom/google/r/b/a/aog;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 48
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 169
    iput-byte v0, p0, Lcom/google/r/b/a/aog;->e:B

    .line 191
    iput v0, p0, Lcom/google/r/b/a/aog;->f:I

    .line 49
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/aog;->c:Z

    .line 51
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const v10, 0x7fffffff

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v4, 0x1

    .line 57
    invoke-direct {p0}, Lcom/google/r/b/a/aog;-><init>()V

    .line 60
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 63
    :cond_0
    :goto_0
    if-nez v3, :cond_7

    .line 64
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 65
    sparse-switch v0, :sswitch_data_0

    .line 70
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 72
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 68
    goto :goto_0

    .line 77
    :sswitch_1
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v4, :cond_1

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    .line 79
    or-int/lit8 v1, v1, 0x1

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_2

    .line 111
    iget-object v1, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    .line 113
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aog;->au:Lcom/google/n/bn;

    throw v0

    .line 85
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 86
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 87
    and-int/lit8 v0, v1, 0x1

    if-eq v0, v4, :cond_3

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v10, :cond_4

    move v0, v2

    :goto_1
    if-lez v0, :cond_3

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    .line 89
    or-int/lit8 v1, v1, 0x1

    .line 91
    :cond_3
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v10, :cond_5

    move v0, v2

    :goto_3
    if-lez v0, :cond_6

    .line 92
    iget-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 106
    :catch_1
    move-exception v0

    .line 107
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 108
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 87
    :cond_4
    :try_start_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 91
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 94
    :cond_6
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 98
    :sswitch_3
    iget v0, p0, Lcom/google/r/b/a/aog;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/aog;->a:I

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/aog;->c:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 110
    :cond_7
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v4, :cond_8

    .line 111
    iget-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    .line 113
    :cond_8
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aog;->au:Lcom/google/n/bn;

    .line 114
    return-void

    .line 65
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 46
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 169
    iput-byte v0, p0, Lcom/google/r/b/a/aog;->e:B

    .line 191
    iput v0, p0, Lcom/google/r/b/a/aog;->f:I

    .line 47
    return-void
.end method

.method public static d()Lcom/google/r/b/a/aog;
    .locals 1

    .prologue
    .line 456
    sget-object v0, Lcom/google/r/b/a/aog;->d:Lcom/google/r/b/a/aog;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/aoi;
    .locals 1

    .prologue
    .line 276
    new-instance v0, Lcom/google/r/b/a/aoi;

    invoke-direct {v0}, Lcom/google/r/b/a/aoi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    sget-object v0, Lcom/google/r/b/a/aog;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 181
    invoke-virtual {p0}, Lcom/google/r/b/a/aog;->c()I

    .line 182
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v4, v2, v3}, Lcom/google/n/l;->c(IJ)V

    .line 182
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 185
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aog;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1

    .line 186
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/r/b/a/aog;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/aog;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 189
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 171
    iget-byte v1, p0, Lcom/google/r/b/a/aog;->e:B

    .line 172
    if-ne v1, v0, :cond_0

    .line 176
    :goto_0
    return v0

    .line 173
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aog;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 193
    iget v0, p0, Lcom/google/r/b/a/aog;->f:I

    .line 194
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 209
    :goto_0
    return v0

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x8

    .line 200
    add-int/lit8 v0, v0, 0x0

    .line 201
    iget-object v1, p0, Lcom/google/r/b/a/aog;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 203
    iget v1, p0, Lcom/google/r/b/a/aog;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 204
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/r/b/a/aog;->c:Z

    .line 205
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/aog;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    iput v0, p0, Lcom/google/r/b/a/aog;->f:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/google/r/b/a/aog;->newBuilder()Lcom/google/r/b/a/aoi;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/aoi;->a(Lcom/google/r/b/a/aog;)Lcom/google/r/b/a/aoi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/google/r/b/a/aog;->newBuilder()Lcom/google/r/b/a/aoi;

    move-result-object v0

    return-object v0
.end method
