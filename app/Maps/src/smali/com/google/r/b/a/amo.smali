.class public final Lcom/google/r/b/a/amo;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/amr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/amo;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/r/b/a/amo;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Lcom/google/n/ao;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8080
    new-instance v0, Lcom/google/r/b/a/amp;

    invoke-direct {v0}, Lcom/google/r/b/a/amp;-><init>()V

    sput-object v0, Lcom/google/r/b/a/amo;->PARSER:Lcom/google/n/ax;

    .line 8248
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/amo;->i:Lcom/google/n/aw;

    .line 8613
    new-instance v0, Lcom/google/r/b/a/amo;

    invoke-direct {v0}, Lcom/google/r/b/a/amo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/amo;->f:Lcom/google/r/b/a/amo;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 8012
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 8128
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amo;->d:Lcom/google/n/ao;

    .line 8185
    iput-byte v2, p0, Lcom/google/r/b/a/amo;->g:B

    .line 8219
    iput v2, p0, Lcom/google/r/b/a/amo;->h:I

    .line 8013
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/amo;->b:I

    .line 8014
    const/16 v0, 0x3c

    iput v0, p0, Lcom/google/r/b/a/amo;->c:I

    .line 8015
    iget-object v0, p0, Lcom/google/r/b/a/amo;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 8016
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/amo;->e:Ljava/lang/Object;

    .line 8017
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 8023
    invoke-direct {p0}, Lcom/google/r/b/a/amo;-><init>()V

    .line 8024
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 8029
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 8030
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 8031
    sparse-switch v3, :sswitch_data_0

    .line 8036
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 8038
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 8034
    goto :goto_0

    .line 8043
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 8044
    invoke-static {v3}, Lcom/google/r/b/a/amx;->a(I)Lcom/google/r/b/a/amx;

    move-result-object v4

    .line 8045
    if-nez v4, :cond_1

    .line 8046
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 8071
    :catch_0
    move-exception v0

    .line 8072
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8077
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/amo;->au:Lcom/google/n/bn;

    throw v0

    .line 8048
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/r/b/a/amo;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/amo;->a:I

    .line 8049
    iput v3, p0, Lcom/google/r/b/a/amo;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 8073
    :catch_1
    move-exception v0

    .line 8074
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 8075
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 8054
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/r/b/a/amo;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 8055
    iget v3, p0, Lcom/google/r/b/a/amo;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/amo;->a:I

    goto :goto_0

    .line 8059
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 8060
    iget v4, p0, Lcom/google/r/b/a/amo;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/amo;->a:I

    .line 8061
    iput-object v3, p0, Lcom/google/r/b/a/amo;->e:Ljava/lang/Object;

    goto :goto_0

    .line 8065
    :sswitch_4
    iget v3, p0, Lcom/google/r/b/a/amo;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/amo;->a:I

    .line 8066
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/r/b/a/amo;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 8077
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amo;->au:Lcom/google/n/bn;

    .line 8078
    return-void

    .line 8031
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8010
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 8128
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/amo;->d:Lcom/google/n/ao;

    .line 8185
    iput-byte v1, p0, Lcom/google/r/b/a/amo;->g:B

    .line 8219
    iput v1, p0, Lcom/google/r/b/a/amo;->h:I

    .line 8011
    return-void
.end method

.method public static d()Lcom/google/r/b/a/amo;
    .locals 1

    .prologue
    .line 8616
    sget-object v0, Lcom/google/r/b/a/amo;->f:Lcom/google/r/b/a/amo;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/amq;
    .locals 1

    .prologue
    .line 8310
    new-instance v0, Lcom/google/r/b/a/amq;

    invoke-direct {v0}, Lcom/google/r/b/a/amq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/amo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8092
    sget-object v0, Lcom/google/r/b/a/amo;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 8203
    invoke-virtual {p0}, Lcom/google/r/b/a/amo;->c()I

    .line 8204
    iget v0, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 8205
    iget v0, p0, Lcom/google/r/b/a/amo;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 8207
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 8208
    iget-object v0, p0, Lcom/google/r/b/a/amo;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 8210
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 8211
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/amo;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amo;->e:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 8213
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_3

    .line 8214
    iget v0, p0, Lcom/google/r/b/a/amo;->c:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 8216
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/amo;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 8217
    return-void

    .line 8211
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 8187
    iget-byte v0, p0, Lcom/google/r/b/a/amo;->g:B

    .line 8188
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 8198
    :goto_0
    return v0

    .line 8189
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 8191
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 8192
    iget-object v0, p0, Lcom/google/r/b/a/amo;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/f/e;->d()Lcom/google/maps/f/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/f/e;

    invoke-virtual {v0}, Lcom/google/maps/f/e;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 8193
    iput-byte v2, p0, Lcom/google/r/b/a/amo;->g:B

    move v0, v2

    .line 8194
    goto :goto_0

    :cond_2
    move v0, v2

    .line 8191
    goto :goto_1

    .line 8197
    :cond_3
    iput-byte v1, p0, Lcom/google/r/b/a/amo;->g:B

    move v0, v1

    .line 8198
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v1, 0xa

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 8221
    iget v0, p0, Lcom/google/r/b/a/amo;->h:I

    .line 8222
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 8243
    :goto_0
    return v0

    .line 8225
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 8226
    iget v0, p0, Lcom/google/r/b/a/amo;->b:I

    .line 8227
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 8229
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_6

    .line 8230
    iget-object v3, p0, Lcom/google/r/b/a/amo;->d:Lcom/google/n/ao;

    .line 8231
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    .line 8233
    :goto_3
    iget v0, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_1

    .line 8234
    const/4 v4, 0x3

    .line 8235
    iget-object v0, p0, Lcom/google/r/b/a/amo;->e:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/amo;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 8237
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/amo;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 8238
    iget v0, p0, Lcom/google/r/b/a/amo;->c:I

    .line 8239
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int v0, v2, v1

    add-int/2addr v3, v0

    .line 8241
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/amo;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 8242
    iput v0, p0, Lcom/google/r/b/a/amo;->h:I

    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 8227
    goto :goto_1

    .line 8235
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_6
    move v3, v0

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8004
    invoke-static {}, Lcom/google/r/b/a/amo;->newBuilder()Lcom/google/r/b/a/amq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/amq;->a(Lcom/google/r/b/a/amo;)Lcom/google/r/b/a/amq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8004
    invoke-static {}, Lcom/google/r/b/a/amo;->newBuilder()Lcom/google/r/b/a/amq;

    move-result-object v0

    return-object v0
.end method
