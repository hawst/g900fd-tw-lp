.class public final Lcom/google/r/b/a/bu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/co;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/bu;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/bu;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field g:Z

.field h:Lcom/google/n/ao;

.field i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1230
    new-instance v0, Lcom/google/r/b/a/bv;

    invoke-direct {v0}, Lcom/google/r/b/a/bv;-><init>()V

    sput-object v0, Lcom/google/r/b/a/bu;->PARSER:Lcom/google/n/ax;

    .line 3042
    new-instance v0, Lcom/google/r/b/a/bw;

    invoke-direct {v0}, Lcom/google/r/b/a/bw;-><init>()V

    .line 3073
    new-instance v0, Lcom/google/r/b/a/bx;

    invoke-direct {v0}, Lcom/google/r/b/a/bx;-><init>()V

    .line 3147
    new-instance v0, Lcom/google/r/b/a/by;

    invoke-direct {v0}, Lcom/google/r/b/a/by;-><init>()V

    .line 3321
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/bu;->m:Lcom/google/n/aw;

    .line 4061
    new-instance v0, Lcom/google/r/b/a/bu;

    invoke-direct {v0}, Lcom/google/r/b/a/bu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/bu;->j:Lcom/google/r/b/a/bu;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1038
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3191
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/bu;->h:Lcom/google/n/ao;

    .line 3221
    iput-byte v3, p0, Lcom/google/r/b/a/bu;->k:B

    .line 3261
    iput v3, p0, Lcom/google/r/b/a/bu;->l:I

    .line 1039
    iput v1, p0, Lcom/google/r/b/a/bu;->b:I

    .line 1040
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    .line 1041
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    .line 1042
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    .line 1043
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    .line 1044
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/r/b/a/bu;->g:Z

    .line 1045
    iget-object v0, p0, Lcom/google/r/b/a/bu;->h:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1046
    iput v1, p0, Lcom/google/r/b/a/bu;->i:I

    .line 1047
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v2, -0x1

    const/16 v10, 0x10

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/16 v7, 0x8

    .line 1053
    invoke-direct {p0}, Lcom/google/r/b/a/bu;-><init>()V

    .line 1054
    const/4 v1, 0x0

    .line 1056
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 1058
    const/4 v0, 0x0

    move v3, v0

    .line 1059
    :cond_0
    :goto_0
    if-nez v3, :cond_19

    .line 1060
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1061
    sparse-switch v0, :sswitch_data_0

    .line 1066
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1068
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 1063
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 1064
    goto :goto_0

    .line 1073
    :sswitch_1
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v7, :cond_1

    .line 1074
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    .line 1076
    or-int/lit8 v1, v1, 0x8

    .line 1078
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 1079
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 1078
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1209
    :catch_0
    move-exception v0

    .line 1210
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1215
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x8

    if-ne v2, v7, :cond_2

    .line 1216
    iget-object v2, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    .line 1218
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v8, :cond_3

    .line 1219
    iget-object v2, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    .line 1221
    :cond_3
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v9, :cond_4

    .line 1222
    iget-object v2, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    .line 1224
    :cond_4
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v10, :cond_5

    .line 1225
    iget-object v1, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    .line 1227
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/bu;->au:Lcom/google/n/bn;

    throw v0

    .line 1083
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/r/b/a/bu;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 1084
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/bu;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1211
    :catch_1
    move-exception v0

    .line 1212
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1213
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1088
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/bu;->a:I

    .line 1089
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/bu;->i:I

    goto/16 :goto_0

    .line 1093
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 1094
    invoke-static {v0}, Lcom/google/r/b/a/ca;->a(I)Lcom/google/r/b/a/ca;

    move-result-object v5

    .line 1095
    if-nez v5, :cond_6

    .line 1096
    const/4 v5, 0x6

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 1098
    :cond_6
    and-int/lit8 v5, v1, 0x2

    if-eq v5, v8, :cond_7

    .line 1099
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    .line 1100
    or-int/lit8 v1, v1, 0x2

    .line 1102
    :cond_7
    iget-object v5, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1107
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 1108
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 1109
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_8

    move v0, v2

    :goto_2
    if-lez v0, :cond_b

    .line 1110
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 1111
    invoke-static {v0}, Lcom/google/r/b/a/ca;->a(I)Lcom/google/r/b/a/ca;

    move-result-object v6

    .line 1112
    if-nez v6, :cond_9

    .line 1113
    const/4 v6, 0x6

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 1109
    :cond_8
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_2

    .line 1115
    :cond_9
    and-int/lit8 v6, v1, 0x2

    if-eq v6, v8, :cond_a

    .line 1116
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    .line 1117
    or-int/lit8 v1, v1, 0x2

    .line 1119
    :cond_a
    iget-object v6, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1122
    :cond_b
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 1126
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 1127
    invoke-static {v0}, Lcom/google/r/b/a/df;->a(I)Lcom/google/r/b/a/df;

    move-result-object v5

    .line 1128
    if-nez v5, :cond_c

    .line 1129
    const/4 v5, 0x7

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 1131
    :cond_c
    and-int/lit8 v5, v1, 0x4

    if-eq v5, v9, :cond_d

    .line 1132
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    .line 1133
    or-int/lit8 v1, v1, 0x4

    .line 1135
    :cond_d
    iget-object v5, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1140
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 1141
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 1142
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_e

    move v0, v2

    :goto_4
    if-lez v0, :cond_11

    .line 1143
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 1144
    invoke-static {v0}, Lcom/google/r/b/a/df;->a(I)Lcom/google/r/b/a/df;

    move-result-object v6

    .line 1145
    if-nez v6, :cond_f

    .line 1146
    const/4 v6, 0x7

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_3

    .line 1142
    :cond_e
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_4

    .line 1148
    :cond_f
    and-int/lit8 v6, v1, 0x4

    if-eq v6, v9, :cond_10

    .line 1149
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    .line 1150
    or-int/lit8 v1, v1, 0x4

    .line 1152
    :cond_10
    iget-object v6, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1155
    :cond_11
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 1159
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 1160
    invoke-static {v0}, Lcom/google/r/b/a/ck;->a(I)Lcom/google/r/b/a/ck;

    move-result-object v5

    .line 1161
    if-nez v5, :cond_12

    .line 1162
    const/16 v5, 0x8

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 1164
    :cond_12
    and-int/lit8 v5, v1, 0x10

    if-eq v5, v10, :cond_13

    .line 1165
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    .line 1166
    or-int/lit8 v1, v1, 0x10

    .line 1168
    :cond_13
    iget-object v5, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1173
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 1174
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 1175
    :goto_5
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_14

    move v0, v2

    :goto_6
    if-lez v0, :cond_17

    .line 1176
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 1177
    invoke-static {v0}, Lcom/google/r/b/a/ck;->a(I)Lcom/google/r/b/a/ck;

    move-result-object v6

    .line 1178
    if-nez v6, :cond_15

    .line 1179
    const/16 v6, 0x8

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 1175
    :cond_14
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_6

    .line 1181
    :cond_15
    and-int/lit8 v6, v1, 0x10

    if-eq v6, v10, :cond_16

    .line 1182
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    .line 1183
    or-int/lit8 v1, v1, 0x10

    .line 1185
    :cond_16
    iget-object v6, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1188
    :cond_17
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 1192
    :sswitch_a
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/bu;->a:I

    .line 1193
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/b/a/bu;->g:Z

    goto/16 :goto_0

    .line 1197
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 1198
    invoke-static {v0}, Lcom/google/r/b/a/cm;->a(I)Lcom/google/r/b/a/cm;

    move-result-object v5

    .line 1199
    if-nez v5, :cond_18

    .line 1200
    const/16 v5, 0xa

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 1202
    :cond_18
    iget v5, p0, Lcom/google/r/b/a/bu;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/bu;->a:I

    .line 1203
    iput v0, p0, Lcom/google/r/b/a/bu;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1215
    :cond_19
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_1a

    .line 1216
    iget-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    .line 1218
    :cond_1a
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v8, :cond_1b

    .line 1219
    iget-object v0, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    .line 1221
    :cond_1b
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v9, :cond_1c

    .line 1222
    iget-object v0, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    .line 1224
    :cond_1c
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v10, :cond_1d

    .line 1225
    iget-object v0, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    .line 1227
    :cond_1d
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/bu;->au:Lcom/google/n/bn;

    .line 1228
    return-void

    .line 1061
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
        0x28 -> :sswitch_3
        0x30 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x50 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1036
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3191
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/bu;->h:Lcom/google/n/ao;

    .line 3221
    iput-byte v1, p0, Lcom/google/r/b/a/bu;->k:B

    .line 3261
    iput v1, p0, Lcom/google/r/b/a/bu;->l:I

    .line 1037
    return-void
.end method

.method public static d()Lcom/google/r/b/a/bu;
    .locals 1

    .prologue
    .line 4064
    sget-object v0, Lcom/google/r/b/a/bu;->j:Lcom/google/r/b/a/bu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/bz;
    .locals 1

    .prologue
    .line 3383
    new-instance v0, Lcom/google/r/b/a/bz;

    invoke-direct {v0}, Lcom/google/r/b/a/bz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/bu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1242
    sget-object v0, Lcom/google/r/b/a/bu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x4

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 3233
    invoke-virtual {p0}, Lcom/google/r/b/a/bu;->c()I

    move v1, v2

    .line 3234
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 3235
    iget-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3237
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_1

    .line 3238
    iget-object v0, p0, Lcom/google/r/b/a/bu;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3240
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_2

    .line 3241
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/bu;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    :cond_2
    move v1, v2

    .line 3243
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3244
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 3243
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 3246
    :goto_2
    iget-object v0, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 3247
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 3246
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3249
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 3250
    iget-object v0, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 3249
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 3252
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 3253
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/r/b/a/bu;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 3255
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 3256
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/r/b/a/bu;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 3258
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/bu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3259
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3223
    iget-byte v1, p0, Lcom/google/r/b/a/bu;->k:B

    .line 3224
    if-ne v1, v0, :cond_0

    .line 3228
    :goto_0
    return v0

    .line 3225
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3227
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/bu;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 3263
    iget v0, p0, Lcom/google/r/b/a/bu;->l:I

    .line 3264
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3316
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 3267
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 3268
    iget-object v0, p0, Lcom/google/r/b/a/bu;->e:Ljava/util/List;

    .line 3269
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 3267
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 3271
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 3272
    iget-object v0, p0, Lcom/google/r/b/a/bu;->h:Lcom/google/n/ao;

    .line 3273
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 3275
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 3276
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/bu;->i:I

    .line 3277
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_4

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    :cond_3
    move v1, v2

    move v5, v2

    .line 3281
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 3282
    iget-object v0, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    .line 3283
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v5, v0

    .line 3281
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v4

    .line 3277
    goto :goto_2

    :cond_5
    move v0, v4

    .line 3283
    goto :goto_4

    .line 3285
    :cond_6
    add-int v0, v3, v5

    .line 3286
    iget-object v1, p0, Lcom/google/r/b/a/bu;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int v5, v0, v1

    move v1, v2

    move v3, v2

    .line 3290
    :goto_5
    iget-object v0, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 3291
    iget-object v0, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    .line 3292
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v3, v0

    .line 3290
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_7
    move v0, v4

    .line 3292
    goto :goto_6

    .line 3294
    :cond_8
    add-int v0, v5, v3

    .line 3295
    iget-object v1, p0, Lcom/google/r/b/a/bu;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int v5, v0, v1

    move v1, v2

    move v3, v2

    .line 3299
    :goto_7
    iget-object v0, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 3300
    iget-object v0, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    .line 3301
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v3, v0

    .line 3299
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_9
    move v0, v4

    .line 3301
    goto :goto_8

    .line 3303
    :cond_a
    add-int v0, v5, v3

    .line 3304
    iget-object v1, p0, Lcom/google/r/b/a/bu;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3306
    iget v1, p0, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_b

    .line 3307
    const/16 v1, 0x9

    iget-boolean v3, p0, Lcom/google/r/b/a/bu;->g:Z

    .line 3308
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3310
    :cond_b
    iget v1, p0, Lcom/google/r/b/a/bu;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v6, :cond_d

    .line 3311
    iget v1, p0, Lcom/google/r/b/a/bu;->b:I

    .line 3312
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_c

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_c
    add-int v1, v2, v4

    add-int/2addr v0, v1

    .line 3314
    :cond_d
    iget-object v1, p0, Lcom/google/r/b/a/bu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3315
    iput v0, p0, Lcom/google/r/b/a/bu;->l:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1030
    invoke-static {}, Lcom/google/r/b/a/bu;->newBuilder()Lcom/google/r/b/a/bz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/bz;->a(Lcom/google/r/b/a/bu;)Lcom/google/r/b/a/bz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1030
    invoke-static {}, Lcom/google/r/b/a/bu;->newBuilder()Lcom/google/r/b/a/bz;

    move-result-object v0

    return-object v0
.end method
