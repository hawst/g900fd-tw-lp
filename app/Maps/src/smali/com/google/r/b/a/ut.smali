.class public final Lcom/google/r/b/a/ut;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/uw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ut;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/r/b/a/ut;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/google/r/b/a/uu;

    invoke-direct {v0}, Lcom/google/r/b/a/uu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ut;->PARSER:Lcom/google/n/ax;

    .line 312
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/ut;->j:Lcom/google/n/aw;

    .line 793
    new-instance v0, Lcom/google/r/b/a/ut;

    invoke-direct {v0}, Lcom/google/r/b/a/ut;-><init>()V

    sput-object v0, Lcom/google/r/b/a/ut;->g:Lcom/google/r/b/a/ut;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 237
    iput-byte v0, p0, Lcom/google/r/b/a/ut;->h:B

    .line 274
    iput v0, p0, Lcom/google/r/b/a/ut;->i:I

    .line 18
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ut;->b:I

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ut;->c:I

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    .line 21
    const/16 v0, 0x16

    iput v0, p0, Lcom/google/r/b/a/ut;->e:I

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/16 v9, 0x10

    const/4 v8, 0x4

    .line 29
    invoke-direct {p0}, Lcom/google/r/b/a/ut;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v3, :cond_9

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 37
    sparse-switch v0, :sswitch_data_0

    .line 42
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 44
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/ut;->a:I

    .line 50
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ut;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x4

    if-ne v2, v8, :cond_1

    .line 103
    iget-object v2, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    .line 105
    :cond_1
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v9, :cond_2

    .line 106
    iget-object v1, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    .line 108
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ut;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_3

    .line 55
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    .line 56
    or-int/lit8 v1, v1, 0x4

    .line 58
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 98
    :catch_1
    move-exception v0

    .line 99
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 100
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 63
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 64
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_4

    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_5

    move v0, v2

    :goto_1
    if-lez v0, :cond_4

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    .line 66
    or-int/lit8 v1, v1, 0x4

    .line 68
    :cond_4
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v0, v7, :cond_6

    move v0, v2

    :goto_3
    if-lez v0, :cond_7

    .line 69
    iget-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 64
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 68
    :cond_6
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 71
    :cond_7
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 75
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ut;->a:I

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ut;->e:I

    goto/16 :goto_0

    .line 80
    :sswitch_5
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v9, :cond_8

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    .line 83
    or-int/lit8 v1, v1, 0x10

    .line 85
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 86
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 85
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 90
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/r/b/a/ut;->a:I

    .line 91
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/r/b/a/ut;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 102
    :cond_9
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v8, :cond_a

    .line 103
    iget-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    .line 105
    :cond_a
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v9, :cond_b

    .line 106
    iget-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    .line 108
    :cond_b
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ut;->au:Lcom/google/n/bn;

    .line 109
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 237
    iput-byte v0, p0, Lcom/google/r/b/a/ut;->h:B

    .line 274
    iput v0, p0, Lcom/google/r/b/a/ut;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/ut;
    .locals 1

    .prologue
    .line 796
    sget-object v0, Lcom/google/r/b/a/ut;->g:Lcom/google/r/b/a/ut;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/uv;
    .locals 1

    .prologue
    .line 374
    new-instance v0, Lcom/google/r/b/a/uv;

    invoke-direct {v0}, Lcom/google/r/b/a/uv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/ut;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/google/r/b/a/ut;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 255
    invoke-virtual {p0}, Lcom/google/r/b/a/ut;->c()I

    .line 256
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 257
    iget v0, p0, Lcom/google/r/b/a/ut;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    :cond_0
    move v1, v2

    .line 259
    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 259
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 262
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 263
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/r/b/a/ut;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 265
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 266
    iget-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 265
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 268
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_4

    .line 269
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/ut;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 271
    :cond_4
    iget-object v0, p0, Lcom/google/r/b/a/ut;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 272
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 239
    iget-byte v0, p0, Lcom/google/r/b/a/ut;->h:B

    .line 240
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 250
    :cond_0
    :goto_0
    return v2

    .line 241
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 243
    :goto_1
    iget-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 244
    iget-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/oa;->d()Lcom/google/r/b/a/oa;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/oa;

    invoke-virtual {v0}, Lcom/google/r/b/a/oa;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 245
    iput-byte v2, p0, Lcom/google/r/b/a/ut;->h:B

    goto :goto_0

    .line 243
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 249
    :cond_3
    iput-byte v3, p0, Lcom/google/r/b/a/ut;->h:B

    move v2, v3

    .line 250
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 276
    iget v0, p0, Lcom/google/r/b/a/ut;->i:I

    .line 277
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 307
    :goto_0
    return v0

    .line 280
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 281
    iget v0, p0, Lcom/google/r/b/a/ut;->b:I

    .line 282
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    :goto_2
    move v4, v3

    move v5, v3

    .line 286
    :goto_3
    iget-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 287
    iget-object v0, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    .line 288
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v5, v0

    .line 286
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 282
    goto :goto_1

    :cond_2
    move v0, v1

    .line 288
    goto :goto_4

    .line 290
    :cond_3
    add-int v0, v2, v5

    .line 291
    iget-object v2, p0, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 293
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_8

    .line 294
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/r/b/a/ut;->e:I

    .line 295
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v0, v2

    :goto_6
    move v2, v3

    move v4, v0

    .line 297
    :goto_7
    iget-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 298
    iget-object v0, p0, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    .line 299
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 297
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    :cond_4
    move v0, v1

    .line 295
    goto :goto_5

    .line 301
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/ut;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    .line 302
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/r/b/a/ut;->c:I

    .line 303
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v2, :cond_6

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 305
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/ut;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 306
    iput v0, p0, Lcom/google/r/b/a/ut;->i:I

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto :goto_6

    :cond_9
    move v2, v3

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ut;->newBuilder()Lcom/google/r/b/a/uv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/uv;->a(Lcom/google/r/b/a/ut;)Lcom/google/r/b/a/uv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/ut;->newBuilder()Lcom/google/r/b/a/uv;

    move-result-object v0

    return-object v0
.end method
