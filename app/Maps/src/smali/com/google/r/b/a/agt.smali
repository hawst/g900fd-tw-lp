.class public final Lcom/google/r/b/a/agt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/agz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/agt;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/maps/g/a/iy;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/agt;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field e:I

.field public f:I

.field g:Z

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2425
    new-instance v0, Lcom/google/r/b/a/agu;

    invoke-direct {v0}, Lcom/google/r/b/a/agu;-><init>()V

    sput-object v0, Lcom/google/r/b/a/agt;->PARSER:Lcom/google/n/ax;

    .line 2632
    new-instance v0, Lcom/google/r/b/a/agv;

    invoke-direct {v0}, Lcom/google/r/b/a/agv;-><init>()V

    sput-object v0, Lcom/google/r/b/a/agt;->i:Lcom/google/n/aj;

    .line 2743
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/agt;->m:Lcom/google/n/aw;

    .line 3211
    new-instance v0, Lcom/google/r/b/a/agt;

    invoke-direct {v0}, Lcom/google/r/b/a/agt;-><init>()V

    sput-object v0, Lcom/google/r/b/a/agt;->j:Lcom/google/r/b/a/agt;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 2291
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2660
    iput-byte v0, p0, Lcom/google/r/b/a/agt;->k:B

    .line 2697
    iput v0, p0, Lcom/google/r/b/a/agt;->l:I

    .line 2292
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/r/b/a/agt;->b:I

    .line 2293
    iput v3, p0, Lcom/google/r/b/a/agt;->c:I

    .line 2294
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/b/a/agt;->d:J

    .line 2295
    iput v2, p0, Lcom/google/r/b/a/agt;->e:I

    .line 2296
    iput v3, p0, Lcom/google/r/b/a/agt;->f:I

    .line 2297
    iput-boolean v2, p0, Lcom/google/r/b/a/agt;->g:Z

    .line 2298
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    .line 2299
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/16 v10, 0x40

    const/4 v2, 0x1

    .line 2305
    invoke-direct {p0}, Lcom/google/r/b/a/agt;-><init>()V

    .line 2308
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v0, v3

    .line 2311
    :cond_0
    :goto_0
    if-nez v4, :cond_b

    .line 2312
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 2313
    sparse-switch v1, :sswitch_data_0

    .line 2318
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    .line 2320
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 2316
    goto :goto_0

    .line 2325
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 2326
    invoke-static {v1}, Lcom/google/maps/g/a/hc;->a(I)Lcom/google/maps/g/a/hc;

    move-result-object v6

    .line 2327
    if-nez v6, :cond_2

    .line 2328
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2413
    :catch_0
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 2414
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2419
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x40

    if-ne v1, v10, :cond_1

    .line 2420
    iget-object v1, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    .line 2422
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/agt;->au:Lcom/google/n/bn;

    throw v0

    .line 2330
    :cond_2
    :try_start_2
    iget v6, p0, Lcom/google/r/b/a/agt;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/r/b/a/agt;->a:I

    .line 2331
    iput v1, p0, Lcom/google/r/b/a/agt;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2415
    :catch_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 2416
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 2417
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2336
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 2337
    invoke-static {v1}, Lcom/google/maps/g/a/fu;->a(I)Lcom/google/maps/g/a/fu;

    move-result-object v6

    .line 2338
    if-nez v6, :cond_3

    .line 2339
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 2419
    :catchall_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto :goto_2

    .line 2341
    :cond_3
    iget v6, p0, Lcom/google/r/b/a/agt;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/r/b/a/agt;->a:I

    .line 2342
    iput v1, p0, Lcom/google/r/b/a/agt;->c:I

    goto :goto_0

    .line 2347
    :sswitch_3
    iget v1, p0, Lcom/google/r/b/a/agt;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/r/b/a/agt;->a:I

    .line 2348
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/r/b/a/agt;->d:J

    goto :goto_0

    .line 2352
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 2353
    invoke-static {v1}, Lcom/google/r/b/a/agx;->a(I)Lcom/google/r/b/a/agx;

    move-result-object v6

    .line 2354
    if-nez v6, :cond_4

    .line 2355
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 2357
    :cond_4
    iget v6, p0, Lcom/google/r/b/a/agt;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/r/b/a/agt;->a:I

    .line 2358
    iput v1, p0, Lcom/google/r/b/a/agt;->e:I

    goto/16 :goto_0

    .line 2363
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 2364
    invoke-static {v1}, Lcom/google/maps/g/a/gs;->a(I)Lcom/google/maps/g/a/gs;

    move-result-object v6

    .line 2365
    if-nez v6, :cond_5

    .line 2366
    const/4 v6, 0x5

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 2368
    :cond_5
    iget v6, p0, Lcom/google/r/b/a/agt;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/r/b/a/agt;->a:I

    .line 2369
    iput v1, p0, Lcom/google/r/b/a/agt;->f:I

    goto/16 :goto_0

    .line 2374
    :sswitch_6
    iget v1, p0, Lcom/google/r/b/a/agt;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/r/b/a/agt;->a:I

    .line 2375
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_6

    move v1, v2

    :goto_4
    iput-boolean v1, p0, Lcom/google/r/b/a/agt;->g:Z

    goto/16 :goto_0

    :cond_6
    move v1, v3

    goto :goto_4

    .line 2379
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    .line 2380
    invoke-static {v6}, Lcom/google/maps/g/a/iy;->a(I)Lcom/google/maps/g/a/iy;

    move-result-object v1

    .line 2381
    if-nez v1, :cond_7

    .line 2382
    const/4 v1, 0x7

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 2384
    :cond_7
    and-int/lit8 v1, v0, 0x40

    if-eq v1, v10, :cond_e

    .line 2385
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2386
    or-int/lit8 v1, v0, 0x40

    .line 2388
    :goto_5
    :try_start_5
    iget-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 2390
    goto/16 :goto_0

    .line 2393
    :sswitch_8
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 2394
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 2395
    :goto_6
    iget v1, p1, Lcom/google/n/j;->f:I

    const v7, 0x7fffffff

    if-ne v1, v7, :cond_8

    const/4 v1, -0x1

    :goto_7
    if-lez v1, :cond_a

    .line 2396
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v7

    .line 2397
    invoke-static {v7}, Lcom/google/maps/g/a/iy;->a(I)Lcom/google/maps/g/a/iy;

    move-result-object v1

    .line 2398
    if-nez v1, :cond_9

    .line 2399
    const/4 v1, 0x7

    invoke-virtual {v5, v1, v7}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_6

    .line 2395
    :cond_8
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_7

    .line 2401
    :cond_9
    and-int/lit8 v1, v0, 0x40

    if-eq v1, v10, :cond_d

    .line 2402
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2403
    or-int/lit8 v1, v0, 0x40

    .line 2405
    :goto_8
    :try_start_7
    iget-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 2407
    goto :goto_6

    .line 2408
    :cond_a
    :try_start_8
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 2419
    :cond_b
    and-int/lit8 v0, v0, 0x40

    if-ne v0, v10, :cond_c

    .line 2420
    iget-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    .line 2422
    :cond_c
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/agt;->au:Lcom/google/n/bn;

    .line 2423
    return-void

    .line 2415
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 2413
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_d
    move v1, v0

    goto :goto_8

    :cond_e
    move v1, v0

    goto :goto_5

    .line 2313
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2289
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2660
    iput-byte v0, p0, Lcom/google/r/b/a/agt;->k:B

    .line 2697
    iput v0, p0, Lcom/google/r/b/a/agt;->l:I

    .line 2290
    return-void
.end method

.method public static a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;
    .locals 1

    .prologue
    .line 2808
    invoke-static {}, Lcom/google/r/b/a/agt;->newBuilder()Lcom/google/r/b/a/agw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/agw;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/r/b/a/agt;
    .locals 1

    .prologue
    .line 3214
    sget-object v0, Lcom/google/r/b/a/agt;->j:Lcom/google/r/b/a/agt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/agw;
    .locals 1

    .prologue
    .line 2805
    new-instance v0, Lcom/google/r/b/a/agw;

    invoke-direct {v0}, Lcom/google/r/b/a/agw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/agt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2437
    sget-object v0, Lcom/google/r/b/a/agt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2672
    invoke-virtual {p0}, Lcom/google/r/b/a/agt;->c()I

    .line 2673
    iget v0, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2674
    iget v0, p0, Lcom/google/r/b/a/agt;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 2676
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2677
    iget v0, p0, Lcom/google/r/b/a/agt;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 2679
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 2680
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/r/b/a/agt;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 2682
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2683
    iget v0, p0, Lcom/google/r/b/a/agt;->e:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(II)V

    .line 2685
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2686
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/b/a/agt;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 2688
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 2689
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/r/b/a/agt;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 2691
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2692
    const/4 v2, 0x7

    iget-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 2691
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2694
    :cond_6
    iget-object v0, p0, Lcom/google/r/b/a/agt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2695
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2662
    iget-byte v1, p0, Lcom/google/r/b/a/agt;->k:B

    .line 2663
    if-ne v1, v0, :cond_0

    .line 2667
    :goto_0
    return v0

    .line 2664
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2666
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/agt;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 2699
    iget v0, p0, Lcom/google/r/b/a/agt;->l:I

    .line 2700
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 2738
    :goto_0
    return v0

    .line 2703
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 2704
    iget v0, p0, Lcom/google/r/b/a/agt;->b:I

    .line 2705
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 2707
    :goto_2
    iget v3, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 2708
    iget v3, p0, Lcom/google/r/b/a/agt;->c:I

    .line 2709
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 2711
    :cond_1
    iget v3, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 2712
    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/google/r/b/a/agt;->d:J

    .line 2713
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 2715
    :cond_2
    iget v3, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 2716
    iget v3, p0, Lcom/google/r/b/a/agt;->e:I

    .line 2717
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 2719
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 2720
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/r/b/a/agt;->f:I

    .line 2721
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 2723
    :cond_4
    iget v3, p0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_b

    .line 2724
    const/4 v3, 0x6

    iget-boolean v4, p0, Lcom/google/r/b/a/agt;->g:Z

    .line 2725
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    move v3, v0

    :goto_6
    move v4, v2

    .line 2729
    :goto_7
    iget-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 2730
    iget-object v0, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    .line 2731
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v4

    .line 2729
    add-int/lit8 v2, v2, 0x1

    move v4, v0

    goto :goto_7

    :cond_5
    move v0, v1

    .line 2705
    goto/16 :goto_1

    :cond_6
    move v3, v1

    .line 2709
    goto/16 :goto_3

    :cond_7
    move v3, v1

    .line 2717
    goto :goto_4

    :cond_8
    move v3, v1

    .line 2721
    goto :goto_5

    :cond_9
    move v0, v1

    .line 2731
    goto :goto_8

    .line 2733
    :cond_a
    add-int v0, v3, v4

    .line 2734
    iget-object v1, p0, Lcom/google/r/b/a/agt;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2736
    iget-object v1, p0, Lcom/google/r/b/a/agt;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2737
    iput v0, p0, Lcom/google/r/b/a/agt;->l:I

    goto/16 :goto_0

    :cond_b
    move v3, v0

    goto :goto_6

    :cond_c
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2283
    invoke-static {}, Lcom/google/r/b/a/agt;->newBuilder()Lcom/google/r/b/a/agw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/agw;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2283
    invoke-static {}, Lcom/google/r/b/a/agt;->newBuilder()Lcom/google/r/b/a/agw;

    move-result-object v0

    return-object v0
.end method
