.class public final Lcom/google/r/b/a/ne;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/nh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/nc;",
        "Lcom/google/r/b/a/ne;",
        ">;",
        "Lcom/google/r/b/a/nh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 6120
    sget-object v0, Lcom/google/r/b/a/nc;->f:Lcom/google/r/b/a/nc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6199
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/ne;->b:I

    .line 6235
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ne;->c:Ljava/lang/Object;

    .line 6311
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/ne;->d:Ljava/lang/Object;

    .line 6388
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    .line 6121
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 6112
    new-instance v2, Lcom/google/r/b/a/nc;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/nc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/ne;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/ne;->b:I

    iput v1, v2, Lcom/google/r/b/a/nc;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/ne;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/nc;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/ne;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/nc;->d:Ljava/lang/Object;

    iget v1, p0, Lcom/google/r/b/a/ne;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/r/b/a/ne;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/r/b/a/ne;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    iput v0, v2, Lcom/google/r/b/a/nc;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 6112
    check-cast p1, Lcom/google/r/b/a/nc;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/ne;->a(Lcom/google/r/b/a/nc;)Lcom/google/r/b/a/ne;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/nc;)Lcom/google/r/b/a/ne;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6165
    invoke-static {}, Lcom/google/r/b/a/nc;->d()Lcom/google/r/b/a/nc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6190
    :goto_0
    return-object p0

    .line 6166
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 6167
    iget v2, p1, Lcom/google/r/b/a/nc;->b:I

    invoke-static {v2}, Lcom/google/r/b/a/nf;->a(I)Lcom/google/r/b/a/nf;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/r/b/a/nf;->a:Lcom/google/r/b/a/nf;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 6166
    goto :goto_1

    .line 6167
    :cond_3
    iget v3, p0, Lcom/google/r/b/a/ne;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/ne;->a:I

    iget v2, v2, Lcom/google/r/b/a/nf;->e:I

    iput v2, p0, Lcom/google/r/b/a/ne;->b:I

    .line 6169
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 6170
    iget v2, p0, Lcom/google/r/b/a/ne;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/ne;->a:I

    .line 6171
    iget-object v2, p1, Lcom/google/r/b/a/nc;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/ne;->c:Ljava/lang/Object;

    .line 6174
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/nc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    :goto_3
    if-eqz v0, :cond_6

    .line 6175
    iget v0, p0, Lcom/google/r/b/a/ne;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/ne;->a:I

    .line 6176
    iget-object v0, p1, Lcom/google/r/b/a/nc;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/ne;->d:Ljava/lang/Object;

    .line 6179
    :cond_6
    iget-object v0, p1, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 6180
    iget-object v0, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 6181
    iget-object v0, p1, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    .line 6182
    iget v0, p0, Lcom/google/r/b/a/ne;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/r/b/a/ne;->a:I

    .line 6189
    :cond_7
    :goto_4
    iget-object v0, p1, Lcom/google/r/b/a/nc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_8
    move v2, v1

    .line 6169
    goto :goto_2

    :cond_9
    move v0, v1

    .line 6174
    goto :goto_3

    .line 6184
    :cond_a
    iget v0, p0, Lcom/google/r/b/a/ne;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_b

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/ne;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/ne;->a:I

    .line 6185
    :cond_b
    iget-object v0, p0, Lcom/google/r/b/a/ne;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/r/b/a/nc;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 6194
    const/4 v0, 0x1

    return v0
.end method
