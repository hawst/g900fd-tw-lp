.class public final Lcom/google/r/b/a/mq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/mr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/mo;",
        "Lcom/google/r/b/a/mq;",
        ">;",
        "Lcom/google/r/b/a/mr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/r/b/a/ads;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 782
    sget-object v0, Lcom/google/r/b/a/mo;->c:Lcom/google/r/b/a/mo;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 828
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    .line 783
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 774
    new-instance v2, Lcom/google/r/b/a/mo;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/mo;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/mq;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    iput-object v1, v2, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    iput v0, v2, Lcom/google/r/b/a/mo;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 774
    check-cast p1, Lcom/google/r/b/a/mo;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/mq;->a(Lcom/google/r/b/a/mo;)Lcom/google/r/b/a/mq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/mo;)Lcom/google/r/b/a/mq;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 808
    invoke-static {}, Lcom/google/r/b/a/mo;->d()Lcom/google/r/b/a/mo;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 813
    :goto_0
    return-object p0

    .line 809
    :cond_0
    iget v0, p1, Lcom/google/r/b/a/mo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 810
    iget-object v0, p1, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v0

    :goto_2
    iget v2, p0, Lcom/google/r/b/a/mq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_4

    iget-object v1, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    invoke-static {v1}, Lcom/google/r/b/a/ads;->a(Lcom/google/r/b/a/ads;)Lcom/google/r/b/a/adu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/adu;->a(Lcom/google/r/b/a/ads;)Lcom/google/r/b/a/adu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/adu;->c()Lcom/google/r/b/a/ads;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    :goto_3
    iget v0, p0, Lcom/google/r/b/a/mq;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/mq;->a:I

    .line 812
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/mo;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 809
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 810
    :cond_3
    iget-object v0, p1, Lcom/google/r/b/a/mo;->b:Lcom/google/r/b/a/ads;

    goto :goto_2

    :cond_4
    iput-object v0, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    goto :goto_3
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 817
    iget v0, p0, Lcom/google/r/b/a/mq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 818
    iget-object v0, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 823
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 817
    goto :goto_0

    .line 818
    :cond_1
    iget-object v0, p0, Lcom/google/r/b/a/mq;->b:Lcom/google/r/b/a/ads;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 823
    goto :goto_2
.end method
