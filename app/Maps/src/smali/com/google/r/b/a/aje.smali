.class public final Lcom/google/r/b/a/aje;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ajj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/r/b/a/aje;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field c:I

.field public d:I

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field public h:I

.field public i:Lcom/google/n/ao;

.field public j:Lcom/google/n/ao;

.field public k:Lcom/google/n/ao;

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1591
    new-instance v0, Lcom/google/r/b/a/ajf;

    invoke-direct {v0}, Lcom/google/r/b/a/ajf;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aje;->PARSER:Lcom/google/n/ax;

    .line 2028
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/aje;->o:Lcom/google/n/aw;

    .line 2839
    new-instance v0, Lcom/google/r/b/a/aje;

    invoke-direct {v0}, Lcom/google/r/b/a/aje;-><init>()V

    sput-object v0, Lcom/google/r/b/a/aje;->l:Lcom/google/r/b/a/aje;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1472
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1882
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    .line 1898
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    .line 1914
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    .line 1929
    iput-byte v3, p0, Lcom/google/r/b/a/aje;->m:B

    .line 1975
    iput v3, p0, Lcom/google/r/b/a/aje;->n:I

    .line 1473
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    .line 1474
    iput v2, p0, Lcom/google/r/b/a/aje;->c:I

    .line 1475
    iput v2, p0, Lcom/google/r/b/a/aje;->d:I

    .line 1476
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aje;->e:Ljava/lang/Object;

    .line 1477
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aje;->f:Ljava/lang/Object;

    .line 1478
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    .line 1479
    iput v2, p0, Lcom/google/r/b/a/aje;->h:I

    .line 1480
    iget-object v0, p0, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 1481
    iget-object v0, p0, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 1482
    iget-object v0, p0, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 1483
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1489
    invoke-direct {p0}, Lcom/google/r/b/a/aje;-><init>()V

    .line 1490
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1495
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 1496
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1497
    sparse-switch v3, :sswitch_data_0

    .line 1502
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1504
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1500
    goto :goto_0

    .line 1509
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1510
    iget v4, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/r/b/a/aje;->a:I

    .line 1511
    iput-object v3, p0, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1582
    :catch_0
    move-exception v0

    .line 1583
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1588
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/aje;->au:Lcom/google/n/bn;

    throw v0

    .line 1515
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1516
    invoke-static {v3}, Lcom/google/maps/g/f/i;->a(I)Lcom/google/maps/g/f/i;

    move-result-object v4

    .line 1517
    if-nez v4, :cond_1

    .line 1518
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1584
    :catch_1
    move-exception v0

    .line 1585
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1586
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1520
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/r/b/a/aje;->a:I

    .line 1521
    iput v3, p0, Lcom/google/r/b/a/aje;->c:I

    goto :goto_0

    .line 1526
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1527
    iget v4, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/r/b/a/aje;->a:I

    .line 1528
    iput-object v3, p0, Lcom/google/r/b/a/aje;->e:Ljava/lang/Object;

    goto :goto_0

    .line 1532
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1533
    iget v4, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/r/b/a/aje;->a:I

    .line 1534
    iput-object v3, p0, Lcom/google/r/b/a/aje;->f:Ljava/lang/Object;

    goto :goto_0

    .line 1538
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1539
    iget v4, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/r/b/a/aje;->a:I

    .line 1540
    iput-object v3, p0, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    goto :goto_0

    .line 1544
    :sswitch_6
    iget-object v3, p0, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1545
    iget v3, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/aje;->a:I

    goto/16 :goto_0

    .line 1549
    :sswitch_7
    iget-object v3, p0, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1550
    iget v3, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/aje;->a:I

    goto/16 :goto_0

    .line 1554
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1555
    invoke-static {v3}, Lcom/google/r/b/a/aiy;->a(I)Lcom/google/r/b/a/aiy;

    move-result-object v4

    .line 1556
    if-nez v4, :cond_2

    .line 1557
    const/16 v4, 0x8

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 1559
    :cond_2
    iget v4, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/r/b/a/aje;->a:I

    .line 1560
    iput v3, p0, Lcom/google/r/b/a/aje;->d:I

    goto/16 :goto_0

    .line 1565
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 1566
    invoke-static {v3}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v4

    .line 1567
    if-nez v4, :cond_3

    .line 1568
    const/16 v4, 0x9

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 1570
    :cond_3
    iget v4, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/r/b/a/aje;->a:I

    .line 1571
    iput v3, p0, Lcom/google/r/b/a/aje;->h:I

    goto/16 :goto_0

    .line 1576
    :sswitch_a
    iget-object v3, p0, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1577
    iget v3, p0, Lcom/google/r/b/a/aje;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/r/b/a/aje;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1588
    :cond_4
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->au:Lcom/google/n/bn;

    .line 1589
    return-void

    .line 1497
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1470
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1882
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    .line 1898
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    .line 1914
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    .line 1929
    iput-byte v1, p0, Lcom/google/r/b/a/aje;->m:B

    .line 1975
    iput v1, p0, Lcom/google/r/b/a/aje;->n:I

    .line 1471
    return-void
.end method

.method public static h()Lcom/google/r/b/a/aje;
    .locals 1

    .prologue
    .line 2842
    sget-object v0, Lcom/google/r/b/a/aje;->l:Lcom/google/r/b/a/aje;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/ajg;
    .locals 1

    .prologue
    .line 2090
    new-instance v0, Lcom/google/r/b/a/ajg;

    invoke-direct {v0}, Lcom/google/r/b/a/ajg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1603
    sget-object v0, Lcom/google/r/b/a/aje;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1941
    invoke-virtual {p0}, Lcom/google/r/b/a/aje;->c()I

    .line 1942
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1943
    iget-object v0, p0, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1945
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1946
    iget v0, p0, Lcom/google/r/b/a/aje;->c:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 1948
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_2

    .line 1949
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/r/b/a/aje;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1951
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 1952
    iget-object v0, p0, Lcom/google/r/b/a/aje;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->f:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1954
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 1955
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1957
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 1958
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1960
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_6

    .line 1961
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1963
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_7

    .line 1964
    iget v0, p0, Lcom/google/r/b/a/aje;->d:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 1966
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 1967
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/r/b/a/aje;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 1969
    :cond_8
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 1970
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1972
    :cond_9
    iget-object v0, p0, Lcom/google/r/b/a/aje;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1973
    return-void

    .line 1943
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 1949
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 1952
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 1955
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1931
    iget-byte v1, p0, Lcom/google/r/b/a/aje;->m:B

    .line 1932
    if-ne v1, v0, :cond_0

    .line 1936
    :goto_0
    return v0

    .line 1933
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1935
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/aje;->m:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1977
    iget v0, p0, Lcom/google/r/b/a/aje;->n:I

    .line 1978
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2023
    :goto_0
    return v0

    .line 1981
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_11

    .line 1983
    iget-object v0, p0, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1985
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_10

    .line 1986
    iget v2, p0, Lcom/google/r/b/a/aje;->c:I

    .line 1987
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_a

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 1989
    :goto_4
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_1

    .line 1990
    const/4 v4, 0x3

    .line 1991
    iget-object v0, p0, Lcom/google/r/b/a/aje;->e:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1993
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_2

    .line 1995
    iget-object v0, p0, Lcom/google/r/b/a/aje;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1997
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_3

    .line 1998
    const/4 v4, 0x5

    .line 1999
    iget-object v0, p0, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    :goto_7
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 2001
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_4

    .line 2002
    const/4 v0, 0x6

    iget-object v4, p0, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    .line 2003
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 2005
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_5

    .line 2006
    const/4 v0, 0x7

    iget-object v4, p0, Lcom/google/r/b/a/aje;->j:Lcom/google/n/ao;

    .line 2007
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 2009
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_6

    .line 2010
    const/16 v0, 0x8

    iget v4, p0, Lcom/google/r/b/a/aje;->d:I

    .line 2011
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 2013
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_7

    .line 2014
    const/16 v0, 0x9

    iget v4, p0, Lcom/google/r/b/a/aje;->h:I

    .line 2015
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 2017
    :cond_7
    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_8

    .line 2018
    iget-object v0, p0, Lcom/google/r/b/a/aje;->k:Lcom/google/n/ao;

    .line 2019
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 2021
    :cond_8
    iget-object v0, p0, Lcom/google/r/b/a/aje;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2022
    iput v0, p0, Lcom/google/r/b/a/aje;->n:I

    goto/16 :goto_0

    .line 1983
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_a
    move v2, v3

    .line 1987
    goto/16 :goto_3

    .line 1991
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 1995
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 1999
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    :cond_e
    move v0, v3

    .line 2011
    goto :goto_8

    :cond_f
    move v0, v3

    .line 2015
    goto :goto_9

    :cond_10
    move v2, v0

    goto/16 :goto_4

    :cond_11
    move v0, v1

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1677
    iget-object v0, p0, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    .line 1678
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1679
    check-cast v0, Ljava/lang/String;

    .line 1687
    :goto_0
    return-object v0

    .line 1681
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1683
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1684
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1685
    iput-object v1, p0, Lcom/google/r/b/a/aje;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1687
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1464
    invoke-static {}, Lcom/google/r/b/a/aje;->newBuilder()Lcom/google/r/b/a/ajg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/ajg;->a(Lcom/google/r/b/a/aje;)Lcom/google/r/b/a/ajg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1464
    invoke-static {}, Lcom/google/r/b/a/aje;->newBuilder()Lcom/google/r/b/a/ajg;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1835
    iget-object v0, p0, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    .line 1836
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1837
    check-cast v0, Ljava/lang/String;

    .line 1845
    :goto_0
    return-object v0

    .line 1839
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1841
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1842
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1843
    iput-object v1, p0, Lcom/google/r/b/a/aje;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1845
    goto :goto_0
.end method
