.class public final Lcom/google/r/b/a/aqi;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/aql;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/aqg;",
        "Lcom/google/r/b/a/aqi;",
        ">;",
        "Lcom/google/r/b/a/aql;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:I

.field private e:I

.field private f:Z

.field private g:I

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Ljava/lang/Object;

.field private r:Ljava/lang/Object;

.field private s:Z


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x2760

    const/4 v1, 0x1

    .line 804
    sget-object v0, Lcom/google/r/b/a/aqg;->t:Lcom/google/r/b/a/aqg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1001
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/r/b/a/aqi;->b:I

    .line 1033
    iput-boolean v1, p0, Lcom/google/r/b/a/aqi;->c:Z

    .line 1065
    const/16 v0, 0xe

    iput v0, p0, Lcom/google/r/b/a/aqi;->d:I

    .line 1097
    iput v2, p0, Lcom/google/r/b/a/aqi;->e:I

    .line 1225
    iput-boolean v1, p0, Lcom/google/r/b/a/aqi;->i:Z

    .line 1321
    const/16 v0, 0x78

    iput v0, p0, Lcom/google/r/b/a/aqi;->l:I

    .line 1353
    iput v2, p0, Lcom/google/r/b/a/aqi;->m:I

    .line 1385
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/r/b/a/aqi;->n:I

    .line 1485
    const-string v0, "https://www.google.com/maps/vt/"

    iput-object v0, p0, Lcom/google/r/b/a/aqi;->q:Ljava/lang/Object;

    .line 1561
    const-string v0, "https://www.google.com/maps/vt/"

    iput-object v0, p0, Lcom/google/r/b/a/aqi;->r:Ljava/lang/Object;

    .line 1637
    iput-boolean v1, p0, Lcom/google/r/b/a/aqi;->s:Z

    .line 805
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    .line 796
    new-instance v2, Lcom/google/r/b/a/aqg;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/aqg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_11

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/aqi;->b:I

    iput v1, v2, Lcom/google/r/b/a/aqg;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/r/b/a/aqi;->c:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aqg;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/aqi;->d:I

    iput v1, v2, Lcom/google/r/b/a/aqg;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/r/b/a/aqi;->e:I

    iput v1, v2, Lcom/google/r/b/a/aqg;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/r/b/a/aqi;->f:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aqg;->f:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/r/b/a/aqi;->g:I

    iput v1, v2, Lcom/google/r/b/a/aqg;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/r/b/a/aqi;->h:I

    iput v1, v2, Lcom/google/r/b/a/aqg;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v1, p0, Lcom/google/r/b/a/aqi;->i:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aqg;->i:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v1, p0, Lcom/google/r/b/a/aqi;->j:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aqg;->j:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-boolean v1, p0, Lcom/google/r/b/a/aqi;->k:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aqg;->k:Z

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget v1, p0, Lcom/google/r/b/a/aqi;->l:I

    iput v1, v2, Lcom/google/r/b/a/aqg;->l:I

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget v1, p0, Lcom/google/r/b/a/aqi;->m:I

    iput v1, v2, Lcom/google/r/b/a/aqg;->m:I

    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget v1, p0, Lcom/google/r/b/a/aqi;->n:I

    iput v1, v2, Lcom/google/r/b/a/aqg;->n:I

    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget-boolean v1, p0, Lcom/google/r/b/a/aqi;->o:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aqg;->o:Z

    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    or-int/lit16 v0, v0, 0x4000

    :cond_d
    iget-boolean v1, p0, Lcom/google/r/b/a/aqi;->p:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aqg;->p:Z

    and-int v1, v3, v5

    if-ne v1, v5, :cond_e

    or-int/2addr v0, v5

    :cond_e
    iget-object v1, p0, Lcom/google/r/b/a/aqi;->q:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    and-int v1, v3, v6

    if-ne v1, v6, :cond_f

    or-int/2addr v0, v6

    :cond_f
    iget-object v1, p0, Lcom/google/r/b/a/aqi;->r:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/aqg;->r:Ljava/lang/Object;

    and-int v1, v3, v7

    if-ne v1, v7, :cond_10

    or-int/2addr v0, v7

    :cond_10
    iget-boolean v1, p0, Lcom/google/r/b/a/aqi;->s:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/aqg;->s:Z

    iput v0, v2, Lcom/google/r/b/a/aqg;->a:I

    return-object v2

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 796
    check-cast p1, Lcom/google/r/b/a/aqg;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aqi;->a(Lcom/google/r/b/a/aqg;)Lcom/google/r/b/a/aqi;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/aqg;)Lcom/google/r/b/a/aqi;
    .locals 7

    .prologue
    const/high16 v6, 0x20000

    const/high16 v5, 0x10000

    const v4, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 932
    invoke-static {}, Lcom/google/r/b/a/aqg;->d()Lcom/google/r/b/a/aqg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 992
    :goto_0
    return-object p0

    .line 933
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_e

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 934
    iget v2, p1, Lcom/google/r/b/a/aqg;->b:I

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput v2, p0, Lcom/google/r/b/a/aqi;->b:I

    .line 936
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 937
    iget-boolean v2, p1, Lcom/google/r/b/a/aqg;->c:Z

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aqi;->c:Z

    .line 939
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 940
    iget v2, p1, Lcom/google/r/b/a/aqg;->d:I

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput v2, p0, Lcom/google/r/b/a/aqi;->d:I

    .line 942
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 943
    iget v2, p1, Lcom/google/r/b/a/aqg;->e:I

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput v2, p0, Lcom/google/r/b/a/aqi;->e:I

    .line 945
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 946
    iget-boolean v2, p1, Lcom/google/r/b/a/aqg;->f:Z

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aqi;->f:Z

    .line 948
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 949
    iget v2, p1, Lcom/google/r/b/a/aqg;->g:I

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput v2, p0, Lcom/google/r/b/a/aqi;->g:I

    .line 951
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 952
    iget v2, p1, Lcom/google/r/b/a/aqg;->h:I

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput v2, p0, Lcom/google/r/b/a/aqi;->h:I

    .line 954
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 955
    iget-boolean v2, p1, Lcom/google/r/b/a/aqg;->i:Z

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aqi;->i:Z

    .line 957
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 958
    iget-boolean v2, p1, Lcom/google/r/b/a/aqg;->j:Z

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aqi;->j:Z

    .line 960
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 961
    iget-boolean v2, p1, Lcom/google/r/b/a/aqg;->k:Z

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aqi;->k:Z

    .line 963
    :cond_a
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 964
    iget v2, p1, Lcom/google/r/b/a/aqg;->l:I

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput v2, p0, Lcom/google/r/b/a/aqi;->l:I

    .line 966
    :cond_b
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 967
    iget v2, p1, Lcom/google/r/b/a/aqg;->m:I

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput v2, p0, Lcom/google/r/b/a/aqi;->m:I

    .line 969
    :cond_c
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_d
    if-eqz v2, :cond_1c

    .line 970
    iget v2, p1, Lcom/google/r/b/a/aqg;->n:I

    invoke-static {v2}, Lcom/google/r/b/a/aqj;->a(I)Lcom/google/r/b/a/aqj;

    move-result-object v2

    if-nez v2, :cond_d

    sget-object v2, Lcom/google/r/b/a/aqj;->a:Lcom/google/r/b/a/aqj;

    :cond_d
    if-nez v2, :cond_1b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v1

    .line 933
    goto/16 :goto_1

    :cond_f
    move v2, v1

    .line 936
    goto/16 :goto_2

    :cond_10
    move v2, v1

    .line 939
    goto/16 :goto_3

    :cond_11
    move v2, v1

    .line 942
    goto/16 :goto_4

    :cond_12
    move v2, v1

    .line 945
    goto/16 :goto_5

    :cond_13
    move v2, v1

    .line 948
    goto/16 :goto_6

    :cond_14
    move v2, v1

    .line 951
    goto/16 :goto_7

    :cond_15
    move v2, v1

    .line 954
    goto/16 :goto_8

    :cond_16
    move v2, v1

    .line 957
    goto/16 :goto_9

    :cond_17
    move v2, v1

    .line 960
    goto :goto_a

    :cond_18
    move v2, v1

    .line 963
    goto :goto_b

    :cond_19
    move v2, v1

    .line 966
    goto :goto_c

    :cond_1a
    move v2, v1

    .line 969
    goto :goto_d

    .line 970
    :cond_1b
    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iget v2, v2, Lcom/google/r/b/a/aqj;->c:I

    iput v2, p0, Lcom/google/r/b/a/aqi;->n:I

    .line 972
    :cond_1c
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_e
    if-eqz v2, :cond_1d

    .line 973
    iget-boolean v2, p1, Lcom/google/r/b/a/aqg;->o:Z

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aqi;->o:Z

    .line 975
    :cond_1d
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_23

    move v2, v0

    :goto_f
    if-eqz v2, :cond_1e

    .line 976
    iget-boolean v2, p1, Lcom/google/r/b/a/aqg;->p:Z

    iget v3, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/r/b/a/aqi;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aqi;->p:Z

    .line 978
    :cond_1e
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_24

    move v2, v0

    :goto_10
    if-eqz v2, :cond_1f

    .line 979
    iget v2, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/2addr v2, v4

    iput v2, p0, Lcom/google/r/b/a/aqi;->a:I

    .line 980
    iget-object v2, p1, Lcom/google/r/b/a/aqg;->q:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aqi;->q:Ljava/lang/Object;

    .line 983
    :cond_1f
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_25

    move v2, v0

    :goto_11
    if-eqz v2, :cond_20

    .line 984
    iget v2, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/2addr v2, v5

    iput v2, p0, Lcom/google/r/b/a/aqi;->a:I

    .line 985
    iget-object v2, p1, Lcom/google/r/b/a/aqg;->r:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aqi;->r:Ljava/lang/Object;

    .line 988
    :cond_20
    iget v2, p1, Lcom/google/r/b/a/aqg;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_26

    :goto_12
    if-eqz v0, :cond_21

    .line 989
    iget-boolean v0, p1, Lcom/google/r/b/a/aqg;->s:Z

    iget v1, p0, Lcom/google/r/b/a/aqi;->a:I

    or-int/2addr v1, v6

    iput v1, p0, Lcom/google/r/b/a/aqi;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/aqi;->s:Z

    .line 991
    :cond_21
    iget-object v0, p1, Lcom/google/r/b/a/aqg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_22
    move v2, v1

    .line 972
    goto :goto_e

    :cond_23
    move v2, v1

    .line 975
    goto :goto_f

    :cond_24
    move v2, v1

    .line 978
    goto :goto_10

    :cond_25
    move v2, v1

    .line 983
    goto :goto_11

    :cond_26
    move v0, v1

    .line 988
    goto :goto_12
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 996
    const/4 v0, 0x1

    return v0
.end method
