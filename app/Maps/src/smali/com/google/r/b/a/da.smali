.class public final enum Lcom/google/r/b/a/da;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/da;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/da;

.field public static final enum b:Lcom/google/r/b/a/da;

.field private static final synthetic d:[Lcom/google/r/b/a/da;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5642
    new-instance v0, Lcom/google/r/b/a/da;

    const-string v1, "LIST"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/da;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/da;->a:Lcom/google/r/b/a/da;

    .line 5646
    new-instance v0, Lcom/google/r/b/a/da;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/da;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/da;->b:Lcom/google/r/b/a/da;

    .line 5637
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/r/b/a/da;

    sget-object v1, Lcom/google/r/b/a/da;->a:Lcom/google/r/b/a/da;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/da;->b:Lcom/google/r/b/a/da;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/r/b/a/da;->d:[Lcom/google/r/b/a/da;

    .line 5676
    new-instance v0, Lcom/google/r/b/a/db;

    invoke-direct {v0}, Lcom/google/r/b/a/db;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 5685
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5686
    iput p3, p0, Lcom/google/r/b/a/da;->c:I

    .line 5687
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/da;
    .locals 1

    .prologue
    .line 5664
    packed-switch p0, :pswitch_data_0

    .line 5667
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 5665
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/da;->a:Lcom/google/r/b/a/da;

    goto :goto_0

    .line 5666
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/da;->b:Lcom/google/r/b/a/da;

    goto :goto_0

    .line 5664
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/da;
    .locals 1

    .prologue
    .line 5637
    const-class v0, Lcom/google/r/b/a/da;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/da;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/da;
    .locals 1

    .prologue
    .line 5637
    sget-object v0, Lcom/google/r/b/a/da;->d:[Lcom/google/r/b/a/da;

    invoke-virtual {v0}, [Lcom/google/r/b/a/da;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/da;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 5660
    iget v0, p0, Lcom/google/r/b/a/da;->c:I

    return v0
.end method
