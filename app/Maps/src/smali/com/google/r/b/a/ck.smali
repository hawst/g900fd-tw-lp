.class public final enum Lcom/google/r/b/a/ck;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ck;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ck;

.field public static final enum b:Lcom/google/r/b/a/ck;

.field public static final enum c:Lcom/google/r/b/a/ck;

.field public static final enum d:Lcom/google/r/b/a/ck;

.field public static final enum e:Lcom/google/r/b/a/ck;

.field private static final synthetic g:[Lcom/google/r/b/a/ck;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1508
    new-instance v0, Lcom/google/r/b/a/ck;

    const-string v1, "COMBINE_ACTIONS_WITH_PREVIOUS_ITEM"

    invoke-direct {v0, v1, v7, v3}, Lcom/google/r/b/a/ck;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ck;->a:Lcom/google/r/b/a/ck;

    .line 1512
    new-instance v0, Lcom/google/r/b/a/ck;

    const-string v1, "REMOVE_PRECEDING_DIVIDER_LINE"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/r/b/a/ck;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ck;->b:Lcom/google/r/b/a/ck;

    .line 1516
    new-instance v0, Lcom/google/r/b/a/ck;

    const-string v1, "REMOVE_FOLLOWING_DIVIDER_LINE"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/r/b/a/ck;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ck;->c:Lcom/google/r/b/a/ck;

    .line 1520
    new-instance v0, Lcom/google/r/b/a/ck;

    const-string v1, "TITLE_CAPITALIZATION_STYLE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/ck;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ck;->d:Lcom/google/r/b/a/ck;

    .line 1524
    new-instance v0, Lcom/google/r/b/a/ck;

    const-string v1, "FIFE_IMAGE_URL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/google/r/b/a/ck;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ck;->e:Lcom/google/r/b/a/ck;

    .line 1503
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/r/b/a/ck;

    sget-object v1, Lcom/google/r/b/a/ck;->a:Lcom/google/r/b/a/ck;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/r/b/a/ck;->b:Lcom/google/r/b/a/ck;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/ck;->c:Lcom/google/r/b/a/ck;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/ck;->d:Lcom/google/r/b/a/ck;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/r/b/a/ck;->e:Lcom/google/r/b/a/ck;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/r/b/a/ck;->g:[Lcom/google/r/b/a/ck;

    .line 1569
    new-instance v0, Lcom/google/r/b/a/cl;

    invoke-direct {v0}, Lcom/google/r/b/a/cl;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1578
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1579
    iput p3, p0, Lcom/google/r/b/a/ck;->f:I

    .line 1580
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ck;
    .locals 1

    .prologue
    .line 1554
    packed-switch p0, :pswitch_data_0

    .line 1560
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1555
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/ck;->a:Lcom/google/r/b/a/ck;

    goto :goto_0

    .line 1556
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ck;->b:Lcom/google/r/b/a/ck;

    goto :goto_0

    .line 1557
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/ck;->c:Lcom/google/r/b/a/ck;

    goto :goto_0

    .line 1558
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/ck;->d:Lcom/google/r/b/a/ck;

    goto :goto_0

    .line 1559
    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/ck;->e:Lcom/google/r/b/a/ck;

    goto :goto_0

    .line 1554
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ck;
    .locals 1

    .prologue
    .line 1503
    const-class v0, Lcom/google/r/b/a/ck;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ck;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ck;
    .locals 1

    .prologue
    .line 1503
    sget-object v0, Lcom/google/r/b/a/ck;->g:[Lcom/google/r/b/a/ck;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ck;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ck;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/google/r/b/a/ck;->f:I

    return v0
.end method
