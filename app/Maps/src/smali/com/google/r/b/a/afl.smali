.class public final Lcom/google/r/b/a/afl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/afm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/afj;",
        "Lcom/google/r/b/a/afl;",
        ">;",
        "Lcom/google/r/b/a/afm;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Ljava/lang/Object;

.field public e:Z

.field public f:Lcom/google/n/ao;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Lcom/google/n/ao;

.field public k:Z

.field private l:Lcom/google/n/ao;

.field private m:I

.field private n:Lcom/google/n/ao;

.field private o:I

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 8130
    sget-object v0, Lcom/google/r/b/a/afj;->q:Lcom/google/r/b/a/afj;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 8333
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    .line 8469
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/r/b/a/afl;->c:I

    .line 8505
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afl;->l:Lcom/google/n/ao;

    .line 8564
    iput v1, p0, Lcom/google/r/b/a/afl;->m:I

    .line 8600
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afl;->n:Lcom/google/n/ao;

    .line 8659
    iput v1, p0, Lcom/google/r/b/a/afl;->o:I

    .line 8696
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    .line 8832
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/afl;->d:Ljava/lang/Object;

    .line 8940
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afl;->f:Lcom/google/n/ao;

    .line 9095
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/r/b/a/afl;->j:Lcom/google/n/ao;

    .line 8131
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8122
    new-instance v2, Lcom/google/r/b/a/afj;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/afj;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/afl;->a:I

    iget v4, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/r/b/a/afl;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_e

    :goto_0
    iget v4, p0, Lcom/google/r/b/a/afl;->c:I

    iput v4, v2, Lcom/google/r/b/a/afj;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, v2, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/afl;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/afl;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/r/b/a/afl;->m:I

    iput v4, v2, Lcom/google/r/b/a/afj;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, v2, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/afl;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/afl;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget v4, p0, Lcom/google/r/b/a/afl;->o:I

    iput v4, v2, Lcom/google/r/b/a/afj;->g:I

    iget v4, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    iget v4, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/r/b/a/afl;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    iput-object v4, v2, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v4, p0, Lcom/google/r/b/a/afl;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget-boolean v4, p0, Lcom/google/r/b/a/afl;->e:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afj;->j:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-object v4, v2, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/afl;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/afl;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x100

    :cond_9
    iget-boolean v4, p0, Lcom/google/r/b/a/afl;->g:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afj;->l:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x200

    :cond_a
    iget-boolean v4, p0, Lcom/google/r/b/a/afl;->h:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afj;->m:Z

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x400

    :cond_b
    iget-boolean v4, p0, Lcom/google/r/b/a/afl;->i:Z

    iput-boolean v4, v2, Lcom/google/r/b/a/afj;->n:Z

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x800

    :cond_c
    iget-object v4, v2, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/r/b/a/afl;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/r/b/a/afl;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_d

    or-int/lit16 v0, v0, 0x1000

    :cond_d
    iget-boolean v1, p0, Lcom/google/r/b/a/afl;->k:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/afj;->p:Z

    iput v0, v2, Lcom/google/r/b/a/afj;->a:I

    return-object v2

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 8122
    check-cast p1, Lcom/google/r/b/a/afj;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/afl;->a(Lcom/google/r/b/a/afj;)Lcom/google/r/b/a/afl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/maps/a/a;)Lcom/google/r/b/a/afl;
    .locals 2

    .prologue
    .line 8520
    if-nez p1, :cond_0

    .line 8521
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8523
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/afl;->l:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 8525
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8526
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/a/al;)Lcom/google/r/b/a/afl;
    .locals 1

    .prologue
    .line 8677
    if-nez p1, :cond_0

    .line 8678
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8680
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8681
    iget v0, p1, Lcom/google/maps/g/a/al;->e:I

    iput v0, p0, Lcom/google/r/b/a/afl;->o:I

    .line 8683
    return-object p0
.end method

.method public final a(Lcom/google/maps/g/a/dx;)Lcom/google/r/b/a/afl;
    .locals 1

    .prologue
    .line 8582
    if-nez p1, :cond_0

    .line 8583
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8585
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8586
    iget v0, p1, Lcom/google/maps/g/a/dx;->e:I

    iput v0, p0, Lcom/google/r/b/a/afl;->m:I

    .line 8588
    return-object p0
.end method

.method public final a(Lcom/google/r/b/a/afj;)Lcom/google/r/b/a/afl;
    .locals 5

    .prologue
    const/16 v4, 0x40

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 8250
    invoke-static {}, Lcom/google/r/b/a/afj;->g()Lcom/google/r/b/a/afj;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 8317
    :goto_0
    return-object p0

    .line 8251
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 8252
    iget-object v0, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8253
    iget-object v0, p1, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    .line 8254
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8261
    :cond_1
    :goto_1
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    .line 8262
    iget v0, p1, Lcom/google/r/b/a/afj;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/hm;->f:Lcom/google/maps/g/a/hm;

    :cond_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8256
    :cond_3
    invoke-virtual {p0}, Lcom/google/r/b/a/afl;->c()V

    .line 8257
    iget-object v0, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/afj;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_4
    move v0, v2

    .line 8261
    goto :goto_2

    .line 8262
    :cond_5
    iget v3, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/afl;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hm;->h:I

    iput v0, p0, Lcom/google/r/b/a/afl;->c:I

    .line 8264
    :cond_6
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_16

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 8265
    iget-object v0, p0, Lcom/google/r/b/a/afl;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afj;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 8266
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8268
    :cond_7
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_17

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 8269
    iget v0, p1, Lcom/google/r/b/a/afj;->e:I

    invoke-static {v0}, Lcom/google/maps/g/a/dx;->a(I)Lcom/google/maps/g/a/dx;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/maps/g/a/dx;->b:Lcom/google/maps/g/a/dx;

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/r/b/a/afl;->a(Lcom/google/maps/g/a/dx;)Lcom/google/r/b/a/afl;

    .line 8271
    :cond_9
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_18

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    .line 8272
    iget-object v0, p0, Lcom/google/r/b/a/afl;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afj;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 8273
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8275
    :cond_a
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_19

    move v0, v1

    :goto_6
    if-eqz v0, :cond_c

    .line 8276
    iget v0, p1, Lcom/google/r/b/a/afj;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_b
    invoke-virtual {p0, v0}, Lcom/google/r/b/a/afl;->a(Lcom/google/maps/g/a/al;)Lcom/google/r/b/a/afl;

    .line 8278
    :cond_c
    iget-object v0, p1, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 8279
    iget-object v0, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 8280
    iget-object v0, p1, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    iput-object v0, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    .line 8281
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8288
    :cond_d
    :goto_7
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_1c

    move v0, v1

    :goto_8
    if-eqz v0, :cond_e

    .line 8289
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8290
    iget-object v0, p1, Lcom/google/r/b/a/afj;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/afl;->d:Ljava/lang/Object;

    .line 8293
    :cond_e
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit8 v0, v0, 0x40

    if-ne v0, v4, :cond_1d

    move v0, v1

    :goto_9
    if-eqz v0, :cond_f

    .line 8294
    iget-boolean v0, p1, Lcom/google/r/b/a/afj;->j:Z

    iget v3, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/afl;->e:Z

    .line 8296
    :cond_f
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_1e

    move v0, v1

    :goto_a
    if-eqz v0, :cond_10

    .line 8297
    iget-object v0, p0, Lcom/google/r/b/a/afl;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afj;->k:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 8298
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8300
    :cond_10
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_1f

    move v0, v1

    :goto_b
    if-eqz v0, :cond_11

    .line 8301
    iget-boolean v0, p1, Lcom/google/r/b/a/afj;->l:Z

    iget v3, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/afl;->g:Z

    .line 8303
    :cond_11
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_20

    move v0, v1

    :goto_c
    if-eqz v0, :cond_12

    .line 8304
    iget-boolean v0, p1, Lcom/google/r/b/a/afj;->m:Z

    iget v3, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/afl;->h:Z

    .line 8306
    :cond_12
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_21

    move v0, v1

    :goto_d
    if-eqz v0, :cond_13

    .line 8307
    iget-boolean v0, p1, Lcom/google/r/b/a/afj;->n:Z

    iget v3, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/afl;->i:Z

    .line 8309
    :cond_13
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_22

    move v0, v1

    :goto_e
    if-eqz v0, :cond_14

    .line 8310
    iget-object v0, p0, Lcom/google/r/b/a/afl;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/r/b/a/afj;->o:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 8311
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8313
    :cond_14
    iget v0, p1, Lcom/google/r/b/a/afj;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_23

    move v0, v1

    :goto_f
    if-eqz v0, :cond_15

    .line 8314
    iget-boolean v0, p1, Lcom/google/r/b/a/afj;->p:Z

    iget v1, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lcom/google/r/b/a/afl;->a:I

    iput-boolean v0, p0, Lcom/google/r/b/a/afl;->k:Z

    .line 8316
    :cond_15
    iget-object v0, p1, Lcom/google/r/b/a/afj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_16
    move v0, v2

    .line 8264
    goto/16 :goto_3

    :cond_17
    move v0, v2

    .line 8268
    goto/16 :goto_4

    :cond_18
    move v0, v2

    .line 8271
    goto/16 :goto_5

    :cond_19
    move v0, v2

    .line 8275
    goto/16 :goto_6

    .line 8283
    :cond_1a
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit8 v0, v0, 0x40

    if-eq v0, v4, :cond_1b

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8284
    :cond_1b
    iget-object v0, p0, Lcom/google/r/b/a/afl;->p:Ljava/util/List;

    iget-object v3, p1, Lcom/google/r/b/a/afj;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    :cond_1c
    move v0, v2

    .line 8288
    goto/16 :goto_8

    :cond_1d
    move v0, v2

    .line 8293
    goto/16 :goto_9

    :cond_1e
    move v0, v2

    .line 8296
    goto/16 :goto_a

    :cond_1f
    move v0, v2

    .line 8300
    goto/16 :goto_b

    :cond_20
    move v0, v2

    .line 8303
    goto/16 :goto_c

    :cond_21
    move v0, v2

    .line 8306
    goto :goto_d

    :cond_22
    move v0, v2

    .line 8309
    goto :goto_e

    :cond_23
    move v0, v2

    .line 8313
    goto :goto_f
.end method

.method public final a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afl;
    .locals 2

    .prologue
    .line 8615
    if-nez p1, :cond_0

    .line 8616
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8618
    :cond_0
    iget-object v0, p0, Lcom/google/r/b/a/afl;->n:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 8620
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8621
    return-object p0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 8321
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 8322
    iget-object v0, p0, Lcom/google/r/b/a/afl;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v0}, Lcom/google/maps/g/hy;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 8327
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 8321
    goto :goto_0

    :cond_1
    move v0, v2

    .line 8327
    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 8335
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 8336
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/r/b/a/afl;->b:Ljava/util/List;

    .line 8339
    iget v0, p0, Lcom/google/r/b/a/afl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/r/b/a/afl;->a:I

    .line 8341
    :cond_0
    return-void
.end method
