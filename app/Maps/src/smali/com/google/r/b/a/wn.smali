.class public final Lcom/google/r/b/a/wn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ws;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/wn;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/r/b/a/wn;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:I

.field d:Z

.field e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/google/r/b/a/wo;

    invoke-direct {v0}, Lcom/google/r/b/a/wo;-><init>()V

    sput-object v0, Lcom/google/r/b/a/wn;->PARSER:Lcom/google/n/ax;

    .line 434
    const/4 v0, 0x0

    sput-object v0, Lcom/google/r/b/a/wn;->m:Lcom/google/n/aw;

    .line 927
    new-instance v0, Lcom/google/r/b/a/wn;

    invoke-direct {v0}, Lcom/google/r/b/a/wn;-><init>()V

    sput-object v0, Lcom/google/r/b/a/wn;->j:Lcom/google/r/b/a/wn;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 349
    iput-byte v0, p0, Lcom/google/r/b/a/wn;->k:B

    .line 389
    iput v0, p0, Lcom/google/r/b/a/wn;->l:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/wn;->b:Ljava/lang/Object;

    .line 19
    iput v1, p0, Lcom/google/r/b/a/wn;->c:I

    .line 20
    iput-boolean v1, p0, Lcom/google/r/b/a/wn;->d:Z

    .line 21
    iput-boolean v1, p0, Lcom/google/r/b/a/wn;->e:Z

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/r/b/a/wn;->f:Z

    .line 23
    iput-boolean v1, p0, Lcom/google/r/b/a/wn;->g:Z

    .line 24
    iput-boolean v1, p0, Lcom/google/r/b/a/wn;->h:Z

    .line 25
    iput-boolean v1, p0, Lcom/google/r/b/a/wn;->i:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Lcom/google/r/b/a/wn;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 38
    :cond_0
    :goto_0
    if-nez v3, :cond_8

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 53
    iget v5, p0, Lcom/google/r/b/a/wn;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/r/b/a/wn;->a:I

    .line 54
    iput-object v0, p0, Lcom/google/r/b/a/wn;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/r/b/a/wn;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 59
    invoke-static {v0}, Lcom/google/r/b/a/wq;->a(I)Lcom/google/r/b/a/wq;

    move-result-object v5

    .line 60
    if-nez v5, :cond_1

    .line 61
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 102
    :catch_1
    move-exception v0

    .line 103
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 104
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :cond_1
    :try_start_4
    iget v5, p0, Lcom/google/r/b/a/wn;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/r/b/a/wn;->a:I

    .line 64
    iput v0, p0, Lcom/google/r/b/a/wn;->c:I

    goto :goto_0

    .line 69
    :sswitch_3
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/wn;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/r/b/a/wn;->d:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 74
    :sswitch_4
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/wn;->a:I

    .line 75
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/r/b/a/wn;->e:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 79
    :sswitch_5
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/r/b/a/wn;->a:I

    .line 80
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/r/b/a/wn;->f:Z

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_3

    .line 84
    :sswitch_6
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/r/b/a/wn;->a:I

    .line 85
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/r/b/a/wn;->g:Z

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_4

    .line 89
    :sswitch_7
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/r/b/a/wn;->a:I

    .line 90
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_6

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/r/b/a/wn;->i:Z

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_5

    .line 94
    :sswitch_8
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/r/b/a/wn;->a:I

    .line 95
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_7

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/r/b/a/wn;->h:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_6

    .line 106
    :cond_8
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wn;->au:Lcom/google/n/bn;

    .line 107
    return-void

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 349
    iput-byte v0, p0, Lcom/google/r/b/a/wn;->k:B

    .line 389
    iput v0, p0, Lcom/google/r/b/a/wn;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/r/b/a/wn;
    .locals 1

    .prologue
    .line 930
    sget-object v0, Lcom/google/r/b/a/wn;->j:Lcom/google/r/b/a/wn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/r/b/a/wp;
    .locals 1

    .prologue
    .line 496
    new-instance v0, Lcom/google/r/b/a/wp;

    invoke-direct {v0}, Lcom/google/r/b/a/wp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/r/b/a/wn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    sget-object v0, Lcom/google/r/b/a/wn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 361
    invoke-virtual {p0}, Lcom/google/r/b/a/wn;->c()I

    .line 362
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/r/b/a/wn;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wn;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 365
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 366
    iget v0, p0, Lcom/google/r/b/a/wn;->c:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 368
    :cond_1
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 369
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/r/b/a/wn;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 371
    :cond_2
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 372
    iget-boolean v0, p0, Lcom/google/r/b/a/wn;->e:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 374
    :cond_3
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 375
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/r/b/a/wn;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 377
    :cond_4
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 378
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/r/b/a/wn;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 380
    :cond_5
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 381
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/r/b/a/wn;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 383
    :cond_6
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 384
    iget-boolean v0, p0, Lcom/google/r/b/a/wn;->h:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(IZ)V

    .line 386
    :cond_7
    iget-object v0, p0, Lcom/google/r/b/a/wn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 387
    return-void

    .line 363
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 351
    iget-byte v1, p0, Lcom/google/r/b/a/wn;->k:B

    .line 352
    if-ne v1, v0, :cond_0

    .line 356
    :goto_0
    return v0

    .line 353
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 355
    :cond_1
    iput-byte v0, p0, Lcom/google/r/b/a/wn;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 391
    iget v0, p0, Lcom/google/r/b/a/wn;->l:I

    .line 392
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 429
    :goto_0
    return v0

    .line 395
    :cond_0
    iget v0, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 397
    iget-object v0, p0, Lcom/google/r/b/a/wn;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/b/a/wn;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 399
    :goto_2
    iget v2, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 400
    iget v2, p0, Lcom/google/r/b/a/wn;->c:I

    .line 401
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 403
    :cond_1
    iget v2, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 404
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/r/b/a/wn;->d:Z

    .line 405
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 407
    :cond_2
    iget v2, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 408
    iget-boolean v2, p0, Lcom/google/r/b/a/wn;->e:Z

    .line 409
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 411
    :cond_3
    iget v2, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 412
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/r/b/a/wn;->f:Z

    .line 413
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 415
    :cond_4
    iget v2, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 416
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/r/b/a/wn;->g:Z

    .line 417
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 419
    :cond_5
    iget v2, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_6

    .line 420
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/r/b/a/wn;->i:Z

    .line 421
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 423
    :cond_6
    iget v2, p0, Lcom/google/r/b/a/wn;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    .line 424
    iget-boolean v2, p0, Lcom/google/r/b/a/wn;->h:Z

    .line 425
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 427
    :cond_7
    iget-object v1, p0, Lcom/google/r/b/a/wn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 428
    iput v0, p0, Lcom/google/r/b/a/wn;->l:I

    goto/16 :goto_0

    .line 397
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 401
    :cond_9
    const/16 v2, 0xa

    goto/16 :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/wn;->newBuilder()Lcom/google/r/b/a/wp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/r/b/a/wp;->a(Lcom/google/r/b/a/wn;)Lcom/google/r/b/a/wp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/r/b/a/wn;->newBuilder()Lcom/google/r/b/a/wp;

    move-result-object v0

    return-object v0
.end method
