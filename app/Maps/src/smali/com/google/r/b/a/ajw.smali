.class public final enum Lcom/google/r/b/a/ajw;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/r/b/a/ajw;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/r/b/a/ajw;

.field public static final enum b:Lcom/google/r/b/a/ajw;

.field public static final enum c:Lcom/google/r/b/a/ajw;

.field public static final enum d:Lcom/google/r/b/a/ajw;

.field private static final synthetic f:[Lcom/google/r/b/a/ajw;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 670
    new-instance v0, Lcom/google/r/b/a/ajw;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/r/b/a/ajw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ajw;->a:Lcom/google/r/b/a/ajw;

    .line 674
    new-instance v0, Lcom/google/r/b/a/ajw;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/r/b/a/ajw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ajw;->b:Lcom/google/r/b/a/ajw;

    .line 678
    new-instance v0, Lcom/google/r/b/a/ajw;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/r/b/a/ajw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ajw;->c:Lcom/google/r/b/a/ajw;

    .line 682
    new-instance v0, Lcom/google/r/b/a/ajw;

    const-string v1, "REQUEST_ERROR"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/r/b/a/ajw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/r/b/a/ajw;->d:Lcom/google/r/b/a/ajw;

    .line 665
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/r/b/a/ajw;

    sget-object v1, Lcom/google/r/b/a/ajw;->a:Lcom/google/r/b/a/ajw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/r/b/a/ajw;->b:Lcom/google/r/b/a/ajw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/r/b/a/ajw;->c:Lcom/google/r/b/a/ajw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/r/b/a/ajw;->d:Lcom/google/r/b/a/ajw;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/r/b/a/ajw;->f:[Lcom/google/r/b/a/ajw;

    .line 722
    new-instance v0, Lcom/google/r/b/a/ajx;

    invoke-direct {v0}, Lcom/google/r/b/a/ajx;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 731
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 732
    iput p3, p0, Lcom/google/r/b/a/ajw;->e:I

    .line 733
    return-void
.end method

.method public static a(I)Lcom/google/r/b/a/ajw;
    .locals 1

    .prologue
    .line 708
    packed-switch p0, :pswitch_data_0

    .line 713
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 709
    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/ajw;->a:Lcom/google/r/b/a/ajw;

    goto :goto_0

    .line 710
    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/ajw;->b:Lcom/google/r/b/a/ajw;

    goto :goto_0

    .line 711
    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/ajw;->c:Lcom/google/r/b/a/ajw;

    goto :goto_0

    .line 712
    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/ajw;->d:Lcom/google/r/b/a/ajw;

    goto :goto_0

    .line 708
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/r/b/a/ajw;
    .locals 1

    .prologue
    .line 665
    const-class v0, Lcom/google/r/b/a/ajw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ajw;

    return-object v0
.end method

.method public static values()[Lcom/google/r/b/a/ajw;
    .locals 1

    .prologue
    .line 665
    sget-object v0, Lcom/google/r/b/a/ajw;->f:[Lcom/google/r/b/a/ajw;

    invoke-virtual {v0}, [Lcom/google/r/b/a/ajw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/r/b/a/ajw;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 704
    iget v0, p0, Lcom/google/r/b/a/ajw;->e:I

    return v0
.end method
