.class public final Lcom/google/r/b/a/aj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/ah;",
        "Lcom/google/r/b/a/aj;",
        ">;",
        "Lcom/google/r/b/a/ak;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field public e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 468
    sget-object v0, Lcom/google/r/b/a/ah;->f:Lcom/google/r/b/a/ah;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 543
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aj;->b:Ljava/lang/Object;

    .line 619
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aj;->c:Ljava/lang/Object;

    .line 695
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aj;->d:Ljava/lang/Object;

    .line 771
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aj;->e:Ljava/lang/Object;

    .line 469
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 460
    new-instance v2, Lcom/google/r/b/a/ah;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/ah;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aj;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/r/b/a/aj;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ah;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/r/b/a/aj;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ah;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/r/b/a/aj;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ah;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/r/b/a/aj;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/ah;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/r/b/a/ah;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 460
    check-cast p1, Lcom/google/r/b/a/ah;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aj;->a(Lcom/google/r/b/a/ah;)Lcom/google/r/b/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/ah;)Lcom/google/r/b/a/aj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 512
    invoke-static {}, Lcom/google/r/b/a/ah;->d()Lcom/google/r/b/a/ah;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 534
    :goto_0
    return-object p0

    .line 513
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/ah;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 514
    iget v2, p0, Lcom/google/r/b/a/aj;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/r/b/a/aj;->a:I

    .line 515
    iget-object v2, p1, Lcom/google/r/b/a/ah;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aj;->b:Ljava/lang/Object;

    .line 518
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/ah;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 519
    iget v2, p0, Lcom/google/r/b/a/aj;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/r/b/a/aj;->a:I

    .line 520
    iget-object v2, p1, Lcom/google/r/b/a/ah;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aj;->c:Ljava/lang/Object;

    .line 523
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/ah;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 524
    iget v2, p0, Lcom/google/r/b/a/aj;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/r/b/a/aj;->a:I

    .line 525
    iget-object v2, p1, Lcom/google/r/b/a/ah;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aj;->d:Ljava/lang/Object;

    .line 528
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/ah;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 529
    iget v0, p0, Lcom/google/r/b/a/aj;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/r/b/a/aj;->a:I

    .line 530
    iget-object v0, p1, Lcom/google/r/b/a/ah;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/r/b/a/aj;->e:Ljava/lang/Object;

    .line 533
    :cond_4
    iget-object v0, p1, Lcom/google/r/b/a/ah;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 513
    goto :goto_1

    :cond_6
    move v2, v1

    .line 518
    goto :goto_2

    :cond_7
    move v2, v1

    .line 523
    goto :goto_3

    :cond_8
    move v0, v1

    .line 528
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x1

    return v0
.end method
