.class public final Lcom/google/r/b/a/aps;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/r/b/a/apv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/r/b/a/apq;",
        "Lcom/google/r/b/a/aps;",
        ">;",
        "Lcom/google/r/b/a/apv;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:I

.field private e:Z

.field private f:I

.field private g:I

.field private h:Z

.field private i:Ljava/lang/Object;

.field private j:Z

.field private k:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 588
    sget-object v0, Lcom/google/r/b/a/apq;->l:Lcom/google/r/b/a/apq;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 711
    const/16 v0, 0x1000

    iput v0, p0, Lcom/google/r/b/a/aps;->b:I

    .line 775
    const/16 v0, 0x19

    iput v0, p0, Lcom/google/r/b/a/aps;->d:I

    .line 839
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/r/b/a/aps;->f:I

    .line 871
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/r/b/a/aps;->g:I

    .line 935
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/b/a/aps;->i:Ljava/lang/Object;

    .line 1043
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/r/b/a/aps;->k:I

    .line 589
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 580
    new-instance v2, Lcom/google/r/b/a/apq;

    invoke-direct {v2, p0}, Lcom/google/r/b/a/apq;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget v1, p0, Lcom/google/r/b/a/aps;->b:I

    iput v1, v2, Lcom/google/r/b/a/apq;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/r/b/a/aps;->c:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/apq;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/r/b/a/aps;->d:I

    iput v1, v2, Lcom/google/r/b/a/apq;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/r/b/a/aps;->e:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/apq;->e:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/r/b/a/aps;->f:I

    iput v1, v2, Lcom/google/r/b/a/apq;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/r/b/a/aps;->g:I

    iput v1, v2, Lcom/google/r/b/a/apq;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v1, p0, Lcom/google/r/b/a/aps;->h:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/apq;->h:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/r/b/a/aps;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v1, p0, Lcom/google/r/b/a/aps;->j:Z

    iput-boolean v1, v2, Lcom/google/r/b/a/apq;->j:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v1, p0, Lcom/google/r/b/a/aps;->k:I

    iput v1, v2, Lcom/google/r/b/a/apq;->k:I

    iput v0, v2, Lcom/google/r/b/a/apq;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 580
    check-cast p1, Lcom/google/r/b/a/apq;

    invoke-virtual {p0, p1}, Lcom/google/r/b/a/aps;->a(Lcom/google/r/b/a/apq;)Lcom/google/r/b/a/aps;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/apq;)Lcom/google/r/b/a/aps;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 668
    invoke-static {}, Lcom/google/r/b/a/apq;->d()Lcom/google/r/b/a/apq;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 702
    :goto_0
    return-object p0

    .line 669
    :cond_0
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 670
    iget v2, p1, Lcom/google/r/b/a/apq;->b:I

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/r/b/a/aps;->a:I

    iput v2, p0, Lcom/google/r/b/a/aps;->b:I

    .line 672
    :cond_1
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 673
    iget-boolean v2, p1, Lcom/google/r/b/a/apq;->c:Z

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/r/b/a/aps;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aps;->c:Z

    .line 675
    :cond_2
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 676
    iget v2, p1, Lcom/google/r/b/a/apq;->d:I

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/r/b/a/aps;->a:I

    iput v2, p0, Lcom/google/r/b/a/aps;->d:I

    .line 678
    :cond_3
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 679
    iget-boolean v2, p1, Lcom/google/r/b/a/apq;->e:Z

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/r/b/a/aps;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aps;->e:Z

    .line 681
    :cond_4
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 682
    iget v2, p1, Lcom/google/r/b/a/apq;->f:I

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/r/b/a/aps;->a:I

    iput v2, p0, Lcom/google/r/b/a/aps;->f:I

    .line 684
    :cond_5
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 685
    iget v2, p1, Lcom/google/r/b/a/apq;->g:I

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/r/b/a/aps;->a:I

    iput v2, p0, Lcom/google/r/b/a/aps;->g:I

    .line 687
    :cond_6
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 688
    iget-boolean v2, p1, Lcom/google/r/b/a/apq;->h:Z

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/r/b/a/aps;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aps;->h:Z

    .line 690
    :cond_7
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 691
    iget v2, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/r/b/a/aps;->a:I

    .line 692
    iget-object v2, p1, Lcom/google/r/b/a/apq;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/r/b/a/aps;->i:Ljava/lang/Object;

    .line 695
    :cond_8
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 696
    iget-boolean v2, p1, Lcom/google/r/b/a/apq;->j:Z

    iget v3, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/r/b/a/aps;->a:I

    iput-boolean v2, p0, Lcom/google/r/b/a/aps;->j:Z

    .line 698
    :cond_9
    iget v2, p1, Lcom/google/r/b/a/apq;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_14

    :goto_a
    if-eqz v0, :cond_16

    .line 699
    iget v0, p1, Lcom/google/r/b/a/apq;->k:I

    invoke-static {v0}, Lcom/google/r/b/a/apt;->a(I)Lcom/google/r/b/a/apt;

    move-result-object v0

    if-nez v0, :cond_a

    sget-object v0, Lcom/google/r/b/a/apt;->e:Lcom/google/r/b/a/apt;

    :cond_a
    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    move v2, v1

    .line 669
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 672
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 675
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 678
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 681
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 684
    goto :goto_6

    :cond_11
    move v2, v1

    .line 687
    goto :goto_7

    :cond_12
    move v2, v1

    .line 690
    goto :goto_8

    :cond_13
    move v2, v1

    .line 695
    goto :goto_9

    :cond_14
    move v0, v1

    .line 698
    goto :goto_a

    .line 699
    :cond_15
    iget v1, p0, Lcom/google/r/b/a/aps;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/r/b/a/aps;->a:I

    iget v0, v0, Lcom/google/r/b/a/apt;->g:I

    iput v0, p0, Lcom/google/r/b/a/aps;->k:I

    .line 701
    :cond_16
    iget-object v0, p1, Lcom/google/r/b/a/apq;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 706
    const/4 v0, 0x1

    return v0
.end method
