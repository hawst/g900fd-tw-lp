.class final Lc/x;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:[B

.field b:I

.field c:I

.field d:Lc/x;

.field e:Lc/x;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/16 v0, 0x800

    new-array v0, v0, [B

    iput-object v0, p0, Lc/x;->a:[B

    return-void
.end method


# virtual methods
.method public final a()Lc/x;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Lc/x;->d:Lc/x;

    if-eq v0, p0, :cond_0

    iget-object v0, p0, Lc/x;->d:Lc/x;

    .line 54
    :goto_0
    iget-object v2, p0, Lc/x;->e:Lc/x;

    iget-object v3, p0, Lc/x;->d:Lc/x;

    iput-object v3, v2, Lc/x;->d:Lc/x;

    .line 55
    iget-object v2, p0, Lc/x;->d:Lc/x;

    iget-object v3, p0, Lc/x;->e:Lc/x;

    iput-object v3, v2, Lc/x;->e:Lc/x;

    .line 56
    iput-object v1, p0, Lc/x;->d:Lc/x;

    .line 57
    iput-object v1, p0, Lc/x;->e:Lc/x;

    .line 58
    return-object v0

    :cond_0
    move-object v0, v1

    .line 53
    goto :goto_0
.end method

.method public final a(Lc/x;I)V
    .locals 6

    .prologue
    const/16 v2, 0x800

    const/4 v5, 0x0

    .line 122
    iget v0, p1, Lc/x;->c:I

    iget v1, p1, Lc/x;->b:I

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    if-le v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 124
    :cond_0
    iget v0, p1, Lc/x;->c:I

    add-int/2addr v0, p2

    if-le v0, v2, :cond_1

    .line 126
    iget-object v0, p1, Lc/x;->a:[B

    iget v1, p1, Lc/x;->b:I

    iget-object v2, p1, Lc/x;->a:[B

    iget v3, p1, Lc/x;->c:I

    iget v4, p1, Lc/x;->b:I

    sub-int/2addr v3, v4

    invoke-static {v0, v1, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    iget v0, p1, Lc/x;->c:I

    iget v1, p1, Lc/x;->b:I

    sub-int/2addr v0, v1

    iput v0, p1, Lc/x;->c:I

    .line 128
    iput v5, p1, Lc/x;->b:I

    .line 131
    :cond_1
    iget-object v0, p0, Lc/x;->a:[B

    iget v1, p0, Lc/x;->b:I

    iget-object v2, p1, Lc/x;->a:[B

    iget v3, p1, Lc/x;->c:I

    invoke-static {v0, v1, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    iget v0, p1, Lc/x;->c:I

    add-int/2addr v0, p2

    iput v0, p1, Lc/x;->c:I

    .line 133
    iget v0, p0, Lc/x;->b:I

    add-int/2addr v0, p2

    iput v0, p0, Lc/x;->b:I

    .line 134
    return-void
.end method
