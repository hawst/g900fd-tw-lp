.class final Lc/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lc/i;


# instance fields
.field public final a:Lc/f;

.field public final b:Lc/z;

.field c:Z


# direct methods
.method public constructor <init>(Lc/z;)V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lc/f;

    invoke-direct {v0}, Lc/f;-><init>()V

    invoke-direct {p0, p1, v0}, Lc/t;-><init>(Lc/z;Lc/f;)V

    .line 35
    return-void
.end method

.method private constructor <init>(Lc/z;Lc/f;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p2, p0, Lc/t;->a:Lc/f;

    .line 30
    iput-object p1, p0, Lc/t;->b:Lc/z;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Lc/aa;)J
    .locals 6

    .prologue
    .line 79
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    const-wide/16 v0, 0x0

    .line 81
    :goto_0
    iget-object v2, p0, Lc/t;->a:Lc/f;

    const-wide/16 v4, 0x800

    invoke-interface {p1, v2, v4, v5}, Lc/aa;->b(Lc/f;J)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 82
    add-long/2addr v0, v2

    .line 83
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    goto :goto_0

    .line 85
    :cond_1
    return-wide v0
.end method

.method public final a()Lc/ab;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lc/t;->b:Lc/z;

    invoke-interface {v0}, Lc/z;->a()Lc/ab;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lc/k;)Lc/i;
    .locals 4

    .prologue
    .line 49
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "byteString == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p1, Lc/k;->b:[B

    const/4 v2, 0x0

    iget-object v3, p1, Lc/k;->b:[B

    array-length v3, v3

    invoke-virtual {v0, v1, v2, v3}, Lc/f;->b([BII)Lc/f;

    .line 51
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    move-result-object v0

    return-object v0
.end method

.method public final a([B)Lc/i;
    .locals 3

    .prologue
    .line 67
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Lc/f;->b([BII)Lc/f;

    .line 69
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lc/f;J)V
    .locals 2

    .prologue
    .line 43
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    invoke-virtual {v0, p1, p2, p3}, Lc/f;->a(Lc/f;J)V

    .line 45
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    .line 46
    return-void
.end method

.method public final b(Ljava/lang/String;)Lc/i;
    .locals 2

    .prologue
    .line 55
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    invoke-virtual {v0, p1}, Lc/f;->a(Ljava/lang/String;)Lc/f;

    .line 57
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 169
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    iget-wide v0, v0, Lc/f;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 171
    iget-object v0, p0, Lc/t;->b:Lc/z;

    iget-object v1, p0, Lc/t;->a:Lc/f;

    iget-object v2, p0, Lc/t;->a:Lc/f;

    iget-wide v2, v2, Lc/f;->b:J

    invoke-interface {v0, v1, v2, v3}, Lc/z;->a(Lc/f;J)V

    .line 173
    :cond_1
    iget-object v0, p0, Lc/t;->b:Lc/z;

    invoke-interface {v0}, Lc/z;->b()V

    .line 174
    return-void
.end method

.method public final c()Lc/f;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lc/t;->a:Lc/f;

    return-object v0
.end method

.method public final c([BII)Lc/i;
    .locals 2

    .prologue
    .line 73
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    invoke-virtual {v0, p1, p2, p3}, Lc/f;->b([BII)Lc/f;

    .line 75
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 6

    .prologue
    .line 177
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    const/4 v0, 0x0

    .line 183
    :try_start_0
    iget-object v1, p0, Lc/t;->a:Lc/f;

    iget-wide v2, v1, Lc/f;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 184
    iget-object v1, p0, Lc/t;->b:Lc/z;

    iget-object v2, p0, Lc/t;->a:Lc/f;

    iget-object v3, p0, Lc/t;->a:Lc/f;

    iget-wide v4, v3, Lc/f;->b:J

    invoke-interface {v1, v2, v4, v5}, Lc/z;->a(Lc/f;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 191
    :cond_2
    :goto_1
    :try_start_1
    iget-object v1, p0, Lc/t;->b:Lc/z;

    invoke-interface {v1}, Lc/z;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 195
    :cond_3
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lc/t;->c:Z

    .line 197
    if-eqz v0, :cond_0

    invoke-static {v0}, Lc/ad;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 192
    :catch_0
    move-exception v1

    .line 193
    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_2

    .line 186
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final d()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lc/u;

    invoke-direct {v0, p0}, Lc/u;-><init>(Lc/t;)V

    return-object v0
.end method

.method public final e(I)Lc/i;
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    invoke-virtual {v0, p1}, Lc/f;->c(I)Lc/f;

    .line 109
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)Lc/i;
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    invoke-virtual {v0, p1}, Lc/f;->b(I)Lc/f;

    .line 97
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    move-result-object v0

    return-object v0
.end method

.method public final g(I)Lc/i;
    .locals 2

    .prologue
    .line 89
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    iget-object v0, p0, Lc/t;->a:Lc/f;

    invoke-virtual {v0, p1}, Lc/f;->a(I)Lc/f;

    .line 91
    invoke-virtual {p0}, Lc/t;->o()Lc/i;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lc/i;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 131
    iget-boolean v0, p0, Lc/t;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :cond_0
    iget-object v4, p0, Lc/t;->a:Lc/f;

    iget-wide v0, v4, Lc/f;->b:J

    cmp-long v5, v0, v2

    if-nez v5, :cond_3

    move-wide v0, v2

    .line 133
    :cond_1
    :goto_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lc/t;->b:Lc/z;

    iget-object v3, p0, Lc/t;->a:Lc/f;

    invoke-interface {v2, v3, v0, v1}, Lc/z;->a(Lc/f;J)V

    .line 134
    :cond_2
    return-object p0

    .line 132
    :cond_3
    iget-object v4, v4, Lc/f;->a:Lc/x;

    iget-object v4, v4, Lc/x;->e:Lc/x;

    iget v5, v4, Lc/x;->c:I

    const/16 v6, 0x800

    if-ge v5, v6, :cond_1

    iget v5, v4, Lc/x;->c:I

    iget v4, v4, Lc/x;->b:I

    sub-int v4, v5, v4

    int-to-long v4, v4

    sub-long/2addr v0, v4

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "buffer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lc/t;->b:Lc/z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
