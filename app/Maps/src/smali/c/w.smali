.class Lc/w;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field final synthetic a:Lc/v;


# direct methods
.method constructor <init>(Lc/v;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lc/w;->a:Lc/v;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method


# virtual methods
.method public available()I
    .locals 4

    .prologue
    .line 273
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-boolean v0, v0, Lc/v;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_0
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-object v0, v0, Lc/v;->a:Lc/f;

    iget-wide v0, v0, Lc/f;->b:J

    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lc/w;->a:Lc/v;

    invoke-virtual {v0}, Lc/v;->close()V

    .line 279
    return-void
.end method

.method public read()I
    .locals 4

    .prologue
    .line 252
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-boolean v0, v0, Lc/v;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_0
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-object v0, v0, Lc/v;->a:Lc/f;

    iget-wide v0, v0, Lc/f;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 254
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-object v0, v0, Lc/v;->b:Lc/aa;

    iget-object v1, p0, Lc/w;->a:Lc/v;

    iget-object v1, v1, Lc/v;->a:Lc/f;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lc/aa;->b(Lc/f;J)J

    move-result-wide v0

    .line 255
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, -0x1

    .line 257
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-object v0, v0, Lc/v;->a:Lc/f;

    invoke-virtual {v0}, Lc/f;->g()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 6

    .prologue
    .line 261
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-boolean v0, v0, Lc/v;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_0
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Lc/ad;->a(JJJ)V

    .line 264
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-object v0, v0, Lc/v;->a:Lc/f;

    iget-wide v0, v0, Lc/f;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 265
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-object v0, v0, Lc/v;->b:Lc/aa;

    iget-object v1, p0, Lc/w;->a:Lc/v;

    iget-object v1, v1, Lc/v;->a:Lc/f;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lc/aa;->b(Lc/f;J)J

    move-result-wide v0

    .line 266
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, -0x1

    .line 269
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lc/w;->a:Lc/v;

    iget-object v0, v0, Lc/v;->a:Lc/f;

    invoke-virtual {v0, p1, p2, p3}, Lc/f;->a([BII)I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lc/w;->a:Lc/v;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".inputStream()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
