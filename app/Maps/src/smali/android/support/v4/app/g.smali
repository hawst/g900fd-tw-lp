.class final Landroid/support/v4/app/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/support/v4/app/g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:[I

.field final b:I

.field final c:I

.field final d:Ljava/lang/String;

.field final e:I

.field final f:I

.field final g:Ljava/lang/CharSequence;

.field final h:I

.field final i:Ljava/lang/CharSequence;

.field final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Landroid/support/v4/app/h;

    invoke-direct {v0}, Landroid/support/v4/app/h;-><init>()V

    sput-object v0, Landroid/support/v4/app/g;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/g;->a:[I

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/g;->b:I

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/g;->c:I

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/g;->d:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/g;->e:I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/g;->f:I

    .line 103
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/g;->g:Ljava/lang/CharSequence;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/g;->h:I

    .line 105
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/g;->i:Ljava/lang/CharSequence;

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/g;->j:Ljava/util/ArrayList;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/g;->k:Ljava/util/ArrayList;

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/a;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iget-object v0, p1, Landroid/support/v4/app/a;->b:Landroid/support/v4/app/e;

    move-object v1, v0

    move v0, v3

    .line 54
    :goto_0
    if-eqz v1, :cond_1

    .line 55
    iget-object v2, v1, Landroid/support/v4/app/e;->i:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, v1, Landroid/support/v4/app/e;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v0, v2

    .line 56
    :cond_0
    iget-object v1, v1, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/e;

    goto :goto_0

    .line 58
    :cond_1
    iget v1, p1, Landroid/support/v4/app/a;->d:I

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v4/app/g;->a:[I

    .line 60
    iget-boolean v0, p1, Landroid/support/v4/app/a;->g:Z

    if-nez v0, :cond_2

    .line 61
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not on back stack"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_2
    iget-object v0, p1, Landroid/support/v4/app/a;->b:Landroid/support/v4/app/e;

    move-object v5, v0

    move v0, v3

    .line 66
    :goto_1
    if-eqz v5, :cond_6

    .line 67
    iget-object v1, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v2, v0, 0x1

    iget v4, v5, Landroid/support/v4/app/e;->c:I

    aput v4, v1, v0

    .line 68
    iget-object v1, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v4, v2, 0x1

    iget-object v0, v5, Landroid/support/v4/app/e;->d:Landroid/support/v4/app/j;

    if-eqz v0, :cond_3

    iget-object v0, v5, Landroid/support/v4/app/e;->d:Landroid/support/v4/app/j;

    iget v0, v0, Landroid/support/v4/app/j;->p:I

    :goto_2
    aput v0, v1, v2

    .line 69
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v1, v4, 0x1

    iget v2, v5, Landroid/support/v4/app/e;->e:I

    aput v2, v0, v4

    .line 70
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v2, v1, 0x1

    iget v4, v5, Landroid/support/v4/app/e;->f:I

    aput v4, v0, v1

    .line 71
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v1, v2, 0x1

    iget v4, v5, Landroid/support/v4/app/e;->g:I

    aput v4, v0, v2

    .line 72
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v2, v1, 0x1

    iget v4, v5, Landroid/support/v4/app/e;->h:I

    aput v4, v0, v1

    .line 73
    iget-object v0, v5, Landroid/support/v4/app/e;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 74
    iget-object v0, v5, Landroid/support/v4/app/e;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 75
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v1, v2, 0x1

    aput v6, v0, v2

    move v2, v3

    .line 76
    :goto_3
    if-ge v2, v6, :cond_4

    .line 77
    iget-object v7, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v4, v1, 0x1

    iget-object v0, v5, Landroid/support/v4/app/e;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/j;

    iget v0, v0, Landroid/support/v4/app/j;->p:I

    aput v0, v7, v1

    .line 76
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v1, v4

    goto :goto_3

    .line 68
    :cond_3
    const/4 v0, -0x1

    goto :goto_2

    :cond_4
    move v0, v1

    .line 82
    :goto_4
    iget-object v1, v5, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/e;

    move-object v5, v1

    goto :goto_1

    .line 80
    :cond_5
    iget-object v1, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v0, v2, 0x1

    aput v3, v1, v2

    goto :goto_4

    .line 84
    :cond_6
    iget v0, p1, Landroid/support/v4/app/a;->e:I

    iput v0, p0, Landroid/support/v4/app/g;->b:I

    .line 85
    iget v0, p1, Landroid/support/v4/app/a;->f:I

    iput v0, p0, Landroid/support/v4/app/g;->c:I

    .line 86
    iget-object v0, p1, Landroid/support/v4/app/a;->h:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/v4/app/g;->d:Ljava/lang/String;

    .line 87
    iget v0, p1, Landroid/support/v4/app/a;->j:I

    iput v0, p0, Landroid/support/v4/app/g;->e:I

    .line 88
    iget v0, p1, Landroid/support/v4/app/a;->k:I

    iput v0, p0, Landroid/support/v4/app/g;->f:I

    .line 89
    iget-object v0, p1, Landroid/support/v4/app/a;->l:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/g;->g:Ljava/lang/CharSequence;

    .line 90
    iget v0, p1, Landroid/support/v4/app/a;->m:I

    iput v0, p0, Landroid/support/v4/app/g;->h:I

    .line 91
    iget-object v0, p1, Landroid/support/v4/app/a;->n:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/g;->i:Ljava/lang/CharSequence;

    .line 92
    iget-object v0, p1, Landroid/support/v4/app/a;->o:Ljava/util/ArrayList;

    iput-object v0, p0, Landroid/support/v4/app/g;->j:Ljava/util/ArrayList;

    .line 93
    iget-object v0, p1, Landroid/support/v4/app/a;->p:Ljava/util/ArrayList;

    iput-object v0, p0, Landroid/support/v4/app/g;->k:Ljava/util/ArrayList;

    .line 94
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/s;)Landroid/support/v4/app/a;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 111
    new-instance v4, Landroid/support/v4/app/a;

    invoke-direct {v4, p1}, Landroid/support/v4/app/a;-><init>(Landroid/support/v4/app/s;)V

    move v0, v1

    .line 113
    :goto_0
    iget-object v2, p0, Landroid/support/v4/app/g;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 115
    new-instance v5, Landroid/support/v4/app/e;

    invoke-direct {v5}, Landroid/support/v4/app/e;-><init>()V

    .line 116
    iget-object v2, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v0, v2, v0

    iput v0, v5, Landroid/support/v4/app/e;->c:I

    .line 117
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v0, v0, v3

    .line 120
    if-ltz v0, :cond_0

    .line 121
    iget-object v3, p1, Landroid/support/v4/app/s;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/j;

    .line 122
    iput-object v0, v5, Landroid/support/v4/app/e;->d:Landroid/support/v4/app/j;

    .line 126
    :goto_1
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v3, v2, 0x1

    aget v0, v0, v2

    iput v0, v5, Landroid/support/v4/app/e;->e:I

    .line 127
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v0, v0, v3

    iput v0, v5, Landroid/support/v4/app/e;->f:I

    .line 128
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v3, v2, 0x1

    aget v0, v0, v2

    iput v0, v5, Landroid/support/v4/app/e;->g:I

    .line 129
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v2, v3, 0x1

    aget v0, v0, v3

    iput v0, v5, Landroid/support/v4/app/e;->h:I

    .line 130
    iget-object v3, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v0, v2, 0x1

    aget v6, v3, v2

    .line 131
    if-lez v6, :cond_1

    .line 132
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, v5, Landroid/support/v4/app/e;->i:Ljava/util/ArrayList;

    move v2, v1

    .line 133
    :goto_2
    if-ge v2, v6, :cond_1

    .line 134
    iget-object v7, p1, Landroid/support/v4/app/s;->f:Ljava/util/ArrayList;

    iget-object v8, p0, Landroid/support/v4/app/g;->a:[I

    add-int/lit8 v3, v0, 0x1

    aget v0, v8, v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/j;

    .line 137
    iget-object v7, v5, Landroid/support/v4/app/e;->i:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_2

    .line 124
    :cond_0
    const/4 v0, 0x0

    iput-object v0, v5, Landroid/support/v4/app/e;->d:Landroid/support/v4/app/j;

    goto :goto_1

    .line 140
    :cond_1
    invoke-virtual {v4, v5}, Landroid/support/v4/app/a;->a(Landroid/support/v4/app/e;)V

    goto :goto_0

    .line 143
    :cond_2
    iget v0, p0, Landroid/support/v4/app/g;->b:I

    iput v0, v4, Landroid/support/v4/app/a;->e:I

    .line 144
    iget v0, p0, Landroid/support/v4/app/g;->c:I

    iput v0, v4, Landroid/support/v4/app/a;->f:I

    .line 145
    iget-object v0, p0, Landroid/support/v4/app/g;->d:Ljava/lang/String;

    iput-object v0, v4, Landroid/support/v4/app/a;->h:Ljava/lang/String;

    .line 146
    iget v0, p0, Landroid/support/v4/app/g;->e:I

    iput v0, v4, Landroid/support/v4/app/a;->j:I

    .line 147
    iput-boolean v9, v4, Landroid/support/v4/app/a;->g:Z

    .line 148
    iget v0, p0, Landroid/support/v4/app/g;->f:I

    iput v0, v4, Landroid/support/v4/app/a;->k:I

    .line 149
    iget-object v0, p0, Landroid/support/v4/app/g;->g:Ljava/lang/CharSequence;

    iput-object v0, v4, Landroid/support/v4/app/a;->l:Ljava/lang/CharSequence;

    .line 150
    iget v0, p0, Landroid/support/v4/app/g;->h:I

    iput v0, v4, Landroid/support/v4/app/a;->m:I

    .line 151
    iget-object v0, p0, Landroid/support/v4/app/g;->i:Ljava/lang/CharSequence;

    iput-object v0, v4, Landroid/support/v4/app/a;->n:Ljava/lang/CharSequence;

    .line 152
    iget-object v0, p0, Landroid/support/v4/app/g;->j:Ljava/util/ArrayList;

    iput-object v0, v4, Landroid/support/v4/app/a;->o:Ljava/util/ArrayList;

    .line 153
    iget-object v0, p0, Landroid/support/v4/app/g;->k:Ljava/util/ArrayList;

    iput-object v0, v4, Landroid/support/v4/app/a;->p:Ljava/util/ArrayList;

    .line 154
    invoke-virtual {v4, v9}, Landroid/support/v4/app/a;->a(I)V

    .line 155
    return-object v4
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    iget-object v0, p0, Landroid/support/v4/app/g;->a:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 164
    iget v0, p0, Landroid/support/v4/app/g;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 165
    iget v0, p0, Landroid/support/v4/app/g;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 166
    iget-object v0, p0, Landroid/support/v4/app/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 167
    iget v0, p0, Landroid/support/v4/app/g;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168
    iget v0, p0, Landroid/support/v4/app/g;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    iget-object v0, p0, Landroid/support/v4/app/g;->g:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 170
    iget v0, p0, Landroid/support/v4/app/g;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    iget-object v0, p0, Landroid/support/v4/app/g;->i:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 172
    iget-object v0, p0, Landroid/support/v4/app/g;->j:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 173
    iget-object v0, p0, Landroid/support/v4/app/g;->k:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 174
    return-void
.end method
