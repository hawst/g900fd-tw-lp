.class public Landroid/support/v4/app/av;
.super Landroid/support/v4/app/bi;
.source "PG"


# instance fields
.field a:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1600
    invoke-direct {p0}, Landroid/support/v4/app/bi;-><init>()V

    .line 1601
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/av;
    .locals 0

    .prologue
    .line 1629
    iput-object p1, p0, Landroid/support/v4/app/av;->a:Landroid/graphics/Bitmap;

    .line 1630
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/av;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 1612
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    iput-object p1, p0, Landroid/support/v4/app/av;->c:Ljava/lang/CharSequence;

    .line 1613
    return-object p0

    .line 1612
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/av;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 1620
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    iput-object p1, p0, Landroid/support/v4/app/av;->d:Ljava/lang/CharSequence;

    .line 1621
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/av;->e:Z

    .line 1622
    return-object p0

    .line 1620
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method
