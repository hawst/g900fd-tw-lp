.class Landroid/support/v4/app/ao;
.super Landroid/support/v4/app/am;
.source "PG"


# static fields
.field static a:Z


# instance fields
.field final b:Landroid/support/v4/g/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/g/q",
            "<",
            "Landroid/support/v4/app/ap;",
            ">;"
        }
    .end annotation
.end field

.field final c:Landroid/support/v4/g/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/g/q",
            "<",
            "Landroid/support/v4/app/ap;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/lang/String;

.field e:Landroid/support/v4/app/m;

.field f:Z

.field g:Z

.field h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v4/app/ao;->a:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Landroid/support/v4/app/m;Z)V
    .locals 1

    .prologue
    .line 477
    invoke-direct {p0}, Landroid/support/v4/app/am;-><init>()V

    .line 194
    new-instance v0, Landroid/support/v4/g/q;

    invoke-direct {v0}, Landroid/support/v4/g/q;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    .line 200
    new-instance v0, Landroid/support/v4/g/q;

    invoke-direct {v0}, Landroid/support/v4/g/q;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    .line 478
    iput-object p1, p0, Landroid/support/v4/app/ao;->d:Ljava/lang/String;

    .line 479
    iput-object p2, p0, Landroid/support/v4/app/ao;->e:Landroid/support/v4/app/m;

    .line 480
    iput-boolean p3, p0, Landroid/support/v4/app/ao;->f:Z

    .line 481
    return-void
.end method

.method private c(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/app/ap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            "Landroid/support/v4/app/an",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/support/v4/app/ap;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 498
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Landroid/support/v4/app/ao;->h:Z

    .line 499
    new-instance v0, Landroid/support/v4/app/ap;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/support/v4/app/ap;-><init>(Landroid/support/v4/app/ao;ILandroid/os/Bundle;Landroid/support/v4/app/an;)V

    invoke-interface {p3, p1}, Landroid/support/v4/app/an;->a(I)Landroid/support/v4/a/a;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    .line 500
    iget-object v1, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget v2, v0, Landroid/support/v4/app/ap;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/g/q;->a(ILjava/lang/Object;)V

    iget-boolean v1, p0, Landroid/support/v4/app/ao;->f:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    :cond_0
    iput-boolean v3, p0, Landroid/support/v4/app/ao;->h:Z

    return-object v0

    :catchall_0
    move-exception v0

    iput-boolean v3, p0, Landroid/support/v4/app/ao;->h:Z

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/a/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I",
            "Landroid/os/Bundle;",
            "Landroid/support/v4/app/an",
            "<TD;>;)",
            "Landroid/support/v4/a/a",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 543
    iget-boolean v0, p0, Landroid/support/v4/app/ao;->h:Z

    if-eqz v0, :cond_0

    .line 544
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/q;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    .line 549
    if-nez v0, :cond_2

    .line 553
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/ao;->c(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/app/ap;

    move-result-object v0

    .line 560
    :goto_0
    iget-boolean v1, v0, Landroid/support/v4/app/ap;->e:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/app/ao;->f:Z

    if-eqz v1, :cond_1

    .line 562
    iget-object v1, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    iget-object v2, v0, Landroid/support/v4/app/ap;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/ap;->b(Landroid/support/v4/a/a;Ljava/lang/Object;)V

    .line 565
    :cond_1
    iget-object v0, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    return-object v0

    .line 556
    :cond_2
    iput-object p3, v0, Landroid/support/v4/app/ap;->c:Landroid/support/v4/app/an;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 658
    iget-boolean v0, p0, Landroid/support/v4/app/ao;->h:Z

    if-eqz v0, :cond_0

    .line 659
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/q;->d(I)I

    move-result v1

    .line 664
    if-ltz v1, :cond_2

    .line 665
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/q;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    .line 666
    iget-object v2, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-object v3, v2, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    aget-object v3, v3, v1

    sget-object v4, Landroid/support/v4/g/q;->a:Ljava/lang/Object;

    if-eq v3, v4, :cond_1

    iget-object v3, v2, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    sget-object v4, Landroid/support/v4/g/q;->a:Ljava/lang/Object;

    aput-object v4, v3, v1

    iput-boolean v5, v2, Landroid/support/v4/g/q;->b:Z

    .line 667
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/ap;->c()V

    .line 669
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/q;->d(I)I

    move-result v1

    .line 670
    if-ltz v1, :cond_4

    .line 671
    iget-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/q;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    .line 672
    iget-object v2, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    iget-object v3, v2, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    aget-object v3, v3, v1

    sget-object v4, Landroid/support/v4/g/q;->a:Ljava/lang/Object;

    if-eq v3, v4, :cond_3

    iget-object v3, v2, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    sget-object v4, Landroid/support/v4/g/q;->a:Ljava/lang/Object;

    aput-object v4, v3, v1

    iput-boolean v5, v2, Landroid/support/v4/g/q;->b:Z

    .line 673
    :cond_3
    invoke-virtual {v0}, Landroid/support/v4/app/ap;->c()V

    .line 675
    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/ao;->e:Landroid/support/v4/app/m;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v4/app/ao;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 676
    iget-object v0, p0, Landroid/support/v4/app/ao;->e:Landroid/support/v4/app/m;

    iget-object v0, v0, Landroid/support/v4/app/m;->b:Landroid/support/v4/app/s;

    invoke-virtual {v0}, Landroid/support/v4/app/s;->d()V

    .line 678
    :cond_5
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 801
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v1, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_0
    iget v0, v0, Landroid/support/v4/g/q;->e:I

    if-lez v0, :cond_1

    .line 802
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 803
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move v1, v2

    .line 804
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0}, Landroid/support/v4/g/q;->b()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 805
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/q;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    .line 806
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v4, v1}, Landroid/support/v4/g/q;->b(I)I

    move-result v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(I)V

    .line 807
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 808
    invoke-virtual {v0, v3, p2, p3, p4}, Landroid/support/v4/app/ap;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 804
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 811
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v0}, Landroid/support/v4/g/q;->b()I

    move-result v0

    if-lez v0, :cond_2

    .line 812
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Inactive Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 813
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 814
    :goto_1
    iget-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v0}, Landroid/support/v4/g/q;->b()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 815
    iget-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v0, v2}, Landroid/support/v4/g/q;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    .line 816
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v3, v2}, Landroid/support/v4/g/q;->b(I)I

    move-result v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    .line 817
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 818
    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/ap;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 814
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 821
    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 825
    .line 826
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v2, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_0
    iget v4, v0, Landroid/support/v4/g/q;->e:I

    move v2, v1

    move v3, v1

    .line 827
    :goto_0
    if-ge v2, v4, :cond_3

    .line 828
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v5, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_1
    iget-object v0, v0, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, Landroid/support/v4/app/ap;

    .line 829
    iget-boolean v5, v0, Landroid/support/v4/app/ap;->h:Z

    if-eqz v5, :cond_2

    iget-boolean v0, v0, Landroid/support/v4/app/ap;->f:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    or-int/2addr v3, v0

    .line 827
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 829
    goto :goto_1

    .line 831
    :cond_3
    return v3
.end method

.method public final b(I)Landroid/support/v4/a/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I)",
            "Landroid/support/v4/a/a",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 686
    iget-boolean v0, p0, Landroid/support/v4/app/ao;->h:Z

    if-eqz v0, :cond_0

    .line 687
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 690
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/q;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    .line 691
    if-eqz v0, :cond_2

    .line 692
    iget-object v1, v0, Landroid/support/v4/app/ap;->n:Landroid/support/v4/app/ap;

    if-eqz v1, :cond_1

    .line 693
    iget-object v0, v0, Landroid/support/v4/app/ap;->n:Landroid/support/v4/app/ap;

    iget-object v0, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    .line 697
    :goto_0
    return-object v0

    .line 695
    :cond_1
    iget-object v0, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    goto :goto_0

    .line 697
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/a/a;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I",
            "Landroid/os/Bundle;",
            "Landroid/support/v4/app/an",
            "<TD;>;)",
            "Landroid/support/v4/a/a",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 593
    iget-boolean v0, p0, Landroid/support/v4/app/ao;->h:Z

    if-eqz v0, :cond_0

    .line 594
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 597
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/q;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    .line 598
    if-eqz v0, :cond_1

    .line 600
    iget-object v1, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v1, p1}, Landroid/support/v4/g/q;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/ap;

    .line 601
    if-eqz v1, :cond_5

    .line 602
    iget-boolean v2, v0, Landroid/support/v4/app/ap;->e:Z

    if-eqz v2, :cond_2

    .line 607
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/support/v4/app/ap;->f:Z

    .line 609
    invoke-virtual {v1}, Landroid/support/v4/app/ap;->c()V

    .line 610
    iget-object v1, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    iput-boolean v3, v1, Landroid/support/v4/a/a;->d:Z

    .line 611
    iget-object v1, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/g/q;->a(ILjava/lang/Object;)V

    .line 646
    :cond_1
    :goto_0
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/ao;->c(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/app/ap;

    move-result-object v0

    .line 647
    iget-object v0, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    :goto_1
    return-object v0

    .line 615
    :cond_2
    iget-boolean v1, v0, Landroid/support/v4/app/ap;->h:Z

    if-nez v1, :cond_3

    .line 619
    iget-object v1, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v1, p1, v4}, Landroid/support/v4/g/q;->a(ILjava/lang/Object;)V

    .line 621
    invoke-virtual {v0}, Landroid/support/v4/app/ap;->c()V

    goto :goto_0

    .line 626
    :cond_3
    iget-object v1, v0, Landroid/support/v4/app/ap;->n:Landroid/support/v4/app/ap;

    if-eqz v1, :cond_4

    .line 627
    iget-object v1, v0, Landroid/support/v4/app/ap;->n:Landroid/support/v4/app/ap;

    invoke-virtual {v1}, Landroid/support/v4/app/ap;->c()V

    .line 629
    iput-object v4, v0, Landroid/support/v4/app/ap;->n:Landroid/support/v4/app/ap;

    .line 631
    :cond_4
    new-instance v1, Landroid/support/v4/app/ap;

    invoke-direct {v1, p0, p1, p2, p3}, Landroid/support/v4/app/ap;-><init>(Landroid/support/v4/app/ao;ILandroid/os/Bundle;Landroid/support/v4/app/an;)V

    invoke-interface {p3, p1}, Landroid/support/v4/app/an;->a(I)Landroid/support/v4/a/a;

    move-result-object v2

    iput-object v2, v1, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    iput-object v1, v0, Landroid/support/v4/app/ap;->n:Landroid/support/v4/app/ap;

    .line 634
    iget-object v0, v0, Landroid/support/v4/app/ap;->n:Landroid/support/v4/app/ap;

    iget-object v0, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    goto :goto_1

    .line 640
    :cond_5
    iget-object v1, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    iput-boolean v3, v1, Landroid/support/v4/a/a;->d:Z

    .line 642
    iget-object v1, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/g/q;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 701
    iget-boolean v0, p0, Landroid/support/v4/app/ao;->f:Z

    if-eqz v0, :cond_1

    .line 703
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 704
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 705
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Called doStart when already started: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 716
    :cond_0
    return-void

    .line 709
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ao;->f:Z

    .line 713
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0}, Landroid/support/v4/g/q;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 714
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/q;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->a()V

    .line 713
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method final c()V
    .locals 2

    .prologue
    .line 719
    iget-boolean v0, p0, Landroid/support/v4/app/ao;->f:Z

    if-nez v0, :cond_0

    .line 721
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 722
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Called doStop when not started: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 731
    :goto_0
    return-void

    .line 727
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0}, Landroid/support/v4/g/q;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    .line 728
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/q;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->b()V

    .line 727
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 730
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/ao;->f:Z

    goto :goto_0
.end method

.method final d()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 734
    iget-boolean v0, p0, Landroid/support/v4/app/ao;->f:Z

    if-nez v0, :cond_1

    .line 736
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 737
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 738
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Called doRetain when not started: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 747
    :cond_0
    return-void

    .line 742
    :cond_1
    iput-boolean v4, p0, Landroid/support/v4/app/ao;->g:Z

    .line 743
    iput-boolean v3, p0, Landroid/support/v4/app/ao;->f:Z

    .line 744
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0}, Landroid/support/v4/g/q;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 745
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/q;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ap;

    iput-boolean v4, v0, Landroid/support/v4/app/ap;->i:Z

    iget-boolean v2, v0, Landroid/support/v4/app/ap;->h:Z

    iput-boolean v2, v0, Landroid/support/v4/app/ap;->j:Z

    iput-boolean v3, v0, Landroid/support/v4/app/ap;->h:Z

    const/4 v2, 0x0

    iput-object v2, v0, Landroid/support/v4/app/ap;->c:Landroid/support/v4/app/an;

    .line 744
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method final e()V
    .locals 3

    .prologue
    .line 761
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v1, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_0
    iget v0, v0, Landroid/support/v4/g/q;->e:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 762
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v2, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_1
    iget-object v0, v0, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Landroid/support/v4/app/ap;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/app/ap;->k:Z

    .line 761
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 764
    :cond_2
    return-void
.end method

.method final f()V
    .locals 4

    .prologue
    .line 767
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v1, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_0
    iget v0, v0, Landroid/support/v4/g/q;->e:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    .line 768
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v2, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_1
    iget-object v0, v0, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Landroid/support/v4/app/ap;

    iget-boolean v2, v0, Landroid/support/v4/app/ap;->h:Z

    if-eqz v2, :cond_2

    iget-boolean v2, v0, Landroid/support/v4/app/ap;->k:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/v4/app/ap;->k:Z

    iget-boolean v2, v0, Landroid/support/v4/app/ap;->e:Z

    if-eqz v2, :cond_2

    iget-object v2, v0, Landroid/support/v4/app/ap;->d:Landroid/support/v4/a/a;

    iget-object v3, v0, Landroid/support/v4/app/ap;->g:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/ap;->b(Landroid/support/v4/a/a;Ljava/lang/Object;)V

    .line 767
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 770
    :cond_3
    return-void
.end method

.method final g()V
    .locals 3

    .prologue
    .line 773
    iget-boolean v0, p0, Landroid/support/v4/app/ao;->g:Z

    if-nez v0, :cond_3

    .line 774
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v1, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_0
    iget v0, v0, Landroid/support/v4/g/q;->e:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 776
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    iget-boolean v2, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_1
    iget-object v0, v0, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->c()V

    .line 775
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 778
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/ao;->b:Landroid/support/v4/g/q;

    invoke-virtual {v0}, Landroid/support/v4/g/q;->c()V

    .line 781
    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    iget-boolean v1, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_4
    iget v0, v0, Landroid/support/v4/g/q;->e:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_6

    .line 783
    iget-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    iget-boolean v2, v0, Landroid/support/v4/g/q;->b:Z

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Landroid/support/v4/g/q;->a()V

    :cond_5
    iget-object v0, v0, Landroid/support/v4/g/q;->d:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->c()V

    .line 782
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 785
    :cond_6
    iget-object v0, p0, Landroid/support/v4/app/ao;->c:Landroid/support/v4/g/q;

    invoke-virtual {v0}, Landroid/support/v4/g/q;->c()V

    .line 786
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 790
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 791
    const-string v1, "LoaderManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 792
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794
    iget-object v1, p0, Landroid/support/v4/app/ao;->e:Landroid/support/v4/app/m;

    invoke-static {v1, v0}, Landroid/support/v4/g/d;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 795
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
