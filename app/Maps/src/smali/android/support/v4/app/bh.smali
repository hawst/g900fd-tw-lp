.class Landroid/support/v4/app/bh;
.super Landroid/support/v4/app/bg;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 638
    invoke-direct {p0}, Landroid/support/v4/app/bg;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/ax;)Landroid/app/Notification;
    .locals 25

    .prologue
    .line 641
    new-instance v1, Landroid/support/v4/app/bs;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/ax;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/ax;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/ax;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/support/v4/app/ax;->f:Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/support/v4/app/ax;->d:Landroid/app/PendingIntent;

    const/4 v10, 0x0

    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v4/app/ax;->e:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v12, v0, Landroid/support/v4/app/ax;->j:I

    move-object/from16 v0, p1

    iget v13, v0, Landroid/support/v4/app/ax;->k:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, Landroid/support/v4/app/ax;->l:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, Landroid/support/v4/app/ax;->h:Z

    const/16 v16, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/support/v4/app/ax;->g:I

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/ax;->r:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-direct/range {v1 .. v24}, Landroid/support/v4/app/bs;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 647
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/ax;->m:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Landroid/support/v4/app/as;->a(Landroid/support/v4/app/aq;Ljava/util/ArrayList;)V

    .line 648
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/ax;->i:Landroid/support/v4/app/bi;

    invoke-static {v1, v2}, Landroid/support/v4/app/as;->a(Landroid/support/v4/app/ar;Landroid/support/v4/app/bi;)V

    .line 649
    iget-object v2, v1, Landroid/support/v4/app/bs;->c:Ljava/util/List;

    invoke-static {v2}, Landroid/support/v4/app/bp;->a(Ljava/util/List;)Landroid/util/SparseArray;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v1, Landroid/support/v4/app/bs;->b:Landroid/os/Bundle;

    const-string v4, "android.support.actionExtras"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_0
    iget-object v2, v1, Landroid/support/v4/app/bs;->a:Landroid/app/Notification$Builder;

    iget-object v3, v1, Landroid/support/v4/app/bs;->b:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    iget-object v1, v1, Landroid/support/v4/app/bs;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method
