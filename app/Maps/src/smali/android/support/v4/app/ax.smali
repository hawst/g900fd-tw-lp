.class public Landroid/support/v4/app/ax;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field d:Landroid/app/PendingIntent;

.field e:Landroid/graphics/Bitmap;

.field f:Ljava/lang/CharSequence;

.field g:I

.field h:Z

.field i:Landroid/support/v4/app/bi;

.field j:I

.field k:I

.field l:Z

.field m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/at;",
            ">;"
        }
    .end annotation
.end field

.field n:Z

.field o:I

.field p:I

.field q:Landroid/app/Notification;

.field public r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 874
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->h:Z

    .line 884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ax;->m:Ljava/util/ArrayList;

    .line 885
    iput-boolean v4, p0, Landroid/support/v4/app/ax;->n:Z

    .line 888
    iput v4, p0, Landroid/support/v4/app/ax;->o:I

    .line 889
    iput v4, p0, Landroid/support/v4/app/ax;->p:I

    .line 892
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    .line 907
    iput-object p1, p0, Landroid/support/v4/app/ax;->a:Landroid/content/Context;

    .line 910
    iget-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 911
    iget-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 912
    iput v4, p0, Landroid/support/v4/app/ax;->g:I

    .line 913
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ax;->r:Ljava/util/ArrayList;

    .line 914
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 1262
    if-eqz p2, :cond_0

    .line 1263
    iget-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1267
    :goto_0
    return-void

    .line 1265
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 1532
    sget-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    invoke-interface {v0, p0}, Landroid/support/v4/app/az;->a(Landroid/support/v4/app/ax;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/ax;
    .locals 1

    .prologue
    .line 958
    iget-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 959
    return-object p0
.end method

.method public final a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/ax;
    .locals 2

    .prologue
    .line 1432
    iget-object v0, p0, Landroid/support/v4/app/ax;->m:Ljava/util/ArrayList;

    new-instance v1, Landroid/support/v4/app/at;

    invoke-direct {v1, p1, p2, p3}, Landroid/support/v4/app/at;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1433
    return-object p0
.end method

.method public final a(J)Landroid/support/v4/app/ax;
    .locals 1

    .prologue
    .line 921
    iget-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 922
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Landroid/support/v4/app/ax;
    .locals 0

    .prologue
    .line 1054
    iput-object p1, p0, Landroid/support/v4/app/ax;->d:Landroid/app/PendingIntent;

    .line 1055
    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/ax;
    .locals 0

    .prologue
    .line 1118
    iput-object p1, p0, Landroid/support/v4/app/ax;->e:Landroid/graphics/Bitmap;

    .line 1119
    return-object p0
.end method

.method public final a(Landroid/support/v4/app/at;)Landroid/support/v4/app/ax;
    .locals 1

    .prologue
    .line 1451
    iget-object v0, p0, Landroid/support/v4/app/ax;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1452
    return-object p0
.end method

.method public final a(Landroid/support/v4/app/bi;)Landroid/support/v4/app/ax;
    .locals 2

    .prologue
    .line 1464
    iget-object v0, p0, Landroid/support/v4/app/ax;->i:Landroid/support/v4/app/bi;

    if-eq v0, p1, :cond_0

    .line 1465
    iput-object p1, p0, Landroid/support/v4/app/ax;->i:Landroid/support/v4/app/bi;

    .line 1466
    iget-object v0, p0, Landroid/support/v4/app/ax;->i:Landroid/support/v4/app/bi;

    if-eqz v0, :cond_0

    .line 1467
    iget-object v0, p0, Landroid/support/v4/app/ax;->i:Landroid/support/v4/app/bi;

    iget-object v1, v0, Landroid/support/v4/app/bi;->b:Landroid/support/v4/app/ax;

    if-eq v1, p0, :cond_0

    iput-object p0, v0, Landroid/support/v4/app/bi;->b:Landroid/support/v4/app/ax;

    iget-object v1, v0, Landroid/support/v4/app/bi;->b:Landroid/support/v4/app/ax;

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/support/v4/app/bi;->b:Landroid/support/v4/app/ax;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/ax;->a(Landroid/support/v4/app/bi;)Landroid/support/v4/app/ax;

    .line 1470
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 982
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    iput-object p1, p0, Landroid/support/v4/app/ax;->b:Ljava/lang/CharSequence;

    .line 983
    return-object p0

    .line 982
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Z)Landroid/support/v4/app/ax;
    .locals 1

    .prologue
    .line 1196
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/ax;->a(IZ)V

    .line 1197
    return-object p0
.end method

.method public final a([J)Landroid/support/v4/app/ax;
    .locals 1

    .prologue
    .line 1165
    iget-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->vibrate:[J

    .line 1166
    return-object p0
.end method

.method public final b(I)Landroid/support/v4/app/ax;
    .locals 0

    .prologue
    .line 1287
    iput p1, p0, Landroid/support/v4/app/ax;->g:I

    .line 1288
    return-object p0
.end method

.method public final b(Landroid/app/PendingIntent;)Landroid/support/v4/app/ax;
    .locals 1

    .prologue
    .line 1066
    iget-object v0, p0, Landroid/support/v4/app/ax;->q:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 1067
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 990
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    iput-object p1, p0, Landroid/support/v4/app/ax;->c:Ljava/lang/CharSequence;

    .line 991
    return-object p0

    .line 990
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method

.method public final c(I)Landroid/support/v4/app/ax;
    .locals 0

    .prologue
    .line 1481
    iput p1, p0, Landroid/support/v4/app/ax;->o:I

    .line 1482
    return-object p0
.end method

.method public final d(I)Landroid/support/v4/app/ax;
    .locals 0

    .prologue
    .line 1493
    iput p1, p0, Landroid/support/v4/app/ax;->p:I

    .line 1494
    return-object p0
.end method
