.class public Landroid/support/v4/app/as;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Landroid/support/v4/app/az;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 815
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 816
    new-instance v0, Landroid/support/v4/app/bb;

    invoke-direct {v0}, Landroid/support/v4/app/bb;-><init>()V

    sput-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    .line 832
    :goto_0
    return-void

    .line 817
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    .line 818
    new-instance v0, Landroid/support/v4/app/ba;

    invoke-direct {v0}, Landroid/support/v4/app/ba;-><init>()V

    sput-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    goto :goto_0

    .line 819
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 820
    new-instance v0, Landroid/support/v4/app/bh;

    invoke-direct {v0}, Landroid/support/v4/app/bh;-><init>()V

    sput-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    goto :goto_0

    .line 821
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 822
    new-instance v0, Landroid/support/v4/app/bg;

    invoke-direct {v0}, Landroid/support/v4/app/bg;-><init>()V

    sput-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    goto :goto_0

    .line 823
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 824
    new-instance v0, Landroid/support/v4/app/bf;

    invoke-direct {v0}, Landroid/support/v4/app/bf;-><init>()V

    sput-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    goto :goto_0

    .line 825
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 826
    new-instance v0, Landroid/support/v4/app/be;

    invoke-direct {v0}, Landroid/support/v4/app/be;-><init>()V

    sput-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    goto :goto_0

    .line 827
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    .line 828
    new-instance v0, Landroid/support/v4/app/bd;

    invoke-direct {v0}, Landroid/support/v4/app/bd;-><init>()V

    sput-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    goto :goto_0

    .line 830
    :cond_6
    new-instance v0, Landroid/support/v4/app/bc;

    invoke-direct {v0}, Landroid/support/v4/app/bc;-><init>()V

    sput-object v0, Landroid/support/v4/app/as;->a:Landroid/support/v4/app/az;

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v4/app/aq;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/at;

    invoke-interface {p0, v0}, Landroid/support/v4/app/aq;->a(Landroid/support/v4/app/bo;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/support/v4/app/ar;Landroid/support/v4/app/bi;)V
    .locals 7

    .prologue
    .line 41
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/support/v4/app/aw;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/support/v4/app/aw;

    iget-object v0, p1, Landroid/support/v4/app/aw;->c:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Landroid/support/v4/app/aw;->e:Z

    iget-object v2, p1, Landroid/support/v4/app/aw;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, Landroid/support/v4/app/aw;->a:Ljava/lang/CharSequence;

    invoke-static {p0, v0, v1, v2, v3}, Landroid/support/v4/app/bp;->a(Landroid/support/v4/app/ar;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Landroid/support/v4/app/ay;

    if-eqz v0, :cond_2

    check-cast p1, Landroid/support/v4/app/ay;

    iget-object v0, p1, Landroid/support/v4/app/ay;->c:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Landroid/support/v4/app/ay;->e:Z

    iget-object v2, p1, Landroid/support/v4/app/ay;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, Landroid/support/v4/app/ay;->a:Ljava/util/ArrayList;

    invoke-static {p0, v0, v1, v2, v3}, Landroid/support/v4/app/bp;->a(Landroid/support/v4/app/ar;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Landroid/support/v4/app/av;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/app/av;

    iget-object v1, p1, Landroid/support/v4/app/av;->c:Ljava/lang/CharSequence;

    iget-boolean v2, p1, Landroid/support/v4/app/av;->e:Z

    iget-object v3, p1, Landroid/support/v4/app/av;->d:Ljava/lang/CharSequence;

    iget-object v4, p1, Landroid/support/v4/app/av;->a:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v4/app/bp;->a(Landroid/support/v4/app/ar;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    goto :goto_0
.end method
