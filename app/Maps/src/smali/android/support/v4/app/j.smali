.class public Landroid/support/v4/app/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnCreateContextMenuListener;


# static fields
.field private static final a:Landroid/support/v4/g/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/g/p",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field static final j:Ljava/lang/Object;


# instance fields
.field A:Z

.field B:I

.field C:Landroid/support/v4/app/s;

.field public D:Landroid/support/v4/app/m;

.field E:Landroid/support/v4/app/s;

.field F:Landroid/support/v4/app/j;

.field G:I

.field H:I

.field I:Ljava/lang/String;

.field J:Z

.field K:Z

.field L:Z

.field M:Z

.field N:Z

.field O:Z

.field P:I

.field Q:Landroid/view/ViewGroup;

.field R:Landroid/view/View;

.field S:Landroid/view/View;

.field T:Z

.field U:Z

.field V:Landroid/support/v4/app/ao;

.field W:Z

.field X:Z

.field Y:Ljava/lang/Object;

.field Z:Ljava/lang/Object;

.field aa:Ljava/lang/Object;

.field ab:Ljava/lang/Object;

.field ac:Ljava/lang/Object;

.field ad:Ljava/lang/Object;

.field ae:Landroid/support/v4/app/cc;

.field af:Landroid/support/v4/app/cc;

.field k:I

.field l:Landroid/view/View;

.field m:I

.field n:Landroid/os/Bundle;

.field o:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field public p:I

.field q:Ljava/lang/String;

.field public r:Landroid/os/Bundle;

.field s:Landroid/support/v4/app/j;

.field t:I

.field u:I

.field v:Z

.field public w:Z

.field x:Z

.field y:Z

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168
    new-instance v0, Landroid/support/v4/g/p;

    invoke-direct {v0}, Landroid/support/v4/g/p;-><init>()V

    sput-object v0, Landroid/support/v4/app/j;->a:Landroid/support/v4/g/p;

    .line 171
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/app/j;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/j;->k:I

    .line 196
    iput v2, p0, Landroid/support/v4/app/j;->p:I

    .line 208
    iput v2, p0, Landroid/support/v4/app/j;->t:I

    .line 279
    iput-boolean v3, p0, Landroid/support/v4/app/j;->N:Z

    .line 301
    iput-boolean v3, p0, Landroid/support/v4/app/j;->U:Z

    .line 307
    iput-object v1, p0, Landroid/support/v4/app/j;->Y:Ljava/lang/Object;

    .line 308
    sget-object v0, Landroid/support/v4/app/j;->j:Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/app/j;->Z:Ljava/lang/Object;

    .line 309
    iput-object v1, p0, Landroid/support/v4/app/j;->aa:Ljava/lang/Object;

    .line 310
    sget-object v0, Landroid/support/v4/app/j;->j:Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/app/j;->ab:Ljava/lang/Object;

    .line 311
    iput-object v1, p0, Landroid/support/v4/app/j;->ac:Ljava/lang/Object;

    .line 312
    sget-object v0, Landroid/support/v4/app/j;->j:Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/app/j;->ad:Ljava/lang/Object;

    .line 316
    iput-object v1, p0, Landroid/support/v4/app/j;->ae:Landroid/support/v4/app/cc;

    .line 317
    iput-object v1, p0, Landroid/support/v4/app/j;->af:Landroid/support/v4/app/cc;

    .line 388
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/j;
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/support/v4/app/j;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/j;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/j;
    .locals 4

    .prologue
    .line 414
    :try_start_0
    sget-object v0, Landroid/support/v4/app/j;->a:Landroid/support/v4/g/p;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/p;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 415
    if-nez v0, :cond_0

    .line 417
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 418
    sget-object v1, Landroid/support/v4/app/j;->a:Landroid/support/v4/g/p;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/g/p;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/j;

    .line 421
    if-eqz p2, :cond_1

    .line 422
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 423
    iput-object p2, v0, Landroid/support/v4/app/j;->r:Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 425
    :cond_1
    return-object v0

    .line 426
    :catch_0
    move-exception v0

    .line 427
    new-instance v1, Landroid/support/v4/app/l;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/l;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 430
    :catch_1
    move-exception v0

    .line 431
    new-instance v1, Landroid/support/v4/app/l;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/l;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 434
    :catch_2
    move-exception v0

    .line 435
    new-instance v1, Landroid/support/v4/app/l;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/l;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 451
    :try_start_0
    sget-object v0, Landroid/support/v4/app/j;->a:Landroid/support/v4/g/p;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/p;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 452
    if-nez v0, :cond_0

    .line 454
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 455
    sget-object v1, Landroid/support/v4/app/j;->a:Landroid/support/v4/g/p;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/g/p;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    :cond_0
    const-class v1, Landroid/support/v4/app/j;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 459
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()V
    .locals 0

    .prologue
    .line 782
    return-void
.end method

.method public static h()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 997
    const/4 v0, 0x0

    return-object v0
.end method

.method public static i()V
    .locals 0

    .prologue
    .line 1053
    return-void
.end method

.method public static j()V
    .locals 0

    .prologue
    .line 1285
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1241
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    .line 1242
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 925
    return-void
.end method

.method final a(ILandroid/support/v4/app/j;)V
    .locals 2

    .prologue
    .line 477
    iput p1, p0, Landroid/support/v4/app/j;->p:I

    .line 478
    if-eqz p2, :cond_0

    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Landroid/support/v4/app/j;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v4/app/j;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/j;->q:Ljava/lang/String;

    .line 483
    :goto_0
    return-void

    .line 481
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:fragment:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/j;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/j;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 990
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    .line 991
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1015
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    .line 1016
    return-void
.end method

.method public b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 933
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    invoke-virtual {v0}, Landroid/support/v4/app/m;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 934
    iget-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    if-nez v1, :cond_0

    new-instance v1, Landroid/support/v4/app/s;

    invoke-direct {v1}, Landroid/support/v4/app/s;-><init>()V

    iput-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    iget-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    iget-object v2, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    new-instance v3, Landroid/support/v4/app/k;

    invoke-direct {v3, p0}, Landroid/support/v4/app/k;-><init>(Landroid/support/v4/app/j;)V

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/s;->a(Landroid/support/v4/app/m;Landroid/support/v4/app/q;Landroid/support/v4/app/j;)V

    iget v1, p0, Landroid/support/v4/app/j;->k:I

    if-lt v1, v7, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    iput-boolean v4, v1, Landroid/support/v4/app/s;->r:Z

    invoke-virtual {v1, v7, v4, v4, v4}, Landroid/support/v4/app/s;->a(IIIZ)V

    :cond_0
    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    .line 935
    iget-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->setFactory(Landroid/view/LayoutInflater$Factory;)V

    .line 936
    return-object v0

    .line 934
    :cond_1
    iget v1, p0, Landroid/support/v4/app/j;->k:I

    if-lt v1, v6, :cond_2

    iget-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    iput-boolean v4, v1, Landroid/support/v4/app/s;->r:Z

    invoke-virtual {v1, v6, v4, v4, v4}, Landroid/support/v4/app/s;->a(IIIZ)V

    goto :goto_0

    :cond_2
    iget v1, p0, Landroid/support/v4/app/j;->k:I

    if-lt v1, v5, :cond_3

    iget-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    iput-boolean v4, v1, Landroid/support/v4/app/s;->r:Z

    invoke-virtual {v1, v5, v4, v4, v4}, Landroid/support/v4/app/s;->a(IIIZ)V

    goto :goto_0

    :cond_3
    iget v1, p0, Landroid/support/v4/app/j;->k:I

    if-lez v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    iput-boolean v4, v1, Landroid/support/v4/app/s;->r:Z

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/support/v4/app/s;->a(IIIZ)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1104
    iput-boolean v1, p0, Landroid/support/v4/app/j;->O:Z

    .line 1106
    iget-boolean v0, p0, Landroid/support/v4/app/j;->W:Z

    if-nez v0, :cond_1

    .line 1107
    iput-boolean v1, p0, Landroid/support/v4/app/j;->W:Z

    .line 1108
    iget-boolean v0, p0, Landroid/support/v4/app/j;->X:Z

    if-nez v0, :cond_0

    .line 1109
    iput-boolean v1, p0, Landroid/support/v4/app/j;->X:Z

    .line 1110
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget-object v1, p0, Landroid/support/v4/app/j;->q:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/j;->W:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/m;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ao;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    .line 1112
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    if-eqz v0, :cond_1

    .line 1113
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    invoke-virtual {v0}, Landroid/support/v4/app/ao;->b()V

    .line 1116
    :cond_1
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    .line 1170
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1186
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    .line 1187
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1080
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    .line 1081
    return-void
.end method

.method public final e()Landroid/support/v4/app/m;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    return-object v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1148
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 493
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final f(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1933
    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->e(Landroid/os/Bundle;)V

    .line 1934
    iget-object v0, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    if-eqz v0, :cond_0

    .line 1935
    iget-object v0, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    invoke-virtual {v0}, Landroid/support/v4/app/s;->g()Landroid/os/Parcelable;

    move-result-object v0

    .line 1936
    if-eqz v0, :cond_0

    .line 1937
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1940
    :cond_0
    return-void
.end method

.method public final g()Landroid/support/v4/app/am;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 877
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    if-eqz v0, :cond_0

    .line 878
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    .line 885
    :goto_0
    return-object v0

    .line 880
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    if-nez v0, :cond_1

    .line 881
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 883
    :cond_1
    iput-boolean v3, p0, Landroid/support/v4/app/j;->X:Z

    .line 884
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget-object v1, p0, Landroid/support/v4/app/j;->q:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/j;->W:Z

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/m;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ao;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    .line 885
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 500
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method final k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1967
    iget-object v0, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    if-eqz v0, :cond_0

    .line 1968
    iget-object v0, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/support/v4/app/s;->a(IIIZ)V

    .line 1970
    :cond_0
    iget-boolean v0, p0, Landroid/support/v4/app/j;->W:Z

    if-eqz v0, :cond_2

    .line 1971
    iput-boolean v3, p0, Landroid/support/v4/app/j;->W:Z

    .line 1972
    iget-boolean v0, p0, Landroid/support/v4/app/j;->X:Z

    if-nez v0, :cond_1

    .line 1973
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->X:Z

    .line 1974
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget-object v1, p0, Landroid/support/v4/app/j;->q:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/j;->W:Z

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/m;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ao;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    .line 1976
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    if-eqz v0, :cond_2

    .line 1977
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget-boolean v0, v0, Landroid/support/v4/app/m;->h:Z

    if-nez v0, :cond_3

    .line 1978
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    invoke-virtual {v0}, Landroid/support/v4/app/ao;->c()V

    .line 1984
    :cond_2
    :goto_0
    return-void

    .line 1980
    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    invoke-virtual {v0}, Landroid/support/v4/app/ao;->d()V

    goto :goto_0
.end method

.method final l()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2002
    iget-object v0, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    if-eqz v0, :cond_0

    .line 2003
    iget-object v0, p0, Landroid/support/v4/app/j;->E:Landroid/support/v4/app/s;

    iput-boolean v1, v0, Landroid/support/v4/app/s;->s:Z

    invoke-virtual {v0}, Landroid/support/v4/app/s;->f()Z

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/support/v4/app/s;->a(IIIZ)V

    iput-object v2, v0, Landroid/support/v4/app/s;->n:Landroid/support/v4/app/m;

    iput-object v2, v0, Landroid/support/v4/app/s;->o:Landroid/support/v4/app/q;

    iput-object v2, v0, Landroid/support/v4/app/s;->p:Landroid/support/v4/app/j;

    .line 2005
    :cond_0
    iput-boolean v3, p0, Landroid/support/v4/app/j;->O:Z

    .line 2006
    iput-boolean v1, p0, Landroid/support/v4/app/j;->O:Z

    iget-boolean v0, p0, Landroid/support/v4/app/j;->X:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, Landroid/support/v4/app/j;->X:Z

    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    iget-object v1, p0, Landroid/support/v4/app/j;->q:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/j;->W:Z

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/m;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ao;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/j;->V:Landroid/support/v4/app/ao;

    invoke-virtual {v0}, Landroid/support/v4/app/ao;->g()V

    .line 2007
    :cond_2
    iget-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    if-nez v0, :cond_3

    .line 2008
    new-instance v0, Landroid/support/v4/app/cd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/cd;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2011
    :cond_3
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1151
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    .line 1152
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 1337
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/m;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1338
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 1173
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/j;->O:Z

    .line 1174
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 505
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 506
    invoke-static {p0, v0}, Landroid/support/v4/g/d;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 507
    iget v1, p0, Landroid/support/v4/app/j;->p:I

    if-ltz v1, :cond_0

    .line 508
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    iget v1, p0, Landroid/support/v4/app/j;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 511
    :cond_0
    iget v1, p0, Landroid/support/v4/app/j;->G:I

    if-eqz v1, :cond_1

    .line 512
    const-string v1, " id=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    iget v1, p0, Landroid/support/v4/app/j;->G:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/j;->I:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 516
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    iget-object v1, p0, Landroid/support/v4/app/j;->I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    :cond_2
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 520
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
