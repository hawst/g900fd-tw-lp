.class Landroid/support/v4/app/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Ljava/util/ArrayList;

.field final synthetic d:Landroid/support/v4/app/f;

.field final synthetic e:Z

.field final synthetic f:Landroid/support/v4/app/j;

.field final synthetic g:Landroid/support/v4/app/j;

.field final synthetic h:Landroid/support/v4/app/a;


# direct methods
.method constructor <init>(Landroid/support/v4/app/a;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Landroid/support/v4/app/f;ZLandroid/support/v4/app/j;Landroid/support/v4/app/j;)V
    .locals 0

    .prologue
    .line 1234
    iput-object p1, p0, Landroid/support/v4/app/c;->h:Landroid/support/v4/app/a;

    iput-object p2, p0, Landroid/support/v4/app/c;->a:Landroid/view/View;

    iput-object p3, p0, Landroid/support/v4/app/c;->b:Ljava/lang/Object;

    iput-object p4, p0, Landroid/support/v4/app/c;->c:Ljava/util/ArrayList;

    iput-object p5, p0, Landroid/support/v4/app/c;->d:Landroid/support/v4/app/f;

    iput-boolean p6, p0, Landroid/support/v4/app/c;->e:Z

    iput-object p7, p0, Landroid/support/v4/app/c;->f:Landroid/support/v4/app/j;

    iput-object p8, p0, Landroid/support/v4/app/c;->g:Landroid/support/v4/app/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1237
    iget-object v0, p0, Landroid/support/v4/app/c;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1239
    iget-object v0, p0, Landroid/support/v4/app/c;->b:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 1240
    iget-object v0, p0, Landroid/support/v4/app/c;->b:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v4/app/c;->c:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Landroid/support/v4/app/af;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 1242
    iget-object v0, p0, Landroid/support/v4/app/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1244
    iget-object v1, p0, Landroid/support/v4/app/c;->h:Landroid/support/v4/app/a;

    iget-object v2, p0, Landroid/support/v4/app/c;->d:Landroid/support/v4/app/f;

    iget-boolean v3, p0, Landroid/support/v4/app/c;->e:Z

    iget-object v4, p0, Landroid/support/v4/app/c;->f:Landroid/support/v4/app/j;

    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    iget-object v4, v4, Landroid/support/v4/app/j;->R:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-object v5, v1, Landroid/support/v4/app/a;->o:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    invoke-static {v0, v4}, Landroid/support/v4/app/af;->a(Ljava/util/Map;Landroid/view/View;)V

    if-eqz v3, :cond_3

    iget-object v4, v1, Landroid/support/v4/app/a;->o:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/support/v4/app/a;->p:Ljava/util/ArrayList;

    invoke-static {v4, v5, v0}, Landroid/support/v4/app/a;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/g/a;)Landroid/support/v4/g/a;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v3, :cond_4

    invoke-virtual {v1, v2, v0, v6}, Landroid/support/v4/app/a;->a(Landroid/support/v4/app/f;Landroid/support/v4/g/a;Z)V

    .line 1246
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/g/a;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1247
    iget-object v1, p0, Landroid/support/v4/app/c;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/c;->d:Landroid/support/v4/app/f;

    iget-object v2, v2, Landroid/support/v4/app/f;->d:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1251
    :goto_2
    iget-object v1, p0, Landroid/support/v4/app/c;->b:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/c;->c:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Landroid/support/v4/app/af;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 1254
    iget-object v1, p0, Landroid/support/v4/app/c;->h:Landroid/support/v4/app/a;

    iget-object v2, p0, Landroid/support/v4/app/c;->d:Landroid/support/v4/app/f;

    iget-object v3, v1, Landroid/support/v4/app/a;->p:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/g/a;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v1, v1, Landroid/support/v4/app/a;->p:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v1, v2, Landroid/support/v4/app/f;->c:Landroid/support/v4/app/ak;

    iput-object v0, v1, Landroid/support/v4/app/ak;->a:Landroid/view/View;

    .line 1256
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/c;->h:Landroid/support/v4/app/a;

    iget-object v0, p0, Landroid/support/v4/app/c;->d:Landroid/support/v4/app/f;

    iget-object v0, p0, Landroid/support/v4/app/c;->f:Landroid/support/v4/app/j;

    iget-object v0, p0, Landroid/support/v4/app/c;->g:Landroid/support/v4/app/j;

    iget-boolean v0, p0, Landroid/support/v4/app/c;->e:Z

    .line 1260
    :cond_2
    return v6

    .line 1244
    :cond_3
    iget-object v4, v1, Landroid/support/v4/app/a;->p:Ljava/util/ArrayList;

    invoke-static {v0, v4}, Landroid/support/v4/g/g;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    goto :goto_0

    :cond_4
    invoke-static {v2, v0, v6}, Landroid/support/v4/app/a;->b(Landroid/support/v4/app/f;Landroid/support/v4/g/a;Z)V

    goto :goto_1

    .line 1249
    :cond_5
    iget-object v1, p0, Landroid/support/v4/app/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method
