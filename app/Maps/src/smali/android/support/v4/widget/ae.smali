.class Landroid/support/v4/widget/ae;
.super Landroid/graphics/drawable/Drawable;
.source "PG"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# static fields
.field static final a:Landroid/view/animation/Interpolator;

.field static final b:Landroid/view/animation/Interpolator;

.field private static final g:Landroid/view/animation/Interpolator;


# instance fields
.field final c:Landroid/support/v4/widget/aj;

.field d:F

.field e:F

.field f:Z

.field private final h:[I

.field private final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/content/res/Resources;

.field private k:Landroid/view/View;

.field private l:Landroid/view/animation/Animation;

.field private m:D

.field private n:D

.field private final o:Landroid/graphics/drawable/Drawable$Callback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Landroid/support/v4/widget/ae;->g:Landroid/view/animation/Interpolator;

    .line 54
    new-instance v0, Landroid/support/v4/widget/ai;

    invoke-direct {v0}, Landroid/support/v4/widget/ai;-><init>()V

    sput-object v0, Landroid/support/v4/widget/ae;->a:Landroid/view/animation/Interpolator;

    .line 55
    new-instance v0, Landroid/support/v4/widget/ak;

    invoke-direct {v0}, Landroid/support/v4/widget/ak;-><init>()V

    sput-object v0, Landroid/support/v4/widget/ae;->b:Landroid/view/animation/Interpolator;

    .line 56
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v4, 0x0

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    .line 112
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 76
    new-array v0, v12, [I

    const/high16 v1, -0x1000000

    aput v1, v0, v4

    iput-object v0, p0, Landroid/support/v4/widget/ae;->h:[I

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/ae;->i:Ljava/util/ArrayList;

    .line 381
    new-instance v0, Landroid/support/v4/widget/ah;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/ah;-><init>(Landroid/support/v4/widget/ae;)V

    iput-object v0, p0, Landroid/support/v4/widget/ae;->o:Landroid/graphics/drawable/Drawable$Callback;

    .line 113
    iput-object p2, p0, Landroid/support/v4/widget/ae;->k:Landroid/view/View;

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/ae;->j:Landroid/content/res/Resources;

    .line 116
    new-instance v0, Landroid/support/v4/widget/aj;

    iget-object v1, p0, Landroid/support/v4/widget/ae;->o:Landroid/graphics/drawable/Drawable$Callback;

    invoke-direct {v0, v1}, Landroid/support/v4/widget/aj;-><init>(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    .line 117
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget-object v1, p0, Landroid/support/v4/widget/ae;->h:[I

    iput-object v1, v0, Landroid/support/v4/widget/aj;->j:[I

    iput v4, v0, Landroid/support/v4/widget/aj;->k:I

    .line 119
    const-wide v6, 0x4021800000000000L    # 8.75

    const-wide/high16 v8, 0x4004000000000000L    # 2.5

    const/high16 v10, 0x41200000    # 10.0f

    const/high16 v11, 0x40a00000    # 5.0f

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v1 .. v11}, Landroid/support/v4/widget/ae;->a(DDDDFF)V

    .line 120
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    new-instance v1, Landroid/support/v4/widget/af;

    invoke-direct {v1, p0, v0}, Landroid/support/v4/widget/af;-><init>(Landroid/support/v4/widget/ae;Landroid/support/v4/widget/aj;)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    invoke-virtual {v1, v12}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    sget-object v2, Landroid/support/v4/widget/ae;->g:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance v2, Landroid/support/v4/widget/ag;

    invoke-direct {v2, p0, v0}, Landroid/support/v4/widget/ag;-><init>(Landroid/support/v4/widget/ae;Landroid/support/v4/widget/aj;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iput-object v1, p0, Landroid/support/v4/widget/ae;->l:Landroid/view/animation/Animation;

    .line 121
    return-void
.end method

.method private a(DDDDFF)V
    .locals 7

    .prologue
    .line 125
    iget-object v1, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    .line 126
    iget-object v0, p0, Landroid/support/v4/widget/ae;->j:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 127
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 129
    float-to-double v2, v0

    mul-double/2addr v2, p1

    iput-wide v2, p0, Landroid/support/v4/widget/ae;->m:D

    .line 130
    float-to-double v2, v0

    mul-double/2addr v2, p3

    iput-wide v2, p0, Landroid/support/v4/widget/ae;->n:D

    .line 131
    double-to-float v2, p7

    mul-float/2addr v2, v0

    iput v2, v1, Landroid/support/v4/widget/aj;->h:F

    iget-object v3, v1, Landroid/support/v4/widget/aj;->b:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, v1, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    float-to-double v2, v0

    mul-double/2addr v2, p5

    iput-wide v2, v1, Landroid/support/v4/widget/aj;->r:D

    .line 133
    const/4 v2, 0x0

    iput v2, v1, Landroid/support/v4/widget/aj;->k:I

    .line 134
    mul-float v2, p9, v0

    mul-float v0, v0, p10

    float-to-int v2, v2

    iput v2, v1, Landroid/support/v4/widget/aj;->s:I

    float-to-int v0, v0

    iput v0, v1, Landroid/support/v4/widget/aj;->t:I

    .line 135
    iget-wide v2, p0, Landroid/support/v4/widget/ae;->m:D

    double-to-int v0, v2

    iget-wide v2, p0, Landroid/support/v4/widget/ae;->n:D

    double-to-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    iget-wide v2, v1, Landroid/support/v4/widget/aj;->r:D

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-lez v2, :cond_0

    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-gez v2, :cond_1

    :cond_0
    iget v0, v1, Landroid/support/v4/widget/aj;->h:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v0, v2

    :goto_0
    iput v0, v1, Landroid/support/v4/widget/aj;->i:F

    .line 136
    return-void

    .line 135
    :cond_1
    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    float-to-double v2, v0

    iget-wide v4, v1, Landroid/support/v4/widget/aj;->r:D

    sub-double/2addr v2, v4

    double-to-float v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 176
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iput p1, v0, Landroid/support/v4/widget/aj;->e:F

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v1}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 177
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iput p2, v0, Landroid/support/v4/widget/aj;->f:F

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v1}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 178
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget-boolean v1, v0, Landroid/support/v4/widget/aj;->o:Z

    if-eq v1, p1, :cond_0

    iput-boolean p1, v0, Landroid/support/v4/widget/aj;->o:Z

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 160
    :cond_0
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 220
    invoke-virtual {p0}, Landroid/support/v4/widget/ae;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 221
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 222
    iget v0, p0, Landroid/support/v4/widget/ae;->d:F

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 223
    iget-object v8, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget-object v1, v8, Landroid/support/v4/widget/aj;->a:Landroid/graphics/RectF;

    invoke-virtual {v1, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget v0, v8, Landroid/support/v4/widget/aj;->i:F

    iget v2, v8, Landroid/support/v4/widget/aj;->i:F

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->inset(FF)V

    iget v0, v8, Landroid/support/v4/widget/aj;->e:F

    iget v2, v8, Landroid/support/v4/widget/aj;->g:F

    add-float/2addr v0, v2

    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v2, v0

    iget v0, v8, Landroid/support/v4/widget/aj;->f:F

    iget v3, v8, Landroid/support/v4/widget/aj;->g:F

    add-float/2addr v0, v3

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v0, v3

    sub-float v3, v0, v2

    iget-object v0, v8, Landroid/support/v4/widget/aj;->b:Landroid/graphics/Paint;

    iget-object v4, v8, Landroid/support/v4/widget/aj;->j:[I

    iget v5, v8, Landroid/support/v4/widget/aj;->k:I

    aget v4, v4, v5

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v4, 0x0

    iget-object v5, v8, Landroid/support/v4/widget/aj;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    iget-boolean v0, v8, Landroid/support/v4/widget/aj;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    if-nez v0, :cond_2

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    iget-object v0, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    :goto_0
    iget v0, v8, Landroid/support/v4/widget/aj;->i:F

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v1, v8, Landroid/support/v4/widget/aj;->q:F

    mul-float/2addr v0, v1

    iget-wide v4, v8, Landroid/support/v4/widget/aj;->r:D

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v4, v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    float-to-double v10, v1

    add-double/2addr v4, v10

    double-to-float v1, v4

    iget-wide v4, v8, Landroid/support/v4/widget/aj;->r:D

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v4, v10

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v9

    float-to-double v10, v9

    add-double/2addr v4, v10

    double-to-float v4, v4

    iget-object v5, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v5, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    iget v9, v8, Landroid/support/v4/widget/aj;->s:I

    int-to-float v9, v9

    iget v10, v8, Landroid/support/v4/widget/aj;->q:F

    mul-float/2addr v9, v10

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v5, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    iget v9, v8, Landroid/support/v4/widget/aj;->s:I

    int-to-float v9, v9

    iget v10, v8, Landroid/support/v4/widget/aj;->q:F

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    iget v10, v8, Landroid/support/v4/widget/aj;->t:I

    int-to-float v10, v10

    iget v11, v8, Landroid/support/v4/widget/aj;->q:F

    mul-float/2addr v10, v11

    invoke-virtual {v5, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v5, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    sub-float v0, v1, v0

    invoke-virtual {v5, v0, v4}, Landroid/graphics/Path;->offset(FF)V

    iget-object v0, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    iget-object v0, v8, Landroid/support/v4/widget/aj;->c:Landroid/graphics/Paint;

    iget-object v1, v8, Landroid/support/v4/widget/aj;->j:[I

    iget v4, v8, Landroid/support/v4/widget/aj;->k:I

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    add-float v0, v2, v3

    const/high16 v1, 0x40a00000    # 5.0f

    sub-float/2addr v0, v1

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    iget-object v1, v8, Landroid/support/v4/widget/aj;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_0
    iget v0, v8, Landroid/support/v4/widget/aj;->u:I

    const/16 v1, 0xff

    if-ge v0, v1, :cond_1

    iget-object v0, v8, Landroid/support/v4/widget/aj;->v:Landroid/graphics/Paint;

    iget v1, v8, Landroid/support/v4/widget/aj;->w:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v8, Landroid/support/v4/widget/aj;->v:Landroid/graphics/Paint;

    iget v1, v8, Landroid/support/v4/widget/aj;->u:I

    rsub-int v1, v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v1

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, v8, Landroid/support/v4/widget/aj;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 224
    :cond_1
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 225
    return-void

    .line 223
    :cond_2
    iget-object v0, v8, Landroid/support/v4/widget/aj;->p:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    goto/16 :goto_0
.end method

.method public getAlpha()I
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget v0, v0, Landroid/support/v4/widget/aj;->u:I

    return v0
.end method

.method public getIntrinsicHeight()I
    .locals 2

    .prologue
    .line 210
    iget-wide v0, p0, Landroid/support/v4/widget/ae;->n:D

    double-to-int v0, v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 2

    .prologue
    .line 215
    iget-wide v0, p0, Landroid/support/v4/widget/ae;->m:D

    double-to-int v0, v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 254
    const/4 v0, -0x3

    return v0
.end method

.method public isRunning()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 259
    iget-object v3, p0, Landroid/support/v4/widget/ae;->i:Ljava/util/ArrayList;

    .line 260
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 261
    :goto_0
    if-ge v2, v4, :cond_1

    .line 262
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    .line 263
    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    const/4 v0, 0x1

    .line 267
    :goto_1
    return v0

    .line 261
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 267
    goto :goto_1
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iput p1, v0, Landroid/support/v4/widget/aj;->u:I

    .line 230
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget-object v1, v0, Landroid/support/v4/widget/aj;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 239
    return-void
.end method

.method public start()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 272
    iget-object v0, p0, Landroid/support/v4/widget/ae;->l:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 273
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget v1, v0, Landroid/support/v4/widget/aj;->e:F

    iput v1, v0, Landroid/support/v4/widget/aj;->l:F

    iget v1, v0, Landroid/support/v4/widget/aj;->f:F

    iput v1, v0, Landroid/support/v4/widget/aj;->m:F

    iget v1, v0, Landroid/support/v4/widget/aj;->g:F

    iput v1, v0, Landroid/support/v4/widget/aj;->n:F

    .line 275
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget v0, v0, Landroid/support/v4/widget/aj;->f:F

    iget-object v1, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget v1, v1, Landroid/support/v4/widget/aj;->e:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/ae;->f:Z

    .line 277
    iget-object v0, p0, Landroid/support/v4/widget/ae;->l:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x29a

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 278
    iget-object v0, p0, Landroid/support/v4/widget/ae;->k:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/widget/ae;->l:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 285
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v4/widget/aj;->k:I

    .line 281
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iput v2, v0, Landroid/support/v4/widget/aj;->l:F

    iput v2, v0, Landroid/support/v4/widget/aj;->m:F

    iput v2, v0, Landroid/support/v4/widget/aj;->n:F

    iput v2, v0, Landroid/support/v4/widget/aj;->e:F

    iget-object v1, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v1, v3}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    iput v2, v0, Landroid/support/v4/widget/aj;->f:F

    iget-object v1, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v1, v3}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    iput v2, v0, Landroid/support/v4/widget/aj;->g:F

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v3}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 282
    iget-object v0, p0, Landroid/support/v4/widget/ae;->l:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x535

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 283
    iget-object v0, p0, Landroid/support/v4/widget/ae;->k:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/widget/ae;->l:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 289
    iget-object v0, p0, Landroid/support/v4/widget/ae;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 290
    iput v2, p0, Landroid/support/v4/widget/ae;->d:F

    invoke-virtual {p0}, Landroid/support/v4/widget/ae;->invalidateSelf()V

    .line 291
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget-boolean v1, v0, Landroid/support/v4/widget/aj;->o:Z

    if-eq v1, v4, :cond_0

    iput-boolean v4, v0, Landroid/support/v4/widget/aj;->o:Z

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v3}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 292
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iput v4, v0, Landroid/support/v4/widget/aj;->k:I

    .line 293
    iget-object v0, p0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iput v2, v0, Landroid/support/v4/widget/aj;->l:F

    iput v2, v0, Landroid/support/v4/widget/aj;->m:F

    iput v2, v0, Landroid/support/v4/widget/aj;->n:F

    iput v2, v0, Landroid/support/v4/widget/aj;->e:F

    iget-object v1, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v1, v3}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    iput v2, v0, Landroid/support/v4/widget/aj;->f:F

    iget-object v1, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v1, v3}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    iput v2, v0, Landroid/support/v4/widget/aj;->g:F

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v3}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 294
    return-void
.end method
