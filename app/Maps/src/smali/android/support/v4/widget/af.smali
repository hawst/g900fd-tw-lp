.class Landroid/support/v4/widget/af;
.super Landroid/view/animation/Animation;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v4/widget/aj;

.field final synthetic b:Landroid/support/v4/widget/ae;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/ae;Landroid/support/v4/widget/aj;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Landroid/support/v4/widget/af;->b:Landroid/support/v4/widget/ae;

    iput-object p2, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method public applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 8

    .prologue
    const v7, 0x3f4ccccd    # 0.8f

    const/4 v6, 0x0

    .line 315
    iget-object v0, p0, Landroid/support/v4/widget/af;->b:Landroid/support/v4/widget/ae;

    iget-boolean v0, v0, Landroid/support/v4/widget/ae;->f:Z

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Landroid/support/v4/widget/af;->b:Landroid/support/v4/widget/ae;

    iget-object v0, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iget v1, v0, Landroid/support/v4/widget/aj;->n:F

    div-float/2addr v1, v7

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    double-to-float v1, v2

    iget v2, v0, Landroid/support/v4/widget/aj;->l:F

    iget v3, v0, Landroid/support/v4/widget/aj;->m:F

    iget v4, v0, Landroid/support/v4/widget/aj;->l:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    iput v2, v0, Landroid/support/v4/widget/aj;->e:F

    iget-object v2, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v2, v6}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v2, v0, Landroid/support/v4/widget/aj;->n:F

    iget v3, v0, Landroid/support/v4/widget/aj;->n:F

    sub-float/2addr v1, v3

    mul-float/2addr v1, p1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/support/v4/widget/aj;->g:F

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v6}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 345
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iget v0, v0, Landroid/support/v4/widget/aj;->h:F

    float-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    iget-object v4, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iget-wide v4, v4, Landroid/support/v4/widget/aj;->r:D

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 323
    iget-object v1, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iget v1, v1, Landroid/support/v4/widget/aj;->m:F

    .line 324
    iget-object v2, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iget v2, v2, Landroid/support/v4/widget/aj;->l:F

    .line 325
    iget-object v3, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iget v3, v3, Landroid/support/v4/widget/aj;->n:F

    .line 329
    sub-float v0, v7, v0

    .line 330
    sget-object v4, Landroid/support/v4/widget/ae;->b:Landroid/view/animation/Interpolator;

    invoke-interface {v4, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v4

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    .line 332
    iget-object v1, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iput v0, v1, Landroid/support/v4/widget/aj;->f:F

    iget-object v0, v1, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v6}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 334
    sget-object v0, Landroid/support/v4/widget/ae;->a:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v7

    add-float/2addr v0, v2

    .line 336
    iget-object v1, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iput v0, v1, Landroid/support/v4/widget/aj;->e:F

    iget-object v0, v1, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v6}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 338
    const/high16 v0, 0x3e800000    # 0.25f

    mul-float/2addr v0, p1

    add-float/2addr v0, v3

    .line 339
    iget-object v1, p0, Landroid/support/v4/widget/af;->a:Landroid/support/v4/widget/aj;

    iput v0, v1, Landroid/support/v4/widget/aj;->g:F

    iget-object v0, v1, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v6}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 341
    const/high16 v0, 0x43100000    # 144.0f

    mul-float/2addr v0, p1

    const/high16 v1, 0x44340000    # 720.0f

    iget-object v2, p0, Landroid/support/v4/widget/af;->b:Landroid/support/v4/widget/ae;

    iget v2, v2, Landroid/support/v4/widget/ae;->e:F

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 343
    iget-object v1, p0, Landroid/support/v4/widget/af;->b:Landroid/support/v4/widget/ae;

    iput v0, v1, Landroid/support/v4/widget/ae;->d:F

    invoke-virtual {v1}, Landroid/support/v4/widget/ae;->invalidateSelf()V

    goto :goto_0
.end method
