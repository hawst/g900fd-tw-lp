.class public Landroid/support/v4/widget/SwipeRefreshLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field private static final i:Ljava/lang/String;

.field private static final r:[I


# instance fields
.field private final A:Landroid/view/animation/Animation;

.field a:Z

.field b:I

.field c:Landroid/support/v4/widget/CircleImageView;

.field public d:I

.field public e:I

.field f:Landroid/support/v4/widget/ae;

.field g:F

.field h:Z

.field private j:Landroid/view/View;

.field private k:I

.field private l:F

.field private m:Z

.field private n:F

.field private o:Z

.field private p:I

.field private final q:Landroid/view/animation/DecelerateInterpolator;

.field private s:I

.field private t:Landroid/view/animation/Animation;

.field private u:Landroid/view/animation/Animation;

.field private v:Landroid/view/animation/Animation;

.field private w:I

.field private x:I

.field private y:Landroid/view/animation/Animation$AnimationListener;

.field private final z:Landroid/view/animation/Animation;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const-class v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->i:Ljava/lang/String;

    .line 113
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101000e

    aput v2, v0, v1

    sput-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->r:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, -0x1

    const v4, -0x50506

    const/high16 v3, 0x42200000    # 40.0f

    const/4 v2, 0x0

    .line 268
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 95
    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    .line 97
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    .line 101
    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->m:Z

    .line 105
    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    .line 118
    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->s:I

    .line 149
    new-instance v0, Landroid/support/v4/widget/be;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/be;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->y:Landroid/view/animation/Animation$AnimationListener;

    .line 842
    new-instance v0, Landroid/support/v4/widget/bi;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/bi;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->z:Landroid/view/animation/Animation;

    .line 865
    new-instance v0, Landroid/support/v4/widget/bj;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/bj;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->A:Landroid/view/animation/Animation;

    .line 270
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->k:I

    .line 272
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    .line 275
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setWillNotDraw(Z)V

    .line 276
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Landroid/view/animation/DecelerateInterpolator;

    .line 278
    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->r:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 279
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 280
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 282
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 283
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:I

    .line 284
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->x:I

    .line 286
    new-instance v1, Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-direct {v1, v2, v4, v3}, Landroid/support/v4/widget/CircleImageView;-><init>(Landroid/content/Context;IF)V

    iput-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    new-instance v1, Landroid/support/v4/widget/ae;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Landroid/support/v4/widget/ae;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    iget-object v1, v1, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iput v4, v1, Landroid/support/v4/widget/aj;->w:I

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/CircleImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/CircleImageView;->setVisibility(I)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->addView(Landroid/view/View;)V

    .line 287
    invoke-static {p0, v5}, Landroid/support/v4/view/at;->a(Landroid/view/ViewGroup;Z)V

    .line 289
    const/high16 v1, 0x42800000    # 64.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->g:F

    .line 290
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->g:F

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    .line 291
    return-void
.end method

.method private a(II)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 429
    new-instance v0, Landroid/support/v4/widget/bg;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/v4/widget/bg;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;II)V

    .line 440
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 442
    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    const/4 v2, 0x0

    iput-object v2, v1, Landroid/support/v4/widget/CircleImageView;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 443
    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v1}, Landroid/support/v4/widget/CircleImageView;->clearAnimation()V

    .line 444
    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/CircleImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 445
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 505
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    if-nez v0, :cond_0

    .line 506
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 507
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 508
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 509
    iput-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    .line 514
    :cond_0
    return-void

    .line 506
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v4/widget/SwipeRefreshLayout;F)V
    .locals 2

    .prologue
    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v1}, Landroid/support/v4/widget/CircleImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/ae;->setAlpha(I)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-static {v0, p1}, Landroid/support/v4/view/at;->d(Landroid/view/View;F)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-static {v0, p1}, Landroid/support/v4/view/at;->e(Landroid/view/View;F)V

    goto :goto_1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 906
    invoke-static {p1}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 907
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 908
    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    if-ne v1, v2, :cond_0

    .line 911
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 912
    :goto_0
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    .line 914
    :cond_0
    return-void

    .line 911
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Landroid/support/v4/widget/SwipeRefreshLayout;F)V
    .locals 3

    .prologue
    .line 58
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:I

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->e:I

    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v1}, Landroid/support/v4/widget/CircleImageView;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(IZ)V

    return-void
.end method

.method private b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 584
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v0, v3, :cond_4

    .line 585
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 586
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    check-cast v0, Landroid/widget/AbsListView;

    .line 587
    invoke-virtual {v0}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v3

    if-gtz v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getPaddingTop()I

    move-result v0

    if-ge v3, v0, :cond_1

    :cond_0
    move v0, v1

    .line 594
    :goto_0
    return v0

    :cond_1
    move v0, v2

    .line 587
    goto :goto_0

    .line 591
    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    .line 594
    :cond_4
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method a(IZ)V
    .locals 2

    .prologue
    .line 897
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v0}, Landroid/support/v4/widget/CircleImageView;->bringToFront()V

    .line 898
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/CircleImageView;->offsetTopAndBottom(I)V

    .line 899
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v0}, Landroid/support/v4/widget/CircleImageView;->getTop()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    .line 900
    if-eqz p2, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 901
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->invalidate()V

    .line 903
    :cond_0
    return-void
.end method

.method a(Landroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    .prologue
    .line 406
    new-instance v0, Landroid/support/v4/widget/bf;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/bf;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    .line 412
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 413
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    iput-object p1, v0, Landroid/support/v4/widget/CircleImageView;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 414
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v0}, Landroid/support/v4/widget/CircleImageView;->clearAnimation()V

    .line 415
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->t:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CircleImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 416
    return-void
.end method

.method protected getChildDrawingOrder(II)I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->s:I

    if-gez v0, :cond_1

    .line 304
    :cond_0
    :goto_0
    return p2

    .line 296
    :cond_1
    add-int/lit8 v0, p1, -0x1

    if-ne p2, v0, :cond_2

    .line 298
    iget p2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->s:I

    goto :goto_0

    .line 299
    :cond_2
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->s:I

    if-lt p2, v0, :cond_0

    .line 301
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v0, 0x0

    .line 600
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a()V

    .line 602
    invoke-static {p1}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;)I

    move-result v1

    .line 604
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->b()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    if-eqz v3, :cond_1

    .line 652
    :cond_0
    :goto_0
    return v0

    .line 613
    :cond_1
    packed-switch v1, :pswitch_data_0

    .line 652
    :cond_2
    :goto_1
    :pswitch_0
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:Z

    goto :goto_0

    .line 615
    :pswitch_1
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->e:I

    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v3}, Landroid/support/v4/widget/CircleImageView;->getTop()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {p0, v1, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(IZ)V

    .line 616
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    .line 617
    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:Z

    .line 618
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    invoke-static {p1, v1}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;I)I

    move-result v1

    if-gez v1, :cond_3

    move v1, v2

    .line 619
    :goto_2
    cmpl-float v3, v1, v2

    if-eqz v3, :cond_0

    .line 622
    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:F

    .line 625
    :pswitch_2
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    if-ne v1, v4, :cond_4

    .line 626
    sget-object v1, Landroid/support/v4/widget/SwipeRefreshLayout;->i:Ljava/lang/String;

    goto :goto_0

    .line 618
    :cond_3
    invoke-static {p1, v1}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    goto :goto_2

    .line 630
    :cond_4
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    invoke-static {p1, v1}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;I)I

    move-result v1

    if-gez v1, :cond_5

    move v1, v2

    .line 631
    :goto_3
    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 634
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:F

    sub-float v0, v1, v0

    .line 635
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->k:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:Z

    if-nez v0, :cond_2

    .line 636
    iput-boolean v5, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:Z

    .line 637
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    const/16 v1, 0x4c

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ae;->setAlpha(I)V

    goto :goto_1

    .line 630
    :cond_5
    invoke-static {p1, v1}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    goto :goto_3

    .line 642
    :pswitch_3
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 647
    :pswitch_4
    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:Z

    .line 648
    iput v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    goto :goto_1

    .line 613
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 527
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredWidth()I

    move-result v0

    .line 528
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v1

    .line 529
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    if-nez v2, :cond_2

    .line 533
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a()V

    .line 535
    :cond_2
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 538
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    .line 539
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v3

    .line 540
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v4

    .line 541
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v5

    sub-int v5, v0, v5

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    .line 542
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v6

    sub-int/2addr v1, v6

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v1, v6

    .line 543
    add-int/2addr v5, v3

    add-int/2addr v1, v4

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/view/View;->layout(IIII)V

    .line 544
    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v1}, Landroid/support/v4/widget/CircleImageView;->getMeasuredWidth()I

    move-result v1

    .line 545
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v2}, Landroid/support/v4/widget/CircleImageView;->getMeasuredHeight()I

    move-result v2

    .line 546
    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    div-int/lit8 v4, v0, 0x2

    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v4, v5

    iget v5, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    add-int/2addr v1, v2

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/support/v4/widget/CircleImageView;->layout(IIII)V

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 552
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 553
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    if-nez v0, :cond_0

    .line 554
    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a()V

    .line 556
    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    if-nez v0, :cond_2

    .line 577
    :cond_1
    :goto_0
    return-void

    .line 559
    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->j:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 563
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->x:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/CircleImageView;->measure(II)V

    .line 565
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->m:Z

    if-nez v0, :cond_3

    .line 566
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->m:Z

    .line 567
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v0}, Landroid/support/v4/widget/CircleImageView;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->e:I

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    .line 569
    :cond_3
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->s:I

    .line 571
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 572
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    if-ne v1, v2, :cond_4

    .line 573
    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->s:I

    goto :goto_0

    .line 571
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    .line 674
    invoke-static {p1}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 676
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 682
    :cond_0
    const/4 v0, 0x0

    .line 810
    :goto_0
    return v0

    .line 685
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 810
    :cond_2
    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 687
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    .line 688
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:Z

    goto :goto_1

    .line 692
    :pswitch_2
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 693
    if-gez v0, :cond_3

    .line 694
    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->i:Ljava/lang/String;

    .line 695
    const/4 v0, 0x0

    goto :goto_0

    .line 698
    :cond_3
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 699
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    .line 700
    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:Z

    if-eqz v1, :cond_2

    .line 701
    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/ae;->a(Z)V

    .line 702
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    div-float v1, v0, v1

    .line 703
    const/4 v2, 0x0

    cmpg-float v2, v1, v2

    if-gez v2, :cond_4

    .line 704
    const/4 v0, 0x0

    goto :goto_0

    .line 706
    :cond_4
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 707
    float-to-double v2, v1

    const-wide v4, 0x3fd999999999999aL    # 0.4

    sub-double/2addr v2, v4

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    double-to-float v2, v2

    const/high16 v3, 0x40a00000    # 5.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    .line 708
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    sub-float/2addr v3, v4

    .line 709
    iget v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->g:F

    .line 711
    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v6, v4

    invoke-static {v3, v6}, Ljava/lang/Math;->min(FF)F

    move-result v3

    div-float/2addr v3, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 713
    const/high16 v5, 0x40800000    # 4.0f

    div-float v5, v3, v5

    float-to-double v6, v5

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v3, v5

    float-to-double v8, v3

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    sub-double/2addr v6, v8

    double-to-float v3, v6

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v3, v5

    .line 715
    mul-float v5, v4, v3

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    .line 717
    iget v6, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->e:I

    mul-float/2addr v1, v4

    add-float/2addr v1, v5

    float-to-int v1, v1

    add-int/2addr v1, v6

    .line 720
    iget-object v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v4}, Landroid/support/v4/widget/CircleImageView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_5

    .line 721
    iget-object v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/support/v4/widget/CircleImageView;->setVisibility(I)V

    .line 723
    :cond_5
    iget-object v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v4, v5}, Landroid/support/v4/view/at;->d(Landroid/view/View;F)V

    .line 725
    iget-object v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v4, v5}, Landroid/support/v4/view/at;->e(Landroid/view/View;F)V

    .line 727
    iget v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_9

    .line 728
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v0}, Landroid/support/v4/widget/ae;->getAlpha()I

    move-result v0

    const/16 v4, 0x4c

    if-le v0, v4, :cond_6

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->u:Landroid/view/animation/Animation;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_6

    .line 734
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v0}, Landroid/support/v4/widget/ae;->getAlpha()I

    move-result v0

    const/16 v4, 0x4c

    invoke-direct {p0, v0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(II)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->u:Landroid/view/animation/Animation;

    .line 736
    :cond_6
    const v0, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v2

    .line 737
    iget-object v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    const/4 v5, 0x0

    const v6, 0x3f4ccccd    # 0.8f

    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-virtual {v4, v5, v0}, Landroid/support/v4/widget/ae;->a(FF)V

    .line 738
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget-object v0, v0, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iget v5, v0, Landroid/support/v4/widget/aj;->q:F

    cmpl-float v5, v4, v5

    if-eqz v5, :cond_7

    iput v4, v0, Landroid/support/v4/widget/aj;->q:F

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 746
    :cond_7
    :goto_3
    const/high16 v0, -0x41800000    # -0.25f

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v2, v4

    add-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    .line 747
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    iget-object v2, v2, Landroid/support/v4/widget/ae;->c:Landroid/support/v4/widget/aj;

    iput v0, v2, Landroid/support/v4/widget/aj;->g:F

    iget-object v0, v2, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 748
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    sub-int v0, v1, v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(IZ)V

    goto/16 :goto_1

    .line 728
    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    .line 740
    :cond_9
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v0}, Landroid/support/v4/widget/ae;->getAlpha()I

    move-result v0

    const/16 v4, 0xff

    if-ge v0, v4, :cond_7

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->v:Landroid/view/animation/Animation;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_7

    .line 743
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v0}, Landroid/support/v4/widget/ae;->getAlpha()I

    move-result v0

    const/16 v4, 0xff

    invoke-direct {p0, v0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(II)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->v:Landroid/view/animation/Animation;

    goto :goto_3

    .line 740
    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    .line 754
    :pswitch_3
    invoke-static {p1}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 755
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    goto/16 :goto_1

    .line 760
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 765
    :pswitch_5
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_c

    .line 766
    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    .line 767
    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->i:Ljava/lang/String;

    .line 769
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 771
    :cond_c
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 772
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 773
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->n:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    .line 774
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->o:Z

    .line 775
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->l:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_10

    .line 776
    const/4 v0, 0x1

    const/4 v1, 0x1

    iget-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    if-eq v2, v0, :cond_e

    iput-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:Z

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a()V

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    if-eqz v0, :cond_f

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->y:Landroid/view/animation/Animation$AnimationListener;

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:I

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->z:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->z:Landroid/view/animation/Animation;

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    if-eqz v1, :cond_d

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    iput-object v1, v0, Landroid/support/v4/widget/CircleImageView;->a:Landroid/view/animation/Animation$AnimationListener;

    :cond_d
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v0}, Landroid/support/v4/widget/CircleImageView;->clearAnimation()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CircleImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 805
    :cond_e
    :goto_5
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->p:I

    .line 806
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 776
    :cond_f
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->y:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_5

    .line 779
    :cond_10
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    .line 780
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/ae;->a(FF)V

    .line 782
    new-instance v0, Landroid/support/v4/widget/bh;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/bh;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    .line 802
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->d:I

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->A:Landroid/view/animation/Animation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->reset()V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->A:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->A:Landroid/view/animation/Animation;

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->q:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    if-eqz v0, :cond_11

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    iput-object v0, v1, Landroid/support/v4/widget/CircleImageView;->a:Landroid/view/animation/Animation$AnimationListener;

    :cond_11
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v0}, Landroid/support/v4/widget/CircleImageView;->clearAnimation()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->A:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CircleImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 803
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ae;->a(Z)V

    goto :goto_5

    .line 685
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 666
    return-void
.end method
