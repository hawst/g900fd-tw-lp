.class Landroid/support/v4/widget/ag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Landroid/support/v4/widget/aj;

.field final synthetic b:Landroid/support/v4/widget/ae;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/ae;Landroid/support/v4/widget/aj;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/ae;

    iput-object p2, p0, Landroid/support/v4/widget/ag;->a:Landroid/support/v4/widget/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 364
    iget-object v0, p0, Landroid/support/v4/widget/ag;->a:Landroid/support/v4/widget/aj;

    iget v1, v0, Landroid/support/v4/widget/aj;->e:F

    iput v1, v0, Landroid/support/v4/widget/aj;->l:F

    iget v1, v0, Landroid/support/v4/widget/aj;->f:F

    iput v1, v0, Landroid/support/v4/widget/aj;->m:F

    iget v1, v0, Landroid/support/v4/widget/aj;->g:F

    iput v1, v0, Landroid/support/v4/widget/aj;->n:F

    .line 365
    iget-object v0, p0, Landroid/support/v4/widget/ag;->a:Landroid/support/v4/widget/aj;

    iget v1, v0, Landroid/support/v4/widget/aj;->k:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, v0, Landroid/support/v4/widget/aj;->j:[I

    array-length v2, v2

    rem-int/2addr v1, v2

    iput v1, v0, Landroid/support/v4/widget/aj;->k:I

    .line 366
    iget-object v0, p0, Landroid/support/v4/widget/ag;->a:Landroid/support/v4/widget/aj;

    iget-object v1, p0, Landroid/support/v4/widget/ag;->a:Landroid/support/v4/widget/aj;

    iget v1, v1, Landroid/support/v4/widget/aj;->f:F

    iput v1, v0, Landroid/support/v4/widget/aj;->e:F

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v4}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 367
    iget-object v0, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/ae;

    iget-boolean v0, v0, Landroid/support/v4/widget/ae;->f:Z

    if-eqz v0, :cond_1

    .line 370
    iget-object v0, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/ae;

    iput-boolean v3, v0, Landroid/support/v4/widget/ae;->f:Z

    .line 371
    const-wide/16 v0, 0x535

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 372
    iget-object v0, p0, Landroid/support/v4/widget/ag;->a:Landroid/support/v4/widget/aj;

    iget-boolean v1, v0, Landroid/support/v4/widget/aj;->o:Z

    if-eq v1, v3, :cond_0

    iput-boolean v3, v0, Landroid/support/v4/widget/aj;->o:Z

    iget-object v0, v0, Landroid/support/v4/widget/aj;->d:Landroid/graphics/drawable/Drawable$Callback;

    invoke-interface {v0, v4}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 376
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/ae;

    iget-object v1, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/ae;

    iget v1, v1, Landroid/support/v4/widget/ae;->e:F

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    const/high16 v2, 0x40a00000    # 5.0f

    rem-float/2addr v1, v2

    iput v1, v0, Landroid/support/v4/widget/ae;->e:F

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/ae;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v4/widget/ae;->e:F

    .line 355
    return-void
.end method
