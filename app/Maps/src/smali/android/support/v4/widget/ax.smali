.class Landroid/support/v4/widget/ax;
.super Landroid/support/v4/widget/bn;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v4/widget/SlidingPaneLayout;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
    .locals 0

    .prologue
    .line 1296
    iput-object p1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-direct {p0}, Landroid/support/v4/widget/bn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1362
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 1365
    iget-object v2, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-static {v2}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    if-eqz v1, :cond_1

    .line 1366
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/SlidingPaneLayout;->getWidth()I

    move-result v1

    iget-object v2, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v2}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingRight()I

    move-result v2

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->rightMargin:I

    add-int/2addr v0, v2

    iget-object v2, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v2, v2, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 1368
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->d:I

    sub-int v1, v0, v1

    .line 1369
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1375
    :goto_1
    return v0

    .line 1365
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1371
    :cond_1
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingLeft()I

    move-result v1

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    .line 1372
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->d:I

    add-int/2addr v1, v0

    .line 1373
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/16 v2, 0x20

    .line 1309
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->f:Landroid/support/v4/widget/bk;

    iget v0, v0, Landroid/support/v4/widget/bk;->a:I

    if-nez v0, :cond_0

    .line 1310
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1311
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->a(Landroid/view/View;)V

    .line 1312
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->sendAccessibilityEvent(I)V

    .line 1313
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->g:Z

    .line 1319
    :cond_0
    :goto_0
    return-void

    .line 1315
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->sendAccessibilityEvent(I)V

    .line 1316
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/widget/SlidingPaneLayout;->g:Z

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 1387
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->f:Landroid/support/v4/widget/bk;

    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v1, p2}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;I)V

    .line 1388
    return-void
.end method

.method public final a(Landroid/view/View;F)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    .line 1335
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    .line 1338
    iget-object v2, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-static {v2}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v2

    if-ne v2, v1, :cond_3

    :goto_0
    if-eqz v1, :cond_4

    .line 1339
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingRight()I

    move-result v1

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    .line 1340
    cmpg-float v1, p2, v3

    if-ltz v1, :cond_0

    cmpl-float v1, p2, v3

    if-nez v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->c:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_1

    .line 1341
    :cond_0
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->d:I

    add-int/2addr v0, v1

    .line 1343
    :cond_1
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1344
    iget-object v2, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v2}, Landroid/support/v4/widget/SlidingPaneLayout;->getWidth()I

    move-result v2

    sub-int v0, v2, v0

    sub-int/2addr v0, v1

    .line 1351
    :cond_2
    :goto_1
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->f:Landroid/support/v4/widget/bk;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/widget/bk;->a(II)Z

    .line 1352
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->invalidate()V

    .line 1353
    return-void

    .line 1338
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 1346
    :cond_4
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingLeft()I

    move-result v1

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    .line 1347
    cmpl-float v1, p2, v3

    if-gtz v1, :cond_5

    cmpl-float v1, p2, v3

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->c:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_2

    .line 1348
    :cond_5
    iget-object v1, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget v1, v1, Landroid/support/v4/widget/SlidingPaneLayout;->d:I

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public final a(Landroid/view/View;II)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1329
    iget-object v3, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-object v1, v3, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    iput v0, v3, Landroid/support/v4/widget/SlidingPaneLayout;->c:F

    .line 1330
    :goto_0
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->invalidate()V

    .line 1331
    return-void

    .line 1329
    :cond_0
    invoke-static {v3}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_3

    move v1, v0

    :goto_1
    iget-object v0, v3, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    iget-object v2, v3, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    if-eqz v1, :cond_1

    invoke-virtual {v3}, Landroid/support/v4/widget/SlidingPaneLayout;->getWidth()I

    move-result v4

    sub-int/2addr v4, p2

    sub-int p2, v4, v2

    :cond_1
    if-eqz v1, :cond_4

    invoke-virtual {v3}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingRight()I

    move-result v2

    :goto_2
    if-eqz v1, :cond_5

    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->rightMargin:I

    :goto_3
    add-int/2addr v1, v2

    sub-int v1, p2, v1

    int-to-float v1, v1

    iget v2, v3, Landroid/support/v4/widget/SlidingPaneLayout;->d:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v3, Landroid/support/v4/widget/SlidingPaneLayout;->c:F

    iget-boolean v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, v3, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    iget v1, v3, Landroid/support/v4/widget/SlidingPaneLayout;->c:F

    iget v2, v3, Landroid/support/v4/widget/SlidingPaneLayout;->a:I

    invoke-virtual {v3, v0, v1, v2}, Landroid/support/v4/widget/SlidingPaneLayout;->a(Landroid/view/View;FI)V

    :cond_2
    iget-object v0, v3, Landroid/support/v4/widget/SlidingPaneLayout;->b:Landroid/view/View;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Landroid/support/v4/widget/SlidingPaneLayout;->getPaddingLeft()I

    move-result v2

    goto :goto_2

    :cond_5
    iget v1, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->leftMargin:I

    goto :goto_3
.end method

.method public final a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1300
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->e:Z

    if-eqz v0, :cond_0

    .line 1301
    const/4 v0, 0x0

    .line 1304
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;

    iget-boolean v0, v0, Landroid/support/v4/widget/SlidingPaneLayout$LayoutParams;->b:Z

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1324
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->a()V

    .line 1325
    return-void
.end method

.method public final c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1357
    iget-object v0, p0, Landroid/support/v4/widget/ax;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    iget v0, v0, Landroid/support/v4/widget/SlidingPaneLayout;->d:I

    return v0
.end method

.method public final d(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1382
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method
