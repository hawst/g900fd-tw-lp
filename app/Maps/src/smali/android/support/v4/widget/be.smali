.class Landroid/support/v4/widget/be;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Landroid/support/v4/widget/SwipeRefreshLayout;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    const/16 v2, 0xff

    .line 160
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/ae;->setAlpha(I)V

    .line 163
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v0}, Landroid/support/v4/widget/ae;->start()V

    .line 164
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->h:Z

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 181
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v1}, Landroid/support/v4/widget/CircleImageView;->getTop()I

    move-result v1

    iput v1, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    .line 182
    return-void

    .line 166
    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v0}, Landroid/support/v4/widget/ae;->stop()V

    .line 171
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CircleImageView;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->c:Landroid/support/v4/widget/CircleImageView;

    invoke-virtual {v1}, Landroid/support/v4/widget/CircleImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Landroid/support/v4/widget/ae;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/ae;->setAlpha(I)V

    .line 174
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 175
    iget-object v0, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v1, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->e:I

    iget-object v2, p0, Landroid/support/v4/widget/be;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v2, v2, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    sub-int/2addr v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(IZ)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 152
    return-void
.end method
