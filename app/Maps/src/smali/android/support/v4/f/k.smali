.class Landroid/support/v4/f/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v4/f/m;


# static fields
.field public static final a:Landroid/support/v4/f/k;


# instance fields
.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 235
    new-instance v0, Landroid/support/v4/f/k;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/support/v4/f/k;-><init>(Z)V

    sput-object v0, Landroid/support/v4/f/k;->a:Landroid/support/v4/f/k;

    .line 236
    new-instance v0, Landroid/support/v4/f/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/v4/f/k;-><init>(Z)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-boolean p1, p0, Landroid/support/v4/f/k;->b:Z

    .line 233
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;II)I
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 206
    .line 207
    add-int v5, p2, p3

    move v0, v2

    :goto_0
    if-ge p2, v5, :cond_2

    .line 208
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v4

    packed-switch v4, :pswitch_data_0

    move v4, v3

    :goto_1
    packed-switch v4, :pswitch_data_1

    .line 220
    :goto_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :pswitch_0
    move v4, v1

    .line 208
    goto :goto_1

    :pswitch_1
    move v4, v2

    goto :goto_1

    .line 210
    :pswitch_2
    iget-boolean v0, p0, Landroid/support/v4/f/k;->b:Z

    if-eqz v0, :cond_1

    move v1, v2

    .line 228
    :cond_0
    :goto_3
    return v1

    :cond_1
    move v0, v1

    .line 214
    goto :goto_2

    .line 216
    :pswitch_3
    iget-boolean v0, p0, Landroid/support/v4/f/k;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 219
    goto :goto_2

    .line 225
    :cond_2
    if-eqz v0, :cond_3

    .line 226
    iget-boolean v0, p0, Landroid/support/v4/f/k;->b:Z

    if-nez v0, :cond_0

    move v1, v2

    goto :goto_3

    :cond_3
    move v1, v3

    .line 228
    goto :goto_3

    .line 208
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
