.class Landroid/support/v4/f/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final f:[B


# instance fields
.field final a:Ljava/lang/String;

.field final b:Z

.field final c:I

.field d:I

.field e:C


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x700

    .line 505
    new-array v0, v3, [B

    sput-object v0, Landroid/support/v4/f/c;->f:[B

    .line 506
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 507
    sget-object v1, Landroid/support/v4/f/c;->f:[B

    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v2

    aput-byte v2, v1, v0

    .line 506
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 509
    :cond_0
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550
    iput-object p1, p0, Landroid/support/v4/f/c;->a:Ljava/lang/String;

    .line 551
    iput-boolean p2, p0, Landroid/support/v4/f/c;->b:Z

    .line 552
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Landroid/support/v4/f/c;->c:I

    .line 553
    return-void
.end method

.method static a(C)B
    .locals 1

    .prologue
    .line 724
    const/16 v0, 0x700

    if-ge p0, v0, :cond_0

    sget-object v0, Landroid/support/v4/f/c;->f:[B

    aget-byte v0, v0, p0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method final a()B
    .locals 7

    .prologue
    const/16 v2, 0xd

    const/16 v1, 0xc

    const/16 v6, 0x3e

    const/16 v5, 0x3b

    .line 767
    iget-object v0, p0, Landroid/support/v4/f/c;->a:Ljava/lang/String;

    iget v3, p0, Landroid/support/v4/f/c;->d:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, Landroid/support/v4/f/c;->e:C

    .line 768
    iget-char v0, p0, Landroid/support/v4/f/c;->e:C

    invoke-static {v0}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 769
    iget-object v0, p0, Landroid/support/v4/f/c;->a:Ljava/lang/String;

    iget v1, p0, Landroid/support/v4/f/c;->d:I

    invoke-static {v0, v1}, Ljava/lang/Character;->codePointBefore(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 770
    iget v1, p0, Landroid/support/v4/f/c;->d:I

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Landroid/support/v4/f/c;->d:I

    .line 771
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v0

    .line 783
    :cond_0
    :goto_0
    return v0

    .line 773
    :cond_1
    iget v0, p0, Landroid/support/v4/f/c;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/f/c;->d:I

    .line 774
    iget-char v0, p0, Landroid/support/v4/f/c;->e:C

    const/16 v3, 0x700

    if-ge v0, v3, :cond_3

    sget-object v3, Landroid/support/v4/f/c;->f:[B

    aget-byte v0, v3, v0

    .line 775
    :goto_1
    iget-boolean v3, p0, Landroid/support/v4/f/c;->b:Z

    if-eqz v3, :cond_0

    .line 777
    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    if-ne v3, v6, :cond_8

    .line 778
    iget v0, p0, Landroid/support/v4/f/c;->d:I

    :cond_2
    :goto_2
    iget v3, p0, Landroid/support/v4/f/c;->d:I

    if-lez v3, :cond_7

    iget-object v3, p0, Landroid/support/v4/f/c;->a:Ljava/lang/String;

    iget v4, p0, Landroid/support/v4/f/c;->d:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Landroid/support/v4/f/c;->d:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    iput-char v3, p0, Landroid/support/v4/f/c;->e:C

    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    const/16 v4, 0x3c

    if-ne v3, v4, :cond_4

    move v0, v1

    goto :goto_0

    .line 774
    :cond_3
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v0

    goto :goto_1

    .line 778
    :cond_4
    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    if-eq v3, v6, :cond_7

    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    const/16 v4, 0x22

    if-eq v3, v4, :cond_5

    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    const/16 v4, 0x27

    if-ne v3, v4, :cond_2

    :cond_5
    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    :cond_6
    iget v4, p0, Landroid/support/v4/f/c;->d:I

    if-lez v4, :cond_2

    iget-object v4, p0, Landroid/support/v4/f/c;->a:Ljava/lang/String;

    iget v5, p0, Landroid/support/v4/f/c;->d:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Landroid/support/v4/f/c;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    iput-char v4, p0, Landroid/support/v4/f/c;->e:C

    if-ne v4, v3, :cond_6

    goto :goto_2

    :cond_7
    iput v0, p0, Landroid/support/v4/f/c;->d:I

    iput-char v6, p0, Landroid/support/v4/f/c;->e:C

    move v0, v2

    goto :goto_0

    .line 779
    :cond_8
    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    if-ne v3, v5, :cond_0

    .line 780
    iget v0, p0, Landroid/support/v4/f/c;->d:I

    :cond_9
    iget v3, p0, Landroid/support/v4/f/c;->d:I

    if-lez v3, :cond_b

    iget-object v3, p0, Landroid/support/v4/f/c;->a:Ljava/lang/String;

    iget v4, p0, Landroid/support/v4/f/c;->d:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Landroid/support/v4/f/c;->d:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    iput-char v3, p0, Landroid/support/v4/f/c;->e:C

    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    const/16 v4, 0x26

    if-ne v3, v4, :cond_a

    :goto_3
    move v0, v1

    goto/16 :goto_0

    :cond_a
    iget-char v3, p0, Landroid/support/v4/f/c;->e:C

    if-ne v3, v5, :cond_9

    :cond_b
    iput v0, p0, Landroid/support/v4/f/c;->d:I

    iput-char v5, p0, Landroid/support/v4/f/c;->e:C

    move v1, v2

    goto :goto_3
.end method
