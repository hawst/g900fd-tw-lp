.class Landroid/support/v7/internal/widget/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Landroid/support/v7/internal/widget/AbsSpinnerCompat;


# virtual methods
.method final a()V
    .locals 6

    .prologue
    .line 440
    iget-object v2, p0, Landroid/support/v7/internal/widget/b;->a:Landroid/util/SparseArray;

    .line 441
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 442
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 443
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 444
    if-eqz v0, :cond_0

    .line 445
    iget-object v4, p0, Landroid/support/v7/internal/widget/b;->b:Landroid/support/v7/internal/widget/AbsSpinnerCompat;

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Landroid/support/v7/internal/widget/AbsSpinnerCompat;->a(Landroid/support/v7/internal/widget/AbsSpinnerCompat;Landroid/view/View;Z)V

    .line 442
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 448
    :cond_1
    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 449
    return-void
.end method
