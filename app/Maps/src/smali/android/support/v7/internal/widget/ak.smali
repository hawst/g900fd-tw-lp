.class public Landroid/support/v7/internal/widget/ak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/internal/widget/w;


# instance fields
.field a:Landroid/support/v7/widget/Toolbar;

.field b:Ljava/lang/CharSequence;

.field private c:I

.field private d:Landroid/view/View;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/lang/CharSequence;

.field private final j:Landroid/support/v7/internal/widget/ag;

.field private k:I

.field private l:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Z)V
    .locals 2

    .prologue
    .line 88
    sget v0, Landroid/support/v7/a/i;->a:I

    sget v1, Landroid/support/v7/a/e;->g:I

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v7/internal/widget/ak;-><init>(Landroid/support/v7/widget/Toolbar;ZII)V

    .line 90
    return-void
.end method

.method private constructor <init>(Landroid/support/v7/widget/Toolbar;ZII)V
    .locals 7

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput v6, p0, Landroid/support/v7/internal/widget/ak;->k:I

    .line 94
    iput-object p1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    .line 95
    iget-object v0, p1, Landroid/support/v7/widget/Toolbar;->k:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->b:Ljava/lang/CharSequence;

    .line 96
    iget-object v0, p1, Landroid/support/v7/widget/Toolbar;->l:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->h:Ljava/lang/CharSequence;

    .line 97
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->b:Ljava/lang/CharSequence;

    .line 99
    if-eqz p2, :cond_12

    .line 100
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Landroid/support/v7/a/j;->a:[I

    sget v3, Landroid/support/v7/a/b;->c:I

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    new-instance v3, Landroid/support/v7/internal/widget/aj;

    invoke-direct {v3, v0, v2}, Landroid/support/v7/internal/widget/aj;-><init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 103
    iget-object v0, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 104
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 105
    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->b:Ljava/lang/CharSequence;

    iget v2, p0, Landroid/support/v7/internal/widget/ak;->c:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    sget v0, Landroid/support/v7/a/j;->n:I

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 109
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 110
    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->h:Ljava/lang/CharSequence;

    iget v2, p0, Landroid/support/v7/internal/widget/ak;->c:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 113
    :cond_1
    sget v0, Landroid/support/v7/a/j;->l:I

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/aj;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_2

    .line 115
    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->f:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ak;->b()V

    .line 118
    :cond_2
    sget v0, Landroid/support/v7/a/j;->k:I

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/aj;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_3

    .line 120
    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->e:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ak;->b()V

    .line 123
    :cond_3
    sget v0, Landroid/support/v7/a/j;->j:I

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/aj;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_4

    .line 125
    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->g:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ak;->d()V

    .line 128
    :cond_4
    sget v0, Landroid/support/v7/a/j;->h:I

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/widget/ak;->a(I)V

    .line 130
    sget v0, Landroid/support/v7/a/j;->g:I

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 132
    if-eqz v0, :cond_7

    .line 133
    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iget-object v4, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v0, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->d:Landroid/view/View;

    if-eqz v2, :cond_5

    iget v2, p0, Landroid/support/v7/internal/widget/ak;->c:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_5

    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Landroid/support/v7/internal/widget/ak;->d:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    :cond_5
    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->d:Landroid/view/View;

    if-eqz v0, :cond_6

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 135
    :cond_6
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    or-int/lit8 v0, v0, 0x10

    invoke-direct {p0, v0}, Landroid/support/v7/internal/widget/ak;->a(I)V

    .line 138
    :cond_7
    sget v0, Landroid/support/v7/a/j;->i:I

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    .line 139
    if-lez v0, :cond_8

    .line 140
    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 141
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 142
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/Toolbar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    :cond_8
    sget v0, Landroid/support/v7/a/j;->f:I

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    .line 147
    sget v2, Landroid/support/v7/a/j;->e:I

    iget-object v4, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v4, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    .line 149
    if-gez v0, :cond_9

    if-ltz v2, :cond_a

    .line 150
    :cond_9
    iget-object v4, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v4, v4, Landroid/support/v7/widget/Toolbar;->j:Landroid/support/v7/internal/widget/z;

    invoke-virtual {v4, v0, v2}, Landroid/support/v7/internal/widget/z;->a(II)V

    .line 154
    :cond_a
    sget v0, Landroid/support/v7/a/j;->q:I

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 155
    if-eqz v0, :cond_b

    .line 156
    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    iput v0, v2, Landroid/support/v7/widget/Toolbar;->g:I

    iget-object v5, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/widget/TextView;

    if-eqz v5, :cond_b

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v4, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 159
    :cond_b
    sget v0, Landroid/support/v7/a/j;->o:I

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 161
    if-eqz v0, :cond_c

    .line 162
    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    iput v0, v2, Landroid/support/v7/widget/Toolbar;->h:I

    iget-object v5, v2, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/TextView;

    if-eqz v5, :cond_c

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v4, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 165
    :cond_c
    sget v0, Landroid/support/v7/a/j;->m:I

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 166
    if-eqz v0, :cond_d

    .line 167
    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/Toolbar;->setPopupTheme(I)V

    .line 170
    :cond_d
    iget-object v0, v3, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 172
    iget-object v0, v3, Landroid/support/v7/internal/widget/aj;->c:Landroid/support/v7/internal/widget/ag;

    if-nez v0, :cond_e

    new-instance v0, Landroid/support/v7/internal/widget/ag;

    iget-object v2, v3, Landroid/support/v7/internal/widget/aj;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/support/v7/internal/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, v3, Landroid/support/v7/internal/widget/aj;->c:Landroid/support/v7/internal/widget/ag;

    :cond_e
    iget-object v0, v3, Landroid/support/v7/internal/widget/aj;->c:Landroid/support/v7/internal/widget/ag;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->j:Landroid/support/v7/internal/widget/ag;

    .line 179
    :goto_0
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->k:I

    if-eq p3, v0, :cond_f

    iput p3, p0, Landroid/support/v7/internal/widget/ak;->k:I

    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageButton;

    if-eqz v2, :cond_15

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->k:I

    if-nez v0, :cond_16

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->i:Ljava/lang/CharSequence;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ak;->c()V

    .line 180
    :cond_f
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageButton;

    if-eqz v2, :cond_10

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    :cond_10
    iput-object v1, p0, Landroid/support/v7/internal/widget/ak;->i:Ljava/lang/CharSequence;

    .line 182
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->j:Landroid/support/v7/internal/widget/ag;

    invoke-virtual {v0, p4}, Landroid/support/v7/internal/widget/ag;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->l:Landroid/graphics/drawable/Drawable;

    if-eq v1, v0, :cond_11

    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->l:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/internal/widget/ak;->d()V

    .line 184
    :cond_11
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Landroid/support/v7/internal/widget/al;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/al;-><init>(Landroid/support/v7/internal/widget/ak;)V

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->a()V

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    return-void

    .line 174
    :cond_12
    const/16 v0, 0xb

    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v3, v2, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageButton;

    if-eqz v3, :cond_14

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :goto_3
    if-eqz v2, :cond_13

    const/16 v0, 0xf

    :cond_13
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    .line 176
    new-instance v0, Landroid/support/v7/internal/widget/ag;

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/internal/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ak;->j:Landroid/support/v7/internal/widget/ag;

    goto :goto_0

    :cond_14
    move-object v2, v1

    .line 174
    goto :goto_3

    :cond_15
    move-object v0, v1

    .line 179
    goto :goto_1

    :cond_16
    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 429
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    .line 430
    xor-int/2addr v0, p1

    .line 431
    iput p1, p0, Landroid/support/v7/internal/widget/ak;->c:I

    .line 432
    if-eqz v0, :cond_3

    .line 433
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 434
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_4

    .line 435
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ak;->d()V

    .line 436
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ak;->c()V

    .line 442
    :cond_0
    :goto_0
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_1

    .line 443
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ak;->b()V

    .line 446
    :cond_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    .line 447
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_5

    .line 448
    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 449
    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ak;->h:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 456
    :cond_2
    :goto_1
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->d:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 457
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_6

    .line 458
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 464
    :cond_3
    :goto_2
    return-void

    .line 438
    :cond_4
    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 451
    :cond_5
    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 452
    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 460
    :cond_6
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_2
.end method

.method private b()V
    .locals 4

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    iget v1, p0, Landroid/support/v7/internal/widget/ak;->c:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 368
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 369
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->f:Landroid/graphics/drawable/Drawable;

    .line 374
    :cond_0
    :goto_0
    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_6

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    if-nez v2, :cond_1

    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    :cond_1
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)V

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;)V

    :cond_2
    :goto_1
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    if-eqz v2, :cond_3

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 375
    :cond_3
    return-void

    .line 369
    :cond_4
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 371
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 374
    :cond_6
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 654
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->i:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 656
    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->k:I

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    .line 661
    :cond_0
    :goto_1
    return-void

    .line 656
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 658
    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->i:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 664
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 665
    iget-object v1, p0, Landroid/support/v7/internal/widget/ak;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->g:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 667
    :cond_0
    return-void

    .line 665
    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/ak;->l:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return v0
.end method
