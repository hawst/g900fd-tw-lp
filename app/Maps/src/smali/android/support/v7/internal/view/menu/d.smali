.class public abstract Landroid/support/v7/internal/view/menu/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/internal/view/menu/j;


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/support/v7/internal/view/menu/e;


# virtual methods
.method public a(Landroid/support/v7/internal/view/menu/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 177
    instance-of v0, p2, Landroid/support/v7/internal/view/menu/m;

    if-eqz v0, :cond_0

    .line 178
    check-cast p2, Landroid/support/v7/internal/view/menu/m;

    move-object v0, p2

    .line 182
    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/g;Landroid/support/v7/internal/view/menu/m;)V

    .line 183
    check-cast v0, Landroid/view/View;

    return-object v0

    .line 180
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v0, v1, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/e;)V
    .locals 1

    .prologue
    .line 66
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/d;->a:Landroid/content/Context;

    .line 67
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/d;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 68
    iput-object p2, p0, Landroid/support/v7/internal/view/menu/d;->b:Landroid/support/v7/internal/view/menu/e;

    .line 69
    return-void
.end method

.method public a(Landroid/support/v7/internal/view/menu/e;Z)V
    .locals 0

    .prologue
    .line 206
    return-void
.end method

.method public abstract a(Landroid/support/v7/internal/view/menu/g;Landroid/support/v7/internal/view/menu/m;)V
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/g;)Z
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/v7/internal/view/menu/n;)Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public final b(Landroid/support/v7/internal/view/menu/g;)Z
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x0

    return v0
.end method
