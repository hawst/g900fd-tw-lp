.class Landroid/support/v7/internal/widget/s;
.super Landroid/database/DataSetObserver;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v7/internal/widget/ActivityChooserView;


# direct methods
.method constructor <init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Landroid/support/v7/internal/widget/s;->a:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 248
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 249
    iget-object v0, p0, Landroid/support/v7/internal/widget/s;->a:Landroid/support/v7/internal/widget/ActivityChooserView;

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/t;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    :goto_0
    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-object v1, v1, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/k;->a()I

    move-result v1

    iget-object v2, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-object v2, v2, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/k;->c()I

    move-result v2

    if-eq v1, v5, :cond_0

    if-le v1, v5, :cond_3

    if-lez v2, :cond_3

    :cond_0
    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-object v1, v1, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/k;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v3, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->m:I

    if-eqz v3, :cond_1

    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->m:I

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->b:Landroid/support/v7/widget/LinearLayoutCompat;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/LinearLayoutCompat;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 250
    :goto_2
    return-void

    .line 249
    :cond_2
    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v6}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    iget-object v1, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    :cond_4
    iget-object v0, v0, Landroid/support/v7/internal/widget/ActivityChooserView;->b:Landroid/support/v7/widget/LinearLayoutCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutCompat;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method
