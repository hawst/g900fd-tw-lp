.class public Landroid/support/v7/internal/widget/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/cw;


# instance fields
.field final synthetic a:Landroid/support/v7/internal/widget/AbsActionBarView;

.field private b:Z


# direct methods
.method protected constructor <init>(Landroid/support/v7/internal/widget/AbsActionBarView;)V
    .locals 1

    .prologue
    .line 255
    iput-object p1, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/AbsActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/a;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 268
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/AbsActionBarView;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/AbsActionBarView;->setVisibility(I)V

    .line 269
    iput-boolean v1, p0, Landroid/support/v7/internal/widget/a;->b:Z

    .line 270
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 274
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/a;->b:Z

    if-eqz v0, :cond_0

    .line 279
    :goto_0
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/AbsActionBarView;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/internal/widget/AbsActionBarView;->c:Landroid/support/v4/view/cj;

    .line 277
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/AbsActionBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/AbsActionBarView;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/AbsActionBarView;

    goto :goto_0
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/a;->b:Z

    .line 286
    return-void
.end method
