.class public Landroid/support/v7/internal/widget/aj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/res/TypedArray;

.field public c:Landroid/support/v7/internal/widget/ag;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Landroid/support/v7/internal/widget/aj;->a:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    .line 56
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 61
    if-eqz v0, :cond_1

    .line 62
    iget-object v1, p0, Landroid/support/v7/internal/widget/aj;->c:Landroid/support/v7/internal/widget/ag;

    if-nez v1, :cond_0

    new-instance v1, Landroid/support/v7/internal/widget/ag;

    iget-object v2, p0, Landroid/support/v7/internal/widget/aj;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v7/internal/widget/ag;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/aj;->c:Landroid/support/v7/internal/widget/ag;

    :cond_0
    iget-object v1, p0, Landroid/support/v7/internal/widget/aj;->c:Landroid/support/v7/internal/widget/ag;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ag;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/aj;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
