.class Landroid/support/v7/internal/widget/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;


# direct methods
.method constructor <init>(Landroid/support/v7/internal/widget/ActionBarOverlayLayout;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 135
    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a()V

    .line 136
    iget-object v1, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v3, v0

    iget-object v0, v2, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v4, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v4, v2, v0, v3}, Landroid/support/v4/view/cr;->c(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v3, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->f:Landroid/support/v4/view/cw;

    iget-object v0, v2, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    sget-object v4, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v4, v2, v0, v3}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    :cond_1
    iput-object v2, v1, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->d:Landroid/support/v4/view/cj;

    .line 139
    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    .line 140
    iget-object v1, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, v2, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_2

    sget-object v4, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v4, v2, v0, v3}, Landroid/support/v4/view/cr;->c(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/j;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v3, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->g:Landroid/support/v4/view/cw;

    iget-object v0, v2, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_3

    sget-object v4, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v4, v2, v0, v3}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    :cond_3
    iput-object v2, v1, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->e:Landroid/support/v4/view/cj;

    .line 144
    :cond_4
    return-void
.end method
