.class public Landroid/support/v7/internal/widget/ActivityChooserView;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field public final a:Landroid/support/v7/internal/widget/t;

.field final b:Landroid/support/v7/widget/LinearLayoutCompat;

.field final c:Landroid/graphics/drawable/Drawable;

.field final d:Landroid/widget/FrameLayout;

.field public final e:Landroid/widget/ImageView;

.field final f:Landroid/widget/FrameLayout;

.field final g:Landroid/widget/ImageView;

.field public h:Landroid/support/v4/view/n;

.field public final i:Landroid/database/DataSetObserver;

.field public j:Z

.field public k:I

.field public l:Z

.field public m:I

.field private final n:Landroid/support/v7/internal/widget/u;

.field private final o:I

.field private final p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private q:Landroid/support/v7/widget/ListPopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 211
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 124
    new-instance v0, Landroid/support/v7/internal/widget/q;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/q;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/database/DataSetObserver;

    .line 138
    new-instance v0, Landroid/support/v7/internal/widget/r;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/r;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 172
    iput v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->k:I

    .line 213
    sget-object v0, Landroid/support/v7/a/j;->C:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 216
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->k:I

    .line 220
    sget v1, Landroid/support/v7/a/j;->D:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 223
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 225
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 226
    sget v2, Landroid/support/v7/a/h;->b:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 228
    new-instance v0, Landroid/support/v7/internal/widget/u;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/u;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/internal/widget/u;

    .line 230
    sget v0, Landroid/support/v7/a/f;->e:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/LinearLayoutCompat;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->b:Landroid/support/v7/widget/LinearLayoutCompat;

    .line 231
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->b:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutCompat;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/graphics/drawable/Drawable;

    .line 233
    sget v0, Landroid/support/v7/a/f;->f:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    .line 234
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/internal/widget/u;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/internal/widget/u;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 236
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    sget v2, Landroid/support/v7/a/f;->j:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->g:Landroid/widget/ImageView;

    .line 238
    sget v0, Landroid/support/v7/a/f;->h:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/widget/FrameLayout;

    .line 239
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/widget/FrameLayout;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/internal/widget/u;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/widget/FrameLayout;

    sget v2, Landroid/support/v7/a/f;->j:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/widget/ImageView;

    .line 242
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 244
    new-instance v0, Landroid/support/v7/internal/widget/t;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/t;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    .line 245
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    new-instance v1, Landroid/support/v7/internal/widget/s;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/s;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/t;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 253
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Landroid/support/v7/a/d;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->o:I

    .line 256
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 327
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-object v0, v0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/k;

    if-nez v0, :cond_0

    .line 328
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data model. Did you call #setDataModel?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 331
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 333
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 336
    :goto_0
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-object v3, v3, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v3}, Landroid/support/v7/internal/widget/k;->a()I

    move-result v4

    .line 337
    if-eqz v0, :cond_9

    move v3, v1

    .line 338
    :goto_1
    const v5, 0x7fffffff

    if-eq p1, v5, :cond_a

    add-int/2addr v3, p1

    if-le v4, v3, :cond_a

    .line 340
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-boolean v4, v3, Landroid/support/v7/internal/widget/t;->e:Z

    if-eq v4, v1, :cond_1

    iput-boolean v1, v3, Landroid/support/v7/internal/widget/t;->e:Z

    invoke-virtual {v3}, Landroid/support/v7/internal/widget/t;->notifyDataSetChanged()V

    .line 341
    :cond_1
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    add-int/lit8 v4, p1, -0x1

    iget v5, v3, Landroid/support/v7/internal/widget/t;->b:I

    if-eq v5, v4, :cond_2

    iput v4, v3, Landroid/support/v7/internal/widget/t;->b:I

    invoke-virtual {v3}, Landroid/support/v7/internal/widget/t;->notifyDataSetChanged()V

    .line 347
    :cond_2
    :goto_2
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->b()Landroid/support/v7/widget/ListPopupWindow;

    move-result-object v3

    .line 348
    iget-object v4, v3, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    if-nez v4, :cond_7

    .line 349
    iget-boolean v4, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->j:Z

    if-nez v4, :cond_3

    if-nez v0, :cond_c

    .line 350
    :cond_3
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-boolean v4, v2, Landroid/support/v7/internal/widget/t;->c:Z

    if-ne v4, v1, :cond_4

    iget-boolean v4, v2, Landroid/support/v7/internal/widget/t;->d:Z

    if-eq v4, v0, :cond_5

    :cond_4
    iput-boolean v1, v2, Landroid/support/v7/internal/widget/t;->c:Z

    iput-boolean v0, v2, Landroid/support/v7/internal/widget/t;->d:Z

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/t;->notifyDataSetChanged()V

    .line 354
    :cond_5
    :goto_3
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/t;->a()I

    move-result v0

    iget v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->o:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 355
    invoke-virtual {v3, v0}, Landroid/support/v7/widget/ListPopupWindow;->a(I)V

    .line 356
    invoke-virtual {v3}, Landroid/support/v7/widget/ListPopupWindow;->a()V

    .line 357
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->h:Landroid/support/v4/view/n;

    if-eqz v0, :cond_6

    .line 358
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->h:Landroid/support/v4/view/n;

    iget-object v2, v0, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/o;

    if-eqz v2, :cond_6

    iget-object v0, v0, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/o;

    invoke-interface {v0, v1}, Landroid/support/v4/view/o;->a(Z)V

    .line 360
    :cond_6
    iget-object v0, v3, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Landroid/support/v7/a/i;->c:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 363
    :cond_7
    return-void

    :cond_8
    move v0, v2

    .line 333
    goto :goto_0

    :cond_9
    move v3, v2

    .line 337
    goto :goto_1

    .line 343
    :cond_a
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-boolean v4, v3, Landroid/support/v7/internal/widget/t;->e:Z

    if-eq v4, v2, :cond_b

    iput-boolean v2, v3, Landroid/support/v7/internal/widget/t;->e:Z

    invoke-virtual {v3}, Landroid/support/v7/internal/widget/t;->notifyDataSetChanged()V

    .line 344
    :cond_b
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget v4, v3, Landroid/support/v7/internal/widget/t;->b:I

    if-eq v4, p1, :cond_2

    iput p1, v3, Landroid/support/v7/internal/widget/t;->b:I

    invoke-virtual {v3}, Landroid/support/v7/internal/widget/t;->notifyDataSetChanged()V

    goto :goto_2

    .line 352
    :cond_c
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-boolean v4, v0, Landroid/support/v7/internal/widget/t;->c:Z

    if-ne v4, v2, :cond_d

    iget-boolean v4, v0, Landroid/support/v7/internal/widget/t;->d:Z

    if-eq v4, v2, :cond_5

    :cond_d
    iput-boolean v2, v0, Landroid/support/v7/internal/widget/t;->c:Z

    iput-boolean v2, v0, Landroid/support/v7/internal/widget/t;->d:Z

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/t;->notifyDataSetChanged()V

    goto :goto_3
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 371
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->b()Landroid/support/v7/widget/ListPopupWindow;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->b()Landroid/support/v7/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/ListPopupWindow;->b()V

    .line 373
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 374
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 375
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 378
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b()Landroid/support/v7/widget/ListPopupWindow;
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:Landroid/support/v7/widget/ListPopupWindow;

    if-nez v0, :cond_0

    .line 485
    new-instance v0, Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:Landroid/support/v7/widget/ListPopupWindow;

    .line 486
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ListPopupWindow;->a(Landroid/widget/ListAdapter;)V

    .line 487
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:Landroid/support/v7/widget/ListPopupWindow;

    iput-object p0, v0, Landroid/support/v7/widget/ListPopupWindow;->f:Landroid/view/View;

    .line 488
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:Landroid/support/v7/widget/ListPopupWindow;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/ListPopupWindow;->j:Z

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 489
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/internal/widget/u;

    iput-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->g:Landroid/widget/AdapterView$OnItemClickListener;

    .line 490
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/internal/widget/u;

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 492
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:Landroid/support/v7/widget/ListPopupWindow;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 392
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 393
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-object v0, v0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/k;

    .line 394
    if-eqz v0, :cond_0

    .line 395
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/k;->registerObserver(Ljava/lang/Object;)V

    .line 397
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->l:Z

    .line 398
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 402
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 403
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v7/internal/widget/t;

    iget-object v0, v0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/k;

    .line 404
    if-eqz v0, :cond_0

    .line 405
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/k;->unregisterObserver(Ljava/lang/Object;)V

    .line 407
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 408
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 409
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->p:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 411
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->b()Landroid/support/v7/widget/ListPopupWindow;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 412
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->a()Z

    .line 414
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->l:Z

    .line 415
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 433
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->b:Landroid/support/v7/widget/LinearLayoutCompat;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/support/v7/widget/LinearLayoutCompat;->layout(IIII)V

    .line 434
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->b()Landroid/support/v7/widget/ListPopupWindow;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->a()Z

    .line 437
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 419
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->b:Landroid/support/v7/widget/LinearLayoutCompat;

    .line 423
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 424
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 427
    :cond_0
    invoke-virtual {p0, v0, p1, p2}, Landroid/support/v7/internal/widget/ActivityChooserView;->measureChild(Landroid/view/View;II)V

    .line 428
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->setMeasuredDimension(II)V

    .line 429
    return-void
.end method
