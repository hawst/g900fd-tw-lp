.class public Landroid/support/v7/app/g;
.super Landroid/support/v4/app/i;
.source "PG"


# instance fields
.field ag:Landroid/support/v7/c/l;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/i;->c:Z

    iget-object v1, p0, Landroid/support/v4/app/i;->f:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/i;->f:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 45
    :cond_0
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    new-instance v1, Landroid/support/v7/app/c;

    invoke-direct {v1, v0}, Landroid/support/v7/app/c;-><init>(Landroid/content/Context;)V

    .line 112
    invoke-virtual {p0}, Landroid/support/v7/app/g;->m()V

    iget-object v0, p0, Landroid/support/v7/app/g;->ag:Landroid/support/v7/c/l;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/c;->a(Landroid/support/v7/c/l;)V

    .line 113
    return-object v1
.end method

.method m()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/app/g;->ag:Landroid/support/v7/c/l;

    if-nez v0, :cond_1

    .line 59
    iget-object v0, p0, Landroid/support/v4/app/j;->r:Landroid/os/Bundle;

    .line 60
    if-eqz v0, :cond_0

    .line 61
    const-string v1, "selector"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/c/l;->a(Landroid/os/Bundle;)Landroid/support/v7/c/l;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/g;->ag:Landroid/support/v7/c/l;

    .line 63
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/g;->ag:Landroid/support/v7/c/l;

    if-nez v0, :cond_1

    .line 64
    sget-object v0, Landroid/support/v7/c/l;->c:Landroid/support/v7/c/l;

    iput-object v0, p0, Landroid/support/v7/app/g;->ag:Landroid/support/v7/c/l;

    .line 67
    :cond_1
    return-void
.end method
