.class Landroid/support/v7/app/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Landroid/support/v7/app/h;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/support/v7/app/h;)V
    .locals 1

    .prologue
    .line 144
    iput-object p1, p0, Landroid/support/v7/app/i;->a:Landroid/support/v7/app/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v0, Landroid/support/v7/app/j;

    invoke-direct {v0, p0}, Landroid/support/v7/app/j;-><init>(Landroid/support/v7/app/i;)V

    iput-object v0, p0, Landroid/support/v7/app/i;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    .prologue
    .line 174
    if-eqz p3, :cond_0

    .line 175
    iget-object v0, p0, Landroid/support/v7/app/i;->a:Landroid/support/v7/app/h;

    iget-object v0, v0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    invoke-virtual {v0, p2}, Landroid/support/v7/c/w;->a(I)V

    .line 177
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Landroid/support/v7/app/i;->a:Landroid/support/v7/app/h;

    iget-boolean v0, v0, Landroid/support/v7/app/h;->d:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Landroid/support/v7/app/i;->a:Landroid/support/v7/app/h;

    iget-object v0, v0, Landroid/support/v7/app/h;->c:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/app/i;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/i;->a:Landroid/support/v7/app/h;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/h;->d:Z

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Landroid/support/v7/app/i;->a:Landroid/support/v7/app/h;

    iget-object v0, v0, Landroid/support/v7/app/h;->c:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/app/i;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/SeekBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 170
    return-void
.end method
