.class final Landroid/support/v7/app/a;
.super Landroid/support/v7/c/o;
.source "PG"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v7/app/MediaRouteActionProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v7/app/MediaRouteActionProvider;)V
    .locals 1

    .prologue
    .line 289
    invoke-direct {p0}, Landroid/support/v7/c/o;-><init>()V

    .line 290
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v7/app/a;->a:Ljava/lang/ref/WeakReference;

    .line 291
    return-void
.end method

.method private g(Landroid/support/v7/c/n;)V
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Landroid/support/v7/app/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteActionProvider;

    .line 325
    if-eqz v0, :cond_0

    .line 330
    :goto_0
    return-void

    .line 328
    :cond_0
    invoke-virtual {p1, p0}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/o;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v7/c/n;)V
    .locals 0

    .prologue
    .line 295
    invoke-direct {p0, p1}, Landroid/support/v7/app/a;->g(Landroid/support/v7/c/n;)V

    .line 296
    return-void
.end method

.method public final b(Landroid/support/v7/c/n;)V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0, p1}, Landroid/support/v7/app/a;->g(Landroid/support/v7/c/n;)V

    .line 301
    return-void
.end method

.method public final c(Landroid/support/v7/c/n;)V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0, p1}, Landroid/support/v7/app/a;->g(Landroid/support/v7/c/n;)V

    .line 306
    return-void
.end method

.method public final d(Landroid/support/v7/c/n;)V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0, p1}, Landroid/support/v7/app/a;->g(Landroid/support/v7/c/n;)V

    .line 311
    return-void
.end method

.method public final e(Landroid/support/v7/c/n;)V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0, p1}, Landroid/support/v7/app/a;->g(Landroid/support/v7/c/n;)V

    .line 316
    return-void
.end method

.method public final f(Landroid/support/v7/c/n;)V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0, p1}, Landroid/support/v7/app/a;->g(Landroid/support/v7/c/n;)V

    .line 321
    return-void
.end method
