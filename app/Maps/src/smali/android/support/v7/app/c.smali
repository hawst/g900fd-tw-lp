.class public Landroid/support/v7/app/c;
.super Landroid/app/Dialog;
.source "PG"


# instance fields
.field private final a:Landroid/support/v7/c/n;

.field private final b:Landroid/support/v7/app/d;

.field private c:Landroid/support/v7/c/l;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/w;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/support/v7/app/e;

.field private f:Landroid/widget/ListView;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/c;-><init>(Landroid/content/Context;I)V

    .line 62
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/support/v7/app/o;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 54
    sget-object v0, Landroid/support/v7/c/l;->c:Landroid/support/v7/c/l;

    iput-object v0, p0, Landroid/support/v7/app/c;->c:Landroid/support/v7/c/l;

    .line 66
    invoke-virtual {p0}, Landroid/support/v7/app/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 68
    invoke-static {v0}, Landroid/support/v7/c/n;->a(Landroid/content/Context;)Landroid/support/v7/c/n;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/c;->a:Landroid/support/v7/c/n;

    .line 69
    new-instance v0, Landroid/support/v7/app/d;

    invoke-direct {v0, p0}, Landroid/support/v7/app/d;-><init>(Landroid/support/v7/app/c;)V

    iput-object v0, p0, Landroid/support/v7/app/c;->b:Landroid/support/v7/app/d;

    .line 70
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 180
    iget-boolean v0, p0, Landroid/support/v7/app/c;->g:Z

    if-eqz v0, :cond_5

    .line 181
    iget-object v0, p0, Landroid/support/v7/app/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 182
    iget-object v0, p0, Landroid/support/v7/app/c;->d:Ljava/util/ArrayList;

    iget-object v3, p0, Landroid/support/v7/app/c;->a:Landroid/support/v7/c/n;

    invoke-static {}, Landroid/support/v7/c/n;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 183
    iget-object v5, p0, Landroid/support/v7/app/c;->d:Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v4, v0, -0x1

    if-lez v0, :cond_4

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/w;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    sget-object v3, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v6, v3, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-nez v6, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v3, v3, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-ne v3, v0, :cond_1

    move v3, v1

    :goto_1
    if-nez v3, :cond_3

    iget-boolean v3, v0, Landroid/support/v7/c/w;->f:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/app/c;->c:Landroid/support/v7/c/l;

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v0, v0, Landroid/support/v7/c/w;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Landroid/support/v7/c/l;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-nez v0, :cond_6

    invoke-interface {v5, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v4

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 184
    :cond_4
    iget-object v0, p0, Landroid/support/v7/app/c;->d:Ljava/util/ArrayList;

    sget-object v1, Landroid/support/v7/app/f;->a:Landroid/support/v7/app/f;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 185
    iget-object v0, p0, Landroid/support/v7/app/c;->e:Landroid/support/v7/app/e;

    invoke-virtual {v0}, Landroid/support/v7/app/e;->notifyDataSetChanged()V

    .line 187
    :cond_5
    return-void

    :cond_6
    move v0, v4

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/c/l;)V
    .locals 3

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/c;->c:Landroid/support/v7/c/l;

    invoke-virtual {v0, p1}, Landroid/support/v7/c/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 93
    iput-object p1, p0, Landroid/support/v7/app/c;->c:Landroid/support/v7/c/l;

    .line 95
    iget-boolean v0, p0, Landroid/support/v7/app/c;->g:Z

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Landroid/support/v7/app/c;->a:Landroid/support/v7/c/n;

    iget-object v1, p0, Landroid/support/v7/app/c;->b:Landroid/support/v7/app/d;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/o;)V

    .line 97
    iget-object v0, p0, Landroid/support/v7/app/c;->a:Landroid/support/v7/c/n;

    iget-object v1, p0, Landroid/support/v7/app/c;->b:Landroid/support/v7/app/d;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/l;Landroid/support/v7/c/o;I)V

    .line 101
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/app/c;->a()V

    .line 103
    :cond_2
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 161
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 163
    iput-boolean v3, p0, Landroid/support/v7/app/c;->g:Z

    .line 164
    iget-object v0, p0, Landroid/support/v7/app/c;->a:Landroid/support/v7/c/n;

    iget-object v1, p0, Landroid/support/v7/app/c;->c:Landroid/support/v7/c/l;

    iget-object v2, p0, Landroid/support/v7/app/c;->b:Landroid/support/v7/app/d;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/l;Landroid/support/v7/c/o;I)V

    .line 165
    invoke-virtual {p0}, Landroid/support/v7/app/c;->a()V

    .line 166
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 139
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 141
    invoke-virtual {p0}, Landroid/support/v7/app/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/Window;->requestFeature(I)Z

    .line 143
    sget v0, Landroid/support/v7/d/d;->a:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/c;->setContentView(I)V

    .line 144
    sget v0, Landroid/support/v7/d/e;->a:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/c;->setTitle(I)V

    .line 147
    invoke-virtual {p0}, Landroid/support/v7/app/c;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/app/c;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Landroid/support/v7/d/b;->d:I

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, v3, Landroid/util/TypedValue;->resourceId:I

    :goto_0
    invoke-virtual {v1, v5, v0}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/app/c;->d:Ljava/util/ArrayList;

    .line 152
    new-instance v0, Landroid/support/v7/app/e;

    invoke-virtual {p0}, Landroid/support/v7/app/c;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/app/c;->d:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Landroid/support/v7/app/e;-><init>(Landroid/support/v7/app/c;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Landroid/support/v7/app/c;->e:Landroid/support/v7/app/e;

    .line 153
    sget v0, Landroid/support/v7/d/c;->c:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Landroid/support/v7/app/c;->f:Landroid/widget/ListView;

    .line 154
    iget-object v0, p0, Landroid/support/v7/app/c;->f:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v7/app/c;->e:Landroid/support/v7/app/e;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 155
    iget-object v0, p0, Landroid/support/v7/app/c;->f:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v7/app/c;->e:Landroid/support/v7/app/e;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 156
    iget-object v0, p0, Landroid/support/v7/app/c;->f:Landroid/widget/ListView;

    const v1, 0x1020004

    invoke-virtual {p0, v1}, Landroid/support/v7/app/c;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 157
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/c;->g:Z

    .line 171
    iget-object v0, p0, Landroid/support/v7/app/c;->a:Landroid/support/v7/c/n;

    iget-object v1, p0, Landroid/support/v7/app/c;->b:Landroid/support/v7/app/d;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/o;)V

    .line 173
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 174
    return-void
.end method
