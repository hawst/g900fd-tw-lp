.class public Landroid/support/v7/app/h;
.super Landroid/app/Dialog;
.source "PG"


# instance fields
.field final a:Landroid/support/v7/c/n;

.field final b:Landroid/support/v7/c/w;

.field c:Landroid/widget/SeekBar;

.field d:Z

.field private final e:Landroid/support/v7/app/l;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Z

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/h;-><init>(Landroid/content/Context;I)V

    .line 71
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 74
    invoke-static {p1, v1}, Landroid/support/v7/app/o;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 60
    iput-boolean v1, p0, Landroid/support/v7/app/h;->i:Z

    .line 75
    invoke-virtual {p0}, Landroid/support/v7/app/h;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 77
    invoke-static {v0}, Landroid/support/v7/c/n;->a(Landroid/content/Context;)Landroid/support/v7/c/n;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/h;->a:Landroid/support/v7/c/n;

    .line 78
    new-instance v0, Landroid/support/v7/app/l;

    invoke-direct {v0, p0}, Landroid/support/v7/app/l;-><init>(Landroid/support/v7/app/h;)V

    iput-object v0, p0, Landroid/support/v7/app/h;->e:Landroid/support/v7/app/l;

    .line 79
    iget-object v0, p0, Landroid/support/v7/app/h;->a:Landroid/support/v7/c/n;

    invoke-static {}, Landroid/support/v7/c/n;->c()Landroid/support/v7/c/w;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    .line 80
    return-void
.end method


# virtual methods
.method a()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242
    iget-object v3, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    sget-object v4, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v5, v4, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-nez v5, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v4, v4, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-ne v4, v3, :cond_1

    move v3, v1

    :goto_0
    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    sget-object v4, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v5, v4, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-nez v5, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v3, v2

    goto :goto_0

    :cond_2
    iget-object v4, v4, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-ne v4, v3, :cond_4

    move v3, v1

    :goto_1
    if-eqz v3, :cond_5

    .line 243
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/app/h;->dismiss()V

    move v0, v2

    .line 261
    :goto_2
    return v0

    :cond_4
    move v3, v2

    .line 242
    goto :goto_1

    .line 247
    :cond_5
    iget-object v3, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    iget-object v3, v3, Landroid/support/v7/c/w;->d:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/support/v7/app/h;->setTitle(Ljava/lang/CharSequence;)V

    .line 248
    invoke-virtual {p0}, Landroid/support/v7/app/h;->b()V

    .line 250
    iget-object v3, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    iget-boolean v3, v3, Landroid/support/v7/c/w;->g:Z

    if-eqz v3, :cond_a

    iget-object v3, p0, Landroid/support/v7/app/h;->f:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_7

    invoke-virtual {p0}, Landroid/support/v7/app/h;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v3, Landroid/support/v7/d/b;->c:I

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    invoke-virtual {v6, v3, v5, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_9

    iget v3, v5, Landroid/util/TypedValue;->resourceId:I

    :goto_3
    if-eqz v3, :cond_6

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_6
    iput-object v0, p0, Landroid/support/v7/app/h;->f:Landroid/graphics/drawable/Drawable;

    :cond_7
    iget-object v0, p0, Landroid/support/v7/app/h;->f:Landroid/graphics/drawable/Drawable;

    .line 251
    :goto_4
    iget-object v3, p0, Landroid/support/v7/app/h;->h:Landroid/graphics/drawable/Drawable;

    if-eq v0, v3, :cond_8

    .line 252
    iput-object v0, p0, Landroid/support/v7/app/h;->h:Landroid/graphics/drawable/Drawable;

    .line 258
    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 259
    invoke-virtual {p0}, Landroid/support/v7/app/h;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    :cond_8
    move v0, v1

    .line 261
    goto :goto_2

    :cond_9
    move v3, v2

    .line 250
    goto :goto_3

    :cond_a
    iget-object v3, p0, Landroid/support/v7/app/h;->g:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_c

    invoke-virtual {p0}, Landroid/support/v7/app/h;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v3, Landroid/support/v7/d/b;->e:I

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    invoke-virtual {v6, v3, v5, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_d

    iget v3, v5, Landroid/util/TypedValue;->resourceId:I

    :goto_5
    if-eqz v3, :cond_b

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_b
    iput-object v0, p0, Landroid/support/v7/app/h;->g:Landroid/graphics/drawable/Drawable;

    :cond_c
    iget-object v0, p0, Landroid/support/v7/app/h;->g:Landroid/graphics/drawable/Drawable;

    goto :goto_4

    :cond_d
    move v3, v2

    goto :goto_5
.end method

.method b()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 281
    iget-boolean v2, p0, Landroid/support/v7/app/h;->d:Z

    if-nez v2, :cond_0

    .line 282
    iget-boolean v2, p0, Landroid/support/v7/app/h;->i:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    iget v2, v2, Landroid/support/v7/c/w;->k:I

    if-ne v2, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Landroid/support/v7/app/h;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Landroid/support/v7/app/h;->c:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 285
    iget-object v0, p0, Landroid/support/v7/app/h;->c:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 290
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 282
    goto :goto_0

    .line 287
    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/h;->j:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    .line 208
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 210
    iget-object v0, p0, Landroid/support/v7/app/h;->a:Landroid/support/v7/c/n;

    sget-object v1, Landroid/support/v7/c/l;->c:Landroid/support/v7/c/l;

    iget-object v2, p0, Landroid/support/v7/app/h;->e:Landroid/support/v7/app/l;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/l;Landroid/support/v7/c/o;I)V

    .line 212
    invoke-virtual {p0}, Landroid/support/v7/app/h;->a()Z

    .line 213
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Landroid/support/v7/app/h;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 140
    sget v0, Landroid/support/v7/d/d;->b:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/h;->setContentView(I)V

    .line 142
    sget v0, Landroid/support/v7/d/c;->d:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/h;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroid/support/v7/app/h;->j:Landroid/widget/LinearLayout;

    .line 143
    sget v0, Landroid/support/v7/d/c;->e:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/h;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Landroid/support/v7/app/h;->c:Landroid/widget/SeekBar;

    .line 144
    iget-object v0, p0, Landroid/support/v7/app/h;->c:Landroid/widget/SeekBar;

    new-instance v1, Landroid/support/v7/app/i;

    invoke-direct {v1, p0}, Landroid/support/v7/app/i;-><init>(Landroid/support/v7/app/h;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 180
    sget v0, Landroid/support/v7/d/c;->b:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/h;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/h;->l:Landroid/widget/Button;

    .line 181
    iget-object v0, p0, Landroid/support/v7/app/h;->l:Landroid/widget/Button;

    new-instance v1, Landroid/support/v7/app/k;

    invoke-direct {v1, p0}, Landroid/support/v7/app/k;-><init>(Landroid/support/v7/app/h;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    invoke-virtual {p0}, Landroid/support/v7/app/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/h;->k:Landroid/view/View;

    .line 194
    sget v0, Landroid/support/v7/d/c;->a:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/h;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 196
    iget-object v1, p0, Landroid/support/v7/app/h;->k:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Landroid/support/v7/app/h;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 198
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Landroid/support/v7/app/h;->a:Landroid/support/v7/c/n;

    iget-object v1, p0, Landroid/support/v7/app/h;->e:Landroid/support/v7/app/l;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/o;)V

    .line 219
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 220
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/16 v3, 0x19

    const/4 v1, 0x1

    .line 224
    if-eq p1, v3, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_2

    .line 226
    :cond_0
    iget-object v2, p0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    if-ne p1, v3, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v7/c/w;->b(I)V

    .line 229
    :goto_1
    return v1

    :cond_1
    move v0, v1

    .line 226
    goto :goto_0

    .line 229
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 234
    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    .line 236
    :cond_0
    const/4 v0, 0x1

    .line 238
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
