.class Landroid/support/v7/app/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/support/v7/app/h;


# direct methods
.method constructor <init>(Landroid/support/v7/app/h;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Landroid/support/v7/app/k;->a:Landroid/support/v7/app/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Landroid/support/v7/app/k;->a:Landroid/support/v7/app/h;

    iget-object v0, v0, Landroid/support/v7/app/h;->b:Landroid/support/v7/c/w;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    sget-object v1, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v2, v1, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, v1, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-ne v1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Landroid/support/v7/app/k;->a:Landroid/support/v7/app/h;

    iget-object v0, v0, Landroid/support/v7/app/h;->a:Landroid/support/v7/c/n;

    invoke-static {}, Landroid/support/v7/c/n;->b()Landroid/support/v7/c/w;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/c/w;->b()V

    .line 187
    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/k;->a:Landroid/support/v7/app/h;

    invoke-virtual {v0}, Landroid/support/v7/app/h;->dismiss()V

    .line 188
    return-void

    .line 184
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
