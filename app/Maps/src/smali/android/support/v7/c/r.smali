.class public final Landroid/support/v7/c/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/c/at;
.implements Landroid/support/v7/c/bg;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v7/c/n;",
            ">;>;"
        }
    .end annotation
.end field

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/w;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/v;",
            ">;"
        }
    .end annotation
.end field

.field final e:Landroid/support/v7/c/av;

.field final f:Landroid/support/v7/c/aw;

.field g:Landroid/support/v7/c/aq;

.field public h:Landroid/support/v7/c/w;

.field public i:Landroid/support/v7/c/w;

.field j:Landroid/support/v7/c/h;

.field private final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/u;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Landroid/support/v7/c/t;

.field private final m:Landroid/support/v7/c/s;

.field private final n:Z

.field private o:Landroid/support/v7/c/c;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1503
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/r;->b:Ljava/util/ArrayList;

    .line 1505
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    .line 1506
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    .line 1508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/r;->k:Ljava/util/ArrayList;

    .line 1510
    new-instance v0, Landroid/support/v7/c/av;

    invoke-direct {v0}, Landroid/support/v7/c/av;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/r;->e:Landroid/support/v7/c/av;

    .line 1512
    new-instance v0, Landroid/support/v7/c/t;

    invoke-direct {v0, p0}, Landroid/support/v7/c/t;-><init>(Landroid/support/v7/c/r;)V

    iput-object v0, p0, Landroid/support/v7/c/r;->l:Landroid/support/v7/c/t;

    .line 1513
    new-instance v0, Landroid/support/v7/c/s;

    invoke-direct {v0, p0}, Landroid/support/v7/c/s;-><init>(Landroid/support/v7/c/r;)V

    iput-object v0, p0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    .line 1526
    iput-object p1, p0, Landroid/support/v7/c/r;->a:Landroid/content/Context;

    .line 1527
    invoke-static {p1}, Landroid/support/v4/c/a/a;->a(Landroid/content/Context;)Landroid/support/v4/c/a/a;

    .line 1528
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/c/r;->n:Z

    .line 1535
    invoke-static {p1, p0}, Landroid/support/v7/c/aw;->a(Landroid/content/Context;Landroid/support/v7/c/bg;)Landroid/support/v7/c/aw;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/c/r;->f:Landroid/support/v7/c/aw;

    .line 1536
    iget-object v0, p0, Landroid/support/v7/c/r;->f:Landroid/support/v7/c/aw;

    invoke-virtual {p0, v0}, Landroid/support/v7/c/r;->a(Landroid/support/v7/c/d;)V

    .line 1537
    return-void

    .line 1528
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1936
    iget-object v0, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    iget-object v3, v0, Landroid/support/v7/c/w;->n:Landroid/support/v7/c/a;

    if-eqz v3, :cond_5

    iget-boolean v0, v0, Landroid/support/v7/c/w;->f:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 1937
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Clearing the default route because it is no longer selectable: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1939
    iput-object v6, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    .line 1941
    :cond_0
    iget-object v0, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1942
    iget-object v0, p0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/w;

    .line 1943
    iget-object v3, v0, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v3, v3, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    iget-object v5, p0, Landroid/support/v7/c/r;->f:Landroid/support/v7/c/aw;

    if-ne v3, v5, :cond_6

    iget-object v3, v0, Landroid/support/v7/c/w;->b:Ljava/lang/String;

    const-string v5, "DEFAULT_ROUTE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v1

    :goto_1
    if-eqz v3, :cond_1

    iget-object v3, v0, Landroid/support/v7/c/w;->n:Landroid/support/v7/c/a;

    if-eqz v3, :cond_7

    iget-boolean v3, v0, Landroid/support/v7/c/w;->f:Z

    if-eqz v3, :cond_7

    move v3, v1

    :goto_2
    if-eqz v3, :cond_1

    .line 1944
    iput-object v0, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    .line 1945
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Found default route: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1952
    :cond_2
    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    iget-object v3, v0, Landroid/support/v7/c/w;->n:Landroid/support/v7/c/a;

    if-eqz v3, :cond_8

    iget-boolean v0, v0, Landroid/support/v7/c/w;->f:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    if-nez v0, :cond_3

    .line 1953
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unselecting the current route because it is no longer selectable: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1955
    invoke-virtual {p0, v6}, Landroid/support/v7/c/r;->a(Landroid/support/v7/c/w;)V

    .line 1957
    :cond_3
    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-nez v0, :cond_9

    .line 1961
    invoke-direct {p0}, Landroid/support/v7/c/r;->b()Landroid/support/v7/c/w;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/c/r;->a(Landroid/support/v7/c/w;)V

    .line 1966
    :cond_4
    :goto_4
    return-void

    :cond_5
    move v0, v2

    .line 1936
    goto/16 :goto_0

    :cond_6
    move v3, v2

    .line 1943
    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_2

    :cond_8
    move v0, v2

    .line 1952
    goto :goto_3

    .line 1962
    :cond_9
    if-eqz p1, :cond_4

    .line 1964
    invoke-direct {p0}, Landroid/support/v7/c/r;->c()V

    goto :goto_4
.end method

.method private b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1925
    iget-object v0, p0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1926
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1927
    iget-object v0, p0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/w;

    iget-object v0, v0, Landroid/support/v7/c/w;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1931
    :goto_1
    return v0

    .line 1926
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1931
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private b()Landroid/support/v7/c/w;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1973
    iget-object v0, p0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/w;

    .line 1974
    iget-object v1, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-eq v0, v1, :cond_0

    iget-object v1, v0, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v1, v1, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    iget-object v5, p0, Landroid/support/v7/c/r;->f:Landroid/support/v7/c/aw;

    if-ne v1, v5, :cond_1

    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/support/v7/c/w;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/support/v7/c/w;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/support/v7/c/w;->n:Landroid/support/v7/c/a;

    if-eqz v1, :cond_2

    iget-boolean v1, v0, Landroid/support/v7/c/w;->f:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    if-eqz v1, :cond_0

    .line 1980
    :goto_2
    return-object v0

    :cond_1
    move v1, v3

    .line 1974
    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    .line 1980
    :cond_3
    iget-object v0, p0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    goto :goto_2
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2086
    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-eqz v0, :cond_0

    .line 2087
    iget-object v0, p0, Landroid/support/v7/c/r;->e:Landroid/support/v7/c/av;

    iget-object v1, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->l:I

    iput v1, v0, Landroid/support/v7/c/av;->a:I

    .line 2088
    iget-object v0, p0, Landroid/support/v7/c/r;->e:Landroid/support/v7/c/av;

    iget-object v1, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->m:I

    iput v1, v0, Landroid/support/v7/c/av;->b:I

    .line 2089
    iget-object v0, p0, Landroid/support/v7/c/r;->e:Landroid/support/v7/c/av;

    iget-object v1, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->k:I

    iput v1, v0, Landroid/support/v7/c/av;->c:I

    .line 2090
    iget-object v0, p0, Landroid/support/v7/c/r;->e:Landroid/support/v7/c/av;

    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    iget v0, v0, Landroid/support/v7/c/w;->j:I

    .line 2091
    iget-object v0, p0, Landroid/support/v7/c/r;->e:Landroid/support/v7/c/av;

    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    iget v0, v0, Landroid/support/v7/c/w;->i:I

    .line 2093
    iget-object v0, p0, Landroid/support/v7/c/r;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2094
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2095
    iget-object v0, p0, Landroid/support/v7/c/r;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/u;

    .line 2096
    iget-object v0, v0, Landroid/support/v7/c/u;->a:Landroid/support/v7/c/r;

    iget-object v0, v0, Landroid/support/v7/c/r;->e:Landroid/support/v7/c/av;

    .line 2094
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2098
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/support/v7/c/n;
    .locals 3

    .prologue
    .line 1549
    iget-object v0, p0, Landroid/support/v7/c/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 1550
    iget-object v0, p0, Landroid/support/v7/c/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/n;

    .line 1551
    if-nez v0, :cond_0

    .line 1552
    iget-object v0, p0, Landroid/support/v7/c/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 1553
    :cond_0
    iget-object v2, v0, Landroid/support/v7/c/n;->c:Landroid/content/Context;

    if-ne v2, p1, :cond_2

    .line 1559
    :goto_1
    return-object v0

    .line 1557
    :cond_1
    new-instance v0, Landroid/support/v7/c/n;

    invoke-direct {v0, p1}, Landroid/support/v7/c/n;-><init>(Landroid/content/Context;)V

    .line 1558
    iget-object v1, p0, Landroid/support/v7/c/r;->b:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v7/c/w;
    .locals 2

    .prologue
    .line 2035
    iget-object v0, p0, Landroid/support/v7/c/r;->f:Landroid/support/v7/c/aw;

    invoke-virtual {p0, v0}, Landroid/support/v7/c/r;->c(Landroid/support/v7/c/d;)I

    move-result v0

    .line 2036
    if-ltz v0, :cond_0

    .line 2037
    iget-object v1, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/v;

    .line 2038
    invoke-virtual {v0, p1}, Landroid/support/v7/c/v;->a(Ljava/lang/String;)I

    move-result v1

    .line 2039
    if-ltz v1, :cond_0

    .line 2040
    iget-object v0, v0, Landroid/support/v7/c/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/w;

    .line 2043
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1678
    .line 1680
    new-instance v8, Landroid/support/v7/c/m;

    invoke-direct {v8}, Landroid/support/v7/c/m;-><init>()V

    .line 1681
    iget-object v0, p0, Landroid/support/v7/c/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v5

    move v4, v5

    :goto_0
    add-int/lit8 v7, v0, -0x1

    if-ltz v7, :cond_6

    .line 1682
    iget-object v0, p0, Landroid/support/v7/c/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/n;

    .line 1683
    if-nez v0, :cond_0

    .line 1684
    iget-object v0, p0, Landroid/support/v7/c/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v7

    goto :goto_0

    .line 1686
    :cond_0
    iget-object v1, v0, Landroid/support/v7/c/n;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v5

    .line 1687
    :goto_1
    if-ge v6, v9, :cond_5

    .line 1688
    iget-object v1, v0, Landroid/support/v7/c/n;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/c/p;

    .line 1689
    iget-object v10, v1, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/l;

    if-nez v10, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v10}, Landroid/support/v7/c/l;->a()V

    iget-object v10, v10, Landroid/support/v7/c/l;->b:Ljava/util/List;

    invoke-virtual {v8, v10}, Landroid/support/v7/c/m;->a(Ljava/util/Collection;)Landroid/support/v7/c/m;

    .line 1690
    iget v10, v1, Landroid/support/v7/c/p;->d:I

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_2

    move v2, v3

    move v4, v3

    .line 1694
    :cond_2
    iget v10, v1, Landroid/support/v7/c/p;->d:I

    and-int/lit8 v10, v10, 0x4

    if-eqz v10, :cond_3

    .line 1695
    iget-boolean v10, p0, Landroid/support/v7/c/r;->n:Z

    if-nez v10, :cond_3

    move v4, v3

    .line 1699
    :cond_3
    iget v1, v1, Landroid/support/v7/c/p;->d:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    move v4, v3

    .line 1687
    :cond_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_5
    move v0, v7

    .line 1704
    goto :goto_0

    .line 1705
    :cond_6
    if-eqz v4, :cond_8

    invoke-virtual {v8}, Landroid/support/v7/c/m;->a()Landroid/support/v7/c/l;

    move-result-object v0

    .line 1708
    :goto_2
    iget-object v1, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    if-eqz v1, :cond_9

    iget-object v1, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    invoke-virtual {v1}, Landroid/support/v7/c/c;->a()V

    iget-object v1, v1, Landroid/support/v7/c/c;->b:Landroid/support/v7/c/l;

    invoke-virtual {v1, v0}, Landroid/support/v7/c/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    iget-object v1, v1, Landroid/support/v7/c/c;->a:Landroid/os/Bundle;

    const-string v3, "activeScan"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v2, :cond_9

    .line 1738
    :cond_7
    return-void

    .line 1705
    :cond_8
    sget-object v0, Landroid/support/v7/c/l;->c:Landroid/support/v7/c/l;

    goto :goto_2

    .line 1713
    :cond_9
    invoke-virtual {v0}, Landroid/support/v7/c/l;->a()V

    iget-object v1, v0, Landroid/support/v7/c/l;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    if-nez v2, :cond_c

    .line 1715
    iget-object v0, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    if-eqz v0, :cond_7

    .line 1718
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    .line 1723
    :goto_3
    sget-boolean v0, Landroid/support/v7/c/n;->a:Z

    if-eqz v0, :cond_a

    .line 1724
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Updated discovery request: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1726
    :cond_a
    if-eqz v4, :cond_b

    if-nez v2, :cond_b

    iget-boolean v0, p0, Landroid/support/v7/c/r;->n:Z

    .line 1727
    :cond_b
    iget-object v0, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v5

    .line 1735
    :goto_4
    if-ge v1, v2, :cond_7

    .line 1736
    iget-object v0, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/v;

    iget-object v0, v0, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    iget-object v3, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    invoke-virtual {v0, v3}, Landroid/support/v7/c/d;->a(Landroid/support/v7/c/c;)V

    .line 1735
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1721
    :cond_c
    new-instance v1, Landroid/support/v7/c/c;

    invoke-direct {v1, v0, v2}, Landroid/support/v7/c/c;-><init>(Landroid/support/v7/c/l;Z)V

    iput-object v1, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    goto :goto_3
.end method

.method public final a(Landroid/support/v7/c/d;)V
    .locals 3

    .prologue
    .line 1742
    invoke-virtual {p0, p1}, Landroid/support/v7/c/r;->c(Landroid/support/v7/c/d;)I

    move-result v0

    .line 1743
    if-gez v0, :cond_1

    .line 1745
    new-instance v0, Landroid/support/v7/c/v;

    invoke-direct {v0, p1}, Landroid/support/v7/c/v;-><init>(Landroid/support/v7/c/d;)V

    .line 1746
    iget-object v1, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1747
    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    .line 1748
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Provider added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1750
    :cond_0
    iget-object v1, p0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v2, 0x201

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1752
    iget-object v1, p1, Landroid/support/v7/c/d;->g:Landroid/support/v7/c/i;

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/c/r;->a(Landroid/support/v7/c/v;Landroid/support/v7/c/i;)V

    .line 1754
    iget-object v0, p0, Landroid/support/v7/c/r;->l:Landroid/support/v7/c/t;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iput-object v0, p1, Landroid/support/v7/c/d;->d:Landroid/support/v7/c/e;

    .line 1756
    iget-object v0, p0, Landroid/support/v7/c/r;->o:Landroid/support/v7/c/c;

    invoke-virtual {p1, v0}, Landroid/support/v7/c/d;->a(Landroid/support/v7/c/c;)V

    .line 1758
    :cond_1
    return-void
.end method

.method a(Landroid/support/v7/c/v;Landroid/support/v7/c/i;)V
    .locals 16

    .prologue
    .line 1802
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/c/v;->d:Landroid/support/v7/c/i;

    move-object/from16 v0, p2

    if-eq v2, v0, :cond_1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    iput-object v0, v1, Landroid/support/v7/c/v;->d:Landroid/support/v7/c/i;

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_12

    .line 1805
    const/4 v7, 0x0

    .line 1806
    const/4 v6, 0x0

    .line 1807
    if-eqz p2, :cond_d

    .line 1808
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/c/i;->c()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1809
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/c/i;->b()V

    move-object/from16 v0, p2

    iget-object v9, v0, Landroid/support/v7/c/i;->a:Ljava/util/List;

    .line 1811
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    .line 1812
    const/4 v2, 0x0

    move v8, v2

    :goto_1
    if-ge v8, v10, :cond_d

    .line 1813
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/c/a;

    .line 1814
    iget-object v3, v2, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v4, "id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1815
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/support/v7/c/v;->a(Ljava/lang/String;)I

    move-result v5

    .line 1816
    if-gez v5, :cond_4

    .line 1818
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v7/c/v;->c:Landroid/support/v7/c/g;

    iget-object v4, v4, Landroid/support/v7/c/g;->a:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Landroid/support/v7/c/r;->b(Ljava/lang/String;)I

    move-result v3

    if-gez v3, :cond_2

    move-object v3, v4

    .line 1819
    :goto_2
    new-instance v4, Landroid/support/v7/c/w;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v11, v3}, Landroid/support/v7/c/w;-><init>(Landroid/support/v7/c/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 1820
    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v7/c/v;->b:Ljava/util/ArrayList;

    add-int/lit8 v3, v7, 0x1

    invoke-virtual {v5, v7, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1821
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1823
    invoke-virtual {v4, v2}, Landroid/support/v7/c/w;->a(Landroid/support/v7/c/a;)I

    .line 1825
    sget-boolean v2, Landroid/support/v7/c/n;->a:Z

    if-eqz v2, :cond_0

    .line 1826
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Route added: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1828
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v5, 0x101

    invoke-virtual {v2, v5, v4}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    move v2, v6

    .line 1812
    :goto_3
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v6, v2

    move v7, v3

    goto :goto_1

    .line 1802
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1818
    :cond_2
    const/4 v3, 0x2

    :goto_4
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "%s_%d"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    const/4 v14, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v5, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Landroid/support/v7/c/r;->b(Ljava/lang/String;)I

    move-result v12

    if-gez v12, :cond_3

    move-object v3, v5

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1829
    :cond_4
    if-ge v5, v7, :cond_5

    .line 1830
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring route descriptor with duplicate id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v2, v6

    move v3, v7

    goto :goto_3

    .line 1834
    :cond_5
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v7/c/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/c/w;

    .line 1835
    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v7/c/v;->b:Ljava/util/ArrayList;

    add-int/lit8 v4, v7, 0x1

    invoke-static {v11, v5, v7}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1838
    invoke-virtual {v3, v2}, Landroid/support/v7/c/w;->a(Landroid/support/v7/c/a;)I

    move-result v2

    .line 1840
    if-eqz v2, :cond_13

    .line 1841
    and-int/lit8 v5, v2, 0x1

    if-eqz v5, :cond_7

    .line 1842
    sget-boolean v5, Landroid/support/v7/c/n;->a:Z

    if-eqz v5, :cond_6

    .line 1843
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Route changed: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1845
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v7, 0x103

    invoke-virtual {v5, v7, v3}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    .line 1848
    :cond_7
    and-int/lit8 v5, v2, 0x2

    if-eqz v5, :cond_9

    .line 1849
    sget-boolean v5, Landroid/support/v7/c/n;->a:Z

    if-eqz v5, :cond_8

    .line 1850
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Route volume changed: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1852
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v7, 0x104

    invoke-virtual {v5, v7, v3}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    .line 1855
    :cond_9
    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_b

    .line 1856
    sget-boolean v2, Landroid/support/v7/c/n;->a:Z

    if-eqz v2, :cond_a

    .line 1857
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Route presentation display changed: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1860
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v5, 0x105

    invoke-virtual {v2, v5, v3}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1863
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-ne v3, v2, :cond_13

    .line 1864
    const/4 v2, 0x1

    move v3, v4

    goto/16 :goto_3

    .line 1870
    :cond_c
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring invalid provider descriptor: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1875
    :cond_d
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/c/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_5
    if-lt v3, v7, :cond_e

    .line 1877
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/c/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/c/w;

    .line 1878
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/support/v7/c/w;->a(Landroid/support/v7/c/a;)I

    .line 1880
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1875
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_5

    .line 1884
    :cond_e
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Landroid/support/v7/c/r;->a(Z)V

    .line 1891
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/c/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_6
    if-lt v3, v7, :cond_10

    .line 1892
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/c/v;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/c/w;

    .line 1893
    sget-boolean v4, Landroid/support/v7/c/n;->a:Z

    if-eqz v4, :cond_f

    .line 1894
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Route removed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1896
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v5, 0x102

    invoke-virtual {v4, v5, v2}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1891
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_6

    .line 1900
    :cond_10
    sget-boolean v2, Landroid/support/v7/c/n;->a:Z

    if-eqz v2, :cond_11

    .line 1901
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider changed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1903
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v3, 0x203

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1905
    :cond_12
    return-void

    :cond_13
    move v2, v6

    move v3, v4

    goto/16 :goto_3
.end method

.method a(Landroid/support/v7/c/w;)V
    .locals 3

    .prologue
    .line 2002
    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-eq v0, p1, :cond_5

    .line 2003
    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-eqz v0, :cond_1

    .line 2004
    sget-boolean v0, Landroid/support/v7/c/n;->a:Z

    if-eqz v0, :cond_0

    .line 2005
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route unselected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2007
    :cond_0
    iget-object v0, p0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v1, 0x107

    iget-object v2, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2008
    iget-object v0, p0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    if-eqz v0, :cond_1

    .line 2009
    iget-object v0, p0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    invoke-virtual {v0}, Landroid/support/v7/c/h;->c()V

    .line 2010
    iget-object v0, p0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    invoke-virtual {v0}, Landroid/support/v7/c/h;->a()V

    .line 2011
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    .line 2015
    :cond_1
    iput-object p1, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    .line 2017
    iget-object v0, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-eqz v0, :cond_4

    .line 2018
    iget-object v0, p1, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v0, v0, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    iget-object v1, p1, Landroid/support/v7/c/w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/d;->a(Ljava/lang/String;)Landroid/support/v7/c/h;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    .line 2020
    iget-object v0, p0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    if-eqz v0, :cond_2

    .line 2021
    iget-object v0, p0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    invoke-virtual {v0}, Landroid/support/v7/c/h;->b()V

    .line 2023
    :cond_2
    sget-boolean v0, Landroid/support/v7/c/n;->a:Z

    if-eqz v0, :cond_3

    .line 2024
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route selected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2026
    :cond_3
    iget-object v0, p0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v1, 0x106

    iget-object v2, p0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2029
    :cond_4
    invoke-direct {p0}, Landroid/support/v7/c/r;->c()V

    .line 2031
    :cond_5
    return-void
.end method

.method public final a(Landroid/support/v7/c/l;I)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1650
    invoke-virtual {p1}, Landroid/support/v7/c/l;->a()V

    iget-object v0, p1, Landroid/support/v7/c/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1673
    :goto_0
    return v0

    .line 1655
    :cond_0
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/c/r;->n:Z

    if-eqz v0, :cond_1

    move v0, v2

    .line 1656
    goto :goto_0

    .line 1660
    :cond_1
    iget-object v0, p0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v1

    .line 1661
    :goto_1
    if-ge v3, v5, :cond_7

    .line 1662
    iget-object v0, p0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/w;

    .line 1663
    and-int/lit8 v4, p2, 0x1

    if-eqz v4, :cond_3

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    sget-object v4, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v6, v4, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-nez v6, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v4, v4, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-ne v4, v0, :cond_4

    move v4, v2

    :goto_2
    if-nez v4, :cond_6

    .line 1665
    :cond_3
    if-nez p1, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v4, v1

    .line 1663
    goto :goto_2

    .line 1665
    :cond_5
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v0, v0, Landroid/support/v7/c/w;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/support/v7/c/l;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 1668
    goto :goto_0

    .line 1661
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_7
    move v0, v1

    .line 1673
    goto :goto_0
.end method

.method public final b(Landroid/support/v7/c/d;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1762
    invoke-virtual {p0, p1}, Landroid/support/v7/c/r;->c(Landroid/support/v7/c/d;)I

    move-result v1

    .line 1763
    if-ltz v1, :cond_1

    .line 1765
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iput-object v2, p1, Landroid/support/v7/c/d;->d:Landroid/support/v7/c/e;

    .line 1767
    invoke-virtual {p1, v2}, Landroid/support/v7/c/d;->a(Landroid/support/v7/c/c;)V

    .line 1769
    iget-object v0, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/v;

    .line 1770
    invoke-virtual {p0, v0, v2}, Landroid/support/v7/c/r;->a(Landroid/support/v7/c/v;Landroid/support/v7/c/i;)V

    .line 1772
    sget-boolean v2, Landroid/support/v7/c/n;->a:Z

    if-eqz v2, :cond_0

    .line 1773
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider removed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1775
    :cond_0
    iget-object v2, p0, Landroid/support/v7/c/r;->m:Landroid/support/v7/c/s;

    const/16 v3, 0x202

    invoke-virtual {v2, v3, v0}, Landroid/support/v7/c/s;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1776
    iget-object v0, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1778
    :cond_1
    return-void
.end method

.method c(Landroid/support/v7/c/d;)I
    .locals 3

    .prologue
    .line 1791
    iget-object v0, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1792
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1793
    iget-object v0, p0, Landroid/support/v7/c/r;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/v;

    iget-object v0, v0, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 1797
    :goto_1
    return v0

    .line 1792
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1797
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
