.class public abstract Landroid/support/v7/c/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/support/v7/c/g;

.field final c:Landroid/support/v7/c/f;

.field d:Landroid/support/v7/c/e;

.field e:Landroid/support/v7/c/c;

.field f:Z

.field g:Landroid/support/v7/c/i;

.field h:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v7/c/g;)V
    .locals 3

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Landroid/support/v7/c/f;

    invoke-direct {v0, p0}, Landroid/support/v7/c/f;-><init>(Landroid/support/v7/c/d;)V

    iput-object v0, p0, Landroid/support/v7/c/d;->c:Landroid/support/v7/c/f;

    .line 83
    if-nez p1, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    iput-object p1, p0, Landroid/support/v7/c/d;->a:Landroid/content/Context;

    .line 88
    if-nez p2, :cond_1

    .line 89
    new-instance v0, Landroid/support/v7/c/g;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Landroid/support/v7/c/g;-><init>(Landroid/content/ComponentName;)V

    iput-object v0, p0, Landroid/support/v7/c/d;->b:Landroid/support/v7/c/g;

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_1
    iput-object p2, p0, Landroid/support/v7/c/d;->b:Landroid/support/v7/c/g;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/support/v7/c/h;
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/support/v7/c/c;)V
    .locals 2

    .prologue
    .line 148
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    .line 150
    iget-object v0, p0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    invoke-virtual {v0, p1}, Landroid/support/v7/c/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iput-object p1, p0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    .line 156
    iget-boolean v0, p0, Landroid/support/v7/c/d;->f:Z

    if-nez v0, :cond_0

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/c/d;->f:Z

    .line 158
    iget-object v0, p0, Landroid/support/v7/c/d;->c:Landroid/support/v7/c/f;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v7/c/f;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/c/i;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 222
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    .line 224
    iget-object v0, p0, Landroid/support/v7/c/d;->g:Landroid/support/v7/c/i;

    if-eq v0, p1, :cond_0

    .line 225
    iput-object p1, p0, Landroid/support/v7/c/d;->g:Landroid/support/v7/c/i;

    .line 226
    iget-boolean v0, p0, Landroid/support/v7/c/d;->h:Z

    if-nez v0, :cond_0

    .line 227
    iput-boolean v1, p0, Landroid/support/v7/c/d;->h:Z

    .line 228
    iget-object v0, p0, Landroid/support/v7/c/d;->c:Landroid/support/v7/c/f;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/f;->sendEmptyMessage(I)Z

    .line 231
    :cond_0
    return-void
.end method

.method public b(Landroid/support/v7/c/c;)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method
