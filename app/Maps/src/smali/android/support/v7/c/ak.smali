.class final Landroid/support/v7/c/ak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final a:Landroid/os/Messenger;

.field final b:Landroid/support/v7/c/ap;

.field c:I

.field d:I

.field e:I

.field f:I

.field final g:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v7/c/q;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic h:Landroid/support/v7/c/aj;

.field private final i:Landroid/os/Messenger;


# direct methods
.method public constructor <init>(Landroid/support/v7/c/aj;Landroid/os/Messenger;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 394
    iput-object p1, p0, Landroid/support/v7/c/ak;->h:Landroid/support/v7/c/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput v0, p0, Landroid/support/v7/c/ak;->c:I

    .line 387
    iput v0, p0, Landroid/support/v7/c/ak;->d:I

    .line 391
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/ak;->g:Landroid/util/SparseArray;

    .line 395
    iput-object p2, p0, Landroid/support/v7/c/ak;->a:Landroid/os/Messenger;

    .line 396
    new-instance v0, Landroid/support/v7/c/ap;

    invoke-direct {v0, p0}, Landroid/support/v7/c/ap;-><init>(Landroid/support/v7/c/ak;)V

    iput-object v0, p0, Landroid/support/v7/c/ak;->b:Landroid/support/v7/c/ap;

    .line 397
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Landroid/support/v7/c/ak;->b:Landroid/support/v7/c/ap;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/support/v7/c/ak;->i:Landroid/os/Messenger;

    .line 398
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 6

    .prologue
    .line 534
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 535
    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 536
    const/4 v1, 0x7

    iget v2, p0, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/c/ak;->c:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 538
    return-void
.end method

.method public final a(Landroid/support/v7/c/c;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 561
    const/16 v1, 0xa

    iget v2, p0, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/c/ak;->c:I

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v4, p1, Landroid/support/v7/c/c;->a:Landroid/os/Bundle;

    :goto_0
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 563
    return-void

    :cond_0
    move-object v4, v5

    .line 561
    goto :goto_0
.end method

.method public final a()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 401
    iget v0, p0, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Landroid/support/v7/c/ak;->c:I

    iput v0, p0, Landroid/support/v7/c/ak;->f:I

    .line 402
    iget v2, p0, Landroid/support/v7/c/ak;->f:I

    move-object v0, p0

    move v3, v1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v6

    .line 414
    :goto_0
    return v1

    .line 409
    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/c/ak;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Landroid/support/v7/c/ak;->binderDied()V

    move v1, v6

    .line 414
    goto :goto_0
.end method

.method a(IIILjava/lang/Object;Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 566
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 567
    iput p1, v0, Landroid/os/Message;->what:I

    .line 568
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 569
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 570
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 571
    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 572
    iget-object v1, p0, Landroid/support/v7/c/ak;->i:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 574
    :try_start_0
    iget-object v1, p0, Landroid/support/v7/c/ak;->a:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    const/4 v0, 0x1

    .line 579
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 582
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(IILandroid/os/Bundle;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 457
    iget v0, p0, Landroid/support/v7/c/ak;->e:I

    if-nez v0, :cond_3

    iget v0, p0, Landroid/support/v7/c/ak;->f:I

    if-ne p1, v0, :cond_3

    if-lez p2, :cond_3

    .line 460
    iput v3, p0, Landroid/support/v7/c/ak;->f:I

    .line 461
    iput p2, p0, Landroid/support/v7/c/ak;->e:I

    .line 462
    iget-object v4, p0, Landroid/support/v7/c/ak;->h:Landroid/support/v7/c/aj;

    if-eqz p3, :cond_0

    new-instance v0, Landroid/support/v7/c/i;

    invoke-direct {v0, p3, v1}, Landroid/support/v7/c/i;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    :goto_0
    invoke-static {v4, p0, v0}, Landroid/support/v7/c/aj;->a(Landroid/support/v7/c/aj;Landroid/support/v7/c/ak;Landroid/support/v7/c/i;)V

    .line 464
    iget-object v1, p0, Landroid/support/v7/c/ak;->h:Landroid/support/v7/c/aj;

    iget-object v0, v1, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    if-ne v0, p0, :cond_2

    iput-boolean v2, v1, Landroid/support/v7/c/aj;->o:Z

    iget-object v0, v1, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_1
    if-ge v3, v4, :cond_1

    iget-object v0, v1, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/an;

    iget-object v5, v1, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    invoke-virtual {v0, v5}, Landroid/support/v7/c/an;->a(Landroid/support/v7/c/ak;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    move-object v0, v1

    .line 462
    goto :goto_0

    .line 464
    :cond_1
    iget-object v0, v1, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    if-eqz v0, :cond_2

    iget-object v1, v1, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    invoke-virtual {v1, v0}, Landroid/support/v7/c/ak;->a(Landroid/support/v7/c/c;)V

    :cond_2
    move v0, v2

    .line 467
    :goto_2
    return v0

    :cond_3
    move v0, v3

    goto :goto_2
.end method

.method public final b(II)V
    .locals 6

    .prologue
    .line 541
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 542
    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 543
    const/16 v1, 0x8

    iget v2, p0, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/c/ak;->c:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 545
    return-void
.end method

.method public final binderDied()V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Landroid/support/v7/c/ak;->h:Landroid/support/v7/c/aj;

    iget-object v0, v0, Landroid/support/v7/c/aj;->k:Landroid/support/v7/c/ao;

    new-instance v1, Landroid/support/v7/c/am;

    invoke-direct {v1, p0}, Landroid/support/v7/c/am;-><init>(Landroid/support/v7/c/ak;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/c/ao;->post(Ljava/lang/Runnable;)Z

    .line 507
    return-void
.end method
