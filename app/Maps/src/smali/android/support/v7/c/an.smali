.class final Landroid/support/v7/c/an;
.super Landroid/support/v7/c/h;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v7/c/aj;

.field private final b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field private e:I

.field private f:Landroid/support/v7/c/ak;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/support/v7/c/aj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, Landroid/support/v7/c/an;->a:Landroid/support/v7/c/aj;

    invoke-direct {p0}, Landroid/support/v7/c/h;-><init>()V

    .line 298
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/c/an;->d:I

    .line 305
    iput-object p2, p0, Landroid/support/v7/c/an;->b:Ljava/lang/String;

    .line 306
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Landroid/support/v7/c/an;->a:Landroid/support/v7/c/aj;

    iget-object v1, v0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/support/v7/c/an;->d()V

    invoke-virtual {v0}, Landroid/support/v7/c/aj;->b()V

    .line 335
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    iget v1, p0, Landroid/support/v7/c/an;->g:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/c/ak;->a(II)V

    .line 361
    :goto_0
    return-void

    .line 358
    :cond_0
    iput p1, p0, Landroid/support/v7/c/an;->d:I

    .line 359
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/c/an;->e:I

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/c/ak;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 309
    iput-object p1, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    .line 310
    iget-object v0, p0, Landroid/support/v7/c/an;->b:Ljava/lang/String;

    iget v3, p1, Landroid/support/v7/c/ak;->d:I

    add-int/lit8 v1, v3, 0x1

    iput v1, p1, Landroid/support/v7/c/ak;->d:I

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v1, "routeId"

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x3

    iget v2, p1, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p1, Landroid/support/v7/c/ak;->c:I

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    iput v3, p0, Landroid/support/v7/c/an;->g:I

    .line 311
    iget-boolean v0, p0, Landroid/support/v7/c/an;->c:Z

    if-eqz v0, :cond_1

    .line 312
    iget v3, p0, Landroid/support/v7/c/an;->g:I

    const/4 v1, 0x5

    iget v2, p1, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p1, Landroid/support/v7/c/ak;->c:I

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 313
    iget v0, p0, Landroid/support/v7/c/an;->d:I

    if-ltz v0, :cond_0

    .line 314
    iget v0, p0, Landroid/support/v7/c/an;->g:I

    iget v1, p0, Landroid/support/v7/c/an;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/c/ak;->a(II)V

    .line 315
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/c/an;->d:I

    .line 317
    :cond_0
    iget v0, p0, Landroid/support/v7/c/an;->e:I

    if-eqz v0, :cond_1

    .line 318
    iget v0, p0, Landroid/support/v7/c/an;->g:I

    iget v1, p0, Landroid/support/v7/c/an;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/c/ak;->b(II)V

    .line 319
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/c/an;->e:I

    .line 322
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 339
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/c/an;->c:Z

    .line 340
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    iget v3, p0, Landroid/support/v7/c/an;->g:I

    const/4 v1, 0x5

    iget v2, v0, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v5, v2, 0x1

    iput v5, v0, Landroid/support/v7/c/ak;->c:I

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 343
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    iget v1, p0, Landroid/support/v7/c/an;->g:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/c/ak;->b(II)V

    .line 370
    :goto_0
    return-void

    .line 368
    :cond_0
    iget v0, p0, Landroid/support/v7/c/an;->e:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/c/an;->e:I

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/c/an;->c:Z

    .line 348
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    iget v3, p0, Landroid/support/v7/c/an;->g:I

    const/4 v1, 0x6

    iget v2, v0, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v5, v2, 0x1

    iput v5, v0, Landroid/support/v7/c/ak;->c:I

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 351
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 325
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    iget v3, p0, Landroid/support/v7/c/an;->g:I

    const/4 v1, 0x4

    iget v2, v0, Landroid/support/v7/c/ak;->c:I

    add-int/lit8 v5, v2, 0x1

    iput v5, v0, Landroid/support/v7/c/ak;->c:I

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 327
    iput-object v4, p0, Landroid/support/v7/c/an;->f:Landroid/support/v7/c/ak;

    .line 328
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/c/an;->g:I

    .line 330
    :cond_0
    return-void
.end method
