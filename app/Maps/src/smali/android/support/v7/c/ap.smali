.class final Landroid/support/v7/c/ap;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v7/c/ak;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v7/c/ak;)V
    .locals 1

    .prologue
    .line 604
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 605
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v7/c/ap;->a:Ljava/lang/ref/WeakReference;

    .line 606
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 614
    iget-object v0, p0, Landroid/support/v7/c/ap;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/ak;

    .line 615
    if-eqz v0, :cond_1

    .line 616
    iget v5, p1, Landroid/os/Message;->what:I

    .line 617
    iget v6, p1, Landroid/os/Message;->arg1:I

    .line 618
    iget v7, p1, Landroid/os/Message;->arg2:I

    .line 619
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 620
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v8

    .line 621
    packed-switch v5, :pswitch_data_0

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    .line 622
    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_1

    .line 623
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unhandled message from server: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 627
    :cond_1
    return-void

    .line 621
    :pswitch_0
    iget v1, v0, Landroid/support/v7/c/ak;->f:I

    if-ne v6, v1, :cond_3

    iput v2, v0, Landroid/support/v7/c/ak;->f:I

    iget-object v1, v0, Landroid/support/v7/c/ak;->h:Landroid/support/v7/c/aj;

    const-string v2, "Registation failed"

    iget-object v3, v1, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    if-ne v3, v0, :cond_3

    sget-boolean v3, Landroid/support/v7/c/aj;->i:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ": Service connection error - "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v1}, Landroid/support/v7/c/aj;->d()V

    :cond_3
    iget-object v1, v0, Landroid/support/v7/c/ak;->g:Landroid/util/SparseArray;

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/c/q;

    if-eqz v1, :cond_4

    iget-object v0, v0, Landroid/support/v7/c/ak;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->remove(I)V

    :cond_4
    move v0, v4

    goto :goto_0

    :pswitch_1
    move v0, v4

    goto :goto_0

    :pswitch_2
    if-eqz v1, :cond_5

    instance-of v3, v1, Landroid/os/Bundle;

    if-eqz v3, :cond_0

    :cond_5
    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v0, v6, v7, v1}, Landroid/support/v7/c/ak;->a(IILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    if-eqz v1, :cond_6

    instance-of v5, v1, Landroid/os/Bundle;

    if-eqz v5, :cond_0

    :cond_6
    check-cast v1, Landroid/os/Bundle;

    iget v5, v0, Landroid/support/v7/c/ak;->e:I

    if-eqz v5, :cond_8

    iget-object v5, v0, Landroid/support/v7/c/ak;->h:Landroid/support/v7/c/aj;

    if-eqz v1, :cond_7

    new-instance v2, Landroid/support/v7/c/i;

    invoke-direct {v2, v1, v3}, Landroid/support/v7/c/i;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    move-object v1, v2

    :goto_1
    invoke-static {v5, v0, v1}, Landroid/support/v7/c/aj;->a(Landroid/support/v7/c/aj;Landroid/support/v7/c/ak;Landroid/support/v7/c/i;)V

    move v0, v4

    goto :goto_0

    :cond_7
    move-object v1, v3

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_0

    :pswitch_4
    if-eqz v1, :cond_9

    instance-of v3, v1, Landroid/os/Bundle;

    if-eqz v3, :cond_0

    :cond_9
    check-cast v1, Landroid/os/Bundle;

    iget-object v1, v0, Landroid/support/v7/c/ak;->g:Landroid/util/SparseArray;

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/c/q;

    if-eqz v1, :cond_a

    iget-object v0, v0, Landroid/support/v7/c/ak;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->remove(I)V

    move v0, v4

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto/16 :goto_0

    :pswitch_5
    if-eqz v1, :cond_b

    instance-of v3, v1, Landroid/os/Bundle;

    if-eqz v3, :cond_0

    :cond_b
    if-nez v8, :cond_c

    :goto_2
    check-cast v1, Landroid/os/Bundle;

    iget-object v1, v0, Landroid/support/v7/c/ak;->g:Landroid/util/SparseArray;

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/c/q;

    if-eqz v1, :cond_d

    iget-object v0, v0, Landroid/support/v7/c/ak;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->remove(I)V

    move v0, v4

    goto/16 :goto_0

    :cond_c
    const-string v3, "error"

    invoke-virtual {v8, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2

    :cond_d
    move v0, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method
