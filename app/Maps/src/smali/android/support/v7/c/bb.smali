.class Landroid/support/v7/c/bb;
.super Landroid/support/v7/c/ax;
.source "PG"

# interfaces
.implements Landroid/support/v7/c/ag;


# instance fields
.field private r:Landroid/support/v7/c/af;

.field private s:Landroid/support/v7/c/ai;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/c/bg;)V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0, p1, p2}, Landroid/support/v7/c/ax;-><init>(Landroid/content/Context;Landroid/support/v7/c/bg;)V

    .line 717
    return-void
.end method


# virtual methods
.method protected a(Landroid/support/v7/c/az;Landroid/support/v7/c/b;)V
    .locals 3

    .prologue
    .line 742
    invoke-super {p0, p1, p2}, Landroid/support/v7/c/ax;->a(Landroid/support/v7/c/az;Landroid/support/v7/c/b;)V

    .line 744
    iget-object v0, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 745
    const/4 v0, 0x0

    iget-object v1, p2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v2, "enabled"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 748
    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/c/bb;->a(Landroid/support/v7/c/az;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    const/4 v0, 0x1

    iget-object v1, p2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v2, "connecting"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 752
    :cond_1
    iget-object v0, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_2

    .line 755
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    iget-object v1, p2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v2, "presentationDisplayId"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 757
    :cond_2
    return-void
.end method

.method protected a(Landroid/support/v7/c/az;)Z
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Landroid/support/v7/c/bb;->s:Landroid/support/v7/c/ai;

    if-nez v0, :cond_0

    .line 777
    new-instance v0, Landroid/support/v7/c/ai;

    invoke-direct {v0}, Landroid/support/v7/c/ai;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/bb;->s:Landroid/support/v7/c/ai;

    .line 779
    :cond_0
    iget-object v0, p0, Landroid/support/v7/c/bb;->s:Landroid/support/v7/c/ai;

    iget-object v1, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/ai;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 761
    invoke-super {p0}, Landroid/support/v7/c/ax;->b()V

    .line 763
    iget-object v0, p0, Landroid/support/v7/c/bb;->r:Landroid/support/v7/c/af;

    if-nez v0, :cond_0

    .line 764
    new-instance v0, Landroid/support/v7/c/af;

    iget-object v2, p0, Landroid/support/v7/c/d;->a:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v7/c/d;->c:Landroid/support/v7/c/f;

    invoke-direct {v0, v2, v3}, Landroid/support/v7/c/af;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/support/v7/c/bb;->r:Landroid/support/v7/c/af;

    .line 767
    :cond_0
    iget-object v2, p0, Landroid/support/v7/c/bb;->r:Landroid/support/v7/c/af;

    iget-boolean v0, p0, Landroid/support/v7/c/bb;->n:Z

    if-eqz v0, :cond_2

    iget v0, p0, Landroid/support/v7/c/bb;->m:I

    :goto_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    iget-boolean v0, v2, Landroid/support/v7/c/af;->c:Z

    if-nez v0, :cond_1

    iget-object v0, v2, Landroid/support/v7/c/af;->b:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/v7/c/af;->c:Z

    iget-object v0, v2, Landroid/support/v7/c/af;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 768
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 767
    goto :goto_0

    :cond_3
    iget-boolean v0, v2, Landroid/support/v7/c/af;->c:Z

    if-eqz v0, :cond_1

    iput-boolean v1, v2, Landroid/support/v7/c/af;->c:Z

    iget-object v0, v2, Landroid/support/v7/c/af;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method protected final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 772
    new-instance v0, Landroid/support/v7/c/ah;

    invoke-direct {v0, p0}, Landroid/support/v7/c/ah;-><init>(Landroid/support/v7/c/ag;)V

    return-object v0
.end method

.method public final f(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 721
    invoke-virtual {p0, p1}, Landroid/support/v7/c/bb;->g(Ljava/lang/Object;)I

    move-result v0

    .line 722
    if-ltz v0, :cond_0

    .line 723
    iget-object v1, p0, Landroid/support/v7/c/bb;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    .line 724
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v1

    .line 726
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    .line 728
    :goto_0
    iget-object v3, v0, Landroid/support/v7/c/az;->c:Landroid/support/v7/c/a;

    iget-object v3, v3, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v4, "presentationDisplayId"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 730
    new-instance v2, Landroid/support/v7/c/b;

    iget-object v3, v0, Landroid/support/v7/c/az;->c:Landroid/support/v7/c/a;

    invoke-direct {v2, v3}, Landroid/support/v7/c/b;-><init>(Landroid/support/v7/c/a;)V

    iget-object v3, v2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v4, "presentationDisplayId"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2}, Landroid/support/v7/c/b;->a()Landroid/support/v7/c/a;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/c/az;->c:Landroid/support/v7/c/a;

    .line 734
    invoke-virtual {p0}, Landroid/support/v7/c/bb;->a()V

    .line 737
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 726
    goto :goto_0
.end method
