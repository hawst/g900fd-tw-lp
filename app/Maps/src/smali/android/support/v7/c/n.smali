.class public final Landroid/support/v7/c/n;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Z

.field public static b:Landroid/support/v7/c/r;


# instance fields
.field final c:Landroid/content/Context;

.field final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    const-string v0, "MediaRouter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Landroid/support/v7/c/n;->a:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/n;->d:Ljava/util/ArrayList;

    .line 193
    iput-object p1, p0, Landroid/support/v7/c/n;->c:Landroid/content/Context;

    .line 194
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v7/c/n;
    .locals 6

    .prologue
    .line 214
    if-nez p0, :cond_0

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_1
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    if-nez v0, :cond_2

    .line 220
    new-instance v0, Landroid/support/v7/c/r;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/c/r;-><init>(Landroid/content/Context;)V

    .line 221
    sput-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    new-instance v1, Landroid/support/v7/c/aq;

    iget-object v2, v0, Landroid/support/v7/c/r;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/support/v7/c/aq;-><init>(Landroid/content/Context;Landroid/support/v7/c/at;)V

    iput-object v1, v0, Landroid/support/v7/c/r;->g:Landroid/support/v7/c/aq;

    iget-object v0, v0, Landroid/support/v7/c/r;->g:Landroid/support/v7/c/aq;

    iget-boolean v1, v0, Landroid/support/v7/c/aq;->c:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/c/aq;->c:Z

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_RESTARTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, v0, Landroid/support/v7/c/aq;->a:Landroid/content/Context;

    iget-object v3, v0, Landroid/support/v7/c/aq;->d:Landroid/content/BroadcastReceiver;

    const/4 v4, 0x0

    iget-object v5, v0, Landroid/support/v7/c/aq;->b:Landroid/os/Handler;

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iget-object v1, v0, Landroid/support/v7/c/aq;->b:Landroid/os/Handler;

    iget-object v0, v0, Landroid/support/v7/c/aq;->e:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    :cond_2
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    invoke-virtual {v0, p0}, Landroid/support/v7/c/r;->a(Landroid/content/Context;)Landroid/support/v7/c/n;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/c/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v0, v0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static a(Landroid/support/v7/c/l;I)Z
    .locals 2

    .prologue
    .line 377
    if-nez p0, :cond_0

    .line 378
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_1
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/c/r;->a(Landroid/support/v7/c/l;I)Z

    move-result v0

    return v0
.end method

.method static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 684
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/support/v7/c/o;)I
    .locals 3

    .prologue
    .line 559
    iget-object v0, p0, Landroid/support/v7/c/n;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 560
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 561
    iget-object v0, p0, Landroid/support/v7/c/n;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/p;

    iget-object v0, v0, Landroid/support/v7/c/p;->b:Landroid/support/v7/c/o;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 565
    :goto_1
    return v0

    .line 560
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 565
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static b()Landroid/support/v7/c/w;
    .locals 2

    .prologue
    .line 254
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255
    :cond_0
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v1, v0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, v0, Landroid/support/v7/c/r;->h:Landroid/support/v7/c/w;

    return-object v0
.end method

.method public static c()Landroid/support/v7/c/w;
    .locals 2

    .prologue
    .line 305
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :cond_0
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v1, v0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, v0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    return-object v0
.end method

.method public static d()V
    .locals 2

    .prologue
    .line 677
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 678
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 681
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/c/l;Landroid/support/v7/c/o;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 497
    if-nez p1, :cond_0

    .line 498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_0
    if-nez p2, :cond_1

    .line 501
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503
    :cond_1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 505
    :cond_2
    sget-boolean v0, Landroid/support/v7/c/n;->a:Z

    if-eqz v0, :cond_3

    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "addCallback: selector="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    :cond_3
    invoke-direct {p0, p2}, Landroid/support/v7/c/n;->b(Landroid/support/v7/c/o;)I

    move-result v0

    .line 512
    if-gez v0, :cond_5

    .line 513
    new-instance v0, Landroid/support/v7/c/p;

    invoke-direct {v0, p0, p2}, Landroid/support/v7/c/p;-><init>(Landroid/support/v7/c/n;Landroid/support/v7/c/o;)V

    .line 514
    iget-object v1, p0, Landroid/support/v7/c/n;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    :goto_0
    iget v1, v0, Landroid/support/v7/c/p;->d:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p3

    if-eqz v1, :cond_9

    .line 520
    iget v1, v0, Landroid/support/v7/c/p;->d:I

    or-int/2addr v1, p3

    iput v1, v0, Landroid/support/v7/c/p;->d:I

    move v1, v2

    .line 523
    :goto_1
    iget-object v4, v0, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/l;

    if-eqz p1, :cond_4

    invoke-virtual {v4}, Landroid/support/v7/c/l;->a()V

    invoke-virtual {p1}, Landroid/support/v7/c/l;->a()V

    iget-object v3, v4, Landroid/support/v7/c/l;->b:Ljava/util/List;

    iget-object v4, p1, Landroid/support/v7/c/l;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v3

    :cond_4
    if-nez v3, :cond_8

    .line 524
    new-instance v1, Landroid/support/v7/c/m;

    iget-object v3, v0, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/l;

    invoke-direct {v1, v3}, Landroid/support/v7/c/m;-><init>(Landroid/support/v7/c/l;)V

    if-nez p1, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 516
    :cond_5
    iget-object v1, p0, Landroid/support/v7/c/n;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/p;

    goto :goto_0

    .line 524
    :cond_6
    invoke-virtual {p1}, Landroid/support/v7/c/l;->a()V

    iget-object v3, p1, Landroid/support/v7/c/l;->b:Ljava/util/List;

    invoke-virtual {v1, v3}, Landroid/support/v7/c/m;->a(Ljava/util/Collection;)Landroid/support/v7/c/m;

    invoke-virtual {v1}, Landroid/support/v7/c/m;->a()Landroid/support/v7/c/l;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/l;

    .line 529
    :goto_2
    if-eqz v2, :cond_7

    .line 530
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    invoke-virtual {v0}, Landroid/support/v7/c/r;->a()V

    .line 532
    :cond_7
    return-void

    :cond_8
    move v2, v1

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/c/o;)V
    .locals 2

    .prologue
    .line 542
    if-nez p1, :cond_0

    .line 543
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 545
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_1
    sget-boolean v0, Landroid/support/v7/c/n;->a:Z

    if-eqz v0, :cond_2

    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "removeCallback: callback="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 551
    :cond_2
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/support/v7/c/o;)I

    move-result v0

    .line 552
    if-ltz v0, :cond_3

    .line 553
    iget-object v1, p0, Landroid/support/v7/c/n;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 554
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    invoke-virtual {v0}, Landroid/support/v7/c/r;->a()V

    .line 556
    :cond_3
    return-void
.end method
