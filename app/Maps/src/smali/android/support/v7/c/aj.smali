.class final Landroid/support/v7/c/aj;
.super Landroid/support/v7/c/d;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field static final i:Z


# instance fields
.field final j:Landroid/content/ComponentName;

.field final k:Landroid/support/v7/c/ao;

.field final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/an;",
            ">;"
        }
    .end annotation
.end field

.field m:Z

.field n:Landroid/support/v7/c/ak;

.field o:Z

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "MediaRouteProviderProxy"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Landroid/support/v7/c/aj;->i:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Landroid/support/v7/c/g;

    invoke-direct {v0, p2}, Landroid/support/v7/c/g;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, p1, v0}, Landroid/support/v7/c/d;-><init>(Landroid/content/Context;Landroid/support/v7/c/g;)V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    .line 61
    iput-object p2, p0, Landroid/support/v7/c/aj;->j:Landroid/content/ComponentName;

    .line 62
    new-instance v0, Landroid/support/v7/c/ao;

    invoke-direct {v0, p0}, Landroid/support/v7/c/ao;-><init>(Landroid/support/v7/c/aj;)V

    iput-object v0, p0, Landroid/support/v7/c/aj;->k:Landroid/support/v7/c/ao;

    .line 63
    return-void
.end method

.method static synthetic a(Landroid/support/v7/c/aj;Landroid/support/v7/c/ak;Landroid/support/v7/c/i;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    if-ne v0, p1, :cond_1

    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Descriptor changed, descriptor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0, p2}, Landroid/support/v7/c/aj;->a(Landroid/support/v7/c/i;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v7/c/h;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Landroid/support/v7/c/d;->g:Landroid/support/v7/c/i;

    .line 68
    if-eqz v0, :cond_5

    .line 69
    invoke-virtual {v0}, Landroid/support/v7/c/i;->b()V

    iget-object v4, v0, Landroid/support/v7/c/i;->a:Ljava/util/List;

    .line 70
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v1, v2

    .line 71
    :goto_0
    if-ge v1, v5, :cond_5

    .line 72
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/a;

    .line 73
    iget-object v0, v0, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v6, "id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 74
    new-instance v0, Landroid/support/v7/c/an;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/c/an;-><init>(Landroid/support/v7/c/aj;Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-boolean v1, p0, Landroid/support/v7/c/aj;->o:Z

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/an;->a(Landroid/support/v7/c/ak;)V

    .line 79
    :cond_0
    iget-boolean v1, p0, Landroid/support/v7/c/aj;->m:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    if-eqz v1, :cond_2

    move v2, v3

    :cond_1
    :goto_1
    if-eqz v2, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/c/aj;->c()V

    .line 84
    :goto_2
    return-object v0

    .line 79
    :cond_2
    iget-object v1, p0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    move v2, v3

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/c/aj;->d()V

    goto :goto_2

    .line 71
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 84
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 101
    iget-boolean v1, p0, Landroid/support/v7/c/aj;->m:Z

    if-nez v1, :cond_2

    .line 102
    sget-boolean v1, Landroid/support/v7/c/aj;->i:Z

    if-eqz v1, :cond_0

    .line 103
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Starting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iput-boolean v0, p0, Landroid/support/v7/c/aj;->m:Z

    .line 107
    iget-boolean v1, p0, Landroid/support/v7/c/aj;->m:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    if-eqz v1, :cond_3

    :cond_1
    :goto_0
    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/c/aj;->c()V

    .line 109
    :cond_2
    :goto_1
    return-void

    .line 107
    :cond_3
    iget-object v1, p0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/c/aj;->d()V

    goto :goto_1
.end method

.method b()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 130
    iget-boolean v1, p0, Landroid/support/v7/c/aj;->m:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 131
    invoke-virtual {p0}, Landroid/support/v7/c/aj;->c()V

    .line 135
    :goto_1
    return-void

    .line 130
    :cond_1
    iget-object v1, p0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 133
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/c/aj;->d()V

    goto :goto_1
.end method

.method public final b(Landroid/support/v7/c/c;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 89
    iget-boolean v1, p0, Landroid/support/v7/c/aj;->o:Z

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    invoke-virtual {v1, p1}, Landroid/support/v7/c/ak;->a(Landroid/support/v7/c/c;)V

    .line 92
    :cond_0
    iget-boolean v1, p0, Landroid/support/v7/c/aj;->m:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/c/aj;->c()V

    .line 93
    :goto_1
    return-void

    .line 92
    :cond_2
    iget-object v1, p0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/c/aj;->d()V

    goto :goto_1
.end method

.method c()V
    .locals 3

    .prologue
    .line 154
    iget-boolean v0, p0, Landroid/support/v7/c/aj;->p:Z

    if-nez v0, :cond_1

    .line 155
    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_0

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Binding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Landroid/support/v7/c/aj;->j:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 162
    :try_start_0
    iget-object v1, p0, Landroid/support/v7/c/d;->a:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/c/aj;->p:Z

    .line 163
    iget-boolean v0, p0, Landroid/support/v7/c/aj;->p:Z

    if-nez v0, :cond_1

    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_1

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Bind failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :cond_1
    :goto_0
    return-void

    .line 167
    :catch_0
    move-exception v0

    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_1

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Bind failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method d()V
    .locals 2

    .prologue
    .line 175
    iget-boolean v0, p0, Landroid/support/v7/c/aj;->p:Z

    if-eqz v0, :cond_1

    .line 176
    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_0

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Unbinding"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/c/aj;->p:Z

    .line 181
    invoke-virtual {p0}, Landroid/support/v7/c/aj;->e()V

    .line 182
    iget-object v0, p0, Landroid/support/v7/c/d;->a:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 184
    :cond_1
    return-void
.end method

.method e()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 260
    iget-object v0, p0, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    if-eqz v0, :cond_1

    .line 261
    invoke-virtual {p0, v4}, Landroid/support/v7/c/aj;->a(Landroid/support/v7/c/i;)V

    .line 262
    iput-boolean v2, p0, Landroid/support/v7/c/aj;->o:Z

    .line 263
    iget-object v0, p0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/an;

    invoke-virtual {v0}, Landroid/support/v7/c/an;->d()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 264
    :cond_0
    iget-object v0, p0, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    const/4 v1, 0x2

    move v3, v2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/c/ak;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    iget-object v1, v0, Landroid/support/v7/c/ak;->b:Landroid/support/v7/c/ap;

    iget-object v1, v1, Landroid/support/v7/c/ap;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->clear()V

    iget-object v1, v0, Landroid/support/v7/c/ak;->a:Landroid/os/Messenger;

    invoke-virtual {v1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v1, v0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    iget-object v1, v0, Landroid/support/v7/c/ak;->h:Landroid/support/v7/c/aj;

    iget-object v1, v1, Landroid/support/v7/c/aj;->k:Landroid/support/v7/c/ao;

    new-instance v2, Landroid/support/v7/c/al;

    invoke-direct {v2, v0}, Landroid/support/v7/c/al;-><init>(Landroid/support/v7/c/ak;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/c/ao;->post(Ljava/lang/Runnable;)Z

    .line 265
    iput-object v4, p0, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    .line 267
    :cond_1
    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 188
    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_0

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Connected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/c/aj;->p:Z

    if-eqz v0, :cond_1

    .line 193
    invoke-virtual {p0}, Landroid/support/v7/c/aj;->e()V

    .line 195
    if-eqz p2, :cond_2

    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 196
    :goto_0
    invoke-static {v0}, Landroid/support/v7/c/k;->a(Landroid/os/Messenger;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 197
    new-instance v1, Landroid/support/v7/c/ak;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/c/ak;-><init>(Landroid/support/v7/c/aj;Landroid/os/Messenger;)V

    .line 198
    invoke-virtual {v1}, Landroid/support/v7/c/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 199
    iput-object v1, p0, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    .line 209
    :cond_1
    :goto_1
    return-void

    .line 195
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :cond_3
    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_1

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Registration failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 206
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Service returned invalid messenger binder"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 213
    sget-boolean v0, Landroid/support/v7/c/aj;->i:Z

    if-eqz v0, :cond_0

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Service disconnected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/c/aj;->e()V

    .line 217
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Service connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/c/aj;->j:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
