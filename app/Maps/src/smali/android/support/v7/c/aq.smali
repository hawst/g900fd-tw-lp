.class final Landroid/support/v7/c/aq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/os/Handler;

.field c:Z

.field final d:Landroid/content/BroadcastReceiver;

.field final e:Ljava/lang/Runnable;

.field private final f:Landroid/support/v7/c/at;

.field private final g:Landroid/content/pm/PackageManager;

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/aj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/c/at;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    .line 139
    new-instance v0, Landroid/support/v7/c/ar;

    invoke-direct {v0, p0}, Landroid/support/v7/c/ar;-><init>(Landroid/support/v7/c/aq;)V

    iput-object v0, p0, Landroid/support/v7/c/aq;->d:Landroid/content/BroadcastReceiver;

    .line 146
    new-instance v0, Landroid/support/v7/c/as;

    invoke-direct {v0, p0}, Landroid/support/v7/c/as;-><init>(Landroid/support/v7/c/aq;)V

    iput-object v0, p0, Landroid/support/v7/c/aq;->e:Ljava/lang/Runnable;

    .line 49
    iput-object p1, p0, Landroid/support/v7/c/aq;->a:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Landroid/support/v7/c/aq;->f:Landroid/support/v7/c/at;

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/aq;->b:Landroid/os/Handler;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/c/aq;->g:Landroid/content/pm/PackageManager;

    .line 53
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 129
    iget-object v0, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 130
    :goto_0
    if-ge v1, v3, :cond_2

    .line 131
    iget-object v0, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/aj;

    .line 132
    iget-object v4, v0, Landroid/support/v7/c/aj;->j:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, v0, Landroid/support/v7/c/aj;->j:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    move v0, v1

    .line 136
    :goto_2
    return v0

    :cond_0
    move v0, v2

    .line 132
    goto :goto_1

    .line 130
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method static synthetic a(Landroid/support/v7/c/aq;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 38
    iget-boolean v0, p0, Landroid/support/v7/c/aq;->c:Z

    if-eqz v0, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/c/aq;->g:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v0, :cond_8

    iget-object v3, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v6, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Landroid/support/v7/c/aq;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-gez v6, :cond_0

    new-instance v3, Landroid/support/v7/c/aj;

    iget-object v6, p0, Landroid/support/v7/c/aq;->a:Landroid/content/Context;

    new-instance v7, Landroid/content/ComponentName;

    iget-object v8, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v7, v8, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v6, v7}, Landroid/support/v7/c/aj;-><init>(Landroid/content/Context;Landroid/content/ComponentName;)V

    invoke-virtual {v3}, Landroid/support/v7/c/aj;->a()V

    iget-object v6, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v6, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Landroid/support/v7/c/aq;->f:Landroid/support/v7/c/at;

    invoke-interface {v1, v3}, Landroid/support/v7/c/at;->a(Landroid/support/v7/c/d;)V

    move v1, v0

    goto :goto_0

    :cond_0
    if-lt v6, v1, :cond_8

    iget-object v0, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/aj;

    invoke-virtual {v0}, Landroid/support/v7/c/aj;->a()V

    iget-object v3, v0, Landroid/support/v7/c/aj;->n:Landroid/support/v7/c/ak;

    if-nez v3, :cond_1

    iget-boolean v3, v0, Landroid/support/v7/c/aj;->m:Z

    if-eqz v3, :cond_3

    iget-object v3, v0, Landroid/support/v7/c/d;->e:Landroid/support/v7/c/c;

    if-eqz v3, :cond_2

    move v3, v4

    :goto_1
    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/c/aj;->d()V

    invoke-virtual {v0}, Landroid/support/v7/c/aj;->c()V

    :cond_1
    iget-object v3, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    add-int/lit8 v0, v1, 0x1

    invoke-static {v3, v6, v1}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v3, v0, Landroid/support/v7/c/aj;->l:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    goto :goto_1

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_3
    if-lt v3, v1, :cond_7

    iget-object v0, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/aj;

    iget-object v4, p0, Landroid/support/v7/c/aq;->f:Landroid/support/v7/c/at;

    invoke-interface {v4, v0}, Landroid/support/v7/c/at;->b(Landroid/support/v7/c/d;)V

    iget-object v4, p0, Landroid/support/v7/c/aq;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-boolean v4, v0, Landroid/support/v7/c/aj;->m:Z

    if-eqz v4, :cond_6

    sget-boolean v4, Landroid/support/v7/c/aj;->i:Z

    if-eqz v4, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": Stopping"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    iput-boolean v2, v0, Landroid/support/v7/c/aj;->m:Z

    invoke-virtual {v0}, Landroid/support/v7/c/aj;->b()V

    :cond_6
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_3

    :cond_7
    return-void

    :cond_8
    move v0, v1

    goto :goto_2
.end method
