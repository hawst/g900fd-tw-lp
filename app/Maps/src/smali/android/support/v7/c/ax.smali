.class Landroid/support/v7/c/ax;
.super Landroid/support/v7/c/aw;
.source "PG"

# interfaces
.implements Landroid/support/v7/c/ac;
.implements Landroid/support/v7/c/y;


# static fields
.field private static final r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private static final s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final i:Ljava/lang/Object;

.field public final j:Ljava/lang/Object;

.field public final k:Ljava/lang/Object;

.field public final l:Ljava/lang/Object;

.field public m:I

.field public n:Z

.field public o:Z

.field public final p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/az;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/c/ba;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Landroid/support/v7/c/bg;

.field private u:Landroid/support/v7/c/ab;

.field private v:Landroid/support/v7/c/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 208
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 209
    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 211
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 212
    sput-object v1, Landroid/support/v7/c/ax;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 218
    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 220
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 221
    sput-object v1, Landroid/support/v7/c/ax;->s:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/c/bg;)V
    .locals 3

    .prologue
    .line 249
    invoke-direct {p0, p1}, Landroid/support/v7/c/aw;-><init>(Landroid/content/Context;)V

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/ax;->q:Ljava/util/ArrayList;

    .line 250
    iput-object p2, p0, Landroid/support/v7/c/ax;->t:Landroid/support/v7/c/bg;

    .line 251
    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    .line 252
    invoke-virtual {p0}, Landroid/support/v7/c/ax;->c()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/c/ax;->j:Ljava/lang/Object;

    .line 253
    new-instance v0, Landroid/support/v7/c/ad;

    invoke-direct {v0, p0}, Landroid/support/v7/c/ad;-><init>(Landroid/support/v7/c/ac;)V

    iput-object v0, p0, Landroid/support/v7/c/ax;->k:Ljava/lang/Object;

    .line 255
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 256
    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    sget v2, Landroid/support/v7/d/e;->c:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    check-cast v0, Landroid/media/MediaRouter;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRouter;->createRouteCategory(Ljava/lang/CharSequence;Z)Landroid/media/MediaRouter$RouteCategory;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/c/ax;->l:Ljava/lang/Object;

    .line 259
    invoke-direct {p0}, Landroid/support/v7/c/ax;->e()V

    .line 260
    return-void
.end method

.method private a(Landroid/support/v7/c/az;)V
    .locals 3

    .prologue
    .line 571
    new-instance v0, Landroid/support/v7/c/b;

    iget-object v1, p1, Landroid/support/v7/c/az;->b:Ljava/lang/String;

    iget-object v2, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    invoke-direct {p0, v2}, Landroid/support/v7/c/ax;->i(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v7/c/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    invoke-virtual {p0, p1, v0}, Landroid/support/v7/c/ax;->a(Landroid/support/v7/c/az;Landroid/support/v7/c/b;)V

    .line 574
    invoke-virtual {v0}, Landroid/support/v7/c/b;->a()Landroid/support/v7/c/a;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v7/c/az;->c:Landroid/support/v7/c/a;

    .line 575
    return-void
.end method

.method private b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 545
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 546
    iget-object v0, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    iget-object v0, v0, Landroid/support/v7/c/az;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 550
    :goto_1
    return v0

    .line 545
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 550
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private e(Landroid/support/v7/c/w;)I
    .locals 3

    .prologue
    .line 554
    iget-object v0, p0, Landroid/support/v7/c/ax;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 555
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 556
    iget-object v0, p0, Landroid/support/v7/c/ax;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/ba;

    iget-object v0, v0, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 560
    :goto_1
    return v0

    .line 555
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 560
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 309
    .line 310
    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    invoke-virtual {v0}, Landroid/media/MediaRouter;->getRouteCount()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v0, v1

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 311
    invoke-direct {p0, v1}, Landroid/support/v7/c/ax;->f(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 312
    goto :goto_1

    .line 313
    :cond_1
    if-eqz v0, :cond_2

    .line 314
    invoke-virtual {p0}, Landroid/support/v7/c/ax;->a()V

    .line 316
    :cond_2
    return-void
.end method

.method private f(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 319
    move-object v0, p1

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/c/ba;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v7/c/ba;

    :goto_0
    if-nez v0, :cond_5

    invoke-virtual {p0, p1}, Landroid/support/v7/c/ax;->g(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_5

    .line 321
    invoke-virtual {p0}, Landroid/support/v7/c/ax;->d()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    move v0, v4

    :goto_1
    if-eqz v0, :cond_2

    const-string v0, "DEFAULT_ROUTE"

    :goto_2
    invoke-direct {p0, v0}, Landroid/support/v7/c/ax;->b(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_3

    .line 322
    :goto_3
    new-instance v1, Landroid/support/v7/c/az;

    invoke-direct {v1, p1, v0}, Landroid/support/v7/c/az;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    invoke-direct {p0, v1}, Landroid/support/v7/c/ax;->a(Landroid/support/v7/c/az;)V

    .line 324
    iget-object v0, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v4

    .line 327
    :goto_4
    return v0

    .line 319
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v5

    .line 321
    goto :goto_1

    :cond_2
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ROUTE_%08x"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Landroid/support/v7/c/ax;->i(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    move v1, v2

    :goto_5
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s_%d"

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v3, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Landroid/support/v7/c/ax;->b(Ljava/lang/String;)I

    move-result v6

    if-gez v6, :cond_4

    move-object v0, v3

    goto :goto_3

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    move v0, v5

    .line 327
    goto :goto_4
.end method

.method private i(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Landroid/support/v7/c/d;->a:Landroid/content/Context;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1, v0}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 583
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v7/c/h;
    .locals 2

    .prologue
    .line 264
    invoke-direct {p0, p1}, Landroid/support/v7/c/ax;->b(Ljava/lang/String;)I

    move-result v0

    .line 265
    if-ltz v0, :cond_0

    .line 266
    iget-object v1, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    .line 267
    new-instance v1, Landroid/support/v7/c/ay;

    iget-object v0, v0, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/c/ay;-><init>(Landroid/support/v7/c/ax;Ljava/lang/Object;)V

    move-object v0, v1

    .line 269
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a()V
    .locals 4

    .prologue
    .line 523
    new-instance v2, Landroid/support/v7/c/j;

    invoke-direct {v2}, Landroid/support/v7/c/j;-><init>()V

    .line 525
    iget-object v0, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 526
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 527
    iget-object v0, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    iget-object v0, v0, Landroid/support/v7/c/az;->c:Landroid/support/v7/c/a;

    invoke-virtual {v2, v0}, Landroid/support/v7/c/j;->a(Landroid/support/v7/c/a;)Landroid/support/v7/c/j;

    .line 526
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 530
    :cond_0
    invoke-virtual {v2}, Landroid/support/v7/c/j;->a()Landroid/support/v7/c/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/c/ax;->a(Landroid/support/v7/c/i;)V

    .line 531
    return-void
.end method

.method protected a(Landroid/support/v7/c/az;Landroid/support/v7/c/b;)V
    .locals 3

    .prologue
    .line 588
    iget-object v0, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v0

    .line 590
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 591
    sget-object v1, Landroid/support/v7/c/ax;->r:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Landroid/support/v7/c/b;->a(Ljava/util/Collection;)Landroid/support/v7/c/b;

    .line 593
    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 594
    sget-object v0, Landroid/support/v7/c/ax;->s:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Landroid/support/v7/c/b;->a(Ljava/util/Collection;)Landroid/support/v7/c/b;

    .line 597
    :cond_1
    iget-object v0, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I

    move-result v0

    iget-object v1, p2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v2, "playbackType"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 599
    iget-object v0, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackStream()I

    move-result v0

    iget-object v1, p2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v2, "playbackStream"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 601
    iget-object v0, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v0

    iget-object v1, p2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v2, "volume"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 603
    iget-object v0, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v0

    iget-object v1, p2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v2, "volumeMax"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 605
    iget-object v0, p1, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    move-result v0

    iget-object v1, p2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v2, "volumeHandling"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 607
    return-void
.end method

.method protected a(Landroid/support/v7/c/ba;)V
    .locals 2

    .prologue
    .line 610
    iget-object v0, p1, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    iget-object v1, v1, Landroid/support/v7/c/w;->d:Ljava/lang/String;

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setName(Ljava/lang/CharSequence;)V

    .line 612
    iget-object v0, p1, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->i:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackType(I)V

    .line 614
    iget-object v0, p1, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->j:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackStream(I)V

    .line 616
    iget-object v0, p1, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->l:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolume(I)V

    .line 618
    iget-object v0, p1, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->m:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeMax(I)V

    .line 620
    iget-object v0, p1, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    iget v1, v1, Landroid/support/v7/c/w;->k:I

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeHandling(I)V

    .line 622
    return-void
.end method

.method public final a(Landroid/support/v7/c/w;)V
    .locals 4

    .prologue
    .line 450
    iget-object v0, p1, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v0, v0, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    if-eq v0, p0, :cond_1

    .line 451
    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v7/c/ax;->l:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$RouteCategory;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->createUserRoute(Landroid/media/MediaRouter$RouteCategory;)Landroid/media/MediaRouter$UserRouteInfo;

    move-result-object v1

    .line 453
    new-instance v3, Landroid/support/v7/c/ba;

    invoke-direct {v3, p1, v1}, Landroid/support/v7/c/ba;-><init>(Landroid/support/v7/c/w;Ljava/lang/Object;)V

    move-object v0, v1

    .line 454
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0, v3}, Landroid/media/MediaRouter$RouteInfo;->setTag(Ljava/lang/Object;)V

    .line 455
    iget-object v2, p0, Landroid/support/v7/c/ax;->k:Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    check-cast v2, Landroid/media/MediaRouter$VolumeCallback;

    invoke-virtual {v0, v2}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeCallback(Landroid/media/MediaRouter$VolumeCallback;)V

    .line 456
    invoke-virtual {p0, v3}, Landroid/support/v7/c/ax;->a(Landroid/support/v7/c/ba;)V

    .line 457
    iget-object v0, p0, Landroid/support/v7/c/ax;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->addUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    const v1, 0x800003

    check-cast v0, Landroid/media/MediaRouter;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 465
    invoke-virtual {p0, v0}, Landroid/support/v7/c/ax;->g(Ljava/lang/Object;)I

    move-result v0

    .line 466
    if-ltz v0, :cond_0

    .line 467
    iget-object v1, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    .line 468
    iget-object v0, v0, Landroid/support/v7/c/az;->b:Ljava/lang/String;

    iget-object v1, p1, Landroid/support/v7/c/w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {p1}, Landroid/support/v7/c/w;->b()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    const v1, 0x800003

    check-cast v0, Landroid/media/MediaRouter;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 398
    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/c/ba;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/support/v7/c/ba;

    .line 399
    :goto_1
    if-eqz v0, :cond_3

    .line 400
    iget-object v0, v0, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    invoke-virtual {v0}, Landroid/support/v7/c/w;->b()V

    goto :goto_0

    .line 398
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 404
    :cond_3
    invoke-virtual {p0, p1}, Landroid/support/v7/c/ax;->g(Ljava/lang/Object;)I

    move-result v0

    .line 405
    if-ltz v0, :cond_0

    .line 406
    iget-object v1, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    .line 407
    iget-object v1, p0, Landroid/support/v7/c/ax;->t:Landroid/support/v7/c/bg;

    iget-object v0, v0, Landroid/support/v7/c/az;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Landroid/support/v7/c/bg;->a(Ljava/lang/String;)Landroid/support/v7/c/w;

    move-result-object v0

    .line 409
    if-eqz v0, :cond_0

    .line 410
    invoke-virtual {v0}, Landroid/support/v7/c/w;->b()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 434
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/c/ba;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/support/v7/c/ba;

    .line 435
    :goto_0
    if-eqz v0, :cond_0

    .line 436
    iget-object v0, v0, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    invoke-virtual {v0, p2}, Landroid/support/v7/c/w;->a(I)V

    .line 438
    :cond_0
    return-void

    .line 434
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 625
    iget-boolean v0, p0, Landroid/support/v7/c/ax;->o:Z

    if-eqz v0, :cond_0

    .line 626
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/c/ax;->o:Z

    .line 627
    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v7/c/ax;->j:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$Callback;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    .line 630
    :cond_0
    iget v0, p0, Landroid/support/v7/c/ax;->m:I

    if-eqz v0, :cond_1

    .line 631
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/c/ax;->o:Z

    .line 632
    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    iget v2, p0, Landroid/support/v7/c/ax;->m:I

    iget-object v1, p0, Landroid/support/v7/c/ax;->j:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$Callback;

    invoke-virtual {v0, v2, v1}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    .line 634
    :cond_1
    return-void
.end method

.method public final b(Landroid/support/v7/c/c;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 274
    .line 276
    if-eqz p1, :cond_5

    .line 277
    invoke-virtual {p1}, Landroid/support/v7/c/c;->a()V

    iget-object v1, p1, Landroid/support/v7/c/c;->b:Landroid/support/v7/c/l;

    .line 278
    invoke-virtual {v1}, Landroid/support/v7/c/l;->a()V

    iget-object v3, v1, Landroid/support/v7/c/l;->b:Ljava/util/List;

    .line 279
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    .line 280
    :goto_0
    if-ge v2, v4, :cond_2

    .line 281
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 282
    const-string v5, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 283
    or-int/lit8 v0, v1, 0x1

    .line 280
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 284
    :cond_0
    const-string v5, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    or-int/lit8 v0, v1, 0x2

    goto :goto_1

    .line 287
    :cond_1
    const/high16 v0, 0x800000

    or-int/2addr v0, v1

    goto :goto_1

    .line 290
    :cond_2
    iget-object v0, p1, Landroid/support/v7/c/c;->a:Landroid/os/Bundle;

    const-string v2, "activeScan"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 293
    :goto_2
    iget v2, p0, Landroid/support/v7/c/ax;->m:I

    if-ne v2, v1, :cond_3

    iget-boolean v2, p0, Landroid/support/v7/c/ax;->n:Z

    if-eq v2, v0, :cond_4

    .line 294
    :cond_3
    iput v1, p0, Landroid/support/v7/c/ax;->m:I

    .line 295
    iput-boolean v0, p0, Landroid/support/v7/c/ax;->n:Z

    .line 296
    invoke-virtual {p0}, Landroid/support/v7/c/ax;->b()V

    .line 297
    invoke-direct {p0}, Landroid/support/v7/c/ax;->e()V

    .line 299
    :cond_4
    return-void

    :cond_5
    move v1, v0

    goto :goto_2
.end method

.method public final b(Landroid/support/v7/c/w;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 477
    iget-object v0, p1, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v0, v0, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    if-eq v0, p0, :cond_0

    .line 478
    invoke-direct {p0, p1}, Landroid/support/v7/c/ax;->e(Landroid/support/v7/c/w;)I

    move-result v0

    .line 479
    if-ltz v0, :cond_0

    .line 480
    iget-object v1, p0, Landroid/support/v7/c/ax;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/ba;

    .line 481
    iget-object v1, v0, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    check-cast v1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v1, v2}, Landroid/media/MediaRouter$RouteInfo;->setTag(Ljava/lang/Object;)V

    .line 482
    iget-object v1, v0, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    check-cast v1, Landroid/media/MediaRouter$UserRouteInfo;

    check-cast v2, Landroid/media/MediaRouter$VolumeCallback;

    invoke-virtual {v1, v2}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeCallback(Landroid/media/MediaRouter$VolumeCallback;)V

    .line 483
    iget-object v1, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    iget-object v2, v0, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, Landroid/media/MediaRouter;

    move-object v1, v2

    check-cast v1, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    .line 486
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 303
    invoke-direct {p0, p1}, Landroid/support/v7/c/ax;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {p0}, Landroid/support/v7/c/ax;->a()V

    .line 306
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 442
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/c/ba;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/support/v7/c/ba;

    .line 443
    :goto_0
    if-eqz v0, :cond_0

    .line 444
    iget-object v0, v0, Landroid/support/v7/c/ba;->a:Landroid/support/v7/c/w;

    invoke-virtual {v0, p2}, Landroid/support/v7/c/w;->b(I)V

    .line 446
    :cond_0
    return-void

    .line 442
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 637
    new-instance v0, Landroid/support/v7/c/z;

    invoke-direct {v0, p0}, Landroid/support/v7/c/z;-><init>(Landroid/support/v7/c/y;)V

    return-object v0
.end method

.method public final c(Landroid/support/v7/c/w;)V
    .locals 2

    .prologue
    .line 490
    iget-object v0, p1, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v0, v0, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    if-eq v0, p0, :cond_0

    .line 491
    invoke-direct {p0, p1}, Landroid/support/v7/c/ax;->e(Landroid/support/v7/c/w;)I

    move-result v0

    .line 492
    if-ltz v0, :cond_0

    .line 493
    iget-object v1, p0, Landroid/support/v7/c/ax;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/ba;

    .line 494
    invoke-virtual {p0, v0}, Landroid/support/v7/c/ax;->a(Landroid/support/v7/c/ba;)V

    .line 497
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 350
    move-object v0, p1

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/c/ba;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/support/v7/c/ba;

    :goto_0
    if-nez v0, :cond_0

    .line 351
    invoke-virtual {p0, p1}, Landroid/support/v7/c/ax;->g(Ljava/lang/Object;)I

    move-result v0

    .line 352
    if-ltz v0, :cond_0

    .line 353
    iget-object v1, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 354
    invoke-virtual {p0}, Landroid/support/v7/c/ax;->a()V

    .line 357
    :cond_0
    return-void

    .line 350
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 653
    iget-object v0, p0, Landroid/support/v7/c/ax;->v:Landroid/support/v7/c/aa;

    if-nez v0, :cond_0

    .line 654
    new-instance v0, Landroid/support/v7/c/aa;

    invoke-direct {v0}, Landroid/support/v7/c/aa;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/ax;->v:Landroid/support/v7/c/aa;

    .line 656
    :cond_0
    iget-object v0, p0, Landroid/support/v7/c/ax;->v:Landroid/support/v7/c/aa;

    iget-object v1, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/aa;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/support/v7/c/w;)V
    .locals 2

    .prologue
    .line 501
    invoke-virtual {p1}, Landroid/support/v7/c/w;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    iget-object v0, p1, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    invoke-static {}, Landroid/support/v7/c/n;->d()V

    iget-object v0, v0, Landroid/support/v7/c/v;->a:Landroid/support/v7/c/d;

    if-eq v0, p0, :cond_2

    .line 508
    invoke-direct {p0, p1}, Landroid/support/v7/c/ax;->e(Landroid/support/v7/c/w;)I

    move-result v0

    .line 509
    if-ltz v0, :cond_0

    .line 510
    iget-object v1, p0, Landroid/support/v7/c/ax;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/ba;

    .line 511
    iget-object v0, v0, Landroid/support/v7/c/ba;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Landroid/support/v7/c/ax;->h(Ljava/lang/Object;)V

    goto :goto_0

    .line 514
    :cond_2
    iget-object v0, p1, Landroid/support/v7/c/w;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/support/v7/c/ax;->b(Ljava/lang/String;)I

    move-result v0

    .line 515
    if-ltz v0, :cond_0

    .line 516
    iget-object v1, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    .line 517
    iget-object v0, v0, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Landroid/support/v7/c/ax;->h(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 361
    move-object v0, p1

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/c/ba;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/support/v7/c/ba;

    :goto_0
    if-nez v0, :cond_0

    .line 362
    invoke-virtual {p0, p1}, Landroid/support/v7/c/ax;->g(Ljava/lang/Object;)I

    move-result v0

    .line 363
    if-ltz v0, :cond_0

    .line 364
    iget-object v1, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    .line 365
    invoke-direct {p0, v0}, Landroid/support/v7/c/ax;->a(Landroid/support/v7/c/az;)V

    .line 366
    invoke-virtual {p0}, Landroid/support/v7/c/ax;->a()V

    .line 369
    :cond_0
    return-void

    .line 361
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 373
    move-object v0, p1

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/c/ba;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/support/v7/c/ba;

    :goto_0
    if-nez v0, :cond_0

    .line 374
    invoke-virtual {p0, p1}, Landroid/support/v7/c/ax;->g(Ljava/lang/Object;)I

    move-result v0

    .line 375
    if-ltz v0, :cond_0

    .line 376
    iget-object v1, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    .line 377
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v1

    .line 378
    iget-object v2, v0, Landroid/support/v7/c/az;->c:Landroid/support/v7/c/a;

    iget-object v2, v2, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "volume"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 379
    new-instance v2, Landroid/support/v7/c/b;

    iget-object v3, v0, Landroid/support/v7/c/az;->c:Landroid/support/v7/c/a;

    invoke-direct {v2, v3}, Landroid/support/v7/c/b;-><init>(Landroid/support/v7/c/a;)V

    iget-object v3, v2, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v4, "volume"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2}, Landroid/support/v7/c/b;->a()Landroid/support/v7/c/a;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/c/az;->c:Landroid/support/v7/c/a;

    .line 383
    invoke-virtual {p0}, Landroid/support/v7/c/ax;->a()V

    .line 387
    :cond_0
    return-void

    .line 373
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final g(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 534
    iget-object v0, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 535
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 536
    iget-object v0, p0, Landroid/support/v7/c/ax;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/az;

    iget-object v0, v0, Landroid/support/v7/c/az;->a:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 540
    :goto_1
    return v0

    .line 535
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 540
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected h(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 645
    iget-object v0, p0, Landroid/support/v7/c/ax;->u:Landroid/support/v7/c/ab;

    if-nez v0, :cond_0

    .line 646
    new-instance v0, Landroid/support/v7/c/ab;

    invoke-direct {v0}, Landroid/support/v7/c/ab;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/ax;->u:Landroid/support/v7/c/ab;

    .line 648
    :cond_0
    iget-object v1, p0, Landroid/support/v7/c/ax;->u:Landroid/support/v7/c/ab;

    iget-object v0, p0, Landroid/support/v7/c/ax;->i:Ljava/lang/Object;

    const v2, 0x800003

    check-cast v0, Landroid/media/MediaRouter;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v3

    const/high16 v4, 0x800000

    and-int/2addr v3, v4

    if-nez v3, :cond_1

    iget-object v3, v1, Landroid/support/v7/c/ab;->a:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_1

    :try_start_0
    iget-object v1, v1, Landroid/support/v7/c/ab;->a:Ljava/lang/reflect/Method;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 650
    :goto_0
    return-void

    :catch_0
    move-exception v1

    .line 648
    :cond_1
    :goto_1
    invoke-virtual {v0, v2, p1}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1
.end method
