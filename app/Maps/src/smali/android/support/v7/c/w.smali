.class public final Landroid/support/v7/c/w;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/support/v7/c/v;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Z

.field public final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field i:I

.field j:I

.field public k:I

.field public l:I

.field public m:I

.field n:Landroid/support/v7/c/a;

.field private o:I

.field private p:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/support/v7/c/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/w;->h:Ljava/util/ArrayList;

    .line 710
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/c/w;->o:I

    .line 764
    iput-object p1, p0, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    .line 765
    iput-object p2, p0, Landroid/support/v7/c/w;->b:Ljava/lang/String;

    .line 766
    iput-object p3, p0, Landroid/support/v7/c/w;->c:Ljava/lang/String;

    .line 767
    return-void
.end method


# virtual methods
.method final a(Landroid/support/v7/c/a;)I
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1178
    .line 1179
    iget-object v0, p0, Landroid/support/v7/c/w;->n:Landroid/support/v7/c/a;

    if-eq v0, p1, :cond_a

    .line 1180
    iput-object p1, p0, Landroid/support/v7/c/w;->n:Landroid/support/v7/c/a;

    .line 1181
    if-eqz p1, :cond_a

    .line 1182
    iget-object v0, p0, Landroid/support/v7/c/w;->d:Ljava/lang/String;

    iget-object v3, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v4, "name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/support/v7/c/n;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1183
    iget-object v0, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/c/w;->d:Ljava/lang/String;

    move v0, v1

    .line 1186
    :goto_0
    iget-object v3, p0, Landroid/support/v7/c/w;->e:Ljava/lang/String;

    iget-object v4, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v5, "status"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/support/v7/c/n;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1187
    iget-object v0, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "status"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/c/w;->e:Ljava/lang/String;

    move v0, v1

    .line 1190
    :cond_0
    iget-boolean v3, p0, Landroid/support/v7/c/w;->f:Z

    iget-object v4, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v5, "enabled"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eq v3, v4, :cond_1

    .line 1191
    iget-object v0, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "enabled"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/c/w;->f:Z

    move v0, v1

    .line 1194
    :cond_1
    iget-boolean v3, p0, Landroid/support/v7/c/w;->g:Z

    iget-object v4, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v5, "connecting"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eq v3, v4, :cond_2

    .line 1195
    iget-object v3, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v4, "connecting"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Landroid/support/v7/c/w;->g:Z

    .line 1196
    or-int/lit8 v0, v0, 0x1

    .line 1198
    :cond_2
    iget-object v3, p0, Landroid/support/v7/c/w;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/support/v7/c/a;->a()V

    iget-object v4, p1, Landroid/support/v7/c/a;->b:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1199
    iget-object v3, p0, Landroid/support/v7/c/w;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1200
    iget-object v3, p0, Landroid/support/v7/c/w;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/support/v7/c/a;->a()V

    iget-object v4, p1, Landroid/support/v7/c/a;->b:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1201
    or-int/lit8 v0, v0, 0x1

    .line 1203
    :cond_3
    iget v3, p0, Landroid/support/v7/c/w;->i:I

    iget-object v4, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v5, "playbackType"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v3, v4, :cond_4

    .line 1204
    iget-object v3, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v4, "playbackType"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/c/w;->i:I

    .line 1205
    or-int/lit8 v0, v0, 0x1

    .line 1207
    :cond_4
    iget v1, p0, Landroid/support/v7/c/w;->j:I

    iget-object v3, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v4, "playbackStream"

    invoke-virtual {v3, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-eq v1, v3, :cond_5

    .line 1208
    iget-object v1, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "playbackStream"

    invoke-virtual {v1, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/c/w;->j:I

    .line 1209
    or-int/lit8 v0, v0, 0x1

    .line 1211
    :cond_5
    iget v1, p0, Landroid/support/v7/c/w;->k:I

    iget-object v3, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v4, "volumeHandling"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-eq v1, v3, :cond_6

    .line 1212
    iget-object v1, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "volumeHandling"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/c/w;->k:I

    .line 1213
    or-int/lit8 v0, v0, 0x3

    .line 1215
    :cond_6
    iget v1, p0, Landroid/support/v7/c/w;->l:I

    iget-object v2, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "volume"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_7

    .line 1216
    iget-object v1, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v2, "volume"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Landroid/support/v7/c/w;->l:I

    .line 1217
    or-int/lit8 v0, v0, 0x3

    .line 1219
    :cond_7
    iget v1, p0, Landroid/support/v7/c/w;->m:I

    iget-object v2, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "volumeMax"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_8

    .line 1220
    iget-object v1, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v2, "volumeMax"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Landroid/support/v7/c/w;->m:I

    .line 1221
    or-int/lit8 v0, v0, 0x3

    .line 1223
    :cond_8
    iget v1, p0, Landroid/support/v7/c/w;->o:I

    iget-object v2, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "presentationDisplayId"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v1, v2, :cond_9

    .line 1224
    iget-object v1, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v2, "presentationDisplayId"

    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/c/w;->o:I

    .line 1225
    or-int/lit8 v0, v0, 0x5

    .line 1228
    :cond_9
    iget-object v1, p0, Landroid/support/v7/c/w;->p:Landroid/os/Bundle;

    iget-object v2, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v3, "extras"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/support/v7/c/n;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1229
    iget-object v1, p1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    const-string v2, "extras"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/c/w;->p:Landroid/os/Bundle;

    .line 1230
    or-int/lit8 v2, v0, 0x1

    .line 1234
    :cond_a
    :goto_1
    return v2

    :cond_b
    move v2, v0

    goto :goto_1

    :cond_c
    move v0, v2

    goto/16 :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1084
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    .line 1085
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget v1, p0, Landroid/support/v7/c/w;->m:I

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, v0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-ne p0, v2, :cond_0

    iget-object v2, v0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    if-eqz v2, :cond_0

    iget-object v0, v0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    invoke-virtual {v0, v1}, Landroid/support/v7/c/h;->a(I)V

    .line 1086
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 846
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    .line 847
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v1, v0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-ne v0, p0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 913
    if-nez p1, :cond_0

    .line 914
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "category must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 916
    :cond_0
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    .line 918
    iget-object v0, p0, Landroid/support/v7/c/w;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 919
    :goto_0
    if-ge v2, v3, :cond_2

    .line 920
    iget-object v0, p0, Landroid/support/v7/c/w;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 921
    const/4 v0, 0x1

    .line 924
    :goto_1
    return v0

    .line 919
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 924
    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1155
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    .line 1156
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v1, v0, Landroid/support/v7/c/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ignoring attempt to select removed route: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1157
    :goto_0
    return-void

    .line 1156
    :cond_0
    iget-boolean v1, p0, Landroid/support/v7/c/w;->f:Z

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ignoring attempt to select disabled route: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p0}, Landroid/support/v7/c/r;->a(Landroid/support/v7/c/w;)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1098
    invoke-static {}, Landroid/support/v7/c/n;->d()V

    .line 1099
    if-eqz p1, :cond_0

    .line 1100
    sget-object v0, Landroid/support/v7/c/n;->b:Landroid/support/v7/c/r;

    iget-object v1, v0, Landroid/support/v7/c/r;->i:Landroid/support/v7/c/w;

    if-ne p0, v1, :cond_0

    iget-object v1, v0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/c/r;->j:Landroid/support/v7/c/h;

    invoke-virtual {v0, p1}, Landroid/support/v7/c/h;->b(I)V

    .line 1102
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaRouter.RouteInfo{ uniqueId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/c/w;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/c/w;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/c/w;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/c/w;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connecting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/c/w;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/c/w;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/c/w;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeHandling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/c/w;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/c/w;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/c/w;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presentationDisplayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/c/w;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/c/w;->p:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", providerPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/c/w;->a:Landroid/support/v7/c/v;

    iget-object v1, v1, Landroid/support/v7/c/v;->c:Landroid/support/v7/c/g;

    iget-object v1, v1, Landroid/support/v7/c/g;->a:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
