.class public Landroid/support/v7/widget/ListPopupWindow;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static k:Ljava/lang/reflect/Method;


# instance fields
.field public a:Landroid/widget/PopupWindow;

.field public b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

.field public c:I

.field d:I

.field public e:I

.field public f:Landroid/view/View;

.field public g:Landroid/widget/AdapterView$OnItemClickListener;

.field final h:Landroid/support/v7/widget/az;

.field i:Landroid/os/Handler;

.field public j:Z

.field private l:Landroid/content/Context;

.field private m:Landroid/widget/ListAdapter;

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Landroid/database/DataSetObserver;

.field private final v:Landroid/support/v7/widget/ay;

.field private final w:Landroid/support/v7/widget/ax;

.field private final x:Landroid/support/v7/widget/av;

.field private y:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 80
    :try_start_0
    const-class v0, Landroid/widget/PopupWindow;

    const-string v1, "setClipToScreenEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ListPopupWindow;->k:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 192
    const/4 v0, 0x0

    sget v1, Landroid/support/v7/a/b;->o:I

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 203
    sget v0, Landroid/support/v7/a/b;->o:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 204
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 216
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x2

    const/4 v2, 0x0

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->n:I

    .line 93
    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    .line 98
    iput v2, p0, Landroid/support/v7/widget/ListPopupWindow;->c:I

    .line 100
    iput-boolean v2, p0, Landroid/support/v7/widget/ListPopupWindow;->s:Z

    .line 101
    iput-boolean v2, p0, Landroid/support/v7/widget/ListPopupWindow;->t:Z

    .line 102
    const v0, 0x7fffffff

    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->d:I

    .line 105
    iput v2, p0, Landroid/support/v7/widget/ListPopupWindow;->e:I

    .line 116
    new-instance v0, Landroid/support/v7/widget/az;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/az;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->h:Landroid/support/v7/widget/az;

    .line 117
    new-instance v0, Landroid/support/v7/widget/ay;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ay;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->v:Landroid/support/v7/widget/ay;

    .line 118
    new-instance v0, Landroid/support/v7/widget/ax;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ax;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->w:Landroid/support/v7/widget/ax;

    .line 119
    new-instance v0, Landroid/support/v7/widget/av;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/av;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->x:Landroid/support/v7/widget/av;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->i:Landroid/os/Handler;

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    .line 228
    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow;->l:Landroid/content/Context;

    .line 230
    sget-object v0, Landroid/support/v7/a/j;->U:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 232
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/ListPopupWindow;->p:I

    .line 234
    sget v1, Landroid/support/v7/a/j;->W:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/ListPopupWindow;->q:I

    .line 236
    iget v1, p0, Landroid/support/v7/widget/ListPopupWindow;->q:I

    if-eqz v1, :cond_0

    .line 237
    iput-boolean v3, p0, Landroid/support/v7/widget/ListPopupWindow;->r:Z

    .line 239
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 241
    new-instance v0, Landroid/support/v7/internal/widget/AppCompatPopupWindow;

    invoke-direct {v0, p1, p2, p3}, Landroid/support/v7/internal/widget/AppCompatPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    .line 242
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 245
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 246
    invoke-static {v0}, Landroid/support/v4/f/q;->a(Ljava/util/Locale;)I

    .line 247
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 17

    .prologue
    .line 584
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->l:Landroid/content/Context;

    new-instance v1, Landroid/support/v7/widget/aq;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Landroid/support/v7/widget/aq;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    new-instance v3, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v7/widget/ListPopupWindow;->j:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-direct {v3, v2, v1}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;-><init>(Landroid/content/Context;Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->m:Landroid/widget/ListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->g:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->setFocusableInTouchMode(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    new-instance v2, Landroid/support/v7/widget/ar;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/support/v7/widget/ar;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->w:Landroid/support/v7/widget/ax;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    :goto_1
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget-boolean v2, v0, Landroid/support/v7/widget/ListPopupWindow;->r:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v7/widget/ListPopupWindow;->q:I

    :cond_0
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ListPopupWindow;->f:Landroid/view/View;

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/ListPopupWindow;->q:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v10

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/ListPopupWindow;->n:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    add-int/2addr v1, v10

    .line 586
    :goto_3
    const/4 v3, 0x0

    .line 587
    const/4 v4, 0x0

    .line 589
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_b

    const/4 v2, 0x1

    .line 591
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 592
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_c

    .line 595
    const/4 v5, -0x1

    .line 602
    :goto_5
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ListPopupWindow;->n:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_12

    .line 605
    if-eqz v2, :cond_e

    move v6, v1

    .line 606
    :goto_6
    if-eqz v2, :cond_10

    .line 607
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_f

    const/4 v1, -0x1

    :goto_7
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 622
    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 624
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->f:Landroid/view/View;

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ListPopupWindow;->p:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/ListPopupWindow;->q:I

    invoke-virtual/range {v1 .. v6}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 665
    :cond_1
    :goto_9
    return-void

    .line 584
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    packed-switch v2, :pswitch_data_0

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    :goto_a
    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    invoke-virtual {v12}, Landroid/support/v7/internal/widget/ListViewCompat;->getListPaddingTop()I

    move-result v4

    invoke-virtual {v12}, Landroid/support/v7/internal/widget/ListViewCompat;->getListPaddingBottom()I

    move-result v5

    invoke-virtual {v12}, Landroid/support/v7/internal/widget/ListViewCompat;->getListPaddingLeft()I

    invoke-virtual {v12}, Landroid/support/v7/internal/widget/ListViewCompat;->getListPaddingRight()I

    invoke-virtual {v12}, Landroid/support/v7/internal/widget/ListViewCompat;->getDividerHeight()I

    move-result v3

    invoke-virtual {v12}, Landroid/support/v7/internal/widget/ListViewCompat;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v12}, Landroid/support/v7/internal/widget/ListViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v13

    if-nez v13, :cond_6

    add-int v6, v4, v5

    :goto_b
    if-lez v6, :cond_1c

    add-int/lit8 v1, v1, 0x0

    :goto_c
    add-int/2addr v1, v6

    goto/16 :goto_3

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    const/high16 v3, -0x80000000

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_a

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_a

    :cond_6
    add-int/2addr v5, v4

    if-lez v3, :cond_7

    if-eqz v6, :cond_7

    :goto_d
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-interface {v13}, Landroid/widget/ListAdapter;->getCount()I

    move-result v14

    const/4 v4, 0x0

    move v11, v4

    :goto_e
    if-ge v11, v14, :cond_a

    invoke-interface {v13, v11}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v4

    if-eq v4, v7, :cond_1d

    const/4 v7, 0x0

    move/from16 v16, v4

    move-object v4, v7

    move/from16 v7, v16

    :goto_f
    invoke-interface {v13, v11, v4, v12}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    if-eqz v4, :cond_8

    iget v15, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v15, :cond_8

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v4, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    :goto_10
    invoke-virtual {v8, v2, v4}, Landroid/view/View;->measure(II)V

    if-lez v11, :cond_1e

    add-int v4, v5, v3

    :goto_11
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v4

    if-lt v5, v10, :cond_9

    move v6, v10

    goto/16 :goto_b

    :cond_7
    const/4 v3, 0x0

    goto :goto_d

    :cond_8
    const/4 v4, 0x0

    const/4 v15, 0x0

    invoke-static {v4, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_10

    :cond_9
    move v4, v6

    add-int/lit8 v6, v11, 0x1

    move v11, v6

    move v6, v4

    goto :goto_e

    :cond_a
    move v6, v5

    goto/16 :goto_b

    .line 589
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 596
    :cond_c
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    const/4 v4, -0x2

    if-ne v3, v4, :cond_d

    .line 597
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ListPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    goto/16 :goto_5

    .line 599
    :cond_d
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    goto/16 :goto_5

    .line 605
    :cond_e
    const/4 v6, -0x1

    goto/16 :goto_6

    .line 607
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 611
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_11

    const/4 v1, -0x1

    :goto_12
    const/4 v3, -0x1

    invoke-virtual {v2, v1, v3}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    goto/16 :goto_8

    :cond_11
    const/4 v1, 0x0

    goto :goto_12

    .line 616
    :cond_12
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/ListPopupWindow;->n:I

    const/4 v3, -0x2

    if-ne v2, v3, :cond_13

    move v6, v1

    .line 617
    goto/16 :goto_8

    .line 619
    :cond_13
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ListPopupWindow;->n:I

    goto/16 :goto_8

    .line 627
    :cond_14
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    const/4 v5, -0x1

    if-ne v2, v5, :cond_18

    .line 628
    const/4 v2, -0x1

    .line 637
    :goto_13
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ListPopupWindow;->n:I

    const/4 v5, -0x1

    if-ne v3, v5, :cond_1a

    .line 638
    const/4 v1, -0x1

    .line 647
    :goto_14
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v2, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 648
    const/4 v1, 0x1

    sget-object v2, Landroid/support/v7/widget/ListPopupWindow;->k:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_15

    :try_start_0
    sget-object v2, Landroid/support/v7/widget/ListPopupWindow;->k:Ljava/lang/reflect/Method;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 652
    :cond_15
    :goto_15
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 653
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->v:Landroid/support/v7/widget/ay;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 654
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->f:Landroid/view/View;

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ListPopupWindow;->p:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/ListPopupWindow;->q:I

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/ListPopupWindow;->c:I

    invoke-static {v1, v2, v3, v4, v5}, Landroid/support/v4/widget/al;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    .line 656
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->setSelection(I)V

    .line 658
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v7/widget/ListPopupWindow;->j:Z

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    invoke-virtual {v1}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->isInTouchMode()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 659
    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    if-eqz v1, :cond_17

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->g:Z

    invoke-virtual {v1}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->requestLayout()V

    .line 661
    :cond_17
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v7/widget/ListPopupWindow;->j:Z

    if-nez v1, :cond_1

    .line 662
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->i:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->x:Landroid/support/v7/widget/av;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_9

    .line 630
    :cond_18
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    const/4 v5, -0x2

    if-ne v2, v5, :cond_19

    .line 631
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/ListPopupWindow;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v2, v3

    goto/16 :goto_13

    .line 633
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v2, v3

    goto/16 :goto_13

    .line 640
    :cond_1a
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ListPopupWindow;->n:I

    const/4 v5, -0x2

    if-ne v3, v5, :cond_1b

    .line 641
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v1, v4

    goto/16 :goto_14

    .line 643
    :cond_1b
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ListPopupWindow;->n:I

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v1, v4

    goto/16 :goto_14

    :catch_0
    move-exception v1

    goto/16 :goto_15

    :cond_1c
    move v1, v9

    goto/16 :goto_c

    :cond_1d
    move-object v4, v8

    goto/16 :goto_f

    :cond_1e
    move v4, v5

    goto/16 :goto_11

    .line 584
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 509
    if-eqz v0, :cond_0

    .line 510
    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 511
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    .line 515
    :goto_0
    return-void

    .line 513
    :cond_0
    iput p1, p0, Landroid/support/v7/widget/ListPopupWindow;->o:I

    goto :goto_0
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->u:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    .line 257
    new-instance v0, Landroid/support/v7/widget/aw;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/aw;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->u:Landroid/database/DataSetObserver;

    .line 261
    :cond_0
    :goto_0
    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow;->m:Landroid/widget/ListAdapter;

    .line 262
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->m:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->u:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 266
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    if-eqz v0, :cond_2

    .line 267
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->m:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ListPopupWindow$DropDownListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 269
    :cond_2
    return-void

    .line 258
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->m:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->m:Landroid/widget/ListAdapter;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->u:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 671
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 673
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 674
    iput-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->b:Landroid/support/v7/widget/ListPopupWindow$DropDownListView;

    .line 675
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->i:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->h:Landroid/support/v7/widget/az;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 676
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method
