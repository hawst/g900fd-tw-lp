.class public Landroid/support/v7/widget/StaggeredGridLayoutManager;
.super Landroid/support/v7/widget/bs;
.source "PG"


# instance fields
.field private A:Z

.field private final B:Ljava/lang/Runnable;

.field a:Landroid/support/v7/widget/bc;

.field b:Landroid/support/v7/widget/bc;

.field c:I

.field d:Z

.field e:I

.field f:I

.field g:Landroid/support/v7/widget/dc;

.field private h:I

.field private i:[Landroid/support/v7/widget/dh;

.field private j:I

.field private k:Landroid/support/v7/widget/ah;

.field private l:Z

.field private q:Ljava/util/BitSet;

.field private r:I

.field private s:Z

.field private t:Z

.field private u:Landroid/support/v7/widget/df;

.field private v:I

.field private w:I

.field private x:I

.field private final y:Landroid/support/v7/widget/db;

.field private z:Z


# direct methods
.method public constructor <init>(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 205
    invoke-direct {p0}, Landroid/support/v7/widget/bs;-><init>()V

    .line 96
    iput v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    .line 116
    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    .line 121
    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    .line 132
    iput v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    .line 138
    const/high16 v1, -0x80000000

    iput v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    .line 144
    new-instance v1, Landroid/support/v7/widget/dc;

    invoke-direct {v1}, Landroid/support/v7/widget/dc;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    .line 149
    const/4 v1, 0x2

    iput v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->r:I

    .line 175
    new-instance v1, Landroid/support/v7/widget/db;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/db;-><init>(Landroid/support/v7/widget/StaggeredGridLayoutManager;)V

    iput-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->y:Landroid/support/v7/widget/db;

    .line 183
    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    .line 189
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    .line 191
    new-instance v1, Landroid/support/v7/widget/cz;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/cz;-><init>(Landroid/support/v7/widget/StaggeredGridLayoutManager;)V

    iput-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->B:Ljava/lang/Runnable;

    .line 206
    iput p2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    .line 207
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-nez v1, :cond_0

    invoke-super {p0, v4}, Landroid/support/v7/widget/bs;->a(Ljava/lang/String;)V

    :cond_0
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-eq p1, v1, :cond_4

    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v2, v1, Landroid/support/v7/widget/dc;->a:[I

    if-eqz v2, :cond_1

    iget-object v2, v1, Landroid/support/v7/widget/dc;->a:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    :cond_1
    iput-object v4, v1, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    :cond_2
    iput p1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    new-instance v1, Ljava/util/BitSet;

    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    invoke-direct {v1, v2}, Ljava/util/BitSet;-><init>(I)V

    iput-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->q:Ljava/util/BitSet;

    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    new-array v1, v1, [Landroid/support/v7/widget/dh;

    iput-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    :goto_0
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    new-instance v2, Landroid/support/v7/widget/dh;

    invoke-direct {v2, p0, v0}, Landroid/support/v7/widget/dh;-><init>(Landroid/support/v7/widget/StaggeredGridLayoutManager;I)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 208
    :cond_4
    return-void
.end method

.method private static a(III)I
    .locals 2

    .prologue
    .line 992
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 1000
    :cond_0
    :goto_0
    return p0

    .line 995
    :cond_1
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 996
    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    .line 997
    :cond_2
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, p1

    sub-int/2addr v1, p2

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p0

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/ah;Landroid/support/v7/widget/cc;)I
    .locals 18

    .prologue
    .line 1308
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->q:Ljava/util/BitSet;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/BitSet;->set(IIZ)V

    .line 1318
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/ah;->d:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1320
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->b()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v3, v3, Landroid/support/v7/widget/ah;->a:I

    add-int/2addr v2, v3

    .line 1321
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v3, v3, Landroid/support/v7/widget/ah;->e:I

    add-int/2addr v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->e()I

    move-result v4

    add-int/2addr v3, v4

    move v14, v2

    move v15, v3

    .line 1329
    :goto_0
    move-object/from16 v0, p2

    iget v3, v0, Landroid/support/v7/widget/ah;->d:I

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v2, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v4, v4, v2

    iget-object v4, v4, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v4, v4, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v15}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/dh;II)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1325
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v3, v3, Landroid/support/v7/widget/ah;->a:I

    sub-int/2addr v2, v3

    .line 1326
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v3, v3, Landroid/support/v7/widget/ah;->e:I

    sub-int v3, v2, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->a()I

    move-result v4

    sub-int/2addr v3, v4

    move v14, v2

    move v15, v3

    goto :goto_0

    .line 1332
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->b()I

    move-result v2

    move/from16 v16, v2

    .line 1336
    :cond_3
    :goto_2
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/ah;->b:I

    if-ltz v2, :cond_7

    move-object/from16 v0, p2

    iget v3, v0, Landroid/support/v7/widget/ah;->b:I

    move-object/from16 v0, p3

    iget-boolean v2, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p3

    iget v2, v0, Landroid/support/v7/widget/cc;->f:I

    move-object/from16 v0, p3

    iget v4, v0, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v2, v4

    :goto_3
    if-ge v3, v2, :cond_7

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->q:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2c

    .line 1337
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/ah;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/bw;->b(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/ah;->b:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/support/v7/widget/ah;->c:I

    add-int/2addr v2, v4

    move-object/from16 v0, p2

    iput v2, v0, Landroid/support/v7/widget/ah;->b:I

    .line 1338
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    .line 1339
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/ah;->d:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_8

    .line 1340
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;)V

    .line 1344
    :goto_5
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->w:I

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->x:I

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    iget v8, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->leftMargin:I

    iget v9, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v9

    iget v9, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->rightMargin:I

    iget v10, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    invoke-static {v5, v8, v9}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(III)I

    move-result v5

    iget v8, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->topMargin:I

    iget v9, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v8, v9

    iget v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->bottomMargin:I

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v7

    invoke-static {v6, v8, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(III)I

    move-result v2

    invoke-virtual {v3, v5, v2}, Landroid/view/View;->measure(II)V

    .line 1346
    iget-object v2, v4, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v5, v2, Landroid/support/v7/widget/ce;->f:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_9

    iget v2, v2, Landroid/support/v7/widget/ce;->b:I

    .line 1347
    :goto_6
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v6, v5, Landroid/support/v7/widget/dc;->a:[I

    if-eqz v6, :cond_4

    iget-object v6, v5, Landroid/support/v7/widget/dc;->a:[I

    array-length v6, v6

    if-lt v2, v6, :cond_a

    :cond_4
    const/4 v5, -0x1

    move v6, v5

    .line 1349
    :goto_7
    const/4 v5, -0x1

    if-ne v6, v5, :cond_b

    const/4 v5, 0x1

    move v12, v5

    .line 1350
    :goto_8
    if-eqz v12, :cond_18

    .line 1351
    move-object/from16 v0, p2

    iget v5, v0, Landroid/support/v7/widget/ah;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    if-nez v6, :cond_e

    const/4 v6, -0x1

    if-ne v5, v6, :cond_c

    const/4 v5, 0x1

    :goto_9
    move-object/from16 v0, p0

    iget-boolean v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eq v5, v6, :cond_d

    const/4 v5, 0x1

    :goto_a
    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    add-int/lit8 v7, v5, -0x1

    const/4 v6, -0x1

    const/4 v5, -0x1

    :goto_b
    move-object/from16 v0, p2

    iget v8, v0, Landroid/support/v7/widget/ah;->d:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_14

    const/4 v10, 0x0

    const v8, 0x7fffffff

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v9}, Landroid/support/v7/widget/bc;->a()I

    move-result v13

    move v11, v7

    :goto_c
    if-eq v11, v6, :cond_15

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v9, v7, v11

    invoke-virtual {v9, v13}, Landroid/support/v7/widget/dh;->b(I)I

    move-result v7

    if-ge v7, v8, :cond_2f

    move-object v8, v9

    :goto_d
    add-int v9, v11, v5

    move v11, v9

    move-object v10, v8

    move v8, v7

    goto :goto_c

    .line 1332
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_2

    .line 1336
    :cond_6
    move-object/from16 v0, p3

    iget v2, v0, Landroid/support/v7/widget/cc;->e:I

    goto/16 :goto_3

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 1342
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;I)V

    goto/16 :goto_5

    .line 1346
    :cond_9
    iget v2, v2, Landroid/support/v7/widget/ce;->f:I

    goto :goto_6

    .line 1347
    :cond_a
    iget-object v5, v5, Landroid/support/v7/widget/dc;->a:[I

    aget v5, v5, v2

    move v6, v5

    goto :goto_7

    .line 1349
    :cond_b
    const/4 v5, 0x0

    move v12, v5

    goto :goto_8

    .line 1351
    :cond_c
    const/4 v5, 0x0

    goto :goto_9

    :cond_d
    const/4 v5, 0x0

    goto :goto_a

    :cond_e
    const/4 v6, -0x1

    if-ne v5, v6, :cond_f

    const/4 v5, 0x1

    :goto_e
    move-object/from16 v0, p0

    iget-boolean v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-ne v5, v6, :cond_10

    const/4 v5, 0x1

    :goto_f
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v6}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_11

    const/4 v6, 0x1

    :goto_10
    if-ne v5, v6, :cond_12

    const/4 v5, 0x1

    goto :goto_a

    :cond_f
    const/4 v5, 0x0

    goto :goto_e

    :cond_10
    const/4 v5, 0x0

    goto :goto_f

    :cond_11
    const/4 v6, 0x0

    goto :goto_10

    :cond_12
    const/4 v5, 0x0

    goto/16 :goto_a

    :cond_13
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    const/4 v5, 0x1

    goto :goto_b

    :cond_14
    const/4 v10, 0x0

    const/high16 v8, -0x80000000

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v9}, Landroid/support/v7/widget/bc;->b()I

    move-result v13

    move v11, v7

    :goto_11
    if-eq v11, v6, :cond_15

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v9, v7, v11

    invoke-virtual {v9, v13}, Landroid/support/v7/widget/dh;->a(I)I

    move-result v7

    if-le v7, v8, :cond_2e

    move-object v8, v9

    :goto_12
    add-int v9, v11, v5

    move v11, v9

    move-object v10, v8

    move v8, v7

    goto :goto_11

    .line 1352
    :cond_15
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    invoke-virtual {v5, v2}, Landroid/support/v7/widget/dc;->c(I)V

    iget-object v5, v5, Landroid/support/v7/widget/dc;->a:[I

    iget v6, v10, Landroid/support/v7/widget/dh;->e:I

    aput v6, v5, v2

    move-object/from16 v17, v10

    .line 1365
    :goto_13
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/ah;->d:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_19

    .line 1366
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/dh;->b(I)I

    move-result v5

    .line 1368
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v2

    add-int v7, v5, v2

    .line 1369
    if-eqz v12, :cond_16

    .line 1380
    :cond_16
    :goto_14
    move-object/from16 v0, v17

    iput-object v0, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    .line 1395
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/ah;->d:I

    const/4 v6, 0x1

    if-ne v2, v6, :cond_1a

    iget-object v2, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/dh;->b(Landroid/view/View;)V

    .line 1396
    :goto_15
    move-object/from16 v0, v17

    iget v2, v0, Landroid/support/v7/widget/dh;->e:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j:I

    mul-int/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->a()I

    move-result v4

    add-int/2addr v4, v2

    .line 1399
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v2

    add-int v6, v4, v2

    .line 1400
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    const/4 v8, 0x1

    if-ne v2, v8, :cond_1b

    move-object/from16 v2, p0

    .line 1401
    invoke-direct/range {v2 .. v7}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;IIII)V

    .line 1406
    :goto_16
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v2, v2, Landroid/support/v7/widget/ah;->d:I

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v2, v15}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/dh;II)V

    .line 1411
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v2, v2, Landroid/support/v7/widget/ah;->d:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_23

    move-object/from16 v0, v17

    iget v2, v0, Landroid/support/v7/widget/dh;->b:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_1c

    move-object/from16 v0, v17

    iget v2, v0, Landroid/support/v7/widget/dh;->b:I

    :goto_17
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/dh;->a(I)I

    move-result v4

    const/4 v3, 0x1

    :goto_18
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v3, v5, :cond_1d

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v5, v5, v3

    invoke-virtual {v5, v2}, Landroid/support/v7/widget/dh;->a(I)I

    move-result v5

    if-le v5, v4, :cond_17

    move v4, v5

    :cond_17
    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    .line 1360
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v10, v2, v6

    move-object/from16 v17, v10

    goto/16 :goto_13

    .line 1371
    :cond_19
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/dh;->a(I)I

    move-result v7

    .line 1379
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v2

    sub-int v5, v7, v2

    goto/16 :goto_14

    .line 1395
    :cond_1a
    iget-object v2, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/dh;->a(Landroid/view/View;)V

    goto/16 :goto_15

    :cond_1b
    move-object/from16 v8, p0

    move-object v9, v3

    move v10, v5

    move v11, v4

    move v12, v7

    move v13, v6

    .line 1403
    invoke-direct/range {v8 .. v13}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;IIII)V

    goto :goto_16

    .line 1411
    :cond_1c
    invoke-virtual/range {v17 .. v17}, Landroid/support/v7/widget/dh;->a()V

    move-object/from16 v0, v17

    iget v2, v0, Landroid/support/v7/widget/dh;->b:I

    goto :goto_17

    :cond_1d
    invoke-static {v14, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v3}, Landroid/support/v7/widget/bc;->c()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->a()I

    move-result v4

    sub-int/2addr v3, v4

    add-int v6, v2, v3

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_19
    if-ltz v4, :cond_3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v7}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v2

    if-le v2, v6, :cond_3

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    iget-object v8, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget-object v2, v8, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    iget-object v2, v8, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    add-int/lit8 v3, v9, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    const/4 v5, 0x0

    iput-object v5, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget-object v5, v2, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v5, v5, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_21

    const/4 v5, 0x1

    :goto_1a
    if-nez v5, :cond_1e

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v2, v2, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_22

    const/4 v2, 0x1

    :goto_1b
    if-eqz v2, :cond_1f

    :cond_1e
    iget v2, v8, Landroid/support/v7/widget/dh;->d:I

    iget-object v5, v8, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v5, v5, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v5, v3}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v8, Landroid/support/v7/widget/dh;->d:I

    :cond_1f
    const/4 v2, 0x1

    if-ne v9, v2, :cond_20

    const/high16 v2, -0x80000000

    iput v2, v8, Landroid/support/v7/widget/dh;->b:I

    :cond_20
    const/high16 v2, -0x80000000

    iput v2, v8, Landroid/support/v7/widget/dh;->c:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v7, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;Landroid/support/v7/widget/bw;)V

    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_19

    :cond_21
    const/4 v5, 0x0

    goto :goto_1a

    :cond_22
    const/4 v2, 0x0

    goto :goto_1b

    :cond_23
    move-object/from16 v0, v17

    iget v2, v0, Landroid/support/v7/widget/dh;->c:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_25

    move-object/from16 v0, v17

    iget v2, v0, Landroid/support/v7/widget/dh;->c:I

    :goto_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/dh;->b(I)I

    move-result v4

    const/4 v3, 0x1

    :goto_1d
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v3, v5, :cond_26

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v5, v5, v3

    invoke-virtual {v5, v2}, Landroid/support/v7/widget/dh;->b(I)I

    move-result v5

    if-ge v5, v4, :cond_24

    move v4, v5

    :cond_24
    add-int/lit8 v3, v3, 0x1

    goto :goto_1d

    :cond_25
    invoke-virtual/range {v17 .. v17}, Landroid/support/v7/widget/dh;->b()V

    move-object/from16 v0, v17

    iget v2, v0, Landroid/support/v7/widget/dh;->c:I

    goto :goto_1c

    :cond_26
    invoke-static {v14, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v3}, Landroid/support/v7/widget/bc;->c()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->a()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int v5, v2, v3

    :goto_1e
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v2

    if-lez v2, :cond_3

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v6}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v2

    if-ge v2, v5, :cond_3

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    iget-object v7, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget-object v2, v7, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    const/4 v4, 0x0

    iput-object v4, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget-object v4, v7, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_27

    const/high16 v4, -0x80000000

    iput v4, v7, Landroid/support/v7/widget/dh;->c:I

    :cond_27
    iget-object v4, v2, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v4, v4, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_2a

    const/4 v4, 0x1

    :goto_1f
    if-nez v4, :cond_28

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v2, v2, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_2b

    const/4 v2, 0x1

    :goto_20
    if-eqz v2, :cond_29

    :cond_28
    iget v2, v7, Landroid/support/v7/widget/dh;->d:I

    iget-object v4, v7, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v4, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v7, Landroid/support/v7/widget/dh;->d:I

    :cond_29
    const/high16 v2, -0x80000000

    iput v2, v7, Landroid/support/v7/widget/dh;->b:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v6, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;Landroid/support/v7/widget/bw;)V

    goto :goto_1e

    :cond_2a
    const/4 v4, 0x0

    goto :goto_1f

    :cond_2b
    const/4 v2, 0x0

    goto :goto_20

    .line 1416
    :cond_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v2, v2, Landroid/support/v7/widget/ah;->d:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2d

    .line 1417
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(I)I

    move-result v2

    .line 1418
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v4, v4, Landroid/support/v7/widget/ah;->a:I

    sub-int v2, v14, v2

    add-int/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1421
    :goto_21
    return v2

    .line 1420
    :cond_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->b()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j(I)I

    move-result v2

    .line 1421
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v4, v4, Landroid/support/v7/widget/ah;->a:I

    sub-int/2addr v2, v14

    add-int/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_21

    :cond_2e
    move v7, v8

    move-object v8, v10

    goto/16 :goto_12

    :cond_2f
    move v7, v8

    move-object v8, v10

    goto/16 :goto_d
.end method

.method private a(Landroid/support/v7/widget/cc;)I
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 911
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_0

    .line 915
    :goto_0
    return v4

    .line 914
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k()V

    .line 915
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_2

    :goto_2
    invoke-direct {p0, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    iget-boolean v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v7/widget/ck;->a(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/bc;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/bs;ZZ)I

    move-result v4

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2
.end method

.method private a(Z)Landroid/view/View;
    .locals 6

    .prologue
    .line 1138
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k()V

    .line 1139
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    .line 1140
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v3

    .line 1141
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v4

    .line 1142
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    .line 1143
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    .line 1144
    if-eqz p1, :cond_0

    iget-object v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v5

    if-lt v5, v2, :cond_1

    :cond_0
    iget-object v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v5

    if-gt v5, v3, :cond_1

    .line 1149
    :goto_1
    return-object v0

    .line 1142
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1149
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(ILandroid/support/v7/widget/cc;)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1199
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v3, v2, Landroid/support/v7/widget/ah;->a:I

    .line 1200
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput p1, v2, Landroid/support/v7/widget/ah;->b:I

    .line 1201
    iget-object v2, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    iget-boolean v2, v2, Landroid/support/v7/widget/ca;->k:Z

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 1202
    iget v2, p2, Landroid/support/v7/widget/cc;->a:I

    .line 1203
    iget-boolean v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-ge v2, p1, :cond_1

    move v2, v0

    :goto_1
    if-ne v4, v2, :cond_2

    .line 1204
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v3, v2, Landroid/support/v7/widget/ah;->e:I

    .line 1211
    :goto_2
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v1, v2, Landroid/support/v7/widget/ah;->d:I

    .line 1212
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget-boolean v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v3, :cond_4

    :goto_3
    iput v0, v2, Landroid/support/v7/widget/ah;->c:I

    .line 1214
    return-void

    :cond_0
    move v2, v3

    .line 1201
    goto :goto_0

    :cond_1
    move v2, v3

    .line 1203
    goto :goto_1

    .line 1206
    :cond_2
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget-object v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v3}, Landroid/support/v7/widget/bc;->d()I

    move-result v3

    iput v3, v2, Landroid/support/v7/widget/ah;->e:I

    goto :goto_2

    .line 1209
    :cond_3
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v3, v2, Landroid/support/v7/widget/ah;->e:I

    goto :goto_2

    :cond_4
    move v0, v1

    .line 1212
    goto :goto_3
.end method

.method private a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)V
    .locals 2

    .prologue
    .line 1168
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j(I)I

    move-result v0

    .line 1169
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->b()I

    move-result v1

    sub-int v0, v1, v0

    .line 1171
    if-lez v0, :cond_0

    .line 1172
    neg-int v1, v0

    invoke-direct {p0, v1, p1, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v1

    neg-int v1, v1

    .line 1176
    sub-int/2addr v0, v1

    .line 1177
    if-eqz p3, :cond_0

    if-lez v0, :cond_0

    .line 1178
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->a(I)V

    .line 1180
    :cond_0
    return-void
.end method

.method private a(Landroid/support/v7/widget/dh;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, -0x80000000

    .line 1510
    iget v1, p1, Landroid/support/v7/widget/dh;->d:I

    .line 1511
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 1512
    iget v0, p1, Landroid/support/v7/widget/dh;->b:I

    if-eq v0, v2, :cond_1

    iget v0, p1, Landroid/support/v7/widget/dh;->b:I

    .line 1513
    :goto_0
    add-int/2addr v0, v1

    if-ge v0, p3, :cond_0

    .line 1514
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->q:Ljava/util/BitSet;

    iget v1, p1, Landroid/support/v7/widget/dh;->e:I

    invoke-virtual {v0, v1, v3}, Ljava/util/BitSet;->set(IZ)V

    .line 1522
    :cond_0
    :goto_1
    return-void

    .line 1512
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/widget/dh;->a()V

    iget v0, p1, Landroid/support/v7/widget/dh;->b:I

    goto :goto_0

    .line 1517
    :cond_2
    iget v0, p1, Landroid/support/v7/widget/dh;->c:I

    if-eq v0, v2, :cond_3

    iget v0, p1, Landroid/support/v7/widget/dh;->c:I

    .line 1518
    :goto_2
    sub-int/2addr v0, v1

    if-le v0, p3, :cond_0

    .line 1519
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->q:Ljava/util/BitSet;

    iget v1, p1, Landroid/support/v7/widget/dh;->e:I

    invoke-virtual {v0, v1, v3}, Ljava/util/BitSet;->set(IZ)V

    goto :goto_1

    .line 1517
    :cond_3
    invoke-virtual {p1}, Landroid/support/v7/widget/dh;->b()V

    iget v0, p1, Landroid/support/v7/widget/dh;->c:I

    goto :goto_2
.end method

.method private b(Z)Landroid/view/View;
    .locals 5

    .prologue
    .line 1153
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k()V

    .line 1154
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    .line 1155
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v3

    .line 1156
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1157
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    .line 1158
    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v4

    if-lt v4, v2, :cond_1

    if-eqz p1, :cond_0

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v4

    if-gt v4, v3, :cond_1

    .line 1163
    :cond_0
    :goto_1
    return-object v0

    .line 1156
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1163
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(III)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1280
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1281
    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/dc;->b(I)I

    .line 1282
    packed-switch p3, :pswitch_data_0

    .line 1296
    :goto_1
    :pswitch_0
    add-int v2, p1, p2

    if-gt v2, v0, :cond_4

    .line 1304
    :cond_0
    :goto_2
    return-void

    .line 1280
    :cond_1
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_0

    .line 1284
    :pswitch_1
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    invoke-virtual {v2, p1, p2}, Landroid/support/v7/widget/dc;->b(II)V

    goto :goto_1

    .line 1287
    :pswitch_2
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    invoke-virtual {v2, p1, p2}, Landroid/support/v7/widget/dc;->a(II)V

    goto :goto_1

    .line 1291
    :pswitch_3
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    invoke-virtual {v2, p1, v3}, Landroid/support/v7/widget/dc;->a(II)V

    .line 1292
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    invoke-virtual {v2, p2, v3}, Landroid/support/v7/widget/dc;->b(II)V

    goto :goto_1

    .line 1300
    :cond_4
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_6

    .line 1301
    :cond_5
    :goto_3
    if-gt p1, v1, :cond_0

    .line 1302
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_2

    .line 1300
    :cond_6
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v1

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-eqz v0, :cond_5

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v1

    goto :goto_3

    .line 1282
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private b(ILandroid/support/v7/widget/cc;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1217
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v2, v1, Landroid/support/v7/widget/ah;->a:I

    .line 1218
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput p1, v1, Landroid/support/v7/widget/ah;->b:I

    .line 1219
    iget-object v1, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    iget-boolean v1, v1, Landroid/support/v7/widget/ca;->k:Z

    if-eqz v1, :cond_1

    move v1, v0

    :goto_0
    if-eqz v1, :cond_4

    .line 1220
    iget v1, p2, Landroid/support/v7/widget/cc;->a:I

    .line 1221
    iget-boolean v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-le v1, p1, :cond_2

    move v1, v0

    :goto_1
    if-ne v3, v1, :cond_3

    .line 1222
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v2, v1, Landroid/support/v7/widget/ah;->e:I

    .line 1229
    :goto_2
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v0, v1, Landroid/support/v7/widget/ah;->d:I

    .line 1230
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget-boolean v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v2, :cond_0

    const/4 v0, -0x1

    :cond_0
    iput v0, v1, Landroid/support/v7/widget/ah;->c:I

    .line 1232
    return-void

    :cond_1
    move v1, v2

    .line 1219
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1221
    goto :goto_1

    .line 1224
    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->d()I

    move-result v2

    iput v2, v1, Landroid/support/v7/widget/ah;->e:I

    goto :goto_2

    .line 1227
    :cond_4
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v2, v1, Landroid/support/v7/widget/ah;->e:I

    goto :goto_2
.end method

.method private b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)V
    .locals 2

    .prologue
    .line 1184
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(I)I

    move-result v0

    .line 1185
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->a()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1187
    if-lez v0, :cond_0

    .line 1188
    invoke-direct {p0, v0, p1, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v1

    .line 1192
    sub-int/2addr v0, v1

    .line 1193
    if-eqz p3, :cond_0

    if-lez v0, :cond_0

    .line 1194
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->a(I)V

    .line 1196
    :cond_0
    return-void
.end method

.method private b(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 1489
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    .line 1496
    iget v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->leftMargin:I

    add-int/2addr v1, p2

    iget v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->topMargin:I

    add-int/2addr v2, p3

    iget v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->rightMargin:I

    sub-int v3, p4, v3

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->bottomMargin:I

    sub-int v0, p5, v0

    invoke-static {p1, v1, v2, v3, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;IIII)V

    .line 1498
    return-void
.end method

.method private d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1749
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k()V

    .line 1751
    if-lez p1, :cond_3

    .line 1752
    iget-object v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v1, v3, Landroid/support/v7/widget/ah;->d:I

    .line 1753
    iget-object v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget-boolean v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v4, :cond_1

    :goto_0
    iput v0, v3, Landroid/support/v7/widget/ah;->c:I

    .line 1755
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 1762
    :goto_1
    iget-object v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v4, v4, Landroid/support/v7/widget/ah;->c:I

    add-int/2addr v0, v4

    iput v0, v3, Landroid/support/v7/widget/ah;->b:I

    .line 1763
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1764
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v3, v0, Landroid/support/v7/widget/ah;->a:I

    .line 1765
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget-object v4, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    if-eqz v4, :cond_6

    iget-object v4, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    iget-boolean v4, v4, Landroid/support/v7/widget/ca;->k:Z

    if-eqz v4, :cond_6

    :goto_2
    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->d()I

    move-result v2

    :cond_0
    iput v2, v0, Landroid/support/v7/widget/ah;->e:I

    .line 1766
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    invoke-direct {p0, p2, v0, p3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/ah;Landroid/support/v7/widget/cc;)I

    move-result v0

    .line 1768
    if-ge v3, v0, :cond_7

    .line 1779
    :goto_3
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bc;->a(I)V

    .line 1781
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->s:Z

    .line 1782
    return p1

    :cond_1
    move v0, v1

    .line 1753
    goto :goto_0

    .line 1755
    :cond_2
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_1

    .line 1757
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iput v0, v3, Landroid/support/v7/widget/ah;->d:I

    .line 1758
    iget-object v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget-boolean v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v4, :cond_4

    move v0, v1

    :cond_4
    iput v0, v3, Landroid/support/v7/widget/ah;->c:I

    .line 1760
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_1

    :cond_6
    move v1, v2

    .line 1765
    goto :goto_2

    .line 1770
    :cond_7
    if-gez p1, :cond_8

    .line 1771
    neg-int p1, v0

    goto :goto_3

    :cond_8
    move p1, v0

    .line 1773
    goto :goto_3
.end method

.method private h(Landroid/support/v7/widget/cc;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 932
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_0

    .line 936
    :goto_0
    return v4

    .line 935
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k()V

    .line 936
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_2

    :goto_2
    invoke-direct {p0, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/ck;->a(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/bc;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/bs;Z)I

    move-result v4

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2
.end method

.method private i(I)I
    .locals 3

    .prologue
    .line 1536
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/dh;->a(I)I

    move-result v1

    .line 1537
    const/4 v0, 0x1

    :goto_0
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v0, v2, :cond_1

    .line 1538
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/dh;->a(I)I

    move-result v2

    .line 1539
    if-ge v2, v1, :cond_0

    move v1, v2

    .line 1537
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1543
    :cond_1
    return v1
.end method

.method private i(Landroid/support/v7/widget/cc;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 953
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_0

    .line 957
    :goto_0
    return v4

    .line 956
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k()V

    .line 957
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_2

    :goto_2
    invoke-direct {p0, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/ck;->b(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/bc;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/bs;Z)I

    move-result v4

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2
.end method

.method private j(I)I
    .locals 3

    .prologue
    .line 1547
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/dh;->b(I)I

    move-result v1

    .line 1548
    const/4 v0, 0x1

    :goto_0
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v0, v2, :cond_1

    .line 1549
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/dh;->b(I)I

    move-result v2

    .line 1550
    if-le v2, v1, :cond_0

    move v1, v2

    .line 1548
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1554
    :cond_1
    return v1
.end method

.method private k()V
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    if-nez v0, :cond_0

    .line 493
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    invoke-static {p0, v0}, Landroid/support/v7/widget/bc;->a(Landroid/support/v7/widget/bs;I)Landroid/support/v7/widget/bc;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    .line 494
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    rsub-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Landroid/support/v7/widget/bc;->a(Landroid/support/v7/widget/bs;I)Landroid/support/v7/widget/bc;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bc;

    .line 496
    new-instance v0, Landroid/support/v7/widget/ah;

    invoke-direct {v0}, Landroid/support/v7/widget/ah;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    .line 498
    :cond_0
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 507
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v2

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    .line 508
    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    .line 512
    :goto_1
    return-void

    :cond_1
    move v2, v1

    .line 507
    goto :goto_0

    .line 510
    :cond_2
    iget-boolean v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    if-nez v2, :cond_3

    :goto_2
    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 1678
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 1122
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    if-nez v0, :cond_0

    .line 1123
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    .line 1125
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    goto :goto_0
.end method

.method public final a()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1830
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    .prologue
    .line 1836
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    .prologue
    .line 1841
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 1842
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 1844
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1257
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(III)V

    .line 1258
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1005
    instance-of v0, p1, Landroid/support/v7/widget/df;

    if-eqz v0, :cond_0

    .line 1006
    check-cast p1, Landroid/support/v7/widget/df;

    iput-object p1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    .line 1007
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 1011
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    .line 1262
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v1, v0, Landroid/support/v7/widget/dc;->a:[I

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/support/v7/widget/dc;->a:[I

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    :cond_0
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    .line 1263
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 1264
    :cond_1
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 2

    .prologue
    .line 1698
    new-instance v0, Landroid/support/v7/widget/da;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/da;-><init>(Landroid/support/v7/widget/StaggeredGridLayoutManager;Landroid/content/Context;)V

    .line 1712
    iput p2, v0, Landroid/support/v7/widget/ca;->g:I

    .line 1713
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ca;)V

    .line 1714
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/bw;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/high16 v4, -0x80000000

    .line 269
    move v0, v1

    :goto_0
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v0, v2, :cond_0

    .line 270
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v2, v2, v0

    iget-object v3, v2, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iput v4, v2, Landroid/support/v7/widget/dh;->b:I

    iput v4, v2, Landroid/support/v7/widget/dh;->c:I

    iput v1, v2, Landroid/support/v7/widget/dh;->d:I

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 272
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Landroid/view/View;Landroid/support/v4/view/a/h;)V
    .locals 8

    .prologue
    .line 1067
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1068
    instance-of v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    if-nez v1, :cond_0

    .line 1069
    invoke-super {p0, p3, p4}, Landroid/support/v7/widget/bs;->a(Landroid/view/View;Landroid/support/v4/view/a/h;)V

    .line 1084
    :goto_0
    return-void

    .line 1072
    :cond_0
    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    .line 1073
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    if-nez v1, :cond_2

    .line 1074
    iget-object v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    if-nez v1, :cond_1

    const/4 v1, -0x1

    :goto_1
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Landroid/support/v4/view/a/q;

    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/view/a/k;->a(IIIIZZ)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/support/v4/view/a/q;-><init>(Ljava/lang/Object;)V

    sget-object v1, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v2, p4, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    move-object v0, v7

    check-cast v0, Landroid/support/v4/view/a/q;

    iget-object v0, v0, Landroid/support/v4/view/a/q;->a:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget v1, v0, Landroid/support/v7/widget/dh;->e:I

    goto :goto_1

    .line 1079
    :cond_2
    const/4 v1, -0x1

    const/4 v2, -0x1

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    if-nez v3, :cond_3

    const/4 v3, -0x1

    :goto_2
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Landroid/support/v4/view/a/q;

    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/view/a/k;->a(IIIIZZ)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/support/v4/view/a/q;-><init>(Ljava/lang/Object;)V

    sget-object v1, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v2, p4, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    move-object v0, v7

    check-cast v0, Landroid/support/v4/view/a/q;

    iget-object v0, v0, Landroid/support/v4/view/a/q;->a:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget v3, v0, Landroid/support/v7/widget/dh;->e:I

    goto :goto_2
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1088
    invoke-super {p0, p1}, Landroid/support/v7/widget/bs;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1089
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-lez v0, :cond_0

    .line 1090
    invoke-static {p1}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/ae;

    move-result-object v0

    .line 1092
    invoke-direct {p0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v1

    .line 1093
    invoke-direct {p0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v2

    .line 1094
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 1107
    :cond_0
    :goto_0
    return-void

    .line 1097
    :cond_1
    invoke-static {v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v1

    .line 1098
    invoke-static {v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v2

    .line 1099
    if-ge v1, v2, :cond_2

    .line 1100
    sget-object v3, Landroid/support/v4/view/a/ae;->a:Landroid/support/v4/view/a/ah;

    iget-object v4, v0, Landroid/support/v4/view/a/ae;->b:Ljava/lang/Object;

    invoke-interface {v3, v4, v1}, Landroid/support/v4/view/a/ah;->a(Ljava/lang/Object;I)V

    .line 1101
    sget-object v1, Landroid/support/v4/view/a/ae;->a:Landroid/support/v4/view/a/ah;

    iget-object v0, v0, Landroid/support/v4/view/a/ae;->b:Ljava/lang/Object;

    invoke-interface {v1, v0, v2}, Landroid/support/v4/view/a/ah;->c(Ljava/lang/Object;I)V

    goto :goto_0

    .line 1103
    :cond_2
    sget-object v3, Landroid/support/v4/view/a/ae;->a:Landroid/support/v4/view/a/ah;

    iget-object v4, v0, Landroid/support/v4/view/a/ae;->b:Ljava/lang/Object;

    invoke-interface {v3, v4, v2}, Landroid/support/v4/view/a/ah;->a(Ljava/lang/Object;I)V

    .line 1104
    sget-object v2, Landroid/support/v4/view/a/ae;->a:Landroid/support/v4/view/a/ah;

    iget-object v0, v0, Landroid/support/v4/view/a/ae;->b:Ljava/lang/Object;

    invoke-interface {v2, v0, v1}, Landroid/support/v4/view/a/ah;->c(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-nez v0, :cond_0

    .line 467
    invoke-super {p0, p1}, Landroid/support/v7/widget/bs;->a(Ljava/lang/String;)V

    .line 469
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1850
    instance-of v0, p1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    return v0
.end method

.method b(I)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1688
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v3

    if-nez v3, :cond_2

    .line 1689
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_1

    .line 1692
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 1689
    goto :goto_0

    .line 1691
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v3

    if-nez v3, :cond_4

    move v3, v0

    .line 1692
    :goto_1
    if-ge p1, v3, :cond_3

    move v0, v1

    :cond_3
    iget-boolean v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eq v0, v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 1691
    :cond_4
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v3

    goto :goto_1
.end method

.method public final b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 1684
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 2

    .prologue
    .line 1131
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1132
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    .line 1134
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/bs;->b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 907
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 1252
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(III)V

    .line 1253
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 923
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final c()Landroid/os/Parcelable;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, -0x1

    const/high16 v4, -0x80000000

    const/4 v1, 0x0

    .line 1015
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-eqz v0, :cond_0

    .line 1016
    new-instance v0, Landroid/support/v7/widget/df;

    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/df;-><init>(Landroid/support/v7/widget/df;)V

    .line 1061
    :goto_0
    return-object v0

    .line 1018
    :cond_0
    new-instance v3, Landroid/support/v7/widget/df;

    invoke-direct {v3}, Landroid/support/v7/widget/df;-><init>()V

    .line 1019
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    iput-boolean v0, v3, Landroid/support/v7/widget/df;->h:Z

    .line 1020
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->s:Z

    iput-boolean v0, v3, Landroid/support/v7/widget/df;->i:Z

    .line 1021
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t:Z

    iput-boolean v0, v3, Landroid/support/v7/widget/df;->j:Z

    .line 1023
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v0, v0, Landroid/support/v7/widget/dc;->a:[I

    if-eqz v0, :cond_2

    .line 1024
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v0, v0, Landroid/support/v7/widget/dc;->a:[I

    iput-object v0, v3, Landroid/support/v7/widget/df;->f:[I

    .line 1025
    iget-object v0, v3, Landroid/support/v7/widget/df;->f:[I

    array-length v0, v0

    iput v0, v3, Landroid/support/v7/widget/df;->e:I

    .line 1026
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v0, v0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    iput-object v0, v3, Landroid/support/v7/widget/df;->g:Ljava/util/List;

    .line 1031
    :goto_1
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-lez v0, :cond_9

    .line 1032
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k()V

    .line 1033
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->s:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    iput v0, v3, Landroid/support/v7/widget/df;->a:I

    .line 1035
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_6

    invoke-direct {p0, v5}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_7

    move v0, v2

    :goto_4
    iput v0, v3, Landroid/support/v7/widget/df;->b:I

    .line 1036
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    iput v0, v3, Landroid/support/v7/widget/df;->c:I

    .line 1037
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    new-array v0, v0, [I

    iput-object v0, v3, Landroid/support/v7/widget/df;->d:[I

    .line 1038
    :goto_5
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v1, v0, :cond_a

    .line 1040
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->s:Z

    if-eqz v0, :cond_8

    .line 1041
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v0, v0, v1

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/dh;->b(I)I

    move-result v0

    .line 1042
    if-eq v0, v4, :cond_1

    .line 1043
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->b()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1051
    :cond_1
    :goto_6
    iget-object v2, v3, Landroid/support/v7/widget/df;->d:[I

    aput v0, v2, v1

    .line 1038
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1028
    :cond_2
    iput v1, v3, Landroid/support/v7/widget/df;->e:I

    goto :goto_1

    .line 1033
    :cond_3
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_2

    .line 1035
    :cond_6
    invoke-direct {p0, v5}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v0

    goto :goto_3

    :cond_7
    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_4

    .line 1046
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v0, v0, v1

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/dh;->a(I)I

    move-result v0

    .line 1047
    if-eq v0, v4, :cond_1

    .line 1048
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_6

    .line 1054
    :cond_9
    iput v2, v3, Landroid/support/v7/widget/df;->a:I

    .line 1055
    iput v2, v3, Landroid/support/v7/widget/df;->b:I

    .line 1056
    iput v1, v3, Landroid/support/v7/widget/df;->c:I

    :cond_a
    move-object v0, v3

    .line 1061
    goto/16 :goto_0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1718
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->a:I

    if-eq v0, p1, :cond_0

    .line 1719
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/df;->d:[I

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/df;->c:I

    iput v2, v0, Landroid/support/v7/widget/df;->a:I

    iput v2, v0, Landroid/support/v7/widget/df;->b:I

    .line 1721
    :cond_0
    iput p1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    .line 1722
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    .line 1723
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 1724
    :cond_1
    return-void
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 1273
    const/4 v0, 0x2

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(III)V

    .line 1274
    return-void
.end method

.method public final c(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, -0x1

    const/4 v3, 0x1

    const/high16 v9, -0x80000000

    const/4 v1, 0x0

    .line 532
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k()V

    .line 534
    iget-object v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->y:Landroid/support/v7/widget/db;

    .line 535
    iput v10, v5, Landroid/support/v7/widget/db;->a:I

    iput v9, v5, Landroid/support/v7/widget/db;->b:I

    iput-boolean v1, v5, Landroid/support/v7/widget/db;->c:Z

    iput-boolean v1, v5, Landroid/support/v7/widget/db;->d:Z

    .line 537
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-eqz v0, :cond_10

    .line 538
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->c:I

    if-lez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->c:I

    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_0
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v2, v2, v0

    iget-object v4, v2, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iput v9, v2, Landroid/support/v7/widget/dh;->b:I

    iput v9, v2, Landroid/support/v7/widget/dh;->c:I

    iput v1, v2, Landroid/support/v7/widget/dh;->d:I

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-object v2, v2, Landroid/support/v7/widget/df;->d:[I

    aget v2, v2, v0

    if-eq v2, v9, :cond_0

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-boolean v4, v4, Landroid/support/v7/widget/df;->i:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->b()I

    move-result v4

    add-int/2addr v2, v4

    :cond_0
    :goto_1
    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v4, v4, v0

    iput v2, v4, Landroid/support/v7/widget/dh;->b:I

    iput v2, v4, Landroid/support/v7/widget/dh;->c:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->a()I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iput-object v11, v0, Landroid/support/v7/widget/df;->d:[I

    iput v1, v0, Landroid/support/v7/widget/df;->c:I

    iput v1, v0, Landroid/support/v7/widget/df;->e:I

    iput-object v11, v0, Landroid/support/v7/widget/df;->f:[I

    iput-object v11, v0, Landroid/support/v7/widget/df;->g:Ljava/util/List;

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v2, v2, Landroid/support/v7/widget/df;->b:I

    iput v2, v0, Landroid/support/v7/widget/df;->a:I

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-boolean v0, v0, Landroid/support/v7/widget/df;->j:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t:Z

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-boolean v0, v0, Landroid/support/v7/widget/df;->h:Z

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-nez v2, :cond_4

    invoke-super {p0, v11}, Landroid/support/v7/widget/bs;->a(Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-eqz v2, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-boolean v2, v2, Landroid/support/v7/widget/df;->h:Z

    if-eq v2, v0, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iput-boolean v0, v2, Landroid/support/v7/widget/df;->h:Z

    :cond_5
    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    :cond_6
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l()V

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->a:I

    if-eq v0, v10, :cond_f

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->a:I

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-boolean v0, v0, Landroid/support/v7/widget/df;->i:Z

    iput-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    :goto_2
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->e:I

    if-le v0, v3, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-object v2, v2, Landroid/support/v7/widget/df;->f:[I

    iput-object v2, v0, Landroid/support/v7/widget/dc;->a:[I

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget-object v2, v2, Landroid/support/v7/widget/df;->g:Ljava/util/List;

    iput-object v2, v0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    .line 544
    :cond_7
    :goto_3
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_8

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    if-ne v0, v10, :cond_11

    :cond_8
    move v0, v1

    :goto_4
    if-nez v0, :cond_9

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->s:Z

    if-eqz v0, :cond_27

    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_24

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v2, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v2

    :goto_5
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_6
    if-ltz v4, :cond_26

    invoke-virtual {p0, v4}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v2

    if-ltz v2, :cond_25

    if-ge v2, v0, :cond_25

    move v0, v2

    :goto_7
    iput v0, v5, Landroid/support/v7/widget/db;->a:I

    iput v9, v5, Landroid/support/v7/widget/db;->b:I

    .line 546
    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-nez v0, :cond_c

    .line 547
    iget-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    iget-boolean v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->s:Z

    if-ne v0, v2, :cond_a

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v0

    if-ne v0, v3, :cond_2b

    move v0, v3

    :goto_8
    iget-boolean v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t:Z

    if-eq v0, v2, :cond_c

    .line 549
    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v2, v0, Landroid/support/v7/widget/dc;->a:[I

    if-eqz v2, :cond_b

    iget-object v2, v0, Landroid/support/v7/widget/dc;->a:[I

    invoke-static {v2, v10}, Ljava/util/Arrays;->fill([II)V

    :cond_b
    iput-object v11, v0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    .line 550
    iput-boolean v3, v5, Landroid/support/v7/widget/db;->d:Z

    .line 554
    :cond_c
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-lez v0, :cond_32

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->c:I

    if-gtz v0, :cond_32

    .line 556
    :cond_d
    iget-boolean v0, v5, Landroid/support/v7/widget/db;->d:Z

    if-eqz v0, :cond_2c

    move v0, v1

    .line 557
    :goto_9
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v0, v2, :cond_32

    .line 559
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v2, v2, v0

    iget-object v4, v2, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iput v9, v2, Landroid/support/v7/widget/dh;->b:I

    iput v9, v2, Landroid/support/v7/widget/dh;->c:I

    iput v1, v2, Landroid/support/v7/widget/dh;->d:I

    .line 560
    iget v2, v5, Landroid/support/v7/widget/db;->b:I

    if-eq v2, v9, :cond_e

    .line 561
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v2, v2, v0

    iget v4, v5, Landroid/support/v7/widget/db;->b:I

    iput v4, v2, Landroid/support/v7/widget/dh;->b:I

    iput v4, v2, Landroid/support/v7/widget/dh;->c:I

    .line 557
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 538
    :cond_f
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    iput-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    goto/16 :goto_2

    .line 540
    :cond_10
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l()V

    .line 541
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    iput-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    goto/16 :goto_3

    .line 544
    :cond_11
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    if-ltz v0, :cond_12

    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_13

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v4, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v4

    :goto_a
    if-lt v2, v0, :cond_14

    :cond_12
    iput v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    iput v9, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    move v0, v1

    goto/16 :goto_4

    :cond_13
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_a

    :cond_14
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->a:I

    if-eq v0, v10, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    iget v0, v0, Landroid/support/v7/widget/df;->c:I

    if-gtz v0, :cond_23

    :cond_15
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1f

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_17

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_16

    move v0, v1

    :goto_b
    iput v0, v5, Landroid/support/v7/widget/db;->a:I

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    if-eq v0, v9, :cond_1a

    iget-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    if-eqz v0, :cond_19

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    iget v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    sub-int/2addr v0, v4

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4, v2}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, v5, Landroid/support/v7/widget/db;->b:I

    :goto_c
    move v0, v3

    goto/16 :goto_4

    :cond_16
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_b

    :cond_17
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_18

    move v0, v1

    goto :goto_b

    :cond_18
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_b

    :cond_19
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v0

    iget v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    add-int/2addr v0, v4

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4, v2}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, v5, Landroid/support/v7/widget/db;->b:I

    goto :goto_c

    :cond_1a
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v0

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->d()I

    move-result v4

    if-le v0, v4, :cond_1c

    iget-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    if-eqz v0, :cond_1b

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    :goto_d
    iput v0, v5, Landroid/support/v7/widget/db;->b:I

    move v0, v3

    goto/16 :goto_4

    :cond_1b
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v0

    goto :goto_d

    :cond_1c
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v0

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4}, Landroid/support/v7/widget/bc;->a()I

    move-result v4

    sub-int/2addr v0, v4

    if-gez v0, :cond_1d

    neg-int v0, v0

    iput v0, v5, Landroid/support/v7/widget/db;->b:I

    move v0, v3

    goto/16 :goto_4

    :cond_1d
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v4, v2}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    if-gez v0, :cond_1e

    iput v0, v5, Landroid/support/v7/widget/db;->b:I

    move v0, v3

    goto/16 :goto_4

    :cond_1e
    iput v9, v5, Landroid/support/v7/widget/db;->b:I

    :goto_e
    move v0, v3

    goto/16 :goto_4

    :cond_1f
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    iput v0, v5, Landroid/support/v7/widget/db;->a:I

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    if-ne v0, v9, :cond_21

    iget v0, v5, Landroid/support/v7/widget/db;->a:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(I)I

    move-result v0

    if-ne v0, v3, :cond_20

    move v0, v3

    :goto_f
    iput-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    invoke-virtual {v5}, Landroid/support/v7/widget/db;->a()V

    :goto_10
    iput-boolean v3, v5, Landroid/support/v7/widget/db;->d:Z

    goto :goto_e

    :cond_20
    move v0, v1

    goto :goto_f

    :cond_21
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    iget-boolean v2, v5, Landroid/support/v7/widget/db;->c:Z

    if-eqz v2, :cond_22

    iget-object v2, v5, Landroid/support/v7/widget/db;->e:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->b()I

    move-result v2

    sub-int v0, v2, v0

    iput v0, v5, Landroid/support/v7/widget/db;->b:I

    goto :goto_10

    :cond_22
    iget-object v2, v5, Landroid/support/v7/widget/db;->e:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v5, Landroid/support/v7/widget/db;->b:I

    goto :goto_10

    :cond_23
    iput v9, v5, Landroid/support/v7/widget/db;->b:I

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    iput v0, v5, Landroid/support/v7/widget/db;->a:I

    goto :goto_e

    :cond_24
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto/16 :goto_5

    :cond_25
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto/16 :goto_6

    :cond_26
    move v0, v1

    goto/16 :goto_7

    :cond_27
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_28

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v2, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v2

    :goto_11
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v6

    move v4, v1

    :goto_12
    if-ge v4, v6, :cond_2a

    invoke-virtual {p0, v4}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v2

    if-ltz v2, :cond_29

    if-ge v2, v0, :cond_29

    move v0, v2

    goto/16 :goto_7

    :cond_28
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_11

    :cond_29
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_12

    :cond_2a
    move v0, v1

    goto/16 :goto_7

    :cond_2b
    move v0, v1

    .line 547
    goto/16 :goto_8

    :cond_2c
    move v2, v1

    .line 565
    :goto_13
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v2, v0, :cond_32

    .line 566
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v4, v0, v2

    iget-boolean v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    iget v7, v5, Landroid/support/v7/widget/db;->b:I

    if-eqz v6, :cond_2f

    invoke-virtual {v4, v9}, Landroid/support/v7/widget/dh;->b(I)I

    move-result v0

    :goto_14
    iget-object v8, v4, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    iput v9, v4, Landroid/support/v7/widget/dh;->b:I

    iput v9, v4, Landroid/support/v7/widget/dh;->c:I

    iput v1, v4, Landroid/support/v7/widget/dh;->d:I

    if-eq v0, v9, :cond_2e

    if-eqz v6, :cond_2d

    iget-object v8, v4, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v8, v8, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v8}, Landroid/support/v7/widget/bc;->b()I

    move-result v8

    if-lt v0, v8, :cond_2e

    :cond_2d
    if-nez v6, :cond_30

    iget-object v6, v4, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v6, v6, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v6}, Landroid/support/v7/widget/bc;->a()I

    move-result v6

    if-le v0, v6, :cond_30

    .line 565
    :cond_2e
    :goto_15
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_13

    .line 566
    :cond_2f
    invoke-virtual {v4, v9}, Landroid/support/v7/widget/dh;->a(I)I

    move-result v0

    goto :goto_14

    :cond_30
    if-eq v7, v9, :cond_31

    add-int/2addr v0, v7

    :cond_31
    iput v0, v4, Landroid/support/v7/widget/dh;->c:I

    iput v0, v4, Landroid/support/v7/widget/dh;->b:I

    goto :goto_15

    .line 570
    :cond_32
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/bw;)V

    .line 571
    iput-boolean v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    .line 572
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->d()I

    move-result v0

    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    div-int/2addr v0, v2

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j:I

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->d()I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v:I

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    if-ne v0, v3, :cond_36

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->w:I

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->x:I

    .line 573
    :goto_16
    iget-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    if-eqz v0, :cond_37

    .line 575
    iget v0, v5, Landroid/support/v7/widget/db;->a:I

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(ILandroid/support/v7/widget/cc;)V

    .line 576
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/ah;Landroid/support/v7/widget/cc;)I

    .line 578
    iget v0, v5, Landroid/support/v7/widget/db;->a:I

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(ILandroid/support/v7/widget/cc;)V

    .line 579
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v2, v0, Landroid/support/v7/widget/ah;->b:I

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v4, v4, Landroid/support/v7/widget/ah;->c:I

    add-int/2addr v2, v4

    iput v2, v0, Landroid/support/v7/widget/ah;->b:I

    .line 580
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/ah;Landroid/support/v7/widget/cc;)I

    .line 591
    :goto_17
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-lez v0, :cond_33

    .line 592
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_38

    .line 593
    invoke-direct {p0, p1, p2, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)V

    .line 594
    invoke-direct {p0, p1, p2, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)V

    .line 601
    :cond_33
    :goto_18
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_35

    .line 602
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-lez v0, :cond_34

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    if-eq v0, v10, :cond_34

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    if-eqz v0, :cond_34

    .line 604
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->B:Ljava/lang/Runnable;

    invoke-static {v0, v2}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 606
    :cond_34
    iput v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    .line 607
    iput v9, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:I

    .line 609
    :cond_35
    iget-boolean v0, v5, Landroid/support/v7/widget/db;->c:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->s:Z

    .line 610
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v0

    if-ne v0, v3, :cond_39

    :goto_19
    iput-boolean v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t:Z

    .line 611
    iput-object v11, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/df;

    .line 612
    return-void

    .line 572
    :cond_36
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->x:I

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->w:I

    goto :goto_16

    .line 583
    :cond_37
    iget v0, v5, Landroid/support/v7/widget/db;->a:I

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(ILandroid/support/v7/widget/cc;)V

    .line 584
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/ah;Landroid/support/v7/widget/cc;)I

    .line 586
    iget v0, v5, Landroid/support/v7/widget/db;->a:I

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(ILandroid/support/v7/widget/cc;)V

    .line 587
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v2, v0, Landroid/support/v7/widget/ah;->b:I

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    iget v4, v4, Landroid/support/v7/widget/ah;->c:I

    add-int/2addr v2, v4

    iput v2, v0, Landroid/support/v7/widget/ah;->b:I

    .line 588
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/ah;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/ah;Landroid/support/v7/widget/cc;)I

    goto :goto_17

    .line 596
    :cond_38
    invoke-direct {p0, p1, p2, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)V

    .line 597
    invoke-direct {p0, p1, p2, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)V

    goto :goto_18

    :cond_39
    move v3, v1

    .line 610
    goto :goto_19
.end method

.method public final d(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 928
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 1268
    const/4 v0, 0x3

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(III)V

    .line 1269
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1672
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 944
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1667
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 949
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final f(I)V
    .locals 2

    .prologue
    .line 1236
    invoke-super {p0, p1}, Landroid/support/v7/widget/bs;->f(I)V

    .line 1237
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v0, v1, :cond_0

    .line 1238
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/dh;->c(I)V

    .line 1237
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1240
    :cond_0
    return-void
.end method

.method public final g(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 965
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final g(I)V
    .locals 2

    .prologue
    .line 1244
    invoke-super {p0, p1}, Landroid/support/v7/widget/bs;->g(I)V

    .line 1245
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    if-ge v0, v1, :cond_0

    .line 1246
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:[Landroid/support/v7/widget/dh;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/dh;->c(I)V

    .line 1245
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1248
    :cond_0
    return-void
.end method

.method public final h(I)V
    .locals 0

    .prologue
    .line 262
    if-nez p1, :cond_0

    .line 263
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()V

    .line 265
    :cond_0
    return-void
.end method

.method j()V
    .locals 13

    .prologue
    .line 217
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->r:I

    if-nez v0, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_5

    .line 222
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 223
    :goto_1
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x0

    :goto_2
    move v7, v1

    move v8, v0

    .line 228
    :goto_3
    if-nez v8, :cond_1a

    .line 229
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    new-instance v9, Ljava/util/BitSet;

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    invoke-direct {v9, v0}, Ljava/util/BitSet;-><init>(I)V

    const/4 v0, 0x0

    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:I

    const/4 v3, 0x1

    invoke-virtual {v9, v0, v2, v3}, Ljava/util/BitSet;->set(IIZ)V

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_8

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_9

    const/4 v0, 0x1

    move v2, v0

    :goto_5
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_a

    add-int/lit8 v1, v1, -0x1

    const/4 v0, -0x1

    move v6, v0

    :goto_6
    if-ge v1, v6, :cond_b

    const/4 v0, 0x1

    move v3, v0

    :goto_7
    move v5, v1

    :goto_8
    if-eq v5, v6, :cond_19

    invoke-virtual {p0, v5}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    iget-object v0, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget v0, v0, Landroid/support/v7/widget/dh;->e:I

    invoke-virtual {v9, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget-boolean v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v10, :cond_d

    iget v10, v0, Landroid/support/v7/widget/dh;->c:I

    const/high16 v11, -0x80000000

    if-eq v10, v11, :cond_c

    iget v0, v0, Landroid/support/v7/widget/dh;->c:I

    :goto_9
    iget-object v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v10}, Landroid/support/v7/widget/bc;->b()I

    move-result v10

    if-ge v0, v10, :cond_f

    const/4 v0, 0x1

    :goto_a
    if-eqz v0, :cond_10

    move-object v0, v4

    .line 230
    :goto_b
    if-eqz v0, :cond_1a

    .line 231
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget-object v1, v0, Landroid/support/v7/widget/dc;->a:[I

    if-eqz v1, :cond_2

    iget-object v1, v0, Landroid/support/v7/widget/dc;->a:[I

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    :cond_2
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/bs;->p:Z

    .line 233
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto/16 :goto_0

    .line 222
    :cond_3
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto/16 :goto_1

    .line 223
    :cond_4
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v1

    goto/16 :goto_2

    .line 225
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    .line 226
    :goto_c
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f()I

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x0

    :goto_d
    move v7, v1

    move v8, v0

    goto/16 :goto_3

    .line 225
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_c

    .line 226
    :cond_7
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;)I

    move-result v1

    goto :goto_d

    .line 229
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v0, -0x1

    move v2, v0

    goto/16 :goto_5

    :cond_a
    const/4 v0, 0x0

    move v6, v1

    move v1, v0

    goto/16 :goto_6

    :cond_b
    const/4 v0, -0x1

    move v3, v0

    goto/16 :goto_7

    :cond_c
    invoke-virtual {v0}, Landroid/support/v7/widget/dh;->b()V

    iget v0, v0, Landroid/support/v7/widget/dh;->c:I

    goto :goto_9

    :cond_d
    iget v10, v0, Landroid/support/v7/widget/dh;->b:I

    const/high16 v11, -0x80000000

    if-eq v10, v11, :cond_e

    iget v0, v0, Landroid/support/v7/widget/dh;->b:I

    :goto_e
    iget-object v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v10}, Landroid/support/v7/widget/bc;->a()I

    move-result v10

    if-le v0, v10, :cond_f

    const/4 v0, 0x1

    goto/16 :goto_a

    :cond_e
    invoke-virtual {v0}, Landroid/support/v7/widget/dh;->a()V

    iget v0, v0, Landroid/support/v7/widget/dh;->b:I

    goto :goto_e

    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_a

    :cond_10
    iget-object v0, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget v0, v0, Landroid/support/v7/widget/dh;->e:I

    invoke-virtual {v9, v0}, Ljava/util/BitSet;->clear(I)V

    :cond_11
    add-int v0, v5, v3

    if-eq v0, v6, :cond_18

    add-int v0, v5, v3

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e(I)Landroid/view/View;

    move-result-object v10

    const/4 v0, 0x0

    iget-boolean v11, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v11, :cond_14

    iget-object v11, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v11, v4}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v11

    iget-object v12, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v12, v10}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v12

    if-ge v11, v12, :cond_12

    move-object v0, v4

    goto/16 :goto_b

    :cond_12
    if-ne v11, v12, :cond_13

    const/4 v0, 0x1

    :cond_13
    :goto_f
    if-eqz v0, :cond_18

    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget v1, v1, Landroid/support/v7/widget/dh;->e:I

    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    iget v0, v0, Landroid/support/v7/widget/dh;->e:I

    sub-int v0, v1, v0

    if-gez v0, :cond_16

    const/4 v0, 0x1

    move v1, v0

    :goto_10
    if-gez v2, :cond_17

    const/4 v0, 0x1

    :goto_11
    if-eq v1, v0, :cond_18

    move-object v0, v4

    goto/16 :goto_b

    :cond_14
    iget-object v11, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v11, v4}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v11

    iget-object v12, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v12, v10}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v12

    if-le v11, v12, :cond_15

    move-object v0, v4

    goto/16 :goto_b

    :cond_15
    if-ne v11, v12, :cond_13

    const/4 v0, 0x1

    goto :goto_f

    :cond_16
    const/4 v0, 0x0

    move v1, v0

    goto :goto_10

    :cond_17
    const/4 v0, 0x0

    goto :goto_11

    :cond_18
    add-int v0, v5, v3

    move v5, v0

    goto/16 :goto_8

    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_b

    .line 237
    :cond_1a
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    if-eqz v0, :cond_0

    .line 240
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:Z

    if-eqz v0, :cond_1b

    const/4 v0, -0x1

    .line 241
    :goto_12
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    add-int/lit8 v2, v7, 0x1

    invoke-virtual {v1, v8, v2, v0}, Landroid/support/v7/widget/dc;->a(III)Landroid/support/v7/widget/dd;

    move-result-object v1

    .line 243
    if-nez v1, :cond_1c

    .line 244
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    .line 245
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/dc;->a(I)I

    goto/16 :goto_0

    .line 240
    :cond_1b
    const/4 v0, 0x1

    goto :goto_12

    .line 248
    :cond_1c
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget v3, v1, Landroid/support/v7/widget/dd;->a:I

    neg-int v0, v0

    invoke-virtual {v2, v8, v3, v0}, Landroid/support/v7/widget/dc;->a(III)Landroid/support/v7/widget/dd;

    move-result-object v0

    .line 251
    if-nez v0, :cond_1d

    .line 252
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget v1, v1, Landroid/support/v7/widget/dd;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/dc;->a(I)I

    .line 256
    :goto_13
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/bs;->p:Z

    .line 257
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto/16 :goto_0

    .line 254
    :cond_1d
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:Landroid/support/v7/widget/dc;

    iget v0, v0, Landroid/support/v7/widget/dd;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/dc;->a(I)I

    goto :goto_13
.end method
