.class Landroid/support/v7/widget/dc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:[I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/widget/dd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 2206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434
    return-void
.end method


# virtual methods
.method final a(I)I
    .locals 2

    .prologue
    .line 2217
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2218
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 2219
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dd;

    .line 2220
    iget v0, v0, Landroid/support/v7/widget/dd;->a:I

    if-lt v0, p1, :cond_0

    .line 2221
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2218
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2225
    :cond_1
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/dc;->b(I)I

    move-result v0

    return v0
.end method

.method public final a(III)Landroid/support/v7/widget/dd;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2416
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    if-nez v0, :cond_1

    move-object v0, v2

    .line 2428
    :cond_0
    :goto_0
    return-object v0

    .line 2419
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2420
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dd;

    .line 2421
    iget v3, v0, Landroid/support/v7/widget/dd;->a:I

    if-lt v3, p2, :cond_2

    move-object v0, v2

    .line 2422
    goto :goto_0

    .line 2424
    :cond_2
    iget v3, v0, Landroid/support/v7/widget/dd;->a:I

    if-lt v3, p1, :cond_3

    if-eqz p3, :cond_0

    iget v3, v0, Landroid/support/v7/widget/dd;->b:I

    if-eq v3, p3, :cond_0

    .line 2419
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 2428
    goto :goto_0
.end method

.method final a(II)V
    .locals 4

    .prologue
    .line 2290
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 2299
    :cond_0
    return-void

    .line 2293
    :cond_1
    add-int v0, p1, p2

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/dc;->c(I)V

    .line 2294
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    add-int v1, p1, p2

    iget-object v2, p0, Landroid/support/v7/widget/dc;->a:[I

    iget-object v3, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v3, v3

    sub-int/2addr v3, p1

    sub-int/2addr v3, p2

    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2296
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    iget-object v1, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v1, v1

    sub-int/2addr v1, p2

    iget-object v2, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v2, v2

    const/4 v3, -0x1

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->fill([IIII)V

    .line 2298
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    add-int v2, p1, p2

    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dd;

    iget v3, v0, Landroid/support/v7/widget/dd;->a:I

    if-lt v3, p1, :cond_2

    iget v3, v0, Landroid/support/v7/widget/dd;->a:I

    if-ge v3, v2, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget v3, v0, Landroid/support/v7/widget/dd;->a:I

    sub-int/2addr v3, p2

    iput v3, v0, Landroid/support/v7/widget/dd;->a:I

    goto :goto_1
.end method

.method final b(I)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2232
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    if-nez v0, :cond_0

    move v0, v1

    .line 2245
    :goto_0
    return v0

    .line 2235
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    move v0, v1

    .line 2236
    goto :goto_0

    .line 2238
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    if-nez v0, :cond_2

    move v0, v1

    .line 2239
    :goto_1
    if-ne v0, v1, :cond_9

    .line 2240
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    iget-object v2, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v2, v2

    invoke-static {v0, p1, v2, v1}, Ljava/util/Arrays;->fill([IIII)V

    .line 2241
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v0, v0

    goto :goto_0

    .line 2238
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    if-nez v0, :cond_5

    move-object v0, v2

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_a

    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dd;

    iget v0, v0, Landroid/support/v7/widget/dd;->a:I

    if-lt v0, p1, :cond_7

    :goto_4
    if-eq v2, v1, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dd;

    iget-object v3, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget v0, v0, Landroid/support/v7/widget/dd;->a:I

    goto :goto_1

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_5
    if-ltz v3, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dd;

    iget v4, v0, Landroid/support/v7/widget/dd;->a:I

    if-eq v4, p1, :cond_3

    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_5

    :cond_6
    move-object v0, v2

    goto :goto_2

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_1

    .line 2244
    :cond_9
    iget-object v2, p0, Landroid/support/v7/widget/dc;->a:[I

    add-int/lit8 v3, v0, 0x1

    invoke-static {v2, p1, v3, v1}, Ljava/util/Arrays;->fill([IIII)V

    .line 2245
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_a
    move v2, v1

    goto :goto_4
.end method

.method final b(II)V
    .locals 4

    .prologue
    .line 2320
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 2329
    :cond_0
    return-void

    .line 2323
    :cond_1
    add-int v0, p1, p2

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/dc;->c(I)V

    .line 2324
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    iget-object v1, p0, Landroid/support/v7/widget/dc;->a:[I

    add-int v2, p1, p2

    iget-object v3, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v3, v3

    sub-int/2addr v3, p1

    sub-int/2addr v3, p2

    invoke-static {v0, p1, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2326
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    add-int v1, p1, p2

    const/4 v2, -0x1

    invoke-static {v0, p1, v1, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 2328
    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/dc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dd;

    iget v2, v0, Landroid/support/v7/widget/dd;->a:I

    if-lt v2, p1, :cond_2

    iget v2, v0, Landroid/support/v7/widget/dd;->a:I

    add-int/2addr v2, p2

    iput v2, v0, Landroid/support/v7/widget/dd;->a:I

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method final c(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 2271
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    if-nez v0, :cond_1

    .line 2272
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    .line 2273
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([II)V

    .line 2280
    :cond_0
    :goto_0
    return-void

    .line 2274
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_0

    .line 2275
    iget-object v1, p0, Landroid/support/v7/widget/dc;->a:[I

    .line 2276
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v0, v0

    :goto_1
    if-gt v0, p1, :cond_2

    shl-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    .line 2277
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v2, v1

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2278
    iget-object v0, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v1, v1

    iget-object v2, p0, Landroid/support/v7/widget/dc;->a:[I

    array-length v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->fill([IIII)V

    goto :goto_0
.end method
