.class Landroid/support/v7/widget/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Landroid/support/v7/widget/u;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/u;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/u;

    iput-object p2, p0, Landroid/support/v7/widget/x;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 165
    iget-object v0, p0, Landroid/support/v7/widget/x;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 166
    iget-object v3, p0, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/u;

    iget-object v1, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    iget-object v4, v3, Landroid/support/v7/widget/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    iget-object v1, v4, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_1

    sget-object v6, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v6, v4, v1, v5}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_1
    iget-wide v6, v3, Landroid/support/v7/widget/bn;->i:J

    iget-object v1, v4, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_2

    sget-object v5, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v5, v1, v6, v7}, Landroid/support/v4/view/cr;->a(Landroid/view/View;J)V

    :cond_2
    new-instance v1, Landroid/support/v7/widget/z;

    invoke-direct {v1, v3, v0, v4}, Landroid/support/v7/widget/z;-><init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ce;Landroid/support/v4/view/cj;)V

    iget-object v0, v4, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_3

    sget-object v3, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v3, v4, v0, v1}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    :cond_3
    iget-object v0, v4, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v1, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v1, v4, v0}, Landroid/support/v4/view/cr;->b(Landroid/support/v4/view/cj;Landroid/view/View;)V

    goto :goto_0

    .line 168
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/x;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 169
    iget-object v0, p0, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/u;

    iget-object v0, v0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/x;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 170
    return-void
.end method
