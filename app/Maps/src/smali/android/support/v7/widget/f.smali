.class Landroid/support/v7/widget/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/internal/view/menu/k;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/ActionMenuPresenter;


# virtual methods
.method public final a(Landroid/support/v7/internal/view/menu/e;Z)V
    .locals 2

    .prologue
    .line 732
    instance-of v0, p1, Landroid/support/v7/internal/view/menu/n;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 733
    check-cast v0, Landroid/support/v7/internal/view/menu/n;

    iget-object v0, v0, Landroid/support/v7/internal/view/menu/n;->i:Landroid/support/v7/internal/view/menu/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/e;->a(Z)V

    .line 735
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/f;->a:Landroid/support/v7/widget/ActionMenuPresenter;

    const/4 v0, 0x0

    .line 736
    if-eqz v0, :cond_1

    .line 737
    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/k;->a(Landroid/support/v7/internal/view/menu/e;Z)V

    .line 739
    :cond_1
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/e;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 723
    if-nez p1, :cond_0

    move v0, v1

    .line 727
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 725
    check-cast v0, Landroid/support/v7/internal/view/menu/n;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/n;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    .line 726
    iget-object v0, p0, Landroid/support/v7/widget/f;->a:Landroid/support/v7/widget/ActionMenuPresenter;

    const/4 v0, 0x0

    .line 727
    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Landroid/support/v7/internal/view/menu/k;->a(Landroid/support/v7/internal/view/menu/e;)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
