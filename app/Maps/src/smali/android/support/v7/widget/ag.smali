.class public abstract Landroid/support/v7/widget/ag;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/util/SparseIntArray;


# virtual methods
.method public abstract a()I
.end method

.method final a(II)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 614
    invoke-virtual {p0}, Landroid/support/v7/widget/ag;->a()I

    move-result v4

    if-ne v4, p2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v3, v1

    move v2, v1

    :goto_1
    if-ge v3, p1, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/widget/ag;->a()I

    move-result v0

    add-int/2addr v2, v0

    if-ne v2, p2, :cond_3

    move v0, v1

    :cond_2
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    :cond_3
    if-gt v2, p2, :cond_2

    move v0, v2

    goto :goto_2

    :cond_4
    add-int v0, v2, v4

    if-gt v0, p2, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public final b(II)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 711
    .line 713
    invoke-virtual {p0}, Landroid/support/v7/widget/ag;->a()I

    move-result v5

    move v4, v2

    move v0, v2

    move v3, v2

    .line 714
    :goto_0
    if-ge v4, p1, :cond_1

    .line 715
    invoke-virtual {p0}, Landroid/support/v7/widget/ag;->a()I

    move-result v1

    .line 716
    add-int/2addr v3, v1

    .line 717
    if-ne v3, p2, :cond_0

    .line 719
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .line 714
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    goto :goto_0

    .line 720
    :cond_0
    if-le v3, p2, :cond_3

    .line 723
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 726
    :cond_1
    add-int v1, v3, v5

    if-le v1, p2, :cond_2

    .line 727
    add-int/lit8 v0, v0, 0x1

    .line 729
    :cond_2
    return v0

    :cond_3
    move v1, v3

    goto :goto_1
.end method
