.class public abstract Landroid/support/v7/widget/bk;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "Landroid/support/v7/widget/ce;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Landroid/support/v7/widget/bl;

.field b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4069
    new-instance v0, Landroid/support/v7/widget/bl;

    invoke-direct {v0}, Landroid/support/v7/widget/bl;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bk;->a:Landroid/support/v7/widget/bl;

    .line 4070
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/bk;->b:Z

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a(I)I
    .locals 1

    .prologue
    .line 4156
    const/4 v0, 0x0

    return v0
.end method

.method public abstract a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/ce;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)TVH;"
        }
    .end annotation
.end method

.method public a(Landroid/support/v7/widget/ce;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;)V"
        }
    .end annotation

    .prologue
    .line 4231
    return-void
.end method

.method public abstract a(Landroid/support/v7/widget/ce;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I)V"
        }
    .end annotation
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 4168
    iget-object v0, p0, Landroid/support/v7/widget/bk;->a:Landroid/support/v7/widget/bl;

    invoke-virtual {v0}, Landroid/support/v7/widget/bl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4169
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot change whether this adapter has stable IDs while the adapter has registered observers."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4172
    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/bk;->b:Z

    .line 4173
    return-void
.end method

.method public b(I)J
    .locals 2

    .prologue
    .line 4184
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 4338
    iget-object v0, p0, Landroid/support/v7/widget/bk;->a:Landroid/support/v7/widget/bl;

    invoke-virtual {v0}, Landroid/support/v7/widget/bl;->b()V

    .line 4339
    return-void
.end method

.method public final b(Landroid/support/v7/widget/ce;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I)V"
        }
    .end annotation

    .prologue
    .line 4134
    iput p2, p1, Landroid/support/v7/widget/ce;->b:I

    .line 4135
    iget-boolean v0, p0, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v0, :cond_0

    .line 4136
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/bk;->b(I)J

    move-result-wide v0

    iput-wide v0, p1, Landroid/support/v7/widget/ce;->d:J

    .line 4138
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/bk;->a(Landroid/support/v7/widget/ce;I)V

    .line 4139
    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, -0x8

    or-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/support/v7/widget/ce;->i:I

    .line 4141
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 4353
    iget-object v0, p0, Landroid/support/v7/widget/bk;->a:Landroid/support/v7/widget/bl;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/bl;->a(II)V

    .line 4354
    return-void
.end method
