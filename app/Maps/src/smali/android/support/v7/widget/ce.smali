.class public abstract Landroid/support/v7/widget/ce;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/view/View;

.field b:I

.field c:I

.field d:J

.field e:I

.field f:I

.field g:Landroid/support/v7/widget/ce;

.field h:Landroid/support/v7/widget/ce;

.field i:I

.field j:Landroid/support/v7/widget/bw;

.field private k:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 6790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6713
    iput v2, p0, Landroid/support/v7/widget/ce;->b:I

    .line 6714
    iput v2, p0, Landroid/support/v7/widget/ce;->c:I

    .line 6715
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v7/widget/ce;->d:J

    .line 6716
    iput v2, p0, Landroid/support/v7/widget/ce;->e:I

    .line 6717
    iput v2, p0, Landroid/support/v7/widget/ce;->f:I

    .line 6720
    iput-object v3, p0, Landroid/support/v7/widget/ce;->g:Landroid/support/v7/widget/ce;

    .line 6722
    iput-object v3, p0, Landroid/support/v7/widget/ce;->h:Landroid/support/v7/widget/ce;

    .line 6784
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ce;->k:I

    .line 6788
    iput-object v3, p0, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    .line 6791
    if-nez p1, :cond_0

    .line 6792
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "itemView may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6794
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 6795
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 6867
    iget v0, p0, Landroid/support/v7/widget/ce;->e:I

    return v0
.end method

.method final a(IZ)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 6804
    iget v0, p0, Landroid/support/v7/widget/ce;->c:I

    if-ne v0, v1, :cond_0

    .line 6805
    iget v0, p0, Landroid/support/v7/widget/ce;->b:I

    iput v0, p0, Landroid/support/v7/widget/ce;->c:I

    .line 6807
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/ce;->f:I

    if-ne v0, v1, :cond_1

    .line 6808
    iget v0, p0, Landroid/support/v7/widget/ce;->b:I

    iput v0, p0, Landroid/support/v7/widget/ce;->f:I

    .line 6810
    :cond_1
    if-eqz p2, :cond_2

    .line 6811
    iget v0, p0, Landroid/support/v7/widget/ce;->f:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/ce;->f:I

    .line 6813
    :cond_2
    iget v0, p0, Landroid/support/v7/widget/ce;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/ce;->b:I

    .line 6814
    iget-object v0, p0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 6815
    iget-object v0, p0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->e:Z

    .line 6817
    :cond_3
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 6972
    if-eqz p1, :cond_1

    iget v0, p0, Landroid/support/v7/widget/ce;->k:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iput v0, p0, Landroid/support/v7/widget/ce;->k:I

    .line 6973
    iget v0, p0, Landroid/support/v7/widget/ce;->k:I

    if-gez v0, :cond_2

    .line 6974
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ce;->k:I

    .line 6979
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6989
    :cond_0
    :goto_1
    return-void

    .line 6972
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/ce;->k:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6981
    :cond_2
    if-nez p1, :cond_3

    iget v0, p0, Landroid/support/v7/widget/ce;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 6982
    iget v0, p0, Landroid/support/v7/widget/ce;->i:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Landroid/support/v7/widget/ce;->i:I

    goto :goto_1

    .line 6983
    :cond_3
    if-eqz p1, :cond_0

    iget v0, p0, Landroid/support/v7/widget/ce;->k:I

    if-nez v0, :cond_0

    .line 6984
    iget v0, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Landroid/support/v7/widget/ce;->i:I

    goto :goto_1
.end method

.method final b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 6931
    iput v3, p0, Landroid/support/v7/widget/ce;->i:I

    .line 6932
    iput v2, p0, Landroid/support/v7/widget/ce;->b:I

    .line 6933
    iput v2, p0, Landroid/support/v7/widget/ce;->c:I

    .line 6934
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v7/widget/ce;->d:J

    .line 6935
    iput v2, p0, Landroid/support/v7/widget/ce;->f:I

    .line 6936
    iput v3, p0, Landroid/support/v7/widget/ce;->k:I

    .line 6937
    iput-object v4, p0, Landroid/support/v7/widget/ce;->g:Landroid/support/v7/widget/ce;

    .line 6938
    iput-object v4, p0, Landroid/support/v7/widget/ce;->h:Landroid/support/v7/widget/ce;

    .line 6939
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6943
    new-instance v3, Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "ViewHolder{"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " position="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Landroid/support/v7/widget/ce;->b:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Landroid/support/v7/widget/ce;->d:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", oldPos="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Landroid/support/v7/widget/ce;->c:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", pLpos:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Landroid/support/v7/widget/ce;->f:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 6946
    iget-object v2, p0, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    if-eqz v2, :cond_a

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    const-string v2, " scrap"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6947
    :cond_0
    iget v2, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    const-string v2, " invalid"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6948
    :cond_1
    iget v2, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_c

    move v2, v0

    :goto_2
    if-nez v2, :cond_2

    const-string v2, " unbound"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6949
    :cond_2
    iget v2, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    const-string v2, " update"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6950
    :cond_3
    iget v2, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    const-string v2, " removed"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6951
    :cond_4
    iget v2, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    const-string v2, " ignored"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6952
    :cond_5
    iget v2, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    const-string v2, " changed"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6953
    :cond_6
    iget v2, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    const-string v2, " tmpDetached"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6954
    :cond_7
    iget v2, p0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_12

    iget-object v2, p0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/at;->c(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_12

    :goto_8
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " not recyclable("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v7/widget/ce;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6955
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_9

    const-string v0, " no parent"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6956
    :cond_9
    const-string v0, "}"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6957
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_a
    move v2, v1

    .line 6946
    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 6947
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 6948
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 6949
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 6950
    goto :goto_4

    :cond_f
    move v2, v1

    .line 6951
    goto :goto_5

    :cond_10
    move v2, v1

    .line 6952
    goto :goto_6

    :cond_11
    move v2, v1

    .line 6953
    goto :goto_7

    :cond_12
    move v0, v1

    .line 6954
    goto :goto_8
.end method
