.class Landroid/support/v7/widget/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Landroid/support/v7/widget/u;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/u;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/u;

    iput-object p2, p0, Landroid/support/v7/widget/w;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x0

    .line 143
    iget-object v0, p0, Landroid/support/v7/widget/w;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ad;

    .line 144
    iget-object v5, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/u;

    iget-object v1, v0, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    if-nez v1, :cond_c

    move-object v1, v2

    :goto_1
    iget-object v3, v0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    if-eqz v3, :cond_d

    iget-object v3, v3, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    :goto_2
    if-eqz v1, :cond_6

    iget-object v6, v5, Landroid/support/v7/widget/u;->g:Ljava/util/ArrayList;

    iget-object v7, v0, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v6

    iget-wide v8, v5, Landroid/support/v7/widget/bn;->l:J

    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_1

    sget-object v7, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v7, v1, v8, v9}, Landroid/support/v4/view/cr;->a(Landroid/view/View;J)V

    :cond_1
    iget v1, v0, Landroid/support/v7/widget/ad;->e:I

    iget v7, v0, Landroid/support/v7/widget/ad;->c:I

    sub-int/2addr v1, v7

    int-to-float v7, v1

    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_2

    sget-object v8, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v8, v6, v1, v7}, Landroid/support/v4/view/cr;->b(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_2
    iget v1, v0, Landroid/support/v7/widget/ad;->f:I

    iget v7, v0, Landroid/support/v7/widget/ad;->d:I

    sub-int/2addr v1, v7

    int-to-float v7, v1

    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_3

    sget-object v8, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v8, v6, v1, v7}, Landroid/support/v4/view/cr;->c(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_3
    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_4

    sget-object v7, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v7, v6, v1, v10}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_4
    new-instance v7, Landroid/support/v7/widget/ab;

    invoke-direct {v7, v5, v0, v6}, Landroid/support/v7/widget/ab;-><init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ad;Landroid/support/v4/view/cj;)V

    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_5

    sget-object v8, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v8, v6, v1, v7}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    :cond_5
    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_6

    sget-object v7, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v7, v6, v1}, Landroid/support/v4/view/cr;->b(Landroid/support/v4/view/cj;Landroid/view/View;)V

    :cond_6
    if-eqz v3, :cond_0

    iget-object v1, v5, Landroid/support/v7/widget/u;->g:Ljava/util/ArrayList;

    iget-object v6, v0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v6

    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_7

    sget-object v7, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v7, v6, v1, v10}, Landroid/support/v4/view/cr;->b(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_7
    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_8

    sget-object v7, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v7, v6, v1, v10}, Landroid/support/v4/view/cr;->c(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_8
    iget-wide v8, v5, Landroid/support/v7/widget/bn;->l:J

    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_9

    sget-object v7, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v7, v1, v8, v9}, Landroid/support/v4/view/cr;->a(Landroid/view/View;J)V

    :cond_9
    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v1, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_a

    sget-object v8, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v8, v6, v1, v7}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_a
    new-instance v1, Landroid/support/v7/widget/ac;

    invoke-direct {v1, v5, v0, v6, v3}, Landroid/support/v7/widget/ac;-><init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ad;Landroid/support/v4/view/cj;Landroid/view/View;)V

    iget-object v0, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_b

    sget-object v3, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v3, v6, v0, v1}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    :cond_b
    iget-object v0, v6, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v1, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v1, v6, v0}, Landroid/support/v4/view/cr;->b(Landroid/support/v4/view/cj;Landroid/view/View;)V

    goto/16 :goto_0

    :cond_c
    iget-object v1, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    goto/16 :goto_1

    :cond_d
    move-object v3, v2

    goto/16 :goto_2

    .line 146
    :cond_e
    iget-object v0, p0, Landroid/support/v7/widget/w;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 147
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/u;

    iget-object v0, v0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/w;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 148
    return-void
.end method
