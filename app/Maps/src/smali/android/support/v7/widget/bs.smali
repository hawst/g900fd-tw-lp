.class public abstract Landroid/support/v7/widget/bs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field m:Landroid/support/v7/widget/r;

.field public n:Landroid/support/v7/widget/RecyclerView;

.field o:Landroid/support/v7/widget/ca;

.field p:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4488
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/bs;->p:Z

    return-void
.end method

.method public static a(IIIZ)I
    .locals 4

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 5571
    sub-int v2, p0, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 5575
    if-eqz p3, :cond_2

    .line 5576
    if-ltz p2, :cond_1

    .line 5597
    :cond_0
    :goto_0
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0

    :cond_1
    move v0, v1

    move p2, v1

    .line 5583
    goto :goto_0

    .line 5586
    :cond_2
    if-gez p2, :cond_0

    .line 5589
    const/4 v3, -0x1

    if-ne p2, v3, :cond_3

    move p2, v2

    .line 5591
    goto :goto_0

    .line 5592
    :cond_3
    const/4 v0, -0x2

    if-ne p2, v0, :cond_4

    .line 5594
    const/high16 v0, -0x80000000

    move p2, v2

    goto :goto_0

    :cond_4
    move v0, v1

    move p2, v1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 5656
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    .line 5657
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    sub-int v3, p3, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, p4, v0

    invoke-virtual {p0, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 5659
    return-void
.end method

.method private a(Landroid/view/View;IZ)V
    .locals 10

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 4917
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v5

    .line 4918
    if-nez p3, :cond_0

    iget v0, v5, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    move v0, v2

    :goto_0
    if-eqz v0, :cond_6

    .line 4920
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4929
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 4930
    iget v1, v5, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_7

    move v1, v2

    :goto_2
    if-nez v1, :cond_2

    iget-object v1, v5, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    if-eqz v1, :cond_8

    move v1, v2

    :goto_3
    if-eqz v1, :cond_b

    .line 4931
    :cond_2
    iget-object v1, v5, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    if-eqz v1, :cond_9

    :goto_4
    if-eqz v2, :cond_a

    .line 4932
    iget-object v1, v5, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    invoke-virtual {v1, v5}, Landroid/support/v7/widget/bw;->b(Landroid/support/v7/widget/ce;)V

    .line 4936
    :goto_5
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2, v3}, Landroid/support/v7/widget/r;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    .line 4961
    :cond_3
    :goto_6
    iget-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->f:Z

    if-eqz v1, :cond_4

    .line 4965
    iget-object v1, v5, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 4966
    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->f:Z

    .line 4968
    :cond_4
    return-void

    :cond_5
    move v0, v3

    .line 4918
    goto :goto_0

    .line 4927
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    move v1, v3

    .line 4930
    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3

    :cond_9
    move v2, v3

    .line 4931
    goto :goto_4

    .line 4934
    :cond_a
    iget v1, v5, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, -0x21

    iput v1, v5, Landroid/support/v7/widget/ce;->i:I

    goto :goto_5

    .line 4940
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v6, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-ne v1, v6, :cond_15

    .line 4942
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v6, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v6, p1}, Landroid/support/v7/widget/t;->a(Landroid/view/View;)I

    move-result v6

    if-ne v6, v4, :cond_d

    move v1, v4

    .line 4943
    :goto_7
    if-ne p2, v4, :cond_c

    .line 4944
    iget-object v6, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v7, v6, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v7}, Landroid/support/v7/widget/t;->a()I

    move-result v7

    iget-object v6, v6, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    sub-int p2, v7, v6

    .line 4946
    :cond_c
    if-ne v1, v4, :cond_f

    .line 4947
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Added View has RecyclerView as parent but view is not a real child. Unfiltered index:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4942
    :cond_d
    iget-object v7, v1, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v7, v6}, Landroid/support/v7/widget/s;->b(I)Z

    move-result v7

    if-eqz v7, :cond_e

    move v1, v4

    goto :goto_7

    :cond_e
    iget-object v1, v1, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/s;->d(I)I

    move-result v1

    sub-int v1, v6, v1

    goto :goto_7

    .line 4951
    :cond_f
    if-eq v1, p2, :cond_3

    .line 4952
    iget-object v4, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v4, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v6, v1}, Landroid/support/v7/widget/bs;->e(I)Landroid/view/View;

    move-result-object v7

    if-nez v7, :cond_10

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot move a child from non-existing index:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    invoke-direct {v6, v1}, Landroid/support/v7/widget/bs;->b(I)V

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-static {v7}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v8

    iget v4, v8, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_12

    move v4, v2

    :goto_8
    if-eqz v4, :cond_13

    iget-object v4, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v9, v4, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_11

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_11
    :goto_9
    iget-object v4, v6, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget v6, v8, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v6, v6, 0x8

    if-eqz v6, :cond_14

    :goto_a
    invoke-virtual {v4, v7, p2, v1, v2}, Landroid/support/v7/widget/r;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    goto/16 :goto_6

    :cond_12
    move v4, v3

    goto :goto_8

    :cond_13
    iget-object v4, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_14
    move v2, v3

    goto :goto_a

    .line 4955
    :cond_15
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, p1, p2, v3}, Landroid/support/v7/widget/r;->a(Landroid/view/View;IZ)V

    .line 4956
    iput-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->e:Z

    .line 4957
    iget-object v1, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    iget-boolean v1, v1, Landroid/support/v7/widget/ca;->k:Z

    if-eqz v1, :cond_3

    .line 4958
    iget-object v1, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    iget-object v2, v1, Landroid/support/v7/widget/ca;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/view/View;)I

    move-result v2

    iget v4, v1, Landroid/support/v7/widget/ca;->g:I

    if-ne v2, v4, :cond_3

    iput-object p1, v1, Landroid/support/v7/widget/ca;->l:Landroid/view/View;

    goto/16 :goto_6
.end method

.method public static b(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 5017
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 5098
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/r;->a(I)I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/r;->a(I)I

    move-result v1

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v1}, Landroid/support/v7/widget/t;->c(I)V

    iget-object v0, v0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/s;->c(I)Z

    .line 5099
    return-void
.end method

.method public static c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 5610
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    .line 5611
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static d(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 5624
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    .line 5625
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static g()Landroid/view/View;
    .locals 1

    .prologue
    .line 5834
    const/4 v0, 0x0

    return-object v0
.end method

.method public static h()Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Z"
        }
    .end annotation

    .prologue
    .line 5963
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 4746
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6412
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-nez v1, :cond_1

    .line 6415
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/bs;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->a()I

    move-result v0

    goto :goto_0
.end method

.method public abstract a()Landroid/support/v7/widget/RecyclerView$LayoutParams;
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    .prologue
    .line 4729
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    .prologue
    .line 4705
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v0, :cond_0

    .line 4706
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    check-cast p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/support/v7/widget/RecyclerView$LayoutParams;)V

    .line 4710
    :goto_0
    return-object v0

    .line 4707
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 4708
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    .line 4710
    :cond_1
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public a(I)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 5045
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v2, v0

    :goto_0
    move v4, v1

    .line 5046
    :goto_1
    if-ge v4, v0, :cond_7

    .line 5047
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/r;->a(I)I

    move-result v5

    iget-object v2, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v5}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v2

    .line 5048
    :goto_2
    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v7

    .line 5049
    if-eqz v7, :cond_6

    .line 5050
    iget v5, v7, Landroid/support/v7/widget/ce;->f:I

    const/4 v8, -0x1

    if-ne v5, v8, :cond_3

    iget v5, v7, Landroid/support/v7/widget/ce;->b:I

    :goto_3
    if-ne v5, p1, :cond_6

    iget v5, v7, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v5, v5, 0x80

    if-eqz v5, :cond_4

    move v5, v6

    :goto_4
    if-nez v5, :cond_6

    iget-object v5, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v5, v5, Landroid/support/v7/widget/cc;->i:Z

    if-nez v5, :cond_0

    iget v5, v7, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_5

    move v5, v6

    :goto_5
    if-nez v5, :cond_6

    .line 5057
    :cond_0
    :goto_6
    return-object v2

    :cond_1
    move v0, v1

    .line 5045
    goto :goto_0

    :cond_2
    move-object v2, v3

    .line 5047
    goto :goto_2

    .line 5050
    :cond_3
    iget v5, v7, Landroid/support/v7/widget/ce;->f:I

    goto :goto_3

    :cond_4
    move v5, v1

    goto :goto_4

    :cond_5
    move v5, v1

    goto :goto_5

    .line 5046
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_7
    move-object v2, v3

    .line 5057
    goto :goto_6
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 5985
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 6203
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 5973
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 4809
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/bw;)V
    .locals 0

    .prologue
    .line 4593
    return-void
.end method

.method public final a(Landroid/support/v7/widget/bw;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5447
    invoke-virtual {p0}, Landroid/support/v7/widget/bs;->f()I

    move-result v0

    .line 5448
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_b

    .line 5449
    invoke-virtual {p0, v3}, Landroid/support/v7/widget/bs;->e(I)Landroid/view/View;

    move-result-object v4

    .line 5450
    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v5

    iget v0, v5, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_0

    iget v0, v5, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    iget v0, v5, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    if-nez v0, :cond_5

    iget v0, v5, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    if-nez v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v0, v0, Landroid/support/v7/widget/bk;->b:Z

    if-nez v0, :cond_5

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/bs;->d(I)V

    invoke-virtual {p1, v5}, Landroid/support/v7/widget/bw;->a(Landroid/support/v7/widget/ce;)V

    .line 5448
    :cond_0
    :goto_5
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 5450
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    invoke-direct {p0, v3}, Landroid/support/v7/widget/bs;->b(I)V

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v4

    iput-object p1, v4, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    iget v0, v4, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    move v0, v1

    :goto_6
    if-eqz v0, :cond_6

    iget-object v0, p1, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v5, :cond_6

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    :cond_6
    iget v0, v4, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    move v0, v1

    :goto_7
    if-eqz v0, :cond_a

    iget v0, v4, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_9

    move v0, v1

    :goto_8
    if-nez v0, :cond_a

    iget-object v0, p1, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v0, v0, Landroid/support/v7/widget/bk;->b:Z

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v0, v2

    goto :goto_6

    :cond_8
    move v0, v2

    goto :goto_7

    :cond_9
    move v0, v2

    goto :goto_8

    :cond_a
    iget-object v0, p1, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 5452
    :cond_b
    return-void
.end method

.method public a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Landroid/view/View;Landroid/support/v4/view/a/h;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 6358
    invoke-virtual {p0}, Landroid/support/v7/widget/bs;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, Landroid/support/v7/widget/bs;->b(Landroid/view/View;)I

    move-result v1

    .line 6359
    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/bs;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p3}, Landroid/support/v7/widget/bs;->b(Landroid/view/View;)I

    move-result v3

    .line 6360
    :goto_1
    new-instance v7, Landroid/support/v4/view/a/q;

    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    move v4, v2

    move v6, v5

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/view/a/k;->a(IIIIZZ)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/support/v4/view/a/q;-><init>(Ljava/lang/Object;)V

    .line 6363
    sget-object v1, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v2, p4, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    move-object v0, v7

    check-cast v0, Landroid/support/v4/view/a/q;

    iget-object v0, v0, Landroid/support/v4/view/a/q;->a:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 6364
    return-void

    :cond_0
    move v1, v5

    .line 6358
    goto :goto_0

    :cond_1
    move v3, v5

    .line 6359
    goto :goto_1
.end method

.method public final a(Landroid/support/v7/widget/ca;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 4818
    iget-object v0, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    iget-boolean v0, v0, Landroid/support/v7/widget/ca;->k:Z

    if-eqz v0, :cond_0

    .line 4820
    iget-object v0, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    invoke-virtual {v0}, Landroid/support/v7/widget/ca;->b()V

    .line 4822
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    .line 4823
    iget-object v0, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iput-object v1, v0, Landroid/support/v7/widget/ca;->h:Landroid/support/v7/widget/RecyclerView;

    iput-object p0, v0, Landroid/support/v7/widget/ca;->i:Landroid/support/v7/widget/bs;

    iget v1, v0, Landroid/support/v7/widget/ca;->g:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid target position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, v0, Landroid/support/v7/widget/ca;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget v2, v0, Landroid/support/v7/widget/ca;->g:I

    iput v2, v1, Landroid/support/v7/widget/cc;->a:I

    iput-boolean v3, v0, Landroid/support/v7/widget/ca;->k:Z

    iput-boolean v3, v0, Landroid/support/v7/widget/ca;->j:Z

    iget v1, v0, Landroid/support/v7/widget/ca;->g:I

    iget-object v2, v0, Landroid/support/v7/widget/ca;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/bs;->a(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/widget/ca;->l:Landroid/view/View;

    iget-object v0, v0, Landroid/support/v7/widget/ca;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    iget-boolean v1, v0, Landroid/support/v7/widget/cd;->d:Z

    if-eqz v1, :cond_2

    iput-boolean v3, v0, Landroid/support/v7/widget/cd;->e:Z

    .line 4824
    :goto_0
    return-void

    .line 4823
    :cond_2
    iget-object v1, v0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 4901
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/bs;->a(Landroid/view/View;IZ)V

    .line 4902
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 4890
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/bs;->a(Landroid/view/View;IZ)V

    .line 4891
    return-void
.end method

.method final a(Landroid/view/View;Landroid/support/v4/view/a/h;)V
    .locals 2

    .prologue
    .line 6334
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v0

    .line 6336
    if-eqz v0, :cond_0

    iget v0, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    .line 6337
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Landroid/view/View;Landroid/support/v4/view/a/h;)V

    .line 6340
    :cond_0
    return-void

    .line 6336
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/support/v7/widget/bw;)V
    .locals 3

    .prologue
    .line 5214
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, p1}, Landroid/support/v7/widget/t;->a(Landroid/view/View;)I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v1}, Landroid/support/v7/widget/t;->a(I)V

    iget-object v2, v0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/s;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 5215
    :cond_0
    invoke-virtual {p2, p1}, Landroid/support/v7/widget/bw;->a(Landroid/view/View;)V

    .line 5216
    return-void
.end method

.method public a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x1

    .line 6301
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-static {p1}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/ae;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 6302
    :cond_0
    :goto_0
    return-void

    .line 6301
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v0}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    :goto_1
    sget-object v2, Landroid/support/v4/view/a/ae;->a:Landroid/support/v4/view/a/ah;

    iget-object v3, v1, Landroid/support/v4/view/a/ae;->b:Ljava/lang/Object;

    invoke-interface {v2, v3, v0}, Landroid/support/v4/view/a/ah;->a(Ljava/lang/Object;Z)V

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->a()I

    move-result v0

    sget-object v2, Landroid/support/v4/view/a/ae;->a:Landroid/support/v4/view/a/ah;

    iget-object v1, v1, Landroid/support/v4/view/a/ae;->b:Ljava/lang/Object;

    invoke-interface {v2, v1, v0}, Landroid/support/v4/view/a/ah;->b(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4531
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    .line 4532
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->s:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call this method while RecyclerView is computing a layout or scrolling"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4534
    :cond_1
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$LayoutParams;)Z
    .locals 1

    .prologue
    .line 4688
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 4763
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6431
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-nez v1, :cond_1

    .line 6434
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/bs;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->a()I

    move-result v0

    goto :goto_0
.end method

.method public b(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 6051
    const/4 v0, 0x0

    return v0
.end method

.method public b(II)V
    .locals 0

    .prologue
    .line 5995
    return-void
.end method

.method final b(Landroid/support/v7/widget/bw;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5482
    iget-object v0, p1, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 5483
    :goto_0
    if-ge v3, v4, :cond_4

    .line 5484
    iget-object v0, p1, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v5, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 5485
    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v6

    .line 5486
    iget v0, v6, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_1

    .line 5487
    iget v0, v6, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 5490
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v5, v2}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 5492
    :cond_0
    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v0

    const/4 v5, 0x0

    iput-object v5, v0, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    iget v5, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v5, v5, -0x21

    iput v5, v0, Landroid/support/v7/widget/ce;->i:I

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/bw;->a(Landroid/support/v7/widget/ce;)V

    .line 5483
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 5486
    goto :goto_1

    :cond_3
    move v0, v2

    .line 5487
    goto :goto_2

    .line 5494
    :cond_4
    iget-object v0, p1, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 5495
    if-lez v4, :cond_5

    .line 5496
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 5498
    :cond_5
    return-void
.end method

.method public final b(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 4913
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/bs;->a(Landroid/view/View;IZ)V

    .line 4914
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 4557
    const/4 v0, 0x0

    return v0
.end method

.method public c(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 6096
    const/4 v0, 0x0

    return v0
.end method

.method public c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 6197
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)Landroid/view/View;
    .locals 1

    .prologue
    .line 5814
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 4796
    return-void
.end method

.method public c(II)V
    .locals 0

    .prologue
    .line 6005
    return-void
.end method

.method public c(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)V
    .locals 0

    .prologue
    .line 4657
    return-void
.end method

.method public d(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 6036
    const/4 v0, 0x0

    return v0
.end method

.method public final d(I)V
    .locals 4

    .prologue
    .line 4991
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/r;->a(I)I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    .line 4992
    :goto_0
    if-eqz v0, :cond_0

    .line 4993
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/r;->a(I)I

    move-result v1

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3, v1}, Landroid/support/v7/widget/t;->a(I)V

    iget-object v3, v0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/s;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4995
    :cond_0
    return-void

    .line 4991
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(II)V
    .locals 0

    .prologue
    .line 6021
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 4773
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 6081
    const/4 v0, 0x0

    return v0
.end method

.method public final e(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 5246
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/r;->a(I)I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 4783
    const/4 v0, 0x0

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 5237
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 6066
    const/4 v0, 0x0

    return v0
.end method

.method public f(I)V
    .locals 5

    .prologue
    .line 5379
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 5380
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v1, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v2, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, v1, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v4

    iget-object v3, v3, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3, v4}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5382
    :cond_0
    return-void
.end method

.method public g(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 6111
    const/4 v0, 0x0

    return v0
.end method

.method public g(I)V
    .locals 5

    .prologue
    .line 5391
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 5392
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v1, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v2, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, v1, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v4

    iget-object v3, v3, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3, v4}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5394
    :cond_0
    return-void
.end method

.method public h(I)V
    .locals 0

    .prologue
    .line 6223
    return-void
.end method

.method final i()V
    .locals 1

    .prologue
    .line 6206
    iget-object v0, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    if-eqz v0, :cond_0

    .line 6207
    iget-object v0, p0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    invoke-virtual {v0}, Landroid/support/v7/widget/ca;->b()V

    .line 6209
    :cond_0
    return-void
.end method
