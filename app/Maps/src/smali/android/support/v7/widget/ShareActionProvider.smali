.class public Landroid/support/v7/widget/ShareActionProvider;
.super Landroid/support/v4/view/n;
.source "PG"


# instance fields
.field final c:Landroid/content/Context;

.field d:Ljava/lang/String;

.field private e:I

.field private final f:Landroid/support/v7/widget/cy;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0, p1}, Landroid/support/v4/view/n;-><init>(Landroid/content/Context;)V

    .line 124
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/v7/widget/ShareActionProvider;->e:I

    .line 129
    new-instance v0, Landroid/support/v7/widget/cy;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cy;-><init>(Landroid/support/v7/widget/ShareActionProvider;)V

    iput-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->f:Landroid/support/v7/widget/cy;

    .line 145
    const-string v0, "share_history.xml"

    iput-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->d:Ljava/lang/String;

    .line 158
    iput-object p1, p0, Landroid/support/v7/widget/ShareActionProvider;->c:Landroid/content/Context;

    .line 159
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/SubMenu;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 216
    invoke-interface {p1}, Landroid/view/SubMenu;->clear()V

    .line 218
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->c:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/widget/ShareActionProvider;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/support/v7/internal/widget/k;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/k;

    move-result-object v2

    .line 219
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 221
    invoke-virtual {v2}, Landroid/support/v7/internal/widget/k;->a()I

    move-result v4

    .line 222
    iget v0, p0, Landroid/support/v7/widget/ShareActionProvider;->e:I

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v0, v1

    .line 225
    :goto_0
    if-ge v0, v5, :cond_0

    .line 226
    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/k;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 227
    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {p1, v1, v0, v0, v7}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-interface {v7, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v6

    iget-object v7, p0, Landroid/support/v7/widget/ShareActionProvider;->f:Landroid/support/v7/widget/cy;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 232
    :cond_0
    if-ge v5, v4, :cond_1

    .line 234
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->c:Landroid/content/Context;

    sget v6, Landroid/support/v7/a/i;->b:I

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v5, v5, v0}, Landroid/view/SubMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v5

    move v0, v1

    .line 237
    :goto_1
    if-ge v0, v4, :cond_1

    .line 238
    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/k;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 239
    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v5, v1, v0, v0, v7}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-interface {v7, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v6

    iget-object v7, p0, Landroid/support/v7/widget/ShareActionProvider;->f:Landroid/support/v7/widget/cy;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 244
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x1

    return v0
.end method
