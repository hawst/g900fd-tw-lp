.class public Landroid/support/v7/widget/cf;
.super Landroid/support/v4/view/a;
.source "PG"


# instance fields
.field final b:Landroid/support/v7/widget/RecyclerView;

.field final c:Landroid/support/v4/view/a;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/view/a;-><init>()V

    .line 75
    new-instance v0, Landroid/support/v7/widget/cg;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cg;-><init>(Landroid/support/v7/widget/cf;)V

    iput-object v0, p0, Landroid/support/v7/widget/cf;->c:Landroid/support/v4/view/a;

    .line 35
    iput-object p1, p0, Landroid/support/v7/widget/cf;->b:Landroid/support/v7/widget/RecyclerView;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/support/v4/view/a/h;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 52
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/support/v4/view/a/h;)V

    .line 53
    const-class v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v2, p2, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/k;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Landroid/support/v7/widget/cf;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_4

    .line 55
    iget-object v0, p0, Landroid/support/v7/widget/cf;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v2, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    const-class v3, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v5, p2, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    invoke-interface {v4, v5, v3}, Landroid/support/v4/view/a/k;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    iget-object v3, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v7}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v7}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/16 v3, 0x2000

    sget-object v4, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v5, p2, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    invoke-interface {v4, v5, v3}, Landroid/support/v4/view/a/k;->a(Ljava/lang/Object;I)V

    sget-object v3, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v4, p2, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    invoke-interface {v3, v4, v6}, Landroid/support/v4/view/a/k;->f(Ljava/lang/Object;Z)V

    :cond_1
    iget-object v3, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v6}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v6}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const/16 v3, 0x1000

    sget-object v4, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v5, p2, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    invoke-interface {v4, v5, v3}, Landroid/support/v4/view/a/k;->a(Ljava/lang/Object;I)V

    sget-object v3, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v4, p2, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    invoke-interface {v3, v4, v6}, Landroid/support/v4/view/a/k;->f(Ljava/lang/Object;Z)V

    :cond_3
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v3

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bs;->b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v1

    new-instance v0, Landroid/support/v4/view/a/p;

    sget-object v2, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    invoke-interface {v2, v3, v1, v8, v8}, Landroid/support/v4/view/a/k;->a(IIZI)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/a/p;-><init>(Ljava/lang/Object;)V

    sget-object v1, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v2, p2, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/view/a/p;

    iget-object v0, v0, Landroid/support/v4/view/a/p;->a:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/k;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 57
    :cond_4
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v3, -0x1

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 40
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/a;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v4

    .line 47
    :cond_0
    :goto_0
    return v1

    .line 43
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/cf;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Landroid/support/v7/widget/cf;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    sparse-switch p2, :sswitch_data_0

    move v0, v1

    move v2, v1

    :goto_1
    if-nez v2, :cond_2

    if-eqz v0, :cond_0

    :cond_2
    iget-object v1, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    move v1, v4

    goto :goto_0

    :sswitch_0
    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v3}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_3

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    :goto_2
    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_4

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    :goto_3
    sub-int v2, v0, v2

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_5

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    :goto_4
    sub-int v0, v2, v0

    neg-int v0, v0

    :goto_5
    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_6

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    :goto_6
    iget-object v3, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_7

    iget-object v3, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    :goto_7
    sub-int v3, v2, v3

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_8

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    :goto_8
    sub-int v2, v3, v2

    neg-int v2, v2

    move v6, v2

    move v2, v0

    move v0, v6

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    move v2, v1

    goto :goto_6

    :cond_7
    move v3, v1

    goto :goto_7

    :cond_8
    move v2, v1

    goto :goto_8

    :sswitch_1
    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v4}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_9

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    :goto_9
    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_a

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    :goto_a
    sub-int v2, v0, v2

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_b

    iget-object v0, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    :goto_b
    sub-int v0, v2, v0

    :goto_c
    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v4}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_c

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    :goto_d
    iget-object v3, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_d

    iget-object v3, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    :goto_e
    sub-int v3, v2, v3

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_e

    iget-object v2, v5, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    :goto_f
    sub-int v2, v3, v2

    move v6, v2

    move v2, v0

    move v0, v6

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto :goto_9

    :cond_a
    move v2, v1

    goto :goto_a

    :cond_b
    move v0, v1

    goto :goto_b

    :cond_c
    move v2, v1

    goto :goto_d

    :cond_d
    move v3, v1

    goto :goto_e

    :cond_e
    move v2, v1

    goto :goto_f

    :cond_f
    move v2, v0

    move v0, v1

    goto/16 :goto_1

    :cond_10
    move v0, v1

    goto :goto_c

    :cond_11
    move v0, v1

    goto/16 :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_1
        0x2000 -> :sswitch_0
    .end sparse-switch
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 62
    const-class v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 63
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 64
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    .line 65
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/bs;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 69
    :cond_0
    return-void
.end method
