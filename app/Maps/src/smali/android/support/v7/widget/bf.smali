.class Landroid/support/v7/widget/bf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 153
    iget-object v0, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-nez v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->r:Z

    if-eqz v0, :cond_2

    .line 158
    iget-object v0, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->g()V

    goto :goto_0

    .line 159
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    iget-object v0, v0, Landroid/support/v7/widget/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v3

    :goto_1
    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v2, v0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v2, :cond_3

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->m:Z

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->n:Z

    .line 161
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0}, Landroid/support/v7/widget/h;->a()V

    .line 162
    iget-object v0, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->n:Z

    if-nez v0, :cond_f

    .line 165
    iget-object v4, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v5, v2, v0

    move v0, v1

    :goto_2
    if-ge v0, v5, :cond_f

    iget-object v2, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v6

    iget-object v2, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v6}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v6

    if-eqz v6, :cond_5

    iget v2, v6, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_7

    move v2, v3

    :goto_3
    if-nez v2, :cond_5

    iget v2, v6, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_8

    move v2, v3

    :goto_4
    if-nez v2, :cond_4

    iget v2, v6, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_9

    move v2, v3

    :goto_5
    if-eqz v2, :cond_a

    :cond_4
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    :cond_5
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    move v0, v1

    .line 159
    goto :goto_1

    :cond_7
    move v2, v1

    .line 165
    goto :goto_3

    :cond_8
    move v2, v1

    goto :goto_4

    :cond_9
    move v2, v1

    goto :goto_5

    :cond_a
    iget v2, v6, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_c

    move v2, v3

    :goto_7
    if-eqz v2, :cond_5

    iget-object v2, v4, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget v7, v6, Landroid/support/v7/widget/ce;->b:I

    invoke-virtual {v2, v7}, Landroid/support/v7/widget/bk;->a(I)I

    move-result v2

    iget v7, v6, Landroid/support/v7/widget/ce;->e:I

    if-ne v7, v2, :cond_e

    iget v2, v6, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_d

    move v2, v3

    :goto_8
    if-eqz v2, :cond_b

    iget-object v2, v4, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v2, :cond_b

    iget-object v2, v4, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    :cond_b
    iget-object v2, v4, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget v7, v6, Landroid/support/v7/widget/ce;->b:I

    invoke-virtual {v2, v6, v7}, Landroid/support/v7/widget/bk;->b(Landroid/support/v7/widget/ce;I)V

    goto :goto_6

    :cond_c
    move v2, v1

    goto :goto_7

    :cond_d
    move v2, v1

    goto :goto_8

    :cond_e
    const/4 v2, 0x4

    iget v7, v6, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v2, v7

    iput v2, v6, Landroid/support/v7/widget/ce;->i:I

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_6

    .line 167
    :cond_f
    iget-object v0, p0, Landroid/support/v7/widget/bf;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    goto/16 :goto_0
.end method
