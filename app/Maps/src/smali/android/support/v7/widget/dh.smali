.class Landroid/support/v7/widget/dh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field b:I

.field c:I

.field d:I

.field final e:I

.field final synthetic f:Landroid/support/v7/widget/StaggeredGridLayoutManager;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/StaggeredGridLayoutManager;I)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 1939
    iput-object p1, p0, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    .line 1934
    iput v1, p0, Landroid/support/v7/widget/dh;->b:I

    .line 1935
    iput v1, p0, Landroid/support/v7/widget/dh;->c:I

    .line 1936
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/dh;->d:I

    .line 1940
    iput p2, p0, Landroid/support/v7/widget/dh;->e:I

    .line 1941
    return-void
.end method


# virtual methods
.method final a(I)I
    .locals 2

    .prologue
    .line 1944
    iget v0, p0, Landroid/support/v7/widget/dh;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1945
    iget p1, p0, Landroid/support/v7/widget/dh;->b:I

    .line 1951
    :cond_0
    :goto_0
    return p1

    .line 1947
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1950
    invoke-virtual {p0}, Landroid/support/v7/widget/dh;->a()V

    .line 1951
    iget p1, p0, Landroid/support/v7/widget/dh;->b:I

    goto :goto_0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 1955
    iget-object v0, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1956
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    .line 1957
    iget-object v1, p0, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/dh;->b:I

    .line 1958
    return-void
.end method

.method final a(Landroid/view/View;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2011
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    .line 2012
    iput-object p0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    .line 2013
    iget-object v3, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2014
    iput v4, p0, Landroid/support/v7/widget/dh;->b:I

    .line 2015
    iget-object v3, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 2016
    iput v4, p0, Landroid/support/v7/widget/dh;->c:I

    .line 2018
    :cond_0
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v3, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_3

    move v3, v1

    :goto_0
    if-nez v3, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v0, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 2019
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/dh;->d:I

    iget-object v1, p0, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/dh;->d:I

    .line 2021
    :cond_2
    return-void

    :cond_3
    move v3, v2

    .line 2018
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method final b(I)I
    .locals 2

    .prologue
    .line 1977
    iget v0, p0, Landroid/support/v7/widget/dh;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1978
    iget p1, p0, Landroid/support/v7/widget/dh;->c:I

    .line 1985
    :cond_0
    :goto_0
    return p1

    .line 1980
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1981
    if-eqz v0, :cond_0

    .line 1984
    invoke-virtual {p0}, Landroid/support/v7/widget/dh;->b()V

    .line 1985
    iget p1, p0, Landroid/support/v7/widget/dh;->c:I

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 1989
    iget-object v0, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1990
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    .line 1991
    iget-object v1, p0, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/dh;->c:I

    .line 1992
    return-void
.end method

.method final b(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/high16 v4, -0x80000000

    const/4 v1, 0x1

    .line 2024
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;

    .line 2025
    iput-object p0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LayoutParams;->a:Landroid/support/v7/widget/dh;

    .line 2026
    iget-object v3, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2027
    iput v4, p0, Landroid/support/v7/widget/dh;->c:I

    .line 2028
    iget-object v3, p0, Landroid/support/v7/widget/dh;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 2029
    iput v4, p0, Landroid/support/v7/widget/dh;->b:I

    .line 2031
    :cond_0
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v3, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_3

    move v3, v1

    :goto_0
    if-nez v3, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v0, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 2032
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/dh;->d:I

    iget-object v1, p0, Landroid/support/v7/widget/dh;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/dh;->d:I

    .line 2034
    :cond_2
    return-void

    :cond_3
    move v3, v2

    .line 2031
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method final c(I)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 2109
    iget v0, p0, Landroid/support/v7/widget/dh;->b:I

    if-eq v0, v1, :cond_0

    .line 2110
    iget v0, p0, Landroid/support/v7/widget/dh;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/dh;->b:I

    .line 2112
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/dh;->c:I

    if-eq v0, v1, :cond_1

    .line 2113
    iget v0, p0, Landroid/support/v7/widget/dh;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/dh;->c:I

    .line 2115
    :cond_1
    return-void
.end method
