.class Landroid/support/v7/widget/cu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/SearchView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/SearchView;)V
    .locals 0

    .prologue
    .line 904
    iput-object p1, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    iget-object v0, v0, Landroid/support/v7/widget/SearchView;->e:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    .line 908
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Landroid/support/v7/widget/SearchView;->c(Landroid/support/v7/widget/SearchView;)V

    .line 920
    :cond_0
    :goto_0
    return-void

    .line 909
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    iget-object v0, v0, Landroid/support/v7/widget/SearchView;->g:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_2

    .line 910
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Landroid/support/v7/widget/SearchView;->d(Landroid/support/v7/widget/SearchView;)V

    goto :goto_0

    .line 911
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    iget-object v0, v0, Landroid/support/v7/widget/SearchView;->f:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_3

    .line 912
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->a()V

    goto :goto_0

    .line 913
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    iget-object v0, v0, Landroid/support/v7/widget/SearchView;->h:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_4

    .line 914
    sget-boolean v0, Landroid/support/v7/widget/SearchView;->c:Z

    if-eqz v0, :cond_0

    .line 915
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    goto :goto_0

    .line 917
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    iget-object v0, v0, Landroid/support/v7/widget/SearchView;->d:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    if-ne p1, v0, :cond_0

    .line 918
    iget-object v0, p0, Landroid/support/v7/widget/cu;->a:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->d()V

    goto :goto_0
.end method
