.class Landroid/support/v7/widget/ab;
.super Landroid/support/v7/widget/af;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v7/widget/ad;

.field final synthetic b:Landroid/support/v4/view/cj;

.field final synthetic c:Landroid/support/v7/widget/u;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ad;Landroid/support/v4/view/cj;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Landroid/support/v7/widget/ab;->c:Landroid/support/v7/widget/u;

    iput-object p2, p0, Landroid/support/v7/widget/ab;->a:Landroid/support/v7/widget/ad;

    iput-object p3, p0, Landroid/support/v7/widget/ab;->b:Landroid/support/v4/view/cj;

    invoke-direct {p0}, Landroid/support/v7/widget/af;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Landroid/support/v7/widget/ab;->c:Landroid/support/v7/widget/u;

    iget-object v0, p0, Landroid/support/v7/widget/ab;->a:Landroid/support/v7/widget/ad;

    iget-object v0, v0, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    .line 347
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 351
    iget-object v1, p0, Landroid/support/v7/widget/ab;->b:Landroid/support/v4/view/cj;

    const/4 v2, 0x0

    iget-object v0, v1, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v3, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v3, v1, v0, v2}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    .line 352
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 353
    invoke-static {p1, v4}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 354
    invoke-static {p1, v4}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 355
    iget-object v0, p0, Landroid/support/v7/widget/ab;->c:Landroid/support/v7/widget/u;

    iget-object v1, p0, Landroid/support/v7/widget/ab;->a:Landroid/support/v7/widget/ad;

    iget-object v1, v1, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    iget-object v2, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/bo;->d(Landroid/support/v7/widget/ce;)V

    .line 356
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ab;->c:Landroid/support/v7/widget/u;

    iget-object v0, v0, Landroid/support/v7/widget/u;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/ab;->a:Landroid/support/v7/widget/ad;

    iget-object v1, v1, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 357
    iget-object v0, p0, Landroid/support/v7/widget/ab;->c:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->b()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->d()V

    .line 358
    :cond_2
    return-void
.end method
