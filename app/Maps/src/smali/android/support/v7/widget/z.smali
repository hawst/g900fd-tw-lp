.class Landroid/support/v7/widget/z;
.super Landroid/support/v7/widget/af;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v7/widget/ce;

.field final synthetic b:Landroid/support/v4/view/cj;

.field final synthetic c:Landroid/support/v7/widget/u;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ce;Landroid/support/v4/view/cj;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Landroid/support/v7/widget/z;->c:Landroid/support/v7/widget/u;

    iput-object p2, p0, Landroid/support/v7/widget/z;->a:Landroid/support/v7/widget/ce;

    iput-object p3, p0, Landroid/support/v7/widget/z;->b:Landroid/support/v4/view/cj;

    invoke-direct {p0}, Landroid/support/v7/widget/af;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Landroid/support/v7/widget/z;->c:Landroid/support/v7/widget/u;

    iget-object v0, p0, Landroid/support/v7/widget/z;->a:Landroid/support/v7/widget/ce;

    .line 230
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 238
    iget-object v1, p0, Landroid/support/v7/widget/z;->b:Landroid/support/v4/view/cj;

    const/4 v2, 0x0

    iget-object v0, v1, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v3, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v3, v1, v0, v2}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    .line 239
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/z;->c:Landroid/support/v7/widget/u;

    iget-object v1, p0, Landroid/support/v7/widget/z;->a:Landroid/support/v7/widget/ce;

    iget-object v2, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/bo;->b(Landroid/support/v7/widget/ce;)V

    .line 240
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/z;->c:Landroid/support/v7/widget/u;

    iget-object v0, v0, Landroid/support/v7/widget/u;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/z;->a:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 241
    iget-object v0, p0, Landroid/support/v7/widget/z;->c:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->b()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->d()V

    .line 242
    :cond_2
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 233
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 234
    return-void
.end method
