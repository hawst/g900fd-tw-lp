.class Landroid/support/v7/widget/cd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:I

.field b:I

.field c:Landroid/support/v4/widget/aq;

.field d:Z

.field e:Z

.field final synthetic f:Landroid/support/v7/widget/RecyclerView;

.field private g:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2767
    iput-object p1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2758
    sget-object v0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Landroid/support/v7/widget/cd;->g:Landroid/view/animation/Interpolator;

    .line 2762
    iput-boolean v1, p0, Landroid/support/v7/widget/cd;->d:Z

    .line 2765
    iput-boolean v1, p0, Landroid/support/v7/widget/cd;->e:Z

    .line 2768
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/widget/RecyclerView;->G:Landroid/view/animation/Interpolator;

    new-instance v2, Landroid/support/v4/widget/aq;

    invoke-direct {v2, v0, v1}, Landroid/support/v4/widget/aq;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v2, p0, Landroid/support/v7/widget/cd;->c:Landroid/support/v4/widget/aq;

    .line 2769
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    .line 2922
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-le v2, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v4, v4

    mul-int v1, p1, p1

    mul-int v5, p2, p2

    add-int/2addr v1, v5

    int-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v5, v6

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    :goto_1
    div-int/lit8 v6, v1, 0x2

    int-to-float v5, v5

    mul-float/2addr v5, v12

    int-to-float v7, v1

    div-float/2addr v5, v7

    invoke-static {v12, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    int-to-float v7, v6

    int-to-float v6, v6

    const/high16 v8, 0x3f000000    # 0.5f

    sub-float/2addr v5, v8

    float-to-double v8, v5

    const-wide v10, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v8, v10

    double-to-float v5, v8

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v5, v8

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    if-lez v4, :cond_2

    const/high16 v0, 0x447a0000    # 1000.0f

    int-to-float v1, v4

    div-float v1, v5, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    :goto_2
    const/16 v1, 0x7d0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sget-object v1, Landroid/support/v7/widget/RecyclerView;->G:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/support/v7/widget/cd;->a(IIILandroid/view/animation/Interpolator;)V

    .line 2923
    return-void

    :cond_0
    move v0, v1

    .line 2922
    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v12

    const/high16 v1, 0x43960000    # 300.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_3
.end method

.method public final a(IIILandroid/view/animation/Interpolator;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2962
    iget-object v0, p0, Landroid/support/v7/widget/cd;->g:Landroid/view/animation/Interpolator;

    if-eq v0, p4, :cond_0

    .line 2963
    iput-object p4, p0, Landroid/support/v7/widget/cd;->g:Landroid/view/animation/Interpolator;

    .line 2964
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/support/v4/widget/aq;

    invoke-direct {v1, v0, p4}, Landroid/support/v4/widget/aq;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Landroid/support/v7/widget/cd;->c:Landroid/support/v4/widget/aq;

    .line 2966
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 2967
    iput v2, p0, Landroid/support/v7/widget/cd;->b:I

    iput v2, p0, Landroid/support/v7/widget/cd;->a:I

    .line 2968
    iget-object v1, p0, Landroid/support/v7/widget/cd;->c:Landroid/support/v4/widget/aq;

    iget-object v0, v1, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v1, v1, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    move v3, v2

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/widget/ar;->a(Ljava/lang/Object;IIIII)V

    .line 2969
    iget-boolean v0, p0, Landroid/support/v7/widget/cd;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->e:Z

    .line 2970
    :goto_0
    return-void

    .line 2969
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public run()V
    .locals 15

    .prologue
    .line 2773
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->e:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->d:Z

    .line 2774
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2777
    iget-object v8, p0, Landroid/support/v7/widget/cd;->c:Landroid/support/v4/widget/aq;

    .line 2778
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v9, v0, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    .line 2779
    iget-object v0, v8, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v1, v8, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/ar;->e(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2780
    iget-object v0, v8, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v1, v8, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/ar;->b(Ljava/lang/Object;)I

    move-result v10

    .line 2781
    iget-object v0, v8, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v1, v8, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/ar;->c(Ljava/lang/Object;)I

    move-result v11

    .line 2782
    iget v0, p0, Landroid/support/v7/widget/cd;->a:I

    sub-int v12, v10, v0

    .line 2783
    iget v0, p0, Landroid/support/v7/widget/cd;->b:I

    sub-int v13, v11, v0

    .line 2784
    const/4 v3, 0x0

    .line 2785
    const/4 v1, 0x0

    .line 2786
    iput v10, p0, Landroid/support/v7/widget/cd;->a:I

    .line 2787
    iput v11, p0, Landroid/support/v7/widget/cd;->b:I

    .line 2788
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 2789
    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v4, :cond_5

    .line 2790
    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v5, v4, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v5, :cond_0

    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/support/v7/widget/RecyclerView;->m:Z

    const/4 v5, 0x0

    iput-boolean v5, v4, Landroid/support/v7/widget/RecyclerView;->n:Z

    .line 2791
    :cond_0
    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/support/v7/widget/RecyclerView;->s:Z

    .line 2792
    if-eqz v12, :cond_1

    .line 2793
    iget-object v2, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v3, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v2, v12, v3, v4}, Landroid/support/v7/widget/bs;->a(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v3

    .line 2794
    sub-int v2, v12, v3

    .line 2796
    :cond_1
    if-eqz v13, :cond_2

    .line 2797
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, v13, v1, v4}, Landroid/support/v7/widget/bs;->b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v1

    .line 2798
    sub-int v0, v13, v1

    .line 2800
    :cond_2
    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v4, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v5, :cond_3

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    .line 2823
    :cond_3
    if-eqz v9, :cond_4

    iget-boolean v4, v9, Landroid/support/v7/widget/ca;->j:Z

    if-nez v4, :cond_4

    iget-boolean v4, v9, Landroid/support/v7/widget/ca;->k:Z

    if-eqz v4, :cond_4

    .line 2825
    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v5, v4, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v5, :cond_18

    iget v5, v4, Landroid/support/v7/widget/cc;->f:I

    iget v4, v4, Landroid/support/v7/widget/cc;->g:I

    sub-int v4, v5, v4

    .line 2826
    :goto_0
    if-nez v4, :cond_19

    .line 2827
    invoke-virtual {v9}, Landroid/support/v7/widget/ca;->b()V

    .line 2835
    :cond_4
    :goto_1
    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v5, 0x0

    iput-boolean v5, v4, Landroid/support/v7/widget/RecyclerView;->s:Z

    .line 2836
    iget-object v4, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    :cond_5
    move v4, v0

    move v5, v2

    move v6, v1

    move v7, v3

    .line 2838
    if-ne v12, v7, :cond_1b

    if-ne v13, v6, :cond_1b

    const/4 v0, 0x1

    .line 2839
    :goto_2
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2840
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 2842
    :cond_6
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    .line 2844
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v12, v13}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    .line 2846
    :cond_7
    if-nez v5, :cond_8

    if-eqz v4, :cond_10

    .line 2847
    :cond_8
    iget-object v1, v8, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v2, v8, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Landroid/support/v4/widget/ar;->d(Ljava/lang/Object;)F

    move-result v1

    float-to-int v2, v1

    .line 2849
    const/4 v1, 0x0

    .line 2850
    if-eq v5, v10, :cond_25

    .line 2851
    if-gez v5, :cond_1c

    neg-int v1, v2

    :goto_3
    move v3, v1

    .line 2854
    :goto_4
    const/4 v1, 0x0

    .line 2855
    if-eq v4, v11, :cond_24

    .line 2856
    if-gez v4, :cond_1e

    neg-int v2, v2

    .line 2859
    :cond_9
    :goto_5
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;)I

    move-result v1

    const/4 v12, 0x2

    if-eq v1, v12, :cond_d

    .line 2861
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    if-gez v3, :cond_1f

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->c()V

    iget-object v12, v1, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    neg-int v13, v3

    sget-object v14, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v12, v12, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v14, v12, v13}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;I)Z

    :cond_a
    :goto_6
    if-gez v2, :cond_20

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->e()V

    iget-object v12, v1, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    neg-int v13, v2

    sget-object v14, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v12, v12, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v14, v12, v13}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;I)Z

    :cond_b
    :goto_7
    if-nez v3, :cond_c

    if-eqz v2, :cond_d

    :cond_c
    invoke-static {v1}, Landroid/support/v4/view/at;->d(Landroid/view/View;)V

    .line 2863
    :cond_d
    if-nez v3, :cond_e

    if-eq v5, v10, :cond_e

    iget-object v1, v8, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v3, v8, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v1, v3}, Landroid/support/v4/widget/ar;->g(Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_10

    :cond_e
    if-nez v2, :cond_f

    if-eq v4, v11, :cond_f

    iget-object v1, v8, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v2, v8, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Landroid/support/v4/widget/ar;->h(Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_10

    .line 2865
    :cond_f
    iget-object v1, v8, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v2, v8, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Landroid/support/v4/widget/ar;->f(Ljava/lang/Object;)V

    .line 2868
    :cond_10
    if-nez v7, :cond_11

    if-eqz v6, :cond_12

    .line 2870
    :cond_11
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;IIII)V

    .line 2871
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->B:Landroid/support/v7/widget/bu;

    if-eqz v1, :cond_12

    .line 2872
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->B:Landroid/support/v7/widget/bu;

    iget-object v2, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2, v7, v6}, Landroid/support/v7/widget/bu;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 2876
    :cond_12
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 2877
    iget-object v1, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 2880
    :cond_13
    iget-object v1, v8, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v2, v8, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Landroid/support/v4/widget/ar;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    if-nez v0, :cond_21

    .line 2881
    :cond_14
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 2887
    :cond_15
    :goto_8
    if-eqz v9, :cond_16

    iget-boolean v0, v9, Landroid/support/v7/widget/ca;->j:Z

    if-eqz v0, :cond_16

    .line 2888
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v9, v0, v1}, Landroid/support/v7/widget/ca;->a(Landroid/support/v7/widget/ca;II)V

    .line 2890
    :cond_16
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->d:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/cd;->e:Z

    if-eqz v0, :cond_17

    iget-boolean v0, p0, Landroid/support/v7/widget/cd;->d:Z

    if-eqz v0, :cond_23

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->e:Z

    .line 2891
    :cond_17
    :goto_9
    return-void

    .line 2825
    :cond_18
    iget v4, v4, Landroid/support/v7/widget/cc;->e:I

    goto/16 :goto_0

    .line 2828
    :cond_19
    iget v5, v9, Landroid/support/v7/widget/ca;->g:I

    if-lt v5, v4, :cond_1a

    .line 2829
    add-int/lit8 v4, v4, -0x1

    iput v4, v9, Landroid/support/v7/widget/ca;->g:I

    .line 2830
    sub-int v4, v12, v2

    sub-int v5, v13, v0

    invoke-static {v9, v4, v5}, Landroid/support/v7/widget/ca;->a(Landroid/support/v7/widget/ca;II)V

    goto/16 :goto_1

    .line 2832
    :cond_1a
    sub-int v4, v12, v2

    sub-int v5, v13, v0

    invoke-static {v9, v4, v5}, Landroid/support/v7/widget/ca;->a(Landroid/support/v7/widget/ca;II)V

    goto/16 :goto_1

    .line 2838
    :cond_1b
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2851
    :cond_1c
    if-lez v5, :cond_1d

    move v1, v2

    goto/16 :goto_3

    :cond_1d
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 2856
    :cond_1e
    if-gtz v4, :cond_9

    const/4 v2, 0x0

    goto/16 :goto_5

    .line 2861
    :cond_1f
    if-lez v3, :cond_a

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->d()V

    iget-object v12, v1, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    sget-object v13, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v12, v12, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v13, v12, v3}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;I)Z

    goto/16 :goto_6

    :cond_20
    if-lez v2, :cond_b

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->f()V

    iget-object v12, v1, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    sget-object v13, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v12, v12, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v13, v12, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;I)Z

    goto/16 :goto_7

    .line 2883
    :cond_21
    iget-boolean v0, p0, Landroid/support/v7/widget/cd;->d:Z

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->e:Z

    goto :goto_8

    :cond_22
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_8

    .line 2890
    :cond_23
    iget-object v0, p0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_9

    :cond_24
    move v2, v1

    goto/16 :goto_5

    :cond_25
    move v3, v1

    goto/16 :goto_4
.end method
