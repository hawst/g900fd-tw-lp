.class public Landroid/support/v7/widget/u;
.super Landroid/support/v7/widget/bn;
.source "PG"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;>;"
        }
    .end annotation
.end field

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ae;",
            ">;>;"
        }
    .end annotation
.end field

.field c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ad;",
            ">;>;"
        }
    .end annotation
.end field

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ae;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v7/widget/bn;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->d:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->e:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->f:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->g:Ljava/util/ArrayList;

    .line 620
    return-void
.end method

.method private a(Landroid/support/v7/widget/ad;)V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p1, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p1, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/u;->a(Landroid/support/v7/widget/ad;Landroid/support/v7/widget/ce;)Z

    .line 399
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_1

    .line 400
    iget-object v0, p1, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/u;->a(Landroid/support/v7/widget/ad;Landroid/support/v7/widget/ce;)Z

    .line 402
    :cond_1
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 615
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 616
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v2

    iget-object v0, v2, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v3, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v3, v2, v0}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;)V

    .line 615
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 618
    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Landroid/support/v7/widget/ce;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/widget/ad;",
            ">;",
            "Landroid/support/v7/widget/ce;",
            ")V"
        }
    .end annotation

    .prologue
    .line 385
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 386
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ad;

    .line 387
    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/u;->a(Landroid/support/v7/widget/ad;Landroid/support/v7/widget/ce;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 388
    iget-object v2, v0, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    if-nez v2, :cond_0

    iget-object v2, v0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    if-nez v2, :cond_0

    .line 389
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 385
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 393
    :cond_1
    return-void
.end method

.method private a(Landroid/support/v7/widget/ad;Landroid/support/v7/widget/ce;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 404
    .line 405
    iget-object v0, p1, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    if-ne v0, p2, :cond_1

    .line 406
    iput-object v1, p1, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    .line 413
    :goto_0
    iget-object v0, p2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 414
    iget-object v0, p2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 415
    iget-object v0, p2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 416
    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, p2}, Landroid/support/v7/widget/bo;->d(Landroid/support/v7/widget/ce;)V

    .line 417
    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 407
    :cond_1
    iget-object v0, p1, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    if-ne v0, p2, :cond_2

    .line 408
    iput-object v1, p1, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    goto :goto_0

    .line 411
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 97
    iget-object v1, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    move v2, v0

    .line 98
    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    move v4, v0

    .line 99
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    move v5, v0

    .line 100
    :goto_2
    iget-object v1, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    move v8, v0

    .line 101
    :goto_3
    if-nez v2, :cond_5

    if-nez v4, :cond_5

    if-nez v8, :cond_5

    if-nez v5, :cond_5

    .line 183
    :cond_0
    :goto_4
    return-void

    :cond_1
    move v2, v3

    .line 97
    goto :goto_0

    :cond_2
    move v4, v3

    .line 98
    goto :goto_1

    :cond_3
    move v5, v3

    .line 99
    goto :goto_2

    :cond_4
    move v8, v3

    .line 100
    goto :goto_3

    .line 106
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 107
    iget-object v1, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v10

    iget-wide v12, p0, Landroid/support/v7/widget/bn;->j:J

    iget-object v1, v10, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_6

    sget-object v11, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v11, v1, v12, v13}, Landroid/support/v4/view/cr;->a(Landroid/view/View;J)V

    :cond_6
    const/4 v11, 0x0

    iget-object v1, v10, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_7

    sget-object v12, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v12, v10, v1, v11}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_7
    new-instance v11, Landroid/support/v7/widget/y;

    invoke-direct {v11, p0, v0, v10}, Landroid/support/v7/widget/y;-><init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ce;Landroid/support/v4/view/cj;)V

    iget-object v1, v10, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_8

    sget-object v12, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v12, v10, v1, v11}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    :cond_8
    iget-object v1, v10, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_9

    sget-object v11, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v11, v10, v1}, Landroid/support/v4/view/cr;->b(Landroid/support/v4/view/cj;Landroid/view/View;)V

    :cond_9
    iget-object v1, p0, Landroid/support/v7/widget/u;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 109
    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 111
    if-eqz v4, :cond_b

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    iget-object v1, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 114
    iget-object v1, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v1, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 116
    new-instance v1, Landroid/support/v7/widget/v;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/widget/v;-><init>(Landroid/support/v7/widget/u;Ljava/util/ArrayList;)V

    .line 127
    if-eqz v2, :cond_e

    .line 128
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ae;

    iget-object v0, v0, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 129
    iget-wide v10, p0, Landroid/support/v7/widget/bn;->j:J

    invoke-static {v0, v1, v10, v11}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 135
    :cond_b
    :goto_6
    if-eqz v5, :cond_c

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 137
    iget-object v1, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 138
    iget-object v1, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v1, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 140
    new-instance v1, Landroid/support/v7/widget/w;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/widget/w;-><init>(Landroid/support/v7/widget/u;Ljava/util/ArrayList;)V

    .line 150
    if-eqz v2, :cond_f

    .line 151
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ad;

    iget-object v0, v0, Landroid/support/v7/widget/ad;->a:Landroid/support/v7/widget/ce;

    .line 152
    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    iget-wide v10, p0, Landroid/support/v7/widget/bn;->j:J

    invoke-static {v0, v1, v10, v11}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 158
    :cond_c
    :goto_7
    if-eqz v8, :cond_0

    .line 159
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 160
    iget-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 161
    iget-object v0, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 163
    new-instance v13, Landroid/support/v7/widget/x;

    invoke-direct {v13, p0, v12}, Landroid/support/v7/widget/x;-><init>(Landroid/support/v7/widget/u;Ljava/util/ArrayList;)V

    .line 172
    if-nez v2, :cond_d

    if-nez v4, :cond_d

    if-eqz v5, :cond_13

    .line 173
    :cond_d
    if-eqz v2, :cond_10

    iget-wide v0, p0, Landroid/support/v7/widget/bn;->j:J

    move-wide v10, v0

    .line 174
    :goto_8
    if-eqz v4, :cond_11

    iget-wide v0, p0, Landroid/support/v7/widget/bn;->k:J

    move-wide v8, v0

    .line 175
    :goto_9
    if-eqz v5, :cond_12

    iget-wide v0, p0, Landroid/support/v7/widget/bn;->l:J

    .line 176
    :goto_a
    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    add-long v4, v10, v0

    .line 177
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 178
    invoke-static {v0, v13, v4, v5}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    goto/16 :goto_4

    .line 131
    :cond_e
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_6

    .line 154
    :cond_f
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_7

    :cond_10
    move-wide v10, v6

    .line 173
    goto :goto_8

    :cond_11
    move-wide v8, v6

    .line 174
    goto :goto_9

    :cond_12
    move-wide v0, v6

    .line 175
    goto :goto_a

    .line 180
    :cond_13
    invoke-interface {v13}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_4
.end method

.method public final a(Landroid/support/v7/widget/ce;)Z
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/u;->c(Landroid/support/v7/widget/ce;)V

    .line 188
    iget-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/support/v7/widget/ce;IIII)Z
    .locals 7

    .prologue
    .line 249
    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 250
    int-to-float v1, p2

    iget-object v2, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/at;->o(Landroid/view/View;)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v2, v1

    .line 251
    int-to-float v1, p3

    iget-object v3, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v3}, Landroid/support/v4/view/at;->p(Landroid/view/View;)F

    move-result v3

    add-float/2addr v1, v3

    float-to-int v3, v1

    .line 252
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/u;->c(Landroid/support/v7/widget/ce;)V

    .line 253
    sub-int v1, p4, v2

    .line 254
    sub-int v4, p5, v3

    .line 255
    if-nez v1, :cond_1

    if-nez v4, :cond_1

    .line 256
    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/bo;->c(Landroid/support/v7/widget/ce;)V

    .line 257
    :cond_0
    const/4 v0, 0x0

    .line 266
    :goto_0
    return v0

    .line 259
    :cond_1
    if-eqz v1, :cond_2

    .line 260
    neg-int v1, v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 262
    :cond_2
    if-eqz v4, :cond_3

    .line 263
    neg-int v1, v4

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 265
    :cond_3
    iget-object v6, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    new-instance v0, Landroid/support/v7/widget/ae;

    move-object v1, p1

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/ae;-><init>(Landroid/support/v7/widget/ce;IIII)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/ce;Landroid/support/v7/widget/ce;IIII)Z
    .locals 8

    .prologue
    .line 311
    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/at;->o(Landroid/view/View;)F

    move-result v0

    .line 312
    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/at;->p(Landroid/view/View;)F

    move-result v1

    .line 313
    iget-object v2, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/at;->f(Landroid/view/View;)F

    move-result v2

    .line 314
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/u;->c(Landroid/support/v7/widget/ce;)V

    .line 315
    sub-int v3, p5, p3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    float-to-int v3, v3

    .line 316
    sub-int v4, p6, p4

    int-to-float v4, v4

    sub-float/2addr v4, v1

    float-to-int v4, v4

    .line 318
    iget-object v5, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v5, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 319
    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 320
    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 321
    if-eqz p2, :cond_0

    iget-object v0, p2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 323
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/u;->c(Landroid/support/v7/widget/ce;)V

    .line 324
    iget-object v0, p2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    neg-int v1, v3

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 325
    iget-object v0, p2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    neg-int v1, v4

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 326
    iget-object v0, p2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 328
    :cond_0
    iget-object v7, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    new-instance v0, Landroid/support/v7/widget/ad;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/ad;-><init>(Landroid/support/v7/widget/ce;Landroid/support/v7/widget/ce;IIII)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/ce;)Z
    .locals 2

    .prologue
    .line 215
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/u;->c(Landroid/support/v7/widget/ce;)V

    .line 216
    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 217
    iget-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 530
    iget-object v0, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 531
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 532
    iget-object v0, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ae;

    .line 533
    iget-object v2, v0, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/ce;

    iget-object v2, v2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 534
    invoke-static {v2, v5}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 535
    invoke-static {v2, v5}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 536
    iget-object v0, v0, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/ce;

    iget-object v2, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v2, v0}, Landroid/support/v7/widget/bo;->c(Landroid/support/v7/widget/ce;)V

    .line 537
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 531
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 539
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 540
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    .line 541
    iget-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 542
    iget-object v2, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v2, v0}, Landroid/support/v7/widget/bo;->a(Landroid/support/v7/widget/ce;)V

    .line 543
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 540
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 545
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 546
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_5

    .line 547
    iget-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 548
    iget-object v2, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 549
    invoke-static {v2, v6}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 550
    iget-object v2, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v2, v0}, Landroid/support/v7/widget/bo;->b(Landroid/support/v7/widget/ce;)V

    .line 551
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 546
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 553
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 554
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_6

    .line 555
    iget-object v0, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ad;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/u;->a(Landroid/support/v7/widget/ad;)V

    .line 554
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 557
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 558
    invoke-virtual {p0}, Landroid/support/v7/widget/u;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 612
    :goto_4
    return-void

    .line 562
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 563
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_5
    if-ltz v3, :cond_b

    .line 564
    iget-object v0, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 565
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 566
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_6
    if-ltz v2, :cond_a

    .line 567
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ae;

    .line 568
    iget-object v4, v1, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/ce;

    .line 569
    iget-object v4, v4, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 570
    invoke-static {v4, v5}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 571
    invoke-static {v4, v5}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 572
    iget-object v1, v1, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/ce;

    iget-object v4, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v4, :cond_8

    iget-object v4, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v4, v1}, Landroid/support/v7/widget/bo;->c(Landroid/support/v7/widget/ce;)V

    .line 573
    :cond_8
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 574
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 575
    iget-object v1, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 566
    :cond_9
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_6

    .line 563
    :cond_a
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_5

    .line 579
    :cond_b
    iget-object v0, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 580
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_7
    if-ltz v3, :cond_f

    .line 581
    iget-object v0, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 582
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 583
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_8
    if-ltz v2, :cond_e

    .line 584
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ce;

    .line 585
    iget-object v4, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 586
    invoke-static {v4, v6}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 587
    iget-object v4, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v4, :cond_c

    iget-object v4, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v4, v1}, Landroid/support/v7/widget/bo;->b(Landroid/support/v7/widget/ce;)V

    .line 588
    :cond_c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 589
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 590
    iget-object v1, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 583
    :cond_d
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_8

    .line 580
    :cond_e
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_7

    .line 594
    :cond_f
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 595
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_9
    if-ltz v3, :cond_12

    .line 596
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 597
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 598
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_a
    if-ltz v2, :cond_11

    .line 599
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ad;

    invoke-direct {p0, v1}, Landroid/support/v7/widget/u;->a(Landroid/support/v7/widget/ad;)V

    .line 600
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 601
    iget-object v1, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 598
    :cond_10
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_a

    .line 595
    :cond_11
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_9

    .line 606
    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/u;->f:Ljava/util/ArrayList;

    invoke-static {v0}, Landroid/support/v7/widget/u;->a(Ljava/util/List;)V

    .line 607
    iget-object v0, p0, Landroid/support/v7/widget/u;->e:Ljava/util/ArrayList;

    invoke-static {v0}, Landroid/support/v7/widget/u;->a(Ljava/util/List;)V

    .line 608
    iget-object v0, p0, Landroid/support/v7/widget/u;->d:Ljava/util/ArrayList;

    invoke-static {v0}, Landroid/support/v7/widget/u;->a(Ljava/util/List;)V

    .line 609
    iget-object v0, p0, Landroid/support/v7/widget/u;->g:Ljava/util/ArrayList;

    invoke-static {v0}, Landroid/support/v7/widget/u;->a(Ljava/util/List;)V

    .line 611
    invoke-virtual {p0}, Landroid/support/v7/widget/u;->d()V

    goto/16 :goto_4
.end method

.method public final c(Landroid/support/v7/widget/ce;)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 422
    iget-object v4, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 424
    invoke-static {v4}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v1

    iget-object v0, v1, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v2, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v2, v1, v0}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;)V

    .line 426
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    .line 427
    iget-object v0, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ae;

    .line 428
    iget-object v0, v0, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/ce;

    if-ne v0, p1, :cond_2

    .line 429
    invoke-static {v4, v5}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 430
    invoke-static {v4, v5}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 431
    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/bo;->c(Landroid/support/v7/widget/ce;)V

    .line 432
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/u;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 426
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 435
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/u;->p:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1}, Landroid/support/v7/widget/u;->a(Ljava/util/List;Landroid/support/v7/widget/ce;)V

    .line 436
    iget-object v0, p0, Landroid/support/v7/widget/u;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 437
    invoke-static {v4, v6}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 438
    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/bo;->a(Landroid/support/v7/widget/ce;)V

    .line 440
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/u;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 441
    invoke-static {v4, v6}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 442
    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/bo;->b(Landroid/support/v7/widget/ce;)V

    .line 445
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_7

    .line 446
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 447
    invoke-direct {p0, v0, p1}, Landroid/support/v7/widget/u;->a(Ljava/util/List;Landroid/support/v7/widget/ce;)V

    .line 448
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 449
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 445
    :cond_6
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 452
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_2
    if-ltz v3, :cond_b

    .line 453
    iget-object v0, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 454
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_3
    if-ltz v2, :cond_9

    .line 455
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ae;

    .line 456
    iget-object v1, v1, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/ce;

    if-ne v1, p1, :cond_a

    .line 457
    invoke-static {v4, v5}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 458
    invoke-static {v4, v5}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 459
    iget-object v1, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v1, :cond_8

    iget-object v1, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v1, p1}, Landroid/support/v7/widget/bo;->c(Landroid/support/v7/widget/ce;)V

    .line 460
    :cond_8
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 461
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 462
    iget-object v0, p0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 452
    :cond_9
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    .line 454
    :cond_a
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_3

    .line 468
    :cond_b
    iget-object v0, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_e

    .line 469
    iget-object v0, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 470
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 471
    invoke-static {v4, v6}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 472
    iget-object v2, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v2, :cond_c

    iget-object v2, p0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v2, p1}, Landroid/support/v7/widget/bo;->b(Landroid/support/v7/widget/ce;)V

    .line 473
    :cond_c
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 474
    iget-object v0, p0, Landroid/support/v7/widget/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 468
    :cond_d
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    .line 480
    :cond_e
    iget-object v0, p0, Landroid/support/v7/widget/u;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 485
    iget-object v0, p0, Landroid/support/v7/widget/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 490
    iget-object v0, p0, Landroid/support/v7/widget/u;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 495
    iget-object v0, p0, Landroid/support/v7/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 499
    invoke-virtual {p0}, Landroid/support/v7/widget/u;->b()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {p0}, Landroid/support/v7/widget/u;->d()V

    .line 500
    :cond_f
    return-void
.end method
