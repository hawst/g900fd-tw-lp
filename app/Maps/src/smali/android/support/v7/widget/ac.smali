.class Landroid/support/v7/widget/ac;
.super Landroid/support/v7/widget/af;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v7/widget/ad;

.field final synthetic b:Landroid/support/v4/view/cj;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Landroid/support/v7/widget/u;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ad;Landroid/support/v4/view/cj;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Landroid/support/v7/widget/ac;->d:Landroid/support/v7/widget/u;

    iput-object p2, p0, Landroid/support/v7/widget/ac;->a:Landroid/support/v7/widget/ad;

    iput-object p3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v4/view/cj;

    iput-object p4, p0, Landroid/support/v7/widget/ac;->c:Landroid/view/View;

    invoke-direct {p0}, Landroid/support/v7/widget/af;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Landroid/support/v7/widget/ac;->d:Landroid/support/v7/widget/u;

    iget-object v0, p0, Landroid/support/v7/widget/ac;->a:Landroid/support/v7/widget/ad;

    iget-object v0, v0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    .line 369
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 372
    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v4/view/cj;

    const/4 v2, 0x0

    iget-object v0, v1, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v3, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v3, v1, v0, v2}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    .line 373
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ac;->c:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 374
    iget-object v0, p0, Landroid/support/v7/widget/ac;->c:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 375
    iget-object v0, p0, Landroid/support/v7/widget/ac;->c:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 376
    iget-object v0, p0, Landroid/support/v7/widget/ac;->d:Landroid/support/v7/widget/u;

    iget-object v1, p0, Landroid/support/v7/widget/ac;->a:Landroid/support/v7/widget/ad;

    iget-object v1, v1, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    iget-object v2, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/bo;->d(Landroid/support/v7/widget/ce;)V

    .line 377
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ac;->d:Landroid/support/v7/widget/u;

    iget-object v0, v0, Landroid/support/v7/widget/u;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/ac;->a:Landroid/support/v7/widget/ad;

    iget-object v1, v1, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 378
    iget-object v0, p0, Landroid/support/v7/widget/ac;->d:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->b()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->d()V

    .line 379
    :cond_2
    return-void
.end method
