.class Landroid/support/v7/widget/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/widget/o;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/l;)F
    .locals 2

    .prologue
    .line 57
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ch;

    iget v0, v0, Landroid/support/v7/widget/ch;->a:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public final a(Landroid/support/v7/widget/l;F)V
    .locals 2

    .prologue
    .line 36
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ch;

    iget v1, v0, Landroid/support/v7/widget/ch;->a:F

    cmpl-float v1, p2, v1

    if-eqz v1, :cond_0

    iput p2, v0, Landroid/support/v7/widget/ch;->a:F

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ch;->a(Landroid/graphics/Rect;)V

    invoke-virtual {v0}, Landroid/support/v7/widget/ch;->invalidateSelf()V

    .line 37
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/l;Landroid/content/Context;IFFF)V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Landroid/support/v7/widget/ch;

    invoke-direct {v0, p3, p4}, Landroid/support/v7/widget/ch;-><init>(IF)V

    .line 27
    invoke-interface {p1, v0}, Landroid/support/v7/widget/l;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v0, p1

    .line 28
    check-cast v0, Landroid/view/View;

    .line 29
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipToOutline(Z)V

    .line 30
    invoke-virtual {v0, p5}, Landroid/view/View;->setElevation(F)V

    .line 31
    invoke-virtual {p0, p1, p6}, Landroid/support/v7/widget/k;->b(Landroid/support/v7/widget/l;F)V

    .line 32
    return-void
.end method

.method public final b(Landroid/support/v7/widget/l;)F
    .locals 2

    .prologue
    .line 62
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ch;

    iget v0, v0, Landroid/support/v7/widget/ch;->a:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final b(Landroid/support/v7/widget/l;F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 45
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ch;

    invoke-interface {p1}, Landroid/support/v7/widget/l;->a()Z

    move-result v1

    invoke-interface {p1}, Landroid/support/v7/widget/l;->b()Z

    move-result v2

    iget v3, v0, Landroid/support/v7/widget/ch;->b:F

    cmpl-float v3, p2, v3

    if-nez v3, :cond_0

    iget-boolean v3, v0, Landroid/support/v7/widget/ch;->c:Z

    if-ne v3, v1, :cond_0

    iget-boolean v3, v0, Landroid/support/v7/widget/ch;->d:Z

    if-eq v3, v2, :cond_1

    :cond_0
    iput p2, v0, Landroid/support/v7/widget/ch;->b:F

    iput-boolean v1, v0, Landroid/support/v7/widget/ch;->c:Z

    iput-boolean v2, v0, Landroid/support/v7/widget/ch;->d:Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ch;->a(Landroid/graphics/Rect;)V

    invoke-virtual {v0}, Landroid/support/v7/widget/ch;->invalidateSelf()V

    .line 47
    :cond_1
    invoke-interface {p1}, Landroid/support/v7/widget/l;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1, v4, v4, v4, v4}, Landroid/support/v7/widget/l;->setShadowPadding(IIII)V

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_2
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ch;

    iget v1, v0, Landroid/support/v7/widget/ch;->b:F

    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ch;

    iget v0, v0, Landroid/support/v7/widget/ch;->a:F

    invoke-interface {p1}, Landroid/support/v7/widget/l;->b()Z

    move-result v2

    invoke-static {v1, v0, v2}, Landroid/support/v7/widget/ci;->b(FFZ)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface {p1}, Landroid/support/v7/widget/l;->b()Z

    move-result v3

    invoke-static {v1, v0, v3}, Landroid/support/v7/widget/ci;->a(FFZ)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-interface {p1, v2, v0, v2, v0}, Landroid/support/v7/widget/l;->setShadowPadding(IIII)V

    goto :goto_0
.end method

.method public final c(Landroid/support/v7/widget/l;)V
    .locals 1

    .prologue
    .line 97
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ch;

    iget v0, v0, Landroid/support/v7/widget/ch;->b:F

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/k;->b(Landroid/support/v7/widget/l;F)V

    .line 98
    return-void
.end method

.method public final c(Landroid/support/v7/widget/l;F)V
    .locals 0

    .prologue
    .line 72
    check-cast p1, Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setElevation(F)V

    .line 73
    return-void
.end method
