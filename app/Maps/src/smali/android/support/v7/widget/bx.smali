.class Landroid/support/v7/widget/bx;
.super Landroid/support/v7/widget/bm;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 2979
    iput-object p1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Landroid/support/v7/widget/bm;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 3031
    iget-object v0, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_0

    .line 3032
    iget-object v0, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->f:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 3037
    :goto_0
    return-void

    .line 3034
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->o:Z

    .line 3035
    iget-object v0, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2982
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView;->s:Z

    if-eqz v1, :cond_1

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call this method while RecyclerView is computing a layout or scrolling"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2983
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v1, v1, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v1, :cond_3

    .line 2987
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v0, v1, Landroid/support/v7/widget/cc;->h:Z

    .line 2988
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v0, v1, Landroid/support/v7/widget/RecyclerView;->r:Z

    .line 2993
    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    iget-object v1, v1, Landroid/support/v7/widget/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    :goto_1
    if-nez v0, :cond_2

    .line 2994
    iget-object v0, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 2996
    :cond_2
    return-void

    .line 2990
    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v0, v1, Landroid/support/v7/widget/cc;->h:Z

    .line 2991
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v0, v1, Landroid/support/v7/widget/RecyclerView;->r:Z

    goto :goto_0

    .line 2993
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(II)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 3000
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView;->s:Z

    if-eqz v1, :cond_1

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call this method while RecyclerView is computing a layout or scrolling"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3001
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    iget-object v2, v1, Landroid/support/v7/widget/h;->a:Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, p1, p2}, Landroid/support/v7/widget/h;->a(III)Landroid/support/v7/widget/j;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, v1, Landroid/support/v7/widget/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v0, :cond_3

    :goto_0
    if-eqz v0, :cond_2

    .line 3002
    invoke-direct {p0}, Landroid/support/v7/widget/bx;->b()V

    .line 3004
    :cond_2
    return-void

    .line 3001
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(II)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3008
    iget-object v2, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v3, 0x0

    iget-boolean v2, v2, Landroid/support/v7/widget/RecyclerView;->s:Z

    if-eqz v2, :cond_1

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call this method while RecyclerView is computing a layout or scrolling"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3009
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/bx;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    iget-object v3, v2, Landroid/support/v7/widget/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, p1, p2}, Landroid/support/v7/widget/h;->a(III)Landroid/support/v7/widget/j;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v2, Landroid/support/v7/widget/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v0, :cond_3

    :goto_0
    if-eqz v0, :cond_2

    .line 3010
    invoke-direct {p0}, Landroid/support/v7/widget/bx;->b()V

    .line 3012
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 3009
    goto :goto_0
.end method
