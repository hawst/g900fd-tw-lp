.class Landroid/support/v7/widget/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Landroid/support/v7/widget/u;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/u;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Landroid/support/v7/widget/v;->b:Landroid/support/v7/widget/u;

    iput-object p2, p0, Landroid/support/v7/widget/v;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 119
    iget-object v0, p0, Landroid/support/v7/widget/v;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ae;

    .line 120
    iget-object v1, p0, Landroid/support/v7/widget/v;->b:Landroid/support/v7/widget/u;

    iget-object v2, v0, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/ce;

    iget v3, v0, Landroid/support/v7/widget/ae;->b:I

    iget v4, v0, Landroid/support/v7/widget/ae;->c:I

    iget v5, v0, Landroid/support/v7/widget/ae;->d:I

    iget v0, v0, Landroid/support/v7/widget/ae;->e:I

    iget-object v7, v2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    sub-int v3, v5, v3

    sub-int v4, v0, v4

    if-eqz v3, :cond_1

    invoke-static {v7}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v5

    iget-object v0, v5, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    sget-object v8, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v8, v5, v0, v10}, Landroid/support/v4/view/cr;->b(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_1
    if-eqz v4, :cond_2

    invoke-static {v7}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v5

    iget-object v0, v5, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_2

    sget-object v8, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v8, v5, v0, v10}, Landroid/support/v4/view/cr;->c(Landroid/support/v4/view/cj;Landroid/view/View;F)V

    :cond_2
    iget-object v0, v1, Landroid/support/v7/widget/u;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v7}, Landroid/support/v4/view/at;->s(Landroid/view/View;)Landroid/support/v4/view/cj;

    move-result-object v5

    iget-wide v8, v1, Landroid/support/v7/widget/bn;->k:J

    iget-object v0, v5, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_3

    sget-object v7, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v7, v0, v8, v9}, Landroid/support/v4/view/cr;->a(Landroid/view/View;J)V

    :cond_3
    new-instance v0, Landroid/support/v7/widget/aa;

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/aa;-><init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ce;IILandroid/support/v4/view/cj;)V

    iget-object v1, v5, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_4

    sget-object v2, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v2, v5, v1, v0}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    :cond_4
    iget-object v0, v5, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v1, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v1, v5, v0}, Landroid/support/v4/view/cr;->b(Landroid/support/v4/view/cj;Landroid/view/View;)V

    goto/16 :goto_0

    .line 123
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/v;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 124
    iget-object v0, p0, Landroid/support/v7/widget/v;->b:Landroid/support/v7/widget/u;

    iget-object v0, v0, Landroid/support/v7/widget/u;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/v;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 125
    return-void
.end method
