.class Landroid/support/v7/widget/bj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/widget/i;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 410
    iput-object p1, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c(Landroid/support/v7/widget/j;)V
    .locals 3

    .prologue
    .line 441
    iget v0, p1, Landroid/support/v7/widget/j;->a:I

    packed-switch v0, :pswitch_data_0

    .line 455
    :goto_0
    return-void

    .line 443
    :pswitch_0
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget v1, p1, Landroid/support/v7/widget/j;->b:I

    iget v2, p1, Landroid/support/v7/widget/j;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bs;->a(II)V

    goto :goto_0

    .line 446
    :pswitch_1
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget v1, p1, Landroid/support/v7/widget/j;->b:I

    iget v2, p1, Landroid/support/v7/widget/j;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bs;->b(II)V

    goto :goto_0

    .line 449
    :pswitch_2
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget v1, p1, Landroid/support/v7/widget/j;->b:I

    iget v2, p1, Landroid/support/v7/widget/j;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bs;->c(II)V

    goto :goto_0

    .line 452
    :pswitch_3
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget v1, p1, Landroid/support/v7/widget/j;->b:I

    iget v2, p1, Landroid/support/v7/widget/j;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bs;->d(II)V

    goto :goto_0

    .line 441
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(I)Landroid/support/v7/widget/ce;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 413
    iget-object v4, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0}, Landroid/support/v7/widget/t;->a()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_2

    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v3, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    if-nez v3, :cond_1

    iget v3, v0, Landroid/support/v7/widget/ce;->b:I

    if-ne v3, p1, :cond_1

    :goto_2
    return-object v0

    :cond_0
    move v3, v2

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 418
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v7/widget/RecyclerView;->a(IIZ)V

    .line 419
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 420
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget v1, v0, Landroid/support/v7/widget/cc;->g:I

    add-int/2addr v1, p2

    iput v1, v0, Landroid/support/v7/widget/cc;->g:I

    .line 421
    return-void
.end method

.method public final a(Landroid/support/v7/widget/j;)V
    .locals 0

    .prologue
    .line 437
    invoke-direct {p0, p1}, Landroid/support/v7/widget/bj;->c(Landroid/support/v7/widget/j;)V

    .line 438
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v7/widget/RecyclerView;->a(IIZ)V

    .line 426
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 427
    return-void
.end method

.method public final b(Landroid/support/v7/widget/j;)V
    .locals 0

    .prologue
    .line 459
    invoke-direct {p0, p1}, Landroid/support/v7/widget/bj;->c(Landroid/support/v7/widget/j;)V

    .line 460
    return-void
.end method

.method public final c(II)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 431
    iget-object v4, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0}, Landroid/support/v7/widget/t;->a()I

    move-result v5

    add-int v6, p1, p2

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_3

    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v2}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v7

    invoke-static {v7}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v8

    if-eqz v8, :cond_1

    iget v0, v8, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_1

    iget v0, v8, Landroid/support/v7/widget/ce;->b:I

    if-lt v0, p1, :cond_1

    iget v0, v8, Landroid/support/v7/widget/ce;->b:I

    if-ge v0, v6, :cond_1

    iget v0, v8, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v0, v9

    iput v0, v8, Landroid/support/v7/widget/ce;->i:I

    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v0, :cond_0

    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    :cond_0
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->e:Z

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    add-int v5, p1, p2

    iget-object v0, v4, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_6

    iget-object v0, v4, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_4

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v7, -0x1

    if-ne v1, v7, :cond_5

    iget v1, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_3
    if-lt v1, p1, :cond_4

    if-ge v1, v5, :cond_4

    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v1, v9

    iput v1, v0, Landroid/support/v7/widget/ce;->i:I

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_5
    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_3

    .line 432
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->D:Z

    .line 433
    return-void
.end method

.method public final d(II)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 464
    iget-object v4, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v5

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    iget-object v2, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v2, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v6

    if-eqz v6, :cond_0

    iget v2, v6, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    if-nez v2, :cond_0

    iget v2, v6, Landroid/support/v7/widget/ce;->b:I

    if-lt v2, p1, :cond_0

    invoke-virtual {v6, p2, v0}, Landroid/support/v7/widget/ce;->a(IZ)V

    iget-object v2, v4, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v3, v2, Landroid/support/v7/widget/cc;->h:Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v2, v0

    goto :goto_1

    :cond_2
    iget-object v5, v4, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v1, v5, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v0

    :goto_2
    if-ge v2, v6, :cond_5

    iget-object v0, v5, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_3

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v7, -0x1

    if-ne v1, v7, :cond_4

    iget v1, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_3
    if-lt v1, p1, :cond_3

    invoke-virtual {v0, p2, v3}, Landroid/support/v7/widget/ce;->a(IZ)V

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_3

    :cond_5
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 465
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 466
    return-void
.end method

.method public final e(II)V
    .locals 11

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 470
    iget-object v7, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v7, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0}, Landroid/support/v7/widget/t;->a()I

    move-result v8

    if-ge p1, p2, :cond_1

    move v0, v1

    move v3, p2

    move v4, p1

    :goto_0
    move v5, v6

    :goto_1
    if-ge v5, v8, :cond_3

    iget-object v9, v7, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v9, v9, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v9, v5}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v9

    invoke-static {v9}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v9

    if-eqz v9, :cond_0

    iget v10, v9, Landroid/support/v7/widget/ce;->b:I

    if-lt v10, v4, :cond_0

    iget v10, v9, Landroid/support/v7/widget/ce;->b:I

    if-gt v10, v3, :cond_0

    iget v10, v9, Landroid/support/v7/widget/ce;->b:I

    if-ne v10, p1, :cond_2

    sub-int v10, p2, p1

    invoke-virtual {v9, v10, v6}, Landroid/support/v7/widget/ce;->a(IZ)V

    :goto_2
    iget-object v9, v7, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v2, v9, Landroid/support/v7/widget/cc;->h:Z

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    move v3, p1

    move v4, p2

    goto :goto_0

    :cond_2
    invoke-virtual {v9, v0, v6}, Landroid/support/v7/widget/ce;->a(IZ)V

    goto :goto_2

    :cond_3
    iget-object v8, v7, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    if-ge p1, p2, :cond_5

    move v3, p2

    move v4, p1

    :goto_3
    iget-object v0, v8, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v5, v6

    :goto_4
    if-ge v5, v9, :cond_7

    iget-object v0, v8, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_4

    iget v10, v0, Landroid/support/v7/widget/ce;->b:I

    if-lt v10, v4, :cond_4

    iget v10, v0, Landroid/support/v7/widget/ce;->b:I

    if-gt v10, v3, :cond_4

    iget v10, v0, Landroid/support/v7/widget/ce;->b:I

    if-ne v10, p1, :cond_6

    sub-int v10, p2, p1

    invoke-virtual {v0, v10, v6}, Landroid/support/v7/widget/ce;->a(IZ)V

    :cond_4
    :goto_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_4

    :cond_5
    move v1, v2

    move v3, p1

    move v4, p2

    goto :goto_3

    :cond_6
    invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ce;->a(IZ)V

    goto :goto_5

    :cond_7
    invoke-virtual {v7}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 472
    iget-object v0, p0, Landroid/support/v7/widget/bj;->a:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v2, v0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 473
    return-void
.end method
