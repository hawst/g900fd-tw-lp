.class Landroid/support/v7/widget/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/widget/o;


# instance fields
.field final a:Landroid/graphics/RectF;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/m;->a:Landroid/graphics/RectF;

    return-void
.end method

.method private d(Landroid/support/v7/widget/l;)V
    .locals 4

    .prologue
    .line 92
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 93
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ci;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ci;->getPadding(Landroid/graphics/Rect;)Z

    move-object v0, p1

    .line 94
    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/m;->b(Landroid/support/v7/widget/l;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumHeight(I)V

    move-object v0, p1

    .line 95
    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/m;->a(Landroid/support/v7/widget/l;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumWidth(I)V

    .line 96
    iget v0, v1, Landroid/graphics/Rect;->left:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-interface {p1, v0, v2, v3, v1}, Landroid/support/v7/widget/l;->setShadowPadding(IIII)V

    .line 98
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/l;)F
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 150
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ci;

    iget v1, v0, Landroid/support/v7/widget/ci;->j:F

    iget v2, v0, Landroid/support/v7/widget/ci;->h:F

    iget v3, v0, Landroid/support/v7/widget/ci;->b:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v0, Landroid/support/v7/widget/ci;->j:F

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v4

    iget v2, v0, Landroid/support/v7/widget/ci;->j:F

    iget v0, v0, Landroid/support/v7/widget/ci;->b:I

    int-to-float v0, v0

    add-float/2addr v0, v2

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Landroid/support/v7/widget/n;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/n;-><init>(Landroid/support/v7/widget/m;)V

    sput-object v0, Landroid/support/v7/widget/ci;->c:Landroid/support/v7/widget/cj;

    .line 72
    return-void
.end method

.method public final a(Landroid/support/v7/widget/l;F)V
    .locals 3

    .prologue
    .line 118
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ci;

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v1, p2

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, v0, Landroid/support/v7/widget/ci;->h:F

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_0

    iput v1, v0, Landroid/support/v7/widget/ci;->h:F

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/ci;->m:Z

    invoke-virtual {v0}, Landroid/support/v7/widget/ci;->invalidateSelf()V

    .line 119
    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v7/widget/m;->d(Landroid/support/v7/widget/l;)V

    .line 120
    return-void
.end method

.method public final a(Landroid/support/v7/widget/l;Landroid/content/Context;IFFF)V
    .locals 6

    .prologue
    .line 77
    new-instance v0, Landroid/support/v7/widget/ci;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/ci;-><init>(Landroid/content/res/Resources;IFFF)V

    .line 79
    invoke-interface {p1}, Landroid/support/v7/widget/l;->b()Z

    move-result v1

    iput-boolean v1, v0, Landroid/support/v7/widget/ci;->n:Z

    invoke-virtual {v0}, Landroid/support/v7/widget/ci;->invalidateSelf()V

    .line 80
    invoke-interface {p1, v0}, Landroid/support/v7/widget/l;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 81
    invoke-direct {p0, p1}, Landroid/support/v7/widget/m;->d(Landroid/support/v7/widget/l;)V

    .line 82
    return-void
.end method

.method public final b(Landroid/support/v7/widget/l;)F
    .locals 6

    .prologue
    const/high16 v5, 0x3fc00000    # 1.5f

    const/high16 v4, 0x40000000    # 2.0f

    .line 155
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ci;

    iget v1, v0, Landroid/support/v7/widget/ci;->j:F

    iget v2, v0, Landroid/support/v7/widget/ci;->h:F

    iget v3, v0, Landroid/support/v7/widget/ci;->b:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v0, Landroid/support/v7/widget/ci;->j:F

    mul-float/2addr v3, v5

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v4

    iget v2, v0, Landroid/support/v7/widget/ci;->j:F

    mul-float/2addr v2, v5

    iget v0, v0, Landroid/support/v7/widget/ci;->b:I

    int-to-float v0, v0

    add-float/2addr v0, v2

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    return v0
.end method

.method public final b(Landroid/support/v7/widget/l;F)V
    .locals 2

    .prologue
    .line 139
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ci;

    iget v1, v0, Landroid/support/v7/widget/ci;->l:F

    invoke-virtual {v0, v1, p2}, Landroid/support/v7/widget/ci;->a(FF)V

    .line 140
    invoke-direct {p0, p1}, Landroid/support/v7/widget/m;->d(Landroid/support/v7/widget/l;)V

    .line 141
    return-void
.end method

.method public final c(Landroid/support/v7/widget/l;)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public final c(Landroid/support/v7/widget/l;F)V
    .locals 2

    .prologue
    .line 129
    invoke-interface {p1}, Landroid/support/v7/widget/l;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ci;

    iget v1, v0, Landroid/support/v7/widget/ci;->j:F

    invoke-virtual {v0, p2, v1}, Landroid/support/v7/widget/ci;->a(FF)V

    .line 130
    return-void
.end method
