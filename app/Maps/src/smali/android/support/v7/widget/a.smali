.class Landroid/support/v7/widget/a;
.super Landroid/support/v7/internal/view/menu/h;
.source "PG"


# instance fields
.field final synthetic j:Landroid/support/v7/widget/ActionMenuPresenter;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;Landroid/support/v7/internal/view/menu/n;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 686
    iput-object p1, p0, Landroid/support/v7/widget/a;->j:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 687
    sget v5, Landroid/support/v7/a/b;->j:I

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/internal/view/menu/h;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/e;Landroid/view/View;ZI)V

    .line 689
    invoke-virtual {p3}, Landroid/support/v7/internal/view/menu/n;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/g;

    .line 692
    iget v0, v0, Landroid/support/v7/internal/view/menu/g;->f:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    move v0, v6

    :goto_0
    if-nez v0, :cond_0

    .line 694
    iget-object v0, p1, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    if-nez v0, :cond_3

    check-cast v3, Landroid/view/View;

    :goto_1
    iput-object v3, p0, Landroid/support/v7/internal/view/menu/h;->e:Landroid/view/View;

    .line 697
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/widget/f;

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/h;->g:Landroid/support/v7/internal/view/menu/k;

    .line 700
    invoke-virtual {p3}, Landroid/support/v7/internal/view/menu/n;->size()I

    move-result v1

    move v0, v4

    .line 701
    :goto_2
    if-ge v0, v1, :cond_1

    .line 702
    invoke-virtual {p3, v0}, Landroid/support/v7/internal/view/menu/n;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 703
    invoke-interface {v2}, Landroid/view/MenuItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_4

    move v4, v6

    .line 708
    :cond_1
    iput-boolean v4, p0, Landroid/support/v7/internal/view/menu/h;->h:Z

    .line 709
    return-void

    :cond_2
    move v0, v4

    .line 692
    goto :goto_0

    .line 694
    :cond_3
    iget-object v3, p1, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    goto :goto_1

    .line 701
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public onDismiss()V
    .locals 2

    .prologue
    .line 713
    invoke-super {p0}, Landroid/support/v7/internal/view/menu/h;->onDismiss()V

    .line 714
    iget-object v0, p0, Landroid/support/v7/widget/a;->j:Landroid/support/v7/widget/ActionMenuPresenter;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->e:Landroid/support/v7/widget/a;

    .line 715
    iget-object v0, p0, Landroid/support/v7/widget/a;->j:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 716
    return-void
.end method
