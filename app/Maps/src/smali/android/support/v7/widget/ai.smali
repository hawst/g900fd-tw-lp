.class public Landroid/support/v7/widget/ai;
.super Landroid/support/v7/widget/bs;
.source "PG"


# instance fields
.field private a:Landroid/support/v7/widget/am;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field f:I

.field g:Landroid/support/v7/widget/bc;

.field h:Z

.field i:I

.field j:I

.field k:Landroid/support/v7/widget/an;

.field final l:Landroid/support/v7/widget/ak;


# direct methods
.method public constructor <init>(IZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 150
    invoke-direct {p0}, Landroid/support/v7/widget/bs;-><init>()V

    .line 91
    iput-boolean v0, p0, Landroid/support/v7/widget/ai;->c:Z

    .line 98
    iput-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    .line 105
    iput-boolean v0, p0, Landroid/support/v7/widget/ai;->d:Z

    .line 111
    iput-boolean v2, p0, Landroid/support/v7/widget/ai;->e:Z

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/ai;->i:I

    .line 123
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/ai;->j:I

    .line 127
    iput-object v1, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    .line 151
    new-instance v0, Landroid/support/v7/widget/ak;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ak;-><init>(Landroid/support/v7/widget/ai;)V

    iput-object v0, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    .line 152
    if-eqz p1, :cond_0

    if-eq p1, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid orientation:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    if-nez v0, :cond_1

    invoke-super {p0, v1}, Landroid/support/v7/widget/bs;->a(Ljava/lang/String;)V

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/ai;->f:I

    if-eq p1, v0, :cond_2

    iput p1, p0, Landroid/support/v7/widget/ai;->f:I

    iput-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 153
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    if-nez v0, :cond_3

    invoke-super {p0, v1}, Landroid/support/v7/widget/bs;->a(Ljava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->c:Z

    if-eq p2, v0, :cond_4

    iput-boolean p2, p0, Landroid/support/v7/widget/ai;->c:Z

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 154
    :cond_4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 141
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/ai;-><init>(IZ)V

    .line 142
    return-void
.end method

.method private a(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)I
    .locals 3

    .prologue
    .line 795
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    sub-int/2addr v0, p1

    .line 796
    if-lez v0, :cond_1

    .line 798
    neg-int v0, v0

    invoke-direct {p0, v0, p2, p3}, Landroid/support/v7/widget/ai;->d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    neg-int v0, v0

    .line 803
    add-int v1, p1, v0

    .line 804
    if-eqz p4, :cond_0

    .line 806
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->b()I

    move-result v2

    sub-int v1, v2, v1

    .line 807
    if-lez v1, :cond_0

    .line 808
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/bc;->a(I)V

    .line 809
    add-int/2addr v0, v1

    .line 812
    :cond_0
    :goto_0
    return v0

    .line 800
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v2, 0x0

    .line 1257
    iget v3, p2, Landroid/support/v7/widget/am;->c:I

    .line 1258
    iget v0, p2, Landroid/support/v7/widget/am;->g:I

    if-eq v0, v7, :cond_1

    .line 1260
    iget v0, p2, Landroid/support/v7/widget/am;->c:I

    if-gez v0, :cond_0

    .line 1261
    iget v0, p2, Landroid/support/v7/widget/am;->g:I

    iget v1, p2, Landroid/support/v7/widget/am;->c:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/support/v7/widget/am;->g:I

    .line 1263
    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;)V

    .line 1265
    :cond_1
    iget v0, p2, Landroid/support/v7/widget/am;->c:I

    iget v1, p2, Landroid/support/v7/widget/am;->h:I

    add-int/2addr v0, v1

    .line 1266
    new-instance v4, Landroid/support/v7/widget/al;

    invoke-direct {v4}, Landroid/support/v7/widget/al;-><init>()V

    .line 1267
    :cond_2
    if-lez v0, :cond_7

    iget v1, p2, Landroid/support/v7/widget/am;->d:I

    if-ltz v1, :cond_9

    iget v5, p2, Landroid/support/v7/widget/am;->d:I

    iget-boolean v1, p3, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v1, :cond_8

    iget v1, p3, Landroid/support/v7/widget/cc;->f:I

    iget v6, p3, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v1, v6

    :goto_0
    if-ge v5, v1, :cond_9

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_7

    .line 1268
    iput v2, v4, Landroid/support/v7/widget/al;->a:I

    iput-boolean v2, v4, Landroid/support/v7/widget/al;->b:Z

    iput-boolean v2, v4, Landroid/support/v7/widget/al;->c:Z

    iput-boolean v2, v4, Landroid/support/v7/widget/al;->d:Z

    .line 1269
    invoke-virtual {p0, p1, p3, p2, v4}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Landroid/support/v7/widget/am;Landroid/support/v7/widget/al;)V

    .line 1270
    iget-boolean v1, v4, Landroid/support/v7/widget/al;->b:Z

    if-nez v1, :cond_7

    .line 1271
    iget v1, p2, Landroid/support/v7/widget/am;->b:I

    iget v5, v4, Landroid/support/v7/widget/al;->a:I

    iget v6, p2, Landroid/support/v7/widget/am;->f:I

    mul-int/2addr v5, v6

    add-int/2addr v1, v5

    iput v1, p2, Landroid/support/v7/widget/am;->b:I

    .line 1280
    iget-boolean v1, v4, Landroid/support/v7/widget/al;->c:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-object v1, v1, Landroid/support/v7/widget/am;->j:Ljava/util/List;

    if-nez v1, :cond_3

    iget-boolean v1, p3, Landroid/support/v7/widget/cc;->i:Z

    if-nez v1, :cond_4

    .line 1282
    :cond_3
    iget v1, p2, Landroid/support/v7/widget/am;->c:I

    iget v5, v4, Landroid/support/v7/widget/al;->a:I

    sub-int/2addr v1, v5

    iput v1, p2, Landroid/support/v7/widget/am;->c:I

    .line 1284
    iget v1, v4, Landroid/support/v7/widget/al;->a:I

    sub-int/2addr v0, v1

    .line 1287
    :cond_4
    iget v1, p2, Landroid/support/v7/widget/am;->g:I

    if-eq v1, v7, :cond_6

    .line 1288
    iget v1, p2, Landroid/support/v7/widget/am;->g:I

    iget v5, v4, Landroid/support/v7/widget/al;->a:I

    add-int/2addr v1, v5

    iput v1, p2, Landroid/support/v7/widget/am;->g:I

    .line 1289
    iget v1, p2, Landroid/support/v7/widget/am;->c:I

    if-gez v1, :cond_5

    .line 1290
    iget v1, p2, Landroid/support/v7/widget/am;->g:I

    iget v5, p2, Landroid/support/v7/widget/am;->c:I

    add-int/2addr v1, v5

    iput v1, p2, Landroid/support/v7/widget/am;->g:I

    .line 1292
    :cond_5
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;)V

    .line 1294
    :cond_6
    if-eqz p4, :cond_2

    iget-boolean v1, v4, Landroid/support/v7/widget/al;->d:Z

    if-eqz v1, :cond_2

    .line 1295
    :cond_7
    iget v0, p2, Landroid/support/v7/widget/am;->c:I

    sub-int v0, v3, v0

    return v0

    .line 1267
    :cond_8
    iget v1, p3, Landroid/support/v7/widget/cc;->e:I

    goto :goto_0

    :cond_9
    move v1, v2

    goto :goto_1
.end method

.method private a(III)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v7, 0x0

    .line 1476
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 1479
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v8

    .line 1480
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v9

    .line 1481
    if-le p2, p1, :cond_0

    move v1, v2

    :goto_0
    move-object v4, v7

    move-object v6, v7

    .line 1482
    :goto_1
    if-eq p1, p2, :cond_6

    .line 1483
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/r;->a(I)I

    move-result v5

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v5}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v5

    .line 1484
    :goto_2
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v10, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v10, v3, :cond_2

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    .line 1485
    :goto_3
    if-ltz v0, :cond_9

    if-ge v0, p3, :cond_9

    .line 1486
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v0, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    move v0, v2

    :goto_4
    if-eqz v0, :cond_4

    .line 1487
    if-nez v6, :cond_9

    move-object v0, v4

    .line 1482
    :goto_5
    add-int/2addr p1, v1

    move-object v4, v0

    move-object v6, v5

    goto :goto_1

    :cond_0
    move v1, v3

    .line 1481
    goto :goto_0

    :cond_1
    move-object v5, v7

    .line 1483
    goto :goto_2

    .line 1484
    :cond_2
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_3

    .line 1486
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 1490
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v0

    if-ge v0, v9, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v0

    if-ge v0, v8, :cond_7

    .line 1492
    :cond_5
    if-nez v4, :cond_9

    move-object v0, v5

    move-object v5, v6

    .line 1493
    goto :goto_5

    .line 1500
    :cond_6
    if-eqz v4, :cond_8

    move-object v5, v4

    :cond_7
    :goto_6
    return-object v5

    :cond_8
    move-object v5, v6

    goto :goto_6

    :cond_9
    move-object v0, v4

    move-object v5, v6

    goto :goto_5
.end method

.method private a(IIZ)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1580
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 1581
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v3

    .line 1582
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v4

    .line 1583
    if-le p2, p1, :cond_1

    const/4 v0, 0x1

    .line 1584
    :goto_0
    if-eq p1, p2, :cond_4

    .line 1585
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/r;->a(I)I

    move-result v5

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v5}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v1

    .line 1586
    :goto_1
    iget-object v5, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v5, v1}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v5

    .line 1587
    iget-object v6, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v6, v1}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v6

    .line 1588
    if-ge v5, v4, :cond_3

    if-le v6, v3, :cond_3

    .line 1589
    if-eqz p3, :cond_0

    .line 1590
    if-lt v5, v3, :cond_3

    if-gt v6, v4, :cond_3

    .line 1598
    :cond_0
    :goto_2
    return-object v1

    .line 1583
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 1585
    goto :goto_1

    .line 1584
    :cond_3
    add-int/2addr p1, v0

    goto :goto_0

    :cond_4
    move-object v1, v2

    .line 1598
    goto :goto_2
.end method

.method private a(IIZLandroid/support/v7/widget/cc;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 1054
    iget-object v4, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p4}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/cc;)I

    move-result v5

    iput v5, v4, Landroid/support/v7/widget/am;->h:I

    .line 1055
    iget-object v4, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput p1, v4, Landroid/support/v7/widget/am;->f:I

    .line 1057
    if-ne p1, v3, :cond_6

    .line 1058
    iget-object v4, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v5, v4, Landroid/support/v7/widget/am;->h:I

    iget-object v6, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v6}, Landroid/support/v7/widget/bc;->e()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/support/v7/widget/am;->h:I

    .line 1060
    iget-boolean v4, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v4, :cond_2

    :goto_0
    iget-object v4, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v4, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 1062
    :cond_0
    iget-object v4, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput v0, v4, Landroid/support/v7/widget/am;->e:I

    .line 1064
    iget-object v3, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v4, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v4, v2, :cond_5

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_2
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->e:I

    add-int/2addr v0, v2

    iput v0, v3, Landroid/support/v7/widget/am;->d:I

    .line 1065
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/am;->b:I

    .line 1067
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->b()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1080
    :goto_3
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput p2, v1, Landroid/support/v7/widget/am;->c:I

    .line 1081
    if-eqz p3, :cond_1

    .line 1082
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v1, Landroid/support/v7/widget/am;->c:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/support/v7/widget/am;->c:I

    .line 1084
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v0, v1, Landroid/support/v7/widget/am;->g:I

    .line 1085
    return-void

    .line 1060
    :cond_2
    iget-object v4, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v4, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v4, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v4, v0

    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_4
    move v0, v3

    .line 1062
    goto :goto_1

    .line 1064
    :cond_5
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_2

    .line 1071
    :cond_6
    iget-boolean v4, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v4, :cond_8

    iget-object v4, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v4, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v4, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v4, v0

    :cond_7
    add-int/lit8 v0, v0, -0x1

    :cond_8
    iget-object v4, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v4, :cond_9

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v1

    .line 1072
    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v4, v0, Landroid/support/v7/widget/am;->h:I

    iget-object v5, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v5}, Landroid/support/v7/widget/bc;->a()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v0, Landroid/support/v7/widget/am;->h:I

    .line 1073
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-boolean v4, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v4, :cond_a

    :goto_4
    iput v3, v0, Landroid/support/v7/widget/am;->e:I

    .line 1075
    iget-object v3, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v4, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v4, v2, :cond_b

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_5
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->e:I

    add-int/2addr v0, v2

    iput v0, v3, Landroid/support/v7/widget/am;->d:I

    .line 1076
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/am;->b:I

    .line 1077
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->a()I

    move-result v1

    add-int/2addr v0, v1

    goto/16 :goto_3

    :cond_a
    move v3, v2

    .line 1073
    goto :goto_4

    .line 1075
    :cond_b
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_5
.end method

.method private a(Landroid/support/v7/widget/bw;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1126
    if-ne p2, p3, :cond_1

    .line 1141
    :cond_0
    return-void

    .line 1132
    :cond_1
    if-le p3, p2, :cond_4

    .line 1133
    add-int/lit8 v0, p3, -0x1

    move v2, v0

    :goto_0
    if-lt v2, p2, :cond_0

    .line 1134
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/r;->a(I)I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v3}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/bs;->d(I)V

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/bw;->a(Landroid/view/View;)V

    .line 1133
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1134
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1138
    :goto_2
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/bs;->d(I)V

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/bw;->a(Landroid/view/View;)V

    .line 1137
    add-int/lit8 p2, p2, -0x1

    :cond_4
    if-le p2, p3, :cond_0

    .line 1138
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/r;->a(I)I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v2}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    goto :goto_2
.end method

.method private a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1233
    iget-boolean v0, p2, Landroid/support/v7/widget/am;->a:Z

    if-nez v0, :cond_1

    .line 1241
    :cond_0
    :goto_0
    return-void

    .line 1236
    :cond_1
    iget v0, p2, Landroid/support/v7/widget/am;->f:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_8

    .line 1237
    iget v3, p2, Landroid/support/v7/widget/am;->g:I

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v4, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v4, v0

    move v4, v0

    :goto_1
    if-ltz v3, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->c()I

    move-result v0

    sub-int v5, v0, v3

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_5

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v6

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v6}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_3
    iget-object v6, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v0

    if-ge v0, v5, :cond_4

    invoke-direct {p0, p1, v2, v3}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;II)V

    goto :goto_0

    :cond_2
    move v4, v2

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    add-int/lit8 v0, v4, -0x1

    move v2, v0

    :goto_4
    if-ltz v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/r;->a(I)I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v3}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_5
    iget-object v3, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v0

    if-ge v0, v5, :cond_7

    add-int/lit8 v0, v4, -0x1

    invoke-direct {p0, p1, v0, v2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;II)V

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_5

    :cond_7
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_4

    .line 1239
    :cond_8
    iget v5, p2, Landroid/support/v7/widget/am;->g:I

    if-ltz v5, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v3, v0

    :goto_6
    iget-boolean v3, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v3, :cond_c

    add-int/lit8 v2, v0, -0x1

    move v3, v2

    :goto_7
    if-ltz v3, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_a

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v4

    iget-object v2, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v4}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v2

    :goto_8
    iget-object v4, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v4, v2}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v2

    if-le v2, v5, :cond_b

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, v0, v3}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;II)V

    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto :goto_6

    :cond_a
    move-object v2, v1

    goto :goto_8

    :cond_b
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_7

    :cond_c
    move v4, v2

    :goto_9
    if-ge v4, v0, :cond_0

    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v3, :cond_d

    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/r;->a(I)I

    move-result v6

    iget-object v3, v3, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3, v6}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v3

    :goto_a
    iget-object v6, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v6, v3}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v3

    if-le v3, v5, :cond_e

    invoke-direct {p0, p1, v2, v4}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;II)V

    goto/16 :goto_0

    :cond_d
    move-object v3, v1

    goto :goto_a

    :cond_e
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_9
.end method

.method private b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)I
    .locals 4

    .prologue
    .line 820
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v0

    sub-int v0, p1, v0

    .line 821
    if-lez v0, :cond_1

    .line 824
    invoke-direct {p0, v0, p2, p3}, Landroid/support/v7/widget/ai;->d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    neg-int v0, v0

    .line 828
    add-int v1, p1, v0

    .line 829
    if-eqz p4, :cond_0

    .line 831
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    sub-int/2addr v1, v2

    .line 832
    if-lez v1, :cond_0

    .line 833
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    neg-int v3, v1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/bc;->a(I)V

    .line 834
    sub-int/2addr v0, v1

    .line 837
    :cond_0
    :goto_0
    return v0

    .line 826
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1088
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v3, v0

    :goto_0
    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move p1, v2

    .line 1109
    :goto_1
    return p1

    :cond_1
    move v0, v2

    .line 1088
    goto :goto_0

    .line 1091
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput-boolean v1, v0, Landroid/support/v7/widget/am;->a:Z

    .line 1092
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 1093
    if-lez p1, :cond_3

    move v0, v1

    .line 1094
    :goto_2
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1095
    invoke-direct {p0, v0, v3, v1, p3}, Landroid/support/v7/widget/ai;->a(IIZLandroid/support/v7/widget/cc;)V

    .line 1096
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v1, v1, Landroid/support/v7/widget/am;->g:I

    .line 1097
    iget-object v4, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    invoke-direct {p0, p2, v4, p3, v2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I

    move-result v4

    add-int/2addr v1, v4

    .line 1098
    if-gez v1, :cond_4

    move p1, v2

    .line 1102
    goto :goto_1

    .line 1093
    :cond_3
    const/4 v0, -0x1

    goto :goto_2

    .line 1104
    :cond_4
    if-le v3, v1, :cond_5

    mul-int p1, v0, v1

    .line 1105
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bc;->a(I)V

    goto :goto_1
.end method

.method private e(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 845
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->b()I

    move-result v2

    sub-int/2addr v2, p2

    iput v2, v0, Landroid/support/v7/widget/am;->c:I

    .line 846
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/am;->e:I

    .line 848
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput p1, v0, Landroid/support/v7/widget/am;->d:I

    .line 849
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v1, v0, Landroid/support/v7/widget/am;->f:I

    .line 850
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput p2, v0, Landroid/support/v7/widget/am;->b:I

    .line 851
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/am;->g:I

    .line 852
    return-void

    :cond_0
    move v0, v1

    .line 846
    goto :goto_0
.end method

.method private f(II)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 859
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    sub-int v2, p2, v2

    iput v2, v0, Landroid/support/v7/widget/am;->c:I

    .line 860
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput p1, v0, Landroid/support/v7/widget/am;->d:I

    .line 861
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/am;->e:I

    .line 863
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v1, v0, Landroid/support/v7/widget/am;->f:I

    .line 864
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput p2, v0, Landroid/support/v7/widget/am;->b:I

    .line 865
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/am;->g:I

    .line 867
    return-void

    :cond_0
    move v0, v1

    .line 861
    goto :goto_0
.end method

.method private h(Landroid/support/v7/widget/cc;)I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 991
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    :goto_0
    if-nez v0, :cond_1

    .line 995
    :goto_1
    return v3

    :cond_0
    move v0, v3

    .line 991
    goto :goto_0

    .line 994
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 995
    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v2, v0

    :goto_2
    add-int/lit8 v0, v0, -0x1

    :goto_3
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v2, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v2

    :goto_4
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_5

    :goto_5
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v3}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v3

    :goto_6
    iget-boolean v5, p0, Landroid/support/v7/widget/ai;->e:Z

    iget-boolean v6, p0, Landroid/support/v7/widget/ai;->h:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v7/widget/ck;->a(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/bc;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/bs;ZZ)I

    move-result v3

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_3

    :cond_4
    move-object v2, v4

    goto :goto_4

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v3, v0

    :cond_6
    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    :cond_7
    move-object v3, v4

    goto :goto_6
.end method

.method private i(Landroid/support/v7/widget/cc;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1001
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    :goto_0
    if-nez v0, :cond_1

    .line 1005
    :goto_1
    return v3

    :cond_0
    move v0, v3

    .line 1001
    goto :goto_0

    .line 1004
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 1005
    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v2, v0

    :goto_2
    add-int/lit8 v0, v0, -0x1

    :goto_3
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v2, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v2

    :goto_4
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_5

    :goto_5
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v3}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v3

    :goto_6
    iget-boolean v5, p0, Landroid/support/v7/widget/ai;->e:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/ck;->a(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/bc;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/bs;Z)I

    move-result v3

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_3

    :cond_4
    move-object v2, v4

    goto :goto_4

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v3, v0

    :cond_6
    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    :cond_7
    move-object v3, v4

    goto :goto_6
.end method

.method private j(Landroid/support/v7/widget/cc;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1011
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    :goto_0
    if-nez v0, :cond_1

    .line 1015
    :goto_1
    return v3

    :cond_0
    move v0, v3

    .line 1011
    goto :goto_0

    .line 1014
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 1015
    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v2, v0

    :goto_2
    add-int/lit8 v0, v0, -0x1

    :goto_3
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v2, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v2

    :goto_4
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_5

    :goto_5
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v3}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v3

    :goto_6
    iget-boolean v5, p0, Landroid/support/v7/widget/ai;->e:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/ck;->b(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/bc;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/bs;Z)I

    move-result v3

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_3

    :cond_4
    move-object v2, v4

    goto :goto_4

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v3, v0

    :cond_6
    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    :cond_7
    move-object v3, v4

    goto :goto_6
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 322
    iget v2, p0, Landroid/support/v7/widget/ai;->f:I

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v2

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    .line 323
    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->c:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    .line 327
    :goto_1
    return-void

    :cond_1
    move v2, v1

    .line 322
    goto :goto_0

    .line 325
    :cond_2
    iget-boolean v2, p0, Landroid/support/v7/widget/ai;->c:Z

    if-nez v2, :cond_3

    :goto_2
    iput-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private k(Landroid/support/v7/widget/cc;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1447
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/support/v7/widget/cc;->f:I

    iget v1, p1, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v1

    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, v3, v1

    :goto_1
    invoke-direct {p0, v2, v1, v0}, Landroid/support/v7/widget/ai;->a(III)Landroid/view/View;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_0
    iget v0, p1, Landroid/support/v7/widget/cc;->e:I

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    iget-boolean v0, p1, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_4

    iget v0, p1, Landroid/support/v7/widget/cc;->f:I

    iget v1, p1, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v1

    :goto_3
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v2, v1

    :cond_3
    add-int/lit8 v1, v2, -0x1

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2, v0}, Landroid/support/v7/widget/ai;->a(III)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    :cond_4
    iget v0, p1, Landroid/support/v7/widget/cc;->e:I

    goto :goto_3
.end method

.method private k()V
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    if-nez v0, :cond_0

    .line 875
    new-instance v0, Landroid/support/v7/widget/am;

    invoke-direct {v0}, Landroid/support/v7/widget/am;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    .line 877
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    if-nez v0, :cond_1

    .line 878
    iget v0, p0, Landroid/support/v7/widget/ai;->f:I

    invoke-static {p0, v0}, Landroid/support/v7/widget/bc;->a(Landroid/support/v7/widget/bs;I)Landroid/support/v7/widget/bc;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    .line 880
    :cond_1
    return-void
.end method

.method private l()Landroid/view/View;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1422
    iget-boolean v1, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l(Landroid/support/v7/widget/cc;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1463
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_1

    iget v0, p1, Landroid/support/v7/widget/cc;->f:I

    iget v2, p1, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v2

    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, v2, v1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2, v0}, Landroid/support/v7/widget/ai;->a(III)Landroid/view/View;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    iget v0, p1, Landroid/support/v7/widget/cc;->e:I

    goto :goto_0

    :cond_2
    iget-boolean v0, p1, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_3

    iget v0, p1, Landroid/support/v7/widget/cc;->f:I

    iget v2, p1, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v2

    :goto_2
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v2, v2, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int v2, v3, v2

    :goto_3
    invoke-direct {p0, v1, v2, v0}, Landroid/support/v7/widget/ai;->a(III)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget v0, p1, Landroid/support/v7/widget/cc;->e:I

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_3
.end method

.method private m()Landroid/view/View;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1432
    iget-boolean v1, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v1, :cond_0

    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 2

    .prologue
    .line 942
    iget v0, p0, Landroid/support/v7/widget/ai;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 943
    const/4 v0, 0x0

    .line 945
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/ai;->d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/cc;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 392
    iget v1, p1, Landroid/support/v7/widget/cc;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 393
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->d()I

    move-result v0

    .line 395
    :cond_0
    return v0

    :cond_1
    move v1, v0

    .line 392
    goto :goto_0
.end method

.method public a()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 161
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final a(I)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 367
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v2, v0

    move v2, v0

    .line 368
    :goto_0
    if-nez v2, :cond_2

    .line 376
    :cond_0
    :goto_1
    return-object v1

    :cond_1
    move v2, v3

    .line 367
    goto :goto_0

    .line 371
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v3}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v3, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    .line 372
    :goto_3
    sub-int v0, p1, v0

    .line 373
    if-ltz v0, :cond_0

    if-ge v0, v2, :cond_0

    .line 374
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 371
    goto :goto_2

    :cond_4
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_3
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 242
    instance-of v0, p1, Landroid/support/v7/widget/an;

    if-eqz v0, :cond_0

    .line 243
    check-cast p1, Landroid/support/v7/widget/an;

    iput-object p1, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    .line 244
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 251
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 2

    .prologue
    .line 402
    new-instance v0, Landroid/support/v7/widget/aj;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/aj;-><init>(Landroid/support/v7/widget/ai;Landroid/content/Context;)V

    .line 410
    iput p2, v0, Landroid/support/v7/widget/ca;->g:I

    .line 411
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/ca;)V

    .line 412
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/bw;)V
    .locals 0

    .prologue
    .line 195
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/bw;)V

    .line 196
    return-void
.end method

.method a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Landroid/support/v7/widget/am;Landroid/support/v7/widget/al;)V
    .locals 9

    .prologue
    .line 1306
    iget-object v0, p3, Landroid/support/v7/widget/am;->j:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p3, Landroid/support/v7/widget/am;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    const/4 v3, 0x0

    const v1, 0x7fffffff

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_4

    iget-object v0, p3, Landroid/support/v7/widget/am;->j:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-boolean v2, p3, Landroid/support/v7/widget/am;->i:Z

    if-nez v2, :cond_0

    iget v2, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_21

    :cond_0
    iget v2, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v6, -0x1

    if-ne v2, v6, :cond_2

    iget v2, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_2
    iget v6, p3, Landroid/support/v7/widget/am;->d:I

    sub-int/2addr v2, v6

    iget v6, p3, Landroid/support/v7/widget/am;->e:I

    mul-int/2addr v2, v6

    if-ltz v2, :cond_21

    if-ge v2, v1, :cond_21

    if-eqz v2, :cond_3

    move-object v1, v0

    move v0, v2

    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    iget v2, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_2

    :cond_3
    move-object v3, v0

    :cond_4
    if-eqz v3, :cond_6

    iget v0, v3, Landroid/support/v7/widget/ce;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    iget v0, v3, Landroid/support/v7/widget/ce;->b:I

    :goto_4
    iget v1, p3, Landroid/support/v7/widget/am;->e:I

    add-int/2addr v0, v1

    iput v0, p3, Landroid/support/v7/widget/am;->d:I

    iget-object v0, v3, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    move-object v5, v0

    .line 1307
    :goto_5
    if-nez v5, :cond_8

    .line 1313
    const/4 v0, 0x1

    iput-boolean v0, p4, Landroid/support/v7/widget/al;->b:Z

    .line 1376
    :goto_6
    return-void

    .line 1306
    :cond_5
    iget v0, v3, Landroid/support/v7/widget/ce;->f:I

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    move-object v5, v0

    goto :goto_5

    :cond_7
    iget v0, p3, Landroid/support/v7/widget/am;->d:I

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/bw;->b(I)Landroid/view/View;

    move-result-object v0

    iget v1, p3, Landroid/support/v7/widget/am;->d:I

    iget v2, p3, Landroid/support/v7/widget/am;->e:I

    add-int/2addr v1, v2

    iput v1, p3, Landroid/support/v7/widget/am;->d:I

    move-object v5, v0

    goto :goto_5

    .line 1316
    :cond_8
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 1317
    iget-object v1, p3, Landroid/support/v7/widget/am;->j:Ljava/util/List;

    if-nez v1, :cond_d

    .line 1318
    iget-boolean v2, p0, Landroid/support/v7/widget/ai;->h:Z

    iget v1, p3, Landroid/support/v7/widget/am;->f:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_b

    const/4 v1, 0x1

    :goto_7
    if-ne v2, v1, :cond_c

    .line 1320
    const/4 v1, -0x1

    invoke-virtual {p0, v5, v1}, Landroid/support/v7/widget/bs;->b(Landroid/view/View;I)V

    .line 1332
    :goto_8
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v4, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    iget v6, v4, Landroid/graphics/Rect;->left:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    add-int/2addr v6, v2

    iget v2, v4, Landroid/graphics/Rect;->top:I

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v4

    add-int v7, v3, v2

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_10

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    :goto_9
    iget-object v3, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_11

    iget-object v3, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    :goto_a
    iget-object v4, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v4, :cond_12

    iget-object v4, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v4

    :goto_b
    add-int/2addr v3, v4

    iget v4, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    add-int/2addr v3, v6

    iget v4, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->width:I

    invoke-virtual {p0}, Landroid/support/v7/widget/bs;->d()Z

    move-result v6

    invoke-static {v2, v3, v4, v6}, Landroid/support/v7/widget/bs;->a(IIIZ)I

    move-result v6

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_13

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v2

    :goto_c
    iget-object v3, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_14

    iget-object v3, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    :goto_d
    iget-object v4, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v4, :cond_15

    iget-object v4, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v4

    :goto_e
    add-int/2addr v3, v4

    iget v4, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v3, v4

    iget v4, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v3, v4

    add-int/2addr v3, v7

    iget v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->height:I

    invoke-virtual {p0}, Landroid/support/v7/widget/bs;->e()Z

    move-result v4

    invoke-static {v2, v3, v1, v4}, Landroid/support/v7/widget/bs;->a(IIIZ)I

    move-result v1

    invoke-virtual {v5, v6, v1}, Landroid/view/View;->measure(II)V

    .line 1333
    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v5}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v1

    iput v1, p4, Landroid/support/v7/widget/al;->a:I

    .line 1335
    iget v1, p0, Landroid/support/v7/widget/ai;->f:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1c

    .line 1336
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_16

    const/4 v1, 0x1

    :goto_f
    if-eqz v1, :cond_19

    .line 1337
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_17

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    :goto_10
    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_18

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    :goto_11
    sub-int/2addr v1, v2

    .line 1338
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/bc;->d(Landroid/view/View;)I

    move-result v2

    sub-int v2, v1, v2

    .line 1343
    :goto_12
    iget v3, p3, Landroid/support/v7/widget/am;->f:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1b

    .line 1344
    iget v3, p3, Landroid/support/v7/widget/am;->b:I

    .line 1345
    iget v4, p3, Landroid/support/v7/widget/am;->b:I

    iget v6, p4, Landroid/support/v7/widget/al;->a:I

    sub-int/2addr v4, v6

    move v8, v3

    move v3, v4

    move v4, v2

    move v2, v1

    move v1, v8

    .line 1364
    :goto_13
    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v4, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v3, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    sub-int/2addr v2, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    sub-int/2addr v1, v6

    invoke-static {v5, v4, v3, v2, v1}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;IIII)V

    .line 1372
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v1, v1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1f

    const/4 v1, 0x1

    :goto_14
    if-nez v1, :cond_9

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v0, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_20

    const/4 v0, 0x1

    :goto_15
    if-eqz v0, :cond_a

    .line 1373
    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p4, Landroid/support/v7/widget/al;->c:Z

    .line 1375
    :cond_a
    invoke-virtual {v5}, Landroid/view/View;->isFocusable()Z

    move-result v0

    iput-boolean v0, p4, Landroid/support/v7/widget/al;->d:Z

    goto/16 :goto_6

    .line 1318
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 1322
    :cond_c
    const/4 v1, 0x0

    invoke-virtual {p0, v5, v1}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;I)V

    goto/16 :goto_8

    .line 1325
    :cond_d
    iget-boolean v2, p0, Landroid/support/v7/widget/ai;->h:Z

    iget v1, p3, Landroid/support/v7/widget/am;->f:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_e

    const/4 v1, 0x1

    :goto_16
    if-ne v2, v1, :cond_f

    .line 1327
    const/4 v1, -0x1

    invoke-virtual {p0, v5, v1}, Landroid/support/v7/widget/bs;->a(Landroid/view/View;I)V

    goto/16 :goto_8

    .line 1325
    :cond_e
    const/4 v1, 0x0

    goto :goto_16

    .line 1329
    :cond_f
    const/4 v1, 0x0

    invoke-virtual {p0, v5, v1}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;I)V

    goto/16 :goto_8

    .line 1332
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_a

    :cond_12
    const/4 v4, 0x0

    goto/16 :goto_b

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_c

    :cond_14
    const/4 v3, 0x0

    goto/16 :goto_d

    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_e

    .line 1336
    :cond_16
    const/4 v1, 0x0

    goto/16 :goto_f

    .line 1337
    :cond_17
    const/4 v1, 0x0

    goto/16 :goto_10

    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_11

    .line 1340
    :cond_19
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v1

    .line 1341
    :goto_17
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/bc;->d(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v1

    move v8, v2

    move v2, v1

    move v1, v8

    goto/16 :goto_12

    .line 1340
    :cond_1a
    const/4 v1, 0x0

    goto :goto_17

    .line 1347
    :cond_1b
    iget v4, p3, Landroid/support/v7/widget/am;->b:I

    .line 1348
    iget v3, p3, Landroid/support/v7/widget/am;->b:I

    iget v6, p4, Landroid/support/v7/widget/al;->a:I

    add-int/2addr v3, v6

    move v8, v3

    move v3, v4

    move v4, v2

    move v2, v1

    move v1, v8

    goto/16 :goto_13

    .line 1351
    :cond_1c
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_1d

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v1

    .line 1352
    :goto_18
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/bc;->d(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v1

    .line 1354
    iget v3, p3, Landroid/support/v7/widget/am;->f:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1e

    .line 1355
    iget v3, p3, Landroid/support/v7/widget/am;->b:I

    .line 1356
    iget v4, p3, Landroid/support/v7/widget/am;->b:I

    iget v6, p4, Landroid/support/v7/widget/al;->a:I

    sub-int/2addr v4, v6

    move v8, v2

    move v2, v3

    move v3, v1

    move v1, v8

    goto/16 :goto_13

    .line 1351
    :cond_1d
    const/4 v1, 0x0

    goto :goto_18

    .line 1358
    :cond_1e
    iget v4, p3, Landroid/support/v7/widget/am;->b:I

    .line 1359
    iget v3, p3, Landroid/support/v7/widget/am;->b:I

    iget v6, p4, Landroid/support/v7/widget/al;->a:I

    add-int/2addr v3, v6

    move v8, v2

    move v2, v3

    move v3, v1

    move v1, v8

    goto/16 :goto_13

    .line 1372
    :cond_1f
    const/4 v1, 0x0

    goto/16 :goto_14

    :cond_20
    const/4 v0, 0x0

    goto/16 :goto_15

    :cond_21
    move v0, v1

    move-object v1, v3

    goto/16 :goto_3
.end method

.method a(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/ak;)V
    .locals 0

    .prologue
    .line 580
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 204
    invoke-super {p0, p1}, Landroid/support/v7/widget/bs;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 205
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v3, v0

    :goto_0
    if-lez v0, :cond_0

    .line 206
    invoke-static {p1}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/ae;

    move-result-object v3

    .line 208
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v4, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v4, v0

    :goto_1
    invoke-direct {p0, v1, v0, v1}, Landroid/support/v7/widget/ai;->a(IIZ)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_2
    sget-object v4, Landroid/support/v4/view/a/ae;->a:Landroid/support/v4/view/a/ah;

    iget-object v5, v3, Landroid/support/v4/view/a/ae;->b:Ljava/lang/Object;

    invoke-interface {v4, v5, v0}, Landroid/support/v4/view/a/ah;->a(Ljava/lang/Object;I)V

    .line 209
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v4, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v4, v0

    :goto_3
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0, v2, v1}, Landroid/support/v7/widget/ai;->a(IIZ)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_6

    :goto_4
    sget-object v0, Landroid/support/v4/view/a/ae;->a:Landroid/support/v4/view/a/ah;

    iget-object v1, v3, Landroid/support/v4/view/a/ae;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Landroid/support/v4/view/a/ah;->c(Ljava/lang/Object;I)V

    .line 211
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 205
    goto :goto_0

    :cond_2
    move v0, v1

    .line 208
    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v4, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v4, v2, :cond_4

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    goto :goto_2

    :cond_4
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_2

    :cond_5
    move v0, v1

    .line 209
    goto :goto_3

    :cond_6
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v1, v2, :cond_7

    iget v2, v0, Landroid/support/v7/widget/ce;->b:I

    goto :goto_4

    :cond_7
    iget v2, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1114
    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    if-nez v0, :cond_0

    .line 1115
    invoke-super {p0, p1}, Landroid/support/v7/widget/bs;->a(Ljava/lang/String;)V

    .line 1117
    :cond_0
    return-void
.end method

.method public b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 954
    iget v0, p0, Landroid/support/v7/widget/ai;->f:I

    if-nez v0, :cond_0

    .line 955
    const/4 v0, 0x0

    .line 957
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/ai;->d(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 962
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ai;->h(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/graphics/PointF;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 415
    iget-object v4, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v4, :cond_0

    iget-object v4, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v5, v4, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v5}, Landroid/support/v7/widget/t;->a()I

    move-result v5

    iget-object v4, v4, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int v4, v5, v4

    :goto_0
    if-nez v4, :cond_1

    .line 423
    :goto_1
    return-object v0

    :cond_0
    move v4, v2

    .line 415
    goto :goto_0

    .line 418
    :cond_1
    iget-object v4, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v4, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/r;->a(I)I

    move-result v4

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v4}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v4, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v4, v3, :cond_4

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    .line 419
    :goto_2
    if-ge p1, v0, :cond_5

    move v0, v1

    :goto_3
    iget-boolean v2, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eq v0, v2, :cond_3

    move v1, v3

    .line 420
    :cond_3
    iget v0, p0, Landroid/support/v7/widget/ai;->f:I

    if-nez v0, :cond_6

    .line 421
    new-instance v0, Landroid/graphics/PointF;

    int-to-float v1, v1

    invoke-direct {v0, v1, v6}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_1

    .line 418
    :cond_4
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_2

    :cond_5
    move v0, v2

    .line 419
    goto :goto_3

    .line 423
    :cond_6
    new-instance v0, Landroid/graphics/PointF;

    int-to-float v1, v1

    invoke-direct {v0, v6, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1711
    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 967
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ai;->h(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final c()Landroid/os/Parcelable;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 215
    iget-object v2, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    if-eqz v2, :cond_0

    .line 216
    new-instance v0, Landroid/support/v7/widget/an;

    iget-object v1, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/an;-><init>(Landroid/support/v7/widget/an;)V

    .line 237
    :goto_0
    return-object v0

    .line 218
    :cond_0
    new-instance v2, Landroid/support/v7/widget/an;

    invoke-direct {v2}, Landroid/support/v7/widget/an;-><init>()V

    .line 219
    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v4, v3, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    iget-object v3, v3, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int v3, v4, v3

    :goto_1
    if-lez v3, :cond_b

    .line 220
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 221
    iget-boolean v3, p0, Landroid/support/v7/widget/ai;->b:Z

    iget-boolean v4, p0, Landroid/support/v7/widget/ai;->h:Z

    xor-int/2addr v3, v4

    .line 222
    iput-boolean v3, v2, Landroid/support/v7/widget/an;->c:Z

    .line 223
    if-eqz v3, :cond_6

    .line 224
    iget-boolean v3, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v3, :cond_2

    :goto_2
    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v3, :cond_4

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    .line 225
    :goto_3
    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->b()I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v1, v3

    iput v1, v2, Landroid/support/v7/widget/an;->b:I

    .line 227
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v1, v5, :cond_5

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_4
    iput v0, v2, Landroid/support/v7/widget/an;->a:I

    :goto_5
    move-object v0, v2

    .line 237
    goto :goto_0

    :cond_1
    move v3, v0

    .line 219
    goto :goto_1

    .line 224
    :cond_2
    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v3, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v3, v0

    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3

    .line 227
    :cond_5
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_4

    .line 229
    :cond_6
    iget-boolean v3, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v3, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v3, v0

    :cond_7
    add-int/lit8 v0, v0, -0x1

    :cond_8
    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v3, :cond_9

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v1

    .line 230
    :cond_9
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v3, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v3, v5, :cond_a

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_6
    iput v0, v2, Landroid/support/v7/widget/an;->a:I

    .line 231
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->a()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, v2, Landroid/support/v7/widget/an;->b:I

    goto :goto_5

    .line 230
    :cond_a
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_6

    .line 235
    :cond_b
    iput v5, v2, Landroid/support/v7/widget/an;->a:I

    goto :goto_5
.end method

.method public final c(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x1

    const/high16 v6, -0x80000000

    .line 1604
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->j()V

    .line 1605
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, v2, v1

    :goto_0
    if-nez v1, :cond_2

    .line 1642
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move v1, v7

    .line 1605
    goto :goto_0

    .line 1609
    :cond_2
    sparse-switch p1, :sswitch_data_0

    move v3, v6

    .line 1610
    :goto_2
    if-eq v3, v6, :cond_0

    .line 1613
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 1615
    if-ne v3, v4, :cond_7

    .line 1616
    invoke-direct {p0, p3}, Landroid/support/v7/widget/ai;->l(Landroid/support/v7/widget/cc;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1620
    :goto_3
    if-eqz v2, :cond_0

    .line 1627
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 1628
    const v1, 0x3ea8f5c3    # 0.33f

    iget-object v8, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v8}, Landroid/support/v7/widget/bc;->d()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v1, v8

    float-to-int v1, v1

    .line 1629
    invoke-direct {p0, v3, v1, v7, p3}, Landroid/support/v7/widget/ai;->a(IIZLandroid/support/v7/widget/cc;)V

    .line 1630
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v6, v1, Landroid/support/v7/widget/am;->g:I

    .line 1631
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput-boolean v7, v1, Landroid/support/v7/widget/am;->a:Z

    .line 1632
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    invoke-direct {p0, p2, v1, p3, v5}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I

    .line 1634
    if-ne v3, v4, :cond_8

    .line 1635
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->l()Landroid/view/View;

    move-result-object v1

    .line 1639
    :goto_4
    if-eq v1, v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1642
    goto :goto_1

    :sswitch_0
    move v3, v4

    .line 1609
    goto :goto_2

    :sswitch_1
    move v3, v5

    goto :goto_2

    :sswitch_2
    iget v1, p0, Landroid/support/v7/widget/ai;->f:I

    if-ne v1, v5, :cond_3

    move v3, v4

    goto :goto_2

    :cond_3
    move v3, v6

    goto :goto_2

    :sswitch_3
    iget v1, p0, Landroid/support/v7/widget/ai;->f:I

    if-ne v1, v5, :cond_4

    move v3, v5

    goto :goto_2

    :cond_4
    move v3, v6

    goto :goto_2

    :sswitch_4
    iget v1, p0, Landroid/support/v7/widget/ai;->f:I

    if-nez v1, :cond_5

    move v3, v4

    goto :goto_2

    :cond_5
    move v3, v6

    goto :goto_2

    :sswitch_5
    iget v1, p0, Landroid/support/v7/widget/ai;->f:I

    if-nez v1, :cond_6

    move v3, v5

    goto :goto_2

    :cond_6
    move v3, v6

    goto :goto_2

    .line 1618
    :cond_7
    invoke-direct {p0, p3}, Landroid/support/v7/widget/ai;->k(Landroid/support/v7/widget/cc;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto :goto_3

    .line 1637
    :cond_8
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->m()Landroid/view/View;

    move-result-object v1

    goto :goto_4

    .line 1609
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x11 -> :sswitch_4
        0x21 -> :sswitch_2
        0x42 -> :sswitch_5
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 898
    iput p1, p0, Landroid/support/v7/widget/ai;->i:I

    .line 899
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/ai;->j:I

    .line 900
    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    if-eqz v0, :cond_0

    .line 901
    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    const/4 v1, -0x1

    iput v1, v0, Landroid/support/v7/widget/an;->a:I

    .line 903
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 904
    :cond_1
    return-void
.end method

.method public c(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)V
    .locals 12

    .prologue
    .line 442
    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    iget v0, v0, Landroid/support/v7/widget/an;->a:I

    if-ltz v0, :cond_7

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    iget v0, v0, Landroid/support/v7/widget/an;->a:I

    iput v0, p0, Landroid/support/v7/widget/ai;->i:I

    .line 446
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->k()V

    .line 447
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/am;->a:Z

    .line 449
    invoke-direct {p0}, Landroid/support/v7/widget/ai;->j()V

    .line 451
    iget-object v0, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    const/4 v1, -0x1

    iput v1, v0, Landroid/support/v7/widget/ak;->a:I

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/ak;->b:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/ak;->c:Z

    .line 452
    iget-object v0, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    iget-boolean v1, p0, Landroid/support/v7/widget/ai;->h:Z

    iput-boolean v1, v0, Landroid/support/v7/widget/ak;->c:Z

    .line 454
    iget-object v3, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_1

    iget v0, p0, Landroid/support/v7/widget/ai;->i:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    :goto_2
    if-nez v0, :cond_1e

    const/4 v0, 0x0

    :goto_3
    if-nez v0, :cond_2

    invoke-virtual {v3}, Landroid/support/v7/widget/ak;->a()V

    const/4 v0, 0x0

    iput v0, v3, Landroid/support/v7/widget/ak;->a:I

    .line 463
    :cond_2
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/cc;)I

    move-result v1

    .line 464
    iget v0, p2, Landroid/support/v7/widget/cc;->a:I

    iget-object v2, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    iget v2, v2, Landroid/support/v7/widget/ak;->a:I

    if-ge v0, v2, :cond_39

    const/4 v0, 0x1

    .line 465
    :goto_4
    iget-boolean v2, p0, Landroid/support/v7/widget/ai;->h:Z

    if-ne v0, v2, :cond_3a

    .line 467
    const/4 v0, 0x0

    move v11, v1

    move v1, v0

    move v0, v11

    .line 472
    :goto_5
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    add-int/2addr v1, v2

    .line 473
    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->e()I

    move-result v2

    add-int/2addr v0, v2

    .line 474
    iget-boolean v2, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v2, :cond_3

    iget v2, p0, Landroid/support/v7/widget/ai;->i:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    iget v2, p0, Landroid/support/v7/widget/ai;->j:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 479
    iget v2, p0, Landroid/support/v7/widget/ai;->i:I

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ai;->a(I)Landroid/view/View;

    move-result-object v2

    .line 480
    if-eqz v2, :cond_3

    .line 483
    iget-boolean v3, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v3, :cond_3b

    .line 484
    iget-object v3, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v3}, Landroid/support/v7/widget/bc;->b()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v4, v2}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v2

    sub-int v2, v3, v2

    .line 486
    iget v3, p0, Landroid/support/v7/widget/ai;->j:I

    sub-int/2addr v2, v3

    .line 492
    :goto_6
    if-lez v2, :cond_3c

    .line 493
    add-int/2addr v1, v2

    .line 501
    :cond_3
    :goto_7
    iget-object v2, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    invoke-virtual {p0, p2, v2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/ak;)V

    .line 502
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;)V

    .line 503
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget-boolean v3, p2, Landroid/support/v7/widget/cc;->i:Z

    iput-boolean v3, v2, Landroid/support/v7/widget/am;->i:Z

    .line 504
    iget-object v2, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    iget-boolean v2, v2, Landroid/support/v7/widget/ak;->c:Z

    if-eqz v2, :cond_3d

    .line 506
    iget-object v2, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    iget v3, v2, Landroid/support/v7/widget/ak;->a:I

    iget v2, v2, Landroid/support/v7/widget/ak;->b:I

    invoke-direct {p0, v3, v2}, Landroid/support/v7/widget/ai;->f(II)V

    .line 507
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v1, v2, Landroid/support/v7/widget/am;->h:I

    .line 508
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, p2, v2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I

    .line 509
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v1, v1, Landroid/support/v7/widget/am;->b:I

    .line 510
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->c:I

    if-lez v2, :cond_4

    .line 511
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->c:I

    add-int/2addr v0, v2

    .line 514
    :cond_4
    iget-object v2, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    iget v3, v2, Landroid/support/v7/widget/ak;->a:I

    iget v2, v2, Landroid/support/v7/widget/ak;->b:I

    invoke-direct {p0, v3, v2}, Landroid/support/v7/widget/ai;->e(II)V

    .line 515
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v0, v2, Landroid/support/v7/widget/am;->h:I

    .line 516
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v0, Landroid/support/v7/widget/am;->d:I

    iget-object v3, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v3, v3, Landroid/support/v7/widget/am;->e:I

    add-int/2addr v2, v3

    iput v2, v0, Landroid/support/v7/widget/am;->d:I

    .line 517
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I

    .line 518
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v0, v0, Landroid/support/v7/widget/am;->b:I

    .line 539
    :goto_8
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_3f

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v2, v2, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int v2, v3, v2

    :goto_9
    if-lez v2, :cond_56

    .line 543
    iget-boolean v2, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v2, :cond_40

    .line 544
    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, p2, v2}, Landroid/support/v7/widget/ai;->a(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)I

    move-result v2

    .line 545
    add-int/2addr v1, v2

    .line 546
    add-int/2addr v0, v2

    .line 547
    const/4 v2, 0x0

    invoke-direct {p0, v1, p1, p2, v2}, Landroid/support/v7/widget/ai;->b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)I

    move-result v2

    .line 548
    add-int/2addr v1, v2

    .line 549
    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    .line 559
    :goto_a
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->k:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_41

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v3, v0

    :goto_b
    if-eqz v0, :cond_5

    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/ai;->b()Z

    move-result v0

    if-nez v0, :cond_42

    .line 560
    :cond_5
    :goto_c
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_6

    .line 561
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/ai;->i:I

    .line 562
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/ai;->j:I

    .line 563
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->d()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/bc;->b:I

    .line 565
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/ai;->b:Z

    .line 566
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    .line 570
    return-void

    .line 442
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 454
    :cond_8
    iget v0, p0, Landroid/support/v7/widget/ai;->i:I

    if-ltz v0, :cond_9

    iget v1, p0, Landroid/support/v7/widget/ai;->i:I

    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_a

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v2, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v2

    :goto_d
    if-lt v1, v0, :cond_b

    :cond_9
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/ai;->i:I

    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/ai;->j:I

    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_a
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_d

    :cond_b
    iget v0, p0, Landroid/support/v7/widget/ai;->i:I

    iput v0, v3, Landroid/support/v7/widget/ak;->a:I

    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    iget v0, v0, Landroid/support/v7/widget/an;->a:I

    if-ltz v0, :cond_c

    const/4 v0, 0x1

    :goto_e
    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    iget-boolean v0, v0, Landroid/support/v7/widget/an;->c:Z

    iput-boolean v0, v3, Landroid/support/v7/widget/ak;->c:Z

    iget-boolean v0, v3, Landroid/support/v7/widget/ak;->c:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    iget v1, v1, Landroid/support/v7/widget/an;->b:I

    sub-int/2addr v0, v1

    iput v0, v3, Landroid/support/v7/widget/ak;->b:I

    :goto_f
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_c
    const/4 v0, 0x0

    goto :goto_e

    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ai;->k:Landroid/support/v7/widget/an;

    iget v1, v1, Landroid/support/v7/widget/an;->b:I

    add-int/2addr v0, v1

    iput v0, v3, Landroid/support/v7/widget/ak;->b:I

    goto :goto_f

    :cond_e
    iget v0, p0, Landroid/support/v7/widget/ai;->j:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_1b

    iget v0, p0, Landroid/support/v7/widget/ai;->i:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ai;->a(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->d()I

    move-result v2

    if-le v1, v2, :cond_f

    invoke-virtual {v3}, Landroid/support/v7/widget/ak;->a()V

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_f
    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->a()I

    move-result v2

    sub-int/2addr v1, v2

    if-gez v1, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v0

    iput v0, v3, Landroid/support/v7/widget/ak;->b:I

    const/4 v0, 0x0

    iput-boolean v0, v3, Landroid/support/v7/widget/ak;->c:Z

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_10
    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->b()I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    if-gez v1, :cond_11

    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    iput v0, v3, Landroid/support/v7/widget/ak;->b:I

    const/4 v0, 0x1

    iput-boolean v0, v3, Landroid/support/v7/widget/ak;->c:Z

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_11
    iget-boolean v1, v3, Landroid/support/v7/widget/ak;->c:Z

    if-eqz v1, :cond_13

    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v1

    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    const/high16 v2, -0x80000000

    iget v4, v0, Landroid/support/v7/widget/bc;->b:I

    if-ne v2, v4, :cond_12

    const/4 v0, 0x0

    :goto_10
    add-int/2addr v0, v1

    :goto_11
    iput v0, v3, Landroid/support/v7/widget/ak;->b:I

    :goto_12
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_12
    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->d()I

    move-result v2

    iget v0, v0, Landroid/support/v7/widget/bc;->b:I

    sub-int v0, v2, v0

    goto :goto_10

    :cond_13
    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_11

    :cond_14
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    :goto_13
    if-lez v0, :cond_15

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_17

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v1, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_14
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_18

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_15
    iget v1, p0, Landroid/support/v7/widget/ai;->i:I

    if-ge v1, v0, :cond_19

    const/4 v0, 0x1

    :goto_16
    iget-boolean v1, p0, Landroid/support/v7/widget/ai;->h:Z

    if-ne v0, v1, :cond_1a

    const/4 v0, 0x1

    :goto_17
    iput-boolean v0, v3, Landroid/support/v7/widget/ak;->c:Z

    :cond_15
    invoke-virtual {v3}, Landroid/support/v7/widget/ak;->a()V

    goto :goto_12

    :cond_16
    const/4 v0, 0x0

    goto :goto_13

    :cond_17
    const/4 v0, 0x0

    goto :goto_14

    :cond_18
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_15

    :cond_19
    const/4 v0, 0x0

    goto :goto_16

    :cond_1a
    const/4 v0, 0x0

    goto :goto_17

    :cond_1b
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    iput-boolean v0, v3, Landroid/support/v7/widget/ak;->c:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_1c

    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/ai;->j:I

    sub-int/2addr v0, v1

    iput v0, v3, Landroid/support/v7/widget/ak;->b:I

    :goto_18
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_1c
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/ai;->j:I

    add-int/2addr v0, v1

    iput v0, v3, Landroid/support/v7/widget/ak;->b:I

    goto :goto_18

    :cond_1d
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_1e
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_1f

    const/4 v0, 0x0

    move-object v2, v0

    :goto_19
    if-eqz v2, :cond_27

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v1, v1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_22

    const/4 v1, 0x1

    :goto_1a
    if-nez v1, :cond_26

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v4, v1, Landroid/support/v7/widget/ce;->f:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_23

    iget v1, v1, Landroid/support/v7/widget/ce;->b:I

    :goto_1b
    if-ltz v1, :cond_26

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_24

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_1c
    iget-boolean v1, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v1, :cond_25

    iget v1, p2, Landroid/support/v7/widget/cc;->f:I

    iget v4, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v1, v4

    :goto_1d
    if-ge v0, v1, :cond_26

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ak;->a(Landroid/view/View;)V

    const/4 v0, 0x1

    :goto_1e
    if-eqz v0, :cond_27

    const/4 v0, 0x1

    goto/16 :goto_3

    :cond_1f
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_20

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    :cond_20
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_19

    :cond_21
    move-object v2, v0

    goto :goto_19

    :cond_22
    const/4 v1, 0x0

    goto :goto_1a

    :cond_23
    iget v1, v1, Landroid/support/v7/widget/ce;->f:I

    goto :goto_1b

    :cond_24
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_1c

    :cond_25
    iget v1, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_1d

    :cond_26
    const/4 v0, 0x0

    goto :goto_1e

    :cond_27
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->b:Z

    if-eqz v0, :cond_28

    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_28
    iget-boolean v0, v3, Landroid/support/v7/widget/ak;->c:Z

    if-eqz v0, :cond_30

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_2d

    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_2b

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v1, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v1

    :goto_1f
    const/4 v2, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_2c

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v4, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, v4, v1

    :goto_20
    invoke-direct {p0, v2, v1, v0}, Landroid/support/v7/widget/ai;->a(III)Landroid/view/View;

    move-result-object v0

    :goto_21
    if-eqz v0, :cond_38

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/ak;->a(Landroid/view/View;)V

    iget-boolean v1, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v1, :cond_2a

    invoke-virtual {p0}, Landroid/support/v7/widget/ai;->b()Z

    move-result v1

    if-eqz v1, :cond_2a

    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v2}, Landroid/support/v7/widget/bc;->b()I

    move-result v2

    if-ge v1, v2, :cond_29

    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bc;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v1}, Landroid/support/v7/widget/bc;->a()I

    move-result v1

    if-ge v0, v1, :cond_36

    :cond_29
    const/4 v0, 0x1

    :goto_22
    if-eqz v0, :cond_2a

    iget-boolean v0, v3, Landroid/support/v7/widget/ak;->c:Z

    if-eqz v0, :cond_37

    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->b()I

    move-result v0

    :goto_23
    iput v0, v3, Landroid/support/v7/widget/ak;->b:I

    :cond_2a
    const/4 v0, 0x1

    goto/16 :goto_3

    :cond_2b
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_1f

    :cond_2c
    const/4 v1, 0x0

    goto :goto_20

    :cond_2d
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_2e

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v1, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v1

    :goto_24
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_2f

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, v2, v1

    :goto_25
    add-int/lit8 v1, v1, -0x1

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2, v0}, Landroid/support/v7/widget/ai;->a(III)Landroid/view/View;

    move-result-object v0

    goto :goto_21

    :cond_2e
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_24

    :cond_2f
    const/4 v1, 0x0

    goto :goto_25

    :cond_30
    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_33

    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_31

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v1, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v1

    :goto_26
    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_32

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, v2, v1

    :goto_27
    add-int/lit8 v1, v1, -0x1

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2, v0}, Landroid/support/v7/widget/ai;->a(III)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_21

    :cond_31
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_26

    :cond_32
    const/4 v1, 0x0

    goto :goto_27

    :cond_33
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_34

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v1, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v1

    :goto_28
    const/4 v2, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v1, :cond_35

    iget-object v1, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v4, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, v4, v1

    :goto_29
    invoke-direct {p0, v2, v1, v0}, Landroid/support/v7/widget/ai;->a(III)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_21

    :cond_34
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_28

    :cond_35
    const/4 v1, 0x0

    goto :goto_29

    :cond_36
    const/4 v0, 0x0

    goto/16 :goto_22

    :cond_37
    iget-object v0, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->a()I

    move-result v0

    goto/16 :goto_23

    :cond_38
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 464
    :cond_39
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 470
    :cond_3a
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 488
    :cond_3b
    iget-object v3, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/bc;->a(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    invoke-virtual {v3}, Landroid/support/v7/widget/bc;->a()I

    move-result v3

    sub-int/2addr v2, v3

    .line 490
    iget v3, p0, Landroid/support/v7/widget/ai;->j:I

    sub-int v2, v3, v2

    goto/16 :goto_6

    .line 495
    :cond_3c
    sub-int/2addr v0, v2

    goto/16 :goto_7

    .line 521
    :cond_3d
    iget-object v2, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    iget v3, v2, Landroid/support/v7/widget/ak;->a:I

    iget v2, v2, Landroid/support/v7/widget/ak;->b:I

    invoke-direct {p0, v3, v2}, Landroid/support/v7/widget/ai;->e(II)V

    .line 522
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v0, v2, Landroid/support/v7/widget/am;->h:I

    .line 523
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I

    .line 524
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v0, v0, Landroid/support/v7/widget/am;->b:I

    .line 525
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->c:I

    if-lez v2, :cond_3e

    .line 526
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->c:I

    add-int/2addr v1, v2

    .line 529
    :cond_3e
    iget-object v2, p0, Landroid/support/v7/widget/ai;->l:Landroid/support/v7/widget/ak;

    iget v3, v2, Landroid/support/v7/widget/ak;->a:I

    iget v2, v2, Landroid/support/v7/widget/ak;->b:I

    invoke-direct {p0, v3, v2}, Landroid/support/v7/widget/ai;->f(II)V

    .line 530
    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v1, v2, Landroid/support/v7/widget/am;->h:I

    .line 531
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v1, Landroid/support/v7/widget/am;->d:I

    iget-object v3, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v3, v3, Landroid/support/v7/widget/am;->e:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/support/v7/widget/am;->d:I

    .line 532
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, p2, v2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I

    .line 533
    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v1, v1, Landroid/support/v7/widget/am;->b:I

    goto/16 :goto_8

    .line 539
    :cond_3f
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 551
    :cond_40
    const/4 v2, 0x1

    invoke-direct {p0, v1, p1, p2, v2}, Landroid/support/v7/widget/ai;->b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)I

    move-result v2

    .line 552
    add-int/2addr v1, v2

    .line 553
    add-int/2addr v0, v2

    .line 554
    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, p2, v2}, Landroid/support/v7/widget/ai;->a(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Z)I

    move-result v2

    .line 555
    add-int/2addr v1, v2

    .line 556
    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    goto/16 :goto_a

    .line 559
    :cond_41
    const/4 v0, 0x0

    goto/16 :goto_b

    :cond_42
    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v8, p1, Landroid/support/v7/widget/bw;->d:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    const/4 v0, 0x0

    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v3, :cond_43

    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v3, v3, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_2a
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v3, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_44

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    move v3, v0

    :goto_2b
    const/4 v0, 0x0

    move v7, v0

    :goto_2c
    if-ge v7, v9, :cond_49

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget v4, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v10, -0x1

    if-ne v4, v10, :cond_45

    iget v4, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_2d
    if-ge v4, v3, :cond_46

    const/4 v4, 0x1

    :goto_2e
    iget-boolean v10, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eq v4, v10, :cond_47

    const/4 v4, -0x1

    :goto_2f
    const/4 v10, -0x1

    if-ne v4, v10, :cond_48

    iget-object v4, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v6

    move v4, v0

    move v0, v5

    :goto_30
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v6, v4

    move v5, v0

    goto :goto_2c

    :cond_43
    const/4 v0, 0x0

    goto :goto_2a

    :cond_44
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    move v3, v0

    goto :goto_2b

    :cond_45
    iget v4, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_2d

    :cond_46
    const/4 v4, 0x0

    goto :goto_2e

    :cond_47
    const/4 v4, 0x1

    goto :goto_2f

    :cond_48
    iget-object v4, p0, Landroid/support/v7/widget/ai;->g:Landroid/support/v7/widget/bc;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/bc;->c(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v5

    move v4, v6

    goto :goto_30

    :cond_49
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput-object v8, v0, Landroid/support/v7/widget/am;->j:Ljava/util/List;

    if-lez v6, :cond_4a

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_4d

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_4c

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v3, v0

    :goto_31
    add-int/lit8 v0, v0, -0x1

    :goto_32
    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v3, :cond_4e

    iget-object v3, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v3, v3, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_33
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v3, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4f

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_34
    invoke-direct {p0, v0, v2}, Landroid/support/v7/widget/ai;->f(II)V

    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v6, v0, Landroid/support/v7/widget/am;->h:I

    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v2, 0x0

    iput v2, v0, Landroid/support/v7/widget/am;->c:I

    iget-object v2, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v3, v2, Landroid/support/v7/widget/am;->d:I

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_50

    const/4 v0, 0x1

    :goto_35
    add-int/2addr v0, v3

    iput v0, v2, Landroid/support/v7/widget/am;->d:I

    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I

    :cond_4a
    if-lez v5, :cond_4b

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_51

    const/4 v0, 0x0

    :goto_36
    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_53

    iget-object v2, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/r;->a(I)I

    move-result v0

    iget-object v2, v2, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2, v0}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_37
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v2, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_54

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_38
    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/ai;->e(II)V

    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iput v5, v0, Landroid/support/v7/widget/am;->h:I

    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/am;->c:I

    iget-object v1, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    iget v2, v1, Landroid/support/v7/widget/am;->d:I

    iget-boolean v0, p0, Landroid/support/v7/widget/ai;->h:Z

    if-eqz v0, :cond_55

    const/4 v0, -0x1

    :goto_39
    add-int/2addr v0, v2

    iput v0, v1, Landroid/support/v7/widget/am;->d:I

    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/am;Landroid/support/v7/widget/cc;Z)I

    :cond_4b
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/am;->j:Ljava/util/List;

    goto/16 :goto_c

    :cond_4c
    const/4 v0, 0x0

    goto/16 :goto_31

    :cond_4d
    const/4 v0, 0x0

    goto/16 :goto_32

    :cond_4e
    const/4 v0, 0x0

    goto :goto_33

    :cond_4f
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_34

    :cond_50
    const/4 v0, -0x1

    goto :goto_35

    :cond_51
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_52

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v2, v0

    :goto_3a
    add-int/lit8 v0, v0, -0x1

    goto :goto_36

    :cond_52
    const/4 v0, 0x0

    goto :goto_3a

    :cond_53
    const/4 v0, 0x0

    goto :goto_37

    :cond_54
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_38

    :cond_55
    const/4 v0, 0x1

    goto :goto_39

    :cond_56
    move v2, v1

    move v1, v0

    goto/16 :goto_a
.end method

.method public final d(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 972
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ai;->i(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Landroid/support/v7/widget/ai;->f:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 977
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ai;->i(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 266
    iget v1, p0, Landroid/support/v7/widget/ai;->f:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 982
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ai;->j(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method

.method public final g(Landroid/support/v7/widget/cc;)I
    .locals 1

    .prologue
    .line 987
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ai;->j(Landroid/support/v7/widget/cc;)I

    move-result v0

    return v0
.end method
