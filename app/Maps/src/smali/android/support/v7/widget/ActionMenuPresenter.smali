.class public Landroid/support/v7/widget/ActionMenuPresenter;
.super Landroid/support/v7/internal/view/menu/d;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/o;


# instance fields
.field c:Landroid/view/View;

.field d:Landroid/support/v7/widget/e;

.field e:Landroid/support/v7/widget/a;

.field f:Landroid/support/v7/widget/c;

.field final g:Landroid/support/v7/widget/f;

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private final m:Landroid/util/SparseBooleanArray;

.field private n:Landroid/view/View;

.field private o:Landroid/support/v7/widget/b;


# virtual methods
.method public final a(Landroid/support/v7/internal/view/menu/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 168
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/g;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    iget v2, p1, Landroid/support/v7/internal/view/menu/g;->g:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_5

    iget-object v2, p1, Landroid/support/v7/internal/view/menu/g;->h:Landroid/view/View;

    iget-object v2, p1, Landroid/support/v7/internal/view/menu/g;->h:Landroid/view/View;

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_1

    .line 170
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 172
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/g;->isActionViewExpanded()Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v1, 0x8

    :cond_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 174
    check-cast p3, Landroid/support/v7/widget/ActionMenuView;

    .line 175
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 176
    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 177
    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    :cond_3
    return-object v0

    :cond_4
    move v2, v1

    .line 169
    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/e;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 91
    invoke-super {p0, p1, p2}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/e;)V

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 95
    new-instance v3, Landroid/support/v7/internal/view/a;

    invoke-direct {v3, p1}, Landroid/support/v7/internal/view/a;-><init>(Landroid/content/Context;)V

    .line 96
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_2

    :cond_0
    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->h:Z

    .line 100
    iget-object v0, v3, Landroid/support/v7/internal/view/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:I

    .line 105
    iget-object v0, v3, Landroid/support/v7/internal/view/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Landroid/support/v7/a/g;->a:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->k:I

    .line 109
    iget v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:I

    .line 110
    iget-boolean v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->h:Z

    if-eqz v3, :cond_3

    .line 111
    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    if-nez v3, :cond_1

    .line 112
    new-instance v3, Landroid/support/v7/widget/ActionMenuPresenter$OverflowMenuButton;

    invoke-direct {v3, p0, v6}, Landroid/support/v7/widget/ActionMenuPresenter$OverflowMenuButton;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;)V

    iput-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    .line 113
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 114
    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    invoke-virtual {v3, v1, v1}, Landroid/view/View;->measure(II)V

    .line 116
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 121
    :goto_1
    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->j:I

    .line 123
    const/high16 v0, 0x42600000    # 56.0f

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->l:I

    .line 126
    iput-object v6, p0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Landroid/view/View;

    .line 127
    return-void

    .line 96
    :cond_2
    iget-object v4, v3, Landroid/support/v7/internal/view/a;->a:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-static {v4}, Landroid/support/v4/view/bf;->b(Landroid/view/ViewConfiguration;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 118
    :cond_3
    iput-object v6, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/e;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 514
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->f:Landroid/support/v7/widget/c;

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->d:Landroid/support/v7/widget/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/h;->c()V

    move v0, v1

    :goto_0
    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->e:Landroid/support/v7/widget/a;

    if-eqz v3, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->e:Landroid/support/v7/widget/a;

    invoke-virtual {v2}, Landroid/support/v7/widget/a;->c()V

    :goto_1
    or-int/2addr v0, v1

    .line 515
    invoke-super {p0, p1, p2}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/e;Z)V

    .line 516
    return-void

    :cond_0
    move v0, v2

    .line 514
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/g;Landroid/support/v7/internal/view/menu/m;)V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/support/v7/internal/view/menu/g;I)V

    .line 186
    check-cast p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 188
    const/4 v0, 0x0

    iput-object v0, p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->b:Landroid/support/v7/internal/view/menu/f;

    .line 190
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->o:Landroid/support/v7/widget/b;

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Landroid/support/v7/widget/b;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/b;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->o:Landroid/support/v7/widget/b;

    .line 193
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->o:Landroid/support/v7/widget/b;

    iput-object v0, p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->c:Landroid/support/v7/internal/view/menu/c;

    .line 194
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 539
    if-eqz p1, :cond_0

    .line 541
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/n;)Z

    .line 545
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/support/v7/internal/view/menu/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/e;->a(Z)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 17

    .prologue
    .line 379
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/support/v7/internal/view/menu/e;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/e;->d()Ljava/util/ArrayList;

    move-result-object v10

    .line 380
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 381
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/ActionMenuPresenter;->k:I

    .line 382
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/ActionMenuPresenter;->j:I

    .line 383
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 384
    const/4 v4, 0x0

    .line 387
    const/4 v3, 0x0

    .line 388
    const/4 v6, 0x0

    .line 389
    const/4 v2, 0x0

    .line 390
    const/4 v1, 0x0

    move v9, v1

    :goto_0
    if-ge v9, v11, :cond_4

    .line 391
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/g;

    .line 392
    iget v8, v1, Landroid/support/v7/internal/view/menu/g;->g:I

    and-int/lit8 v8, v8, 0x2

    const/4 v13, 0x2

    if-ne v8, v13, :cond_0

    const/4 v8, 0x1

    :goto_1
    if-eqz v8, :cond_1

    .line 393
    add-int/lit8 v1, v4, 0x1

    move/from16 v16, v2

    move v2, v3

    move v3, v1

    move/from16 v1, v16

    .line 399
    :goto_2
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    move v4, v3

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 392
    :cond_0
    const/4 v8, 0x0

    goto :goto_1

    .line 394
    :cond_1
    iget v1, v1, Landroid/support/v7/internal/view/menu/g;->g:I

    and-int/lit8 v1, v1, 0x1

    const/4 v8, 0x1

    if-ne v1, v8, :cond_2

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_3

    .line 395
    add-int/lit8 v1, v3, 0x1

    move v3, v4

    move/from16 v16, v1

    move v1, v2

    move/from16 v2, v16

    goto :goto_2

    .line 394
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    .line 397
    :cond_3
    const/4 v1, 0x1

    move v2, v3

    move v3, v4

    goto :goto_2

    .line 407
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->h:Z

    if-eqz v1, :cond_1c

    if-nez v2, :cond_5

    add-int v1, v4, v3

    if-le v1, v5, :cond_1c

    .line 409
    :cond_5
    add-int/lit8 v1, v5, -0x1

    .line 411
    :goto_4
    sub-int v3, v1, v4

    .line 413
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/v7/widget/ActionMenuPresenter;->m:Landroid/util/SparseBooleanArray;

    .line 414
    invoke-virtual {v13}, Landroid/util/SparseBooleanArray;->clear()V

    .line 416
    const/4 v1, 0x0

    move v9, v1

    move v4, v7

    move/from16 v16, v6

    move v6, v3

    move/from16 v3, v16

    :goto_5
    if-ge v9, v11, :cond_16

    .line 426
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/g;

    .line 428
    iget v2, v1, Landroid/support/v7/internal/view/menu/g;->g:I

    and-int/lit8 v2, v2, 0x2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_8

    const/4 v2, 0x1

    :goto_6
    if-eqz v2, :cond_9

    .line 429
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Landroid/view/View;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v5}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/support/v7/internal/view/menu/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 430
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Landroid/view/View;

    if-nez v5, :cond_6

    .line 431
    move-object/from16 v0, p0

    iput-object v2, v0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Landroid/view/View;

    .line 433
    :cond_6
    invoke-virtual {v2, v12, v12}, Landroid/view/View;->measure(II)V

    .line 439
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 440
    sub-int/2addr v4, v2

    .line 441
    if-nez v3, :cond_1b

    .line 444
    :goto_7
    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/g;->getGroupId()I

    move-result v3

    .line 445
    if-eqz v3, :cond_7

    .line 446
    const/4 v5, 0x1

    invoke-virtual {v13, v3, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 448
    :cond_7
    iget v3, v1, Landroid/support/v7/internal/view/menu/g;->f:I

    or-int/lit8 v3, v3, 0x20

    iput v3, v1, Landroid/support/v7/internal/view/menu/g;->f:I

    move v1, v4

    move v3, v6

    .line 425
    :goto_8
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    move v6, v3

    move v4, v1

    move v3, v2

    goto :goto_5

    .line 428
    :cond_8
    const/4 v2, 0x0

    goto :goto_6

    .line 449
    :cond_9
    iget v2, v1, Landroid/support/v7/internal/view/menu/g;->g:I

    and-int/lit8 v2, v2, 0x1

    const/4 v5, 0x1

    if-ne v2, v5, :cond_d

    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_15

    .line 452
    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/g;->getGroupId()I

    move-result v14

    .line 453
    invoke-virtual {v13, v14}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v15

    .line 454
    if-gtz v6, :cond_a

    if-eqz v15, :cond_e

    :cond_a
    if-lez v4, :cond_e

    const/4 v5, 0x1

    .line 457
    :goto_a
    if-eqz v5, :cond_1a

    .line 458
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Landroid/view/View;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v7}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/support/v7/internal/view/menu/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 459
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Landroid/view/View;

    if-nez v7, :cond_b

    .line 460
    move-object/from16 v0, p0

    iput-object v2, v0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Landroid/view/View;

    .line 462
    :cond_b
    invoke-virtual {v2, v12, v12}, Landroid/view/View;->measure(II)V

    .line 472
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 473
    sub-int v7, v4, v2

    .line 474
    if-nez v3, :cond_19

    move v4, v2

    .line 478
    :goto_b
    add-int v2, v7, v4

    if-lez v2, :cond_f

    const/4 v2, 0x1

    :goto_c
    and-int/2addr v2, v5

    move v8, v2

    move v5, v7

    .line 486
    :goto_d
    if-eqz v8, :cond_10

    if-eqz v14, :cond_10

    .line 487
    const/4 v2, 0x1

    invoke-virtual {v13, v14, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    move v2, v6

    .line 501
    :goto_e
    if-eqz v8, :cond_c

    add-int/lit8 v2, v2, -0x1

    .line 503
    :cond_c
    if-eqz v8, :cond_14

    iget v3, v1, Landroid/support/v7/internal/view/menu/g;->f:I

    or-int/lit8 v3, v3, 0x20

    iput v3, v1, Landroid/support/v7/internal/view/menu/g;->f:I

    move v1, v5

    move v3, v2

    move v2, v4

    goto :goto_8

    .line 449
    :cond_d
    const/4 v2, 0x0

    goto :goto_9

    .line 454
    :cond_e
    const/4 v5, 0x0

    goto :goto_a

    .line 478
    :cond_f
    const/4 v2, 0x0

    goto :goto_c

    .line 488
    :cond_10
    if-eqz v15, :cond_18

    .line 490
    const/4 v2, 0x0

    invoke-virtual {v13, v14, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 491
    const/4 v2, 0x0

    move v7, v2

    move v3, v6

    :goto_f
    if-ge v7, v9, :cond_17

    .line 492
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/internal/view/menu/g;

    .line 493
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/g;->getGroupId()I

    move-result v6

    if-ne v6, v14, :cond_12

    .line 495
    iget v6, v2, Landroid/support/v7/internal/view/menu/g;->f:I

    and-int/lit8 v6, v6, 0x20

    const/16 v15, 0x20

    if-ne v6, v15, :cond_13

    const/4 v6, 0x1

    :goto_10
    if-eqz v6, :cond_11

    add-int/lit8 v3, v3, 0x1

    .line 496
    :cond_11
    iget v6, v2, Landroid/support/v7/internal/view/menu/g;->f:I

    and-int/lit8 v6, v6, -0x21

    iput v6, v2, Landroid/support/v7/internal/view/menu/g;->f:I

    :cond_12
    move v2, v3

    .line 491
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v3, v2

    goto :goto_f

    .line 495
    :cond_13
    const/4 v6, 0x0

    goto :goto_10

    .line 503
    :cond_14
    iget v3, v1, Landroid/support/v7/internal/view/menu/g;->f:I

    and-int/lit8 v3, v3, -0x21

    iput v3, v1, Landroid/support/v7/internal/view/menu/g;->f:I

    move v1, v5

    move v3, v2

    move v2, v4

    .line 504
    goto/16 :goto_8

    .line 506
    :cond_15
    iget v2, v1, Landroid/support/v7/internal/view/menu/g;->f:I

    and-int/lit8 v2, v2, -0x21

    iput v2, v1, Landroid/support/v7/internal/view/menu/g;->f:I

    move v2, v3

    move v1, v4

    move v3, v6

    goto/16 :goto_8

    .line 509
    :cond_16
    const/4 v1, 0x1

    return v1

    :cond_17
    move v2, v3

    goto :goto_e

    :cond_18
    move v2, v6

    goto :goto_e

    :cond_19
    move v4, v3

    goto :goto_b

    :cond_1a
    move v8, v5

    move v5, v4

    move v4, v3

    goto :goto_d

    :cond_1b
    move v2, v3

    goto/16 :goto_7

    :cond_1c
    move v1, v5

    goto/16 :goto_4
.end method

.method public final a(Landroid/support/v7/internal/view/menu/n;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 261
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/n;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 278
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 264
    :goto_1
    iget-object v2, v0, Landroid/support/v7/internal/view/menu/n;->i:Landroid/support/v7/internal/view/menu/e;

    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/support/v7/internal/view/menu/e;

    if-eq v2, v3, :cond_1

    .line 265
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/n;->i:Landroid/support/v7/internal/view/menu/e;

    check-cast v0, Landroid/support/v7/internal/view/menu/n;

    goto :goto_1

    .line 267
    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/n;->getItem()Landroid/view/MenuItem;

    const/4 v0, 0x0

    .line 268
    if-nez v0, :cond_3

    .line 269
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    .line 270
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    .line 273
    :cond_3
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/n;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    .line 274
    new-instance v1, Landroid/support/v7/widget/a;

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->a:Landroid/content/Context;

    invoke-direct {v1, p0, v2, p1}, Landroid/support/v7/widget/a;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;Landroid/support/v7/internal/view/menu/n;)V

    iput-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->e:Landroid/support/v7/widget/a;

    .line 275
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->e:Landroid/support/v7/widget/a;

    iput-object v0, v1, Landroid/support/v7/internal/view/menu/h;->e:Landroid/view/View;

    .line 276
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->e:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/h;->b()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MenuPopupHelper cannot be used without an anchor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_4
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/n;)Z

    .line 278
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 203
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 204
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/menu/d;->b(Z)V

    .line 209
    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 211
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/support/v7/internal/view/menu/e;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/support/v7/internal/view/menu/e;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/e;->e()V

    iget-object v5, v0, Landroid/support/v7/internal/view/menu/e;->d:Ljava/util/ArrayList;

    .line 213
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    .line 214
    :goto_0
    if-ge v4, v6, :cond_1

    .line 215
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/g;

    .line 216
    if-eqz v1, :cond_0

    .line 217
    iput-object p0, v1, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/o;

    .line 214
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/support/v7/internal/view/menu/e;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/support/v7/internal/view/menu/e;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/e;->e()V

    iget-object v0, v0, Landroid/support/v7/internal/view/menu/e;->e:Ljava/util/ArrayList;

    .line 226
    :goto_1
    iget-boolean v4, p0, Landroid/support/v7/widget/ActionMenuPresenter;->h:Z

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    .line 227
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 228
    if-ne v4, v2, :cond_8

    .line 229
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/g;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    :goto_2
    move v3, v0

    .line 235
    :cond_2
    :goto_3
    if-eqz v3, :cond_a

    .line 236
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    if-nez v0, :cond_3

    .line 237
    new-instance v0, Landroid/support/v7/widget/ActionMenuPresenter$OverflowMenuButton;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/ActionMenuPresenter$OverflowMenuButton;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    .line 239
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 240
    if-eqz v0, :cond_5

    .line 241
    if-eqz v0, :cond_4

    .line 242
    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 244
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/support/v7/widget/ActionMenuView;->a()Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/ActionMenuView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    :cond_5
    :goto_4
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->h:Z

    .line 252
    return-void

    :cond_6
    move-object v0, v1

    .line 222
    goto :goto_1

    :cond_7
    move v0, v3

    .line 229
    goto :goto_2

    .line 231
    :cond_8
    if-lez v4, :cond_9

    :goto_5
    move v3, v2

    goto :goto_3

    :cond_9
    move v2, v3

    goto :goto_5

    .line 247
    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_5

    .line 248
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 301
    iget-boolean v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->h:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->d:Landroid/support/v7/widget/e;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->d:Landroid/support/v7/widget/e;

    iget-object v3, v2, Landroid/support/v7/internal/view/menu/h;->f:Landroid/support/v7/widget/ListPopupWindow;

    if-eqz v3, :cond_1

    iget-object v2, v2, Landroid/support/v7/internal/view/menu/h;->f:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v2, v2, Landroid/support/v7/widget/ListPopupWindow;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/support/v7/internal/view/menu/e;

    .line 303
    :cond_0
    return v1

    :cond_1
    move v2, v1

    .line 301
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
