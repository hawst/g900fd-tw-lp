.class public Landroid/support/v7/widget/GridLayoutManager;
.super Landroid/support/v7/widget/ai;
.source "PG"


# static fields
.field static final a:I


# instance fields
.field b:I

.field c:[Landroid/view/View;

.field final d:Landroid/util/SparseIntArray;

.field final e:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Landroid/support/v7/widget/GridLayoutManager;->a:I

    return-void
.end method

.method private a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;I)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 274
    iget-boolean v1, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v1, :cond_0

    .line 275
    invoke-virtual {v3, p3, v0}, Landroid/support/v7/widget/ag;->b(II)I

    move-result v0

    .line 286
    :goto_0
    return v0

    .line 277
    :cond_0
    invoke-virtual {p1, p3}, Landroid/support/v7/widget/bw;->a(I)I

    move-result v1

    .line 278
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot find span size for pre layout position. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 286
    :cond_1
    invoke-virtual {v3, v1, v0}, Landroid/support/v7/widget/ag;->b(II)I

    move-result v0

    goto :goto_0
.end method

.method private b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;I)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 311
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_1

    .line 312
    invoke-virtual {v2}, Landroid/support/v7/widget/ag;->a()I

    move-result v0

    .line 328
    :cond_0
    :goto_0
    return v0

    .line 314
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p3, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 315
    if-ne v0, v1, :cond_0

    .line 318
    invoke-virtual {p1, p3}, Landroid/support/v7/widget/bw;->a(I)I

    move-result v0

    .line 319
    if-ne v0, v1, :cond_2

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 326
    const/4 v0, 0x1

    goto :goto_0

    .line 328
    :cond_2
    invoke-virtual {v2}, Landroid/support/v7/widget/ag;->a()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 102
    iget v1, p0, Landroid/support/v7/widget/GridLayoutManager;->f:I

    if-nez v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v0

    .line 105
    :cond_1
    iget-boolean v1, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v1, :cond_2

    iget v1, p2, Landroid/support/v7/widget/cc;->f:I

    iget v2, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v1, v2

    :goto_1
    if-lez v1, :cond_0

    .line 108
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_3

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v1, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v1

    :goto_2
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/GridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;I)I

    move-result v0

    goto :goto_0

    .line 105
    :cond_2
    iget v1, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_1

    .line 108
    :cond_3
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_2
.end method

.method public final a()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 200
    new-instance v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    .prologue
    .line 206
    new-instance v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    .prologue
    .line 211
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 212
    new-instance v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 214
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    iget-object v0, v0, Landroid/support/v7/widget/ag;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 176
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    iget-object v0, v0, Landroid/support/v7/widget/ag;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 181
    return-void
.end method

.method final a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Landroid/support/v7/widget/am;Landroid/support/v7/widget/al;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 334
    iget v0, p3, Landroid/support/v7/widget/am;->e:I

    if-ne v0, v1, :cond_2

    move v0, v1

    .line 336
    :goto_0
    if-nez v0, :cond_1

    .line 340
    iget v0, p3, Landroid/support/v7/widget/am;->d:I

    iget-boolean v3, p2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v3, :cond_3

    invoke-virtual {v5, v0, v2}, Landroid/support/v7/widget/ag;->a(II)I

    .line 341
    :cond_0
    :goto_1
    iget v0, p3, Landroid/support/v7/widget/am;->d:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/GridLayoutManager;->b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;I)I

    .line 342
    :cond_1
    iput-boolean v1, p4, Landroid/support/v7/widget/al;->b:Z

    .line 367
    return-void

    :cond_2
    move v0, v2

    .line 334
    goto :goto_0

    .line 340
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/GridLayoutManager;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    if-ne v3, v4, :cond_0

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/bw;->a(I)I

    move-result v3

    if-ne v3, v4, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    invoke-virtual {v5, v3, v2}, Landroid/support/v7/widget/ag;->a(II)I

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;Landroid/view/View;Landroid/support/v4/view/a/h;)V
    .locals 9

    .prologue
    .line 126
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 127
    instance-of v1, v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    if-nez v1, :cond_0

    .line 128
    invoke-super {p0, p3, p4}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;Landroid/support/v4/view/a/h;)V

    .line 144
    :goto_0
    return-void

    .line 131
    :cond_0
    check-cast v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    .line 132
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v2, v1, Landroid/support/v7/widget/ce;->f:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    iget v1, v1, Landroid/support/v7/widget/ce;->b:I

    :goto_1
    invoke-direct {p0, p1, p2, v1}, Landroid/support/v7/widget/GridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;I)I

    move-result v3

    .line 133
    iget v1, p0, Landroid/support/v7/widget/GridLayoutManager;->f:I

    if-nez v1, :cond_2

    .line 134
    iget v1, v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;->a:I

    iget v2, v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;->b:I

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Landroid/support/v4/view/a/q;

    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/view/a/k;->a(IIIIZZ)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/support/v4/view/a/q;-><init>(Ljava/lang/Object;)V

    sget-object v1, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v2, p4, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    move-object v0, v7

    check-cast v0, Landroid/support/v4/view/a/q;

    iget-object v0, v0, Landroid/support/v4/view/a/q;->a:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 132
    :cond_1
    iget v1, v1, Landroid/support/v7/widget/ce;->f:I

    goto :goto_1

    .line 139
    :cond_2
    const/4 v4, 0x1

    iget v5, v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;->a:I

    iget v6, v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;->b:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v0, Landroid/support/v4/view/a/q;

    sget-object v2, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    invoke-interface/range {v2 .. v8}, Landroid/support/v4/view/a/k;->a(IIIIZZ)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/a/q;-><init>(Ljava/lang/Object;)V

    sget-object v1, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/k;

    iget-object v2, p4, Landroid/support/v4/view/a/h;->b:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/view/a/q;

    iget-object v0, v0, Landroid/support/v4/view/a/q;->a:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final a(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/ak;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 254
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/cc;Landroid/support/v7/widget/ak;)V

    .line 255
    iget v0, p0, Landroid/support/v7/widget/ai;->f:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    :goto_1
    sub-int v2, v0, v2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    :goto_2
    sub-int v0, v2, v0

    :goto_3
    div-int/lit8 v0, v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/GridLayoutManager;->b:I

    .line 256
    iget-boolean v0, p1, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_7

    iget v0, p1, Landroid/support/v7/widget/cc;->f:I

    iget v2, p1, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v2

    :goto_4
    if-lez v0, :cond_8

    iget-boolean v0, p1, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_8

    .line 257
    iget v0, p2, Landroid/support/v7/widget/ak;->a:I

    invoke-virtual {v3, v0, v1}, Landroid/support/v7/widget/ag;->a(II)I

    move-result v0

    :goto_5
    if-lez v0, :cond_8

    iget v0, p2, Landroid/support/v7/widget/ak;->a:I

    if-lez v0, :cond_8

    iget v0, p2, Landroid/support/v7/widget/ak;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p2, Landroid/support/v7/widget/ak;->a:I

    iget v0, p2, Landroid/support/v7/widget/ak;->a:I

    invoke-virtual {v3, v0, v1}, Landroid/support/v7/widget/ag;->a(II)I

    move-result v0

    goto :goto_5

    :cond_0
    move v0, v1

    .line 255
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    :goto_6
    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    :goto_7
    sub-int v2, v0, v2

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v0

    :goto_8
    sub-int v0, v2, v0

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_6

    :cond_5
    move v2, v1

    goto :goto_7

    :cond_6
    move v0, v1

    goto :goto_8

    .line 256
    :cond_7
    iget v0, p1, Landroid/support/v7/widget/cc;->e:I

    goto :goto_4

    .line 259
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager;->c:[Landroid/view/View;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager;->c:[Landroid/view/View;

    array-length v0, v0

    if-eqz v0, :cond_a

    .line 260
    :cond_9
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Landroid/support/v7/widget/GridLayoutManager;->c:[Landroid/view/View;

    .line 262
    :cond_a
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$LayoutParams;)Z
    .locals 1

    .prologue
    .line 220
    instance-of v0, p1, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    return v0
.end method

.method public final b(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 114
    iget v1, p0, Landroid/support/v7/widget/GridLayoutManager;->f:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 117
    :cond_1
    iget-boolean v1, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v1, :cond_2

    iget v1, p2, Landroid/support/v7/widget/cc;->f:I

    iget v2, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v1, v2

    :goto_1
    if-lez v1, :cond_0

    .line 120
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_3

    iget v0, p2, Landroid/support/v7/widget/cc;->f:I

    iget v1, p2, Landroid/support/v7/widget/cc;->g:I

    sub-int/2addr v0, v1

    :goto_2
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/GridLayoutManager;->a(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;I)I

    move-result v0

    goto :goto_0

    .line 117
    :cond_2
    iget v1, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_1

    .line 120
    :cond_3
    iget v0, p2, Landroid/support/v7/widget/cc;->e:I

    goto :goto_2
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    iget-object v0, v0, Landroid/support/v7/widget/ag;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 186
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 735
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager;->k:Landroid/support/v7/widget/an;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    iget-object v0, v0, Landroid/support/v7/widget/ag;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 191
    return-void
.end method

.method public final c(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 148
    iget-boolean v0, p2, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    move v1, v0

    :goto_0
    move v3, v2

    :goto_1
    if-ge v3, v1, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v2}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v4, v2, Landroid/support/v7/widget/ce;->f:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    iget v2, v2, Landroid/support/v7/widget/ce;->b:I

    :goto_3
    iget-object v4, p0, Landroid/support/v7/widget/GridLayoutManager;->d:Landroid/util/SparseIntArray;

    iget v5, v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;->b:I

    invoke-virtual {v4, v2, v5}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v4, p0, Landroid/support/v7/widget/GridLayoutManager;->e:Landroid/util/SparseIntArray;

    iget v0, v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;->a:I

    invoke-virtual {v4, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    iget v2, v2, Landroid/support/v7/widget/ce;->f:I

    goto :goto_3

    .line 151
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/ai;->c(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)V

    .line 155
    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    iget-object v0, p0, Landroid/support/v7/widget/GridLayoutManager;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 156
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    iget-object v0, v0, Landroid/support/v7/widget/ag;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 196
    return-void
.end method
