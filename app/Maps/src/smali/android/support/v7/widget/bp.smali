.class Landroid/support/v7/widget/bp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/support/v7/widget/bo;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 7805
    iput-object p1, p0, Landroid/support/v7/widget/bp;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/ce;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7809
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ce;->a(Z)V

    .line 7810
    iget-object v2, p0, Landroid/support/v7/widget/bp;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    .line 7811
    iget-object v0, p0, Landroid/support/v7/widget/bp;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 7813
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 7810
    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/ce;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 7817
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ce;->a(Z)V

    .line 7818
    iget v1, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x10

    if-nez v1, :cond_1

    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    .line 7819
    iget-object v0, p0, Landroid/support/v7/widget/bp;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z

    .line 7821
    :cond_0
    return-void

    .line 7818
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/support/v7/widget/ce;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 7825
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ce;->a(Z)V

    .line 7826
    iget v1, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x10

    if-nez v1, :cond_1

    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    .line 7827
    iget-object v0, p0, Landroid/support/v7/widget/bp;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z

    .line 7829
    :cond_0
    return-void

    .line 7826
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/support/v7/widget/ce;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 7833
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ce;->a(Z)V

    .line 7861
    iget-object v1, p1, Landroid/support/v7/widget/ce;->g:Landroid/support/v7/widget/ce;

    if-eqz v1, :cond_0

    iget-object v1, p1, Landroid/support/v7/widget/ce;->h:Landroid/support/v7/widget/ce;

    if-nez v1, :cond_0

    .line 7862
    iput-object v5, p1, Landroid/support/v7/widget/ce;->g:Landroid/support/v7/widget/ce;

    .line 7863
    const/16 v1, -0x41

    iget v2, p1, Landroid/support/v7/widget/ce;->i:I

    iget v3, p1, Landroid/support/v7/widget/ce;->i:I

    xor-int/lit8 v4, v2, -0x1

    and-int/2addr v3, v4

    and-int/2addr v1, v2

    or-int/2addr v1, v3

    iput v1, p1, Landroid/support/v7/widget/ce;->i:I

    .line 7867
    :cond_0
    iput-object v5, p1, Landroid/support/v7/widget/ce;->h:Landroid/support/v7/widget/ce;

    .line 7868
    iget v1, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x10

    if-nez v1, :cond_2

    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_2

    :goto_0
    if-eqz v0, :cond_1

    .line 7869
    iget-object v0, p0, Landroid/support/v7/widget/bp;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z

    .line 7871
    :cond_1
    return-void

    .line 7868
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
