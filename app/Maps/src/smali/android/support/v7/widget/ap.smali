.class public abstract Landroid/support/v7/widget/ap;
.super Landroid/support/v7/widget/ca;
.source "PG"


# instance fields
.field private final a:F

.field public final b:Landroid/view/animation/LinearInterpolator;

.field public final c:Landroid/view/animation/DecelerateInterpolator;

.field public d:Landroid/graphics/PointF;

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Landroid/support/v7/widget/ca;-><init>()V

    .line 78
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ap;->b:Landroid/view/animation/LinearInterpolator;

    .line 80
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ap;->c:Landroid/view/animation/DecelerateInterpolator;

    .line 88
    iput v1, p0, Landroid/support/v7/widget/ap;->e:I

    iput v1, p0, Landroid/support/v7/widget/ap;->f:I

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v1, 0x41c80000    # 25.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Landroid/support/v7/widget/ap;->a:F

    .line 92
    return-void
.end method

.method private static a(IIIII)I
    .locals 2

    .prologue
    .line 265
    packed-switch p4, :pswitch_data_0

    .line 281
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :pswitch_0
    sub-int v0, p2, p0

    .line 284
    :cond_0
    :goto_0
    return v0

    .line 269
    :pswitch_1
    sub-int v0, p3, p1

    goto :goto_0

    .line 271
    :pswitch_2
    sub-int v0, p2, p0

    .line 272
    if-gtz v0, :cond_0

    .line 275
    sub-int v0, p3, p1

    .line 276
    if-ltz v0, :cond_0

    .line 284
    const/4 v0, 0x0

    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/View;I)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 299
    iget-object v3, p0, Landroid/support/v7/widget/ca;->i:Landroid/support/v7/widget/bs;

    .line 300
    invoke-virtual {v3}, Landroid/support/v7/widget/bs;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 309
    :goto_0
    return v2

    .line 303
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 305
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v4, v1

    iget v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    sub-int v4, v1, v4

    .line 306
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v5

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int v5, v1, v0

    .line 307
    iget-object v0, v3, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    iget-object v0, v3, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v0

    .line 308
    :goto_1
    iget-object v1, v3, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_3

    iget-object v1, v3, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    :goto_2
    iget-object v6, v3, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v6, :cond_1

    iget-object v2, v3, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    :cond_1
    sub-int/2addr v1, v2

    .line 309
    invoke-static {v4, v5, v0, v1, p2}, Landroid/support/v7/widget/ap;->a(IIIII)I

    move-result v2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 307
    goto :goto_1

    :cond_3
    move v1, v2

    .line 308
    goto :goto_2
.end method

.method public abstract a(I)Landroid/graphics/PointF;
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ap;->f:I

    iput v0, p0, Landroid/support/v7/widget/ap;->e:I

    .line 145
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ap;->d:Landroid/graphics/PointF;

    .line 146
    return-void
.end method

.method protected final a(IILandroid/support/v7/widget/cb;)V
    .locals 8

    .prologue
    const v7, 0x461c4000    # 10000.0f

    const/4 v3, 0x0

    const/4 v1, 0x0

    const v6, 0x3f99999a    # 1.2f

    .line 121
    iget-object v0, p0, Landroid/support/v7/widget/ca;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v2, v0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v2, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v2}, Landroid/support/v7/widget/t;->a()I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v2, v0

    :goto_0
    if-nez v0, :cond_2

    .line 122
    invoke-virtual {p0}, Landroid/support/v7/widget/ap;->b()V

    .line 137
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 121
    goto :goto_0

    .line 130
    :cond_2
    iget v2, p0, Landroid/support/v7/widget/ap;->e:I

    sub-int v0, v2, p1

    mul-int/2addr v2, v0

    if-gtz v2, :cond_3

    move v0, v1

    :cond_3
    iput v0, p0, Landroid/support/v7/widget/ap;->e:I

    .line 131
    iget v2, p0, Landroid/support/v7/widget/ap;->f:I

    sub-int v0, v2, p2

    mul-int/2addr v2, v0

    if-gtz v2, :cond_5

    :goto_2
    iput v1, p0, Landroid/support/v7/widget/ap;->f:I

    .line 133
    iget v0, p0, Landroid/support/v7/widget/ap;->e:I

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/ap;->f:I

    if-nez v0, :cond_0

    .line 134
    iget v0, p0, Landroid/support/v7/widget/ca;->g:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ap;->a(I)Landroid/graphics/PointF;

    move-result-object v0

    if-eqz v0, :cond_4

    iget v1, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_6

    iget v1, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_6

    :cond_4
    iget v0, p0, Landroid/support/v7/widget/ca;->g:I

    invoke-virtual {p0}, Landroid/support/v7/widget/ap;->b()V

    iget-object v1, p0, Landroid/support/v7/widget/ca;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    goto :goto_1

    :cond_5
    move v1, v0

    .line 131
    goto :goto_2

    .line 134
    :cond_6
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v2

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v3, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iget v1, v0, Landroid/graphics/PointF;->x:F

    float-to-double v4, v1

    div-double/2addr v4, v2

    double-to-float v1, v4

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, v0, Landroid/graphics/PointF;->y:F

    float-to-double v4, v1

    div-double v2, v4, v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iput-object v0, p0, Landroid/support/v7/widget/ap;->d:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/ap;->e:I

    iget v0, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/ap;->f:I

    const/16 v0, 0x2710

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Landroid/support/v7/widget/ap;->a:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget v1, p0, Landroid/support/v7/widget/ap;->e:I

    int-to-float v1, v1

    mul-float/2addr v1, v6

    float-to-int v1, v1

    iget v2, p0, Landroid/support/v7/widget/ap;->f:I

    int-to-float v2, v2

    mul-float/2addr v2, v6

    float-to-int v2, v2

    int-to-float v0, v0

    mul-float/2addr v0, v6

    float-to-int v0, v0

    iget-object v3, p0, Landroid/support/v7/widget/ap;->b:Landroid/view/animation/LinearInterpolator;

    iput v1, p3, Landroid/support/v7/widget/cb;->a:I

    iput v2, p3, Landroid/support/v7/widget/cb;->b:I

    iput v0, p3, Landroid/support/v7/widget/cb;->c:I

    iput-object v3, p3, Landroid/support/v7/widget/cb;->d:Landroid/view/animation/Interpolator;

    const/4 v0, 0x1

    iput-boolean v0, p3, Landroid/support/v7/widget/cb;->e:Z

    goto/16 :goto_1
.end method

.method public a(Landroid/view/View;Landroid/support/v7/widget/cb;)V
    .locals 11

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 107
    iget-object v0, p0, Landroid/support/v7/widget/ap;->d:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ap;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v10

    if-nez v0, :cond_4

    :cond_0
    move v2, v5

    :goto_0
    iget-object v6, p0, Landroid/support/v7/widget/ca;->i:Landroid/support/v7/widget/bs;

    invoke-virtual {v6}, Landroid/support/v7/widget/bs;->d()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v5

    .line 108
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/ap;->d:Landroid/graphics/PointF;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/ap;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-nez v1, :cond_a

    :cond_1
    move v4, v5

    :cond_2
    :goto_2
    invoke-virtual {p0, p1, v4}, Landroid/support/v7/widget/ap;->a(Landroid/view/View;I)I

    move-result v1

    .line 109
    mul-int v2, v0, v0

    mul-int v4, v1, v1

    add-int/2addr v2, v4

    int-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 110
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    iget v4, p0, Landroid/support/v7/widget/ap;->a:F

    mul-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v2, v4

    int-to-double v4, v2

    const-wide v6, 0x3fd57a786c22680aL    # 0.3356

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 111
    if-lez v2, :cond_3

    .line 112
    neg-int v0, v0

    neg-int v1, v1

    iget-object v4, p0, Landroid/support/v7/widget/ap;->c:Landroid/view/animation/DecelerateInterpolator;

    iput v0, p2, Landroid/support/v7/widget/cb;->a:I

    iput v1, p2, Landroid/support/v7/widget/cb;->b:I

    iput v2, p2, Landroid/support/v7/widget/cb;->c:I

    iput-object v4, p2, Landroid/support/v7/widget/cb;->d:Landroid/view/animation/Interpolator;

    iput-boolean v3, p2, Landroid/support/v7/widget/cb;->e:Z

    .line 114
    :cond_3
    return-void

    .line 107
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/ap;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v10

    if-lez v0, :cond_5

    move v2, v3

    goto :goto_0

    :cond_5
    move v2, v4

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v7

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v1, v7, v1

    iget v7, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    sub-int v7, v1, v7

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v8

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int v8, v1, v0

    iget-object v0, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_7

    iget-object v0, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    :goto_3
    iget-object v1, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_8

    iget-object v1, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    :goto_4
    iget-object v9, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v9, :cond_9

    iget-object v6, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v6

    :goto_5
    sub-int/2addr v1, v6

    invoke-static {v7, v8, v0, v1, v2}, Landroid/support/v7/widget/ap;->a(IIIII)I

    move-result v0

    goto/16 :goto_1

    :cond_7
    move v0, v5

    goto :goto_3

    :cond_8
    move v1, v5

    goto :goto_4

    :cond_9
    move v6, v5

    goto :goto_5

    .line 108
    :cond_a
    iget-object v1, p0, Landroid/support/v7/widget/ap;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_2

    move v4, v3

    goto/16 :goto_2
.end method
