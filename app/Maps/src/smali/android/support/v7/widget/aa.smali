.class Landroid/support/v7/widget/aa;
.super Landroid/support/v7/widget/af;
.source "PG"


# instance fields
.field final synthetic a:Landroid/support/v7/widget/ce;

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:Landroid/support/v4/view/cj;

.field final synthetic e:Landroid/support/v7/widget/u;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/u;Landroid/support/v7/widget/ce;IILandroid/support/v4/view/cj;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Landroid/support/v7/widget/aa;->e:Landroid/support/v7/widget/u;

    iput-object p2, p0, Landroid/support/v7/widget/aa;->a:Landroid/support/v7/widget/ce;

    iput p3, p0, Landroid/support/v7/widget/aa;->b:I

    iput p4, p0, Landroid/support/v7/widget/aa;->c:I

    iput-object p5, p0, Landroid/support/v7/widget/aa;->d:Landroid/support/v4/view/cj;

    invoke-direct {p0}, Landroid/support/v7/widget/af;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Landroid/support/v7/widget/aa;->e:Landroid/support/v7/widget/u;

    iget-object v0, p0, Landroid/support/v7/widget/aa;->a:Landroid/support/v7/widget/ce;

    .line 288
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 300
    iget-object v1, p0, Landroid/support/v7/widget/aa;->d:Landroid/support/v4/view/cj;

    const/4 v2, 0x0

    iget-object v0, v1, Landroid/support/v4/view/cj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v3, Landroid/support/v4/view/cj;->c:Landroid/support/v4/view/cr;

    invoke-interface {v3, v1, v0, v2}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/cj;Landroid/view/View;Landroid/support/v4/view/cw;)V

    .line 301
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aa;->e:Landroid/support/v7/widget/u;

    iget-object v1, p0, Landroid/support/v7/widget/aa;->a:Landroid/support/v7/widget/ce;

    iget-object v2, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/bo;->c(Landroid/support/v7/widget/ce;)V

    .line 302
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/aa;->e:Landroid/support/v7/widget/u;

    iget-object v0, v0, Landroid/support/v7/widget/u;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/aa;->a:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 303
    iget-object v0, p0, Landroid/support/v7/widget/aa;->e:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->b()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->d()V

    .line 304
    :cond_2
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    iget v0, p0, Landroid/support/v7/widget/aa;->b:I

    if-eqz v0, :cond_0

    .line 292
    invoke-static {p1, v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 294
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/aa;->c:I

    if-eqz v0, :cond_1

    .line 295
    invoke-static {p1, v1}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 297
    :cond_1
    return-void
.end method
