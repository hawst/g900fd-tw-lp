.class final Landroid/support/v7/widget/bd;
.super Landroid/support/v7/widget/bc;
.source "PG"


# direct methods
.method constructor <init>(Landroid/support/v7/widget/bs;)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0, p1}, Landroid/support/v7/widget/bc;-><init>(Landroid/support/v7/widget/bs;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    iget-object v1, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 240
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 242
    iget-object v1, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v1, v2, v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bs;->f(I)V

    .line 208
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 197
    iget-object v0, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    iget-object v2, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    iget-object v3, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_0

    iget-object v1, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    :cond_0
    sub-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 233
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 235
    iget-object v1, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    iget-object v1, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 217
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 219
    iget-object v1, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    invoke-static {p1}, Landroid/support/v7/widget/bs;->c(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final d()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 247
    iget-object v0, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    iget-object v2, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    iget-object v3, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_2

    iget-object v2, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    :goto_1
    sub-int/2addr v0, v2

    iget-object v2, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    iget-object v3, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_0

    iget-object v1, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    :cond_0
    sub-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public final d(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 225
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 227
    iget-object v1, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    invoke-static {p1}, Landroid/support/v7/widget/bs;->d(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Landroid/support/v7/widget/bd;->a:Landroid/support/v7/widget/bs;

    iget-object v1, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
