.class public Landroid/support/v7/widget/cb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Landroid/view/animation/Interpolator;

.field public e:Z

.field private f:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 7363
    const/high16 v0, -0x80000000

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v7/widget/cb;-><init>(IIILandroid/view/animation/Interpolator;)V

    .line 7364
    return-void
.end method

.method private constructor <init>(IIILandroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7352
    iput-boolean v0, p0, Landroid/support/v7/widget/cb;->e:Z

    .line 7356
    iput v0, p0, Landroid/support/v7/widget/cb;->f:I

    .line 7383
    iput p1, p0, Landroid/support/v7/widget/cb;->a:I

    .line 7384
    iput p2, p0, Landroid/support/v7/widget/cb;->b:I

    .line 7385
    iput p3, p0, Landroid/support/v7/widget/cb;->c:I

    .line 7386
    iput-object p4, p0, Landroid/support/v7/widget/cb;->d:Landroid/view/animation/Interpolator;

    .line 7387
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/cb;Landroid/support/v7/widget/RecyclerView;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 7340
    iget-boolean v0, p0, Landroid/support/v7/widget/cb;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/cb;->d:Landroid/view/animation/Interpolator;

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/cb;->c:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "If you provide an interpolator, you must set a positive duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/cb;->c:I

    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Scroll duration must be a positive number"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/cb;->d:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_3

    iget v0, p0, Landroid/support/v7/widget/cb;->c:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_2

    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    iget v1, p0, Landroid/support/v7/widget/cb;->a:I

    iget v2, p0, Landroid/support/v7/widget/cb;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/cd;->a(II)V

    :goto_0
    iget v0, p0, Landroid/support/v7/widget/cb;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/cb;->f:I

    iget v0, p0, Landroid/support/v7/widget/cb;->f:I

    iput-boolean v5, p0, Landroid/support/v7/widget/cb;->e:Z

    :goto_1
    return-void

    :cond_2
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    iget v1, p0, Landroid/support/v7/widget/cb;->a:I

    iget v2, p0, Landroid/support/v7/widget/cb;->b:I

    iget v3, p0, Landroid/support/v7/widget/cb;->c:I

    sget-object v4, Landroid/support/v7/widget/RecyclerView;->G:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/cd;->a(IIILandroid/view/animation/Interpolator;)V

    goto :goto_0

    :cond_3
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    iget v1, p0, Landroid/support/v7/widget/cb;->a:I

    iget v2, p0, Landroid/support/v7/widget/cb;->b:I

    iget v3, p0, Landroid/support/v7/widget/cb;->c:I

    iget-object v4, p0, Landroid/support/v7/widget/cb;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/cd;->a(IIILandroid/view/animation/Interpolator;)V

    goto :goto_0

    :cond_4
    iput v5, p0, Landroid/support/v7/widget/cb;->f:I

    goto :goto_1
.end method
