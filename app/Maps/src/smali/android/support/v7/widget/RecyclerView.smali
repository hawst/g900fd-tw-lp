.class public Landroid/support/v7/widget/RecyclerView;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field static final G:Landroid/view/animation/Interpolator;

.field static final a:Z


# instance fields
.field public final A:Landroid/support/v7/widget/cc;

.field public B:Landroid/support/v7/widget/bu;

.field C:Z

.field D:Z

.field E:Z

.field F:Landroid/support/v7/widget/cf;

.field private final H:Landroid/support/v7/widget/bx;

.field private I:Landroid/support/v7/widget/by;

.field private J:Z

.field private final K:Landroid/graphics/Rect;

.field private final L:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/bt;",
            ">;"
        }
    .end annotation
.end field

.field private M:Landroid/support/v7/widget/bt;

.field private N:I

.field private O:Landroid/view/VelocityTracker;

.field private P:I

.field private Q:I

.field private R:I

.field private S:I

.field private T:I

.field private final U:I

.field private final V:I

.field private W:Landroid/support/v7/widget/bo;

.field private aa:Ljava/lang/Runnable;

.field public final b:Landroid/support/v7/widget/bw;

.field c:Landroid/support/v7/widget/h;

.field d:Landroid/support/v7/widget/r;

.field final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final f:Ljava/lang/Runnable;

.field public g:Landroid/support/v7/widget/bk;

.field public h:Landroid/support/v7/widget/bs;

.field public final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/bq;",
            ">;"
        }
    .end annotation
.end field

.field j:Z

.field public k:Z

.field l:Z

.field m:Z

.field n:Z

.field o:Z

.field final p:Z

.field final q:Landroid/view/accessibility/AccessibilityManager;

.field r:Z

.field s:Z

.field t:Landroid/support/v4/widget/x;

.field u:Landroid/support/v4/widget/x;

.field v:Landroid/support/v4/widget/x;

.field w:Landroid/support/v4/widget/x;

.field x:Landroid/support/v7/widget/bn;

.field public y:I

.field final z:Landroid/support/v7/widget/cd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 99
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->a:Z

    .line 266
    new-instance v0, Landroid/support/v7/widget/bh;

    invoke-direct {v0}, Landroid/support/v7/widget/bh;-><init>()V

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/view/animation/Interpolator;

    return-void

    .line 99
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 275
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 279
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 282
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 126
    new-instance v0, Landroid/support/v7/widget/bx;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bx;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/bx;

    .line 128
    new-instance v0, Landroid/support/v7/widget/bw;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bw;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    .line 151
    new-instance v0, Landroid/support/v7/widget/bf;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bf;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Ljava/lang/Runnable;

    .line 172
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:Ljava/util/ArrayList;

    .line 193
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    .line 203
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->s:Z

    .line 207
    new-instance v0, Landroid/support/v7/widget/u;

    invoke-direct {v0}, Landroid/support/v7/widget/u;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    .line 232
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->y:I

    .line 233
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 243
    new-instance v0, Landroid/support/v7/widget/cd;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cd;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    .line 245
    new-instance v0, Landroid/support/v7/widget/cc;

    invoke-direct {v0}, Landroid/support/v7/widget/cc;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    .line 250
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 251
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    .line 252
    new-instance v0, Landroid/support/v7/widget/bp;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bp;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/support/v7/widget/bo;

    .line 254
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    .line 256
    new-instance v0, Landroid/support/v7/widget/bg;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bg;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    .line 284
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 285
    const/16 v3, 0x10

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Z

    .line 287
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 288
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    .line 289
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Landroid/support/v7/widget/RecyclerView;->U:I

    .line 290
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    .line 291
    invoke-static {p0}, Landroid/support/v4/view/at;->a(Landroid/view/View;)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    .line 293
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/support/v7/widget/bo;

    iput-object v2, v0, Landroid/support/v7/widget/bn;->h:Landroid/support/v7/widget/bo;

    .line 294
    new-instance v0, Landroid/support/v7/widget/h;

    new-instance v2, Landroid/support/v7/widget/bj;

    invoke-direct {v2, p0}, Landroid/support/v7/widget/bj;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v2}, Landroid/support/v7/widget/h;-><init>(Landroid/support/v7/widget/i;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    .line 295
    new-instance v0, Landroid/support/v7/widget/r;

    new-instance v2, Landroid/support/v7/widget/bi;

    invoke-direct {v2, p0}, Landroid/support/v7/widget/bi;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v2}, Landroid/support/v7/widget/r;-><init>(Landroid/support/v7/widget/t;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    .line 297
    invoke-static {p0}, Landroid/support/v4/view/at;->e(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_1

    .line 299
    invoke-static {p0, v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;I)V

    .line 302
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/view/accessibility/AccessibilityManager;

    .line 304
    new-instance v0, Landroid/support/v7/widget/cf;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cf;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v7/widget/cf;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v7/widget/cf;

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 305
    return-void

    :cond_2
    move v0, v2

    .line 285
    goto :goto_0
.end method

.method private a(Landroid/support/v4/g/a;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/g/a",
            "<",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2127
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 2128
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_4

    .line 2129
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/View;

    .line 2130
    if-nez v5, :cond_1

    const/4 v1, 0x0

    .line 2131
    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/br;

    .line 2132
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v2, v2, Landroid/support/v7/widget/cc;->i:Z

    if-nez v2, :cond_0

    .line 2133
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v2, v2, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    invoke-virtual {v2, v1}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2135
    :cond_0
    invoke-virtual {p1, v5}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2136
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, v5, v1}, Landroid/support/v7/widget/bs;->a(Landroid/view/View;Landroid/support/v7/widget/bw;)V

    .line 2128
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2130
    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_1

    .line 2139
    :cond_2
    if-eqz v0, :cond_3

    .line 2140
    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/br;)V

    goto :goto_2

    .line 2143
    :cond_3
    new-instance v0, Landroid/support/v7/widget/br;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/br;-><init>(Landroid/support/v7/widget/ce;IIII)V

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/br;)V

    goto :goto_2

    .line 2147
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2148
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->detachViewFromParent(I)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;IIII)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView;->onScrollChanged(IIII)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Landroid/support/v7/widget/br;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2177
    iget-object v0, p1, Landroid/support/v7/widget/br;->a:Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 2178
    iget-object v1, p1, Landroid/support/v7/widget/br;->a:Landroid/support/v7/widget/ce;

    invoke-direct {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/ce;)V

    .line 2179
    iget v2, p1, Landroid/support/v7/widget/br;->b:I

    .line 2180
    iget v3, p1, Landroid/support/v7/widget/br;->c:I

    .line 2181
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 2182
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    .line 2183
    if-ne v2, v4, :cond_0

    if-eq v3, v5, :cond_2

    .line 2184
    :cond_0
    iget-object v1, p1, Landroid/support/v7/widget/br;->a:Landroid/support/v7/widget/ce;

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/ce;->a(Z)V

    .line 2185
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v1, v4

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v5

    invoke-virtual {v0, v4, v5, v1, v6}, Landroid/view/View;->layout(IIII)V

    .line 2192
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    iget-object v1, p1, Landroid/support/v7/widget/br;->a:Landroid/support/v7/widget/ce;

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/bn;->a(Landroid/support/v7/widget/ce;IIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2194
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    iput-boolean v7, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    .line 2206
    :cond_1
    :goto_0
    return-void

    .line 2201
    :cond_2
    iget-object v0, p1, Landroid/support/v7/widget/br;->a:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/ce;->a(Z)V

    .line 2202
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    iget-object v1, p1, Landroid/support/v7/widget/br;->a:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bn;->a(Landroid/support/v7/widget/ce;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2203
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    iput-boolean v7, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/ce;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, -0x1

    const/4 v1, 0x1

    .line 707
    iget-object v3, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 708
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_1

    move v0, v1

    .line 709
    :goto_0
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/bw;->b(Landroid/support/v7/widget/ce;)V

    .line 710
    iget v4, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_0

    move v2, v1

    :cond_0
    if-eqz v2, :cond_2

    .line 712
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v3, v6, v2, v1}, Landroid/support/v7/widget/r;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    .line 718
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 708
    goto :goto_0

    .line 713
    :cond_2
    if-nez v0, :cond_3

    .line 714
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3, v6, v1}, Landroid/support/v7/widget/r;->a(Landroid/view/View;IZ)V

    goto :goto_1

    .line 716
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1, v3}, Landroid/support/v7/widget/t;->a(Landroid/view/View;)I

    move-result v1

    if-gez v1, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "view is not a child, cannot hide "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v2, v0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/s;->a(I)V

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 1727
    invoke-static {p1}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1728
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    if-ne v1, v2, :cond_0

    .line 1730
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1731
    :goto_0
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 1732
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 1733
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    .line 1735
    :cond_0
    return-void

    .line 1730
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 87
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v1, :cond_0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v3, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3, p1}, Landroid/support/v7/widget/t;->a(Landroid/view/View;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bw;->b(Landroid/support/v7/widget/ce;)V

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bw;->a(Landroid/support/v7/widget/ce;)V

    :cond_1
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    return v1

    :cond_2
    iget-object v4, v1, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/s;->b(I)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v1, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/s;->c(I)Z

    iget-object v4, v1, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v4, v3}, Landroid/support/v7/widget/t;->a(I)V

    iget-object v1, v1, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_1
.end method

.method static b(Landroid/view/View;)Landroid/support/v7/widget/ce;
    .locals 1

    .prologue
    .line 2576
    if-nez p0, :cond_0

    .line 2577
    const/4 v0, 0x0

    .line 2579
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_0
.end method

.method private b(II)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 974
    .line 976
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 977
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_e

    .line 978
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v0, :cond_0

    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    .line 979
    :cond_0
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->s:Z

    .line 980
    if-eqz p1, :cond_d

    .line 981
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, p1, v2, v3}, Landroid/support/v7/widget/bs;->a(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v3

    .line 982
    sub-int v4, p1, v3

    .line 984
    :goto_0
    if-eqz p2, :cond_c

    .line 985
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, p2, v2, v5}, Landroid/support/v7/widget/bs;->b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    .line 986
    sub-int v2, p2, v0

    .line 988
    :goto_1
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v5, :cond_1

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    .line 1009
    :cond_1
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->s:Z

    .line 1010
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    move v8, v3

    move v3, v2

    move v2, v8

    .line 1012
    :goto_2
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1013
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 1015
    :cond_2
    invoke-static {p0}, Landroid/support/v4/view/at;->a(Landroid/view/View;)I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_6

    .line 1016
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    .line 1017
    if-gez v4, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->c()V

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    neg-int v6, v4

    int-to-float v6, v6

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    sget-object v7, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v5, v5, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v7, v5, v6}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;F)Z

    :cond_3
    :goto_3
    if-gez v3, :cond_b

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->e()V

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    neg-int v6, v3

    int-to-float v6, v6

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    sget-object v7, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v5, v5, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v7, v5, v6}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;F)Z

    :cond_4
    :goto_4
    if-nez v4, :cond_5

    if-eqz v3, :cond_6

    :cond_5
    invoke-static {p0}, Landroid/support/v4/view/at;->d(Landroid/view/View;)V

    .line 1019
    :cond_6
    if-nez v2, :cond_7

    if-eqz v0, :cond_8

    .line 1020
    :cond_7
    invoke-virtual {p0, v1, v1, v1, v1}, Landroid/support/v7/widget/RecyclerView;->onScrollChanged(IIII)V

    .line 1021
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/support/v7/widget/bu;

    if-eqz v1, :cond_8

    .line 1022
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/support/v7/widget/bu;

    invoke-virtual {v1, p0, v2, v0}, Landroid/support/v7/widget/bu;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1025
    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1026
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 1028
    :cond_9
    return-void

    .line 1017
    :cond_a
    if-lez v4, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    int-to-float v6, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    sget-object v7, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v5, v5, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v7, v5, v6}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;F)Z

    goto :goto_3

    :cond_b
    if-lez v3, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->f()V

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    int-to-float v6, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    sget-object v7, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v5, v5, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v7, v5, v6}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;F)Z

    goto :goto_4

    :cond_c
    move v0, v1

    move v2, v1

    goto/16 :goto_1

    :cond_d
    move v3, v1

    move v4, v1

    goto/16 :goto_0

    :cond_e
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_2
.end method

.method public static c(Landroid/view/View;)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 2589
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 2590
    :goto_0
    if-eqz v0, :cond_2

    iget v2, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v2, v1, :cond_1

    iget v0, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_1
    return v0

    .line 2589
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_0

    .line 2590
    :cond_1
    iget v0, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1220
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 1221
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    iget-object v1, v0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, v0, Landroid/support/v7/widget/cd;->c:Landroid/support/v4/widget/aq;

    iget-object v1, v0, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v0, v0, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Landroid/support/v4/widget/ar;->f(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->i()V

    .line 1222
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1258
    const/4 v0, 0x0

    .line 1259
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    sget-object v1, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Landroid/support/v4/widget/aa;->c(Ljava/lang/Object;)Z

    move-result v0

    .line 1260
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->c(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1261
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->c(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1262
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->c(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1263
    :cond_3
    if-eqz v0, :cond_4

    .line 1264
    invoke-static {p0}, Landroid/support/v4/view/at;->d(Landroid/view/View;)V

    .line 1266
    :cond_4
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1837
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    if-eqz v0, :cond_0

    .line 1840
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    iget-object v3, v0, Landroid/support/v7/widget/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/h;->a(Ljava/util/List;)V

    iget-object v3, v0, Landroid/support/v7/widget/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/h;->a(Ljava/util/List;)V

    .line 1841
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->m()V

    .line 1842
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 1847
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1848
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0}, Landroid/support/v7/widget/h;->a()V

    .line 1852
    :goto_0
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    if-eqz v0, :cond_6

    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    :cond_2
    move v0, v1

    .line 1854
    :goto_1
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v3, :cond_7

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v3, :cond_7

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    if-nez v3, :cond_3

    if-nez v0, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-boolean v3, v3, Landroid/support/v7/widget/bs;->p:Z

    if-eqz v3, :cond_7

    :cond_3
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v3, v3, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v3, :cond_7

    :cond_4
    move v3, v2

    :goto_2
    iput-boolean v3, v4, Landroid/support/v7/widget/cc;->j:Z

    .line 1858
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v4, v4, Landroid/support/v7/widget/cc;->j:Z

    if-eqz v4, :cond_9

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_3
    if-eqz v0, :cond_9

    :goto_4
    iput-boolean v2, v3, Landroid/support/v7/widget/cc;->k:Z

    .line 1861
    return-void

    .line 1850
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0}, Landroid/support/v7/widget/h;->c()V

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1852
    goto :goto_1

    :cond_7
    move v3, v1

    .line 1854
    goto :goto_2

    :cond_8
    move v0, v1

    .line 1858
    goto :goto_3

    :cond_9
    move v2, v1

    goto :goto_4
.end method

.method private l()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 2375
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    move v2, v1

    .line 2376
    :goto_0
    if-ge v2, v4, :cond_3

    .line 2377
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v2}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2378
    :goto_1
    iget v3, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_2
    if-nez v3, :cond_0

    .line 2379
    iput v5, v0, Landroid/support/v7/widget/ce;->c:I

    iput v5, v0, Landroid/support/v7/widget/ce;->f:I

    .line 2376
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2377
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_1

    :cond_2
    move v3, v1

    .line 2378
    goto :goto_2

    .line 2382
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v3, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_4

    iget-object v0, v3, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iput v5, v0, Landroid/support/v7/widget/ce;->c:I

    iput v5, v0, Landroid/support/v7/widget/ce;->f:I

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    iget-object v0, v3, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_5

    iget-object v0, v3, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iput v5, v0, Landroid/support/v7/widget/ce;->c:I

    iput v5, v0, Landroid/support/v7/widget/ce;->f:I

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_5
    iget-object v0, v3, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, v3, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_5
    if-ge v1, v2, :cond_6

    iget-object v0, v3, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iput v5, v0, Landroid/support/v7/widget/ce;->c:I

    iput v5, v0, Landroid/support/v7/widget/ce;->f:I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 2383
    :cond_6
    return-void
.end method

.method private m()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v1, 0x0

    .line 2533
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0}, Landroid/support/v7/widget/t;->a()I

    move-result v4

    move v2, v1

    .line 2534
    :goto_0
    if-ge v2, v4, :cond_3

    .line 2535
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v2}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v3, v0

    .line 2536
    :goto_1
    if-eqz v3, :cond_0

    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_0

    .line 2537
    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v0, v5

    iput v0, v3, Landroid/support/v7/widget/ce;->i:I

    .line 2534
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2535
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    move-object v3, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2536
    goto :goto_2

    .line 2540
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->h()V

    .line 2541
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v2, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_5

    iget-object v0, v2, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v0, v0, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, v2, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_3
    if-ge v1, v3, :cond_7

    iget-object v0, v2, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_4

    iget v4, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v4, v5

    iput v4, v0, Landroid/support/v7/widget/ce;->i:I

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    iget-object v0, v2, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_7

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/bw;->c(I)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v2, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget v3, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v3, v5

    iput v3, v0, Landroid/support/v7/widget/ce;->i:I

    :cond_6
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    .line 2542
    :cond_7
    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v7/widget/bk;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    return-object v0
.end method

.method public final a(Landroid/view/View;)Landroid/support/v7/widget/ce;
    .locals 3

    .prologue
    .line 2567
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2568
    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    .line 2569
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a direct child of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2572
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_0
.end method

.method a(I)V
    .locals 2

    .prologue
    .line 812
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->y:I

    if-ne p1, v0, :cond_0

    .line 826
    :goto_0
    return-void

    .line 818
    :cond_0
    iput p1, p0, Landroid/support/v7/widget/RecyclerView;->y:I

    .line 819
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 820
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    iget-object v1, v0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, v0, Landroid/support/v7/widget/cd;->c:Landroid/support/v4/widget/aq;

    iget-object v1, v0, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v0, v0, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Landroid/support/v4/widget/ar;->f(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->i()V

    .line 822
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/support/v7/widget/bu;

    if-eqz v0, :cond_2

    .line 823
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/support/v7/widget/bu;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bu;->a(I)V

    .line 825
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bs;->h(I)V

    goto :goto_0
.end method

.method a(II)V
    .locals 3

    .prologue
    .line 1269
    const/4 v0, 0x0

    .line 1270
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-lez p1, :cond_0

    .line 1271
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    sget-object v1, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Landroid/support/v4/widget/aa;->c(Ljava/lang/Object;)Z

    move-result v0

    .line 1273
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    if-gez p1, :cond_1

    .line 1274
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->c(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1276
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    if-lez p2, :cond_2

    .line 1277
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->c(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1279
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    if-gez p2, :cond_3

    .line 1280
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    sget-object v2, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v1, v1, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v2, v1}, Landroid/support/v4/widget/aa;->c(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1282
    :cond_3
    if-eqz v0, :cond_4

    .line 1283
    invoke-static {p0}, Landroid/support/v4/view/at;->d(Landroid/view/View;)V

    .line 1285
    :cond_4
    return-void
.end method

.method final a(IIZ)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, -0x1

    const/4 v3, 0x1

    .line 2438
    add-int v5, p1, p2

    .line 2439
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0}, Landroid/support/v7/widget/t;->a()I

    move-result v6

    move v1, v2

    .line 2440
    :goto_0
    if-ge v1, v6, :cond_4

    .line 2441
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v4, v0

    .line 2442
    :goto_1
    if-eqz v4, :cond_0

    iget v0, v4, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_2

    move v0, v3

    :goto_2
    if-nez v0, :cond_0

    .line 2443
    iget v0, v4, Landroid/support/v7/widget/ce;->b:I

    if-lt v0, v5, :cond_3

    .line 2449
    neg-int v0, p2

    invoke-virtual {v4, v0, p3}, Landroid/support/v7/widget/ce;->a(IZ)V

    .line 2450
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v3, v0, Landroid/support/v7/widget/cc;->h:Z

    .line 2440
    :cond_0
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2441
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    move-object v4, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 2442
    goto :goto_2

    .line 2451
    :cond_3
    iget v0, v4, Landroid/support/v7/widget/ce;->b:I

    if-lt v0, p1, :cond_0

    .line 2456
    add-int/lit8 v0, p1, -0x1

    neg-int v7, p2

    const/16 v8, 0x8

    iget v9, v4, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v8, v9

    iput v8, v4, Landroid/support/v7/widget/ce;->i:I

    invoke-virtual {v4, v7, p3}, Landroid/support/v7/widget/ce;->a(IZ)V

    iput v0, v4, Landroid/support/v7/widget/ce;->b:I

    .line 2458
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v3, v0, Landroid/support/v7/widget/cc;->h:Z

    goto :goto_3

    .line 2462
    :cond_4
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    add-int v4, p1, p2

    iget-object v0, v3, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_4
    if-ltz v2, :cond_9

    iget-object v0, v3, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_5

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v1, v10, :cond_6

    iget v1, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_5
    if-lt v1, v4, :cond_7

    neg-int v1, p2

    invoke-virtual {v0, v1, p3}, Landroid/support/v7/widget/ce;->a(IZ)V

    :cond_5
    :goto_6
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_4

    :cond_6
    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_5

    :cond_7
    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v1, v10, :cond_8

    iget v1, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_7
    if-lt v1, p1, :cond_5

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/bw;->c(I)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x4

    iget v5, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v1, v5

    iput v1, v0, Landroid/support/v7/widget/ce;->i:I

    goto :goto_6

    :cond_8
    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_7

    .line 2463
    :cond_9
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 2464
    return-void
.end method

.method public final a(Landroid/support/v7/widget/bq;)V
    .locals 2

    .prologue
    .line 872
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    const-string v1, "Cannot add item decoration during a scroll  or layout"

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->h()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 873
    return-void
.end method

.method final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1168
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-eqz v0, :cond_1

    .line 1169
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_0

    .line 1171
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->g()V

    .line 1173
    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    .line 1174
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    .line 1176
    :cond_1
    return-void
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 1402
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-static {}, Landroid/support/v7/widget/bs;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1403
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1405
    :cond_0
    return-void
.end method

.method public final b()Landroid/support/v7/widget/bs;
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    return-object v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 915
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    iget-object v1, v0, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, v0, Landroid/support/v7/widget/cd;->c:Landroid/support/v4/widget/aq;

    iget-object v1, v0, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v0, v0, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Landroid/support/v4/widget/ar;->f(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->i()V

    .line 916
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bs;->c(I)V

    .line 917
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    .line 918
    return-void
.end method

.method final c()V
    .locals 4

    .prologue
    .line 1310
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_0

    .line 1320
    :goto_0
    return-void

    .line 1313
    :cond_0
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    .line 1314
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v0, :cond_1

    .line 1315
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;II)V

    goto :goto_0

    .line 1318
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;II)V

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 2333
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    check-cast p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/RecyclerView$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 2

    .prologue
    .line 1073
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->d(Landroid/support/v7/widget/cc;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 2

    .prologue
    .line 1050
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->b(Landroid/support/v7/widget/cc;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 2

    .prologue
    .line 1093
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->f(Landroid/support/v7/widget/cc;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeVerticalScrollExtent()I
    .locals 2

    .prologue
    .line 1136
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->e(Landroid/support/v7/widget/cc;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 2

    .prologue
    .line 1115
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->c(Landroid/support/v7/widget/cc;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeVerticalScrollRange()I
    .locals 2

    .prologue
    .line 1156
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->g(Landroid/support/v7/widget/cc;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2734
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 2735
    iget-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->e:Z

    if-nez v1, :cond_0

    .line 2736
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    .line 2751
    :goto_0
    return-object v0

    .line 2739
    :cond_0
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    .line 2740
    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2741
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v4

    .line 2742
    :goto_1
    if-ge v3, v5, :cond_2

    .line 2743
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2744
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/bq;

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    iget v7, v1, Landroid/support/v7/widget/ce;->f:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_1

    iget v1, v1, Landroid/support/v7/widget/ce;->b:I

    :goto_2
    invoke-virtual {v6, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2745
    iget v1, v2, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->left:I

    .line 2746
    iget v1, v2, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->top:I

    .line 2747
    iget v1, v2, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 2748
    iget v1, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->bottom:I

    .line 2742
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2744
    :cond_1
    iget v1, v1, Landroid/support/v7/widget/ce;->f:I

    goto :goto_2

    .line 2750
    :cond_2
    iput-boolean v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->e:Z

    move-object v0, v2

    .line 2751
    goto :goto_0
.end method

.method final d()V
    .locals 4

    .prologue
    .line 1323
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_0

    .line 1333
    :goto_0
    return-void

    .line 1326
    :cond_0
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    .line 1327
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v0, :cond_1

    .line 1328
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;II)V

    goto :goto_0

    .line 1331
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;II)V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2262
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 2264
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    .line 2265
    :goto_0
    if-ge v3, v4, :cond_0

    .line 2266
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bq;

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, p1, p0}, Landroid/support/v7/widget/bq;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;)V

    .line 2265
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2271
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 2272
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 2273
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    .line 2274
    :goto_1
    const/high16 v4, 0x43870000    # 270.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2275
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    neg-int v4, v4

    add-int/2addr v0, v4

    int-to-float v0, v0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2276
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    sget-object v4, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, p1}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    .line 2277
    :goto_2
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2279
    :goto_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    sget-object v4, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v3, v3, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v4, v3}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2280
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 2281
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v3, :cond_1

    .line 2282
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2284
    :cond_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_9

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    sget-object v5, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v3, v3, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v5, v3, p1}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v2

    :goto_4
    or-int/2addr v0, v3

    .line 2285
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2287
    :cond_2
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    sget-object v4, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v3, v3, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v4, v3}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2288
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 2289
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v5

    .line 2290
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v3, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    .line 2291
    :goto_5
    const/high16 v6, 0x42b40000    # 90.0f

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2292
    neg-int v3, v3

    int-to-float v3, v3

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2293
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_b

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    sget-object v5, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v3, v3, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v5, v3, p1}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_b

    move v3, v2

    :goto_6
    or-int/2addr v0, v3

    .line 2294
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2296
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_5

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    sget-object v4, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v3, v3, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v4, v3}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2297
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 2298
    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2299
    iget-boolean v4, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v4, :cond_c

    .line 2300
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    neg-int v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    neg-int v5, v5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2304
    :goto_7
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    if-eqz v4, :cond_4

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    sget-object v5, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v4, v4, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v5, v4, p1}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v1, v2

    :cond_4
    or-int/2addr v0, v1

    .line 2305
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2311
    :cond_5
    if-nez v0, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    invoke-virtual {v1}, Landroid/support/v7/widget/bn;->b()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2316
    :goto_8
    if-eqz v2, :cond_6

    .line 2317
    invoke-static {p0}, Landroid/support/v4/view/at;->d(Landroid/view/View;)V

    .line 2319
    :cond_6
    return-void

    :cond_7
    move v0, v1

    .line 2273
    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 2276
    goto/16 :goto_2

    :cond_9
    move v3, v1

    .line 2284
    goto/16 :goto_4

    :cond_a
    move v3, v1

    .line 2290
    goto/16 :goto_5

    :cond_b
    move v3, v1

    .line 2293
    goto :goto_6

    .line 2302
    :cond_c
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_7

    :cond_d
    move v2, v0

    goto :goto_8

    :cond_e
    move v0, v1

    goto/16 :goto_3
.end method

.method final e()V
    .locals 4

    .prologue
    .line 1336
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_0

    .line 1347
    :goto_0
    return-void

    .line 1339
    :cond_0
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    .line 1340
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v0, :cond_1

    .line 1341
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;II)V

    goto :goto_0

    .line 1344
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;II)V

    goto :goto_0
.end method

.method e(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 4460
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_0

    .line 4461
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-nez p1, :cond_1

    .line 4463
    :cond_0
    :goto_0
    return-void

    .line 4461
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_0
.end method

.method final f()V
    .locals 4

    .prologue
    .line 1350
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_0

    .line 1360
    :goto_0
    return-void

    .line 1353
    :cond_0
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    .line 1354
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v0, :cond_1

    .line 1355
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;II)V

    goto :goto_0

    .line 1358
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    sget-object v3, Landroid/support/v4/widget/x;->b:Landroid/support/v4/widget/aa;

    iget-object v0, v0, Landroid/support/v4/widget/x;->a:Ljava/lang/Object;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v4/widget/aa;->a(Ljava/lang/Object;II)V

    goto :goto_0
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1370
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-static {}, Landroid/support/v7/widget/bs;->g()Landroid/view/View;

    move-result-object v0

    .line 1371
    if-eqz v0, :cond_1

    .line 1381
    :cond_0
    :goto_0
    return-object v0

    .line 1374
    :cond_1
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    .line 1375
    invoke-virtual {v0, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1376
    if-nez v0, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v1, :cond_3

    .line 1377
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    iput-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    .line 1378
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, p2, v1, v2}, Landroid/support/v7/widget/bs;->c(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)Landroid/view/View;

    move-result-object v0

    .line 1379
    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 1381
    :cond_3
    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method final g()V
    .locals 14

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1886
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-nez v0, :cond_0

    .line 2097
    :goto_0
    return-void

    .line 1890
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1891
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v0, :cond_1

    iput-boolean v9, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    .line 1892
    :cond_1
    iput-boolean v9, p0, Landroid/support/v7/widget/RecyclerView;->s:Z

    .line 1894
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->k()V

    .line 1896
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v1, v1, Landroid/support/v7/widget/cc;->j:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    :cond_2
    iput-object v7, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    .line 1898
    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 1900
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v1, v1, Landroid/support/v7/widget/cc;->k:Z

    iput-boolean v1, v0, Landroid/support/v7/widget/cc;->i:Z

    .line 1901
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->a()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/cc;->e:I

    .line 1903
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->j:Z

    if-eqz v0, :cond_8

    .line 1905
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->clear()V

    .line 1906
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->clear()V

    .line 1907
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v10, v1, v0

    move v6, v8

    .line 1908
    :goto_1
    if-ge v6, v10, :cond_8

    .line 1909
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/r;->a(I)I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v1, v7

    .line 1910
    :goto_2
    iget v0, v1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    move v0, v9

    :goto_3
    if-nez v0, :cond_4

    iget v0, v1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    move v0, v9

    :goto_4
    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v0, v0, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v0, :cond_4

    .line 1911
    :cond_3
    iget-object v5, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 1914
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v11, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    new-instance v0, Landroid/support/v7/widget/br;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/br;-><init>(Landroid/support/v7/widget/ce;IIII)V

    invoke-virtual {v11, v1, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1908
    :cond_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 1909
    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_2

    :cond_6
    move v0, v8

    .line 1910
    goto :goto_3

    :cond_7
    move v0, v8

    goto :goto_4

    .line 1918
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->k:Z

    if-eqz v0, :cond_1a

    .line 1925
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    move v1, v8

    :goto_5
    if-ge v1, v3, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_a

    move-object v0, v7

    :goto_6
    iget v2, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_b

    move v2, v9

    :goto_7
    if-nez v2, :cond_9

    iget v2, v0, Landroid/support/v7/widget/ce;->c:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_9

    iget v2, v0, Landroid/support/v7/widget/ce;->b:I

    iput v2, v0, Landroid/support/v7/widget/ce;->c:I

    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_a
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_6

    :cond_b
    move v2, v8

    goto :goto_7

    .line 1927
    :cond_c
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    if-eqz v0, :cond_13

    .line 1928
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v4, v1, v0

    move v1, v8

    .line 1929
    :goto_8
    if-ge v1, v4, :cond_13

    .line 1930
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/r;->a(I)I

    move-result v2

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v2}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_e

    move-object v0, v7

    .line 1931
    :goto_9
    iget v2, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_f

    move v2, v9

    :goto_a
    if-eqz v2, :cond_d

    iget v2, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_10

    move v2, v9

    :goto_b
    if-nez v2, :cond_d

    iget v2, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_11

    move v2, v9

    :goto_c
    if-nez v2, :cond_d

    .line 1932
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v2, v2, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v2, :cond_12

    iget-wide v2, v0, Landroid/support/v7/widget/ce;->d:J

    .line 1933
    :goto_d
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v5, v5, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1934
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v2, v2, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v2, v0}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929
    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 1930
    :cond_e
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_9

    :cond_f
    move v2, v8

    .line 1931
    goto :goto_a

    :cond_10
    move v2, v8

    goto :goto_b

    :cond_11
    move v2, v8

    goto :goto_c

    .line 1932
    :cond_12
    iget v2, v0, Landroid/support/v7/widget/ce;->b:I

    int-to-long v2, v2

    goto :goto_d

    .line 1939
    :cond_13
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->h:Z

    .line 1940
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v8, v1, Landroid/support/v7/widget/cc;->h:Z

    .line 1942
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/bs;->c(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)V

    .line 1943
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v0, v1, Landroid/support/v7/widget/cc;->h:Z

    .line 1945
    new-instance v3, Landroid/support/v4/g/a;

    invoke-direct {v3}, Landroid/support/v4/g/a;-><init>()V

    move v2, v8

    .line 1946
    :goto_e
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v1, v0

    if-ge v2, v0, :cond_18

    .line 1948
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/r;->a(I)I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v4

    .line 1949
    if-nez v4, :cond_15

    move-object v0, v7

    :goto_f
    iget v0, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_16

    move v0, v9

    :goto_10
    if-nez v0, :cond_14

    move v1, v8

    .line 1950
    :goto_11
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v0

    if-ge v1, v0, :cond_3c

    .line 1953
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    iget-object v0, v0, Landroid/support/v4/g/p;->g:[Ljava/lang/Object;

    shl-int/lit8 v5, v1, 0x1

    aget-object v0, v0, v5

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 1954
    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    if-ne v0, v4, :cond_17

    move v0, v9

    .line 1959
    :goto_12
    if-nez v0, :cond_14

    .line 1960
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v10

    invoke-direct {v0, v1, v5, v6, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v4, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1946
    :cond_14
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_e

    .line 1949
    :cond_15
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_f

    :cond_16
    move v0, v8

    goto :goto_10

    .line 1952
    :cond_17
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    .line 1965
    :cond_18
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->l()V

    .line 1966
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0}, Landroid/support/v7/widget/h;->b()V

    move-object v6, v3

    .line 1983
    :goto_13
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->a()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/cc;->e:I

    .line 1984
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput v8, v0, Landroid/support/v7/widget/cc;->g:I

    .line 1987
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v8, v0, Landroid/support/v7/widget/cc;->i:Z

    .line 1988
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bs;->c(Landroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)V

    .line 1990
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v8, v0, Landroid/support/v7/widget/cc;->h:Z

    .line 1991
    iput-object v7, p0, Landroid/support/v7/widget/RecyclerView;->I:Landroid/support/v7/widget/by;

    .line 1994
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->j:Z

    if-eqz v0, :cond_21

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v0, :cond_21

    move v0, v9

    :goto_14
    iput-boolean v0, v1, Landroid/support/v7/widget/cc;->j:Z

    .line 1996
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->j:Z

    if-eqz v0, :cond_39

    .line 1998
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    if-eqz v0, :cond_22

    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    move-object v10, v0

    .line 2000
    :goto_15
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v12, v1, v0

    move v11, v8

    .line 2001
    :goto_16
    if-ge v11, v12, :cond_27

    .line 2002
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v11}, Landroid/support/v7/widget/r;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_23

    move-object v1, v7

    .line 2003
    :goto_17
    iget v0, v1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_24

    move v0, v9

    :goto_18
    if-nez v0, :cond_19

    .line 2004
    iget-object v5, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 2007
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v0, v0, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v0, :cond_25

    iget-wide v2, v1, Landroid/support/v7/widget/ce;->d:J

    .line 2008
    :goto_19
    if-eqz v10, :cond_26

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 2009
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v10, v0, v1}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2001
    :cond_19
    :goto_1a
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_16

    .line 1968
    :cond_1a
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->l()V

    .line 1970
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0}, Landroid/support/v7/widget/h;->c()V

    .line 1971
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    if-eqz v0, :cond_3b

    .line 1972
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->a()I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v4, v1, v0

    move v3, v8

    .line 1973
    :goto_1b
    if-ge v3, v4, :cond_3b

    .line 1974
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->b(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1c

    move-object v2, v7

    .line 1975
    :goto_1c
    iget v0, v2, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_1d

    move v0, v9

    :goto_1d
    if-eqz v0, :cond_1b

    iget v0, v2, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1e

    move v0, v9

    :goto_1e
    if-nez v0, :cond_1b

    iget v0, v2, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_1f

    move v0, v9

    :goto_1f
    if-nez v0, :cond_1b

    .line 1976
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v0, v0, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v0, :cond_20

    iget-wide v0, v2, Landroid/support/v7/widget/ce;->d:J

    .line 1977
    :goto_20
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v5, v5, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0, v2}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1978
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0, v2}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1973
    :cond_1b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1b

    .line 1974
    :cond_1c
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    move-object v2, v0

    goto :goto_1c

    :cond_1d
    move v0, v8

    .line 1975
    goto :goto_1d

    :cond_1e
    move v0, v8

    goto :goto_1e

    :cond_1f
    move v0, v8

    goto :goto_1f

    .line 1976
    :cond_20
    iget v0, v2, Landroid/support/v7/widget/ce;->b:I

    int-to-long v0, v0

    goto :goto_20

    :cond_21
    move v0, v8

    .line 1994
    goto/16 :goto_14

    :cond_22
    move-object v10, v7

    .line 1998
    goto/16 :goto_15

    .line 2002
    :cond_23
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto/16 :goto_17

    :cond_24
    move v0, v8

    .line 2003
    goto/16 :goto_18

    .line 2007
    :cond_25
    iget v0, v1, Landroid/support/v7/widget/ce;->b:I

    int-to-long v2, v0

    goto/16 :goto_19

    .line 2011
    :cond_26
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v13, v0, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    new-instance v0, Landroid/support/v7/widget/br;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/br;-><init>(Landroid/support/v7/widget/ce;IIII)V

    invoke-virtual {v13, v1, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1a

    .line 2015
    :cond_27
    invoke-direct {p0, v6}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v4/g/a;)V

    .line 2017
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v0

    .line 2018
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_21
    if-ltz v1, :cond_29

    .line 2019
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    iget-object v0, v0, Landroid/support/v4/g/p;->g:[Ljava/lang/Object;

    shl-int/lit8 v2, v1, 0x1

    aget-object v0, v0, v2

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 2020
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v2, v2, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    invoke-virtual {v2, v0}, Landroid/support/v4/g/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 2021
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    iget-object v0, v0, Landroid/support/v4/g/p;->g:[Ljava/lang/Object;

    shl-int/lit8 v2, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Landroid/support/v7/widget/br;

    .line 2022
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v2, v2, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v2, v1}, Landroid/support/v4/g/a;->b(I)Ljava/lang/Object;

    .line 2024
    iget-object v2, v0, Landroid/support/v7/widget/br;->a:Landroid/support/v7/widget/ce;

    iget-object v2, v2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 2025
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v3, v0, Landroid/support/v7/widget/br;->a:Landroid/support/v7/widget/ce;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/bw;->b(Landroid/support/v7/widget/ce;)V

    .line 2026
    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/br;)V

    .line 2018
    :cond_28
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_21

    .line 2030
    :cond_29
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v0

    .line 2031
    if-lez v0, :cond_2f

    .line 2032
    add-int/lit8 v0, v0, -0x1

    move v11, v0

    :goto_22
    if-ltz v11, :cond_2f

    .line 2033
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    iget-object v0, v0, Landroid/support/v4/g/p;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v11, 0x1

    aget-object v1, v0, v1

    check-cast v1, Landroid/support/v7/widget/ce;

    .line 2034
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    iget-object v0, v0, Landroid/support/v4/g/p;->g:[Ljava/lang/Object;

    shl-int/lit8 v2, v11, 0x1

    add-int/lit8 v2, v2, 0x1

    aget-object v0, v0, v2

    check-cast v0, Landroid/support/v7/widget/br;

    .line 2035
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v2, v2, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v2}, Landroid/support/v4/g/a;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2a

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v2, v2, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v2, v1}, Landroid/support/v4/g/a;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2c

    .line 2037
    :cond_2a
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v2, v2, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    invoke-virtual {v2, v11}, Landroid/support/v4/g/a;->b(I)Ljava/lang/Object;

    .line 2038
    if-eqz v6, :cond_2d

    iget-object v2, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v6, v2}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    move-object v3, v2

    .line 2040
    :goto_23
    iget v4, v0, Landroid/support/v7/widget/br;->b:I

    iget v5, v0, Landroid/support/v7/widget/br;->c:I

    iget-object v0, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    if-eqz v3, :cond_2e

    iget v0, v3, Landroid/graphics/Rect;->left:I

    if-ne v0, v4, :cond_2b

    iget v0, v3, Landroid/graphics/Rect;->top:I

    if-eq v0, v5, :cond_2e

    :cond_2b
    invoke-virtual {v1, v8}, Landroid/support/v7/widget/ce;->a(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    iget v2, v3, Landroid/graphics/Rect;->left:I

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/bn;->a(Landroid/support/v7/widget/ce;IIII)Z

    move-result v0

    if-eqz v0, :cond_2c

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    if-nez v0, :cond_2c

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_2c

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    iput-boolean v9, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    .line 2032
    :cond_2c
    :goto_24
    add-int/lit8 v0, v11, -0x1

    move v11, v0

    goto :goto_22

    :cond_2d
    move-object v3, v7

    .line 2038
    goto :goto_23

    .line 2040
    :cond_2e
    invoke-virtual {v1, v8}, Landroid/support/v7/widget/ce;->a(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bn;->b(Landroid/support/v7/widget/ce;)Z

    move-result v0

    if-eqz v0, :cond_2c

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    if-nez v0, :cond_2c

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_2c

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    iput-boolean v9, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    goto :goto_24

    .line 2046
    :cond_2f
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v11

    move v6, v8

    .line 2047
    :goto_25
    if-ge v6, v11, :cond_32

    .line 2048
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    iget-object v0, v0, Landroid/support/v4/g/p;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v6, 0x1

    aget-object v1, v0, v1

    check-cast v1, Landroid/support/v7/widget/ce;

    .line 2049
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->c:Landroid/support/v4/g/a;

    iget-object v0, v0, Landroid/support/v4/g/p;->g:[Ljava/lang/Object;

    shl-int/lit8 v2, v6, 0x1

    add-int/lit8 v2, v2, 0x1

    aget-object v0, v0, v2

    move-object v5, v0

    check-cast v5, Landroid/support/v7/widget/br;

    .line 2050
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/support/v7/widget/br;

    .line 2051
    if-eqz v3, :cond_31

    if-eqz v5, :cond_31

    .line 2052
    iget v0, v3, Landroid/support/v7/widget/br;->b:I

    iget v2, v5, Landroid/support/v7/widget/br;->b:I

    if-ne v0, v2, :cond_30

    iget v0, v3, Landroid/support/v7/widget/br;->c:I

    iget v2, v5, Landroid/support/v7/widget/br;->c:I

    if-eq v0, v2, :cond_31

    .line 2053
    :cond_30
    invoke-virtual {v1, v8}, Landroid/support/v7/widget/ce;->a(Z)V

    .line 2058
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    iget v2, v3, Landroid/support/v7/widget/br;->b:I

    iget v3, v3, Landroid/support/v7/widget/br;->c:I

    iget v4, v5, Landroid/support/v7/widget/br;->b:I

    iget v5, v5, Landroid/support/v7/widget/br;->c:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/bn;->a(Landroid/support/v7/widget/ce;IIII)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 2060
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    if-nez v0, :cond_31

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_31

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    iput-boolean v9, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    .line 2047
    :cond_31
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_25

    .line 2066
    :cond_32
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    if-eqz v0, :cond_35

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v0

    .line 2068
    :goto_26
    add-int/lit8 v0, v0, -0x1

    move v11, v0

    :goto_27
    if-ltz v11, :cond_39

    .line 2069
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    iget-object v0, v0, Landroid/support/v4/g/p;->g:[Ljava/lang/Object;

    shl-int/lit8 v1, v11, 0x1

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2070
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v0, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ce;

    .line 2071
    iget-object v0, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    .line 2072
    iget v0, v1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_36

    move v0, v9

    :goto_28
    if-nez v0, :cond_34

    .line 2073
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_34

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 2079
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/ce;

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/ce;->a(Z)V

    invoke-direct {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/ce;)V

    iput-object v2, v1, Landroid/support/v7/widget/ce;->g:Landroid/support/v7/widget/ce;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bw;->b(Landroid/support/v7/widget/ce;)V

    iget-object v0, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v0, v1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    if-eqz v2, :cond_33

    iget v0, v2, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_37

    move v0, v9

    :goto_29
    if-eqz v0, :cond_38

    :cond_33
    move v6, v4

    move v5, v3

    :goto_2a
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    invoke-virtual/range {v0 .. v6}, Landroid/support/v7/widget/bn;->a(Landroid/support/v7/widget/ce;Landroid/support/v7/widget/ce;IIII)Z

    move-result v0

    if-eqz v0, :cond_34

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    if-nez v0, :cond_34

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_34

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    iput-boolean v9, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    .line 2068
    :cond_34
    add-int/lit8 v0, v11, -0x1

    move v11, v0

    goto/16 :goto_27

    :cond_35
    move v0, v8

    .line 2066
    goto/16 :goto_26

    :cond_36
    move v0, v8

    .line 2072
    goto :goto_28

    :cond_37
    move v0, v8

    .line 2079
    goto :goto_29

    :cond_38
    iget-object v0, v2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    iget-object v0, v2, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v2, v8}, Landroid/support/v7/widget/ce;->a(Z)V

    iput-object v1, v2, Landroid/support/v7/widget/ce;->h:Landroid/support/v7/widget/ce;

    goto :goto_2a

    .line 2085
    :cond_39
    invoke-virtual {p0, v8}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 2086
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->b(Landroid/support/v7/widget/bw;)V

    .line 2087
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget v1, v1, Landroid/support/v7/widget/cc;->e:I

    iput v1, v0, Landroid/support/v7/widget/cc;->f:I

    .line 2088
    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    .line 2089
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v8, v0, Landroid/support/v7/widget/cc;->j:Z

    .line 2090
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v8, v0, Landroid/support/v7/widget/cc;->k:Z

    .line 2091
    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->s:Z

    .line 2092
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iput-boolean v8, v0, Landroid/support/v7/widget/bs;->p:Z

    .line 2093
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_3a

    .line 2094
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2096
    :cond_3a
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-object v7, v0, Landroid/support/v7/widget/cc;->d:Landroid/support/v4/g/a;

    goto/16 :goto_0

    :cond_3b
    move-object v6, v7

    goto/16 :goto_13

    :cond_3c
    move v0, v8

    goto/16 :goto_12
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2338
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-nez v0, :cond_0

    .line 2339
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2341
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->a()Landroid/support/v7/widget/RecyclerView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2346
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-nez v0, :cond_0

    .line 2347
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2349
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/bs;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2354
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-nez v0, :cond_0

    .line 2355
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2357
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bs;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/RecyclerView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2252
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    move v2, v1

    .line 2253
    :goto_0
    if-ge v2, v3, :cond_0

    .line 2254
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v2}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2255
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iput-boolean v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->e:Z

    .line 2253
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2257
    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v2, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, v2, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v0, :cond_1

    iput-boolean v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->e:Z

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2258
    :cond_2
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1409
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 1410
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    .line 1411
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    .line 1412
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_0

    .line 1413
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    .line 1415
    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->E:Z

    .line 1416
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1420
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 1421
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v0, :cond_0

    .line 1422
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    invoke-virtual {v0}, Landroid/support/v7/widget/bn;->c()V

    .line 1424
    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    .line 1426
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->i()V

    .line 1427
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    .line 1428
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_1

    .line 1429
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/bw;)V

    .line 1431
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1432
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2323
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 2325
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 2326
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2327
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    .line 2326
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2329
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v1, -0x1

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1547
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-eq v5, v9, :cond_0

    if-nez v5, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->M:Landroid/support/v7/widget/bt;

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bt;

    invoke-interface {v0}, Landroid/support/v7/widget/bt;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    if-eq v5, v9, :cond_4

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->M:Landroid/support/v7/widget/bt;

    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    .line 1548
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_2
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->j()V

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 1622
    :cond_3
    :goto_2
    return v2

    .line 1547
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    .line 1552
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->d()Z

    move-result v0

    .line 1553
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v4}, Landroid/support/v7/widget/bs;->e()Z

    move-result v4

    .line 1555
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-nez v5, :cond_7

    .line 1556
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v5

    iput-object v5, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    .line 1558
    :cond_7
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1560
    invoke-static {p1}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;)I

    move-result v5

    .line 1561
    invoke-static {p1}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;)I

    move-result v6

    .line 1563
    packed-switch v5, :pswitch_data_0

    .line 1622
    :cond_8
    :goto_3
    :pswitch_0
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->y:I

    if-eq v0, v2, :cond_3

    move v2, v3

    goto :goto_2

    .line 1565
    :pswitch_1
    invoke-static {p1, v3}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 1566
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 1567
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    .line 1569
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->y:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 1570
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1571
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    goto :goto_3

    .line 1576
    :pswitch_2
    invoke-static {p1, v6}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 1577
    invoke-static {p1, v6}, Landroid/support/v4/view/ac;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 1578
    invoke-static {p1, v6}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    goto :goto_3

    .line 1582
    :pswitch_3
    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-static {p1, v5}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 1583
    if-gez v5, :cond_9

    .line 1584
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error processing scroll; pointer index for id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v3

    .line 1586
    goto/16 :goto_2

    .line 1589
    :cond_9
    invoke-static {p1, v5}, Landroid/support/v4/view/ac;->c(Landroid/view/MotionEvent;I)F

    move-result v6

    add-float/2addr v6, v8

    float-to-int v6, v6

    .line 1590
    invoke-static {p1, v5}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v5

    add-float/2addr v5, v8

    float-to-int v5, v5

    .line 1591
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->y:I

    if-eq v7, v2, :cond_8

    .line 1592
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    sub-int/2addr v6, v7

    .line 1593
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    sub-int/2addr v5, v7

    .line 1595
    if-eqz v0, :cond_e

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-le v0, v7, :cond_e

    .line 1596
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-gez v6, :cond_b

    move v0, v1

    :goto_4
    mul-int/2addr v0, v8

    add-int/2addr v0, v7

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    move v0, v2

    .line 1599
    :goto_5
    if-eqz v4, :cond_a

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-le v4, v6, :cond_a

    .line 1600
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-gez v5, :cond_c

    :goto_6
    mul-int/2addr v1, v4

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    move v0, v2

    .line 1603
    :cond_a
    if-eqz v0, :cond_8

    .line 1604
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1605
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    goto/16 :goto_3

    :cond_b
    move v0, v2

    .line 1596
    goto :goto_4

    :cond_c
    move v1, v2

    .line 1600
    goto :goto_6

    .line 1611
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_3

    .line 1615
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_3

    .line 1619
    :pswitch_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_d
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->j()V

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    goto/16 :goto_3

    :cond_e
    move v0, v3

    goto :goto_5

    .line 1563
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2236
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v0, :cond_0

    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    .line 2237
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->g()V

    .line 2238
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 2239
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    .line 2240
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1739
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Z

    if-eqz v0, :cond_1

    .line 1740
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    .line 1741
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->k()V

    .line 1743
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->k:Z

    if-eqz v0, :cond_2

    .line 1748
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v1, v0, Landroid/support/v7/widget/cc;->i:Z

    .line 1754
    :goto_0
    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->o:Z

    .line 1755
    invoke-virtual {p0, v5}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 1758
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_3

    .line 1759
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->a()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/cc;->e:I

    .line 1764
    :goto_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sparse-switch v3, :sswitch_data_0

    iget-object v1, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v4/view/at;->q(Landroid/view/View;)I

    move-result v1

    :sswitch_0
    sparse-switch v4, :sswitch_data_1

    iget-object v0, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/at;->r(Landroid/view/View;)I

    move-result v0

    :sswitch_1
    iget-object v2, v2, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1, v0}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    .line 1765
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v5, v0, Landroid/support/v7/widget/cc;->i:Z

    .line 1766
    return-void

    .line 1751
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0}, Landroid/support/v7/widget/h;->c()V

    .line 1752
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v5, v0, Landroid/support/v7/widget/cc;->i:Z

    goto :goto_0

    .line 1761
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput v5, v0, Landroid/support/v7/widget/cc;->e:I

    goto :goto_1

    .line 1764
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 691
    check-cast p1, Landroid/support/v7/widget/by;

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->I:Landroid/support/v7/widget/by;

    .line 692
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:Landroid/support/v7/widget/by;

    invoke-virtual {v0}, Landroid/support/v7/widget/by;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 693
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->I:Landroid/support/v7/widget/by;

    iget-object v0, v0, Landroid/support/v7/widget/by;->a:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->I:Landroid/support/v7/widget/by;

    iget-object v1, v1, Landroid/support/v7/widget/by;->a:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->a(Landroid/os/Parcelable;)V

    .line 696
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 677
    new-instance v0, Landroid/support/v7/widget/by;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/by;-><init>(Landroid/os/Parcelable;)V

    .line 678
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->I:Landroid/support/v7/widget/by;

    if-eqz v1, :cond_0

    .line 679
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->I:Landroid/support/v7/widget/by;

    iget-object v1, v1, Landroid/support/v7/widget/by;->a:Landroid/os/Parcelable;

    iput-object v1, v0, Landroid/support/v7/widget/by;->a:Landroid/os/Parcelable;

    .line 686
    :goto_0
    return-object v0

    .line 680
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v1, :cond_1

    .line 681
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v1}, Landroid/support/v7/widget/bs;->c()Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/widget/by;->a:Landroid/os/Parcelable;

    goto :goto_0

    .line 683
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/by;->a:Landroid/os/Parcelable;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 1770
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1771
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 1772
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    .line 1774
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/high16 v6, -0x80000000

    const/4 v3, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 1627
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->M:Landroid/support/v7/widget/bt;

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->M:Landroid/support/v7/widget/bt;

    :cond_0
    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->L:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bt;

    invoke-interface {v0}, Landroid/support/v7/widget/bt;->a()Z

    move-result v5

    if-eqz v5, :cond_5

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->M:Landroid/support/v7/widget/bt;

    move v0, v10

    :goto_1
    if-eqz v0, :cond_7

    .line 1628
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->j()V

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 1715
    :goto_2
    return v10

    .line 1627
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->M:Landroid/support/v7/widget/bt;

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    if-ne v0, v10, :cond_4

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->M:Landroid/support/v7/widget/bt;

    :cond_4
    move v0, v10

    goto :goto_1

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    .line 1632
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->d()Z

    move-result v1

    .line 1633
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v0}, Landroid/support/v7/widget/bs;->e()Z

    move-result v5

    .line 1635
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-nez v0, :cond_8

    .line 1636
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    .line 1638
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1640
    invoke-static {p1}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1641
    invoke-static {p1}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;)I

    move-result v4

    .line 1643
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_2

    .line 1645
    :pswitch_1
    invoke-static {p1, v2}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 1646
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 1647
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    goto :goto_2

    .line 1651
    :pswitch_2
    invoke-static {p1, v4}, Landroid/support/v4/view/ac;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 1652
    invoke-static {p1, v4}, Landroid/support/v4/view/ac;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 1653
    invoke-static {p1, v4}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    goto :goto_2

    .line 1657
    :pswitch_3
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1658
    if-gez v0, :cond_9

    .line 1659
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error processing scroll; pointer index for id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v10, v2

    .line 1661
    goto/16 :goto_2

    .line 1664
    :cond_9
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    add-float/2addr v3, v7

    float-to-int v3, v3

    .line 1665
    invoke-static {p1, v0}, Landroid/support/v4/view/ac;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v4, v0

    .line 1666
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->y:I

    if-eq v0, v10, :cond_b

    .line 1667
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    sub-int v0, v3, v0

    .line 1668
    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    sub-int v6, v4, v6

    .line 1670
    if-eqz v1, :cond_1c

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v7

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-le v7, v8, :cond_1c

    .line 1671
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-gez v0, :cond_e

    const/4 v0, -0x1

    :goto_3
    mul-int/2addr v0, v8

    add-int/2addr v0, v7

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    move v0, v10

    .line 1674
    :goto_4
    if-eqz v5, :cond_a

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v7

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-le v7, v8, :cond_a

    .line 1675
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->T:I

    if-gez v6, :cond_f

    const/4 v0, -0x1

    :goto_5
    mul-int/2addr v0, v8

    add-int/2addr v0, v7

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    move v0, v10

    .line 1678
    :cond_a
    if-eqz v0, :cond_b

    .line 1679
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v10}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1680
    invoke-virtual {p0, v10}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 1683
    :cond_b
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->y:I

    if-ne v0, v10, :cond_d

    .line 1684
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    sub-int v0, v3, v0

    .line 1685
    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    sub-int v6, v4, v6

    .line 1686
    if-eqz v1, :cond_10

    neg-int v0, v0

    :goto_6
    if-eqz v5, :cond_c

    neg-int v2, v6

    :cond_c
    invoke-direct {p0, v0, v2}, Landroid/support/v7/widget/RecyclerView;->b(II)V

    .line 1689
    :cond_d
    iput v3, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    .line 1690
    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->S:I

    goto/16 :goto_2

    :cond_e
    move v0, v10

    .line 1671
    goto :goto_3

    :cond_f
    move v0, v10

    .line 1675
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1686
    goto :goto_6

    .line 1694
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_2

    .line 1698
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    const/16 v4, 0x3e8

    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    int-to-float v7, v7

    invoke-virtual {v0, v4, v7}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1699
    if-eqz v1, :cond_17

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-static {v0, v1}, Landroid/support/v4/view/ap;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    neg-float v0, v0

    move v4, v0

    .line 1701
    :goto_7
    if-eqz v5, :cond_18

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    invoke-static {v0, v1}, Landroid/support/v4/view/ap;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    neg-float v0, v0

    move v1, v0

    .line 1703
    :goto_8
    cmpl-float v0, v4, v3

    if-nez v0, :cond_11

    cmpl-float v0, v1, v3

    if-eqz v0, :cond_15

    :cond_11
    float-to-int v0, v4

    float-to-int v1, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->U:I

    if-ge v3, v4, :cond_12

    move v0, v2

    :cond_12
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->U:I

    if-ge v3, v4, :cond_13

    move v1, v2

    :cond_13
    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    neg-int v3, v3

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    neg-int v0, v0

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->V:I

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    if-nez v4, :cond_14

    if-eqz v5, :cond_1a

    :cond_14
    iget-object v11, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    iget-object v0, v11, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    iput v2, v11, Landroid/support/v7/widget/cd;->b:I

    iput v2, v11, Landroid/support/v7/widget/cd;->a:I

    iget-object v1, v11, Landroid/support/v7/widget/cd;->c:Landroid/support/v4/widget/aq;

    const v7, 0x7fffffff

    const v9, 0x7fffffff

    iget-object v0, v1, Landroid/support/v4/widget/aq;->b:Landroid/support/v4/widget/ar;

    iget-object v1, v1, Landroid/support/v4/widget/aq;->a:Ljava/lang/Object;

    move v3, v2

    move v8, v6

    invoke-interface/range {v0 .. v9}, Landroid/support/v4/widget/ar;->a(Ljava/lang/Object;IIIIIIII)V

    iget-boolean v0, v11, Landroid/support/v7/widget/cd;->d:Z

    if-eqz v0, :cond_19

    iput-boolean v10, v11, Landroid/support/v7/widget/cd;->e:Z

    :goto_9
    move v0, v10

    :goto_a
    if-nez v0, :cond_16

    .line 1704
    :cond_15
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 1706
    :cond_16
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 1707
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->j()V

    goto/16 :goto_2

    :cond_17
    move v4, v3

    .line 1699
    goto :goto_7

    :cond_18
    move v1, v3

    .line 1701
    goto :goto_8

    .line 1703
    :cond_19
    iget-object v0, v11, Landroid/support/v7/widget/cd;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v11}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_9

    :cond_1a
    move v0, v2

    goto :goto_a

    .line 1711
    :pswitch_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->O:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_1b
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->j()V

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    goto/16 :goto_2

    :cond_1c
    move v0, v2

    goto/16 :goto_4

    .line 1643
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected removeDetachedView(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2101
    if-nez p1, :cond_2

    const/4 v0, 0x0

    move-object v3, v0

    .line 2102
    :goto_0
    if-eqz v3, :cond_0

    .line 2103
    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 2104
    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, -0x101

    iput v0, v3, Landroid/support/v7/widget/ce;->i:I

    .line 2110
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-nez p1, :cond_6

    .line 2111
    :cond_1
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->removeDetachedView(Landroid/view/View;Z)V

    .line 2112
    return-void

    .line 2101
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    move-object v3, v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2103
    goto :goto_1

    .line 2105
    :cond_4
    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    if-nez v0, :cond_0

    .line 2106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Called removeDetachedView with a view which is not flagged as tmp detached."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v2

    .line 2105
    goto :goto_3

    .line 2110
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_2
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1386
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-object v3, v2, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    if-eqz v3, :cond_2

    iget-object v2, v2, Landroid/support/v7/widget/bs;->o:Landroid/support/v7/widget/ca;

    iget-boolean v2, v2, Landroid/support/v7/widget/ca;->k:Z

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-nez v2, :cond_0

    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->s:Z

    if-eqz v2, :cond_3

    :cond_0
    move v2, v0

    :goto_1
    if-nez v2, :cond_1

    if-eqz p2, :cond_1

    .line 1387
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1388
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v2}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1389
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v2}, Landroid/support/v7/widget/RecyclerView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1390
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/graphics/Rect;

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-nez v3, :cond_4

    :goto_2
    invoke-virtual {p0, p1, v2, v0}, Landroid/support/v7/widget/RecyclerView;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    .line 1392
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 1393
    return-void

    :cond_2
    move v2, v1

    .line 1386
    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 1390
    goto :goto_2
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1397
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v0, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    iget-object v0, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    :goto_0
    iget-object v2, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_5

    iget-object v2, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    :goto_1
    iget-object v3, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_6

    iget-object v3, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    :goto_2
    iget-object v4, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v4, :cond_7

    iget-object v4, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v4

    :goto_3
    sub-int v7, v3, v4

    iget-object v3, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_8

    iget-object v3, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v3

    :goto_4
    iget-object v4, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v4, :cond_9

    iget-object v4, v6, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v4

    :goto_5
    sub-int v4, v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    iget v6, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v6

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v6

    iget v8, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v8

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v8

    add-int/2addr v8, v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v9

    add-int/2addr v9, v6

    sub-int v0, v3, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int v0, v6, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    sub-int v0, v8, v7

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int v4, v9, v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v6

    if-ne v6, v5, :cond_b

    if-eqz v0, :cond_a

    :goto_6
    move v3, v0

    :cond_0
    :goto_7
    if-eqz v2, :cond_c

    move v0, v2

    :goto_8
    if-nez v3, :cond_1

    if-eqz v0, :cond_3

    :cond_1
    if-eqz p3, :cond_d

    invoke-virtual {p0, v3, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    :cond_2
    :goto_9
    move v1, v5

    :cond_3
    return v1

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    move v4, v1

    goto :goto_3

    :cond_8
    move v3, v1

    goto :goto_4

    :cond_9
    move v4, v1

    goto :goto_5

    :cond_a
    move v0, v3

    goto :goto_6

    :cond_b
    if-nez v3, :cond_0

    move v3, v0

    goto :goto_7

    :cond_c
    move v0, v4

    goto :goto_8

    :cond_d
    if-nez v3, :cond_e

    if-eqz v0, :cond_2

    :cond_e
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/support/v7/widget/cd;

    invoke-virtual {v1, v3, v0}, Landroid/support/v7/widget/cd;->a(II)V

    goto :goto_9
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 2244
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Z

    if-nez v0, :cond_0

    .line 2245
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 2249
    :goto_0
    return-void

    .line 2247
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->n:Z

    goto :goto_0
.end method

.method public scrollBy(II)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 947
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-nez v1, :cond_0

    .line 948
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 951
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v1}, Landroid/support/v7/widget/bs;->d()Z

    move-result v1

    .line 952
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    invoke-virtual {v2}, Landroid/support/v7/widget/bs;->e()Z

    move-result v2

    .line 953
    if-nez v1, :cond_1

    if-eqz v2, :cond_2

    .line 954
    :cond_1
    if-eqz v1, :cond_3

    :goto_0
    if-eqz v2, :cond_4

    :goto_1
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->b(II)V

    .line 956
    :cond_2
    return-void

    :cond_3
    move p1, v0

    .line 954
    goto :goto_0

    :cond_4
    move p2, v0

    goto :goto_1
.end method

.method public scrollTo(II)V
    .locals 2

    .prologue
    .line 941
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "RecyclerView does not support scrolling to an absolute position."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setAdapter(Landroid/support/v7/widget/bk;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 563
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/bx;

    iget-object v0, v0, Landroid/support/v7/widget/bk;->a:Landroid/support/v7/widget/bl;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/bl;->unregisterObserver(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    invoke-virtual {v0}, Landroid/support/v7/widget/bn;->c()V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_9

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v0, v5, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_3

    iget-object v0, v5, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    iget-object v3, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v3}, Landroid/support/v7/widget/t;->a()I

    move-result v3

    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, v3, v0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_8

    iget-object v0, v5, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_4

    iget-object v0, v5, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v7

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v7}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_5

    move-object v0, v2

    :goto_3
    iget v0, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    move v0, v4

    :goto_4
    if-nez v0, :cond_2

    iget-object v0, v5, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    if-eqz v0, :cond_7

    iget-object v0, v5, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/r;->a(I)I

    move-result v7

    iget-object v0, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v0, v7}, Landroid/support/v7/widget/t;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_5
    invoke-virtual {v5, v3}, Landroid/support/v7/widget/bs;->d(I)V

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/bw;->a(Landroid/view/View;)V

    :cond_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v2

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_4

    :cond_7
    move-object v0, v2

    goto :goto_5

    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bs;->b(Landroid/support/v7/widget/bw;)V

    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    iget-object v1, v0, Landroid/support/v7/widget/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/h;->a(Ljava/util/List;)V

    iget-object v1, v0, Landroid/support/v7/widget/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/h;->a(Ljava/util/List;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz p1, :cond_a

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/bx;

    iget-object v2, p1, Landroid/support/v7/widget/bk;->a:Landroid/support/v7/widget/bl;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/bl;->registerObserver(Ljava/lang/Object;)V

    :cond_a
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v1, :cond_b

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    :cond_b
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bw;->a()V

    iget-object v3, v1, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    if-nez v3, :cond_c

    new-instance v3, Landroid/support/v7/widget/bv;

    invoke-direct {v3}, Landroid/support/v7/widget/bv;-><init>()V

    iput-object v3, v1, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    :cond_c
    iget-object v1, v1, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_d

    iget v0, v1, Landroid/support/v7/widget/bv;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v1, Landroid/support/v7/widget/bv;->a:I

    :cond_d
    iget v0, v1, Landroid/support/v7/widget/bv;->a:I

    if-nez v0, :cond_e

    invoke-virtual {v1}, Landroid/support/v7/widget/bv;->a()V

    :cond_e
    if-eqz v2, :cond_f

    iget v0, v1, Landroid/support/v7/widget/bv;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Landroid/support/v7/widget/bv;->a:I

    :cond_f
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iput-boolean v4, v0, Landroid/support/v7/widget/cc;->h:Z

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->m()V

    .line 564
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 565
    return-void
.end method

.method public setClipToPadding(Z)V
    .locals 1

    .prologue
    .line 498
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eq p1, v0, :cond_0

    .line 499
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v4/widget/x;

    .line 501
    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    .line 502
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 503
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 504
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 506
    :cond_1
    return-void
.end method

.method public final setHasFixedSize(Z)V
    .locals 0

    .prologue
    .line 485
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->k:Z

    .line 486
    return-void
.end method

.method public final setLayoutManager(Landroid/support/v7/widget/bs;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 648
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-ne p1, v0, :cond_0

    .line 673
    :goto_0
    return-void

    .line 653
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v0, :cond_2

    .line 654
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_1

    .line 655
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/bw;)V

    .line 657
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-nez v4, :cond_4

    iput-object v4, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iput-object v4, v0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    .line 659
    :cond_2
    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    invoke-virtual {v0}, Landroid/support/v7/widget/bw;->a()V

    .line 660
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iget-object v1, v0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/widget/t;

    invoke-interface {v1}, Landroid/support/v7/widget/t;->b()V

    iget-object v1, v0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/s;

    const-wide/16 v2, 0x0

    iput-wide v2, v1, Landroid/support/v7/widget/s;->a:J

    iget-object v2, v1, Landroid/support/v7/widget/s;->b:Landroid/support/v7/widget/s;

    if-eqz v2, :cond_3

    iget-object v1, v1, Landroid/support/v7/widget/s;->b:Landroid/support/v7/widget/s;

    invoke-virtual {v1}, Landroid/support/v7/widget/s;->a()V

    :cond_3
    iget-object v0, v0, Landroid/support/v7/widget/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 661
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    .line 662
    if-eqz p1, :cond_6

    .line 663
    iget-object v0, p1, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_5

    .line 664
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LayoutManager "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already attached to a RecyclerView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 657
    :cond_4
    iput-object v4, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v4, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iput-object v1, v0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    goto :goto_1

    .line 667
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-nez p0, :cond_7

    iput-object v4, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iput-object v4, v0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    .line 668
    :goto_2
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Z

    if-eqz v0, :cond_6

    .line 669
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    .line 672
    :cond_6
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_0

    .line 667
    :cond_7
    iput-object p0, v0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    iput-object v1, v0, Landroid/support/v7/widget/bs;->m:Landroid/support/v7/widget/r;

    goto :goto_2
.end method

.method public final setOnScrollListener(Landroid/support/v7/widget/bu;)V
    .locals 0

    .prologue
    .line 903
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/support/v7/widget/bu;

    .line 904
    return-void
.end method

.method public final setRecycledViewPool(Landroid/support/v7/widget/bv;)V
    .locals 3

    .prologue
    .line 772
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v1, v0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    iget v2, v1, Landroid/support/v7/widget/bv;->a:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Landroid/support/v7/widget/bv;->a:I

    :cond_0
    iput-object p1, v0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    if-eqz p1, :cond_1

    iget-object v1, v0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    iget-object v0, v0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget v0, v1, Landroid/support/v7/widget/bv;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Landroid/support/v7/widget/bv;->a:I

    .line 773
    :cond_1
    return-void
.end method
