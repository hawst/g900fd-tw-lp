.class Landroid/support/v7/widget/cy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/ShareActionProvider;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/ShareActionProvider;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Landroid/support/v7/widget/cy;->a:Landroid/support/v7/widget/ShareActionProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Landroid/support/v7/widget/cy;->a:Landroid/support/v7/widget/ShareActionProvider;

    iget-object v0, v0, Landroid/support/v7/widget/ShareActionProvider;->c:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v7/widget/cy;->a:Landroid/support/v7/widget/ShareActionProvider;

    iget-object v1, v1, Landroid/support/v7/widget/ShareActionProvider;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/k;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/k;

    move-result-object v0

    .line 315
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 316
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/k;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_0

    .line 318
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 319
    iget-object v1, p0, Landroid/support/v7/widget/cy;->a:Landroid/support/v7/widget/ShareActionProvider;

    iget-object v1, v1, Landroid/support/v7/widget/ShareActionProvider;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 321
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
