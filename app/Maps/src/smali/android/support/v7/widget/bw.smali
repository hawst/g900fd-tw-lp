.class public final Landroid/support/v7/widget/bw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/support/v7/widget/bv;

.field final synthetic f:Landroid/support/v7/widget/RecyclerView;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 3162
    iput-object p1, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    .line 3164
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    .line 3166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    .line 3168
    iget-object v0, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/bw;->d:Ljava/util/List;

    .line 3171
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v7/widget/bw;->g:I

    return-void
.end method

.method private a(IIZ)Landroid/support/v7/widget/ce;
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3739
    iget-object v0, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    .line 3742
    :goto_0
    if-ge v4, v5, :cond_1

    .line 3743
    iget-object v0, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 3744
    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    if-nez v1, :cond_9

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v1, v6, :cond_5

    iget v1, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_2
    if-ne v1, p1, :cond_9

    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    move v1, v2

    :goto_3
    if-nez v1, :cond_9

    iget-object v1, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v1, v1, Landroid/support/v7/widget/cc;->i:Z

    if-nez v1, :cond_0

    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    move v1, v2

    :goto_4
    if-nez v1, :cond_9

    .line 3746
    :cond_0
    if-eq p2, v6, :cond_8

    iget v1, v0, Landroid/support/v7/widget/ce;->e:I

    if-eq v1, p2, :cond_8

    .line 3747
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Scrap view for position "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " isn\'t dirty but has wrong view type! (found "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Landroid/support/v7/widget/ce;->e:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " but expected "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3757
    :cond_1
    if-nez p3, :cond_2

    .line 3758
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->d:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/r;->a(II)Landroid/view/View;

    move-result-object v0

    .line 3759
    if-eqz v0, :cond_2

    .line 3761
    iget-object v1, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    iget-object v4, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bn;->c(Landroid/support/v7/widget/ce;)V

    .line 3766
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    .line 3767
    :goto_5
    if-ge v4, v5, :cond_d

    .line 3768
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 3771
    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_a

    move v1, v2

    :goto_6
    if-nez v1, :cond_c

    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    if-ne v1, v6, :cond_b

    iget v1, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_7
    if-ne v1, p1, :cond_c

    .line 3772
    if-nez p3, :cond_3

    .line 3773
    iget-object v1, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 3782
    :cond_3
    :goto_8
    return-object v0

    :cond_4
    move v1, v3

    .line 3744
    goto/16 :goto_1

    :cond_5
    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    goto/16 :goto_2

    :cond_6
    move v1, v3

    goto/16 :goto_3

    :cond_7
    move v1, v3

    goto :goto_4

    .line 3752
    :cond_8
    const/16 v1, 0x20

    iget v2, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v1, v2

    iput v1, v0, Landroid/support/v7/widget/ce;->i:I

    goto :goto_8

    .line 3742
    :cond_9
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    :cond_a
    move v1, v3

    .line 3771
    goto :goto_6

    :cond_b
    iget v1, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_7

    .line 3767
    :cond_c
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    .line 3782
    :cond_d
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private a(JIZ)Landroid/support/v7/widget/ce;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3787
    iget-object v0, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3788
    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_5

    .line 3789
    iget-object v0, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 3790
    iget-wide v6, v0, Landroid/support/v7/widget/ce;->d:J

    cmp-long v3, v6, p1

    if-nez v3, :cond_4

    iget v3, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v3, v3, 0x20

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    if-nez v3, :cond_4

    .line 3791
    iget v3, v0, Landroid/support/v7/widget/ce;->e:I

    if-ne p3, v3, :cond_3

    .line 3792
    const/16 v3, 0x20

    iget v4, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v3, v4

    iput v3, v0, Landroid/support/v7/widget/ce;->i:I

    .line 3793
    iget v3, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_2

    :goto_2
    if-eqz v1, :cond_0

    .line 3802
    iget-object v1, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v1, v1, Landroid/support/v7/widget/cc;->i:Z

    if-nez v1, :cond_0

    .line 3803
    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, -0xf

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/support/v7/widget/ce;->i:I

    .line 3832
    :cond_0
    :goto_3
    return-object v0

    :cond_1
    move v3, v2

    .line 3790
    goto :goto_1

    :cond_2
    move v1, v2

    .line 3793
    goto :goto_2

    .line 3808
    :cond_3
    if-nez p4, :cond_4

    .line 3810
    iget-object v3, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 3811
    iget-object v3, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v3, v5, v2}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 3812
    iget-object v0, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/bw;->b(Landroid/view/View;)V

    .line 3788
    :cond_4
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    .line 3818
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3819
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_8

    .line 3820
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 3821
    iget-wide v2, v0, Landroid/support/v7/widget/ce;->d:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_7

    .line 3822
    iget v2, v0, Landroid/support/v7/widget/ce;->e:I

    if-ne p3, v2, :cond_6

    .line 3823
    if-nez p4, :cond_0

    .line 3824
    iget-object v2, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 3827
    :cond_6
    if-nez p4, :cond_7

    .line 3828
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/bw;->c(I)Z

    .line 3819
    :cond_7
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    .line 3832
    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private a(Landroid/view/ViewGroup;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 3493
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 3494
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3495
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 3496
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Landroid/support/v7/widget/bw;->a(Landroid/view/ViewGroup;Z)V

    .line 3493
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 3499
    :cond_1
    if-nez p2, :cond_2

    .line 3511
    :goto_1
    return-void

    .line 3503
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 3504
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3505
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 3507
    :cond_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    .line 3508
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3509
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method private c(Landroid/support/v7/widget/ce;)V
    .locals 1

    .prologue
    .line 3836
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    .line 3837
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_0

    .line 3840
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    .line 3842
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    .line 3843
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/cc;->a(Landroid/support/v7/widget/ce;)V

    .line 3846
    return-void
.end method

.method private d(I)Landroid/support/v7/widget/ce;
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/16 v12, 0x20

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 3701
    iget-object v0, p0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    move-object v0, v1

    .line 3726
    :goto_0
    return-object v0

    :cond_1
    move v4, v3

    .line 3705
    :goto_1
    if-ge v4, v6, :cond_5

    .line 3706
    iget-object v0, p0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 3707
    iget v2, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_2

    move v2, v5

    :goto_2
    if-nez v2, :cond_4

    iget v2, v0, Landroid/support/v7/widget/ce;->f:I

    const/4 v7, -0x1

    if-ne v2, v7, :cond_3

    iget v2, v0, Landroid/support/v7/widget/ce;->b:I

    :goto_3
    if-ne v2, p1, :cond_4

    .line 3708
    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v1, v12

    iput v1, v0, Landroid/support/v7/widget/ce;->i:I

    goto :goto_0

    :cond_2
    move v2, v3

    .line 3707
    goto :goto_2

    :cond_3
    iget v2, v0, Landroid/support/v7/widget/ce;->f:I

    goto :goto_3

    .line 3705
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 3713
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v0, v0, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v0, :cond_8

    .line 3714
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0, p1, v3}, Landroid/support/v7/widget/h;->a(II)I

    move-result v0

    .line 3715
    if-lez v0, :cond_8

    iget-object v2, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->a()I

    move-result v2

    if-ge v0, v2, :cond_8

    .line 3716
    iget-object v2, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/bk;->b(I)J

    move-result-wide v8

    move v2, v3

    .line 3717
    :goto_4
    if-ge v2, v6, :cond_8

    .line 3718
    iget-object v0, p0, Landroid/support/v7/widget/bw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 3719
    iget v4, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_6

    move v4, v5

    :goto_5
    if-nez v4, :cond_7

    iget-wide v10, v0, Landroid/support/v7/widget/ce;->d:J

    cmp-long v4, v10, v8

    if-nez v4, :cond_7

    .line 3720
    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v1, v12

    iput v1, v0, Landroid/support/v7/widget/ce;->i:I

    goto :goto_0

    :cond_6
    move v4, v3

    .line 3719
    goto :goto_5

    .line 3717
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_8
    move-object v0, v1

    .line 3726
    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 4

    .prologue
    .line 3315
    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v1, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v1, :cond_1

    iget v1, v0, Landroid/support/v7/widget/cc;->f:I

    iget v0, v0, Landroid/support/v7/widget/cc;->g:I

    sub-int v0, v1, v0

    :goto_0
    if-lt p1, v0, :cond_3

    .line 3316
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "invalid position "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". State item count is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v3, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v3, :cond_2

    iget v3, v0, Landroid/support/v7/widget/cc;->f:I

    iget v0, v0, Landroid/support/v7/widget/cc;->g:I

    sub-int v0, v3, v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3315
    :cond_1
    iget v0, v0, Landroid/support/v7/widget/cc;->e:I

    goto :goto_0

    .line 3316
    :cond_2
    iget v0, v0, Landroid/support/v7/widget/cc;->e:I

    goto :goto_1

    .line 3319
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_4

    .line 3322
    :goto_2
    return p1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/h;->a(I)I

    move-result p1

    goto :goto_2
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 3184
    iget-object v0, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3185
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/bw;->c(I)Z

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3186
    return-void
.end method

.method final a(Landroid/support/v7/widget/ce;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3585
    iget-object v0, p1, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 3586
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Scrapped or attached views may not be recycled. isScrap:"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p1, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " isAttached:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    move v0, v2

    .line 3585
    goto :goto_0

    :cond_2
    move v0, v2

    .line 3586
    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    .line 3592
    :cond_4
    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    .line 3593
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tmp detached view should be removed from RecyclerView before it can be recycled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v2

    .line 3592
    goto :goto_3

    .line 3597
    :cond_6
    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    move v0, v1

    :goto_4
    if-eqz v0, :cond_8

    .line 3598
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v0, v2

    .line 3597
    goto :goto_4

    .line 3601
    :cond_8
    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_a

    iget-object v0, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/at;->c(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_11

    .line 3603
    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_b

    move v0, v1

    :goto_6
    if-nez v0, :cond_f

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->i:Z

    if-nez v0, :cond_9

    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_c

    move v0, v1

    :goto_7
    if-nez v0, :cond_f

    :cond_9
    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_d

    move v0, v1

    :goto_8
    if-nez v0, :cond_f

    .line 3606
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Landroid/support/v7/widget/bw;->g:I

    if-ne v0, v3, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v2

    .line 3607
    :goto_9
    iget-object v3, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_e

    .line 3608
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/bw;->c(I)Z

    move-result v3

    if-nez v3, :cond_e

    .line 3609
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_a
    move v0, v2

    .line 3601
    goto :goto_5

    :cond_b
    move v0, v2

    .line 3603
    goto :goto_6

    :cond_c
    move v0, v2

    goto :goto_7

    :cond_d
    move v0, v2

    goto :goto_8

    .line 3613
    :cond_e
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Landroid/support/v7/widget/bw;->g:I

    if-ge v0, v3, :cond_f

    .line 3614
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 3618
    :cond_f
    if-nez v2, :cond_11

    .line 3619
    iget-object v0, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    if-nez v0, :cond_10

    new-instance v0, Landroid/support/v7/widget/bv;

    invoke-direct {v0}, Landroid/support/v7/widget/bv;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    :cond_10
    iget-object v0, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bv;->a(Landroid/support/v7/widget/ce;)V

    .line 3620
    invoke-direct {p0, p1}, Landroid/support/v7/widget/bw;->c(Landroid/support/v7/widget/ce;)V

    .line 3628
    :cond_11
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/cc;->a(Landroid/support/v7/widget/ce;)V

    .line 3629
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3526
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v3

    .line 3527
    iget v2, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 3528
    iget-object v2, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, p1, v1}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 3530
    :cond_0
    iget-object v2, v3, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 3531
    iget-object v0, v3, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/bw;->b(Landroid/support/v7/widget/ce;)V

    .line 3535
    :cond_1
    :goto_2
    invoke-virtual {p0, v3}, Landroid/support/v7/widget/bw;->a(Landroid/support/v7/widget/ce;)V

    .line 3536
    return-void

    :cond_2
    move v2, v1

    .line 3527
    goto :goto_0

    :cond_3
    move v2, v1

    .line 3530
    goto :goto_1

    .line 3532
    :cond_4
    iget v2, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_5

    :goto_3
    if-eqz v0, :cond_1

    .line 3533
    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, -0x21

    iput v0, v3, Landroid/support/v7/widget/ce;->i:I

    goto :goto_2

    :cond_5
    move v0, v1

    .line 3532
    goto :goto_3
.end method

.method public final b(I)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3340
    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v4, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v4, :cond_1

    iget v4, v0, Landroid/support/v7/widget/cc;->f:I

    iget v0, v0, Landroid/support/v7/widget/cc;->g:I

    sub-int v0, v4, v0

    :goto_0
    if-lt p1, v0, :cond_3

    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid item position "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "). Item count:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v3, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v3, :cond_2

    iget v3, v0, Landroid/support/v7/widget/cc;->f:I

    iget v0, v0, Landroid/support/v7/widget/cc;->g:I

    sub-int v0, v3, v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget v0, v0, Landroid/support/v7/widget/cc;->e:I

    goto :goto_0

    :cond_2
    iget v0, v0, Landroid/support/v7/widget/cc;->e:I

    goto :goto_1

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_27

    invoke-direct {p0, p1}, Landroid/support/v7/widget/bw;->d(I)Landroid/support/v7/widget/ce;

    move-result-object v4

    if-eqz v4, :cond_6

    move v0, v1

    :goto_2
    move-object v10, v4

    move v4, v0

    move-object v0, v10

    :goto_3
    if-nez v0, :cond_26

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, v2}, Landroid/support/v7/widget/bw;->a(IIZ)Landroid/support/v7/widget/ce;

    move-result-object v0

    if-eqz v0, :cond_26

    iget v5, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_7

    move v5, v1

    :goto_4
    if-eqz v5, :cond_8

    move v5, v1

    :goto_5
    if-nez v5, :cond_11

    const/4 v5, 0x4

    iget v6, v0, Landroid/support/v7/widget/ce;->i:I

    or-int/2addr v5, v6

    iput v5, v0, Landroid/support/v7/widget/ce;->i:I

    iget-object v5, v0, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    if-eqz v5, :cond_e

    move v5, v1

    :goto_6
    if-eqz v5, :cond_f

    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v5, v6, v2}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    iget-object v5, v0, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/bw;->b(Landroid/support/v7/widget/ce;)V

    :cond_4
    :goto_7
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/bw;->a(Landroid/support/v7/widget/ce;)V

    move-object v0, v3

    move v3, v4

    :goto_8
    if-nez v0, :cond_25

    iget-object v4, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v4, p1}, Landroid/support/v7/widget/h;->a(I)I

    move-result v5

    if-ltz v5, :cond_5

    iget-object v4, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v4}, Landroid/support/v7/widget/bk;->a()I

    move-result v4

    if-lt v5, v4, :cond_13

    :cond_5
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Inconsistency detected. Invalid item position "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "(offset:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ").state:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v3, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v3, :cond_12

    iget v3, v0, Landroid/support/v7/widget/cc;->f:I

    iget v0, v0, Landroid/support/v7/widget/cc;->g:I

    sub-int v0, v3, v0

    :goto_9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    move v0, v2

    goto/16 :goto_2

    :cond_7
    move v5, v2

    goto :goto_4

    :cond_8
    iget v5, v0, Landroid/support/v7/widget/ce;->b:I

    if-ltz v5, :cond_9

    iget v5, v0, Landroid/support/v7/widget/ce;->b:I

    iget-object v6, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v6}, Landroid/support/v7/widget/bk;->a()I

    move-result v6

    if-lt v5, v6, :cond_a

    :cond_9
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Inconsistency detected. Invalid view holder adapter position"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v5, v5, Landroid/support/v7/widget/cc;->i:Z

    if-nez v5, :cond_b

    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget v6, v0, Landroid/support/v7/widget/ce;->b:I

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/bk;->a(I)I

    move-result v5

    iget v6, v0, Landroid/support/v7/widget/ce;->e:I

    if-eq v5, v6, :cond_b

    move v5, v2

    goto/16 :goto_5

    :cond_b
    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v5, v5, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v5, :cond_d

    iget-wide v6, v0, Landroid/support/v7/widget/ce;->d:J

    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget v8, v0, Landroid/support/v7/widget/ce;->b:I

    invoke-virtual {v5, v8}, Landroid/support/v7/widget/bk;->b(I)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_c

    move v5, v1

    goto/16 :goto_5

    :cond_c
    move v5, v2

    goto/16 :goto_5

    :cond_d
    move v5, v1

    goto/16 :goto_5

    :cond_e
    move v5, v2

    goto/16 :goto_6

    :cond_f
    iget v5, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v5, v5, 0x20

    if-eqz v5, :cond_10

    move v5, v1

    :goto_a
    if-eqz v5, :cond_4

    iget v5, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v5, v5, -0x21

    iput v5, v0, Landroid/support/v7/widget/ce;->i:I

    goto/16 :goto_7

    :cond_10
    move v5, v2

    goto :goto_a

    :cond_11
    move v3, v1

    goto/16 :goto_8

    :cond_12
    iget v0, v0, Landroid/support/v7/widget/cc;->e:I

    goto/16 :goto_9

    :cond_13
    iget-object v4, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/bk;->a(I)I

    move-result v4

    iget-object v6, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-boolean v6, v6, Landroid/support/v7/widget/bk;->b:Z

    if-eqz v6, :cond_14

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bk;->b(I)J

    move-result-wide v6

    invoke-direct {p0, v6, v7, v4, v2}, Landroid/support/v7/widget/bw;->a(JIZ)Landroid/support/v7/widget/ce;

    move-result-object v0

    if-eqz v0, :cond_14

    iput v5, v0, Landroid/support/v7/widget/ce;->b:I

    move v3, v1

    :cond_14
    if-nez v0, :cond_17

    iget-object v0, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    if-nez v0, :cond_15

    new-instance v0, Landroid/support/v7/widget/bv;

    invoke-direct {v0}, Landroid/support/v7/widget/bv;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    :cond_15
    iget-object v0, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    iget-object v4, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/bk;->a(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/bv;->a(I)Landroid/support/v7/widget/ce;

    move-result-object v4

    if-eqz v4, :cond_16

    invoke-virtual {v4}, Landroid/support/v7/widget/ce;->b()V

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->a:Z

    if-eqz v0, :cond_16

    iget-object v0, v4, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_16

    iget-object v0, v4, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v2}, Landroid/support/v7/widget/bw;->a(Landroid/view/ViewGroup;Z)V

    :cond_16
    move-object v0, v4

    :cond_17
    if-nez v0, :cond_25

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    iget-object v4, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v6, v5}, Landroid/support/v7/widget/bk;->a(I)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/support/v7/widget/bk;->a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/ce;

    move-result-object v0

    iput v5, v0, Landroid/support/v7/widget/ce;->e:I

    move v4, v3

    move-object v3, v0

    :goto_b
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_19

    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_18

    move v0, v1

    :goto_c
    if-eqz v0, :cond_19

    iput p1, v3, Landroid/support/v7/widget/ce;->f:I

    move v5, v2

    :goto_d
    iget-object v0, v3, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_21

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v6, v3, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_e
    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->c:Landroid/support/v7/widget/ce;

    if-eqz v4, :cond_23

    if-eqz v5, :cond_23

    :goto_f
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->f:Z

    iget-object v0, v3, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    return-object v0

    :cond_18
    move v0, v2

    goto :goto_c

    :cond_19
    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1e

    move v0, v1

    :goto_10
    if-eqz v0, :cond_1a

    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1f

    move v0, v1

    :goto_11
    if-nez v0, :cond_1a

    iget v0, v3, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_20

    move v0, v1

    :goto_12
    if-eqz v0, :cond_24

    :cond_1a
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/h;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/h;->a(I)I

    move-result v0

    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    invoke-virtual {v5, v3, v0}, Landroid/support/v7/widget/bk;->b(Landroid/support/v7/widget/ce;I)V

    iget-object v0, v3, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->q:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v5, :cond_1c

    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->q:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-static {v0}, Landroid/support/v4/view/at;->e(Landroid/view/View;)I

    move-result v5

    if-nez v5, :cond_1b

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->c(Landroid/view/View;I)V

    :cond_1b
    invoke-static {v0}, Landroid/support/v4/view/at;->b(Landroid/view/View;)Z

    move-result v5

    if-nez v5, :cond_1c

    iget-object v5, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v7/widget/cf;

    iget-object v5, v5, Landroid/support/v7/widget/cf;->c:Landroid/support/v4/view/a;

    invoke-static {v0, v5}, Landroid/support/v4/view/at;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    :cond_1c
    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    iget-boolean v0, v0, Landroid/support/v7/widget/cc;->i:Z

    if-eqz v0, :cond_1d

    iput p1, v3, Landroid/support/v7/widget/ce;->f:I

    :cond_1d
    move v5, v1

    goto/16 :goto_d

    :cond_1e
    move v0, v2

    goto :goto_10

    :cond_1f
    move v0, v2

    goto :goto_11

    :cond_20
    move v0, v2

    goto :goto_12

    :cond_21
    iget-object v6, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v6

    if-nez v6, :cond_22

    iget-object v6, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v6, v3, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_e

    :cond_22
    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    goto/16 :goto_e

    :cond_23
    move v1, v2

    goto/16 :goto_f

    :cond_24
    move v5, v2

    goto/16 :goto_d

    :cond_25
    move v4, v3

    move-object v3, v0

    goto/16 :goto_b

    :cond_26
    move v3, v4

    goto/16 :goto_8

    :cond_27
    move-object v0, v3

    move v4, v2

    goto/16 :goto_3
.end method

.method final b(Landroid/support/v7/widget/ce;)V
    .locals 2

    .prologue
    .line 3677
    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bw;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->x:Landroid/support/v7/widget/bn;

    .line 3678
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bw;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 3682
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    .line 3683
    iget v0, p1, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p1, Landroid/support/v7/widget/ce;->i:I

    .line 3684
    return-void

    .line 3677
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 3637
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/ce;

    move-result-object v0

    .line 3638
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/ce;->j:Landroid/support/v7/widget/bw;

    .line 3639
    iget v1, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v1, v1, -0x21

    iput v1, v0, Landroid/support/v7/widget/ce;->i:I

    .line 3640
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/bw;->a(Landroid/support/v7/widget/ce;)V

    .line 3641
    return-void
.end method

.method final c(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3566
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 3570
    iget v3, v0, Landroid/support/v7/widget/ce;->i:I

    and-int/lit8 v3, v3, 0x10

    if-nez v3, :cond_1

    iget-object v3, v0, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-static {v3}, Landroid/support/v4/view/at;->c(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    :goto_0
    if-eqz v3, :cond_2

    .line 3571
    iget-object v2, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    if-nez v2, :cond_0

    new-instance v2, Landroid/support/v7/widget/bv;

    invoke-direct {v2}, Landroid/support/v7/widget/bv;-><init>()V

    iput-object v2, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/bv;->a(Landroid/support/v7/widget/ce;)V

    .line 3572
    invoke-direct {p0, v0}, Landroid/support/v7/widget/bw;->c(Landroid/support/v7/widget/ce;)V

    .line 3573
    iget-object v0, p0, Landroid/support/v7/widget/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    .line 3576
    :goto_1
    return v0

    :cond_1
    move v3, v2

    .line 3570
    goto :goto_0

    :cond_2
    move v0, v2

    .line 3576
    goto :goto_1
.end method
