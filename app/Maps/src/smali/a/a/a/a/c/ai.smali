.class La/a/a/a/c/ai;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field b:I

.field c:I

.field d:I

.field final synthetic e:La/a/a/a/c/ab;


# direct methods
.method constructor <init>(La/a/a/a/c/ab;)V
    .locals 2

    .prologue
    .line 431
    iput-object p1, p0, La/a/a/a/c/ai;->e:La/a/a/a/c/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 433
    iget-object v0, p0, La/a/a/a/c/ai;->e:La/a/a/a/c/ab;

    iget v0, v0, La/a/a/a/c/ab;->e:I

    iput v0, p0, La/a/a/a/c/ai;->b:I

    .line 436
    const/4 v0, -0x1

    iput v0, p0, La/a/a/a/c/ai;->c:I

    .line 438
    iget-object v0, p0, La/a/a/a/c/ai;->e:La/a/a/a/c/ab;

    iget v0, v0, La/a/a/a/c/ab;->h:I

    iput v0, p0, La/a/a/a/c/ai;->d:I

    .line 440
    iget-object v0, p0, La/a/a/a/c/ai;->e:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->c:[Z

    .line 441
    iget v1, p0, La/a/a/a/c/ai;->d:I

    if-eqz v1, :cond_1

    :cond_0
    iget v1, p0, La/a/a/a/c/ai;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/c/ai;->b:I

    aget-boolean v1, v0, v1

    if-eqz v1, :cond_0

    .line 442
    :cond_1
    return-void
.end method


# virtual methods
.method public final b()I
    .locals 3

    .prologue
    .line 447
    invoke-virtual {p0}, La/a/a/a/c/ai;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 448
    :cond_0
    iget v0, p0, La/a/a/a/c/ai;->b:I

    iput v0, p0, La/a/a/a/c/ai;->c:I

    .line 450
    iget v0, p0, La/a/a/a/c/ai;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/c/ai;->d:I

    if-eqz v0, :cond_2

    .line 451
    iget-object v0, p0, La/a/a/a/c/ai;->e:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->c:[Z

    .line 452
    :cond_1
    iget v1, p0, La/a/a/a/c/ai;->b:I

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, La/a/a/a/c/ai;->e:La/a/a/a/c/ab;

    iget v2, v2, La/a/a/a/c/ab;->g:I

    and-int/2addr v1, v2

    iput v1, p0, La/a/a/a/c/ai;->b:I

    aget-boolean v1, v0, v1

    if-eqz v1, :cond_1

    .line 454
    :cond_2
    iget v0, p0, La/a/a/a/c/ai;->c:I

    return v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 444
    iget v0, p0, La/a/a/a/c/ai;->d:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 458
    iget v0, p0, La/a/a/a/c/ai;->c:I

    if-ne v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 459
    :cond_0
    iget-object v0, p0, La/a/a/a/c/ai;->e:La/a/a/a/c/ab;

    iget v1, v0, La/a/a/a/c/ab;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, La/a/a/a/c/ab;->h:I

    .line 460
    iget-object v0, p0, La/a/a/a/c/ai;->e:La/a/a/a/c/ab;

    iget v1, p0, La/a/a/a/c/ai;->c:I

    invoke-virtual {v0, v1}, La/a/a/a/c/ab;->b(I)I

    move-result v0

    iget v1, p0, La/a/a/a/c/ai;->b:I

    if-ne v0, v1, :cond_1

    iget v0, p0, La/a/a/a/c/ai;->d:I

    if-lez v0, :cond_1

    .line 461
    iget v0, p0, La/a/a/a/c/ai;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/c/ai;->d:I

    .line 462
    invoke-virtual {p0}, La/a/a/a/c/ai;->b()I

    .line 464
    :cond_1
    iput v2, p0, La/a/a/a/c/ai;->c:I

    .line 466
    return-void
.end method
