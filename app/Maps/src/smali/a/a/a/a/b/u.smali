.class La/a/a/a/b/u;
.super La/a/a/a/b/aa;
.source "PG"

# interfaces
.implements La/a/a/a/d/bj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/b/s",
        "<TV;>.a/a/a/a/b/aa;",
        "La/a/a/a/d/bj",
        "<",
        "La/a/a/a/b/ad",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/b/s;

.field private b:La/a/a/a/b/y;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/b/s",
            "<TV;>.a/a/a/a/b/y;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(La/a/a/a/b/s;)V
    .locals 0

    .prologue
    .line 870
    iput-object p1, p0, La/a/a/a/b/u;->a:La/a/a/a/b/s;

    invoke-direct {p0, p1}, La/a/a/a/b/aa;-><init>(La/a/a/a/b/s;)V

    return-void
.end method


# virtual methods
.method public synthetic add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 868
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 868
    new-instance v0, La/a/a/a/b/y;

    iget-object v1, p0, La/a/a/a/b/u;->a:La/a/a/a/b/s;

    invoke-virtual {p0}, La/a/a/a/b/u;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, La/a/a/a/b/y;-><init>(La/a/a/a/b/s;I)V

    iput-object v0, p0, La/a/a/a/b/u;->b:La/a/a/a/b/y;

    return-object v0
.end method

.method public synthetic previous()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 868
    new-instance v0, La/a/a/a/b/y;

    iget-object v1, p0, La/a/a/a/b/u;->a:La/a/a/a/b/s;

    invoke-virtual {p0}, La/a/a/a/b/u;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, La/a/a/a/b/y;-><init>(La/a/a/a/b/s;I)V

    iput-object v0, p0, La/a/a/a/b/u;->b:La/a/a/a/b/y;

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 882
    invoke-super {p0}, La/a/a/a/b/aa;->remove()V

    .line 883
    iget-object v0, p0, La/a/a/a/b/u;->b:La/a/a/a/b/y;

    const/4 v1, -0x1

    iput v1, v0, La/a/a/a/b/y;->a:I

    .line 884
    return-void
.end method

.method public synthetic set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 868
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
