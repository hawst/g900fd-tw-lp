.class public La/a/a/a/c/ab;
.super La/a/a/a/c/b;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final serialVersionUID:J


# instance fields
.field public transient a:[J

.field public transient b:[I

.field public transient c:[Z

.field public final d:F

.field public transient e:I

.field public transient f:I

.field public transient g:I

.field public h:I

.field public volatile transient i:La/a/a/a/c/aa;

.field public volatile transient j:La/a/a/a/c/bg;

.field public volatile transient k:La/a/a/a/b/ai;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 128
    const/16 v0, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1}, La/a/a/a/c/ab;-><init>(IF)V

    .line 129
    return-void
.end method

.method private constructor <init>(IF)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, La/a/a/a/c/b;-><init>()V

    .line 107
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Load factor must be greater than 0 and smaller than or equal to 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_1
    if-gez p1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The expected number of elements must be nonnegative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_2
    iput p2, p0, La/a/a/a/c/ab;->d:F

    .line 110
    invoke-static {p1, p2}, La/a/a/a/c;->b(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/c/ab;->e:I

    .line 111
    iget v0, p0, La/a/a/a/c/ab;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/c/ab;->g:I

    .line 112
    iget v0, p0, La/a/a/a/c/ab;->e:I

    invoke-static {v0, p2}, La/a/a/a/c;->a(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/c/ab;->f:I

    .line 113
    iget v0, p0, La/a/a/a/c/ab;->e:I

    new-array v0, v0, [J

    iput-object v0, p0, La/a/a/a/c/ab;->a:[J

    .line 114
    iget v0, p0, La/a/a/a/c/ab;->e:I

    new-array v0, v0, [I

    iput-object v0, p0, La/a/a/a/c/ab;->b:[I

    .line 115
    iget v0, p0, La/a/a/a/c/ab;->e:I

    new-array v0, v0, [Z

    iput-object v0, p0, La/a/a/a/c/ab;->c:[Z

    .line 116
    return-void
.end method

.method private c(I)V
    .locals 17

    .prologue
    .line 677
    const/4 v3, 0x0

    .line 678
    move-object/from16 v0, p0

    iget-object v5, v0, La/a/a/a/c/ab;->c:[Z

    .line 680
    move-object/from16 v0, p0

    iget-object v6, v0, La/a/a/a/c/ab;->a:[J

    .line 681
    move-object/from16 v0, p0

    iget-object v7, v0, La/a/a/a/c/ab;->b:[I

    .line 682
    add-int/lit8 v8, p1, -0x1

    .line 683
    move/from16 v0, p1

    new-array v9, v0, [J

    .line 684
    move/from16 v0, p1

    new-array v10, v0, [I

    .line 685
    move/from16 v0, p1

    new-array v11, v0, [Z

    .line 686
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/ab;->h:I

    move/from16 v16, v2

    move v2, v3

    move/from16 v3, v16

    :goto_0
    add-int/lit8 v4, v3, -0x1

    if-eqz v3, :cond_2

    .line 687
    :goto_1
    aget-boolean v3, v5, v2

    if-nez v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 688
    :cond_0
    aget-wide v12, v6, v2

    .line 689
    invoke-static {v12, v13}, La/a/a/a/c;->a(J)J

    move-result-wide v14

    long-to-int v3, v14

    and-int/2addr v3, v8

    .line 690
    :goto_2
    aget-boolean v14, v11, v3

    if-eqz v14, :cond_1

    add-int/lit8 v3, v3, 0x1

    and-int/2addr v3, v8

    goto :goto_2

    .line 691
    :cond_1
    const/4 v14, 0x1

    aput-boolean v14, v11, v3

    .line 692
    aput-wide v12, v9, v3

    .line 693
    aget v12, v7, v2

    aput v12, v10, v3

    .line 694
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_0

    .line 696
    :cond_2
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/c/ab;->e:I

    .line 697
    move-object/from16 v0, p0

    iput v8, v0, La/a/a/a/c/ab;->g:I

    .line 698
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/ab;->e:I

    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/c/ab;->d:F

    invoke-static {v2, v3}, La/a/a/a/c;->a(IF)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, La/a/a/a/c/ab;->f:I

    .line 699
    move-object/from16 v0, p0

    iput-object v9, v0, La/a/a/a/c/ab;->a:[J

    .line 700
    move-object/from16 v0, p0

    iput-object v10, v0, La/a/a/a/c/ab;->b:[I

    .line 701
    move-object/from16 v0, p0

    iput-object v11, v0, La/a/a/a/c/ab;->c:[Z

    .line 702
    return-void
.end method

.method private e()La/a/a/a/c/ab;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 714
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/c/ab;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 719
    iput-object v1, v0, La/a/a/a/c/ab;->j:La/a/a/a/c/bg;

    .line 720
    iput-object v1, v0, La/a/a/a/c/ab;->k:La/a/a/a/b/ai;

    .line 721
    iput-object v1, v0, La/a/a/a/c/ab;->i:La/a/a/a/c/aa;

    .line 722
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    invoke-virtual {v1}, [J->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    iput-object v1, v0, La/a/a/a/c/ab;->a:[J

    .line 723
    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, La/a/a/a/c/ab;->b:[I

    .line 724
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    invoke-virtual {v1}, [Z->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Z

    iput-object v1, v0, La/a/a/a/c/ab;->c:[Z

    .line 725
    return-object v0

    .line 717
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/InternalError;

    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    throw v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 10

    .prologue
    .line 760
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 761
    iget v0, p0, La/a/a/a/c/ab;->h:I

    iget v1, p0, La/a/a/a/c/ab;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/c/ab;->e:I

    .line 762
    iget v0, p0, La/a/a/a/c/ab;->e:I

    iget v1, p0, La/a/a/a/c/ab;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->a(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/c/ab;->f:I

    .line 763
    iget v0, p0, La/a/a/a/c/ab;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/c/ab;->g:I

    .line 764
    iget v0, p0, La/a/a/a/c/ab;->e:I

    new-array v2, v0, [J

    iput-object v2, p0, La/a/a/a/c/ab;->a:[J

    .line 765
    iget v0, p0, La/a/a/a/c/ab;->e:I

    new-array v3, v0, [I

    iput-object v3, p0, La/a/a/a/c/ab;->b:[I

    .line 766
    iget v0, p0, La/a/a/a/c/ab;->e:I

    new-array v4, v0, [Z

    iput-object v4, p0, La/a/a/a/c/ab;->c:[Z

    .line 769
    iget v0, p0, La/a/a/a/c/ab;->h:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_1

    .line 770
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v6

    .line 771
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v5

    .line 772
    invoke-static {v6, v7}, La/a/a/a/c;->a(J)J

    move-result-wide v8

    long-to-int v0, v8

    iget v8, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v8

    .line 773
    :goto_1
    aget-boolean v8, v4, v0

    if-eqz v8, :cond_0

    add-int/lit8 v0, v0, 0x1

    iget v8, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v8

    goto :goto_1

    .line 774
    :cond_0
    const/4 v8, 0x1

    aput-boolean v8, v4, v0

    .line 775
    aput-wide v6, v2, v0

    .line 776
    aput v5, v3, v0

    move v0, v1

    goto :goto_0

    .line 779
    :cond_1
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 8

    .prologue
    .line 748
    iget-object v2, p0, La/a/a/a/c/ab;->a:[J

    .line 749
    iget-object v3, p0, La/a/a/a/c/ab;->b:[I

    .line 750
    new-instance v4, La/a/a/a/c/ai;

    invoke-direct {v4, p0}, La/a/a/a/c/ai;-><init>(La/a/a/a/c/ab;)V

    .line 751
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 752
    iget v0, p0, La/a/a/a/c/ab;->h:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    .line 753
    invoke-virtual {v4}, La/a/a/a/c/ai;->b()I

    move-result v0

    .line 754
    aget-wide v6, v2, v0

    invoke-virtual {p1, v6, v7}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 755
    aget v0, v3, v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    move v0, v1

    goto :goto_0

    .line 757
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 5

    .prologue
    .line 283
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    .line 285
    :goto_0
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 286
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 287
    iget v1, p0, La/a/a/a/c/ab;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/c/ab;->h:I

    .line 288
    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    aget v1, v1, v0

    .line 289
    invoke-virtual {p0, v0}, La/a/a/a/c/ab;->b(I)I

    move v0, v1

    .line 294
    :goto_1
    return v0

    .line 292
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 294
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(JI)I
    .locals 5

    .prologue
    .line 189
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    .line 191
    :goto_0
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 192
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 193
    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    aget v1, v1, v0

    .line 194
    iget-object v2, p0, La/a/a/a/c/ab;->b:[I

    aput p3, v2, v0

    move v0, v1

    .line 204
    :goto_1
    return v0

    .line 197
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 199
    :cond_1
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 200
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aput-wide p1, v1, v0

    .line 201
    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    aput p3, v1, v0

    .line 202
    iget v0, p0, La/a/a/a/c/ab;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/c/ab;->h:I

    iget v1, p0, La/a/a/a/c/ab;->f:I

    if-lt v0, v1, :cond_2

    iget v0, p0, La/a/a/a/c/ab;->h:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/c/ab;->c(I)V

    .line 204
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()La/a/a/a/c/bg;
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, La/a/a/a/c/ab;->j:La/a/a/a/c/bg;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/af;

    invoke-direct {v0, p0}, La/a/a/a/c/af;-><init>(La/a/a/a/c/ab;)V

    iput-object v0, p0, La/a/a/a/c/ab;->j:La/a/a/a/c/bg;

    .line 574
    :cond_0
    iget-object v0, p0, La/a/a/a/c/ab;->j:La/a/a/a/c/bg;

    return-object v0
.end method

.method public final a(Ljava/lang/Long;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 8

    .prologue
    .line 207
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 208
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 210
    invoke-static {v4, v5}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    .line 212
    :goto_0
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 213
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v6, v1, v0

    cmp-long v1, v6, v4

    if-nez v1, :cond_0

    .line 214
    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    aget v1, v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 215
    iget-object v3, p0, La/a/a/a/c/ab;->b:[I

    aput v2, v3, v0

    move-object v0, v1

    .line 225
    :goto_1
    return-object v0

    .line 218
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 220
    :cond_1
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    const/4 v3, 0x1

    aput-boolean v3, v1, v0

    .line 221
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aput-wide v4, v1, v0

    .line 222
    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    aput v2, v1, v0

    .line 223
    iget v0, p0, La/a/a/a/c/ab;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/c/ab;->h:I

    iget v1, p0, La/a/a/a/c/ab;->f:I

    if-lt v0, v1, :cond_2

    iget v0, p0, La/a/a/a/c/ab;->h:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/c/ab;->c(I)V

    .line 225
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 6

    .prologue
    .line 298
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 300
    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    .line 302
    :goto_0
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 303
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v4, v1, v0

    cmp-long v1, v4, v2

    if-nez v1, :cond_0

    .line 304
    iget v1, p0, La/a/a/a/c/ab;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/c/ab;->h:I

    .line 305
    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    aget v1, v1, v0

    .line 306
    invoke-virtual {p0, v0}, La/a/a/a/c/ab;->b(I)I

    .line 307
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 311
    :goto_1
    return-object v0

    .line 309
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 311
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(I)Z
    .locals 4

    .prologue
    .line 347
    iget-object v2, p0, La/a/a/a/c/ab;->b:[I

    .line 348
    iget-object v3, p0, La/a/a/a/c/ab;->c:[Z

    .line 349
    iget v0, p0, La/a/a/a/c/ab;->e:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    aget-boolean v0, v3, v1

    if-eqz v0, :cond_1

    aget v0, v2, v1

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    .line 350
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected final b(I)I
    .locals 4

    .prologue
    .line 267
    :goto_0
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    .line 268
    :goto_1
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 269
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v2, v1, v0

    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v2

    long-to-int v1, v2

    iget v2, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v1, v2

    .line 270
    if-gt p1, v0, :cond_1

    if-ge p1, v1, :cond_0

    if-le v1, v0, :cond_2

    .line 273
    :cond_0
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_3

    .line 274
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    iget-object v2, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v2, v2, v0

    aput-wide v2, v1, p1

    .line 275
    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    iget-object v2, p0, La/a/a/a/c/ab;->b:[I

    aget v2, v2, v0

    aput v2, v1, p1

    move p1, v0

    goto :goto_0

    .line 270
    :cond_1
    if-lt p1, v1, :cond_2

    if-gt v1, v0, :cond_0

    .line 271
    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 277
    :cond_3
    iget-object v0, p0, La/a/a/a/c/ab;->c:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 278
    return p1
.end method

.method public final b()La/a/a/a/b/ai;
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, La/a/a/a/c/ab;->k:La/a/a/a/b/ai;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/ac;

    invoke-direct {v0, p0}, La/a/a/a/c/ac;-><init>(La/a/a/a/c/ab;)V

    iput-object v0, p0, La/a/a/a/c/ab;->k:La/a/a/a/b/ai;

    .line 602
    :cond_0
    iget-object v0, p0, La/a/a/a/c/ab;->k:La/a/a/a/b/ai;

    return-object v0
.end method

.method public final b(J)Z
    .locals 5

    .prologue
    .line 338
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    .line 340
    :goto_0
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 341
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 344
    :goto_1
    return v0

    .line 342
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 344
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(J)I
    .locals 5

    .prologue
    .line 327
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    .line 329
    :goto_0
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 330
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    iget-object v1, p0, La/a/a/a/c/ab;->b:[I

    aget v0, v1, v0

    .line 333
    :goto_1
    return v0

    .line 331
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 333
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final synthetic c()La/a/a/a/d/bk;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, La/a/a/a/c/ab;->i:La/a/a/a/c/aa;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/ah;

    invoke-direct {v0, p0}, La/a/a/a/c/ah;-><init>(La/a/a/a/c/ab;)V

    iput-object v0, p0, La/a/a/a/c/ab;->i:La/a/a/a/c/aa;

    :cond_0
    iget-object v0, p0, La/a/a/a/c/ab;->i:La/a/a/a/c/aa;

    return-object v0
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 359
    iget v0, p0, La/a/a/a/c/ab;->h:I

    if-nez v0, :cond_0

    .line 363
    :goto_0
    return-void

    .line 360
    :cond_0
    iput v1, p0, La/a/a/a/c/ab;->h:I

    .line 361
    iget-object v0, p0, La/a/a/a/c/ab;->c:[Z

    invoke-static {v0, v1}, La/a/a/a/a/a;->a([ZZ)V

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, La/a/a/a/c/ab;->e()La/a/a/a/c/ab;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 631
    iget v1, p0, La/a/a/a/c/ab;->h:I

    iget v2, p0, La/a/a/a/c/ab;->d:F

    invoke-static {v1, v2}, La/a/a/a/c;->b(IF)I

    move-result v1

    .line 632
    iget v2, p0, La/a/a/a/c/ab;->e:I

    if-lt v1, v2, :cond_0

    .line 637
    :goto_0
    return v0

    .line 634
    :cond_0
    :try_start_0
    invoke-direct {p0, v1}, La/a/a/a/c/ab;->c(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 636
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 737
    .line 738
    iget v1, p0, La/a/a/a/c/ab;->h:I

    move v3, v0

    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-eqz v1, :cond_1

    .line 739
    :goto_1
    iget-object v1, p0, La/a/a/a/c/ab;->c:[Z

    aget-boolean v1, v1, v0

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 740
    :cond_0
    iget-object v1, p0, La/a/a/a/c/ab;->a:[J

    aget-wide v4, v1, v0

    invoke-static {v4, v5}, La/a/a/a/c;->b(J)I

    move-result v1

    .line 741
    iget-object v4, p0, La/a/a/a/c/ab;->b:[I

    aget v4, v4, v0

    xor-int/2addr v1, v4

    .line 742
    add-int/2addr v1, v3

    .line 743
    add-int/lit8 v0, v0, 0x1

    move v3, v1

    move v1, v2

    goto :goto_0

    .line 745
    :cond_1
    return v3
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 368
    iget v0, p0, La/a/a/a/c/ab;->h:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, La/a/a/a/c/ab;->j:La/a/a/a/c/bg;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/af;

    invoke-direct {v0, p0}, La/a/a/a/c/af;-><init>(La/a/a/a/c/ab;)V

    iput-object v0, p0, La/a/a/a/c/ab;->j:La/a/a/a/c/bg;

    :cond_0
    iget-object v0, p0, La/a/a/a/c/ab;->j:La/a/a/a/c/bg;

    return-object v0
.end method

.method public synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, La/a/a/a/c/ab;->a(Ljava/lang/Long;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0, p1}, La/a/a/a/c/ab;->a(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, La/a/a/a/c/ab;->h:I

    return v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, La/a/a/a/c/ab;->k:La/a/a/a/b/ai;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/ac;

    invoke-direct {v0, p0}, La/a/a/a/c/ac;-><init>(La/a/a/a/c/ab;)V

    iput-object v0, p0, La/a/a/a/c/ab;->k:La/a/a/a/b/ai;

    :cond_0
    iget-object v0, p0, La/a/a/a/c/ab;->k:La/a/a/a/b/ai;

    return-object v0
.end method
