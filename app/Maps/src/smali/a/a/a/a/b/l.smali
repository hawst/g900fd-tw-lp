.class public La/a/a/a/b/l;
.super La/a/a/a/d/u;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "La/a/a/a/d/u",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:La/a/a/a/d/bd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/bd",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(La/a/a/a/d/bd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/a/a/a/d/bd",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 164
    invoke-direct {p0}, La/a/a/a/d/u;-><init>()V

    .line 165
    iput-object p1, p0, La/a/a/a/b/l;->a:La/a/a/a/d/bd;

    .line 166
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, La/a/a/a/b/l;->a:La/a/a/a/d/bd;

    invoke-interface {v0}, La/a/a/a/d/bd;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, La/a/a/a/b/l;->a:La/a/a/a/d/bd;

    invoke-interface {v0}, La/a/a/a/d/bd;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
