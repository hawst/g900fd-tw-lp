.class final La/a/a/a/d/ai;
.super La/a/a/a/d/w;
.source "PG"

# interfaces
.implements La/a/a/a/d/ab;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/w",
        "<",
        "La/a/a/a/d/aa",
        "<TK;>;>;",
        "La/a/a/a/d/ab",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/d/ac;


# direct methods
.method constructor <init>(La/a/a/a/d/ac;)V
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    invoke-direct {p0}, La/a/a/a/d/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()La/a/a/a/d/bg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bg",
            "<",
            "La/a/a/a/d/aa",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 482
    new-instance v0, La/a/a/a/d/ae;

    iget-object v1, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    invoke-direct {v0, v1}, La/a/a/a/d/ae;-><init>(La/a/a/a/d/ac;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    invoke-virtual {v0}, La/a/a/a/d/ac;->clear()V

    .line 523
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 489
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 499
    :goto_0
    return v0

    .line 490
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 491
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 493
    if-nez v2, :cond_2

    const v0, 0x87fcd5c

    :goto_1
    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget v3, v3, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v3

    .line 495
    :goto_2
    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget-object v3, v3, La/a/a/a/d/ac;->c:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_6

    .line 496
    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget-object v3, v3, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-nez v3, :cond_3

    if-nez v2, :cond_4

    :cond_1
    iget-object v2, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget-object v2, v2, La/a/a/a/d/ac;->b:[I

    aget v2, v2, v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v2, v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    .line 493
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_1

    .line 496
    :cond_3
    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget-object v3, v3, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 497
    :cond_4
    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget v3, v3, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v3

    goto :goto_2

    :cond_5
    move v0, v1

    .line 496
    goto :goto_0

    :cond_6
    move v0, v1

    .line 499
    goto :goto_0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 480
    new-instance v0, La/a/a/a/d/ae;

    iget-object v1, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    invoke-direct {v0, v1}, La/a/a/a/d/ae;-><init>(La/a/a/a/d/ac;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 503
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 516
    :goto_0
    return v0

    .line 504
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 505
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 507
    if-nez v2, :cond_2

    const v0, 0x87fcd5c

    :goto_1
    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget v3, v3, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v3

    .line 509
    :goto_2
    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget-object v3, v3, La/a/a/a/d/ac;->c:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_5

    .line 510
    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget-object v3, v3, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-nez v3, :cond_3

    if-nez v2, :cond_4

    .line 511
    :cond_1
    iget-object v0, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/a/a/d/ac;->b(Ljava/lang/Object;)Ljava/lang/Integer;

    .line 512
    const/4 v0, 0x1

    goto :goto_0

    .line 507
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_1

    .line 510
    :cond_3
    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget-object v3, v3, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 514
    :cond_4
    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget v3, v3, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v3

    goto :goto_2

    :cond_5
    move v0, v1

    .line 516
    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, La/a/a/a/d/ai;->a:La/a/a/a/d/ac;

    iget v0, v0, La/a/a/a/d/ac;->h:I

    return v0
.end method
