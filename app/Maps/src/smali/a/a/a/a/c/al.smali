.class public La/a/a/a/c/al;
.super La/a/a/a/c/n;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "La/a/a/a/c/n",
        "<TV;>;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field public static final serialVersionUID:J


# instance fields
.field public transient a:[J

.field public transient b:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TV;"
        }
    .end annotation
.end field

.field public transient c:[Z

.field public final d:F

.field public transient e:I

.field public transient f:I

.field public transient g:I

.field public h:I

.field public volatile transient i:La/a/a/a/c/az;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/c/az",
            "<TV;>;"
        }
    .end annotation
.end field

.field public volatile transient j:La/a/a/a/c/bh;

.field public volatile transient k:La/a/a/a/d/be;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/be",
            "<TV;>;"
        }
    .end annotation
.end field

.field public transient l:I

.field public transient m:I

.field public transient n:[J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 161
    const/16 v0, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1}, La/a/a/a/c/al;-><init>(IF)V

    .line 162
    return-void
.end method

.method private constructor <init>(IF)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 138
    invoke-direct {p0}, La/a/a/a/c/n;-><init>()V

    .line 118
    iput v0, p0, La/a/a/a/c/al;->l:I

    .line 120
    iput v0, p0, La/a/a/a/c/al;->m:I

    .line 139
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Load factor must be greater than 0 and smaller than or equal to 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_1
    if-gez p1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The expected number of elements must be nonnegative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_2
    iput p2, p0, La/a/a/a/c/al;->d:F

    .line 142
    invoke-static {p1, p2}, La/a/a/a/c;->b(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/c/al;->e:I

    .line 143
    iget v0, p0, La/a/a/a/c/al;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/c/al;->g:I

    .line 144
    iget v0, p0, La/a/a/a/c/al;->e:I

    invoke-static {v0, p2}, La/a/a/a/c;->a(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/c/al;->f:I

    .line 145
    iget v0, p0, La/a/a/a/c/al;->e:I

    new-array v0, v0, [J

    iput-object v0, p0, La/a/a/a/c/al;->a:[J

    .line 146
    iget v0, p0, La/a/a/a/c/al;->e:I

    new-array v0, v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    .line 147
    iget v0, p0, La/a/a/a/c/al;->e:I

    new-array v0, v0, [Z

    iput-object v0, p0, La/a/a/a/c/al;->c:[Z

    .line 148
    iget v0, p0, La/a/a/a/c/al;->e:I

    new-array v0, v0, [J

    iput-object v0, p0, La/a/a/a/c/al;->n:[J

    .line 149
    return-void
.end method

.method private b(I)I
    .locals 4

    .prologue
    .line 290
    :goto_0
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    .line 291
    :goto_1
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aget-wide v2, v1, v0

    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v2

    long-to-int v1, v2

    iget v2, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v1, v2

    .line 293
    if-gt p1, v0, :cond_1

    if-ge p1, v1, :cond_0

    if-le v1, v0, :cond_2

    .line 296
    :cond_0
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_3

    .line 297
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    iget-object v2, p0, La/a/a/a/c/al;->a:[J

    aget-wide v2, v2, v0

    aput-wide v2, v1, p1

    .line 298
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget-object v2, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    aput-object v2, v1, p1

    .line 299
    invoke-virtual {p0, v0, p1}, La/a/a/a/c/al;->a(II)V

    move p1, v0

    goto :goto_0

    .line 293
    :cond_1
    if-lt p1, v1, :cond_2

    if-gt v1, v0, :cond_0

    .line 294
    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 301
    :cond_3
    iget-object v0, p0, La/a/a/a/c/al;->c:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 302
    iget-object v0, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 303
    return p1
.end method

.method private c(I)V
    .locals 12

    .prologue
    .line 653
    iget v0, p0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_1

    .line 654
    const/4 v0, -0x1

    iput v0, p0, La/a/a/a/c/al;->m:I

    iput v0, p0, La/a/a/a/c/al;->l:I

    .line 678
    :cond_0
    :goto_0
    return-void

    .line 657
    :cond_1
    iget v0, p0, La/a/a/a/c/al;->l:I

    if-ne v0, p1, :cond_2

    .line 658
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    aget-wide v0, v0, p1

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/c/al;->l:I

    .line 659
    iget v0, p0, La/a/a/a/c/al;->l:I

    if-ltz v0, :cond_0

    .line 661
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    iget v1, p0, La/a/a/a/c/al;->l:I

    aget-wide v2, v0, v1

    const-wide v4, -0x100000000L

    or-long/2addr v2, v4

    aput-wide v2, v0, v1

    goto :goto_0

    .line 665
    :cond_2
    iget v0, p0, La/a/a/a/c/al;->m:I

    if-ne v0, p1, :cond_3

    .line 666
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    aget-wide v0, v0, p1

    const/16 v2, 0x20

    ushr-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/c/al;->m:I

    .line 667
    iget v0, p0, La/a/a/a/c/al;->m:I

    if-ltz v0, :cond_0

    .line 669
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    iget v1, p0, La/a/a/a/c/al;->m:I

    aget-wide v2, v0, v1

    const-wide v4, 0xffffffffL

    or-long/2addr v2, v4

    aput-wide v2, v0, v1

    goto :goto_0

    .line 673
    :cond_3
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    aget-wide v0, v0, p1

    .line 674
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    long-to-int v2, v2

    .line 675
    long-to-int v3, v0

    .line 676
    iget-object v4, p0, La/a/a/a/c/al;->n:[J

    aget-wide v6, v4, v2

    iget-object v5, p0, La/a/a/a/c/al;->n:[J

    aget-wide v8, v5, v2

    const-wide v10, 0xffffffffL

    and-long/2addr v10, v0

    xor-long/2addr v8, v10

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    aput-wide v6, v4, v2

    .line 677
    iget-object v2, p0, La/a/a/a/c/al;->n:[J

    aget-wide v4, v2, v3

    iget-object v6, p0, La/a/a/a/c/al;->n:[J

    aget-wide v6, v6, v3

    const-wide v8, -0x100000000L

    and-long/2addr v0, v8

    xor-long/2addr v0, v6

    const-wide v6, -0x100000000L

    and-long/2addr v0, v6

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    goto :goto_0
.end method

.method private d(I)V
    .locals 24

    .prologue
    .line 1135
    move-object/from16 v0, p0

    iget v7, v0, La/a/a/a/c/al;->l:I

    const/4 v5, -0x1

    const/4 v4, -0x1

    .line 1137
    move-object/from16 v0, p0

    iget-object v8, v0, La/a/a/a/c/al;->a:[J

    .line 1138
    move-object/from16 v0, p0

    iget-object v9, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    .line 1139
    add-int/lit8 v10, p1, -0x1

    .line 1140
    move/from16 v0, p1

    new-array v11, v0, [J

    .line 1141
    move/from16 v0, p1

    new-array v2, v0, [Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    .line 1142
    move/from16 v0, p1

    new-array v12, v0, [Z

    .line 1143
    move-object/from16 v0, p0

    iget-object v13, v0, La/a/a/a/c/al;->n:[J

    .line 1144
    move/from16 v0, p1

    new-array v14, v0, [J

    .line 1145
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, La/a/a/a/c/al;->l:I

    .line 1146
    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/c/al;->h:I

    move v6, v5

    move v5, v4

    :goto_0
    add-int/lit8 v4, v3, -0x1

    if-eqz v3, :cond_2

    .line 1147
    aget-wide v16, v8, v7

    .line 1148
    invoke-static/range {v16 .. v17}, La/a/a/a/c;->a(J)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v3, v0

    and-int/2addr v3, v10

    .line 1149
    :goto_1
    aget-boolean v15, v12, v3

    if-eqz v15, :cond_0

    add-int/lit8 v3, v3, 0x1

    and-int/2addr v3, v10

    goto :goto_1

    .line 1150
    :cond_0
    const/4 v15, 0x1

    aput-boolean v15, v12, v3

    .line 1151
    aput-wide v16, v11, v3

    .line 1152
    aget-object v15, v9, v7

    aput-object v15, v2, v3

    .line 1153
    const/4 v15, -0x1

    if-eq v6, v15, :cond_1

    .line 1154
    aget-wide v16, v14, v5

    aget-wide v18, v14, v5

    int-to-long v0, v3

    move-wide/from16 v20, v0

    const-wide v22, 0xffffffffL

    and-long v20, v20, v22

    xor-long v18, v18, v20

    const-wide v20, 0xffffffffL

    and-long v18, v18, v20

    xor-long v16, v16, v18

    aput-wide v16, v14, v5

    .line 1155
    aget-wide v16, v14, v3

    aget-wide v18, v14, v3

    int-to-long v0, v5

    move-wide/from16 v20, v0

    const-wide v22, 0xffffffffL

    and-long v20, v20, v22

    const/16 v5, 0x20

    shl-long v20, v20, v5

    xor-long v18, v18, v20

    const-wide v20, -0x100000000L

    and-long v18, v18, v20

    xor-long v16, v16, v18

    aput-wide v16, v14, v3

    .line 1164
    :goto_2
    aget-wide v16, v13, v7

    move-wide/from16 v0, v16

    long-to-int v5, v0

    move v6, v7

    move v7, v5

    move v5, v3

    move v3, v4

    .line 1165
    goto :goto_0

    .line 1159
    :cond_1
    move-object/from16 v0, p0

    iput v3, v0, La/a/a/a/c/al;->l:I

    .line 1161
    const-wide/16 v16, -0x1

    aput-wide v16, v14, v3

    goto :goto_2

    .line 1167
    :cond_2
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/c/al;->e:I

    .line 1168
    move-object/from16 v0, p0

    iput v10, v0, La/a/a/a/c/al;->g:I

    .line 1169
    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/c/al;->e:I

    move-object/from16 v0, p0

    iget v4, v0, La/a/a/a/c/al;->d:F

    invoke-static {v3, v4}, La/a/a/a/c;->a(IF)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, La/a/a/a/c/al;->f:I

    .line 1170
    move-object/from16 v0, p0

    iput-object v11, v0, La/a/a/a/c/al;->a:[J

    .line 1171
    move-object/from16 v0, p0

    iput-object v2, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    .line 1172
    move-object/from16 v0, p0

    iput-object v12, v0, La/a/a/a/c/al;->c:[Z

    .line 1173
    move-object/from16 v0, p0

    iput-object v14, v0, La/a/a/a/c/al;->n:[J

    .line 1174
    move-object/from16 v0, p0

    iput v5, v0, La/a/a/a/c/al;->m:I

    .line 1175
    const/4 v2, -0x1

    if-eq v5, v2, :cond_3

    .line 1177
    aget-wide v2, v14, v5

    const-wide v6, 0xffffffffL

    or-long/2addr v2, v6

    aput-wide v2, v14, v5

    .line 1178
    :cond_3
    return-void
.end method

.method private n()La/a/a/a/c/al;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/c/al",
            "<TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1190
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/c/al;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1195
    iput-object v1, v0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    .line 1196
    iput-object v1, v0, La/a/a/a/c/al;->k:La/a/a/a/d/be;

    .line 1197
    iput-object v1, v0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    .line 1198
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    invoke-virtual {v1}, [J->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    iput-object v1, v0, La/a/a/a/c/al;->a:[J

    .line 1199
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    invoke-virtual {v1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    iput-object v1, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    .line 1200
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    invoke-virtual {v1}, [Z->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Z

    iput-object v1, v0, La/a/a/a/c/al;->c:[Z

    .line 1201
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    invoke-virtual {v1}, [J->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    iput-object v1, v0, La/a/a/a/c/al;->n:[J

    .line 1202
    return-object v0

    .line 1193
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/InternalError;

    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    throw v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 18

    .prologue
    .line 1238
    invoke-virtual/range {p1 .. p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1239
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/al;->h:I

    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/c/al;->d:F

    invoke-static {v2, v3}, La/a/a/a/c;->b(IF)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, La/a/a/a/c/al;->e:I

    .line 1240
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/al;->e:I

    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/c/al;->d:F

    invoke-static {v2, v3}, La/a/a/a/c;->a(IF)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, La/a/a/a/c/al;->f:I

    .line 1241
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/al;->e:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, La/a/a/a/c/al;->g:I

    .line 1242
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/al;->e:I

    new-array v6, v2, [J

    move-object/from16 v0, p0

    iput-object v6, v0, La/a/a/a/c/al;->a:[J

    .line 1243
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/al;->e:I

    new-array v2, v2, [Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    move-object/from16 v0, p0

    iput-object v2, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    .line 1244
    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/c/al;->e:I

    new-array v7, v3, [Z

    move-object/from16 v0, p0

    iput-object v7, v0, La/a/a/a/c/al;->c:[Z

    .line 1245
    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/c/al;->e:I

    new-array v8, v3, [J

    move-object/from16 v0, p0

    iput-object v8, v0, La/a/a/a/c/al;->n:[J

    .line 1246
    const/4 v4, -0x1

    .line 1247
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, La/a/a/a/c/al;->m:I

    move-object/from16 v0, p0

    iput v3, v0, La/a/a/a/c/al;->l:I

    .line 1250
    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/c/al;->h:I

    move v5, v4

    :goto_0
    add-int/lit8 v4, v3, -0x1

    if-eqz v3, :cond_2

    .line 1251
    invoke-virtual/range {p1 .. p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v10

    .line 1252
    invoke-virtual/range {p1 .. p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v9

    .line 1253
    invoke-static {v10, v11}, La/a/a/a/c;->a(J)J

    move-result-wide v12

    long-to-int v3, v12

    move-object/from16 v0, p0

    iget v12, v0, La/a/a/a/c/al;->g:I

    and-int/2addr v3, v12

    .line 1254
    :goto_1
    aget-boolean v12, v7, v3

    if-eqz v12, :cond_0

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iget v12, v0, La/a/a/a/c/al;->g:I

    and-int/2addr v3, v12

    goto :goto_1

    .line 1255
    :cond_0
    const/4 v12, 0x1

    aput-boolean v12, v7, v3

    .line 1256
    aput-wide v10, v6, v3

    .line 1257
    aput-object v9, v2, v3

    .line 1258
    move-object/from16 v0, p0

    iget v9, v0, La/a/a/a/c/al;->l:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_1

    .line 1259
    aget-wide v10, v8, v5

    aget-wide v12, v8, v5

    int-to-long v14, v3

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    xor-long/2addr v12, v14

    const-wide v14, 0xffffffffL

    and-long/2addr v12, v14

    xor-long/2addr v10, v12

    aput-wide v10, v8, v5

    .line 1260
    aget-wide v10, v8, v3

    aget-wide v12, v8, v3

    int-to-long v14, v5

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    const/16 v5, 0x20

    shl-long/2addr v14, v5

    xor-long/2addr v12, v14

    const-wide v14, -0x100000000L

    and-long/2addr v12, v14

    xor-long/2addr v10, v12

    aput-wide v10, v8, v3

    move v5, v3

    move v3, v4

    .line 1261
    goto :goto_0

    .line 1264
    :cond_1
    move-object/from16 v0, p0

    iput v3, v0, La/a/a/a/c/al;->l:I

    .line 1266
    aget-wide v10, v8, v3

    const-wide v12, -0x100000000L

    or-long/2addr v10, v12

    aput-wide v10, v8, v3

    move v5, v3

    move v3, v4

    goto :goto_0

    .line 1269
    :cond_2
    move-object/from16 v0, p0

    iput v5, v0, La/a/a/a/c/al;->m:I

    .line 1270
    const/4 v2, -0x1

    if-eq v5, v2, :cond_3

    .line 1272
    aget-wide v2, v8, v5

    const-wide v6, 0xffffffffL

    or-long/2addr v2, v6

    aput-wide v2, v8, v5

    .line 1274
    :cond_3
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 8

    .prologue
    .line 1226
    iget-object v2, p0, La/a/a/a/c/al;->a:[J

    .line 1227
    iget-object v3, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    .line 1228
    new-instance v4, La/a/a/a/c/at;

    invoke-direct {v4, p0}, La/a/a/a/c/at;-><init>(La/a/a/a/c/al;)V

    .line 1229
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 1230
    iget v0, p0, La/a/a/a/c/al;->h:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    .line 1231
    invoke-virtual {v4}, La/a/a/a/c/at;->b()I

    move-result v0

    .line 1232
    aget-wide v6, v2, v0

    invoke-virtual {p1, v6, v7}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 1233
    aget-object v0, v3, v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    .line 1235
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/c/bg;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/aq;

    invoke-direct {v0, p0}, La/a/a/a/c/aq;-><init>(La/a/a/a/c/al;)V

    iput-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    return-object v0
.end method

.method public final a(J)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TV;"
        }
    .end annotation

    .prologue
    .line 308
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    .line 310
    :goto_0
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 311
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 312
    iget v1, p0, La/a/a/a/c/al;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/c/al;->h:I

    .line 313
    invoke-direct {p0, v0}, La/a/a/a/c/al;->c(I)V

    .line 314
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 315
    invoke-direct {p0, v0}, La/a/a/a/c/al;->b(I)I

    move-object v0, v1

    .line 320
    :goto_1
    return-object v0

    .line 318
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 320
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(JLjava/lang/Object;)Ljava/lang/Object;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTV;)TV;"
        }
    .end annotation

    .prologue
    const-wide v10, 0xffffffffL

    .line 222
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    .line 224
    :goto_0
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 225
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 226
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 227
    iget-object v2, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aput-object p3, v2, v0

    move-object v0, v1

    .line 247
    :goto_1
    return-object v0

    .line 230
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 232
    :cond_1
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 233
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aput-wide p1, v1, v0

    .line 234
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aput-object p3, v1, v0

    .line 235
    iget v1, p0, La/a/a/a/c/al;->h:I

    if-nez v1, :cond_3

    .line 236
    iput v0, p0, La/a/a/a/c/al;->m:I

    iput v0, p0, La/a/a/a/c/al;->l:I

    .line 238
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    .line 245
    :goto_2
    iget v0, p0, La/a/a/a/c/al;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/c/al;->h:I

    iget v1, p0, La/a/a/a/c/al;->f:I

    if-lt v0, v1, :cond_2

    iget v0, p0, La/a/a/a/c/al;->h:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/c/al;->d(I)V

    .line 247
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 241
    :cond_3
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    iget v2, p0, La/a/a/a/c/al;->m:I

    aget-wide v4, v1, v2

    iget-object v3, p0, La/a/a/a/c/al;->n:[J

    iget v6, p0, La/a/a/a/c/al;->m:I

    aget-wide v6, v3, v6

    int-to-long v8, v0

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    and-long/2addr v6, v10

    xor-long/2addr v4, v6

    aput-wide v4, v1, v2

    .line 242
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    iget v2, p0, La/a/a/a/c/al;->m:I

    int-to-long v2, v2

    and-long/2addr v2, v10

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v2, v10

    aput-wide v2, v1, v0

    .line 243
    iput v0, p0, La/a/a/a/c/al;->m:I

    goto :goto_2
.end method

.method public final a(Ljava/lang/Long;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    const-wide v10, 0xffffffffL

    .line 250
    .line 251
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 253
    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    .line 255
    :goto_0
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 256
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aget-wide v4, v1, v0

    cmp-long v1, v4, v2

    if-nez v1, :cond_0

    .line 257
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 258
    iget-object v2, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aput-object p2, v2, v0

    move-object v0, v1

    .line 278
    :goto_1
    return-object v0

    .line 261
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 263
    :cond_1
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    const/4 v4, 0x1

    aput-boolean v4, v1, v0

    .line 264
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aput-wide v2, v1, v0

    .line 265
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aput-object p2, v1, v0

    .line 266
    iget v1, p0, La/a/a/a/c/al;->h:I

    if-nez v1, :cond_3

    .line 267
    iput v0, p0, La/a/a/a/c/al;->m:I

    iput v0, p0, La/a/a/a/c/al;->l:I

    .line 269
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    .line 276
    :goto_2
    iget v0, p0, La/a/a/a/c/al;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/c/al;->h:I

    iget v1, p0, La/a/a/a/c/al;->f:I

    if-lt v0, v1, :cond_2

    iget v0, p0, La/a/a/a/c/al;->h:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/c/al;->d(I)V

    .line 278
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 272
    :cond_3
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    iget v2, p0, La/a/a/a/c/al;->m:I

    aget-wide v4, v1, v2

    iget-object v3, p0, La/a/a/a/c/al;->n:[J

    iget v6, p0, La/a/a/a/c/al;->m:I

    aget-wide v6, v3, v6

    int-to-long v8, v0

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    and-long/2addr v6, v10

    xor-long/2addr v4, v6

    aput-wide v4, v1, v2

    .line 273
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    iget v2, p0, La/a/a/a/c/al;->m:I

    int-to-long v2, v2

    and-long/2addr v2, v10

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v2, v10

    aput-wide v2, v1, v0

    .line 274
    iput v0, p0, La/a/a/a/c/al;->m:I

    goto :goto_2
.end method

.method public a(I)V
    .locals 12

    .prologue
    .line 395
    iget v0, p0, La/a/a/a/c/al;->h:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, La/a/a/a/c/al;->m:I

    if-ne v0, p1, :cond_1

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    iget v0, p0, La/a/a/a/c/al;->l:I

    if-ne v0, p1, :cond_2

    .line 397
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    aget-wide v0, v0, p1

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/c/al;->l:I

    .line 399
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    iget v1, p0, La/a/a/a/c/al;->l:I

    aget-wide v2, v0, v1

    const-wide v4, -0x100000000L

    or-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 408
    :goto_1
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    iget v1, p0, La/a/a/a/c/al;->m:I

    aget-wide v2, v0, v1

    iget-object v4, p0, La/a/a/a/c/al;->n:[J

    iget v5, p0, La/a/a/a/c/al;->m:I

    aget-wide v4, v4, v5

    int-to-long v6, p1

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    xor-long/2addr v4, v6

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    xor-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 409
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    iget v1, p0, La/a/a/a/c/al;->m:I

    int-to-long v2, v1

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    const/16 v1, 0x20

    shl-long/2addr v2, v1

    const-wide v4, 0xffffffffL

    or-long/2addr v2, v4

    aput-wide v2, v0, p1

    .line 410
    iput p1, p0, La/a/a/a/c/al;->m:I

    goto :goto_0

    .line 402
    :cond_2
    iget-object v0, p0, La/a/a/a/c/al;->n:[J

    aget-wide v0, v0, p1

    .line 403
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    long-to-int v2, v2

    .line 404
    long-to-int v3, v0

    .line 405
    iget-object v4, p0, La/a/a/a/c/al;->n:[J

    aget-wide v6, v4, v2

    iget-object v5, p0, La/a/a/a/c/al;->n:[J

    aget-wide v8, v5, v2

    const-wide v10, 0xffffffffL

    and-long/2addr v10, v0

    xor-long/2addr v8, v10

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    aput-wide v6, v4, v2

    .line 406
    iget-object v2, p0, La/a/a/a/c/al;->n:[J

    aget-wide v4, v2, v3

    iget-object v6, p0, La/a/a/a/c/al;->n:[J

    aget-wide v6, v6, v3

    const-wide v8, -0x100000000L

    and-long/2addr v0, v8

    xor-long/2addr v0, v6

    const-wide v6, -0x100000000L

    and-long/2addr v0, v6

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    goto :goto_1
.end method

.method protected final a(II)V
    .locals 16

    .prologue
    .line 688
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/al;->h:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 689
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/c/al;->m:I

    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/c/al;->l:I

    .line 691
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/c/al;->n:[J

    const-wide/16 v4, -0x1

    aput-wide v4, v2, p2

    .line 712
    :goto_0
    return-void

    .line 694
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/al;->l:I

    move/from16 v0, p1

    if-ne v2, v0, :cond_1

    .line 695
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/c/al;->l:I

    .line 696
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/c/al;->n:[J

    move-object/from16 v0, p0

    iget-object v3, v0, La/a/a/a/c/al;->n:[J

    aget-wide v4, v3, p1

    long-to-int v3, v4

    aget-wide v4, v2, v3

    move-object/from16 v0, p0

    iget-object v6, v0, La/a/a/a/c/al;->n:[J

    move-object/from16 v0, p0

    iget-object v7, v0, La/a/a/a/c/al;->n:[J

    aget-wide v8, v7, p1

    long-to-int v7, v8

    aget-wide v6, v6, v7

    move/from16 v0, p2

    int-to-long v8, v0

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    const/16 v10, 0x20

    shl-long/2addr v8, v10

    xor-long/2addr v6, v8

    const-wide v8, -0x100000000L

    and-long/2addr v6, v8

    xor-long/2addr v4, v6

    aput-wide v4, v2, v3

    .line 697
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/c/al;->n:[J

    move-object/from16 v0, p0

    iget-object v3, v0, La/a/a/a/c/al;->n:[J

    aget-wide v4, v3, p1

    aput-wide v4, v2, p2

    goto :goto_0

    .line 700
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/c/al;->m:I

    move/from16 v0, p1

    if-ne v2, v0, :cond_2

    .line 701
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/c/al;->m:I

    .line 702
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/c/al;->n:[J

    move-object/from16 v0, p0

    iget-object v3, v0, La/a/a/a/c/al;->n:[J

    aget-wide v4, v3, p1

    const/16 v3, 0x20

    ushr-long/2addr v4, v3

    long-to-int v3, v4

    aget-wide v4, v2, v3

    move-object/from16 v0, p0

    iget-object v6, v0, La/a/a/a/c/al;->n:[J

    move-object/from16 v0, p0

    iget-object v7, v0, La/a/a/a/c/al;->n:[J

    aget-wide v8, v7, p1

    const/16 v7, 0x20

    ushr-long/2addr v8, v7

    long-to-int v7, v8

    aget-wide v6, v6, v7

    move/from16 v0, p2

    int-to-long v8, v0

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    xor-long/2addr v4, v6

    aput-wide v4, v2, v3

    .line 703
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/c/al;->n:[J

    move-object/from16 v0, p0

    iget-object v3, v0, La/a/a/a/c/al;->n:[J

    aget-wide v4, v3, p1

    aput-wide v4, v2, p2

    goto/16 :goto_0

    .line 706
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/c/al;->n:[J

    aget-wide v2, v2, p1

    .line 707
    const/16 v4, 0x20

    ushr-long v4, v2, v4

    long-to-int v4, v4

    .line 708
    long-to-int v5, v2

    .line 709
    move-object/from16 v0, p0

    iget-object v6, v0, La/a/a/a/c/al;->n:[J

    aget-wide v8, v6, v4

    move-object/from16 v0, p0

    iget-object v7, v0, La/a/a/a/c/al;->n:[J

    aget-wide v10, v7, v4

    move/from16 v0, p2

    int-to-long v12, v0

    const-wide v14, 0xffffffffL

    and-long/2addr v12, v14

    xor-long/2addr v10, v12

    const-wide v12, 0xffffffffL

    and-long/2addr v10, v12

    xor-long/2addr v8, v10

    aput-wide v8, v6, v4

    .line 710
    move-object/from16 v0, p0

    iget-object v4, v0, La/a/a/a/c/al;->n:[J

    aget-wide v6, v4, v5

    move-object/from16 v0, p0

    iget-object v8, v0, La/a/a/a/c/al;->n:[J

    aget-wide v8, v8, v5

    move/from16 v0, p2

    int-to-long v10, v0

    const-wide v12, 0xffffffffL

    and-long/2addr v10, v12

    const/16 v12, 0x20

    shl-long/2addr v10, v12

    xor-long/2addr v8, v10

    const-wide v10, -0x100000000L

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    aput-wide v6, v4, v5

    .line 711
    move-object/from16 v0, p0

    iget-object v4, v0, La/a/a/a/c/al;->n:[J

    aput-wide v2, v4, p2

    goto/16 :goto_0
.end method

.method public final b()La/a/a/a/d/be;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/be",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1046
    iget-object v0, p0, La/a/a/a/c/al;->k:La/a/a/a/d/be;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/am;

    invoke-direct {v0, p0}, La/a/a/a/c/am;-><init>(La/a/a/a/c/al;)V

    iput-object v0, p0, La/a/a/a/c/al;->k:La/a/a/a/d/be;

    .line 1060
    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->k:La/a/a/a/d/be;

    return-object v0
.end method

.method public final b(JLjava/lang/Object;)Ljava/lang/Object;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTV;)TV;"
        }
    .end annotation

    .prologue
    const-wide v10, 0xffffffffL

    .line 500
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    .line 501
    iget-object v2, p0, La/a/a/a/c/al;->c:[Z

    .line 502
    iget v3, p0, La/a/a/a/c/al;->g:I

    .line 504
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v4

    long-to-int v0, v4

    and-int/2addr v0, v3

    .line 506
    :goto_0
    aget-boolean v4, v2, v0

    if-eqz v4, :cond_1

    .line 507
    aget-wide v4, v1, v0

    cmp-long v4, p1, v4

    if-nez v4, :cond_0

    .line 508
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 509
    iget-object v2, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aput-object p3, v2, v0

    .line 510
    invoke-virtual {p0, v0}, La/a/a/a/c/al;->a(I)V

    move-object v0, v1

    .line 530
    :goto_1
    return-object v0

    .line 513
    :cond_0
    add-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v3

    goto :goto_0

    .line 515
    :cond_1
    const/4 v3, 0x1

    aput-boolean v3, v2, v0

    .line 516
    aput-wide p1, v1, v0

    .line 517
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aput-object p3, v1, v0

    .line 518
    iget v1, p0, La/a/a/a/c/al;->h:I

    if-nez v1, :cond_3

    .line 519
    iput v0, p0, La/a/a/a/c/al;->m:I

    iput v0, p0, La/a/a/a/c/al;->l:I

    .line 521
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    .line 528
    :goto_2
    iget v0, p0, La/a/a/a/c/al;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/c/al;->h:I

    iget v1, p0, La/a/a/a/c/al;->f:I

    if-lt v0, v1, :cond_2

    iget v0, p0, La/a/a/a/c/al;->h:I

    iget v1, p0, La/a/a/a/c/al;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/c/al;->d(I)V

    .line 530
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 524
    :cond_3
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    iget v2, p0, La/a/a/a/c/al;->m:I

    aget-wide v4, v1, v2

    iget-object v3, p0, La/a/a/a/c/al;->n:[J

    iget v6, p0, La/a/a/a/c/al;->m:I

    aget-wide v6, v3, v6

    int-to-long v8, v0

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    and-long/2addr v6, v10

    xor-long/2addr v4, v6

    aput-wide v4, v1, v2

    .line 525
    iget-object v1, p0, La/a/a/a/c/al;->n:[J

    iget v2, p0, La/a/a/a/c/al;->m:I

    int-to-long v2, v2

    and-long/2addr v2, v10

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v2, v10

    aput-wide v2, v1, v0

    .line 526
    iput v0, p0, La/a/a/a/c/al;->m:I

    goto :goto_2
.end method

.method public final b(J)Z
    .locals 5

    .prologue
    .line 557
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    .line 559
    :goto_0
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 560
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 563
    :goto_1
    return v0

    .line 561
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 563
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(J)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TV;"
        }
    .end annotation

    .prologue
    .line 546
    invoke-static {p1, p2}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    .line 548
    :goto_0
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 549
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0

    .line 552
    :goto_1
    return-object v0

    .line 550
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 552
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 578
    iget v0, p0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_0

    .line 584
    :goto_0
    return-void

    .line 579
    :cond_0
    iput v1, p0, La/a/a/a/c/al;->h:I

    .line 580
    iget-object v0, p0, La/a/a/a/c/al;->c:[Z

    invoke-static {v0, v1}, La/a/a/a/a/a;->a([ZZ)V

    .line 582
    iget-object v0, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, La/a/a/a/d/bb;->a([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 583
    const/4 v0, -0x1

    iput v0, p0, La/a/a/a/c/al;->m:I

    iput v0, p0, La/a/a/a/c/al;->l:I

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, La/a/a/a/c/al;->n()La/a/a/a/c/al;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 566
    iget-object v2, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    .line 567
    iget-object v3, p0, La/a/a/a/c/al;->c:[Z

    .line 568
    iget v0, p0, La/a/a/a/c/al;->e:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_3

    aget-boolean v0, v3, v1

    if-eqz v0, :cond_2

    aget-object v0, v2, v1

    if-nez v0, :cond_1

    if-nez p1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 569
    :goto_1
    return v0

    .line 568
    :cond_1
    aget-object v0, v2, v1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 569
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d()La/a/a/a/c/bh;
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/aq;

    invoke-direct {v0, p0}, La/a/a/a/c/aq;-><init>(La/a/a/a/c/al;)V

    iput-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    .line 1030
    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 718
    iget v0, p0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 719
    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->a:[J

    iget v1, p0, La/a/a/a/c/al;->l:I

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 726
    iget v0, p0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 727
    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->a:[J

    iget v1, p0, La/a/a/a/c/al;->m:I

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final g()La/a/a/a/c/bc;
    .locals 1

    .prologue
    .line 729
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()La/a/a/a/c/ay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/c/ay",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 730
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1214
    .line 1215
    iget v0, p0, La/a/a/a/c/al;->h:I

    move v1, v0

    move v5, v2

    move v0, v2

    :goto_0
    add-int/lit8 v4, v1, -0x1

    if-eqz v1, :cond_2

    .line 1216
    :goto_1
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1217
    :cond_0
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aget-wide v6, v1, v0

    invoke-static {v6, v7}, La/a/a/a/c;->b(J)I

    move-result v3

    .line 1218
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-eq p0, v1, :cond_3

    .line 1219
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    move v1, v2

    :goto_2
    xor-int/2addr v1, v3

    .line 1220
    :goto_3
    add-int/2addr v1, v5

    .line 1221
    add-int/lit8 v0, v0, 0x1

    move v5, v1

    move v1, v4

    goto :goto_0

    .line 1219
    :cond_1
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    .line 1223
    :cond_2
    return v5

    :cond_3
    move v1, v3

    goto :goto_3
.end method

.method public final i()La/a/a/a/c/ay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/c/ay",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 731
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 589
    iget v0, p0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()La/a/a/a/c/ay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/c/ay",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 732
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1089
    iget v1, p0, La/a/a/a/c/al;->h:I

    iget v2, p0, La/a/a/a/c/al;->d:F

    invoke-static {v1, v2}, La/a/a/a/c;->b(IF)I

    move-result v1

    .line 1090
    iget v2, p0, La/a/a/a/c/al;->e:I

    if-lt v1, v2, :cond_0

    .line 1095
    :goto_0
    return v0

    .line 1092
    :cond_0
    :try_start_0
    invoke-direct {p0, v1}, La/a/a/a/c/al;->d(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1094
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/aq;

    invoke-direct {v0, p0}, La/a/a/a/c/aq;-><init>(La/a/a/a/c/al;)V

    iput-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->j:La/a/a/a/c/bh;

    return-object v0
.end method

.method public final synthetic l()La/a/a/a/d/bl;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/as;

    invoke-direct {v0, p0}, La/a/a/a/c/as;-><init>(La/a/a/a/c/al;)V

    iput-object v0, p0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    return-object v0
.end method

.method public final synthetic m()La/a/a/a/d/bk;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/as;

    invoke-direct {v0, p0}, La/a/a/a/c/as;-><init>(La/a/a/a/c/al;)V

    iput-object v0, p0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->i:La/a/a/a/c/az;

    return-object v0
.end method

.method public synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, La/a/a/a/c/al;->a(Ljava/lang/Long;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 324
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 326
    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    .line 328
    :goto_0
    iget-object v1, p0, La/a/a/a/c/al;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 329
    iget-object v1, p0, La/a/a/a/c/al;->a:[J

    aget-wide v4, v1, v0

    cmp-long v1, v4, v2

    if-nez v1, :cond_0

    .line 330
    iget v1, p0, La/a/a/a/c/al;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/c/al;->h:I

    .line 331
    invoke-direct {p0, v0}, La/a/a/a/c/al;->c(I)V

    .line 332
    iget-object v1, p0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 333
    invoke-direct {p0, v0}, La/a/a/a/c/al;->b(I)I

    move-object v0, v1

    .line 338
    :goto_1
    return-object v0

    .line 336
    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v1

    goto :goto_0

    .line 338
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 586
    iget v0, p0, La/a/a/a/c/al;->h:I

    return v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, La/a/a/a/c/al;->k:La/a/a/a/d/be;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/c/am;

    invoke-direct {v0, p0}, La/a/a/a/c/am;-><init>(La/a/a/a/c/al;)V

    iput-object v0, p0, La/a/a/a/c/al;->k:La/a/a/a/d/be;

    :cond_0
    iget-object v0, p0, La/a/a/a/c/al;->k:La/a/a/a/d/be;

    return-object v0
.end method
