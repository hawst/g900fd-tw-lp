.class public abstract La/a/a/a/d/h;
.super La/a/a/a/d/g;
.source "PG"

# interfaces
.implements La/a/a/a/d/aw;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "La/a/a/a/d/g",
        "<TK;TV;>;",
        "La/a/a/a/d/aw",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final serialVersionUID:J = -0x44907a65b4c385f2L


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, La/a/a/a/d/g;-><init>()V

    return-void
.end method


# virtual methods
.method public a()La/a/a/a/d/bk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bk",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, La/a/a/a/d/i;

    invoke-direct {v0, p0}, La/a/a/a/d/i;-><init>(La/a/a/a/d/h;)V

    return-object v0
.end method

.method public b()La/a/a/a/d/be;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/be",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v0, La/a/a/a/d/k;

    invoke-direct {v0, p0}, La/a/a/a/d/k;-><init>(La/a/a/a/d/h;)V

    return-object v0
.end method

.method public c()La/a/a/a/d/bk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bk",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p0}, La/a/a/a/d/h;->j()La/a/a/a/d/bk;

    move-result-object v0

    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, La/a/a/a/d/h;->a()La/a/a/a/d/bk;

    move-result-object v0

    invoke-interface {v0, p1}, La/a/a/a/d/bk;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, La/a/a/a/d/h;->b()La/a/a/a/d/be;

    move-result-object v0

    invoke-interface {v0, p1}, La/a/a/a/d/be;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, La/a/a/a/d/h;->c()La/a/a/a/d/bk;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 205
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 206
    :cond_1
    instance-of v1, p1, Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 207
    check-cast p1, Ljava/util/Map;

    .line 208
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p0}, La/a/a/a/d/h;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 209
    invoke-virtual {p0}, La/a/a/a/d/h;->c()La/a/a/a/d/bk;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, La/a/a/a/d/bk;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 199
    const/4 v1, 0x0

    invoke-virtual {p0}, La/a/a/a/d/h;->size()I

    move-result v0

    .line 200
    invoke-virtual {p0}, La/a/a/a/d/h;->c()La/a/a/a/d/bk;

    move-result-object v2

    invoke-interface {v2}, La/a/a/a/d/bk;->a()La/a/a/a/d/bg;

    move-result-object v3

    move v2, v1

    .line 201
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    invoke-interface {v3}, La/a/a/a/d/bg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 202
    :cond_0
    return v2
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, La/a/a/a/d/h;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, La/a/a/a/d/h;->a()La/a/a/a/d/bk;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    .line 83
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 84
    instance-of v1, p1, La/a/a/a/d/aw;

    if-eqz v1, :cond_0

    .line 86
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_1

    .line 87
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/d/ax;

    .line 88
    invoke-interface {v0}, La/a/a/a/d/ax;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, La/a/a/a/d/ax;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, La/a/a/a/d/h;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 93
    :cond_0
    :goto_1
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_1

    .line 94
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 95
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, La/a/a/a/d/h;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    goto :goto_1

    .line 98
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 212
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 213
    invoke-virtual {p0}, La/a/a/a/d/h;->c()La/a/a/a/d/bk;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/d/bk;->a()La/a/a/a/d/bg;

    move-result-object v4

    .line 214
    invoke-virtual {p0}, La/a/a/a/d/h;->size()I

    move-result v0

    .line 216
    const/4 v1, 0x1

    .line 217
    const-string v2, "{"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v6, v1

    move v1, v0

    move v0, v6

    .line 218
    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-eqz v1, :cond_3

    .line 219
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move v1, v0

    .line 221
    :goto_1
    invoke-interface {v4}, La/a/a/a/d/bg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/d/ax;

    .line 222
    invoke-interface {v0}, La/a/a/a/d/ax;->getKey()Ljava/lang/Object;

    move-result-object v5

    if-ne p0, v5, :cond_1

    const-string v5, "(this map)"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :goto_2
    const-string v5, "=>"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    invoke-interface {v0}, La/a/a/a/d/ax;->getValue()Ljava/lang/Object;

    move-result-object v5

    if-ne p0, v5, :cond_2

    const-string v0, "(this map)"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    move v1, v2

    goto :goto_0

    .line 220
    :cond_0
    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    goto :goto_1

    .line 223
    :cond_1
    invoke-interface {v0}, La/a/a/a/d/ax;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 226
    :cond_2
    invoke-interface {v0}, La/a/a/a/d/ax;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    move v1, v2

    goto :goto_0

    .line 228
    :cond_3
    const-string v0, "}"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, La/a/a/a/d/h;->b()La/a/a/a/d/be;

    move-result-object v0

    return-object v0
.end method
