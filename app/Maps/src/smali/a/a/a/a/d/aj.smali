.class La/a/a/a/d/aj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field b:I

.field c:I

.field d:I

.field final synthetic e:La/a/a/a/d/ac;


# direct methods
.method constructor <init>(La/a/a/a/d/ac;)V
    .locals 2

    .prologue
    .line 418
    iput-object p1, p0, La/a/a/a/d/aj;->e:La/a/a/a/d/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420
    iget-object v0, p0, La/a/a/a/d/aj;->e:La/a/a/a/d/ac;

    iget v0, v0, La/a/a/a/d/ac;->e:I

    iput v0, p0, La/a/a/a/d/aj;->b:I

    .line 423
    const/4 v0, -0x1

    iput v0, p0, La/a/a/a/d/aj;->c:I

    .line 425
    iget-object v0, p0, La/a/a/a/d/aj;->e:La/a/a/a/d/ac;

    iget v0, v0, La/a/a/a/d/ac;->h:I

    iput v0, p0, La/a/a/a/d/aj;->d:I

    .line 427
    iget-object v0, p0, La/a/a/a/d/aj;->e:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->c:[Z

    .line 428
    iget v1, p0, La/a/a/a/d/aj;->d:I

    if-eqz v1, :cond_1

    :cond_0
    iget v1, p0, La/a/a/a/d/aj;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/d/aj;->b:I

    aget-boolean v1, v0, v1

    if-eqz v1, :cond_0

    .line 429
    :cond_1
    return-void
.end method


# virtual methods
.method public final b()I
    .locals 3

    .prologue
    .line 434
    invoke-virtual {p0}, La/a/a/a/d/aj;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 435
    :cond_0
    iget v0, p0, La/a/a/a/d/aj;->b:I

    iput v0, p0, La/a/a/a/d/aj;->c:I

    .line 437
    iget v0, p0, La/a/a/a/d/aj;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/d/aj;->d:I

    if-eqz v0, :cond_2

    .line 438
    iget-object v0, p0, La/a/a/a/d/aj;->e:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->c:[Z

    .line 439
    :cond_1
    iget v1, p0, La/a/a/a/d/aj;->b:I

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, La/a/a/a/d/aj;->e:La/a/a/a/d/ac;

    iget v2, v2, La/a/a/a/d/ac;->g:I

    and-int/2addr v1, v2

    iput v1, p0, La/a/a/a/d/aj;->b:I

    aget-boolean v1, v0, v1

    if-eqz v1, :cond_1

    .line 441
    :cond_2
    iget v0, p0, La/a/a/a/d/aj;->c:I

    return v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 431
    iget v0, p0, La/a/a/a/d/aj;->d:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 445
    iget v0, p0, La/a/a/a/d/aj;->c:I

    if-ne v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 446
    :cond_0
    iget-object v0, p0, La/a/a/a/d/aj;->e:La/a/a/a/d/ac;

    iget v1, v0, La/a/a/a/d/ac;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, La/a/a/a/d/ac;->h:I

    .line 447
    iget-object v0, p0, La/a/a/a/d/aj;->e:La/a/a/a/d/ac;

    iget v1, p0, La/a/a/a/d/aj;->c:I

    invoke-virtual {v0, v1}, La/a/a/a/d/ac;->b(I)I

    move-result v0

    iget v1, p0, La/a/a/a/d/aj;->b:I

    if-ne v0, v1, :cond_1

    iget v0, p0, La/a/a/a/d/aj;->d:I

    if-lez v0, :cond_1

    .line 448
    iget v0, p0, La/a/a/a/d/aj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/d/aj;->d:I

    .line 449
    invoke-virtual {p0}, La/a/a/a/d/aj;->b()I

    .line 451
    :cond_1
    iput v2, p0, La/a/a/a/d/aj;->c:I

    .line 453
    return-void
.end method
