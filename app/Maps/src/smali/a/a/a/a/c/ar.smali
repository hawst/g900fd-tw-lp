.class final La/a/a/a/c/ar;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements La/a/a/a/c/aw;
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/a/a/c/aw",
        "<TV;>;",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Long;",
        "TV;>;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic b:La/a/a/a/c/al;


# direct methods
.method constructor <init>(La/a/a/a/c/al;I)V
    .locals 0

    .prologue
    .line 615
    iput-object p1, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 616
    iput p2, p0, La/a/a/a/c/ar;->a:I

    .line 617
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 622
    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->a:[J

    iget v1, p0, La/a/a/a/c/ar;->a:I

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 634
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 636
    :goto_0
    return v0

    .line 635
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 636
    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->a:[J

    iget v2, p0, La/a/a/a/c/ar;->a:I

    aget-wide v2, v0, v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/c/ar;->a:I

    aget-object v0, v0, v2

    if-nez v0, :cond_2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/c/ar;->a:I

    aget-object v0, v0, v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic getKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 612
    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->a:[J

    iget v1, p0, La/a/a/a/c/ar;->a:I

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 625
    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/c/ar;->a:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 639
    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->a:[J

    iget v1, p0, La/a/a/a/c/ar;->a:I

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, La/a/a/a/c;->b(J)I

    move-result v1

    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/c/ar;->a:I

    aget-object v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/c/ar;->a:I

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 628
    iget-object v0, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/c/ar;->a:I

    aget-object v0, v0, v1

    .line 629
    iget-object v1, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v1, v1, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/c/ar;->a:I

    aput-object p1, v1, v2

    .line 630
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 642
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v1, v1, La/a/a/a/c/al;->a:[J

    iget v2, p0, La/a/a/a/c/ar;->a:I

    aget-wide v2, v1, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, La/a/a/a/c/ar;->b:La/a/a/a/c/al;

    iget-object v1, v1, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/c/ar;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
