.class final La/a/a/a/c/ag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements La/a/a/a/c/z;
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/a/a/c/z;",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic b:La/a/a/a/c/ab;


# direct methods
.method constructor <init>(La/a/a/a/c/ab;I)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    iput p2, p0, La/a/a/a/c/ag;->a:I

    .line 396
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->a:[J

    iget v1, p0, La/a/a/a/c/ag;->a:I

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->b:[I

    iget v1, p0, La/a/a/a/c/ag;->a:I

    aget v0, v0, v1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 419
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 421
    :goto_0
    return v0

    .line 420
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 421
    iget-object v0, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->a:[J

    iget v2, p0, La/a/a/a/c/ag;->a:I

    aget-wide v2, v0, v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    iget-object v0, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->b:[I

    iget v2, p0, La/a/a/a/c/ag;->a:I

    aget v2, v0, v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v2, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic getKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->a:[J

    iget v1, p0, La/a/a/a/c/ag;->a:I

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getValue()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->b:[I

    iget v1, p0, La/a/a/a/c/ag;->a:I

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 424
    iget-object v0, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v0, v0, La/a/a/a/c/ab;->a:[J

    iget v1, p0, La/a/a/a/c/ag;->a:I

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, La/a/a/a/c;->b(J)I

    move-result v0

    iget-object v1, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v1, v1, La/a/a/a/c/ab;->b:[I

    iget v2, p0, La/a/a/a/c/ag;->a:I

    aget v1, v1, v2

    xor-int/2addr v0, v1

    return v0
.end method

.method public final synthetic setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 391
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v1, v1, La/a/a/a/c/ab;->b:[I

    iget v2, p0, La/a/a/a/c/ag;->a:I

    aget v1, v1, v2

    iget-object v2, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v2, v2, La/a/a/a/c/ab;->b:[I

    iget v3, p0, La/a/a/a/c/ag;->a:I

    aput v0, v2, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 427
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v1, v1, La/a/a/a/c/ab;->a:[J

    iget v2, p0, La/a/a/a/c/ag;->a:I

    aget-wide v2, v1, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, La/a/a/a/c/ag;->b:La/a/a/a/c/ab;

    iget-object v1, v1, La/a/a/a/c/ab;->b:[I

    iget v2, p0, La/a/a/a/c/ag;->a:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
