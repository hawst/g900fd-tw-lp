.class public final La/a/a/a/b/z;
.super La/a/a/a/d/x;
.source "PG"

# interfaces
.implements La/a/a/a/b/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/x",
        "<",
        "La/a/a/a/b/ad",
        "<TV;>;>;",
        "La/a/a/a/b/ag",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/b/s;


# direct methods
.method public constructor <init>(La/a/a/a/b/s;)V
    .locals 0

    .prologue
    .line 909
    iput-object p1, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    invoke-direct {p0}, La/a/a/a/d/x;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/d/bg;
    .locals 2

    .prologue
    .line 909
    new-instance v0, La/a/a/a/b/u;

    iget-object v1, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    invoke-direct {v0, v1}, La/a/a/a/b/u;-><init>(La/a/a/a/b/s;)V

    return-object v0
.end method

.method public final b()La/a/a/a/d/bd;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bd",
            "<",
            "La/a/a/a/b/ad",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 911
    new-instance v0, La/a/a/a/b/u;

    iget-object v1, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    invoke-direct {v0, v1}, La/a/a/a/b/u;-><init>(La/a/a/a/b/s;)V

    return-object v0
.end method

.method public final synthetic c()La/a/a/a/d/bg;
    .locals 2

    .prologue
    .line 909
    new-instance v0, La/a/a/a/b/v;

    iget-object v1, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    invoke-direct {v0, v1}, La/a/a/a/b/v;-><init>(La/a/a/a/b/s;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 960
    iget-object v0, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    invoke-virtual {v0}, La/a/a/a/b/s;->clear()V

    .line 961
    return-void
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-",
            "La/a/a/a/b/ad",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 913
    const/4 v0, 0x0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 927
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 937
    :goto_0
    return v0

    .line 928
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 929
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 931
    invoke-static {v2}, La/a/a/a/c;->a(I)I

    move-result v0

    iget-object v3, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v3, v3, La/a/a/a/b/s;->g:I

    and-int/2addr v0, v3

    .line 933
    :goto_1
    iget-object v3, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget-object v3, v3, La/a/a/a/b/s;->c:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_4

    .line 934
    iget-object v3, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget-object v3, v3, La/a/a/a/b/s;->a:[I

    aget v3, v3, v0

    if-ne v3, v2, :cond_3

    iget-object v2, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget-object v2, v2, La/a/a/a/b/s;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget-object v1, v1, La/a/a/a/b/s;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 935
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v3, v3, La/a/a/a/b/s;->g:I

    and-int/2addr v0, v3

    goto :goto_1

    :cond_4
    move v0, v1

    .line 937
    goto :goto_0
.end method

.method public final synthetic first()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 909
    iget-object v0, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, La/a/a/a/b/y;

    iget-object v1, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget-object v2, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v2, v2, La/a/a/a/b/s;->l:I

    invoke-direct {v0, v1, v2}, La/a/a/a/b/y;-><init>(La/a/a/a/b/s;I)V

    return-object v0
.end method

.method public final synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 909
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 909
    new-instance v0, La/a/a/a/b/u;

    iget-object v1, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    invoke-direct {v0, v1}, La/a/a/a/b/u;-><init>(La/a/a/a/b/s;)V

    return-object v0
.end method

.method public final synthetic last()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 909
    iget-object v0, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, La/a/a/a/b/y;

    iget-object v1, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget-object v2, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v2, v2, La/a/a/a/b/s;->m:I

    invoke-direct {v0, v1, v2}, La/a/a/a/b/y;-><init>(La/a/a/a/b/s;I)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 941
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 954
    :goto_0
    return v0

    .line 942
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 943
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 945
    invoke-static {v2}, La/a/a/a/c;->a(I)I

    move-result v0

    iget-object v3, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v3, v3, La/a/a/a/b/s;->g:I

    and-int/2addr v0, v3

    .line 947
    :goto_1
    iget-object v3, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget-object v3, v3, La/a/a/a/b/s;->c:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_2

    .line 948
    iget-object v3, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget-object v3, v3, La/a/a/a/b/s;->a:[I

    aget v3, v3, v0

    if-ne v3, v2, :cond_1

    .line 949
    iget-object v0, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/a/a/b/s;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 950
    const/4 v0, 0x1

    goto :goto_0

    .line 952
    :cond_1
    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v3, v3, La/a/a/a/b/s;->g:I

    and-int/2addr v0, v3

    goto :goto_1

    :cond_2
    move v0, v1

    .line 954
    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, La/a/a/a/b/z;->a:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->h:I

    return v0
.end method

.method public final synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 909
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 909
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
