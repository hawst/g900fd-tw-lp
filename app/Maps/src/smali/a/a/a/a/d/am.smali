.class public La/a/a/a/d/am;
.super La/a/a/a/d/n;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "La/a/a/a/d/n",
        "<TK;TV;>;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field public static final serialVersionUID:J


# instance fields
.field public transient a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TK;"
        }
    .end annotation
.end field

.field public transient b:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TV;"
        }
    .end annotation
.end field

.field public transient c:[Z

.field public final d:F

.field public transient e:I

.field public transient f:I

.field public transient g:I

.field public h:I

.field public volatile transient i:La/a/a/a/d/ba;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/ba",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public volatile transient j:La/a/a/a/d/bl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/bl",
            "<TK;>;"
        }
    .end annotation
.end field

.field public volatile transient k:La/a/a/a/d/be;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/be",
            "<TV;>;"
        }
    .end annotation
.end field

.field public transient l:I

.field public transient m:I

.field public transient n:[J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 159
    const/16 v0, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1}, La/a/a/a/d/am;-><init>(IF)V

    .line 160
    return-void
.end method

.method private constructor <init>(IF)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 136
    invoke-direct {p0}, La/a/a/a/d/n;-><init>()V

    .line 116
    iput v0, p0, La/a/a/a/d/am;->l:I

    .line 118
    iput v0, p0, La/a/a/a/d/am;->m:I

    .line 137
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Load factor must be greater than 0 and smaller than or equal to 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_1
    if-gez p1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The expected number of elements must be nonnegative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_2
    iput p2, p0, La/a/a/a/d/am;->d:F

    .line 140
    invoke-static {p1, p2}, La/a/a/a/c;->b(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/d/am;->e:I

    .line 141
    iget v0, p0, La/a/a/a/d/am;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/d/am;->g:I

    .line 142
    iget v0, p0, La/a/a/a/d/am;->e:I

    invoke-static {v0, p2}, La/a/a/a/c;->a(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/d/am;->f:I

    .line 143
    iget v0, p0, La/a/a/a/d/am;->e:I

    new-array v0, v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    .line 144
    iget v0, p0, La/a/a/a/d/am;->e:I

    new-array v0, v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    .line 145
    iget v0, p0, La/a/a/a/d/am;->e:I

    new-array v0, v0, [Z

    iput-object v0, p0, La/a/a/a/d/am;->c:[Z

    .line 146
    iget v0, p0, La/a/a/a/d/am;->e:I

    new-array v0, v0, [J

    iput-object v0, p0, La/a/a/a/d/am;->n:[J

    .line 147
    return-void
.end method

.method private b(I)V
    .locals 24

    .prologue
    .line 1069
    move-object/from16 v0, p0

    iget v8, v0, La/a/a/a/d/am;->l:I

    const/4 v6, -0x1

    const/4 v5, -0x1

    .line 1071
    move-object/from16 v0, p0

    iget-object v9, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    .line 1072
    move-object/from16 v0, p0

    iget-object v10, v0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    .line 1073
    add-int/lit8 v11, p1, -0x1

    .line 1074
    move/from16 v0, p1

    new-array v2, v0, [Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    .line 1075
    move/from16 v0, p1

    new-array v3, v0, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    .line 1076
    move/from16 v0, p1

    new-array v12, v0, [Z

    .line 1077
    move-object/from16 v0, p0

    iget-object v13, v0, La/a/a/a/d/am;->n:[J

    .line 1078
    move/from16 v0, p1

    new-array v14, v0, [J

    .line 1079
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, La/a/a/a/d/am;->l:I

    .line 1080
    move-object/from16 v0, p0

    iget v4, v0, La/a/a/a/d/am;->h:I

    move v7, v6

    move v6, v5

    :goto_0
    add-int/lit8 v5, v4, -0x1

    if-eqz v4, :cond_3

    .line 1081
    aget-object v15, v9, v8

    .line 1082
    if-nez v15, :cond_0

    const v4, 0x87fcd5c

    :goto_1
    and-int/2addr v4, v11

    .line 1083
    :goto_2
    aget-boolean v16, v12, v4

    if-eqz v16, :cond_1

    add-int/lit8 v4, v4, 0x1

    and-int/2addr v4, v11

    goto :goto_2

    .line 1082
    :cond_0
    invoke-virtual {v15}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, La/a/a/a/c;->a(I)I

    move-result v4

    goto :goto_1

    .line 1084
    :cond_1
    const/16 v16, 0x1

    aput-boolean v16, v12, v4

    .line 1085
    aput-object v15, v2, v4

    .line 1086
    aget-object v15, v10, v8

    aput-object v15, v3, v4

    .line 1087
    const/4 v15, -0x1

    if-eq v7, v15, :cond_2

    .line 1088
    aget-wide v16, v14, v6

    aget-wide v18, v14, v6

    int-to-long v0, v4

    move-wide/from16 v20, v0

    const-wide v22, 0xffffffffL

    and-long v20, v20, v22

    xor-long v18, v18, v20

    const-wide v20, 0xffffffffL

    and-long v18, v18, v20

    xor-long v16, v16, v18

    aput-wide v16, v14, v6

    .line 1089
    aget-wide v16, v14, v4

    aget-wide v18, v14, v4

    int-to-long v6, v6

    const-wide v20, 0xffffffffL

    and-long v6, v6, v20

    const/16 v15, 0x20

    shl-long/2addr v6, v15

    xor-long v6, v6, v18

    const-wide v18, -0x100000000L

    and-long v6, v6, v18

    xor-long v6, v6, v16

    aput-wide v6, v14, v4

    .line 1098
    :goto_3
    aget-wide v6, v13, v8

    long-to-int v6, v6

    move v7, v8

    move v8, v6

    move v6, v4

    move v4, v5

    .line 1099
    goto :goto_0

    .line 1093
    :cond_2
    move-object/from16 v0, p0

    iput v4, v0, La/a/a/a/d/am;->l:I

    .line 1095
    const-wide/16 v6, -0x1

    aput-wide v6, v14, v4

    goto :goto_3

    .line 1101
    :cond_3
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/d/am;->e:I

    .line 1102
    move-object/from16 v0, p0

    iput v11, v0, La/a/a/a/d/am;->g:I

    .line 1103
    move-object/from16 v0, p0

    iget v4, v0, La/a/a/a/d/am;->e:I

    move-object/from16 v0, p0

    iget v5, v0, La/a/a/a/d/am;->d:F

    invoke-static {v4, v5}, La/a/a/a/c;->a(IF)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, La/a/a/a/d/am;->f:I

    .line 1104
    move-object/from16 v0, p0

    iput-object v2, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    .line 1105
    move-object/from16 v0, p0

    iput-object v3, v0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    .line 1106
    move-object/from16 v0, p0

    iput-object v12, v0, La/a/a/a/d/am;->c:[Z

    .line 1107
    move-object/from16 v0, p0

    iput-object v14, v0, La/a/a/a/d/am;->n:[J

    .line 1108
    move-object/from16 v0, p0

    iput v6, v0, La/a/a/a/d/am;->m:I

    .line 1109
    const/4 v2, -0x1

    if-eq v6, v2, :cond_4

    .line 1111
    aget-wide v2, v14, v6

    const-wide v4, 0xffffffffL

    or-long/2addr v2, v4

    aput-wide v2, v14, v6

    .line 1112
    :cond_4
    return-void
.end method

.method private k()La/a/a/a/d/am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/am",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1124
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/d/am;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1129
    iput-object v1, v0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    .line 1130
    iput-object v1, v0, La/a/a/a/d/am;->k:La/a/a/a/d/be;

    .line 1131
    iput-object v1, v0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    .line 1132
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    invoke-virtual {v1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    iput-object v1, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    .line 1133
    iget-object v1, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    invoke-virtual {v1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    iput-object v1, v0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    .line 1134
    iget-object v1, p0, La/a/a/a/d/am;->c:[Z

    invoke-virtual {v1}, [Z->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Z

    iput-object v1, v0, La/a/a/a/d/am;->c:[Z

    .line 1135
    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    invoke-virtual {v1}, [J->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    iput-object v1, v0, La/a/a/a/d/am;->n:[J

    .line 1136
    return-object v0

    .line 1127
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/InternalError;

    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    throw v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 18

    .prologue
    .line 1173
    invoke-virtual/range {p1 .. p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1174
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/d/am;->h:I

    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/d/am;->d:F

    invoke-static {v2, v3}, La/a/a/a/c;->b(IF)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, La/a/a/a/d/am;->e:I

    .line 1175
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/d/am;->e:I

    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/d/am;->d:F

    invoke-static {v2, v3}, La/a/a/a/c;->a(IF)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, La/a/a/a/d/am;->f:I

    .line 1176
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/d/am;->e:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, La/a/a/a/d/am;->g:I

    .line 1177
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/d/am;->e:I

    new-array v2, v2, [Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    move-object/from16 v0, p0

    iput-object v2, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    .line 1178
    move-object/from16 v0, p0

    iget v3, v0, La/a/a/a/d/am;->e:I

    new-array v3, v3, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    move-object/from16 v0, p0

    iput-object v3, v0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    .line 1179
    move-object/from16 v0, p0

    iget v4, v0, La/a/a/a/d/am;->e:I

    new-array v7, v4, [Z

    move-object/from16 v0, p0

    iput-object v7, v0, La/a/a/a/d/am;->c:[Z

    .line 1180
    move-object/from16 v0, p0

    iget v4, v0, La/a/a/a/d/am;->e:I

    new-array v8, v4, [J

    move-object/from16 v0, p0

    iput-object v8, v0, La/a/a/a/d/am;->n:[J

    .line 1181
    const/4 v5, -0x1

    .line 1182
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, La/a/a/a/d/am;->m:I

    move-object/from16 v0, p0

    iput v4, v0, La/a/a/a/d/am;->l:I

    .line 1185
    move-object/from16 v0, p0

    iget v4, v0, La/a/a/a/d/am;->h:I

    move v6, v5

    :goto_0
    add-int/lit8 v5, v4, -0x1

    if-eqz v4, :cond_3

    .line 1186
    invoke-virtual/range {p1 .. p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v9

    .line 1187
    invoke-virtual/range {p1 .. p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v10

    .line 1188
    if-nez v9, :cond_0

    const v4, 0x87fcd5c

    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, La/a/a/a/d/am;->g:I

    and-int/2addr v4, v11

    .line 1189
    :goto_2
    aget-boolean v11, v7, v4

    if-eqz v11, :cond_1

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iget v11, v0, La/a/a/a/d/am;->g:I

    and-int/2addr v4, v11

    goto :goto_2

    .line 1188
    :cond_0
    invoke-virtual {v9}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, La/a/a/a/c;->a(I)I

    move-result v4

    goto :goto_1

    .line 1190
    :cond_1
    const/4 v11, 0x1

    aput-boolean v11, v7, v4

    .line 1191
    aput-object v9, v2, v4

    .line 1192
    aput-object v10, v3, v4

    .line 1193
    move-object/from16 v0, p0

    iget v9, v0, La/a/a/a/d/am;->l:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_2

    .line 1194
    aget-wide v10, v8, v6

    aget-wide v12, v8, v6

    int-to-long v14, v4

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    xor-long/2addr v12, v14

    const-wide v14, 0xffffffffL

    and-long/2addr v12, v14

    xor-long/2addr v10, v12

    aput-wide v10, v8, v6

    .line 1195
    aget-wide v10, v8, v4

    aget-wide v12, v8, v4

    int-to-long v14, v6

    const-wide v16, 0xffffffffL

    and-long v14, v14, v16

    const/16 v6, 0x20

    shl-long/2addr v14, v6

    xor-long/2addr v12, v14

    const-wide v14, -0x100000000L

    and-long/2addr v12, v14

    xor-long/2addr v10, v12

    aput-wide v10, v8, v4

    move v6, v4

    move v4, v5

    .line 1196
    goto :goto_0

    .line 1199
    :cond_2
    move-object/from16 v0, p0

    iput v4, v0, La/a/a/a/d/am;->l:I

    .line 1201
    aget-wide v10, v8, v4

    const-wide v12, -0x100000000L

    or-long/2addr v10, v12

    aput-wide v10, v8, v4

    move v6, v4

    move v4, v5

    goto :goto_0

    .line 1204
    :cond_3
    move-object/from16 v0, p0

    iput v6, v0, La/a/a/a/d/am;->m:I

    .line 1205
    const/4 v2, -0x1

    if-eq v6, v2, :cond_4

    .line 1207
    aget-wide v2, v8, v6

    const-wide v4, 0xffffffffL

    or-long/2addr v2, v4

    aput-wide v2, v8, v6

    .line 1209
    :cond_4
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 6

    .prologue
    .line 1161
    iget-object v2, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    .line 1162
    iget-object v3, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    .line 1163
    new-instance v4, La/a/a/a/d/au;

    invoke-direct {v4, p0}, La/a/a/a/d/au;-><init>(La/a/a/a/d/am;)V

    .line 1164
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 1165
    iget v0, p0, La/a/a/a/d/am;->h:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    .line 1166
    invoke-virtual {v4}, La/a/a/a/d/au;->a()I

    move-result v0

    .line 1167
    aget-object v5, v2, v0

    invoke-virtual {p1, v5}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 1168
    aget-object v0, v3, v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    .line 1170
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/d/bk;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/ar;

    invoke-direct {v0, p0}, La/a/a/a/d/ar;-><init>(La/a/a/a/d/am;)V

    iput-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    const-wide v10, 0xffffffffL

    .line 450
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    .line 451
    iget-object v2, p0, La/a/a/a/d/am;->c:[Z

    .line 452
    iget v3, p0, La/a/a/a/d/am;->g:I

    .line 454
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    and-int/2addr v0, v3

    .line 456
    :goto_1
    aget-boolean v4, v2, v0

    if-eqz v4, :cond_4

    .line 457
    if-nez p1, :cond_2

    aget-object v4, v1, v0

    if-nez v4, :cond_3

    .line 458
    :cond_0
    iget-object v1, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 459
    iget-object v2, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aput-object p2, v2, v0

    .line 460
    invoke-virtual {p0, v0}, La/a/a/a/d/am;->a(I)V

    move-object v0, v1

    .line 480
    :goto_2
    return-object v0

    .line 454
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 457
    :cond_2
    aget-object v4, v1, v0

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 463
    :cond_3
    add-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v3

    goto :goto_1

    .line 465
    :cond_4
    const/4 v3, 0x1

    aput-boolean v3, v2, v0

    .line 466
    aput-object p1, v1, v0

    .line 467
    iget-object v1, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aput-object p2, v1, v0

    .line 468
    iget v1, p0, La/a/a/a/d/am;->h:I

    if-nez v1, :cond_6

    .line 469
    iput v0, p0, La/a/a/a/d/am;->m:I

    iput v0, p0, La/a/a/a/d/am;->l:I

    .line 471
    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    .line 478
    :goto_3
    iget v0, p0, La/a/a/a/d/am;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/d/am;->h:I

    iget v1, p0, La/a/a/a/d/am;->f:I

    if-lt v0, v1, :cond_5

    iget v0, p0, La/a/a/a/d/am;->h:I

    iget v1, p0, La/a/a/a/d/am;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/d/am;->b(I)V

    .line 480
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 474
    :cond_6
    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    iget v2, p0, La/a/a/a/d/am;->m:I

    aget-wide v4, v1, v2

    iget-object v3, p0, La/a/a/a/d/am;->n:[J

    iget v6, p0, La/a/a/a/d/am;->m:I

    aget-wide v6, v3, v6

    int-to-long v8, v0

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    and-long/2addr v6, v10

    xor-long/2addr v4, v6

    aput-wide v4, v1, v2

    .line 475
    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    iget v2, p0, La/a/a/a/d/am;->m:I

    int-to-long v2, v2

    and-long/2addr v2, v10

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v2, v10

    aput-wide v2, v1, v0

    .line 476
    iput v0, p0, La/a/a/a/d/am;->m:I

    goto :goto_3
.end method

.method public a(I)V
    .locals 12

    .prologue
    .line 345
    iget v0, p0, La/a/a/a/d/am;->h:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, La/a/a/a/d/am;->m:I

    if-ne v0, p1, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    iget v0, p0, La/a/a/a/d/am;->l:I

    if-ne v0, p1, :cond_2

    .line 347
    iget-object v0, p0, La/a/a/a/d/am;->n:[J

    aget-wide v0, v0, p1

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/d/am;->l:I

    .line 349
    iget-object v0, p0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/am;->l:I

    aget-wide v2, v0, v1

    const-wide v4, -0x100000000L

    or-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 358
    :goto_1
    iget-object v0, p0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/am;->m:I

    aget-wide v2, v0, v1

    iget-object v4, p0, La/a/a/a/d/am;->n:[J

    iget v5, p0, La/a/a/a/d/am;->m:I

    aget-wide v4, v4, v5

    int-to-long v6, p1

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    xor-long/2addr v4, v6

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    xor-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 359
    iget-object v0, p0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/am;->m:I

    int-to-long v2, v1

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    const/16 v1, 0x20

    shl-long/2addr v2, v1

    const-wide v4, 0xffffffffL

    or-long/2addr v2, v4

    aput-wide v2, v0, p1

    .line 360
    iput p1, p0, La/a/a/a/d/am;->m:I

    goto :goto_0

    .line 352
    :cond_2
    iget-object v0, p0, La/a/a/a/d/am;->n:[J

    aget-wide v0, v0, p1

    .line 353
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    long-to-int v2, v2

    .line 354
    long-to-int v3, v0

    .line 355
    iget-object v4, p0, La/a/a/a/d/am;->n:[J

    aget-wide v6, v4, v2

    iget-object v5, p0, La/a/a/a/d/am;->n:[J

    aget-wide v8, v5, v2

    const-wide v10, 0xffffffffL

    and-long/2addr v10, v0

    xor-long/2addr v8, v10

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    aput-wide v6, v4, v2

    .line 356
    iget-object v2, p0, La/a/a/a/d/am;->n:[J

    aget-wide v4, v2, v3

    iget-object v6, p0, La/a/a/a/d/am;->n:[J

    aget-wide v6, v6, v3

    const-wide v8, -0x100000000L

    and-long/2addr v0, v8

    xor-long/2addr v0, v6

    const-wide v6, -0x100000000L

    and-long/2addr v0, v6

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    goto :goto_1
.end method

.method protected final a(II)V
    .locals 16

    .prologue
    .line 625
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/d/am;->h:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 626
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/d/am;->m:I

    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/d/am;->l:I

    .line 628
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/d/am;->n:[J

    const-wide/16 v4, -0x1

    aput-wide v4, v2, p2

    .line 649
    :goto_0
    return-void

    .line 631
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/d/am;->l:I

    move/from16 v0, p1

    if-ne v2, v0, :cond_1

    .line 632
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/d/am;->l:I

    .line 633
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/d/am;->n:[J

    move-object/from16 v0, p0

    iget-object v3, v0, La/a/a/a/d/am;->n:[J

    aget-wide v4, v3, p1

    long-to-int v3, v4

    aget-wide v4, v2, v3

    move-object/from16 v0, p0

    iget-object v6, v0, La/a/a/a/d/am;->n:[J

    move-object/from16 v0, p0

    iget-object v7, v0, La/a/a/a/d/am;->n:[J

    aget-wide v8, v7, p1

    long-to-int v7, v8

    aget-wide v6, v6, v7

    move/from16 v0, p2

    int-to-long v8, v0

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    const/16 v10, 0x20

    shl-long/2addr v8, v10

    xor-long/2addr v6, v8

    const-wide v8, -0x100000000L

    and-long/2addr v6, v8

    xor-long/2addr v4, v6

    aput-wide v4, v2, v3

    .line 634
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/d/am;->n:[J

    move-object/from16 v0, p0

    iget-object v3, v0, La/a/a/a/d/am;->n:[J

    aget-wide v4, v3, p1

    aput-wide v4, v2, p2

    goto :goto_0

    .line 637
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, La/a/a/a/d/am;->m:I

    move/from16 v0, p1

    if-ne v2, v0, :cond_2

    .line 638
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, La/a/a/a/d/am;->m:I

    .line 639
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/d/am;->n:[J

    move-object/from16 v0, p0

    iget-object v3, v0, La/a/a/a/d/am;->n:[J

    aget-wide v4, v3, p1

    const/16 v3, 0x20

    ushr-long/2addr v4, v3

    long-to-int v3, v4

    aget-wide v4, v2, v3

    move-object/from16 v0, p0

    iget-object v6, v0, La/a/a/a/d/am;->n:[J

    move-object/from16 v0, p0

    iget-object v7, v0, La/a/a/a/d/am;->n:[J

    aget-wide v8, v7, p1

    const/16 v7, 0x20

    ushr-long/2addr v8, v7

    long-to-int v7, v8

    aget-wide v6, v6, v7

    move/from16 v0, p2

    int-to-long v8, v0

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    xor-long/2addr v4, v6

    aput-wide v4, v2, v3

    .line 640
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/d/am;->n:[J

    move-object/from16 v0, p0

    iget-object v3, v0, La/a/a/a/d/am;->n:[J

    aget-wide v4, v3, p1

    aput-wide v4, v2, p2

    goto/16 :goto_0

    .line 643
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, La/a/a/a/d/am;->n:[J

    aget-wide v2, v2, p1

    .line 644
    const/16 v4, 0x20

    ushr-long v4, v2, v4

    long-to-int v4, v4

    .line 645
    long-to-int v5, v2

    .line 646
    move-object/from16 v0, p0

    iget-object v6, v0, La/a/a/a/d/am;->n:[J

    aget-wide v8, v6, v4

    move-object/from16 v0, p0

    iget-object v7, v0, La/a/a/a/d/am;->n:[J

    aget-wide v10, v7, v4

    move/from16 v0, p2

    int-to-long v12, v0

    const-wide v14, 0xffffffffL

    and-long/2addr v12, v14

    xor-long/2addr v10, v12

    const-wide v12, 0xffffffffL

    and-long/2addr v10, v12

    xor-long/2addr v8, v10

    aput-wide v8, v6, v4

    .line 647
    move-object/from16 v0, p0

    iget-object v4, v0, La/a/a/a/d/am;->n:[J

    aget-wide v6, v4, v5

    move-object/from16 v0, p0

    iget-object v8, v0, La/a/a/a/d/am;->n:[J

    aget-wide v8, v8, v5

    move/from16 v0, p2

    int-to-long v10, v0

    const-wide v12, 0xffffffffL

    and-long/2addr v10, v12

    const/16 v12, 0x20

    shl-long/2addr v10, v12

    xor-long/2addr v8, v10

    const-wide v10, -0x100000000L

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    aput-wide v6, v4, v5

    .line 648
    move-object/from16 v0, p0

    iget-object v4, v0, La/a/a/a/d/am;->n:[J

    aput-wide v2, v4, p2

    goto/16 :goto_0
.end method

.method public final b()La/a/a/a/d/be;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/be",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 980
    iget-object v0, p0, La/a/a/a/d/am;->k:La/a/a/a/d/be;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/an;

    invoke-direct {v0, p0}, La/a/a/a/d/an;-><init>(La/a/a/a/d/am;)V

    iput-object v0, p0, La/a/a/a/d/am;->k:La/a/a/a/d/be;

    .line 994
    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->k:La/a/a/a/d/be;

    return-object v0
.end method

.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 517
    iget v0, p0, La/a/a/a/d/am;->h:I

    if-nez v0, :cond_0

    .line 524
    :goto_0
    return-void

    .line 518
    :cond_0
    iput v1, p0, La/a/a/a/d/am;->h:I

    .line 519
    iget-object v0, p0, La/a/a/a/d/am;->c:[Z

    invoke-static {v0, v1}, La/a/a/a/a/a;->a([ZZ)V

    .line 521
    iget-object v0, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    invoke-static {v0, v2}, La/a/a/a/d/bb;->a([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 522
    iget-object v0, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    invoke-static {v0, v2}, La/a/a/a/d/bb;->a([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 523
    const/4 v0, -0x1

    iput v0, p0, La/a/a/a/d/am;->m:I

    iput v0, p0, La/a/a/a/d/am;->l:I

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, La/a/a/a/d/am;->k()La/a/a/a/d/am;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation

    .prologue
    .line 666
    const/4 v0, 0x0

    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 496
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v1

    .line 498
    :goto_1
    iget-object v1, p0, La/a/a/a/d/am;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 499
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 502
    :goto_2
    return v0

    .line 496
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 499
    :cond_2
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 500
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 502
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 505
    iget-object v2, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    .line 506
    iget-object v3, p0, La/a/a/a/d/am;->c:[Z

    .line 507
    iget v0, p0, La/a/a/a/d/am;->e:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_3

    aget-boolean v0, v3, v1

    if-eqz v0, :cond_2

    aget-object v0, v2, v1

    if-nez v0, :cond_1

    if-nez p1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 508
    :goto_1
    return v0

    .line 507
    :cond_1
    aget-object v0, v2, v1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 508
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d()La/a/a/a/d/bl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bl",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 963
    iget-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/ar;

    invoke-direct {v0, p0}, La/a/a/a/d/ar;-><init>(La/a/a/a/d/am;)V

    iput-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    .line 964
    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    return-object v0
.end method

.method public final e()La/a/a/a/d/az;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/az",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 667
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final f()La/a/a/a/d/az;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/az",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 668
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public firstKey()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 655
    iget v0, p0, La/a/a/a/d/am;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 656
    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/d/am;->l:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final g()La/a/a/a/d/az;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/az",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 669
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 485
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v1

    .line 487
    :goto_1
    iget-object v1, p0, La/a/a/a/d/am;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 488
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    :cond_0
    iget-object v1, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0

    .line 491
    :goto_2
    return-object v0

    .line 485
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 488
    :cond_2
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 489
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 491
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1023
    iget v1, p0, La/a/a/a/d/am;->h:I

    iget v2, p0, La/a/a/a/d/am;->d:F

    invoke-static {v1, v2}, La/a/a/a/c;->b(IF)I

    move-result v1

    .line 1024
    iget v2, p0, La/a/a/a/d/am;->e:I

    if-lt v1, v2, :cond_0

    .line 1029
    :goto_0
    return v0

    .line 1026
    :cond_0
    :try_start_0
    invoke-direct {p0, v1}, La/a/a/a/d/am;->b(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1028
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1148
    .line 1149
    iget v0, p0, La/a/a/a/d/am;->h:I

    move v1, v2

    move v3, v0

    move v5, v2

    move v0, v2

    :goto_0
    add-int/lit8 v4, v3, -0x1

    if-eqz v3, :cond_5

    .line 1150
    :goto_1
    iget-object v3, p0, La/a/a/a/d/am;->c:[Z

    aget-boolean v3, v3, v0

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1151
    :cond_0
    iget-object v3, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-eq p0, v3, :cond_1

    .line 1152
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_3

    move v1, v2

    .line 1153
    :cond_1
    :goto_2
    iget-object v3, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-eq p0, v3, :cond_2

    .line 1154
    iget-object v3, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-nez v3, :cond_4

    move v3, v2

    :goto_3
    xor-int/2addr v1, v3

    .line 1155
    :cond_2
    add-int v3, v5, v1

    .line 1156
    add-int/lit8 v0, v0, 0x1

    move v5, v3

    move v3, v4

    goto :goto_0

    .line 1152
    :cond_3
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    .line 1154
    :cond_4
    iget-object v3, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_3

    .line 1158
    :cond_5
    return v5
.end method

.method public synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic i()La/a/a/a/d/bl;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/at;

    invoke-direct {v0, p0}, La/a/a/a/d/at;-><init>(La/a/a/a/d/am;)V

    iput-object v0, p0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 529
    iget v0, p0, La/a/a/a/d/am;->h:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic j()La/a/a/a/d/bk;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/at;

    invoke-direct {v0, p0}, La/a/a/a/d/at;-><init>(La/a/a/a/d/am;)V

    iput-object v0, p0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->i:La/a/a/a/d/ba;

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/ar;

    invoke-direct {v0, p0}, La/a/a/a/d/ar;-><init>(La/a/a/a/d/am;)V

    iput-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->j:La/a/a/a/d/bl;

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 663
    iget v0, p0, La/a/a/a/d/am;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 664
    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/d/am;->m:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    const-wide v10, 0xffffffffL

    .line 220
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v1

    .line 222
    :goto_1
    iget-object v1, p0, La/a/a/a/d/am;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 223
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    .line 224
    :cond_0
    iget-object v1, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v1, v1, v0

    .line 225
    iget-object v2, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aput-object p2, v2, v0

    move-object v0, v1

    .line 245
    :goto_2
    return-object v0

    .line 220
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 223
    :cond_2
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 228
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 230
    :cond_4
    iget-object v1, p0, La/a/a/a/d/am;->c:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 231
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aput-object p1, v1, v0

    .line 232
    iget-object v1, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aput-object p2, v1, v0

    .line 233
    iget v1, p0, La/a/a/a/d/am;->h:I

    if-nez v1, :cond_6

    .line 234
    iput v0, p0, La/a/a/a/d/am;->m:I

    iput v0, p0, La/a/a/a/d/am;->l:I

    .line 236
    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    .line 243
    :goto_3
    iget v0, p0, La/a/a/a/d/am;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/d/am;->h:I

    iget v1, p0, La/a/a/a/d/am;->f:I

    if-lt v0, v1, :cond_5

    iget v0, p0, La/a/a/a/d/am;->h:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/am;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/d/am;->b(I)V

    .line 245
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 239
    :cond_6
    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    iget v2, p0, La/a/a/a/d/am;->m:I

    aget-wide v4, v1, v2

    iget-object v3, p0, La/a/a/a/d/am;->n:[J

    iget v6, p0, La/a/a/a/d/am;->m:I

    aget-wide v6, v3, v6

    int-to-long v8, v0

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    and-long/2addr v6, v10

    xor-long/2addr v4, v6

    aput-wide v4, v1, v2

    .line 240
    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    iget v2, p0, La/a/a/a/d/am;->m:I

    int-to-long v2, v2

    and-long/2addr v2, v10

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v2, v10

    aput-wide v2, v1, v0

    .line 241
    iput v0, p0, La/a/a/a/d/am;->m:I

    goto :goto_3
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 276
    if-nez p1, :cond_3

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v1

    .line 278
    :goto_1
    iget-object v1, p0, La/a/a/a/d/am;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_d

    .line 279
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_4

    if-nez p1, :cond_5

    .line 280
    :cond_0
    iget v1, p0, La/a/a/a/d/am;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/d/am;->h:I

    .line 281
    iget v1, p0, La/a/a/a/d/am;->h:I

    if-nez v1, :cond_6

    const/4 v1, -0x1

    iput v1, p0, La/a/a/a/d/am;->m:I

    iput v1, p0, La/a/a/a/d/am;->l:I

    .line 282
    :cond_1
    :goto_2
    iget-object v1, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v3, v1, v0

    .line 283
    :goto_3
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v1, v2

    :goto_4
    iget-object v2, p0, La/a/a/a/d/am;->c:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_2

    iget-object v2, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v2, v2, v1

    if-nez v2, :cond_9

    const v2, 0x87fcd5c

    :goto_5
    iget v4, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v2, v4

    if-gt v0, v1, :cond_a

    if-ge v0, v2, :cond_2

    if-le v2, v1, :cond_b

    :cond_2
    iget-object v2, p0, La/a/a/a/d/am;->c:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_c

    iget-object v2, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget-object v4, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v4, v4, v1

    aput-object v4, v2, v0

    iget-object v2, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget-object v4, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v4, v4, v1

    aput-object v4, v2, v0

    invoke-virtual {p0, v1, v0}, La/a/a/a/d/am;->a(II)V

    move v0, v1

    goto :goto_3

    .line 276
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 279
    :cond_4
    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 286
    :cond_5
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 281
    :cond_6
    iget v1, p0, La/a/a/a/d/am;->l:I

    if-ne v1, v0, :cond_7

    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    aget-wide v2, v1, v0

    long-to-int v1, v2

    iput v1, p0, La/a/a/a/d/am;->l:I

    iget v1, p0, La/a/a/a/d/am;->l:I

    if-ltz v1, :cond_1

    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    iget v2, p0, La/a/a/a/d/am;->l:I

    aget-wide v4, v1, v2

    const-wide v6, -0x100000000L

    or-long/2addr v4, v6

    aput-wide v4, v1, v2

    goto :goto_2

    :cond_7
    iget v1, p0, La/a/a/a/d/am;->m:I

    if-ne v1, v0, :cond_8

    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    aget-wide v2, v1, v0

    const/16 v1, 0x20

    ushr-long/2addr v2, v1

    long-to-int v1, v2

    iput v1, p0, La/a/a/a/d/am;->m:I

    iget v1, p0, La/a/a/a/d/am;->m:I

    if-ltz v1, :cond_1

    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    iget v2, p0, La/a/a/a/d/am;->m:I

    aget-wide v4, v1, v2

    const-wide v6, 0xffffffffL

    or-long/2addr v4, v6

    aput-wide v4, v1, v2

    goto/16 :goto_2

    :cond_8
    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    aget-wide v2, v1, v0

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    long-to-int v1, v4

    long-to-int v4, v2

    iget-object v5, p0, La/a/a/a/d/am;->n:[J

    aget-wide v6, v5, v1

    iget-object v8, p0, La/a/a/a/d/am;->n:[J

    aget-wide v8, v8, v1

    const-wide v10, 0xffffffffL

    and-long/2addr v10, v2

    xor-long/2addr v8, v10

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    xor-long/2addr v6, v8

    aput-wide v6, v5, v1

    iget-object v1, p0, La/a/a/a/d/am;->n:[J

    aget-wide v6, v1, v4

    iget-object v5, p0, La/a/a/a/d/am;->n:[J

    aget-wide v8, v5, v4

    const-wide v10, -0x100000000L

    and-long/2addr v2, v10

    xor-long/2addr v2, v8

    const-wide v8, -0x100000000L

    and-long/2addr v2, v8

    xor-long/2addr v2, v6

    aput-wide v2, v1, v4

    goto/16 :goto_2

    .line 283
    :cond_9
    iget-object v2, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, La/a/a/a/c;->a(I)I

    move-result v2

    goto/16 :goto_5

    :cond_a
    if-lt v0, v2, :cond_b

    if-gt v2, v1, :cond_2

    :cond_b
    add-int/lit8 v1, v1, 0x1

    iget v2, p0, La/a/a/a/d/am;->g:I

    and-int/2addr v1, v2

    goto/16 :goto_4

    :cond_c
    iget-object v1, p0, La/a/a/a/d/am;->c:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    iget-object v1, p0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    iget-object v1, p0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    move-object v0, v3

    .line 288
    :goto_6
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public size()I
    .locals 1

    .prologue
    .line 526
    iget v0, p0, La/a/a/a/d/am;->h:I

    return v0
.end method

.method public synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, La/a/a/a/d/am;->k:La/a/a/a/d/be;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/an;

    invoke-direct {v0, p0}, La/a/a/a/d/an;-><init>(La/a/a/a/d/am;)V

    iput-object v0, p0, La/a/a/a/d/am;->k:La/a/a/a/d/be;

    :cond_0
    iget-object v0, p0, La/a/a/a/d/am;->k:La/a/a/a/d/be;

    return-object v0
.end method
