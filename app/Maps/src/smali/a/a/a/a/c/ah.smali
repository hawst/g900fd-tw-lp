.class final La/a/a/a/c/ah;
.super La/a/a/a/d/w;
.source "PG"

# interfaces
.implements La/a/a/a/c/aa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/w",
        "<",
        "La/a/a/a/c/z;",
        ">;",
        "La/a/a/a/c/aa;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/c/ab;


# direct methods
.method constructor <init>(La/a/a/a/c/ab;)V
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    invoke-direct {p0}, La/a/a/a/d/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()La/a/a/a/d/bg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bg",
            "<",
            "La/a/a/a/c/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 495
    new-instance v0, La/a/a/a/c/ad;

    iget-object v1, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    invoke-direct {v0, v1}, La/a/a/a/c/ad;-><init>(La/a/a/a/c/ab;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    invoke-virtual {v0}, La/a/a/a/c/ab;->clear()V

    .line 536
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 502
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 512
    :goto_0
    return v0

    .line 503
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 504
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 506
    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v4

    long-to-int v0, v4

    iget-object v4, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget v4, v4, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v4

    .line 508
    :goto_1
    iget-object v4, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget-object v4, v4, La/a/a/a/c/ab;->c:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_3

    .line 509
    iget-object v4, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget-object v4, v4, La/a/a/a/c/ab;->a:[J

    aget-wide v4, v4, v0

    cmp-long v4, v4, v2

    if-nez v4, :cond_2

    iget-object v2, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget-object v2, v2, La/a/a/a/c/ab;->b:[I

    aget v2, v2, v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v2, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 510
    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget v4, v4, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v4

    goto :goto_1

    :cond_3
    move v0, v1

    .line 512
    goto :goto_0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 493
    new-instance v0, La/a/a/a/c/ad;

    iget-object v1, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    invoke-direct {v0, v1}, La/a/a/a/c/ad;-><init>(La/a/a/a/c/ab;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 516
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 529
    :goto_0
    return v0

    .line 517
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 518
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 520
    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v4

    long-to-int v0, v4

    iget-object v4, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget v4, v4, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v4

    .line 522
    :goto_1
    iget-object v4, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget-object v4, v4, La/a/a/a/c/ab;->c:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_2

    .line 523
    iget-object v4, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget-object v4, v4, La/a/a/a/c/ab;->a:[J

    aget-wide v4, v4, v0

    cmp-long v4, v4, v2

    if-nez v4, :cond_1

    .line 524
    iget-object v0, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/a/a/c/ab;->a(Ljava/lang/Object;)Ljava/lang/Integer;

    .line 525
    const/4 v0, 0x1

    goto :goto_0

    .line 527
    :cond_1
    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget v4, v4, La/a/a/a/c/ab;->g:I

    and-int/2addr v0, v4

    goto :goto_1

    :cond_2
    move v0, v1

    .line 529
    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, La/a/a/a/c/ah;->a:La/a/a/a/c/ab;

    iget v0, v0, La/a/a/a/c/ab;->h:I

    return v0
.end method
