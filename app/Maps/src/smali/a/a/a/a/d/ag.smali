.class final La/a/a/a/d/ag;
.super La/a/a/a/d/w;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/w",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/d/ac;


# direct methods
.method constructor <init>(La/a/a/a/d/ac;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    invoke-direct {p0}, La/a/a/a/d/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()La/a/a/a/d/bg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bg",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 541
    new-instance v0, La/a/a/a/d/af;

    iget-object v1, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    invoke-direct {v0, v1}, La/a/a/a/d/af;-><init>(La/a/a/a/d/ac;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    invoke-virtual {v0}, La/a/a/a/d/ac;->clear()V

    .line 556
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    invoke-virtual {v0, p1}, La/a/a/a/d/ac;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 539
    new-instance v0, La/a/a/a/d/af;

    iget-object v1, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    invoke-direct {v0, v1}, La/a/a/a/d/af;-><init>(La/a/a/a/d/ac;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    iget v0, v0, La/a/a/a/d/ac;->h:I

    .line 551
    iget-object v1, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    invoke-virtual {v1, p1}, La/a/a/a/d/ac;->b(Ljava/lang/Object;)Ljava/lang/Integer;

    .line 552
    iget-object v1, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    iget v1, v1, La/a/a/a/d/ac;->h:I

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, La/a/a/a/d/ag;->a:La/a/a/a/d/ac;

    iget v0, v0, La/a/a/a/d/ac;->h:I

    return v0
.end method
