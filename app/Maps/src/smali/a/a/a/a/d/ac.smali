.class public La/a/a/a/d/ac;
.super La/a/a/a/d/b;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "La/a/a/a/d/b",
        "<TK;>;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field public static final serialVersionUID:J


# instance fields
.field public transient a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TK;"
        }
    .end annotation
.end field

.field public transient b:[I

.field public transient c:[Z

.field public final d:F

.field public transient e:I

.field public transient f:I

.field public transient g:I

.field public h:I

.field public volatile transient i:La/a/a/a/d/ab;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/ab",
            "<TK;>;"
        }
    .end annotation
.end field

.field public volatile transient j:La/a/a/a/d/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/bk",
            "<TK;>;"
        }
    .end annotation
.end field

.field public volatile transient k:La/a/a/a/b/ai;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 127
    const/16 v0, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1}, La/a/a/a/d/ac;-><init>(IF)V

    .line 128
    return-void
.end method

.method private constructor <init>(IF)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, La/a/a/a/d/b;-><init>()V

    .line 106
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Load factor must be greater than 0 and smaller than or equal to 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_1
    if-gez p1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The expected number of elements must be nonnegative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_2
    iput p2, p0, La/a/a/a/d/ac;->d:F

    .line 109
    invoke-static {p1, p2}, La/a/a/a/c;->b(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/d/ac;->e:I

    .line 110
    iget v0, p0, La/a/a/a/d/ac;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/d/ac;->g:I

    .line 111
    iget v0, p0, La/a/a/a/d/ac;->e:I

    invoke-static {v0, p2}, La/a/a/a/c;->a(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/d/ac;->f:I

    .line 112
    iget v0, p0, La/a/a/a/d/ac;->e:I

    new-array v0, v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    .line 113
    iget v0, p0, La/a/a/a/d/ac;->e:I

    new-array v0, v0, [I

    iput-object v0, p0, La/a/a/a/d/ac;->b:[I

    .line 114
    iget v0, p0, La/a/a/a/d/ac;->e:I

    new-array v0, v0, [Z

    iput-object v0, p0, La/a/a/a/d/ac;->c:[Z

    .line 115
    return-void
.end method

.method private c(I)V
    .locals 13

    .prologue
    .line 663
    const/4 v2, 0x0

    .line 664
    iget-object v4, p0, La/a/a/a/d/ac;->c:[Z

    .line 666
    iget-object v5, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    .line 667
    iget-object v6, p0, La/a/a/a/d/ac;->b:[I

    .line 668
    add-int/lit8 v7, p1, -0x1

    .line 669
    new-array v0, p1, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 670
    new-array v8, p1, [I

    .line 671
    new-array v9, p1, [Z

    .line 672
    iget v1, p0, La/a/a/a/d/ac;->h:I

    move v12, v1

    move v1, v2

    move v2, v12

    :goto_0
    add-int/lit8 v3, v2, -0x1

    if-eqz v2, :cond_3

    .line 673
    :goto_1
    aget-boolean v2, v4, v1

    if-nez v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 674
    :cond_0
    aget-object v10, v5, v1

    .line 675
    if-nez v10, :cond_1

    const v2, 0x87fcd5c

    :goto_2
    and-int/2addr v2, v7

    .line 676
    :goto_3
    aget-boolean v11, v9, v2

    if-eqz v11, :cond_2

    add-int/lit8 v2, v2, 0x1

    and-int/2addr v2, v7

    goto :goto_3

    .line 675
    :cond_1
    invoke-virtual {v10}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, La/a/a/a/c;->a(I)I

    move-result v2

    goto :goto_2

    .line 677
    :cond_2
    const/4 v11, 0x1

    aput-boolean v11, v9, v2

    .line 678
    aput-object v10, v0, v2

    .line 679
    aget v10, v6, v1

    aput v10, v8, v2

    .line 680
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_0

    .line 682
    :cond_3
    iput p1, p0, La/a/a/a/d/ac;->e:I

    .line 683
    iput v7, p0, La/a/a/a/d/ac;->g:I

    .line 684
    iget v1, p0, La/a/a/a/d/ac;->e:I

    iget v2, p0, La/a/a/a/d/ac;->d:F

    invoke-static {v1, v2}, La/a/a/a/c;->a(IF)I

    move-result v1

    iput v1, p0, La/a/a/a/d/ac;->f:I

    .line 685
    iput-object v0, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    .line 686
    iput-object v8, p0, La/a/a/a/d/ac;->b:[I

    .line 687
    iput-object v9, p0, La/a/a/a/d/ac;->c:[Z

    .line 688
    return-void
.end method

.method private e()La/a/a/a/d/ac;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/ac",
            "<TK;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 700
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/d/ac;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 705
    iput-object v1, v0, La/a/a/a/d/ac;->j:La/a/a/a/d/bk;

    .line 706
    iput-object v1, v0, La/a/a/a/d/ac;->k:La/a/a/a/b/ai;

    .line 707
    iput-object v1, v0, La/a/a/a/d/ac;->i:La/a/a/a/d/ab;

    .line 708
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    invoke-virtual {v1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    iput-object v1, v0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    .line 709
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, La/a/a/a/d/ac;->b:[I

    .line 710
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    invoke-virtual {v1}, [Z->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Z

    iput-object v1, v0, La/a/a/a/d/ac;->c:[Z

    .line 711
    return-object v0

    .line 703
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/InternalError;

    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    throw v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 8

    .prologue
    .line 747
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 748
    iget v0, p0, La/a/a/a/d/ac;->h:I

    iget v1, p0, La/a/a/a/d/ac;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/d/ac;->e:I

    .line 749
    iget v0, p0, La/a/a/a/d/ac;->e:I

    iget v1, p0, La/a/a/a/d/ac;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->a(IF)I

    move-result v0

    iput v0, p0, La/a/a/a/d/ac;->f:I

    .line 750
    iget v0, p0, La/a/a/a/d/ac;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/d/ac;->g:I

    .line 751
    iget v0, p0, La/a/a/a/d/ac;->e:I

    new-array v0, v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    .line 752
    iget v1, p0, La/a/a/a/d/ac;->e:I

    new-array v3, v1, [I

    iput-object v3, p0, La/a/a/a/d/ac;->b:[I

    .line 753
    iget v1, p0, La/a/a/a/d/ac;->e:I

    new-array v4, v1, [Z

    iput-object v4, p0, La/a/a/a/d/ac;->c:[Z

    .line 756
    iget v1, p0, La/a/a/a/d/ac;->h:I

    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-eqz v1, :cond_2

    .line 757
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v5

    .line 758
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v6

    .line 759
    if-nez v5, :cond_0

    const v1, 0x87fcd5c

    :goto_1
    iget v7, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v1, v7

    .line 760
    :goto_2
    aget-boolean v7, v4, v1

    if-eqz v7, :cond_1

    add-int/lit8 v1, v1, 0x1

    iget v7, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v1, v7

    goto :goto_2

    .line 759
    :cond_0
    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, La/a/a/a/c;->a(I)I

    move-result v1

    goto :goto_1

    .line 761
    :cond_1
    const/4 v7, 0x1

    aput-boolean v7, v4, v1

    .line 762
    aput-object v5, v0, v1

    .line 763
    aput v6, v3, v1

    move v1, v2

    goto :goto_0

    .line 766
    :cond_2
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 6

    .prologue
    .line 735
    iget-object v2, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    .line 736
    iget-object v3, p0, La/a/a/a/d/ac;->b:[I

    .line 737
    new-instance v4, La/a/a/a/d/aj;

    invoke-direct {v4, p0}, La/a/a/a/d/aj;-><init>(La/a/a/a/d/ac;)V

    .line 738
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 739
    iget v0, p0, La/a/a/a/d/ac;->h:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    .line 740
    invoke-virtual {v4}, La/a/a/a/d/aj;->b()I

    move-result v0

    .line 741
    aget-object v5, v2, v0

    invoke-virtual {p1, v5}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 742
    aget v0, v3, v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    move v0, v1

    goto :goto_0

    .line 744
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 283
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    .line 285
    :goto_1
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 286
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    .line 287
    :cond_0
    iget v1, p0, La/a/a/a/d/ac;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/d/ac;->h:I

    .line 288
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    aget v1, v1, v0

    .line 289
    invoke-virtual {p0, v0}, La/a/a/a/d/ac;->b(I)I

    move v0, v1

    .line 294
    :goto_2
    return v0

    .line 283
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 286
    :cond_2
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 292
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 294
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I)I"
        }
    .end annotation

    .prologue
    .line 188
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    .line 190
    :goto_1
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 191
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    .line 192
    :cond_0
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    aget v1, v1, v0

    .line 193
    iget-object v2, p0, La/a/a/a/d/ac;->b:[I

    aput p2, v2, v0

    move v0, v1

    .line 203
    :goto_2
    return v0

    .line 188
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 191
    :cond_2
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 198
    :cond_4
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 199
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aput-object p1, v1, v0

    .line 200
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    aput p2, v1, v0

    .line 201
    iget v0, p0, La/a/a/a/d/ac;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/d/ac;->h:I

    iget v1, p0, La/a/a/a/d/ac;->f:I

    if-lt v0, v1, :cond_5

    iget v0, p0, La/a/a/a/d/ac;->h:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/d/ac;->c(I)V

    .line 203
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()La/a/a/a/d/bk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bk",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 559
    iget-object v0, p0, La/a/a/a/d/ac;->j:La/a/a/a/d/bk;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/ag;

    invoke-direct {v0, p0}, La/a/a/a/d/ag;-><init>(La/a/a/a/d/ac;)V

    iput-object v0, p0, La/a/a/a/d/ac;->j:La/a/a/a/d/bk;

    .line 560
    :cond_0
    iget-object v0, p0, La/a/a/a/d/ac;->j:La/a/a/a/d/bk;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 206
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 209
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    .line 211
    :goto_1
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 212
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    .line 213
    :cond_0
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    aget v1, v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 214
    iget-object v3, p0, La/a/a/a/d/ac;->b:[I

    aput v2, v3, v0

    move-object v0, v1

    .line 224
    :goto_2
    return-object v0

    .line 209
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 212
    :cond_2
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 217
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 219
    :cond_4
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    const/4 v3, 0x1

    aput-boolean v3, v1, v0

    .line 220
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aput-object p1, v1, v0

    .line 221
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    aput v2, v1, v0

    .line 222
    iget v0, p0, La/a/a/a/d/ac;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/d/ac;->h:I

    iget v1, p0, La/a/a/a/d/ac;->f:I

    if-lt v0, v1, :cond_5

    iget v0, p0, La/a/a/a/d/ac;->h:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->d:F

    invoke-static {v0, v1}, La/a/a/a/c;->b(IF)I

    move-result v0

    invoke-direct {p0, v0}, La/a/a/a/d/ac;->c(I)V

    .line 224
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(I)Z
    .locals 4

    .prologue
    .line 336
    iget-object v2, p0, La/a/a/a/d/ac;->b:[I

    .line 337
    iget-object v3, p0, La/a/a/a/d/ac;->c:[Z

    .line 338
    iget v0, p0, La/a/a/a/d/ac;->e:I

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    aget-boolean v0, v3, v1

    if-eqz v0, :cond_1

    aget v0, v2, v1

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    .line 339
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected final b(I)I
    .locals 3

    .prologue
    .line 266
    :goto_0
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    .line 267
    :goto_1
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    const v1, 0x87fcd5c

    :goto_2
    iget v2, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v1, v2

    .line 269
    if-gt p1, v0, :cond_2

    if-ge p1, v1, :cond_0

    if-le v1, v0, :cond_3

    .line 272
    :cond_0
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 273
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    iget-object v2, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    aput-object v2, v1, p1

    .line 274
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    iget-object v2, p0, La/a/a/a/d/ac;->b:[I

    aget v2, v2, v0

    aput v2, v1, p1

    move p1, v0

    goto :goto_0

    .line 268
    :cond_1
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, La/a/a/a/c;->a(I)I

    move-result v1

    goto :goto_2

    .line 269
    :cond_2
    if-lt p1, v1, :cond_3

    if-gt v1, v0, :cond_0

    .line 270
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 276
    :cond_4
    iget-object v0, p0, La/a/a/a/d/ac;->c:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 277
    iget-object v0, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 278
    return p1
.end method

.method public final b()La/a/a/a/b/ai;
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, La/a/a/a/d/ac;->k:La/a/a/a/b/ai;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/ad;

    invoke-direct {v0, p0}, La/a/a/a/d/ad;-><init>(La/a/a/a/d/ac;)V

    iput-object v0, p0, La/a/a/a/d/ac;->k:La/a/a/a/b/ai;

    .line 588
    :cond_0
    iget-object v0, p0, La/a/a/a/d/ac;->k:La/a/a/a/b/ai;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 298
    .line 300
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    .line 302
    :goto_1
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 303
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    .line 304
    :cond_0
    iget v1, p0, La/a/a/a/d/ac;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/a/a/a/d/ac;->h:I

    .line 305
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    aget v1, v1, v0

    .line 306
    invoke-virtual {p0, v0}, La/a/a/a/d/ac;->b(I)I

    .line 307
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 311
    :goto_2
    return-object v0

    .line 300
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 303
    :cond_2
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 309
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 311
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final c(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 316
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    .line 318
    :goto_1
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 319
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    :cond_0
    iget-object v1, p0, La/a/a/a/d/ac;->b:[I

    aget v0, v1, v0

    .line 322
    :goto_2
    return v0

    .line 316
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 319
    :cond_2
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 320
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 322
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final synthetic c()La/a/a/a/d/bk;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, La/a/a/a/d/ac;->i:La/a/a/a/d/ab;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/ai;

    invoke-direct {v0, p0}, La/a/a/a/d/ai;-><init>(La/a/a/a/d/ac;)V

    iput-object v0, p0, La/a/a/a/d/ac;->i:La/a/a/a/d/ab;

    :cond_0
    iget-object v0, p0, La/a/a/a/d/ac;->i:La/a/a/a/d/ab;

    return-object v0
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 348
    iget v0, p0, La/a/a/a/d/ac;->h:I

    if-nez v0, :cond_0

    .line 353
    :goto_0
    return-void

    .line 349
    :cond_0
    iput v1, p0, La/a/a/a/d/ac;->h:I

    .line 350
    iget-object v0, p0, La/a/a/a/d/ac;->c:[Z

    invoke-static {v0, v1}, La/a/a/a/a/a;->a([ZZ)V

    .line 352
    iget-object v0, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, La/a/a/a/d/bb;->a([Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, La/a/a/a/d/ac;->e()La/a/a/a/d/ac;

    move-result-object v0

    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 327
    if-nez p1, :cond_1

    const v0, 0x87fcd5c

    :goto_0
    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    .line 329
    :goto_1
    iget-object v1, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_4

    .line 330
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    if-nez p1, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 333
    :goto_2
    return v0

    .line 327
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_0

    .line 330
    :cond_2
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 331
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/a/a/a/d/ac;->g:I

    and-int/2addr v0, v1

    goto :goto_1

    .line 333
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 617
    iget v1, p0, La/a/a/a/d/ac;->h:I

    iget v2, p0, La/a/a/a/d/ac;->d:F

    invoke-static {v1, v2}, La/a/a/a/c;->b(IF)I

    move-result v1

    .line 618
    iget v2, p0, La/a/a/a/d/ac;->e:I

    if-lt v1, v2, :cond_0

    .line 623
    :goto_0
    return v0

    .line 620
    :cond_0
    :try_start_0
    invoke-direct {p0, v1}, La/a/a/a/d/ac;->c(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 622
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 723
    .line 724
    iget v0, p0, La/a/a/a/d/ac;->h:I

    move v1, v2

    move v3, v0

    move v5, v2

    move v0, v2

    :goto_0
    add-int/lit8 v4, v3, -0x1

    if-eqz v3, :cond_3

    .line 725
    :goto_1
    iget-object v3, p0, La/a/a/a/d/ac;->c:[Z

    aget-boolean v3, v3, v0

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 726
    :cond_0
    iget-object v3, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-eq p0, v3, :cond_1

    .line 727
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    move v1, v2

    .line 728
    :cond_1
    :goto_2
    iget-object v3, p0, La/a/a/a/d/ac;->b:[I

    aget v3, v3, v0

    xor-int/2addr v1, v3

    .line 729
    add-int v3, v5, v1

    .line 730
    add-int/lit8 v0, v0, 0x1

    move v5, v3

    move v3, v4

    goto :goto_0

    .line 727
    :cond_2
    iget-object v1, p0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    .line 732
    :cond_3
    return v5
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 358
    iget v0, p0, La/a/a/a/d/ac;->h:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, La/a/a/a/d/ac;->j:La/a/a/a/d/bk;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/ag;

    invoke-direct {v0, p0}, La/a/a/a/d/ag;-><init>(La/a/a/a/d/ac;)V

    iput-object v0, p0, La/a/a/a/d/ac;->j:La/a/a/a/d/bk;

    :cond_0
    iget-object v0, p0, La/a/a/a/d/ac;->j:La/a/a/a/d/bk;

    return-object v0
.end method

.method public synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, La/a/a/a/d/ac;->a(Ljava/lang/Object;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0, p1}, La/a/a/a/d/ac;->b(Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 355
    iget v0, p0, La/a/a/a/d/ac;->h:I

    return v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, La/a/a/a/d/ac;->k:La/a/a/a/b/ai;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a/d/ad;

    invoke-direct {v0, p0}, La/a/a/a/d/ad;-><init>(La/a/a/a/d/ac;)V

    iput-object v0, p0, La/a/a/a/d/ac;->k:La/a/a/a/b/ai;

    :cond_0
    iget-object v0, p0, La/a/a/a/d/ac;->k:La/a/a/a/b/ai;

    return-object v0
.end method
