.class public final La/a/a/a/c/as;
.super La/a/a/a/d/x;
.source "PG"

# interfaces
.implements La/a/a/a/c/az;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/x",
        "<",
        "La/a/a/a/c/aw",
        "<TV;>;>;",
        "La/a/a/a/c/az",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/c/al;


# direct methods
.method public constructor <init>(La/a/a/a/c/al;)V
    .locals 0

    .prologue
    .line 909
    iput-object p1, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    invoke-direct {p0}, La/a/a/a/d/x;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/d/bg;
    .locals 2

    .prologue
    .line 909
    new-instance v0, La/a/a/a/c/an;

    iget-object v1, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    invoke-direct {v0, v1}, La/a/a/a/c/an;-><init>(La/a/a/a/c/al;)V

    return-object v0
.end method

.method public final b()La/a/a/a/d/bd;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bd",
            "<",
            "La/a/a/a/c/aw",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 911
    new-instance v0, La/a/a/a/c/an;

    iget-object v1, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    invoke-direct {v0, v1}, La/a/a/a/c/an;-><init>(La/a/a/a/c/al;)V

    return-object v0
.end method

.method public final synthetic c()La/a/a/a/d/bg;
    .locals 2

    .prologue
    .line 909
    new-instance v0, La/a/a/a/c/ao;

    iget-object v1, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    invoke-direct {v0, v1}, La/a/a/a/c/ao;-><init>(La/a/a/a/c/al;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 960
    iget-object v0, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    invoke-virtual {v0}, La/a/a/a/c/al;->clear()V

    .line 961
    return-void
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-",
            "La/a/a/a/c/aw",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 913
    const/4 v0, 0x0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 927
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 937
    :goto_0
    return v0

    .line 928
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 929
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 931
    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v4

    long-to-int v0, v4

    iget-object v4, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v4, v4, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v4

    .line 933
    :goto_1
    iget-object v4, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget-object v4, v4, La/a/a/a/c/al;->c:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_4

    .line 934
    iget-object v4, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget-object v4, v4, La/a/a/a/c/al;->a:[J

    aget-wide v4, v4, v0

    cmp-long v4, v4, v2

    if-nez v4, :cond_3

    iget-object v2, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget-object v2, v2, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget-object v1, v1, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 935
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v4, v4, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v4

    goto :goto_1

    :cond_4
    move v0, v1

    .line 937
    goto :goto_0
.end method

.method public final synthetic first()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 909
    iget-object v0, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v0, v0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, La/a/a/a/c/ar;

    iget-object v1, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget-object v2, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v2, v2, La/a/a/a/c/al;->l:I

    invoke-direct {v0, v1, v2}, La/a/a/a/c/ar;-><init>(La/a/a/a/c/al;I)V

    return-object v0
.end method

.method public final synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 909
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 909
    new-instance v0, La/a/a/a/c/an;

    iget-object v1, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    invoke-direct {v0, v1}, La/a/a/a/c/an;-><init>(La/a/a/a/c/al;)V

    return-object v0
.end method

.method public final synthetic last()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 909
    iget-object v0, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v0, v0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, La/a/a/a/c/ar;

    iget-object v1, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget-object v2, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v2, v2, La/a/a/a/c/al;->m:I

    invoke-direct {v0, v1, v2}, La/a/a/a/c/ar;-><init>(La/a/a/a/c/al;I)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 941
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 954
    :goto_0
    return v0

    .line 942
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 943
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 945
    invoke-static {v2, v3}, La/a/a/a/c;->a(J)J

    move-result-wide v4

    long-to-int v0, v4

    iget-object v4, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v4, v4, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v4

    .line 947
    :goto_1
    iget-object v4, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget-object v4, v4, La/a/a/a/c/al;->c:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_2

    .line 948
    iget-object v4, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget-object v4, v4, La/a/a/a/c/al;->a:[J

    aget-wide v4, v4, v0

    cmp-long v4, v4, v2

    if-nez v4, :cond_1

    .line 949
    iget-object v0, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/a/a/c/al;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 950
    const/4 v0, 0x1

    goto :goto_0

    .line 952
    :cond_1
    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v4, v4, La/a/a/a/c/al;->g:I

    and-int/2addr v0, v4

    goto :goto_1

    :cond_2
    move v0, v1

    .line 954
    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, La/a/a/a/c/as;->a:La/a/a/a/c/al;

    iget v0, v0, La/a/a/a/c/al;->h:I

    return v0
.end method

.method public final synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 909
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 909
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
