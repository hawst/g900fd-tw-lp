.class final La/a/a/a/d/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements La/a/a/a/d/ax;
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/a/a/d/ax",
        "<TK;TV;>;",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic b:La/a/a/a/d/am;


# direct methods
.method constructor <init>(La/a/a/a/d/am;I)V
    .locals 0

    .prologue
    .line 555
    iput-object p1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556
    iput p2, p0, La/a/a/a/d/as;->a:I

    .line 557
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 571
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 573
    :cond_0
    :goto_0
    return v0

    .line 572
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 573
    iget-object v1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v1, v1, v2

    if-nez v1, :cond_2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_1
    iget-object v1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v1, v1, v2

    if-nez v1, :cond_3

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v1, v1, v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_3
    iget-object v1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v1, v1, v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 559
    iget-object v0, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/d/as;->a:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 562
    iget-object v0, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/d/as;->a:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 576
    iget-object v0, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v0, v0, v2

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v2, v2, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget v3, p0, La/a/a/a/d/as;->a:I

    aget-object v2, v2, v3

    if-nez v2, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 565
    iget-object v0, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/d/as;->a:I

    aget-object v0, v0, v1

    .line 566
    iget-object v1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aput-object p1, v1, v2

    .line 567
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 579
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, La/a/a/a/d/as;->b:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/as;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
