.class La/a/a/a/d/au;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field c:I

.field d:I

.field e:I

.field f:I

.field final synthetic g:La/a/a/a/d/am;


# direct methods
.method constructor <init>(La/a/a/a/d/am;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 685
    iput-object p1, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678
    iput v0, p0, La/a/a/a/d/au;->c:I

    .line 680
    iput v0, p0, La/a/a/a/d/au;->d:I

    .line 682
    iput v0, p0, La/a/a/a/d/au;->e:I

    .line 684
    iput v0, p0, La/a/a/a/d/au;->f:I

    .line 686
    iget v0, p1, La/a/a/a/d/am;->l:I

    iput v0, p0, La/a/a/a/d/au;->d:I

    .line 687
    const/4 v0, 0x0

    iput v0, p0, La/a/a/a/d/au;->f:I

    .line 688
    return-void
.end method

.method private final c()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 713
    iget v0, p0, La/a/a/a/d/au;->f:I

    if-ltz v0, :cond_1

    .line 728
    :cond_0
    :goto_0
    return-void

    .line 714
    :cond_1
    iget v0, p0, La/a/a/a/d/au;->c:I

    if-ne v0, v1, :cond_2

    .line 715
    const/4 v0, 0x0

    iput v0, p0, La/a/a/a/d/au;->f:I

    goto :goto_0

    .line 718
    :cond_2
    iget v0, p0, La/a/a/a/d/au;->d:I

    if-ne v0, v1, :cond_3

    .line 719
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->h:I

    iput v0, p0, La/a/a/a/d/au;->f:I

    goto :goto_0

    .line 722
    :cond_3
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->l:I

    .line 723
    const/4 v1, 0x1

    iput v1, p0, La/a/a/a/d/au;->f:I

    .line 724
    :goto_1
    iget v1, p0, La/a/a/a/d/au;->c:I

    if-eq v0, v1, :cond_0

    .line 725
    iget-object v1, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->n:[J

    aget-wide v0, v1, v0

    long-to-int v0, v0

    .line 726
    iget v1, p0, La/a/a/a/d/au;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, La/a/a/a/d/au;->f:I

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 738
    invoke-virtual {p0}, La/a/a/a/d/au;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->size()I

    move-result v0

    .line 743
    :goto_0
    return v0

    .line 739
    :cond_0
    iget v0, p0, La/a/a/a/d/au;->d:I

    iput v0, p0, La/a/a/a/d/au;->e:I

    .line 740
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/au;->e:I

    aget-wide v0, v0, v1

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/d/au;->d:I

    .line 741
    iget v0, p0, La/a/a/a/d/au;->e:I

    iput v0, p0, La/a/a/a/d/au;->c:I

    .line 742
    iget v0, p0, La/a/a/a/d/au;->f:I

    if-ltz v0, :cond_1

    iget v0, p0, La/a/a/a/d/au;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/d/au;->f:I

    .line 743
    :cond_1
    iget v0, p0, La/a/a/a/d/au;->e:I

    goto :goto_0
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 746
    invoke-virtual {p0}, La/a/a/a/d/au;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 751
    :goto_0
    return v0

    .line 747
    :cond_0
    iget v0, p0, La/a/a/a/d/au;->c:I

    iput v0, p0, La/a/a/a/d/au;->e:I

    .line 748
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/au;->e:I

    aget-wide v0, v0, v1

    const/16 v2, 0x20

    ushr-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/d/au;->c:I

    .line 749
    iget v0, p0, La/a/a/a/d/au;->e:I

    iput v0, p0, La/a/a/a/d/au;->d:I

    .line 750
    iget v0, p0, La/a/a/a/d/au;->f:I

    if-ltz v0, :cond_1

    iget v0, p0, La/a/a/a/d/au;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/d/au;->f:I

    .line 751
    :cond_1
    iget v0, p0, La/a/a/a/d/au;->e:I

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 710
    iget v0, p0, La/a/a/a/d/au;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrevious()Z
    .locals 2

    .prologue
    .line 711
    iget v0, p0, La/a/a/a/d/au;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nextIndex()I
    .locals 1

    .prologue
    .line 730
    invoke-direct {p0}, La/a/a/a/d/au;->c()V

    .line 731
    iget v0, p0, La/a/a/a/d/au;->f:I

    return v0
.end method

.method public previousIndex()I
    .locals 1

    .prologue
    .line 734
    invoke-direct {p0}, La/a/a/a/d/au;->c()V

    .line 735
    iget v0, p0, La/a/a/a/d/au;->f:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/16 v9, 0x20

    const-wide v10, 0xffffffffL

    const/4 v8, -0x1

    .line 755
    invoke-direct {p0}, La/a/a/a/d/au;->c()V

    .line 756
    iget v0, p0, La/a/a/a/d/au;->e:I

    if-ne v0, v8, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 757
    :cond_0
    iget v0, p0, La/a/a/a/d/au;->e:I

    iget v1, p0, La/a/a/a/d/au;->c:I

    if-ne v0, v1, :cond_4

    .line 760
    iget v0, p0, La/a/a/a/d/au;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/d/au;->f:I

    .line 761
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/au;->e:I

    aget-wide v0, v0, v1

    ushr-long/2addr v0, v9

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/d/au;->c:I

    .line 765
    :goto_0
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget v1, v0, La/a/a/a/d/am;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, La/a/a/a/d/am;->h:I

    .line 768
    iget v0, p0, La/a/a/a/d/au;->c:I

    if-ne v0, v8, :cond_5

    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget v1, p0, La/a/a/a/d/au;->d:I

    iput v1, v0, La/a/a/a/d/am;->l:I

    .line 771
    :goto_1
    iget v0, p0, La/a/a/a/d/au;->d:I

    if-ne v0, v8, :cond_6

    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget v1, p0, La/a/a/a/d/au;->c:I

    iput v1, v0, La/a/a/a/d/am;->m:I

    .line 774
    :goto_2
    iget v0, p0, La/a/a/a/d/au;->e:I

    .line 777
    :goto_3
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget v2, v2, La/a/a/a/d/am;->g:I

    and-int/2addr v1, v2

    .line 778
    :goto_4
    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v2, v2, La/a/a/a/d/am;->c:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_1

    .line 779
    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v2, v2, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v2, v2, v1

    if-nez v2, :cond_7

    const v2, 0x87fcd5c

    :goto_5
    iget-object v3, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget v3, v3, La/a/a/a/d/am;->g:I

    and-int/2addr v2, v3

    .line 780
    if-gt v0, v1, :cond_8

    if-ge v0, v2, :cond_1

    if-le v2, v1, :cond_9

    .line 783
    :cond_1
    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v2, v2, La/a/a/a/d/am;->c:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_a

    .line 784
    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v2, v2, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget-object v3, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v3, v3, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v3, v3, v1

    aput-object v3, v2, v0

    .line 785
    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v2, v2, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    iget-object v3, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v3, v3, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v3, v3, v1

    aput-object v3, v2, v0

    .line 786
    iget v2, p0, La/a/a/a/d/au;->d:I

    if-ne v2, v1, :cond_2

    iput v0, p0, La/a/a/a/d/au;->d:I

    .line 787
    :cond_2
    iget v2, p0, La/a/a/a/d/au;->c:I

    if-ne v2, v1, :cond_3

    iput v0, p0, La/a/a/a/d/au;->c:I

    .line 788
    :cond_3
    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    invoke-virtual {v2, v1, v0}, La/a/a/a/d/am;->a(II)V

    move v0, v1

    goto :goto_3

    .line 764
    :cond_4
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/au;->e:I

    aget-wide v0, v0, v1

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/d/au;->d:I

    goto/16 :goto_0

    .line 770
    :cond_5
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/au;->c:I

    aget-wide v2, v0, v1

    iget-object v4, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v4, v4, La/a/a/a/d/am;->n:[J

    iget v5, p0, La/a/a/a/d/au;->c:I

    aget-wide v4, v4, v5

    iget v6, p0, La/a/a/a/d/au;->d:I

    int-to-long v6, v6

    and-long/2addr v6, v10

    xor-long/2addr v4, v6

    and-long/2addr v4, v10

    xor-long/2addr v2, v4

    aput-wide v2, v0, v1

    goto/16 :goto_1

    .line 773
    :cond_6
    iget-object v0, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->n:[J

    iget v1, p0, La/a/a/a/d/au;->d:I

    aget-wide v2, v0, v1

    iget-object v4, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v4, v4, La/a/a/a/d/am;->n:[J

    iget v5, p0, La/a/a/a/d/au;->d:I

    aget-wide v4, v4, v5

    iget v6, p0, La/a/a/a/d/au;->c:I

    int-to-long v6, v6

    and-long/2addr v6, v10

    shl-long/2addr v6, v9

    xor-long/2addr v4, v6

    const-wide v6, -0x100000000L

    and-long/2addr v4, v6

    xor-long/2addr v2, v4

    aput-wide v2, v0, v1

    goto/16 :goto_2

    .line 779
    :cond_7
    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v2, v2, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, La/a/a/a/c;->a(I)I

    move-result v2

    goto/16 :goto_5

    .line 780
    :cond_8
    if-lt v0, v2, :cond_9

    if-gt v2, v1, :cond_1

    .line 781
    :cond_9
    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget v2, v2, La/a/a/a/d/am;->g:I

    and-int/2addr v1, v2

    goto/16 :goto_4

    .line 790
    :cond_a
    iget-object v1, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->c:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 791
    iget-object v1, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aput-object v12, v1, v0

    .line 792
    iget-object v1, p0, La/a/a/a/d/au;->g:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aput-object v12, v1, v0

    .line 793
    iput v8, p0, La/a/a/a/d/au;->e:I

    .line 794
    return-void
.end method
