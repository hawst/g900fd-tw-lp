.class public abstract La/a/a/a/c/v;
.super La/a/a/a/c/t;
.source "PG"

# interfaces
.implements La/a/a/a/c/bg;
.implements Ljava/lang/Cloneable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, La/a/a/a/c/t;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()La/a/a/a/c/be;
.end method

.method public final d(J)Z
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0, p1, p2}, La/a/a/a/c/v;->e(J)Z

    move-result v0

    return v0
.end method

.method public e(J)Z
    .locals 1

    .prologue
    .line 75
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 52
    :cond_1
    instance-of v1, p1, Ljava/util/Set;

    if-eqz v1, :cond_0

    .line 53
    check-cast p1, Ljava/util/Set;

    .line 54
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {p0}, La/a/a/a/c/v;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 55
    invoke-virtual {p0, p1}, La/a/a/a/c/v;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 65
    const/4 v1, 0x0

    invoke-virtual {p0}, La/a/a/a/c/v;->size()I

    move-result v0

    .line 66
    invoke-virtual {p0}, La/a/a/a/c/v;->a()La/a/a/a/c/be;

    move-result-object v3

    move v2, v1

    .line 68
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    .line 69
    invoke-interface {v3}, La/a/a/a/c/be;->a()J

    move-result-wide v4

    .line 70
    invoke-static {v4, v5}, La/a/a/a/c;->b(J)I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 72
    :cond_0
    return v2
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, La/a/a/a/c/v;->a()La/a/a/a/c/be;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 87
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, La/a/a/a/c/v;->e(J)Z

    move-result v0

    return v0
.end method
