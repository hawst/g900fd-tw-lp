.class public abstract La/a/a/a/c/t;
.super Ljava/util/AbstractCollection;
.source "PG"

# interfaces
.implements La/a/a/a/c/bb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractCollection",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "La/a/a/a/c/bb;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()La/a/a/a/c/be;
.end method

.method public a(J)Z
    .locals 5

    .prologue
    .line 187
    invoke-virtual {p0}, La/a/a/a/c/t;->a()La/a/a/a/c/be;

    move-result-object v0

    .line 188
    :cond_0
    invoke-interface {v0}, La/a/a/a/c/be;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, La/a/a/a/c/be;->a()J

    move-result-wide v2

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 189
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 53
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 144
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 145
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    .line 147
    add-int/lit8 v3, v2, -0x1

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 148
    :cond_0
    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 183
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, La/a/a/a/c/t;->a(J)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 211
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    .line 213
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 214
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, La/a/a/a/c/t;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 216
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public d(J)Z
    .locals 5

    .prologue
    .line 193
    invoke-virtual {p0}, La/a/a/a/c/t;->a()La/a/a/a/c/be;

    move-result-object v0

    .line 194
    :cond_0
    invoke-interface {v0}, La/a/a/a/c/be;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    invoke-interface {v0}, La/a/a/a/c/be;->a()J

    move-result-wide v2

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 196
    invoke-interface {v0}, La/a/a/a/c/be;->remove()V

    .line 197
    const/4 v0, 0x1

    .line 199
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 259
    invoke-virtual {p0}, La/a/a/a/c/t;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/c/t;->a()La/a/a/a/c/be;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 168
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, La/a/a/a/c/t;->d(J)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 249
    const/4 v1, 0x0

    .line 250
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    .line 252
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    .line 253
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, La/a/a/a/c/t;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 255
    :cond_0
    return v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 227
    const/4 v1, 0x0

    .line 228
    invoke-virtual {p0}, La/a/a/a/c/t;->size()I

    move-result v0

    .line 230
    invoke-virtual {p0}, La/a/a/a/c/t;->a()La/a/a/a/c/be;

    move-result-object v3

    move v2, v1

    .line 231
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    .line 232
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 233
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 234
    const/4 v0, 0x1

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 238
    :cond_0
    return v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, La/a/a/a/c/t;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    .line 125
    invoke-virtual {p0}, La/a/a/a/c/t;->a()La/a/a/a/c/be;

    move-result-object v1

    invoke-static {v1, v0}, La/a/a/a/d/bh;->a(Ljava/util/Iterator;[Ljava/lang/Object;)I

    .line 126
    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 131
    array-length v0, p1

    invoke-virtual {p0}, La/a/a/a/c/t;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, La/a/a/a/c/t;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 132
    :goto_0
    invoke-virtual {p0}, La/a/a/a/c/t;->a()La/a/a/a/c/be;

    move-result-object v1

    invoke-static {v1, v0}, La/a/a/a/d/bh;->a(Ljava/util/Iterator;[Ljava/lang/Object;)I

    .line 133
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 263
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    invoke-virtual {p0}, La/a/a/a/c/t;->a()La/a/a/a/c/be;

    move-result-object v4

    .line 265
    invoke-virtual {p0}, La/a/a/a/c/t;->size()I

    move-result v1

    .line 267
    const/4 v0, 0x1

    .line 269
    const-string v2, "{"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-eqz v1, :cond_1

    .line 272
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 274
    :goto_1
    invoke-interface {v4}, La/a/a/a/c/be;->a()J

    move-result-wide v6

    .line 278
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v2

    goto :goto_0

    .line 273
    :cond_0
    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 281
    :cond_1
    const-string v0, "}"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
