.class public abstract La/a/a/a/c/n;
.super La/a/a/a/c/h;
.source "PG"

# interfaces
.implements La/a/a/a/c/ay;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "La/a/a/a/c/h",
        "<TV;>;",
        "La/a/a/a/c/ay",
        "<TV;>;"
    }
.end annotation


# static fields
.field public static final serialVersionUID:J = -0x189cf424fb7d7759L


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, La/a/a/a/c/h;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a()La/a/a/a/c/bg;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/c/n;->d()La/a/a/a/c/bh;

    move-result-object v0

    return-object v0
.end method

.method public b()La/a/a/a/d/be;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/be",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v0, La/a/a/a/c/q;

    invoke-direct {v0, p0}, La/a/a/a/c/q;-><init>(La/a/a/a/c/n;)V

    return-object v0
.end method

.method public final synthetic c()La/a/a/a/d/bk;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/c/n;->l()La/a/a/a/d/bl;

    move-result-object v0

    return-object v0
.end method

.method public d()La/a/a/a/c/bh;
    .locals 1

    .prologue
    .line 88
    new-instance v0, La/a/a/a/c/o;

    invoke-direct {v0, p0}, La/a/a/a/c/o;-><init>(La/a/a/a/c/n;)V

    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/c/n;->l()La/a/a/a/d/bl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic firstKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/c/n;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 53
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    invoke-virtual {p0}, La/a/a/a/c/n;->i()La/a/a/a/c/ay;

    move-result-object v0

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/c/n;->d()La/a/a/a/c/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lastKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/c/n;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 53
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    invoke-virtual {p0}, La/a/a/a/c/n;->j()La/a/a/a/c/ay;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 53
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    invoke-virtual {p0}, La/a/a/a/c/n;->h()La/a/a/a/c/ay;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/c/n;->b()La/a/a/a/d/be;

    move-result-object v0

    return-object v0
.end method
