.class final La/a/a/a/d/ar;
.super La/a/a/a/d/x;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/x",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/d/am;


# direct methods
.method constructor <init>(La/a/a/a/d/am;)V
    .locals 0

    .prologue
    .line 928
    iput-object p1, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    invoke-direct {p0}, La/a/a/a/d/x;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/d/bg;
    .locals 2

    .prologue
    .line 928
    new-instance v0, La/a/a/a/d/aq;

    iget-object v1, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    invoke-direct {v0, v1}, La/a/a/a/d/aq;-><init>(La/a/a/a/d/am;)V

    return-object v0
.end method

.method public final synthetic b()La/a/a/a/d/bd;
    .locals 2

    .prologue
    .line 928
    new-instance v0, La/a/a/a/d/aq;

    iget-object v1, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    invoke-direct {v0, v1}, La/a/a/a/d/aq;-><init>(La/a/a/a/d/am;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 947
    iget-object v0, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->clear()V

    .line 948
    return-void
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation

    .prologue
    .line 957
    const/4 v0, 0x0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 939
    iget-object v0, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    invoke-virtual {v0, p1}, La/a/a/a/d/am;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 950
    iget-object v0, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 951
    :cond_0
    iget-object v0, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget-object v1, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget v1, v1, La/a/a/a/d/am;->l:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 928
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 928
    new-instance v0, La/a/a/a/d/aq;

    iget-object v1, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    invoke-direct {v0, v1}, La/a/a/a/d/aq;-><init>(La/a/a/a/d/am;)V

    return-object v0
.end method

.method public final last()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 954
    iget-object v0, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 955
    :cond_0
    iget-object v0, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget-object v0, v0, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    iget-object v1, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget v1, v1, La/a/a/a/d/am;->m:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 942
    iget-object v0, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->h:I

    .line 943
    iget-object v1, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    invoke-virtual {v1, p1}, La/a/a/a/d/am;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    iget-object v1, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget v1, v1, La/a/a/a/d/am;->h:I

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, La/a/a/a/d/ar;->a:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->h:I

    return v0
.end method

.method public final synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 928
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 928
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
