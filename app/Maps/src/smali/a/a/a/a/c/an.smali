.class La/a/a/a/c/an;
.super La/a/a/a/c/at;
.source "PG"

# interfaces
.implements La/a/a/a/d/bj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/c/al",
        "<TV;>.a/a/a/a/c/at;",
        "La/a/a/a/d/bj",
        "<",
        "La/a/a/a/c/aw",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/c/al;

.field private b:La/a/a/a/c/ar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/c/al",
            "<TV;>.a/a/a/a/c/ar;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(La/a/a/a/c/al;)V
    .locals 0

    .prologue
    .line 870
    iput-object p1, p0, La/a/a/a/c/an;->a:La/a/a/a/c/al;

    invoke-direct {p0, p1}, La/a/a/a/c/at;-><init>(La/a/a/a/c/al;)V

    return-void
.end method


# virtual methods
.method public synthetic add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 868
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 868
    new-instance v0, La/a/a/a/c/ar;

    iget-object v1, p0, La/a/a/a/c/an;->a:La/a/a/a/c/al;

    invoke-virtual {p0}, La/a/a/a/c/an;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, La/a/a/a/c/ar;-><init>(La/a/a/a/c/al;I)V

    iput-object v0, p0, La/a/a/a/c/an;->b:La/a/a/a/c/ar;

    return-object v0
.end method

.method public synthetic previous()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 868
    new-instance v0, La/a/a/a/c/ar;

    iget-object v1, p0, La/a/a/a/c/an;->a:La/a/a/a/c/al;

    invoke-virtual {p0}, La/a/a/a/c/an;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, La/a/a/a/c/ar;-><init>(La/a/a/a/c/al;I)V

    iput-object v0, p0, La/a/a/a/c/an;->b:La/a/a/a/c/ar;

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 882
    invoke-super {p0}, La/a/a/a/c/at;->remove()V

    .line 883
    iget-object v0, p0, La/a/a/a/c/an;->b:La/a/a/a/c/ar;

    const/4 v1, -0x1

    iput v1, v0, La/a/a/a/c/ar;->a:I

    .line 884
    return-void
.end method

.method public synthetic set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 868
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
