.class final La/a/a/a/d/ah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements La/a/a/a/d/aa;
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/a/a/d/aa",
        "<TK;>;",
        "Ljava/util/Map$Entry",
        "<TK;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic b:La/a/a/a/d/ac;


# direct methods
.method constructor <init>(La/a/a/a/d/ac;I)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385
    iput p2, p0, La/a/a/a/d/ah;->a:I

    .line 386
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->b:[I

    iget v1, p0, La/a/a/a/d/ah;->a:I

    aget v0, v0, v1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 406
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 408
    :goto_0
    return v0

    .line 407
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 408
    iget-object v0, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/ah;->a:I

    aget-object v0, v0, v2

    if-nez v0, :cond_2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->b:[I

    iget v2, p0, La/a/a/a/d/ah;->a:I

    aget v2, v0, v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v2, v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/ah;->a:I

    aget-object v0, v0, v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/d/ah;->a:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final synthetic getValue()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->b:[I

    iget v1, p0, La/a/a/a/d/ah;->a:I

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 411
    iget-object v0, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/d/ah;->a:I

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v1, v1, La/a/a/a/d/ac;->b:[I

    iget v2, p0, La/a/a/a/d/ah;->a:I

    aget v1, v1, v2

    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v0, v0, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    iget v1, p0, La/a/a/a/d/ah;->a:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 381
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v1, v1, La/a/a/a/d/ac;->b:[I

    iget v2, p0, La/a/a/a/d/ah;->a:I

    aget v1, v1, v2

    iget-object v2, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v2, v2, La/a/a/a/d/ac;->b:[I

    iget v3, p0, La/a/a/a/d/ah;->a:I

    aput v0, v2, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 414
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v1, v1, La/a/a/a/d/ac;->a:[Ljava/lang/Object;

    iget v2, p0, La/a/a/a/d/ah;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, La/a/a/a/d/ah;->b:La/a/a/a/d/ac;

    iget-object v1, v1, La/a/a/a/d/ac;->b:[I

    iget v2, p0, La/a/a/a/d/ah;->a:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
