.class final La/a/a/a/b/x;
.super La/a/a/a/b/q;
.source "PG"


# instance fields
.field final synthetic a:La/a/a/a/b/s;


# direct methods
.method constructor <init>(La/a/a/a/b/s;)V
    .locals 0

    .prologue
    .line 994
    iput-object p1, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    invoke-direct {p0}, La/a/a/a/b/q;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/b/al;
    .locals 2

    .prologue
    .line 994
    new-instance v0, La/a/a/a/b/w;

    iget-object v1, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    invoke-direct {v0, v1}, La/a/a/a/b/w;-><init>(La/a/a/a/b/s;)V

    return-object v0
.end method

.method public final a(II)La/a/a/a/b/ao;
    .locals 1

    .prologue
    .line 1026
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1005
    iget-object v0, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    invoke-virtual {v0, p1}, La/a/a/a/b/s;->b(I)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1016
    iget-object v0, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1017
    :cond_0
    iget-object v0, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget-object v0, v0, La/a/a/a/b/s;->a:[I

    iget-object v1, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget v1, v1, La/a/a/a/b/s;->l:I

    aget v0, v0, v1

    return v0
.end method

.method public final b(I)La/a/a/a/b/ao;
    .locals 1

    .prologue
    .line 1025
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1021
    :cond_0
    iget-object v0, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget-object v0, v0, La/a/a/a/b/s;->a:[I

    iget-object v1, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget v1, v1, La/a/a/a/b/s;->m:I

    aget v0, v0, v1

    return v0
.end method

.method public final c(I)La/a/a/a/b/ao;
    .locals 1

    .prologue
    .line 1024
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 1013
    iget-object v0, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    invoke-virtual {v0}, La/a/a/a/b/s;->clear()V

    .line 1014
    return-void
.end method

.method public final bridge synthetic comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 994
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic d()La/a/a/a/b/ah;
    .locals 2

    .prologue
    .line 994
    new-instance v0, La/a/a/a/b/w;

    iget-object v1, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    invoke-direct {v0, v1}, La/a/a/a/b/w;-><init>(La/a/a/a/b/s;)V

    return-object v0
.end method

.method public final e(I)Z
    .locals 2

    .prologue
    .line 1008
    iget-object v0, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->h:I

    .line 1009
    iget-object v1, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    invoke-virtual {v1, p1}, La/a/a/a/b/s;->a(I)Ljava/lang/Object;

    .line 1010
    iget-object v1, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget v1, v1, La/a/a/a/b/s;->h:I

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 994
    new-instance v0, La/a/a/a/b/w;

    iget-object v1, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    invoke-direct {v0, v1}, La/a/a/a/b/w;-><init>(La/a/a/a/b/s;)V

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, La/a/a/a/b/x;->a:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->h:I

    return v0
.end method
