.class public abstract La/a/a/a/b/p;
.super La/a/a/a/b/n;
.source "PG"

# interfaces
.implements La/a/a/a/b/an;
.implements Ljava/lang/Cloneable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, La/a/a/a/b/n;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()La/a/a/a/b/al;
.end method

.method public final d(I)Z
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0, p1}, La/a/a/a/b/p;->e(I)Z

    move-result v0

    return v0
.end method

.method public e(I)Z
    .locals 1

    .prologue
    .line 75
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 52
    :cond_1
    instance-of v1, p1, Ljava/util/Set;

    if-eqz v1, :cond_0

    .line 53
    check-cast p1, Ljava/util/Set;

    .line 54
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {p0}, La/a/a/a/b/p;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 55
    invoke-virtual {p0, p1}, La/a/a/a/b/p;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 65
    const/4 v1, 0x0

    invoke-virtual {p0}, La/a/a/a/b/p;->size()I

    move-result v0

    .line 66
    invoke-virtual {p0}, La/a/a/a/b/p;->a()La/a/a/a/b/al;

    move-result-object v3

    move v2, v1

    .line 68
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    .line 69
    invoke-interface {v3}, La/a/a/a/b/al;->a()I

    move-result v0

    .line 70
    add-int/2addr v0, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 72
    :cond_0
    return v2
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, La/a/a/a/b/p;->a()La/a/a/a/b/al;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 87
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, La/a/a/a/b/p;->e(I)Z

    move-result v0

    return v0
.end method
