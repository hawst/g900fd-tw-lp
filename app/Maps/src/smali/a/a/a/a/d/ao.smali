.class La/a/a/a/d/ao;
.super La/a/a/a/d/au;
.source "PG"

# interfaces
.implements La/a/a/a/d/bj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/am",
        "<TK;TV;>.a/a/a/a/d/au;",
        "La/a/a/a/d/bj",
        "<",
        "La/a/a/a/d/ax",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/d/am;

.field private b:La/a/a/a/d/as;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/d/am",
            "<TK;TV;>.a/a/a/a/d/as;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(La/a/a/a/d/am;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, La/a/a/a/d/ao;->a:La/a/a/a/d/am;

    invoke-direct {p0, p1}, La/a/a/a/d/au;-><init>(La/a/a/a/d/am;)V

    return-void
.end method


# virtual methods
.method public synthetic add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 806
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 806
    new-instance v0, La/a/a/a/d/as;

    iget-object v1, p0, La/a/a/a/d/ao;->a:La/a/a/a/d/am;

    invoke-virtual {p0}, La/a/a/a/d/ao;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, La/a/a/a/d/as;-><init>(La/a/a/a/d/am;I)V

    iput-object v0, p0, La/a/a/a/d/ao;->b:La/a/a/a/d/as;

    return-object v0
.end method

.method public synthetic previous()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 806
    new-instance v0, La/a/a/a/d/as;

    iget-object v1, p0, La/a/a/a/d/ao;->a:La/a/a/a/d/am;

    invoke-virtual {p0}, La/a/a/a/d/ao;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, La/a/a/a/d/as;-><init>(La/a/a/a/d/am;I)V

    iput-object v0, p0, La/a/a/a/d/ao;->b:La/a/a/a/d/as;

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 820
    invoke-super {p0}, La/a/a/a/d/au;->remove()V

    .line 821
    iget-object v0, p0, La/a/a/a/d/ao;->b:La/a/a/a/d/as;

    const/4 v1, -0x1

    iput v1, v0, La/a/a/a/d/as;->a:I

    .line 822
    return-void
.end method

.method public synthetic set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 806
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
