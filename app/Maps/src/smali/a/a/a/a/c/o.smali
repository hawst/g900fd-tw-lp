.class public La/a/a/a/c/o;
.super La/a/a/a/c/w;
.source "PG"


# instance fields
.field final synthetic a:La/a/a/a/c/n;


# direct methods
.method protected constructor <init>(La/a/a/a/c/n;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-direct {p0}, La/a/a/a/c/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/c/be;
    .locals 2

    .prologue
    .line 91
    new-instance v0, La/a/a/a/c/p;

    iget-object v1, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v1}, La/a/a/a/c/n;->l()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/c/p;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public final a(JJ)La/a/a/a/c/bh;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0}, La/a/a/a/c/n;->j()La/a/a/a/c/ay;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/c/ay;->d()La/a/a/a/c/bh;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0, p1, p2}, La/a/a/a/c/n;->b(J)Z

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0}, La/a/a/a/c/n;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(J)La/a/a/a/c/bh;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0}, La/a/a/a/c/n;->i()La/a/a/a/c/ay;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/c/ay;->d()La/a/a/a/c/bh;

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0}, La/a/a/a/c/n;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(J)La/a/a/a/c/bh;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0}, La/a/a/a/c/n;->h()La/a/a/a/c/ay;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/c/ay;->d()La/a/a/a/c/bh;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0}, La/a/a/a/c/n;->clear()V

    return-void
.end method

.method public synthetic comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0}, La/a/a/a/c/n;->g()La/a/a/a/c/bc;

    move-result-object v0

    return-object v0
.end method

.method public final d()La/a/a/a/c/ba;
    .locals 2

    .prologue
    .line 104
    new-instance v0, La/a/a/a/c/p;

    iget-object v1, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v1}, La/a/a/a/c/n;->l()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/c/p;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 91
    new-instance v0, La/a/a/a/c/p;

    iget-object v1, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v1}, La/a/a/a/c/n;->l()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/c/p;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, La/a/a/a/c/o;->a:La/a/a/a/c/n;

    invoke-virtual {v0}, La/a/a/a/c/n;->size()I

    move-result v0

    return v0
.end method
