.class public La/a/a/a/d/o;
.super La/a/a/a/d/x;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/x",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/d/n;


# direct methods
.method protected constructor <init>(La/a/a/a/d/n;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-direct {p0}, La/a/a/a/d/x;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/d/bg;
    .locals 2

    .prologue
    .line 71
    new-instance v0, La/a/a/a/d/p;

    iget-object v1, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v1}, La/a/a/a/d/n;->i()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/d/p;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public final b()La/a/a/a/d/bd;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bd",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, La/a/a/a/d/p;

    iget-object v1, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v1}, La/a/a/a/d/n;->i()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/d/p;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->clear()V

    return-void
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0, p1}, La/a/a/a/d/n;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public first()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->f()La/a/a/a/d/az;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/d/az;->d()La/a/a/a/d/bl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 71
    new-instance v0, La/a/a/a/d/p;

    iget-object v1, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v1}, La/a/a/a/d/n;->i()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/d/p;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->size()I

    move-result v0

    return v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->g()La/a/a/a/d/az;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/d/az;->d()La/a/a/a/d/bl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, La/a/a/a/d/o;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->e()La/a/a/a/d/az;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/d/az;->d()La/a/a/a/d/bl;

    move-result-object v0

    return-object v0
.end method
