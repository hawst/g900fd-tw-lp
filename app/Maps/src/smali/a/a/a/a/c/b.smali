.class public abstract La/a/a/a/c/b;
.super La/a/a/a/c/a;
.source "PG"

# interfaces
.implements La/a/a/a/c/y;
.implements Ljava/io/Serializable;


# static fields
.field public static final serialVersionUID:J = -0x44907a65b4c385f2L


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, La/a/a/a/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()La/a/a/a/c/bg;
    .locals 1

    .prologue
    .line 191
    new-instance v0, La/a/a/a/c/c;

    invoke-direct {v0, p0}, La/a/a/a/c/c;-><init>(La/a/a/a/c/b;)V

    return-object v0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, La/a/a/a/c/b;->b()La/a/a/a/b/ai;

    move-result-object v0

    invoke-interface {v0, p1}, La/a/a/a/b/ai;->a(I)Z

    move-result v0

    return v0
.end method

.method public b()La/a/a/a/b/ai;
    .locals 1

    .prologue
    .line 224
    new-instance v0, La/a/a/a/c/e;

    invoke-direct {v0, p0}, La/a/a/a/c/e;-><init>(La/a/a/a/c/b;)V

    return-object v0
.end method

.method public b(J)Z
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, La/a/a/a/c/b;->a()La/a/a/a/c/bg;

    move-result-object v0

    invoke-interface {v0, p1, p2}, La/a/a/a/c/bg;->a(J)Z

    move-result v0

    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 70
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, La/a/a/a/c/b;->a(I)Z

    move-result v0

    return v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, La/a/a/a/c/b;->c()La/a/a/a/d/bk;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 267
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 272
    :cond_0
    :goto_0
    return v0

    .line 268
    :cond_1
    instance-of v1, p1, Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 270
    check-cast p1, Ljava/util/Map;

    .line 271
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p0}, La/a/a/a/c/b;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 272
    invoke-virtual {p0}, La/a/a/a/c/b;->c()La/a/a/a/d/bk;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, La/a/a/a/d/bk;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 259
    const/4 v1, 0x0

    invoke-virtual {p0}, La/a/a/a/c/b;->size()I

    move-result v0

    .line 260
    invoke-virtual {p0}, La/a/a/a/c/b;->c()La/a/a/a/d/bk;

    move-result-object v2

    invoke-interface {v2}, La/a/a/a/d/bk;->a()La/a/a/a/d/bg;

    move-result-object v3

    move v2, v1

    .line 262
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_0

    invoke-interface {v3}, La/a/a/a/d/bg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 263
    :cond_0
    return v2
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, La/a/a/a/c/b;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, La/a/a/a/c/b;->a()La/a/a/a/c/bg;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+",
            "Ljava/lang/Long;",
            "+",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    .line 89
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 90
    instance-of v1, p1, La/a/a/a/c/y;

    if-eqz v1, :cond_0

    .line 92
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-eqz v0, :cond_1

    .line 93
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/c/z;

    .line 94
    invoke-interface {v0}, La/a/a/a/c/z;->a()J

    move-result-wide v4

    invoke-interface {v0}, La/a/a/a/c/z;->b()I

    move-result v0

    invoke-virtual {p0, v4, v5, v0}, La/a/a/a/c/b;->a(JI)I

    move v0, v1

    goto :goto_0

    .line 99
    :cond_0
    :goto_1
    add-int/lit8 v2, v0, -0x1

    if-eqz v0, :cond_1

    .line 100
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 101
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {p0, v1, v0}, La/a/a/a/c/b;->a(Ljava/lang/Long;Ljava/lang/Integer;)Ljava/lang/Integer;

    move v0, v2

    goto :goto_1

    .line 104
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 277
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 278
    invoke-virtual {p0}, La/a/a/a/c/b;->c()La/a/a/a/d/bk;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/d/bk;->a()La/a/a/a/d/bg;

    move-result-object v4

    .line 279
    invoke-virtual {p0}, La/a/a/a/c/b;->size()I

    move-result v1

    .line 281
    const/4 v0, 0x1

    .line 283
    const-string v2, "{"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-eqz v1, :cond_1

    .line 286
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move v1, v0

    .line 289
    :goto_1
    invoke-interface {v4}, La/a/a/a/d/bg;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/a/a/a/c/z;

    .line 294
    invoke-interface {v0}, La/a/a/a/c/z;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    const-string v5, "=>"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    invoke-interface {v0}, La/a/a/a/c/z;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    move v1, v2

    goto :goto_0

    .line 287
    :cond_0
    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    goto :goto_1

    .line 302
    :cond_1
    const-string v0, "}"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, La/a/a/a/c/b;->b()La/a/a/a/b/ai;

    move-result-object v0

    return-object v0
.end method
