.class La/a/a/a/b/aa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field c:I

.field d:I

.field e:I

.field f:I

.field final synthetic g:La/a/a/a/b/s;


# direct methods
.method constructor <init>(La/a/a/a/b/s;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 748
    iput-object p1, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 741
    iput v0, p0, La/a/a/a/b/aa;->c:I

    .line 743
    iput v0, p0, La/a/a/a/b/aa;->d:I

    .line 745
    iput v0, p0, La/a/a/a/b/aa;->e:I

    .line 747
    iput v0, p0, La/a/a/a/b/aa;->f:I

    .line 749
    iget v0, p1, La/a/a/a/b/s;->l:I

    iput v0, p0, La/a/a/a/b/aa;->d:I

    .line 750
    const/4 v0, 0x0

    iput v0, p0, La/a/a/a/b/aa;->f:I

    .line 751
    return-void
.end method

.method private final a()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 776
    iget v0, p0, La/a/a/a/b/aa;->f:I

    if-ltz v0, :cond_1

    .line 791
    :cond_0
    :goto_0
    return-void

    .line 777
    :cond_1
    iget v0, p0, La/a/a/a/b/aa;->c:I

    if-ne v0, v1, :cond_2

    .line 778
    const/4 v0, 0x0

    iput v0, p0, La/a/a/a/b/aa;->f:I

    goto :goto_0

    .line 781
    :cond_2
    iget v0, p0, La/a/a/a/b/aa;->d:I

    if-ne v0, v1, :cond_3

    .line 782
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->h:I

    iput v0, p0, La/a/a/a/b/aa;->f:I

    goto :goto_0

    .line 785
    :cond_3
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget v0, v0, La/a/a/a/b/s;->l:I

    .line 786
    const/4 v1, 0x1

    iput v1, p0, La/a/a/a/b/aa;->f:I

    .line 787
    :goto_1
    iget v1, p0, La/a/a/a/b/aa;->c:I

    if-eq v0, v1, :cond_0

    .line 788
    iget-object v1, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v1, v1, La/a/a/a/b/s;->n:[J

    aget-wide v0, v1, v0

    long-to-int v0, v0

    .line 789
    iget v1, p0, La/a/a/a/b/aa;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, La/a/a/a/b/aa;->f:I

    goto :goto_1
.end method


# virtual methods
.method public final b()I
    .locals 2

    .prologue
    .line 801
    invoke-virtual {p0}, La/a/a/a/b/aa;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    invoke-virtual {v0}, La/a/a/a/b/s;->size()I

    move-result v0

    .line 806
    :goto_0
    return v0

    .line 802
    :cond_0
    iget v0, p0, La/a/a/a/b/aa;->d:I

    iput v0, p0, La/a/a/a/b/aa;->e:I

    .line 803
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v0, v0, La/a/a/a/b/s;->n:[J

    iget v1, p0, La/a/a/a/b/aa;->e:I

    aget-wide v0, v0, v1

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/b/aa;->d:I

    .line 804
    iget v0, p0, La/a/a/a/b/aa;->e:I

    iput v0, p0, La/a/a/a/b/aa;->c:I

    .line 805
    iget v0, p0, La/a/a/a/b/aa;->f:I

    if-ltz v0, :cond_1

    iget v0, p0, La/a/a/a/b/aa;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/a/a/a/b/aa;->f:I

    .line 806
    :cond_1
    iget v0, p0, La/a/a/a/b/aa;->e:I

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 809
    invoke-virtual {p0}, La/a/a/a/b/aa;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 814
    :goto_0
    return v0

    .line 810
    :cond_0
    iget v0, p0, La/a/a/a/b/aa;->c:I

    iput v0, p0, La/a/a/a/b/aa;->e:I

    .line 811
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v0, v0, La/a/a/a/b/s;->n:[J

    iget v1, p0, La/a/a/a/b/aa;->e:I

    aget-wide v0, v0, v1

    const/16 v2, 0x20

    ushr-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/b/aa;->c:I

    .line 812
    iget v0, p0, La/a/a/a/b/aa;->e:I

    iput v0, p0, La/a/a/a/b/aa;->d:I

    .line 813
    iget v0, p0, La/a/a/a/b/aa;->f:I

    if-ltz v0, :cond_1

    iget v0, p0, La/a/a/a/b/aa;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/b/aa;->f:I

    .line 814
    :cond_1
    iget v0, p0, La/a/a/a/b/aa;->e:I

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 773
    iget v0, p0, La/a/a/a/b/aa;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrevious()Z
    .locals 2

    .prologue
    .line 774
    iget v0, p0, La/a/a/a/b/aa;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nextIndex()I
    .locals 1

    .prologue
    .line 793
    invoke-direct {p0}, La/a/a/a/b/aa;->a()V

    .line 794
    iget v0, p0, La/a/a/a/b/aa;->f:I

    return v0
.end method

.method public previousIndex()I
    .locals 1

    .prologue
    .line 797
    invoke-direct {p0}, La/a/a/a/b/aa;->a()V

    .line 798
    iget v0, p0, La/a/a/a/b/aa;->f:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove()V
    .locals 12

    .prologue
    const/16 v9, 0x20

    const-wide v10, 0xffffffffL

    const/4 v8, -0x1

    .line 818
    invoke-direct {p0}, La/a/a/a/b/aa;->a()V

    .line 819
    iget v0, p0, La/a/a/a/b/aa;->e:I

    if-ne v0, v8, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 820
    :cond_0
    iget v0, p0, La/a/a/a/b/aa;->e:I

    iget v1, p0, La/a/a/a/b/aa;->c:I

    if-ne v0, v1, :cond_4

    .line 823
    iget v0, p0, La/a/a/a/b/aa;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, La/a/a/a/b/aa;->f:I

    .line 824
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v0, v0, La/a/a/a/b/s;->n:[J

    iget v1, p0, La/a/a/a/b/aa;->e:I

    aget-wide v0, v0, v1

    ushr-long/2addr v0, v9

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/b/aa;->c:I

    .line 828
    :goto_0
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget v1, v0, La/a/a/a/b/s;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, La/a/a/a/b/s;->h:I

    .line 831
    iget v0, p0, La/a/a/a/b/aa;->c:I

    if-ne v0, v8, :cond_5

    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget v1, p0, La/a/a/a/b/aa;->d:I

    iput v1, v0, La/a/a/a/b/s;->l:I

    .line 834
    :goto_1
    iget v0, p0, La/a/a/a/b/aa;->d:I

    if-ne v0, v8, :cond_6

    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget v1, p0, La/a/a/a/b/aa;->c:I

    iput v1, v0, La/a/a/a/b/s;->m:I

    .line 837
    :goto_2
    iget v0, p0, La/a/a/a/b/aa;->e:I

    .line 840
    :goto_3
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget v2, v2, La/a/a/a/b/s;->g:I

    and-int/2addr v1, v2

    .line 841
    :goto_4
    iget-object v2, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v2, v2, La/a/a/a/b/s;->c:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_1

    .line 842
    iget-object v2, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v2, v2, La/a/a/a/b/s;->a:[I

    aget v2, v2, v1

    invoke-static {v2}, La/a/a/a/c;->a(I)I

    move-result v2

    iget-object v3, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget v3, v3, La/a/a/a/b/s;->g:I

    and-int/2addr v2, v3

    .line 843
    if-gt v0, v1, :cond_7

    if-ge v0, v2, :cond_1

    if-le v2, v1, :cond_8

    .line 846
    :cond_1
    iget-object v2, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v2, v2, La/a/a/a/b/s;->c:[Z

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_9

    .line 847
    iget-object v2, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v2, v2, La/a/a/a/b/s;->a:[I

    iget-object v3, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v3, v3, La/a/a/a/b/s;->a:[I

    aget v3, v3, v1

    aput v3, v2, v0

    .line 848
    iget-object v2, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v2, v2, La/a/a/a/b/s;->b:[Ljava/lang/Object;

    iget-object v3, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v3, v3, La/a/a/a/b/s;->b:[Ljava/lang/Object;

    aget-object v3, v3, v1

    aput-object v3, v2, v0

    .line 849
    iget v2, p0, La/a/a/a/b/aa;->d:I

    if-ne v2, v1, :cond_2

    iput v0, p0, La/a/a/a/b/aa;->d:I

    .line 850
    :cond_2
    iget v2, p0, La/a/a/a/b/aa;->c:I

    if-ne v2, v1, :cond_3

    iput v0, p0, La/a/a/a/b/aa;->c:I

    .line 851
    :cond_3
    iget-object v2, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    invoke-virtual {v2, v1, v0}, La/a/a/a/b/s;->a(II)V

    move v0, v1

    goto :goto_3

    .line 827
    :cond_4
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v0, v0, La/a/a/a/b/s;->n:[J

    iget v1, p0, La/a/a/a/b/aa;->e:I

    aget-wide v0, v0, v1

    long-to-int v0, v0

    iput v0, p0, La/a/a/a/b/aa;->d:I

    goto :goto_0

    .line 833
    :cond_5
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v0, v0, La/a/a/a/b/s;->n:[J

    iget v1, p0, La/a/a/a/b/aa;->c:I

    aget-wide v2, v0, v1

    iget-object v4, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v4, v4, La/a/a/a/b/s;->n:[J

    iget v5, p0, La/a/a/a/b/aa;->c:I

    aget-wide v4, v4, v5

    iget v6, p0, La/a/a/a/b/aa;->d:I

    int-to-long v6, v6

    and-long/2addr v6, v10

    xor-long/2addr v4, v6

    and-long/2addr v4, v10

    xor-long/2addr v2, v4

    aput-wide v2, v0, v1

    goto/16 :goto_1

    .line 836
    :cond_6
    iget-object v0, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v0, v0, La/a/a/a/b/s;->n:[J

    iget v1, p0, La/a/a/a/b/aa;->d:I

    aget-wide v2, v0, v1

    iget-object v4, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v4, v4, La/a/a/a/b/s;->n:[J

    iget v5, p0, La/a/a/a/b/aa;->d:I

    aget-wide v4, v4, v5

    iget v6, p0, La/a/a/a/b/aa;->c:I

    int-to-long v6, v6

    and-long/2addr v6, v10

    shl-long/2addr v6, v9

    xor-long/2addr v4, v6

    const-wide v6, -0x100000000L

    and-long/2addr v4, v6

    xor-long/2addr v2, v4

    aput-wide v2, v0, v1

    goto/16 :goto_2

    .line 843
    :cond_7
    if-lt v0, v2, :cond_8

    if-gt v2, v1, :cond_1

    .line 844
    :cond_8
    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget v2, v2, La/a/a/a/b/s;->g:I

    and-int/2addr v1, v2

    goto/16 :goto_4

    .line 853
    :cond_9
    iget-object v1, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v1, v1, La/a/a/a/b/s;->c:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 854
    iget-object v1, p0, La/a/a/a/b/aa;->g:La/a/a/a/b/s;

    iget-object v1, v1, La/a/a/a/b/s;->b:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 855
    iput v8, p0, La/a/a/a/b/aa;->e:I

    .line 856
    return-void
.end method
