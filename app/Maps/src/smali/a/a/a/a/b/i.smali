.class public La/a/a/a/b/i;
.super La/a/a/a/b/q;
.source "PG"


# instance fields
.field final synthetic a:La/a/a/a/b/h;


# direct methods
.method protected constructor <init>(La/a/a/a/b/h;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-direct {p0}, La/a/a/a/b/q;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/b/al;
    .locals 2

    .prologue
    .line 91
    new-instance v0, La/a/a/a/b/j;

    iget-object v1, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v1}, La/a/a/a/b/h;->l()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/b/j;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public final a(II)La/a/a/a/b/ao;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0}, La/a/a/a/b/h;->k()La/a/a/a/b/af;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/b/af;->d()La/a/a/a/b/ao;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0, p1}, La/a/a/a/b/h;->b(I)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0}, La/a/a/a/b/h;->f()I

    move-result v0

    return v0
.end method

.method public final b(I)La/a/a/a/b/ao;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0}, La/a/a/a/b/h;->j()La/a/a/a/b/af;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/b/af;->d()La/a/a/a/b/ao;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0}, La/a/a/a/b/h;->g()I

    move-result v0

    return v0
.end method

.method public final c(I)La/a/a/a/b/ao;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0}, La/a/a/a/b/h;->i()La/a/a/a/b/af;

    move-result-object v0

    invoke-interface {v0}, La/a/a/a/b/af;->d()La/a/a/a/b/ao;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0}, La/a/a/a/b/h;->clear()V

    return-void
.end method

.method public synthetic comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0}, La/a/a/a/b/h;->h()La/a/a/a/b/aj;

    move-result-object v0

    return-object v0
.end method

.method public final d()La/a/a/a/b/ah;
    .locals 2

    .prologue
    .line 104
    new-instance v0, La/a/a/a/b/j;

    iget-object v1, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v1}, La/a/a/a/b/h;->l()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/b/j;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 91
    new-instance v0, La/a/a/a/b/j;

    iget-object v1, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v1}, La/a/a/a/b/h;->l()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/b/j;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, La/a/a/a/b/i;->a:La/a/a/a/b/h;

    invoke-virtual {v0}, La/a/a/a/b/h;->size()I

    move-result v0

    return v0
.end method
