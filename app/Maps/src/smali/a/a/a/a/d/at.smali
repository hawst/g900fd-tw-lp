.class public final La/a/a/a/d/at;
.super La/a/a/a/d/x;
.source "PG"

# interfaces
.implements La/a/a/a/d/ba;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/x",
        "<",
        "La/a/a/a/d/ax",
        "<TK;TV;>;>;",
        "La/a/a/a/d/ba",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/d/am;


# direct methods
.method public constructor <init>(La/a/a/a/d/am;)V
    .locals 0

    .prologue
    .line 847
    iput-object p1, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    invoke-direct {p0}, La/a/a/a/d/x;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/d/bg;
    .locals 2

    .prologue
    .line 847
    new-instance v0, La/a/a/a/d/ao;

    iget-object v1, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    invoke-direct {v0, v1}, La/a/a/a/d/ao;-><init>(La/a/a/a/d/am;)V

    return-object v0
.end method

.method public final b()La/a/a/a/d/bd;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bd",
            "<",
            "La/a/a/a/d/ax",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 849
    new-instance v0, La/a/a/a/d/ao;

    iget-object v1, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    invoke-direct {v0, v1}, La/a/a/a/d/ao;-><init>(La/a/a/a/d/am;)V

    return-object v0
.end method

.method public final synthetic c()La/a/a/a/d/bg;
    .locals 2

    .prologue
    .line 847
    new-instance v0, La/a/a/a/d/ap;

    iget-object v1, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    invoke-direct {v0, v1}, La/a/a/a/d/ap;-><init>(La/a/a/a/d/am;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 898
    iget-object v0, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    invoke-virtual {v0}, La/a/a/a/d/am;->clear()V

    .line 899
    return-void
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-",
            "La/a/a/a/d/ax",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 851
    const/4 v0, 0x0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 865
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 875
    :goto_0
    return v0

    .line 866
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 867
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 869
    if-nez v2, :cond_2

    const v0, 0x87fcd5c

    :goto_1
    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v3, v3, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v3

    .line 871
    :goto_2
    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v3, v3, La/a/a/a/d/am;->c:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_7

    .line 872
    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v3, v3, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-nez v3, :cond_3

    if-nez v2, :cond_4

    :cond_1
    iget-object v2, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v2, v2, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    .line 869
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_1

    .line 872
    :cond_3
    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v3, v3, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 873
    :cond_4
    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v3, v3, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v3

    goto :goto_2

    :cond_5
    move v0, v1

    .line 872
    goto :goto_0

    :cond_6
    iget-object v1, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v1, v1, La/a/a/a/d/am;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_7
    move v0, v1

    .line 875
    goto :goto_0
.end method

.method public final synthetic first()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 847
    iget-object v0, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, La/a/a/a/d/as;

    iget-object v1, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v2, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v2, v2, La/a/a/a/d/am;->l:I

    invoke-direct {v0, v1, v2}, La/a/a/a/d/as;-><init>(La/a/a/a/d/am;I)V

    return-object v0
.end method

.method public final synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 847
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 847
    new-instance v0, La/a/a/a/d/ao;

    iget-object v1, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    invoke-direct {v0, v1}, La/a/a/a/d/ao;-><init>(La/a/a/a/d/am;)V

    return-object v0
.end method

.method public final synthetic last()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 847
    iget-object v0, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, La/a/a/a/d/as;

    iget-object v1, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v2, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v2, v2, La/a/a/a/d/am;->m:I

    invoke-direct {v0, v1, v2}, La/a/a/a/d/as;-><init>(La/a/a/a/d/am;I)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 879
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_0

    move v0, v1

    .line 892
    :goto_0
    return v0

    .line 880
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 881
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 883
    if-nez v2, :cond_2

    const v0, 0x87fcd5c

    :goto_1
    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v3, v3, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v3

    .line 885
    :goto_2
    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v3, v3, La/a/a/a/d/am;->c:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_5

    .line 886
    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v3, v3, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-nez v3, :cond_3

    if-nez v2, :cond_4

    .line 887
    :cond_1
    iget-object v0, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, La/a/a/a/d/am;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 888
    const/4 v0, 0x1

    goto :goto_0

    .line 883
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, La/a/a/a/c;->a(I)I

    move-result v0

    goto :goto_1

    .line 886
    :cond_3
    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget-object v3, v3, La/a/a/a/d/am;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 890
    :cond_4
    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v3, v3, La/a/a/a/d/am;->g:I

    and-int/2addr v0, v3

    goto :goto_2

    :cond_5
    move v0, v1

    .line 892
    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, La/a/a/a/d/at;->a:La/a/a/a/d/am;

    iget v0, v0, La/a/a/a/d/am;->h:I

    return v0
.end method

.method public final synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 847
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 847
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
