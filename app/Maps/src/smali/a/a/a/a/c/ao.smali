.class La/a/a/a/c/ao;
.super La/a/a/a/c/at;
.source "PG"

# interfaces
.implements La/a/a/a/d/bj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/c/al",
        "<TV;>.a/a/a/a/c/at;",
        "La/a/a/a/d/bj",
        "<",
        "La/a/a/a/c/aw",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field final a:La/a/a/a/c/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/a/a/a/c/m",
            "<TV;>;"
        }
    .end annotation
.end field

.field final synthetic b:La/a/a/a/c/al;


# direct methods
.method public constructor <init>(La/a/a/a/c/al;)V
    .locals 4

    .prologue
    .line 890
    iput-object p1, p0, La/a/a/a/c/ao;->b:La/a/a/a/c/al;

    invoke-direct {p0, p1}, La/a/a/a/c/at;-><init>(La/a/a/a/c/al;)V

    .line 889
    new-instance v0, La/a/a/a/c/m;

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, La/a/a/a/c/m;-><init>(JLjava/lang/Object;)V

    iput-object v0, p0, La/a/a/a/c/ao;->a:La/a/a/a/c/m;

    .line 890
    return-void
.end method


# virtual methods
.method public synthetic add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 888
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 888
    invoke-virtual {p0}, La/a/a/a/c/ao;->b()I

    move-result v0

    iget-object v1, p0, La/a/a/a/c/ao;->a:La/a/a/a/c/m;

    iget-object v2, p0, La/a/a/a/c/ao;->b:La/a/a/a/c/al;

    iget-object v2, v2, La/a/a/a/c/al;->a:[J

    aget-wide v2, v2, v0

    iput-wide v2, v1, La/a/a/a/c/m;->a:J

    iget-object v1, p0, La/a/a/a/c/ao;->a:La/a/a/a/c/m;

    iget-object v2, p0, La/a/a/a/c/ao;->b:La/a/a/a/c/al;

    iget-object v2, v2, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v0, v2, v0

    iput-object v0, v1, La/a/a/a/c/m;->b:Ljava/lang/Object;

    iget-object v0, p0, La/a/a/a/c/ao;->a:La/a/a/a/c/m;

    return-object v0
.end method

.method public synthetic previous()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 888
    invoke-virtual {p0}, La/a/a/a/c/ao;->c()I

    move-result v0

    iget-object v1, p0, La/a/a/a/c/ao;->a:La/a/a/a/c/m;

    iget-object v2, p0, La/a/a/a/c/ao;->b:La/a/a/a/c/al;

    iget-object v2, v2, La/a/a/a/c/al;->a:[J

    aget-wide v2, v2, v0

    iput-wide v2, v1, La/a/a/a/c/m;->a:J

    iget-object v1, p0, La/a/a/a/c/ao;->a:La/a/a/a/c/m;

    iget-object v2, p0, La/a/a/a/c/ao;->b:La/a/a/a/c/al;

    iget-object v2, v2, La/a/a/a/c/al;->b:[Ljava/lang/Object;

    aget-object v0, v2, v0

    iput-object v0, v1, La/a/a/a/c/m;->b:Ljava/lang/Object;

    iget-object v0, p0, La/a/a/a/c/ao;->a:La/a/a/a/c/m;

    return-object v0
.end method

.method public synthetic set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 888
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
