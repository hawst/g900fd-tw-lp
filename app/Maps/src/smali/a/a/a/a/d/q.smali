.class public La/a/a/a/d/q;
.super La/a/a/a/d/t;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La/a/a/a/d/t",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/a/a/a/d/n;


# direct methods
.method protected constructor <init>(La/a/a/a/d/n;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, La/a/a/a/d/q;->a:La/a/a/a/d/n;

    invoke-direct {p0}, La/a/a/a/d/t;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()La/a/a/a/d/bg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/bg",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, La/a/a/a/d/r;

    iget-object v1, p0, La/a/a/a/d/q;->a:La/a/a/a/d/n;

    invoke-virtual {v1}, La/a/a/a/d/n;->i()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/d/r;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, La/a/a/a/d/q;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, La/a/a/a/d/q;->a:La/a/a/a/d/n;

    invoke-virtual {v0, p1}, La/a/a/a/d/n;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 114
    new-instance v0, La/a/a/a/d/r;

    iget-object v1, p0, La/a/a/a/d/q;->a:La/a/a/a/d/n;

    invoke-virtual {v1}, La/a/a/a/d/n;->i()La/a/a/a/d/bl;

    move-result-object v1

    invoke-interface {v1}, La/a/a/a/d/bl;->b()La/a/a/a/d/bd;

    move-result-object v1

    invoke-direct {v0, v1}, La/a/a/a/d/r;-><init>(La/a/a/a/d/bd;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, La/a/a/a/d/q;->a:La/a/a/a/d/n;

    invoke-virtual {v0}, La/a/a/a/d/n;->size()I

    move-result v0

    return v0
.end method
