.class public abstract La/a/a/a/b/h;
.super La/a/a/a/b/b;
.source "PG"

# interfaces
.implements La/a/a/a/b/af;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "La/a/a/a/b/b",
        "<TV;>;",
        "La/a/a/a/b/af",
        "<TV;>;"
    }
.end annotation


# static fields
.field public static final serialVersionUID:J = -0x189cf424fb7d7759L


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, La/a/a/a/b/b;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a()La/a/a/a/b/an;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/b/h;->d()La/a/a/a/b/ao;

    move-result-object v0

    return-object v0
.end method

.method public b()La/a/a/a/d/be;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "La/a/a/a/d/be",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v0, La/a/a/a/b/k;

    invoke-direct {v0, p0}, La/a/a/a/b/k;-><init>(La/a/a/a/b/h;)V

    return-object v0
.end method

.method public final synthetic c()La/a/a/a/d/bk;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/b/h;->l()La/a/a/a/d/bl;

    move-result-object v0

    return-object v0
.end method

.method public d()La/a/a/a/b/ao;
    .locals 1

    .prologue
    .line 88
    new-instance v0, La/a/a/a/b/i;

    invoke-direct {v0, p0}, La/a/a/a/b/i;-><init>(La/a/a/a/b/h;)V

    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/b/h;->l()La/a/a/a/d/bl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic firstKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/b/h;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 53
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    invoke-virtual {p0}, La/a/a/a/b/h;->j()La/a/a/a/b/af;

    move-result-object v0

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/b/h;->d()La/a/a/a/b/ao;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lastKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/b/h;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 53
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    invoke-virtual {p0}, La/a/a/a/b/h;->k()La/a/a/a/b/af;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 53
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    invoke-virtual {p0}, La/a/a/a/b/h;->i()La/a/a/a/b/af;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, La/a/a/a/b/h;->b()La/a/a/a/d/be;

    move-result-object v0

    return-object v0
.end method
