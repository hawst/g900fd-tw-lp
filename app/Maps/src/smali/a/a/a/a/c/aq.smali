.class final La/a/a/a/c/aq;
.super La/a/a/a/c/w;
.source "PG"


# instance fields
.field final synthetic a:La/a/a/a/c/al;


# direct methods
.method constructor <init>(La/a/a/a/c/al;)V
    .locals 0

    .prologue
    .line 994
    iput-object p1, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    invoke-direct {p0}, La/a/a/a/c/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()La/a/a/a/c/be;
    .locals 2

    .prologue
    .line 994
    new-instance v0, La/a/a/a/c/ap;

    iget-object v1, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    invoke-direct {v0, v1}, La/a/a/a/c/ap;-><init>(La/a/a/a/c/al;)V

    return-object v0
.end method

.method public final a(JJ)La/a/a/a/c/bh;
    .locals 1

    .prologue
    .line 1026
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(J)Z
    .locals 1

    .prologue
    .line 1005
    iget-object v0, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    invoke-virtual {v0, p1, p2}, La/a/a/a/c/al;->b(J)Z

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1016
    iget-object v0, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget v0, v0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1017
    :cond_0
    iget-object v0, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->a:[J

    iget-object v1, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget v1, v1, La/a/a/a/c/al;->l:I

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final b(J)La/a/a/a/c/bh;
    .locals 1

    .prologue
    .line 1025
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget v0, v0, La/a/a/a/c/al;->h:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1021
    :cond_0
    iget-object v0, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget-object v0, v0, La/a/a/a/c/al;->a:[J

    iget-object v1, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget v1, v1, La/a/a/a/c/al;->m:I

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final c(J)La/a/a/a/c/bh;
    .locals 1

    .prologue
    .line 1024
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 1013
    iget-object v0, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    invoke-virtual {v0}, La/a/a/a/c/al;->clear()V

    .line 1014
    return-void
.end method

.method public final bridge synthetic comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 994
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic d()La/a/a/a/c/ba;
    .locals 2

    .prologue
    .line 994
    new-instance v0, La/a/a/a/c/ap;

    iget-object v1, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    invoke-direct {v0, v1}, La/a/a/a/c/ap;-><init>(La/a/a/a/c/al;)V

    return-object v0
.end method

.method public final e(J)Z
    .locals 3

    .prologue
    .line 1008
    iget-object v0, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget v0, v0, La/a/a/a/c/al;->h:I

    .line 1009
    iget-object v1, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    invoke-virtual {v1, p1, p2}, La/a/a/a/c/al;->a(J)Ljava/lang/Object;

    .line 1010
    iget-object v1, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget v1, v1, La/a/a/a/c/al;->h:I

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 994
    new-instance v0, La/a/a/a/c/ap;

    iget-object v1, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    invoke-direct {v0, v1}, La/a/a/a/c/ap;-><init>(La/a/a/a/c/al;)V

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, La/a/a/a/c/aq;->a:La/a/a/a/c/al;

    iget v0, v0, La/a/a/a/c/al;->h:I

    return v0
.end method
