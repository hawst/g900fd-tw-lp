.class public final Lacp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Laak;

.field b:Lacy;

.field private c:Ljava/lang/String;

.field private d:Lacr;

.field private e:Labc;

.field private f:Labb;

.field private g:Z

.field private h:Z

.field private i:Lacv;


# direct methods
.method constructor <init>(Laak;Ljava/lang/String;Lacy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v1, p0, Lacp;->d:Lacr;

    .line 42
    iput-object v1, p0, Lacp;->e:Labc;

    .line 43
    iput-object v1, p0, Lacp;->f:Labb;

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lacp;->g:Z

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lacp;->h:Z

    .line 46
    iput-object v1, p0, Lacp;->i:Lacv;

    .line 292
    iput-object p1, p0, Lacp;->a:Laak;

    .line 293
    iput-object p2, p0, Lacp;->c:Ljava/lang/String;

    .line 294
    iput-object p3, p0, Lacp;->b:Lacy;

    .line 295
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Field;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    .prologue
    .line 330
    const/4 v0, 0x0

    .line 332
    :try_start_0
    invoke-virtual {p2, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 333
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    :cond_0
    :goto_0
    return-object v0

    .line 335
    :catch_0
    move-exception v1

    invoke-virtual {p2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    .line 336
    if-eqz v1, :cond_0

    .line 337
    invoke-direct {p0, p1, v1}, Lacp;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Field;

    move-result-object v0

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lacp;->e:Labc;

    if-nez v0, :cond_0

    .line 324
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to attach port while not in attachment stage!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lacv;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lacp;->i:Lacv;

    return-object v0
.end method

.method a(Labb;)V
    .locals 1

    .prologue
    .line 308
    iput-object p1, p0, Lacp;->f:Labb;

    .line 309
    const/4 v0, 0x0

    iput-object v0, p0, Lacp;->e:Labc;

    .line 310
    return-void
.end method

.method a(Labc;)V
    .locals 2

    .prologue
    .line 302
    iput-object p1, p0, Lacp;->e:Labc;

    .line 303
    iget-object v0, p0, Lacp;->e:Labc;

    iget-object v1, p0, Lacp;->b:Lacy;

    iget-object v1, v1, Lacy;->a:Labf;

    invoke-virtual {v0, v1}, Labc;->b(Labf;)V

    .line 304
    iget-object v0, p0, Lacp;->a:Laak;

    invoke-virtual {v0, p0}, Laak;->b(Lacp;)V

    .line 305
    return-void
.end method

.method public a(Lacr;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Lacp;->k()V

    .line 119
    iput-object p1, p0, Lacp;->d:Lacr;

    .line 120
    return-void
.end method

.method public a(Lacv;)V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0}, Lacp;->k()V

    .line 87
    iget-object v0, p0, Lacp;->a:Laak;

    invoke-virtual {v0, p1}, Laak;->b(Lacv;)V

    .line 88
    iget-object v0, p0, Lacp;->e:Labc;

    invoke-virtual {p1}, Lacv;->e()Labb;

    move-result-object v1

    invoke-virtual {v0, v1}, Labc;->a(Labb;)V

    .line 89
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lacp;->a:Laak;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lacp;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 159
    if-nez v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to bind to unknown field \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    invoke-virtual {p0, v0}, Lacp;->a(Ljava/lang/reflect/Field;)V

    .line 164
    return-void
.end method

.method public a(Ljava/lang/reflect/Field;)V
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Lacp;->k()V

    .line 139
    new-instance v0, Lacq;

    invoke-direct {v0, p0, p1}, Lacq;-><init>(Lacp;Ljava/lang/reflect/Field;)V

    iput-object v0, p0, Lacp;->d:Lacr;

    .line 140
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lacp;->h:Z

    .line 173
    return-void
.end method

.method public b(Lacv;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lacp;->i:Lacv;

    .line 96
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 244
    iput-boolean p1, p0, Lacp;->g:Z

    .line 245
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lacp;->h:Z

    return v0
.end method

.method public declared-synchronized c()Laap;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lacp;->f:Labb;

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot pull frame from closed input port!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 197
    :cond_0
    :try_start_1
    iget-object v0, p0, Lacp;->f:Labb;

    invoke-virtual {v0}, Labb;->d()Laap;

    move-result-object v0

    .line 198
    if-eqz v0, :cond_3

    .line 199
    iget-object v1, p0, Lacp;->d:Lacr;

    if-eqz v1, :cond_1

    .line 200
    iget-object v1, p0, Lacp;->d:Lacr;

    invoke-interface {v1, p0, v0}, Lacr;->a(Lacp;Laap;)V

    .line 203
    :cond_1
    iget-object v1, p0, Lacp;->a:Laak;

    invoke-virtual {v1, v0}, Laak;->a(Laap;)V

    .line 204
    iget-object v1, v0, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->g()J

    move-result-wide v2

    .line 205
    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 206
    iget-object v1, p0, Lacp;->a:Laak;

    iget-object v2, v0, Laap;->a:Lzp;

    invoke-virtual {v2}, Lzp;->g()J

    move-result-wide v2

    iget-wide v4, v1, Laak;->mCurrentTimestamp:J

    cmp-long v4, v2, v4

    if-gtz v4, :cond_2

    iget-wide v4, v1, Laak;->mCurrentTimestamp:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    :cond_2
    iput-wide v2, v1, Laak;->mCurrentTimestamp:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209
    :cond_3
    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized d()Z
    .locals 1

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lacp;->f:Labb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lacp;->f:Labb;

    invoke-virtual {v0}, Labb;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lacp;->c:Ljava/lang/String;

    return-object v0
.end method

.method public f()Labf;
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0}, Lacp;->i()Labb;

    move-result-object v0

    invoke-virtual {v0}, Labb;->a()Labf;

    move-result-object v0

    return-object v0
.end method

.method public g()Laak;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lacp;->a:Laak;

    return-object v0
.end method

.method h()Z
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Lacp;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lacp;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method i()Labb;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lacp;->f:Labb;

    return-object v0
.end method

.method j()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lacp;->f:Labb;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lacp;->f:Labb;

    invoke-virtual {v0}, Labb;->e()V

    .line 320
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lacp;->a:Laak;

    invoke-virtual {v1}, Laak;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lacp;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
