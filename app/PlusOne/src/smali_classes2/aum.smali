.class public Laum;
.super Lamn;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lawj;

.field private final c:Laur;

.field private final d:Laux;

.field private final e:Lavj;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Laxp;

.field private final j:Lcdu;

.field private final k:Lauy;

.field private final l:Lawk;

.field private final m:Lawk;

.field private final n:Lawk;

.field private final o:Lawk;

.field private final p:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Lauw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Laum;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laum;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Laur;Lawj;Lavj;Ljava/lang/String;Ljava/lang/String;Laxp;Lcdu;Lauy;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 255
    invoke-direct {p0}, Lamn;-><init>()V

    .line 220
    new-instance v0, Lauu;

    invoke-direct {v0, p0}, Lauu;-><init>(Laum;)V

    iput-object v0, p0, Laum;->l:Lawk;

    .line 222
    new-instance v0, Laut;

    invoke-direct {v0, p0}, Laut;-><init>(Laum;)V

    iput-object v0, p0, Laum;->m:Lawk;

    .line 223
    new-instance v0, Laus;

    invoke-direct {v0, p0}, Laus;-><init>(Laum;)V

    iput-object v0, p0, Laum;->n:Lawk;

    .line 227
    new-instance v0, Laun;

    invoke-direct {v0, p0}, Laun;-><init>(Laum;)V

    iput-object v0, p0, Laum;->o:Lawk;

    .line 235
    new-instance v0, Lauo;

    const-class v1, Lauw;

    invoke-direct {v0, p0, v1}, Lauo;-><init>(Laum;Ljava/lang/Class;)V

    iput-object v0, p0, Laum;->p:Lasn;

    .line 256
    const-string v0, "stateController"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laur;

    iput-object v0, p0, Laum;->c:Laur;

    .line 257
    const-string v0, "soundtrackUsageRecorder"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavj;

    iput-object v0, p0, Laum;->e:Lavj;

    .line 259
    const-string v0, "state"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawj;

    iput-object v0, p0, Laum;->b:Lawj;

    .line 260
    new-instance v0, Lauv;

    invoke-direct {v0, p0}, Lauv;-><init>(Laum;)V

    iput-object v0, p0, Laum;->d:Laux;

    .line 261
    const-string v0, "frequentlyUsedTracksText"

    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Laum;->f:Ljava/lang/String;

    .line 263
    const-string v0, "suggestedTracksForThemeText"

    invoke-static {p5, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Laum;->g:Ljava/lang/String;

    .line 265
    const-string v0, "userHintDecider"

    invoke-static {p6, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxp;

    iput-object v0, p0, Laum;->h:Laxp;

    .line 266
    const-string v0, "analyticsSession"

    invoke-static {p7, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Laum;->j:Lcdu;

    .line 267
    const-string v0, "soundtrackPlayer"

    invoke-static {p8, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauy;

    iput-object v0, p0, Laum;->k:Lauy;

    .line 268
    return-void
.end method

.method static synthetic a(Laum;)Lasn;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Laum;->p:Lasn;

    return-object v0
.end method

.method static synthetic a(Laum;Lauw;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Laum;->c(Lauw;)V

    return-void
.end method

.method static synthetic a(Laum;Lboh;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    iget-object v3, p0, Laum;->b:Lawj;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Lawj;->b(Z)V

    iget-object v0, p0, Laum;->b:Lawj;

    invoke-interface {v0, p1}, Lawj;->a(Lboh;)V

    iget-object v0, p0, Laum;->b:Lawj;

    invoke-interface {v0}, Lawj;->aN()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laum;->b:Lawj;

    if-nez p1, :cond_2

    :goto_1
    invoke-interface {v0, v1}, Lawj;->j(Z)V

    :cond_0
    iget-object v0, p0, Laum;->h:Laxp;

    sget-object v1, Laxq;->h:Laxq;

    invoke-virtual {v0, v1}, Laxp;->a(Laxq;)V

    iget-object v0, p0, Laum;->j:Lcdu;

    invoke-virtual {v0}, Lcdu;->i()V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method static synthetic b(Laum;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Laum;->e()V

    return-void
.end method

.method static synthetic b(Laum;Lauw;)V
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Laum;->e()V

    iget-object v0, p0, Laum;->d:Laux;

    invoke-interface {p1, v0}, Lauw;->a(Laux;)V

    invoke-direct {p0}, Laum;->d()V

    invoke-direct {p0, p1}, Laum;->c(Lauw;)V

    return-void
.end method

.method static synthetic c(Laum;)Lawj;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Laum;->b:Lawj;

    return-object v0
.end method

.method static synthetic c()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method static synthetic c(Laum;Lauw;)V
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Laum;->e()V

    iget-object v0, p0, Laum;->k:Lauy;

    invoke-virtual {v0}, Lauy;->a()V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lauw;->a(Laux;)V

    return-void
.end method

.method private c(Lauw;)V
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0, p1}, Laum;->e(Lauw;)V

    .line 336
    invoke-direct {p0, p1}, Laum;->d(Lauw;)V

    .line 337
    invoke-virtual {p0}, Laum;->e()V

    iget-object v0, p0, Laum;->b:Lawj;

    invoke-interface {v0}, Lawj;->ab()Z

    move-result v0

    invoke-interface {p1, v0}, Lauw;->a(Z)V

    iget-object v0, p0, Laum;->b:Lawj;

    invoke-interface {v0}, Lawj;->H()Z

    move-result v0

    invoke-interface {p1, v0}, Lauw;->b(Z)V

    .line 338
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    .line 455
    invoke-virtual {p0}, Laum;->e()V

    .line 457
    iget-object v0, p0, Laum;->b:Lawj;

    invoke-interface {v0}, Lawj;->u()Lbrc;

    move-result-object v0

    .line 458
    if-eqz v0, :cond_2

    .line 459
    invoke-virtual {v0}, Lbrc;->b()V

    .line 462
    iget-object v1, p0, Laum;->b:Lawj;

    invoke-interface {v1}, Lawj;->q()J

    move-result-wide v2

    .line 463
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 464
    invoke-virtual {v0, v2, v3}, Lbrc;->a(J)Lboh;

    move-result-object v1

    if-nez v1, :cond_0

    .line 465
    iget-object v1, p0, Laum;->b:Lawj;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lawj;->a(Lboh;)V

    .line 467
    :cond_0
    invoke-virtual {v0}, Lbrc;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 468
    iget-object v1, p0, Laum;->e:Lavj;

    invoke-virtual {v1, v0}, Lavj;->a(Lbrc;)V

    .line 471
    :cond_1
    iget-object v0, p0, Laum;->p:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lauw;

    invoke-direct {p0, v0}, Laum;->c(Lauw;)V

    .line 473
    :cond_2
    return-void
.end method

.method static synthetic d(Laum;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Laum;->e()V

    return-void
.end method

.method static synthetic d(Laum;Lauw;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Laum;->e(Lauw;)V

    return-void
.end method

.method private d(Lauw;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 355
    invoke-virtual {p0}, Laum;->e()V

    .line 356
    iget-object v2, p0, Laum;->b:Lawj;

    invoke-interface {v2}, Lawj;->u()Lbrc;

    move-result-object v2

    .line 359
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lbrc;->c()Z

    move-result v2

    if-nez v2, :cond_5

    .line 360
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Laum;->b:Lawj;

    invoke-interface {v1}, Lawj;->u()Lbrc;

    move-result-object v7

    iget-object v1, p0, Laum;->b:Lawj;

    invoke-interface {v1}, Lawj;->u()Lbrc;

    move-result-object v1

    iget-object v2, p0, Laum;->b:Lawj;

    invoke-interface {v2}, Lawj;->o()Lbza;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbrc;->a(Lbza;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v0, Lauq;

    iget-object v2, p0, Laum;->g:Ljava/lang/String;

    const-wide/16 v4, -0x3

    invoke-direct {v0, v2, v1, v4, v5}, Lauq;-><init>(Ljava/lang/String;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v0

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Laum;->e:Lavj;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lavj;->a(I)[J

    move-result-object v4

    array-length v5, v4

    move v0, v3

    :goto_1
    if-ge v0, v5, :cond_1

    aget-wide v8, v4, v0

    invoke-virtual {v7, v8, v9}, Lbrc;->a(J)Lboh;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    sget-object v10, Laum;->a:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const/16 v11, 0x36

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "Frequently used track is missing: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lauq;

    iget-object v4, p0, Laum;->f:Ljava/lang/String;

    const-wide/16 v8, -0x2

    invoke-direct {v0, v4, v1, v8, v9}, Lauq;-><init>(Ljava/lang/String;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v7}, Lbrc;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbmo;

    invoke-virtual {v7, v1}, Lbrc;->a(Lbmo;)Ljava/util/List;

    move-result-object v0

    const-wide/16 v4, -0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboh;

    invoke-virtual {v0}, Lboh;->d()J

    move-result-wide v4

    :cond_3
    new-instance v0, Lauq;

    invoke-virtual {v1}, Lbmo;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v1}, Lbrc;->a(Lbmo;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v9, v1, v4, v5}, Lauq;-><init>(Ljava/lang/String;Ljava/util/List;J)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    new-instance v0, Laup;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Laup;-><init>(Ljava/util/List;Lauq;)V

    .line 361
    iget-object v1, v0, Laup;->a:Ljava/util/List;

    iget-object v0, v0, Laup;->b:Lauq;

    invoke-interface {p1, v1}, Lauw;->a(Ljava/util/List;)V

    move v0, v3

    :goto_4
    move v1, v3

    .line 365
    :goto_5
    invoke-interface {p1, v0, v1}, Lauw;->a(ZZ)V

    .line 368
    return-void

    .line 364
    :cond_5
    invoke-interface {p1, v0}, Lauw;->a(Ljava/util/List;)V

    .line 365
    iget-object v0, p0, Laum;->c:Laur;

    .line 366
    invoke-interface {v0}, Laur;->d()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_5

    :cond_6
    move v0, v1

    goto :goto_4

    :cond_7
    move-object v2, v0

    goto/16 :goto_0
.end method

.method static synthetic e(Laum;)Laur;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Laum;->c:Laur;

    return-object v0
.end method

.method static synthetic e(Laum;Lauw;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Laum;->d(Lauw;)V

    return-void
.end method

.method private e(Lauw;)V
    .locals 3

    .prologue
    .line 427
    invoke-virtual {p0}, Laum;->e()V

    .line 428
    iget-object v0, p0, Laum;->b:Lawj;

    invoke-interface {v0}, Lawj;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laum;->b:Lawj;

    .line 429
    invoke-interface {v0}, Lawj;->z()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 430
    :goto_0
    iget-object v1, p0, Laum;->b:Lawj;

    invoke-interface {v1}, Lawj;->w()Lboh;

    move-result-object v1

    iget-object v2, p0, Laum;->b:Lawj;

    invoke-interface {v2}, Lawj;->z()Z

    move-result v2

    invoke-interface {p1, v1, v2, v0}, Lauw;->a(Lboh;ZZ)V

    .line 432
    return-void

    .line 429
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Laum;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Laum;->e()V

    return-void
.end method

.method static synthetic g(Laum;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Laum;->e()V

    return-void
.end method

.method static synthetic h(Laum;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Laum;->e()V

    return-void
.end method

.method static synthetic i(Laum;)Lauy;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Laum;->k:Lauy;

    return-object v0
.end method

.method static synthetic j(Laum;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Laum;->e()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 272
    invoke-super {p0}, Lamn;->a()V

    .line 274
    iget-object v0, p0, Laum;->b:Lawj;

    iget-object v1, p0, Laum;->m:Lawk;

    invoke-interface {v0, v1}, Lawj;->p(Lawk;)V

    .line 275
    iget-object v0, p0, Laum;->b:Lawj;

    iget-object v1, p0, Laum;->o:Lawk;

    invoke-interface {v0, v1}, Lawj;->r(Lawk;)V

    .line 276
    iget-object v0, p0, Laum;->b:Lawj;

    iget-object v1, p0, Laum;->n:Lawk;

    invoke-interface {v0, v1}, Lawj;->v(Lawk;)V

    .line 277
    iget-object v0, p0, Laum;->b:Lawj;

    iget-object v1, p0, Laum;->l:Lawk;

    invoke-interface {v0, v1}, Lawj;->e(Lawk;)V

    .line 280
    iget-object v0, p0, Laum;->b:Lawj;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lawj;->b(Z)V

    .line 282
    iget-object v0, p0, Laum;->p:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 283
    invoke-direct {p0}, Laum;->d()V

    .line 284
    return-void
.end method

.method public a(Lauw;)V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Laum;->p:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 307
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Laum;->k:Lauy;

    invoke-virtual {v0}, Lauy;->a()V

    .line 290
    iget-object v0, p0, Laum;->b:Lawj;

    iget-object v1, p0, Laum;->m:Lawk;

    invoke-interface {v0, v1}, Lawj;->q(Lawk;)V

    .line 291
    iget-object v0, p0, Laum;->b:Lawj;

    iget-object v1, p0, Laum;->o:Lawk;

    invoke-interface {v0, v1}, Lawj;->s(Lawk;)V

    .line 292
    iget-object v0, p0, Laum;->b:Lawj;

    iget-object v1, p0, Laum;->n:Lawk;

    invoke-interface {v0, v1}, Lawj;->w(Lawk;)V

    .line 293
    iget-object v0, p0, Laum;->b:Lawj;

    iget-object v1, p0, Laum;->l:Lawk;

    invoke-interface {v0, v1}, Lawj;->f(Lawk;)V

    .line 294
    iget-object v0, p0, Laum;->p:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 295
    invoke-super {p0}, Lamn;->b()V

    .line 296
    return-void
.end method

.method public b(Lauw;)V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Laum;->p:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 317
    return-void
.end method
