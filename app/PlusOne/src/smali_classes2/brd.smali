.class final Lbrd;
.super Landroid/util/LruCache;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Lboh;",
        "Lbmk;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lbrc;


# direct methods
.method public constructor <init>(Lbrc;)V
    .locals 1

    .prologue
    .line 494
    iput-object p1, p0, Lbrd;->a:Lbrc;

    .line 495
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Landroid/util/LruCache;-><init>(I)V

    .line 496
    return-void
.end method


# virtual methods
.method protected a(Lboh;)Lbmk;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 501
    :try_start_0
    iget-object v1, p0, Lbrd;->a:Lbrc;

    invoke-static {v1, p1}, Lbrc;->a(Lbrc;Lboh;)Lbmk;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbrg; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 508
    :goto_0
    return-object v0

    .line 504
    :catch_0
    move-exception v1

    invoke-static {}, Lbrc;->e()Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not fetch beat info for track "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 506
    :catch_1
    move-exception v1

    .line 507
    invoke-static {}, Lbrc;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x17

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Bad protobuf for track "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected synthetic create(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 492
    check-cast p1, Lboh;

    invoke-virtual {p0, p1}, Lbrd;->a(Lboh;)Lbmk;

    move-result-object v0

    return-object v0
.end method
