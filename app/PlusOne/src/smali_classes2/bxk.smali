.class public final Lbxk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lbmd;

.field public final b:Lbmr;

.field public final c:Lbok;


# direct methods
.method public constructor <init>(Lbmd;Lbmr;Lbok;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, "clip"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iput-object v0, p0, Lbxk;->a:Lbmd;

    .line 44
    const-string v0, "metric"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    iput-object v0, p0, Lbxk;->b:Lbmr;

    .line 45
    const-string v0, "media"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbok;

    iput-object v0, p0, Lbxk;->c:Lbok;

    .line 46
    return-void
.end method

.method public constructor <init>(Lbmr;Lbok;J)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "metric"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    iput-object v0, p0, Lbxk;->b:Lbmr;

    .line 29
    const-string v0, "media"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbok;

    iput-object v0, p0, Lbxk;->c:Lbok;

    .line 30
    invoke-virtual {p2}, Lbok;->b()Lbmv;

    move-result-object v0

    sget-object v1, Lbmv;->a:Lbmv;

    if-ne v0, v1, :cond_0

    .line 31
    invoke-virtual {p1, p2}, Lbmr;->a(Lbok;)Lbmd;

    move-result-object v0

    iput-object v0, p0, Lbxk;->a:Lbmd;

    .line 33
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-virtual {p2}, Lbok;->b()Lbmv;

    move-result-object v0

    sget-object v1, Lbmv;->b:Lbmv;

    if-ne v0, v1, :cond_1

    .line 33
    invoke-static {p2, p3, p4}, Lbpm;->a(Lbok;J)Lbmd;

    move-result-object v0

    iput-object v0, p0, Lbxk;->a:Lbmd;

    goto :goto_0

    .line 35
    :cond_1
    const-string v0, "Only Video and Photo media are supported."

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method
