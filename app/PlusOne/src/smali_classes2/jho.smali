.class final Ljho;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lhei;

.field private final b:Lieh;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ljho;->a:Lhei;

    .line 24
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    iput-object v0, p0, Ljho;->b:Lieh;

    .line 25
    return-void
.end method


# virtual methods
.method a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 29
    iget-object v1, p0, Ljho;->b:Lieh;

    sget-object v2, Ljhj;->a:Lief;

    .line 30
    invoke-interface {v1, v2, p1}, Lieh;->b(Lief;I)Z

    move-result v1

    .line 31
    if-nez v1, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    invoke-virtual {p0, p1}, Ljho;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    invoke-virtual {p0, p1}, Ljho;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    const/4 v0, 0x1

    goto :goto_0
.end method

.method b(I)Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Ljho;->a:Lhei;

    invoke-interface {v0, p1}, Lhei;->c(I)Z

    move-result v0

    .line 52
    if-eqz v0, :cond_0

    iget-object v0, p0, Ljho;->a:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(I)Z
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Ljho;->a:Lhei;

    invoke-interface {v0, p1}, Lhei;->c(I)Z

    move-result v0

    .line 58
    if-eqz v0, :cond_0

    iget-object v0, p0, Ljho;->a:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_managed_account"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
