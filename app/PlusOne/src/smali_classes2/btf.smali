.class public abstract Lbtf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbge;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbgf;

.field private final c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Z

.field private f:I

.field private g:I

.field private h:Z

.field private i:Lbge;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lbtf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbtf;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbgf;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbtf;->c:Ljava/lang/Object;

    .line 49
    const-string v0, "renderContext"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbtf;->b:Lbgf;

    .line 50
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 190
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 191
    :goto_0
    :try_start_0
    iget-object v0, p0, Lbtf;->d:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lbtf;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 126
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    :try_start_0
    iget-boolean v2, p0, Lbtf;->j:Z

    if-eqz v2, :cond_0

    .line 128
    new-instance v0, Lbgo;

    invoke-direct {v0}, Lbgo;-><init>()V

    throw v0

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 131
    :cond_0
    :try_start_1
    iget-boolean v2, p0, Lbtf;->e:Z

    if-eqz v2, :cond_2

    .line 132
    iget-object v2, p0, Lbtf;->i:Lbge;

    .line 133
    const/4 v3, 0x0

    iput-object v3, p0, Lbtf;->i:Lbge;

    .line 134
    if-eqz v2, :cond_1

    .line 135
    new-instance v3, Lbtg;

    invoke-direct {v3, v2}, Lbtg;-><init>(Lbgn;)V

    invoke-interface {v2, v3}, Lbgn;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    .line 145
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lbtf;->e:Z

    .line 148
    :cond_2
    iget-object v2, p0, Lbtf;->i:Lbge;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_3

    .line 150
    :try_start_2
    invoke-direct {p0}, Lbtf;->h()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    :try_start_3
    iget-object v0, p0, Lbtf;->b:Lbgf;

    iget-object v2, p0, Lbtf;->d:Ljava/lang/Object;

    .line 157
    invoke-virtual {p0}, Lbtf;->d()I

    move-result v3

    invoke-virtual {p0}, Lbtf;->e()I

    move-result v4

    const/4 v5, 0x0

    .line 156
    invoke-virtual {v0, v2, v3, v4, v5}, Lbgf;->a(Ljava/lang/Object;IIZ)Lbge;

    move-result-object v0

    iput-object v0, p0, Lbtf;->i:Lbge;

    .line 160
    :cond_3
    iget-object v0, p0, Lbtf;->i:Lbge;

    invoke-interface {v0, p1}, Lbge;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    :goto_0
    return-object v0

    .line 152
    :catch_0
    move-exception v2

    sget-object v2, Lbtf;->a:Ljava/lang/String;

    const-string v3, "Interrupted while waiting for surface"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 154
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 54
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 55
    :try_start_0
    iget-object v0, p0, Lbtf;->i:Lbge;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lbtf;->i:Lbge;

    invoke-interface {v0}, Lbge;->a()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lbtf;->i:Lbge;

    .line 59
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 102
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 103
    :try_start_0
    iget-boolean v0, p0, Lbtf;->h:Z

    if-eqz v0, :cond_0

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbtf;->h:Z

    .line 106
    iget-object v0, p0, Lbtf;->i:Lbge;

    invoke-interface {v0}, Lbge;->c()V

    .line 108
    :cond_0
    iget-object v0, p0, Lbtf;->i:Lbge;

    invoke-interface {v0, p1, p2}, Lbge;->a(J)V

    .line 109
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected a(Ljava/lang/Object;II)V
    .locals 2

    .prologue
    .line 165
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 166
    :try_start_0
    iget-object v0, p0, Lbtf;->d:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    iget v0, p0, Lbtf;->f:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lbtf;->g:I

    if-ne v0, p3, :cond_0

    .line 167
    monitor-exit v1

    .line 177
    :goto_0
    return-void

    .line 170
    :cond_0
    iput-object p1, p0, Lbtf;->d:Ljava/lang/Object;

    .line 171
    iput p2, p0, Lbtf;->f:I

    .line 172
    iput p3, p0, Lbtf;->g:I

    .line 173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbtf;->e:Z

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbtf;->j:Z

    .line 176
    iget-object v0, p0, Lbtf;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 177
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 119
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 120
    :try_start_0
    iget-object v0, p0, Lbtf;->i:Lbge;

    invoke-interface {v0}, Lbge;->b()V

    .line 121
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbtf;->h:Z

    .line 115
    return-void
.end method

.method public d()I
    .locals 2

    .prologue
    .line 64
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 65
    :try_start_0
    iget-boolean v0, p0, Lbtf;->j:Z

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Lbgo;

    invoke-direct {v0}, Lbgo;-><init>()V

    throw v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 69
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lbtf;->h()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    :goto_0
    :try_start_2
    iget v0, p0, Lbtf;->f:I

    monitor-exit v1

    return v0

    .line 71
    :catch_0
    move-exception v0

    sget-object v0, Lbtf;->a:Ljava/lang/String;

    .line 72
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public e()I
    .locals 2

    .prologue
    .line 80
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 81
    :try_start_0
    iget-boolean v0, p0, Lbtf;->j:Z

    if-eqz v0, :cond_0

    .line 82
    new-instance v0, Lbgo;

    invoke-direct {v0}, Lbgo;-><init>()V

    throw v0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 85
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lbtf;->h()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :goto_0
    :try_start_2
    iget v0, p0, Lbtf;->g:I

    monitor-exit v1

    return v0

    .line 87
    :catch_0
    move-exception v0

    sget-object v0, Lbtf;->a:Ljava/lang/String;

    .line 88
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 181
    iget-object v1, p0, Lbtf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 183
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lbtf;->d:Ljava/lang/Object;

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbtf;->e:Z

    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbtf;->j:Z

    .line 186
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
