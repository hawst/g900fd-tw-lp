.class public final Lbuw;
.super Loxu;
.source "PG"


# instance fields
.field private A:Lbuy;

.field public a:F

.field public b:Lbva;

.field public c:Lbvj;

.field public d:[F

.field public e:[F

.field public f:[F

.field public g:[F

.field public h:F

.field public i:F

.field public j:[F

.field public k:Lbvj;

.field public l:F

.field public m:Lbvj;

.field public n:Lbvj;

.field public o:Lbvj;

.field public p:Lbvj;

.field public q:[F

.field public r:[F

.field private s:[F

.field private t:[Lbux;

.field private u:Lbvj;

.field private v:Lbvj;

.field private w:[F

.field private x:[Lbux;

.field private y:[F

.field private z:[F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, 0x7fc00000    # NaNf

    const/4 v1, 0x0

    .line 1858
    invoke-direct {p0}, Loxu;-><init>()V

    .line 2038
    iput v2, p0, Lbuw;->a:F

    .line 2041
    iput-object v1, p0, Lbuw;->b:Lbva;

    .line 2044
    iput-object v1, p0, Lbuw;->c:Lbvj;

    .line 2047
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->d:[F

    .line 2050
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->e:[F

    .line 2053
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->f:[F

    .line 2056
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->g:[F

    .line 2059
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->s:[F

    .line 2062
    sget-object v0, Lbux;->a:[Lbux;

    iput-object v0, p0, Lbuw;->t:[Lbux;

    .line 2065
    iput v2, p0, Lbuw;->h:F

    .line 2068
    iput v2, p0, Lbuw;->i:F

    .line 2071
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->j:[F

    .line 2074
    iput-object v1, p0, Lbuw;->k:Lbvj;

    .line 2077
    iput v2, p0, Lbuw;->l:F

    .line 2080
    iput-object v1, p0, Lbuw;->u:Lbvj;

    .line 2083
    iput-object v1, p0, Lbuw;->v:Lbvj;

    .line 2086
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->w:[F

    .line 2089
    sget-object v0, Lbux;->a:[Lbux;

    iput-object v0, p0, Lbuw;->x:[Lbux;

    .line 2092
    iput-object v1, p0, Lbuw;->m:Lbvj;

    .line 2095
    iput-object v1, p0, Lbuw;->n:Lbvj;

    .line 2098
    iput-object v1, p0, Lbuw;->o:Lbvj;

    .line 2101
    iput-object v1, p0, Lbuw;->p:Lbvj;

    .line 2104
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->y:[F

    .line 2107
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->z:[F

    .line 2110
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->q:[F

    .line 2113
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbuw;->r:[F

    .line 2116
    iput-object v1, p0, Lbuw;->A:Lbuy;

    .line 1858
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2268
    .line 2269
    iget v0, p0, Lbuw;->a:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 2270
    const/4 v0, 0x1

    iget v2, p0, Lbuw;->a:F

    .line 2271
    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 2273
    :goto_0
    iget-object v2, p0, Lbuw;->b:Lbva;

    if-eqz v2, :cond_0

    .line 2274
    const/4 v2, 0x2

    iget-object v3, p0, Lbuw;->b:Lbva;

    .line 2275
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2277
    :cond_0
    iget-object v2, p0, Lbuw;->c:Lbvj;

    if-eqz v2, :cond_1

    .line 2278
    const/4 v2, 0x3

    iget-object v3, p0, Lbuw;->c:Lbvj;

    .line 2279
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2281
    :cond_1
    iget-object v2, p0, Lbuw;->d:[F

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbuw;->d:[F

    array-length v2, v2

    if-lez v2, :cond_2

    .line 2282
    iget-object v2, p0, Lbuw;->d:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    .line 2283
    add-int/2addr v0, v2

    .line 2284
    iget-object v2, p0, Lbuw;->d:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2286
    :cond_2
    iget-object v2, p0, Lbuw;->e:[F

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbuw;->e:[F

    array-length v2, v2

    if-lez v2, :cond_3

    .line 2287
    iget-object v2, p0, Lbuw;->e:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    .line 2288
    add-int/2addr v0, v2

    .line 2289
    iget-object v2, p0, Lbuw;->e:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2291
    :cond_3
    iget-object v2, p0, Lbuw;->f:[F

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbuw;->f:[F

    array-length v2, v2

    if-lez v2, :cond_4

    .line 2292
    iget-object v2, p0, Lbuw;->f:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    .line 2293
    add-int/2addr v0, v2

    .line 2294
    iget-object v2, p0, Lbuw;->f:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2296
    :cond_4
    iget-object v2, p0, Lbuw;->g:[F

    if-eqz v2, :cond_5

    iget-object v2, p0, Lbuw;->g:[F

    array-length v2, v2

    if-lez v2, :cond_5

    .line 2297
    iget-object v2, p0, Lbuw;->g:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    .line 2298
    add-int/2addr v0, v2

    .line 2299
    iget-object v2, p0, Lbuw;->g:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2301
    :cond_5
    iget-object v2, p0, Lbuw;->s:[F

    if-eqz v2, :cond_6

    iget-object v2, p0, Lbuw;->s:[F

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2302
    iget-object v2, p0, Lbuw;->s:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    .line 2303
    add-int/2addr v0, v2

    .line 2304
    iget-object v2, p0, Lbuw;->s:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2306
    :cond_6
    iget-object v2, p0, Lbuw;->t:[Lbux;

    if-eqz v2, :cond_8

    .line 2307
    iget-object v3, p0, Lbuw;->t:[Lbux;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_8

    aget-object v5, v3, v2

    .line 2308
    if-eqz v5, :cond_7

    .line 2309
    const/16 v6, 0x9

    .line 2310
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2307
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2314
    :cond_8
    iget v2, p0, Lbuw;->h:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_9

    .line 2315
    const/16 v2, 0xa

    iget v3, p0, Lbuw;->h:F

    .line 2316
    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 2318
    :cond_9
    iget v2, p0, Lbuw;->i:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_a

    .line 2319
    const/16 v2, 0xb

    iget v3, p0, Lbuw;->i:F

    .line 2320
    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 2322
    :cond_a
    iget-object v2, p0, Lbuw;->j:[F

    if-eqz v2, :cond_b

    iget-object v2, p0, Lbuw;->j:[F

    array-length v2, v2

    if-lez v2, :cond_b

    .line 2323
    iget-object v2, p0, Lbuw;->j:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    .line 2324
    add-int/2addr v0, v2

    .line 2325
    iget-object v2, p0, Lbuw;->j:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2327
    :cond_b
    iget-object v2, p0, Lbuw;->k:Lbvj;

    if-eqz v2, :cond_c

    .line 2328
    const/16 v2, 0xd

    iget-object v3, p0, Lbuw;->k:Lbvj;

    .line 2329
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2331
    :cond_c
    iget v2, p0, Lbuw;->l:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_d

    .line 2332
    const/16 v2, 0xe

    iget v3, p0, Lbuw;->l:F

    .line 2333
    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 2335
    :cond_d
    iget-object v2, p0, Lbuw;->u:Lbvj;

    if-eqz v2, :cond_e

    .line 2336
    const/16 v2, 0xf

    iget-object v3, p0, Lbuw;->u:Lbvj;

    .line 2337
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2339
    :cond_e
    iget-object v2, p0, Lbuw;->v:Lbvj;

    if-eqz v2, :cond_f

    .line 2340
    const/16 v2, 0x10

    iget-object v3, p0, Lbuw;->v:Lbvj;

    .line 2341
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2343
    :cond_f
    iget-object v2, p0, Lbuw;->w:[F

    if-eqz v2, :cond_10

    iget-object v2, p0, Lbuw;->w:[F

    array-length v2, v2

    if-lez v2, :cond_10

    .line 2344
    iget-object v2, p0, Lbuw;->w:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x4

    .line 2345
    add-int/2addr v0, v2

    .line 2346
    iget-object v2, p0, Lbuw;->w:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 2348
    :cond_10
    iget-object v2, p0, Lbuw;->x:[Lbux;

    if-eqz v2, :cond_12

    .line 2349
    iget-object v2, p0, Lbuw;->x:[Lbux;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_12

    aget-object v4, v2, v1

    .line 2350
    if-eqz v4, :cond_11

    .line 2351
    const/16 v5, 0x12

    .line 2352
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2349
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2356
    :cond_12
    iget-object v1, p0, Lbuw;->m:Lbvj;

    if-eqz v1, :cond_13

    .line 2357
    const/16 v1, 0x13

    iget-object v2, p0, Lbuw;->m:Lbvj;

    .line 2358
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2360
    :cond_13
    iget-object v1, p0, Lbuw;->n:Lbvj;

    if-eqz v1, :cond_14

    .line 2361
    const/16 v1, 0x14

    iget-object v2, p0, Lbuw;->n:Lbvj;

    .line 2362
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2364
    :cond_14
    iget-object v1, p0, Lbuw;->o:Lbvj;

    if-eqz v1, :cond_15

    .line 2365
    const/16 v1, 0x15

    iget-object v2, p0, Lbuw;->o:Lbvj;

    .line 2366
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2368
    :cond_15
    iget-object v1, p0, Lbuw;->p:Lbvj;

    if-eqz v1, :cond_16

    .line 2369
    const/16 v1, 0x16

    iget-object v2, p0, Lbuw;->p:Lbvj;

    .line 2370
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2372
    :cond_16
    iget-object v1, p0, Lbuw;->y:[F

    if-eqz v1, :cond_17

    iget-object v1, p0, Lbuw;->y:[F

    array-length v1, v1

    if-lez v1, :cond_17

    .line 2373
    iget-object v1, p0, Lbuw;->y:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 2374
    add-int/2addr v0, v1

    .line 2375
    iget-object v1, p0, Lbuw;->y:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 2377
    :cond_17
    iget-object v1, p0, Lbuw;->z:[F

    if-eqz v1, :cond_18

    iget-object v1, p0, Lbuw;->z:[F

    array-length v1, v1

    if-lez v1, :cond_18

    .line 2378
    iget-object v1, p0, Lbuw;->z:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 2379
    add-int/2addr v0, v1

    .line 2380
    iget-object v1, p0, Lbuw;->z:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 2382
    :cond_18
    iget-object v1, p0, Lbuw;->q:[F

    if-eqz v1, :cond_19

    iget-object v1, p0, Lbuw;->q:[F

    array-length v1, v1

    if-lez v1, :cond_19

    .line 2383
    iget-object v1, p0, Lbuw;->q:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 2384
    add-int/2addr v0, v1

    .line 2385
    iget-object v1, p0, Lbuw;->q:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 2387
    :cond_19
    iget-object v1, p0, Lbuw;->r:[F

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lbuw;->r:[F

    array-length v1, v1

    if-lez v1, :cond_1a

    .line 2388
    iget-object v1, p0, Lbuw;->r:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 2389
    add-int/2addr v0, v1

    .line 2390
    iget-object v1, p0, Lbuw;->r:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 2392
    :cond_1a
    iget-object v1, p0, Lbuw;->A:Lbuy;

    if-eqz v1, :cond_1b

    .line 2393
    const/16 v1, 0x1b

    iget-object v2, p0, Lbuw;->A:Lbuy;

    .line 2394
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2396
    :cond_1b
    iput v0, p0, Lbuw;->ai:I

    .line 2397
    return v0

    :cond_1c
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lbuw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2405
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2406
    sparse-switch v0, :sswitch_data_0

    .line 2410
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2411
    :sswitch_0
    return-object p0

    .line 2416
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbuw;->a:F

    goto :goto_0

    .line 2420
    :sswitch_2
    iget-object v0, p0, Lbuw;->b:Lbva;

    if-nez v0, :cond_1

    .line 2421
    new-instance v0, Lbva;

    invoke-direct {v0}, Lbva;-><init>()V

    iput-object v0, p0, Lbuw;->b:Lbva;

    .line 2423
    :cond_1
    iget-object v0, p0, Lbuw;->b:Lbva;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2427
    :sswitch_3
    iget-object v0, p0, Lbuw;->c:Lbvj;

    if-nez v0, :cond_2

    .line 2428
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbuw;->c:Lbvj;

    .line 2430
    :cond_2
    iget-object v0, p0, Lbuw;->c:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2434
    :sswitch_4
    const/16 v0, 0x25

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2435
    iget-object v0, p0, Lbuw;->d:[F

    array-length v0, v0

    .line 2436
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2437
    iget-object v3, p0, Lbuw;->d:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2438
    iput-object v2, p0, Lbuw;->d:[F

    .line 2439
    :goto_1
    iget-object v2, p0, Lbuw;->d:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 2440
    iget-object v2, p0, Lbuw;->d:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2441
    invoke-virtual {p1}, Loxn;->a()I

    .line 2439
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2444
    :cond_3
    iget-object v2, p0, Lbuw;->d:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto :goto_0

    .line 2448
    :sswitch_5
    const/16 v0, 0x2d

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2449
    iget-object v0, p0, Lbuw;->e:[F

    array-length v0, v0

    .line 2450
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2451
    iget-object v3, p0, Lbuw;->e:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2452
    iput-object v2, p0, Lbuw;->e:[F

    .line 2453
    :goto_2
    iget-object v2, p0, Lbuw;->e:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2454
    iget-object v2, p0, Lbuw;->e:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2455
    invoke-virtual {p1}, Loxn;->a()I

    .line 2453
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2458
    :cond_4
    iget-object v2, p0, Lbuw;->e:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2462
    :sswitch_6
    const/16 v0, 0x35

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2463
    iget-object v0, p0, Lbuw;->f:[F

    array-length v0, v0

    .line 2464
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2465
    iget-object v3, p0, Lbuw;->f:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2466
    iput-object v2, p0, Lbuw;->f:[F

    .line 2467
    :goto_3
    iget-object v2, p0, Lbuw;->f:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2468
    iget-object v2, p0, Lbuw;->f:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2469
    invoke-virtual {p1}, Loxn;->a()I

    .line 2467
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2472
    :cond_5
    iget-object v2, p0, Lbuw;->f:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2476
    :sswitch_7
    const/16 v0, 0x3d

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2477
    iget-object v0, p0, Lbuw;->g:[F

    array-length v0, v0

    .line 2478
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2479
    iget-object v3, p0, Lbuw;->g:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2480
    iput-object v2, p0, Lbuw;->g:[F

    .line 2481
    :goto_4
    iget-object v2, p0, Lbuw;->g:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 2482
    iget-object v2, p0, Lbuw;->g:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2483
    invoke-virtual {p1}, Loxn;->a()I

    .line 2481
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2486
    :cond_6
    iget-object v2, p0, Lbuw;->g:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2490
    :sswitch_8
    const/16 v0, 0x45

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2491
    iget-object v0, p0, Lbuw;->s:[F

    array-length v0, v0

    .line 2492
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2493
    iget-object v3, p0, Lbuw;->s:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2494
    iput-object v2, p0, Lbuw;->s:[F

    .line 2495
    :goto_5
    iget-object v2, p0, Lbuw;->s:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 2496
    iget-object v2, p0, Lbuw;->s:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2497
    invoke-virtual {p1}, Loxn;->a()I

    .line 2495
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2500
    :cond_7
    iget-object v2, p0, Lbuw;->s:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2504
    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2505
    iget-object v0, p0, Lbuw;->t:[Lbux;

    if-nez v0, :cond_9

    move v0, v1

    .line 2506
    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lbux;

    .line 2507
    iget-object v3, p0, Lbuw;->t:[Lbux;

    if-eqz v3, :cond_8

    .line 2508
    iget-object v3, p0, Lbuw;->t:[Lbux;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2510
    :cond_8
    iput-object v2, p0, Lbuw;->t:[Lbux;

    .line 2511
    :goto_7
    iget-object v2, p0, Lbuw;->t:[Lbux;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 2512
    iget-object v2, p0, Lbuw;->t:[Lbux;

    new-instance v3, Lbux;

    invoke-direct {v3}, Lbux;-><init>()V

    aput-object v3, v2, v0

    .line 2513
    iget-object v2, p0, Lbuw;->t:[Lbux;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2514
    invoke-virtual {p1}, Loxn;->a()I

    .line 2511
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 2505
    :cond_9
    iget-object v0, p0, Lbuw;->t:[Lbux;

    array-length v0, v0

    goto :goto_6

    .line 2517
    :cond_a
    iget-object v2, p0, Lbuw;->t:[Lbux;

    new-instance v3, Lbux;

    invoke-direct {v3}, Lbux;-><init>()V

    aput-object v3, v2, v0

    .line 2518
    iget-object v2, p0, Lbuw;->t:[Lbux;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2522
    :sswitch_a
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbuw;->h:F

    goto/16 :goto_0

    .line 2526
    :sswitch_b
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbuw;->i:F

    goto/16 :goto_0

    .line 2530
    :sswitch_c
    const/16 v0, 0x65

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2531
    iget-object v0, p0, Lbuw;->j:[F

    array-length v0, v0

    .line 2532
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2533
    iget-object v3, p0, Lbuw;->j:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2534
    iput-object v2, p0, Lbuw;->j:[F

    .line 2535
    :goto_8
    iget-object v2, p0, Lbuw;->j:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 2536
    iget-object v2, p0, Lbuw;->j:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2537
    invoke-virtual {p1}, Loxn;->a()I

    .line 2535
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 2540
    :cond_b
    iget-object v2, p0, Lbuw;->j:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2544
    :sswitch_d
    iget-object v0, p0, Lbuw;->k:Lbvj;

    if-nez v0, :cond_c

    .line 2545
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbuw;->k:Lbvj;

    .line 2547
    :cond_c
    iget-object v0, p0, Lbuw;->k:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2551
    :sswitch_e
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbuw;->l:F

    goto/16 :goto_0

    .line 2555
    :sswitch_f
    iget-object v0, p0, Lbuw;->u:Lbvj;

    if-nez v0, :cond_d

    .line 2556
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbuw;->u:Lbvj;

    .line 2558
    :cond_d
    iget-object v0, p0, Lbuw;->u:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2562
    :sswitch_10
    iget-object v0, p0, Lbuw;->v:Lbvj;

    if-nez v0, :cond_e

    .line 2563
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbuw;->v:Lbvj;

    .line 2565
    :cond_e
    iget-object v0, p0, Lbuw;->v:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2569
    :sswitch_11
    const/16 v0, 0x8d

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2570
    iget-object v0, p0, Lbuw;->w:[F

    array-length v0, v0

    .line 2571
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2572
    iget-object v3, p0, Lbuw;->w:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2573
    iput-object v2, p0, Lbuw;->w:[F

    .line 2574
    :goto_9
    iget-object v2, p0, Lbuw;->w:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    .line 2575
    iget-object v2, p0, Lbuw;->w:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2576
    invoke-virtual {p1}, Loxn;->a()I

    .line 2574
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 2579
    :cond_f
    iget-object v2, p0, Lbuw;->w:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2583
    :sswitch_12
    const/16 v0, 0x92

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2584
    iget-object v0, p0, Lbuw;->x:[Lbux;

    if-nez v0, :cond_11

    move v0, v1

    .line 2585
    :goto_a
    add-int/2addr v2, v0

    new-array v2, v2, [Lbux;

    .line 2586
    iget-object v3, p0, Lbuw;->x:[Lbux;

    if-eqz v3, :cond_10

    .line 2587
    iget-object v3, p0, Lbuw;->x:[Lbux;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2589
    :cond_10
    iput-object v2, p0, Lbuw;->x:[Lbux;

    .line 2590
    :goto_b
    iget-object v2, p0, Lbuw;->x:[Lbux;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_12

    .line 2591
    iget-object v2, p0, Lbuw;->x:[Lbux;

    new-instance v3, Lbux;

    invoke-direct {v3}, Lbux;-><init>()V

    aput-object v3, v2, v0

    .line 2592
    iget-object v2, p0, Lbuw;->x:[Lbux;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2593
    invoke-virtual {p1}, Loxn;->a()I

    .line 2590
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 2584
    :cond_11
    iget-object v0, p0, Lbuw;->x:[Lbux;

    array-length v0, v0

    goto :goto_a

    .line 2596
    :cond_12
    iget-object v2, p0, Lbuw;->x:[Lbux;

    new-instance v3, Lbux;

    invoke-direct {v3}, Lbux;-><init>()V

    aput-object v3, v2, v0

    .line 2597
    iget-object v2, p0, Lbuw;->x:[Lbux;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2601
    :sswitch_13
    iget-object v0, p0, Lbuw;->m:Lbvj;

    if-nez v0, :cond_13

    .line 2602
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbuw;->m:Lbvj;

    .line 2604
    :cond_13
    iget-object v0, p0, Lbuw;->m:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2608
    :sswitch_14
    iget-object v0, p0, Lbuw;->n:Lbvj;

    if-nez v0, :cond_14

    .line 2609
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbuw;->n:Lbvj;

    .line 2611
    :cond_14
    iget-object v0, p0, Lbuw;->n:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2615
    :sswitch_15
    iget-object v0, p0, Lbuw;->o:Lbvj;

    if-nez v0, :cond_15

    .line 2616
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbuw;->o:Lbvj;

    .line 2618
    :cond_15
    iget-object v0, p0, Lbuw;->o:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2622
    :sswitch_16
    iget-object v0, p0, Lbuw;->p:Lbvj;

    if-nez v0, :cond_16

    .line 2623
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbuw;->p:Lbvj;

    .line 2625
    :cond_16
    iget-object v0, p0, Lbuw;->p:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2629
    :sswitch_17
    const/16 v0, 0xbd

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2630
    iget-object v0, p0, Lbuw;->y:[F

    array-length v0, v0

    .line 2631
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2632
    iget-object v3, p0, Lbuw;->y:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2633
    iput-object v2, p0, Lbuw;->y:[F

    .line 2634
    :goto_c
    iget-object v2, p0, Lbuw;->y:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_17

    .line 2635
    iget-object v2, p0, Lbuw;->y:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2636
    invoke-virtual {p1}, Loxn;->a()I

    .line 2634
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 2639
    :cond_17
    iget-object v2, p0, Lbuw;->y:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2643
    :sswitch_18
    const/16 v0, 0xc5

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2644
    iget-object v0, p0, Lbuw;->z:[F

    array-length v0, v0

    .line 2645
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2646
    iget-object v3, p0, Lbuw;->z:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2647
    iput-object v2, p0, Lbuw;->z:[F

    .line 2648
    :goto_d
    iget-object v2, p0, Lbuw;->z:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_18

    .line 2649
    iget-object v2, p0, Lbuw;->z:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2650
    invoke-virtual {p1}, Loxn;->a()I

    .line 2648
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 2653
    :cond_18
    iget-object v2, p0, Lbuw;->z:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2657
    :sswitch_19
    const/16 v0, 0xcd

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2658
    iget-object v0, p0, Lbuw;->q:[F

    array-length v0, v0

    .line 2659
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2660
    iget-object v3, p0, Lbuw;->q:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2661
    iput-object v2, p0, Lbuw;->q:[F

    .line 2662
    :goto_e
    iget-object v2, p0, Lbuw;->q:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_19

    .line 2663
    iget-object v2, p0, Lbuw;->q:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2664
    invoke-virtual {p1}, Loxn;->a()I

    .line 2662
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 2667
    :cond_19
    iget-object v2, p0, Lbuw;->q:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2671
    :sswitch_1a
    const/16 v0, 0xd5

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2672
    iget-object v0, p0, Lbuw;->r:[F

    array-length v0, v0

    .line 2673
    add-int/2addr v2, v0

    new-array v2, v2, [F

    .line 2674
    iget-object v3, p0, Lbuw;->r:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2675
    iput-object v2, p0, Lbuw;->r:[F

    .line 2676
    :goto_f
    iget-object v2, p0, Lbuw;->r:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1a

    .line 2677
    iget-object v2, p0, Lbuw;->r:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    .line 2678
    invoke-virtual {p1}, Loxn;->a()I

    .line 2676
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 2681
    :cond_1a
    iget-object v2, p0, Lbuw;->r:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 2685
    :sswitch_1b
    iget-object v0, p0, Lbuw;->A:Lbuy;

    if-nez v0, :cond_1b

    .line 2686
    new-instance v0, Lbuy;

    invoke-direct {v0}, Lbuy;-><init>()V

    iput-object v0, p0, Lbuw;->A:Lbuy;

    .line 2688
    :cond_1b
    iget-object v0, p0, Lbuw;->A:Lbuy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2406
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x4a -> :sswitch_9
        0x55 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6a -> :sswitch_d
        0x75 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8d -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xbd -> :sswitch_17
        0xc5 -> :sswitch_18
        0xcd -> :sswitch_19
        0xd5 -> :sswitch_1a
        0xda -> :sswitch_1b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2153
    iget v1, p0, Lbuw;->a:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2154
    const/4 v1, 0x1

    iget v2, p0, Lbuw;->a:F

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 2156
    :cond_0
    iget-object v1, p0, Lbuw;->b:Lbva;

    if-eqz v1, :cond_1

    .line 2157
    const/4 v1, 0x2

    iget-object v2, p0, Lbuw;->b:Lbva;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2159
    :cond_1
    iget-object v1, p0, Lbuw;->c:Lbvj;

    if-eqz v1, :cond_2

    .line 2160
    const/4 v1, 0x3

    iget-object v2, p0, Lbuw;->c:Lbvj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2162
    :cond_2
    iget-object v1, p0, Lbuw;->d:[F

    if-eqz v1, :cond_3

    .line 2163
    iget-object v2, p0, Lbuw;->d:[F

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget v4, v2, v1

    .line 2164
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2163
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2167
    :cond_3
    iget-object v1, p0, Lbuw;->e:[F

    if-eqz v1, :cond_4

    .line 2168
    iget-object v2, p0, Lbuw;->e:[F

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget v4, v2, v1

    .line 2169
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2168
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2172
    :cond_4
    iget-object v1, p0, Lbuw;->f:[F

    if-eqz v1, :cond_5

    .line 2173
    iget-object v2, p0, Lbuw;->f:[F

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    aget v4, v2, v1

    .line 2174
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2173
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2177
    :cond_5
    iget-object v1, p0, Lbuw;->g:[F

    if-eqz v1, :cond_6

    .line 2178
    iget-object v2, p0, Lbuw;->g:[F

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_6

    aget v4, v2, v1

    .line 2179
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2178
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2182
    :cond_6
    iget-object v1, p0, Lbuw;->s:[F

    if-eqz v1, :cond_7

    .line 2183
    iget-object v2, p0, Lbuw;->s:[F

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_7

    aget v4, v2, v1

    .line 2184
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2183
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2187
    :cond_7
    iget-object v1, p0, Lbuw;->t:[Lbux;

    if-eqz v1, :cond_9

    .line 2188
    iget-object v2, p0, Lbuw;->t:[Lbux;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 2189
    if-eqz v4, :cond_8

    .line 2190
    const/16 v5, 0x9

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2188
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 2194
    :cond_9
    iget v1, p0, Lbuw;->h:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_a

    .line 2195
    const/16 v1, 0xa

    iget v2, p0, Lbuw;->h:F

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 2197
    :cond_a
    iget v1, p0, Lbuw;->i:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_b

    .line 2198
    const/16 v1, 0xb

    iget v2, p0, Lbuw;->i:F

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 2200
    :cond_b
    iget-object v1, p0, Lbuw;->j:[F

    if-eqz v1, :cond_c

    .line 2201
    iget-object v2, p0, Lbuw;->j:[F

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_c

    aget v4, v2, v1

    .line 2202
    const/16 v5, 0xc

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2201
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2205
    :cond_c
    iget-object v1, p0, Lbuw;->k:Lbvj;

    if-eqz v1, :cond_d

    .line 2206
    const/16 v1, 0xd

    iget-object v2, p0, Lbuw;->k:Lbvj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2208
    :cond_d
    iget v1, p0, Lbuw;->l:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_e

    .line 2209
    const/16 v1, 0xe

    iget v2, p0, Lbuw;->l:F

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 2211
    :cond_e
    iget-object v1, p0, Lbuw;->u:Lbvj;

    if-eqz v1, :cond_f

    .line 2212
    const/16 v1, 0xf

    iget-object v2, p0, Lbuw;->u:Lbvj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2214
    :cond_f
    iget-object v1, p0, Lbuw;->v:Lbvj;

    if-eqz v1, :cond_10

    .line 2215
    const/16 v1, 0x10

    iget-object v2, p0, Lbuw;->v:Lbvj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2217
    :cond_10
    iget-object v1, p0, Lbuw;->w:[F

    if-eqz v1, :cond_11

    .line 2218
    iget-object v2, p0, Lbuw;->w:[F

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_11

    aget v4, v2, v1

    .line 2219
    const/16 v5, 0x11

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2218
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 2222
    :cond_11
    iget-object v1, p0, Lbuw;->x:[Lbux;

    if-eqz v1, :cond_13

    .line 2223
    iget-object v2, p0, Lbuw;->x:[Lbux;

    array-length v3, v2

    move v1, v0

    :goto_8
    if-ge v1, v3, :cond_13

    aget-object v4, v2, v1

    .line 2224
    if-eqz v4, :cond_12

    .line 2225
    const/16 v5, 0x12

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2223
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 2229
    :cond_13
    iget-object v1, p0, Lbuw;->m:Lbvj;

    if-eqz v1, :cond_14

    .line 2230
    const/16 v1, 0x13

    iget-object v2, p0, Lbuw;->m:Lbvj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2232
    :cond_14
    iget-object v1, p0, Lbuw;->n:Lbvj;

    if-eqz v1, :cond_15

    .line 2233
    const/16 v1, 0x14

    iget-object v2, p0, Lbuw;->n:Lbvj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2235
    :cond_15
    iget-object v1, p0, Lbuw;->o:Lbvj;

    if-eqz v1, :cond_16

    .line 2236
    const/16 v1, 0x15

    iget-object v2, p0, Lbuw;->o:Lbvj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2238
    :cond_16
    iget-object v1, p0, Lbuw;->p:Lbvj;

    if-eqz v1, :cond_17

    .line 2239
    const/16 v1, 0x16

    iget-object v2, p0, Lbuw;->p:Lbvj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2241
    :cond_17
    iget-object v1, p0, Lbuw;->y:[F

    if-eqz v1, :cond_18

    .line 2242
    iget-object v2, p0, Lbuw;->y:[F

    array-length v3, v2

    move v1, v0

    :goto_9
    if-ge v1, v3, :cond_18

    aget v4, v2, v1

    .line 2243
    const/16 v5, 0x17

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2242
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 2246
    :cond_18
    iget-object v1, p0, Lbuw;->z:[F

    if-eqz v1, :cond_19

    .line 2247
    iget-object v2, p0, Lbuw;->z:[F

    array-length v3, v2

    move v1, v0

    :goto_a
    if-ge v1, v3, :cond_19

    aget v4, v2, v1

    .line 2248
    const/16 v5, 0x18

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2247
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 2251
    :cond_19
    iget-object v1, p0, Lbuw;->q:[F

    if-eqz v1, :cond_1a

    .line 2252
    iget-object v2, p0, Lbuw;->q:[F

    array-length v3, v2

    move v1, v0

    :goto_b
    if-ge v1, v3, :cond_1a

    aget v4, v2, v1

    .line 2253
    const/16 v5, 0x19

    invoke-virtual {p1, v5, v4}, Loxo;->a(IF)V

    .line 2252
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 2256
    :cond_1a
    iget-object v1, p0, Lbuw;->r:[F

    if-eqz v1, :cond_1b

    .line 2257
    iget-object v1, p0, Lbuw;->r:[F

    array-length v2, v1

    :goto_c
    if-ge v0, v2, :cond_1b

    aget v3, v1, v0

    .line 2258
    const/16 v4, 0x1a

    invoke-virtual {p1, v4, v3}, Loxo;->a(IF)V

    .line 2257
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 2261
    :cond_1b
    iget-object v0, p0, Lbuw;->A:Lbuy;

    if-eqz v0, :cond_1c

    .line 2262
    const/16 v0, 0x1b

    iget-object v1, p0, Lbuw;->A:Lbuy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2264
    :cond_1c
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1854
    invoke-virtual {p0, p1}, Lbuw;->a(Loxn;)Lbuw;

    move-result-object v0

    return-object v0
.end method
