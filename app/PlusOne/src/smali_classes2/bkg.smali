.class final Lbkg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbkr;


# static fields
.field private static f:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Lbkp;",
            ">;"
        }
    .end annotation
.end field

.field private static g:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Lbku;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Lajl;

.field private c:Lbkn;

.field private d:Z

.field private e:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lbkh;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Lbkh;-><init>(I)V

    sput-object v0, Lbkg;->f:Landroid/util/LruCache;

    .line 47
    new-instance v0, Lbki;

    const v1, 0x8ca0

    invoke-direct {v0, v1}, Lbki;-><init>(I)V

    sput-object v0, Lbkg;->g:Landroid/util/LruCache;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lajl;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lbkg;->a:Landroid/net/Uri;

    .line 59
    iput-object p2, p0, Lbkg;->b:Lajl;

    .line 60
    return-void
.end method

.method private declared-synchronized f()Lbvf;
    .locals 2

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbkg;->b:Lajl;

    iget-object v1, p0, Lbkg;->a:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lajl;->a(Landroid/net/Uri;)Lbvf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lbkg;->d:Z

    if-nez v0, :cond_0

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbkg;->d:Z

    .line 136
    invoke-direct {p0}, Lbkg;->f()Lbvf;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    iget-object v0, v0, Lbvf;->h:[B

    iput-object v0, p0, Lbkg;->e:[B

    .line 141
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    sget-object v0, Lbkg;->f:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lbkg;->g:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lbkg;->g()V

    .line 66
    iget-object v0, p0, Lbkg;->e:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()[B
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lbkg;->g()V

    .line 73
    iget-object v0, p0, Lbkg;->e:[B

    return-object v0
.end method

.method public declared-synchronized c()Lbkn;
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 78
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lbkg;->c:Lbkn;

    if-nez v1, :cond_1

    .line 80
    invoke-direct {p0}, Lbkg;->f()Lbvf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 81
    if-nez v9, :cond_0

    .line 86
    :goto_0
    monitor-exit p0

    return-object v0

    .line 84
    :cond_0
    :try_start_1
    new-instance v1, Lbkn;

    invoke-static {v9}, Lbkn;->b(Lbvf;)J

    move-result-wide v2

    iget-wide v4, v9, Lbvf;->b:J

    iget-wide v6, v9, Lbvf;->g:J

    iget-object v8, v9, Lbvf;->e:Lbuz;

    if-nez v8, :cond_2

    move-object v8, v0

    :goto_1
    iget-boolean v9, v9, Lbvf;->f:Z

    invoke-direct/range {v1 .. v9}, Lbkn;-><init>(JJJLbbs;Z)V

    iput-object v1, p0, Lbkg;->c:Lbkn;

    .line 86
    :cond_1
    iget-object v0, p0, Lbkg;->c:Lbkn;

    goto :goto_0

    .line 84
    :cond_2
    iget-object v0, v9, Lbvf;->e:Lbuz;

    iget-object v8, v0, Lbuz;->b:[B

    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v11, 0x0

    iput-boolean v11, v10, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    const/4 v11, 0x0

    array-length v12, v8

    invoke-static {v8, v11, v12, v10}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v10

    new-instance v8, Lbbs;

    iget v0, v0, Lbuz;->a:F

    invoke-direct {v8, v10, v0}, Lbbs;-><init>(Landroid/graphics/Bitmap;F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Lbku;
    .locals 3

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    sget-object v0, Lbkg;->g:Landroid/util/LruCache;

    iget-object v1, p0, Lbkg;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbku;

    .line 93
    if-nez v0, :cond_0

    .line 94
    invoke-direct {p0}, Lbkg;->f()Lbvf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 95
    if-nez v1, :cond_1

    .line 96
    const/4 v0, 0x0

    .line 101
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 98
    :cond_1
    :try_start_1
    new-instance v0, Lbku;

    invoke-direct {v0, v1}, Lbku;-><init>(Lbvf;)V

    .line 99
    sget-object v1, Lbkg;->g:Landroid/util/LruCache;

    iget-object v2, p0, Lbkg;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()Lbkp;
    .locals 3

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    sget-object v0, Lbkg;->f:Landroid/util/LruCache;

    iget-object v1, p0, Lbkg;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkp;

    .line 108
    if-nez v0, :cond_0

    .line 109
    invoke-direct {p0}, Lbkg;->f()Lbvf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 110
    if-nez v1, :cond_1

    .line 111
    const/4 v0, 0x0

    .line 116
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 113
    :cond_1
    :try_start_1
    new-instance v0, Lbkp;

    invoke-direct {v0, v1}, Lbkp;-><init>(Lbvf;)V

    .line 114
    sget-object v1, Lbkg;->f:Landroid/util/LruCache;

    iget-object v2, p0, Lbkg;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
