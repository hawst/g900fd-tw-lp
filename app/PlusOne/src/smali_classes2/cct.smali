.class public final Lcct;
.super Landroid/util/Property;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Lcom/google/android/apps/moviemaker/ui/VideoPosterView;",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 655
    invoke-direct {p0, p1, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 666
    invoke-static {p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Ljava/lang/Float;)V
    .locals 2

    .prologue
    .line 658
    invoke-static {p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)F

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 659
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;F)F

    .line 660
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 662
    :cond_0
    return-void
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 655
    check-cast p1, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {p0, p1}, Lcct;->a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 655
    check-cast p1, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcct;->a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Ljava/lang/Float;)V

    return-void
.end method
