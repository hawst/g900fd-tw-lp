.class public final Lbon;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbml;


# instance fields
.field private final a:Ljeg;

.field private final b:Lboo;

.field private final c:Lbol;

.field private final d:Lbmy;

.field private final e:Lbms;

.field private final f:J

.field private final g:J

.field private final h:Z


# direct methods
.method public constructor <init>(Ljeg;Lboo;Lbkr;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "mediaIdentifier"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeg;

    iput-object v0, p0, Lbon;->a:Ljeg;

    .line 35
    const-string v0, "metadata"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboo;

    iput-object v0, p0, Lbon;->b:Lboo;

    .line 36
    const-string v0, "metrics"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 37
    invoke-static {p3}, Lbol;->a(Lbkr;)Lbol;

    move-result-object v0

    iput-object v0, p0, Lbon;->c:Lbol;

    .line 38
    invoke-static {p3}, Lbmy;->a(Lbkr;)Lbmy;

    move-result-object v0

    iput-object v0, p0, Lbon;->d:Lbmy;

    .line 39
    invoke-interface {p3}, Lbkr;->d()Lbku;

    move-result-object v0

    const-string v1, "pointMetrics"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbku;

    .line 40
    invoke-virtual {v0}, Lbku;->d()Lbms;

    move-result-object v0

    iput-object v0, p0, Lbon;->e:Lbms;

    .line 42
    invoke-interface {p3}, Lbkr;->c()Lbkn;

    move-result-object v0

    const-string v1, "generalMetrics"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkn;

    .line 43
    invoke-virtual {v0}, Lbkn;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lbon;->f:J

    .line 44
    invoke-virtual {v0}, Lbkn;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lbon;->g:J

    .line 45
    iput-boolean p4, p0, Lbon;->h:Z

    .line 46
    return-void
.end method


# virtual methods
.method public a(Lbzg;J)J
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lbon;->b:Lboo;

    iget v0, v0, Lboo;->c:I

    iget-object v1, p0, Lbon;->b:Lboo;

    iget v1, v1, Lboo;->d:I

    mul-int/2addr v0, v1

    invoke-interface {p1}, Lbzg;->m()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 104
    :goto_0
    return-wide p2

    :cond_0
    invoke-virtual {p0}, Lbon;->h()Lbms;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lbms;->b(J)J

    move-result-wide p2

    goto :goto_0
.end method

.method public a()Ljeg;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbon;->a:Ljeg;

    return-object v0
.end method

.method public b(Lbzg;J)J
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lbon;->b:Lboo;

    iget v0, v0, Lboo;->c:I

    iget-object v1, p0, Lbon;->b:Lboo;

    iget v1, v1, Lboo;->d:I

    mul-int/2addr v0, v1

    invoke-interface {p1}, Lbzg;->m()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 121
    :goto_0
    return-wide p2

    :cond_0
    invoke-virtual {p0}, Lbon;->h()Lbms;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lbms;->c(J)J

    move-result-wide p2

    goto :goto_0
.end method

.method public synthetic b()Lbmu;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lbon;->g()Lboo;

    move-result-object v0

    return-object v0
.end method

.method public c()Lbol;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lbon;->c:Lbol;

    return-object v0
.end method

.method public d()Lbmy;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lbon;->d:Lbmy;

    return-object v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lbon;->f:J

    return-wide v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lbon;->h:Z

    return v0
.end method

.method public g()Lboo;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbon;->b:Lboo;

    return-object v0
.end method

.method public h()Lbms;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lbon;->e:Lbms;

    return-object v0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lbon;->g:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 127
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbon;->a:Ljeg;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbon;->b:Lboo;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lbon;->f:J

    .line 130
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, p0, Lbon;->g:J

    .line 131
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lbon;->h:Z

    .line 132
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 127
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
