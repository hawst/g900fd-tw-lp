.class public Lbxa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbxe;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbxc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lbxa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "metrics"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcfi;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbxa;->a:Ljava/util/List;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Lbok;Lbmr;Lbok;Lbmr;)F
    .locals 4

    .prologue
    .line 35
    const/4 v0, 0x0

    .line 36
    iget-object v1, p0, Lbxa;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxc;

    .line 37
    iget-object v3, v0, Lbxc;->b:Lbxe;

    invoke-interface {v3, p1, p2, p3, p4}, Lbxe;->b(Lbok;Lbmr;Lbok;Lbmr;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 38
    iget-object v3, v0, Lbxc;->b:Lbxe;

    .line 39
    invoke-interface {v3, p1, p2, p3, p4}, Lbxe;->a(Lbok;Lbmr;Lbok;Lbmr;)F

    move-result v3

    .line 40
    iget v0, v0, Lbxc;->a:F

    mul-float/2addr v0, v3

    add-float/2addr v0, v1

    :goto_1
    move v1, v0

    .line 45
    goto :goto_0

    .line 46
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public b(Lbok;Lbmr;Lbok;Lbmr;)Z
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lbxa;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxc;

    .line 55
    iget-object v0, v0, Lbxc;->b:Lbxe;

    invoke-interface {v0, p1, p2, p3, p4}, Lbxe;->b(Lbok;Lbmr;Lbok;Lbmr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x1

    .line 59
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
